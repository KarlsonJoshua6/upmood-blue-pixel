//
//  MoodMixAPI.swift
//  upmood
//
//  Created by John Paul Manoza on 27/08/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import KeychainSwift
import MediaPlayer

// MARK: - APIs Call

extension MoodMixDashboardViewController {
    
    func removeAllThemeArr() {
        themeName.removeAll()
        themeImg.removeAll()
        themeDesc.removeAll()
    }
    
    func removeAllMoodArr() {
        moodName.removeAll()
        moodDefaultImage.removeAll()
        moodSelectedImage.removeAll()
    }
    
    func removeAllMusicArr() {
        self.musicTitle.removeAll()
        self.musicArtist.removeAll()
        self.musicUrl.removeAll()
        self.musicDuration.removeAll()
        self.musicAlbumCover.removeAll()
    }
    
    func getPlayList(emotion: String, stressLevel: String, mood: String, theme: String, id_list: String, playedSongFromPlayerView: Bool) {
        
        var url: URL!
        if id_list.isEmpty {
            url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/moodmix/playlist?emotion=\(emotion)&stress_level=\(stressLevel)&mood=\(mood)&theme=\(theme.replacingOccurrences(of: " ", with: "%20").lowercased())&limit=1000&id_list=")
        } else {
            url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/moodmix/playlist?emotion=\(emotion)&stress_level=\(stressLevel)&mood=\(mood)&theme=\(theme.replacingOccurrences(of: " ", with: "%20").lowercased())&limit=1000&id_list=\(id_list)")
        }
        
        var request = URLRequest(url: url)
        let userToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(userToken)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        print("url: \(url)")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
            
//            if id_list.isEmpty {
//                self.removeAllMusicArr()
//            }
            
            self.removeAllMusicArr()
                
            guard let jsonDic = userObject as? [String: Any] else { return }
            let statusCode = jsonDic["status"] as? Int ?? 0
            let messsage = jsonDic["status"] as? Int ?? 0
    
            guard let data = jsonDic["data"] as? [String: Any] else { return }
            guard let dictionary = data["playlist"] as? [[String: Any]] else { return }
    
            switch statusCode {
            case 200:
                for dataInsideOfDictionary in dictionary {
                    let name = dataInsideOfDictionary["name"] as? String ?? ""
                    let artist = dataInsideOfDictionary["artist"] as? String ?? ""
                    let file = dataInsideOfDictionary["file_path"] as? String ?? ""
                    let image_path = dataInsideOfDictionary["image_path"] as? String ?? ""
                    let duration = dataInsideOfDictionary["duration"] as? Int ?? 0
                    self.musicTitle.append(name)
                    self.musicArtist.append(artist)
                    self.musicUrl.append(file)
                    self.musicDuration.append(duration)
                    self.musicAlbumCover.append(image_path)
                }
                
                self.id_list = data["id_list"] as? String ?? ""
                
                print("musicUrl counts: \(self.musicUrl.count)")
                if self.musicUrl.count < 10 {
                    self.id_list = ""
                }
                
                DispatchQueue.main.async {
                    self.getAndPlayMp3(withIndex: self.musicIndex, playedSongFromPlayerView: playedSongFromPlayerView)
                }
                
                break
            default:
                debugPrint("error : \(messsage)")
            }
                
        }}.resume()
    }
    
   func getArtwork(from url: URL) -> MPMediaItemArtwork {
        if let data = try? Data.init(contentsOf: url), let image = UIImage(data: data) {
            let artwork = MPMediaItemArtwork(boundsSize: image.size, requestHandler: { (_ size : CGSize) -> UIImage in
                return image
            })
            
            return artwork
        
        } else {
            
            let artworkDefault = MPMediaItemArtwork.init(boundsSize: UIImage(named: "default")!.size, requestHandler: { (size) -> UIImage in
                 return UIImage(named: "default")!
             })
            
            return artworkDefault
        }
    }
    
    func getAlbumCover(from url: URL) -> UIImage {
         if let data = try? Data.init(contentsOf: url), let image = UIImage(data: data) {
             return image
         } else {
            return UIImage(named: "default")!
         }
     }
    
    func getMood() {
        
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/moodmix/mood") else { return }
        
        var request = URLRequest(url: url)
        let userToken = KeychainSwift().get("apiToken") ?? ""
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(userToken)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        print("url getMood: \(url)")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
 
            self.removeAllMoodArr()
                
            guard let jsonDic = userObject as? [String: Any] else { return }
            let statusCode = jsonDic["status"] as? Int ?? 0
            let messsage = jsonDic["status"] as? Int ?? 0
    
            guard let dictionary = jsonDic["data"] as? [[String: Any]] else { return }
    
            switch statusCode {
            case 200:
                for dataInsideOfDictionary in dictionary {
                    let name = dataInsideOfDictionary["name"] as? String ?? ""
                    let default_image = dataInsideOfDictionary["default_image"] as? String ?? ""
                    let selected_image = dataInsideOfDictionary["selected_image"] as? String ?? ""
                    
                    self.moodName.append(name)
                    self.moodDefaultImage.append(default_image)
                    self.moodSelectedImage.append(selected_image)
                }
   
                
                DispatchQueue.main.async {
                    self.moodTableView.reloadData()
                    self.viewWillLayoutSubviews()
                }
                
                break
            default:
                debugPrint("error : \(messsage)")
            }
                
        }}.resume()
    }
    
    func getTheme() {
        
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/moodmix/themes") else { return }
        
        var request = URLRequest(url: url)
        let userToken = KeychainSwift().get("apiToken") ?? ""
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(userToken)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        print("url getTheme: \(url)")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
            self.removeAllThemeArr()
            guard let jsonDic = userObject as? [String: Any] else { return }
            let statusCode = jsonDic["status"] as? Int ?? 0
            let messsage = jsonDic["status"] as? Int ?? 0
    
            guard let dictionary = jsonDic["data"] as? [[String: Any]] else { return }
    
            switch statusCode {
            case 200:
                for dataInsideOfDictionary in dictionary {
                    let name = dataInsideOfDictionary["name"] as? String ?? ""
                    let image = dataInsideOfDictionary["image"] as? String ?? ""
                    let description = dataInsideOfDictionary["description"] as? String ?? ""
                    
                    self.themeName.append(name)
                    self.themeImg.append(image)
                    self.themeDesc.append(description)
                }
                
                DispatchQueue.main.async {
                    self.themeCollectionView.reloadData()
                    self.themeCollectionView.layoutIfNeeded()
                    self.themeCollectionViewHeight.constant = self.themeCollectionView.contentSize.height
                }
                
                break
            default:
                debugPrint("error : \(messsage)")
            }
                
        }}.resume()
    }
}
