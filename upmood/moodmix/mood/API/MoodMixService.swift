//
//  PublicSessionService.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

class MoodMixService {
    public func getMood(onSuccess successCallback: ((_ data: [MoodModel]) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
    
        MoodMixAPICallManager.instance.getMood (
            onSuccess: { (data) in
                successCallback?(data)
            },
            onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
            }
        )
    }
    
    public func getTheme(onSuccess successCallback: ((_ data: [ThemeModel]) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
    
        MoodMixAPICallManager.instance.getTheme (
            onSuccess: { (data) in
                successCallback?(data)
            },
            onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
            }
        )
    }
}

