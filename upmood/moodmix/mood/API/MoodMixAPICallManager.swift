//
//  APICallManager.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 13/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import Foundation
import KeychainSwift
import SwiftyJSON

let moodMixUserToken = KeychainSwift().get("apiToken") ?? ""

class MoodMixAPICallManager {
    
    let header: HTTPHeaders = ["Accept": "application/json", "Authorization": "Bearer \(moodMixUserToken)"]
    let params: [String: Any] = ["keyword": "", "timezone": "Asia/Manila"]
    
    static let instance = MoodMixAPICallManager()
    
    enum RequestMethod {
        case get
        case post
    }
    
    enum Endpoint: String {
        case getMood = "/api/v4/ios/moodmix/mood"
        case getTheme = "/api/v4/ios/moodmix/themes"
    }
    
    // MARK: Get Mood
    func getMood(onSuccess successCallback: ((_ data: [MoodModel]) -> Void)?,
                          onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        // Build URL
        let url = Constants.Routes.BASE + Endpoint.getMood.rawValue
        
        print("url po ito: \(url)")
  
        // call API
        self.createRequest(
            url, method: .get, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                print("response po ti: \(responseObject)")
                
                let statusCode = responseObject["status"].intValue
                
                switch statusCode {
                case 200:
//                    // Create dictionary
                    if let responseDict = responseObject["data"].arrayObject {
                       let moodDict = responseDict as! [[String:AnyObject]]

                        // Create object
                        var data = [MoodModel]()
                        for item in moodDict {
                            let single = MoodModel.build(item)
                            data.append(single)
                        }

                        // Fire callback
                        successCallback?(data)
                    } else {
                        failureCallback?("An error has occured.")
                    }
                    break
                case 204:
                    failureCallback?("An error has occured. statusCode == 204")
                    break
                case 400...500:
                    failureCallback?("An error has occured. statusCode == 400...500")
                    break
                default:
                    failureCallback?("An error has occured.")
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    // MARK: Get Theme
    func getTheme(onSuccess successCallback: ((_ data: [ThemeModel]) -> Void)?,
                          onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {

        let url = Constants.Routes.BASE + Endpoint.getTheme.rawValue

        self.createRequest(
            url, method: .get, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                
                switch statusCode {
                case 200:
                    if let responseDict = responseObject["data"].arrayObject {
                       let themeDict = responseDict as! [[String:AnyObject]]
                        
                        var data = [ThemeModel]()
                        for item in themeDict {
                            let single = ThemeModel.build(item)
                            data.append(single)
                        }

                        successCallback?(data)
                    } else {
                        failureCallback?("An error has occured.")
                    }
                    break
                case 204:
                    failureCallback?("An error has occured. statusCode == 204")
                    break
                case 400...500:
                    failureCallback?("An error has occured. statusCode == 400...500")
                    break
                default:
                    failureCallback?("An error has occured.")
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    // MARK: Request Handler
    // Create request
    func createRequest(
        _ url: String,
        method: HTTPMethod,
        headers: [String: String],
        parameters: [String: Any],
        encoding: JSONEncoding,
        onSuccess successCallback: ((JSON) -> Void)?,
        onFailure failureCallback: ((String) -> Void)?
        ) {
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                successCallback?(json)
            case .failure(let error):
                if let callback = failureCallback {
                    // Return
                    callback(error.localizedDescription)
                }
            }
        }
    }
}
