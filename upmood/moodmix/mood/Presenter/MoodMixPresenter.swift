//
//  PublicSessionPresenter.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

// MARK: - Setup View Data

struct MoodMixViewMoodData {
    let name: String
    let default_image: String
    let selected_image: String
}

struct MoodMixThemeViewData {
    let name: String
    let image: String
    let description: String
}

// MARK: - Setup View with Object Protocol

protocol MoodMixView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func getMood(mood: [MoodMixViewMoodData])
    func getTheme(theme: [MoodMixThemeViewData])
    func setEmptyData()
}

// MARK: - Setup Presenter

class MoodMixPresenter {
    
    private let moodMixService: MoodMixService
    weak private var moodMixView : MoodMixView?
    
    init(moodMixService: MoodMixService) {
        self.moodMixService = moodMixService
    }
    
    func attachView(view: MoodMixView) {
        moodMixView = view
    }
    
    func detachView() {
        moodMixView = nil
    }
    
    func getMood() {
        self.moodMixView?.startLoading()
        
        moodMixService.getMood(onSuccess: { (data) in
        
            self.moodMixView?.finishLoading()
            
            if (data.count == 0) {
                self.moodMixView?.setEmptyData()
            } else {
                let mappedData = data.map {
                    return MoodMixViewMoodData(name: "\($0.name ?? "")", default_image: "\($0.default_image ?? "")", selected_image: "\($0.selected_image ?? "")")
                }
                
                self.moodMixView?.getMood(mood: mappedData)
            }
            
        }) { (errorMessage) in
            debugPrint(errorMessage)
            self.moodMixView?.finishLoading()
        }
    }
    
    func getTheme() {
        self.moodMixView?.startLoading()
        
        moodMixService.getTheme(onSuccess: { (data) in
        
            self.moodMixView?.finishLoading()
            
            if (data.count == 0) {
                self.moodMixView?.setEmptyData()
            } else {
                let mappedData = data.map {
                    return MoodMixThemeViewData(name: "\($0.name ?? "")", image: "\($0.image ?? "")", description: "\($0.description ?? "")")
                }
                
                self.moodMixView?.getTheme(theme: mappedData)
            }
            
        }) { (errorMessage) in
            debugPrint(errorMessage)
            self.moodMixView?.finishLoading()
        }
    }
}
