//
//  PublicSession.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

class MoodModel {
    var id: Int?
    var name: String?
    var default_image: String?
    var selected_image: String?
    var status: Int?
    
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: AnyObject]) {
        if let data = dict["id"] as? Int {
            self.id = data
        }
        if let data = dict["name"] as? String {
            self.name = data
        }
        if let data = dict["default_image"] as? String {
            self.default_image = data
        }
        if let data = dict["selected_image"] as? String {
            self.selected_image = data
        }
        if let data = dict["status"] as? Int {
            self.status = data
        }
    }
    
    // MARK: Class Method
    class func build(_ dict: [String: AnyObject]) -> MoodModel {
        let session = MoodModel()
        session.loadFromDictionary(dict)
        return session
    }
}
