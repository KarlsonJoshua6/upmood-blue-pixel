//
//  PublicSession.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

class ThemeModel {
    var id: Int?
    var name: String?
    var image: String?
    var status: Int?
    var description: String?
    
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: AnyObject]) {
        if let data = dict["id"] as? Int {
            self.id = data
        }
        if let data = dict["name"] as? String {
            self.name = data
        }
        if let data = dict["image"] as? String {
            self.image = data
        }
        if let data = dict["status"] as? Int {
            self.status = data
        }
        if let data = dict["description"] as? String {
            self.description = data
        }
    }
    
    // MARK: Class Method
    class func build(_ dict: [String: AnyObject]) -> ThemeModel {
        let session = ThemeModel()
        session.loadFromDictionary(dict)
        return session
    }
}
