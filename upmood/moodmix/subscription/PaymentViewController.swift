//
//  PaymentViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 29/08/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Braintree
import Foundation
import SVProgressHUD
import UIKit

class PaymentViewController: UIViewController {
    
    var braintreeClient: BTAPIClient!
    
    override func viewDidLoad() {
        initBraintree()
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func checkOut(_ sender: UIButton) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        startCheckout()
    }
    
    private func initBraintree() {
        // Example: Initialize BTAPIClient, if you haven't already
        braintreeClient = BTAPIClient(authorization: "sandbox_mfkpp6g9_zzvj37bn4dnwvwjm")
    }
    
    func startCheckout() {
        
        let payPalDriver = BTPayPalDriver(apiClient: braintreeClient)
        payPalDriver.viewControllerPresentingDelegate = self
        payPalDriver.appSwitchDelegate = self // Optional
        
        let request = BTPayPalRequest(amount: "35.00")
        request.currencyCode = "USD"

        payPalDriver.requestOneTimePayment(request) { (tokenizedPayPalAccount, error) in
            SVProgressHUD.dismiss()
            if let tokenizedPayPalAccount = tokenizedPayPalAccount {
                print("Got a nonce: \(tokenizedPayPalAccount.nonce)")

                // Access additional information
                let email = tokenizedPayPalAccount.email
                let firstName = tokenizedPayPalAccount.firstName
                let lastName = tokenizedPayPalAccount.lastName
                let phone = tokenizedPayPalAccount.phone

                // See BTPostalAddress.h for details
//                let billingAddress = tokenizedPayPalAccount.billingAddress
//                let shippingAddress = tokenizedPayPalAccount.shippingAddress
                
                print("email: \(email!) \nName: \(firstName!) \(lastName!)\n Phone: \(phone!)")
                Constants.User.isPremiumUser = true
                self.performSegue(withIdentifier: "showSuccessSubscription", sender: self)
                
            } else if let error = error {
                SVProgressHUD.dismiss()
                self.presentDismissableAlertController(title: Constants.MessageDialog.errorTitle, message: error.localizedDescription)
            } else {
                SVProgressHUD.dismiss()
                self.presentDismissableAlertController(title: Constants.MessageDialog.errorTitle, message: "Buyer canceled payment approval")
            }
        }
    }
}

extension PaymentViewController: BTAppSwitchDelegate, BTViewControllerPresentingDelegate {
    
    // MARK: - BTViewControllerPresentingDelegate
    
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
        present(viewController, animated: true, completion: nil)
    }

    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }

    // MARK: - BTAppSwitchDelegate


    // Optional - display and hide loading indicator UI
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
        showLoadingUI()

        NotificationCenter.default.addObserver(self, selector: #selector(hideLoadingUI), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }

    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
        hideLoadingUI()
    }

    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {

    }

    // MARK: - Private methods

    func showLoadingUI() {
        
    }

    @objc func hideLoadingUI() {
        NotificationCenter
            .default
            .removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
}
