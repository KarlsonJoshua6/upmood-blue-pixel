//
//  SubscriptionViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 29/08/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import UIKit

class SubscriptionViewController: UIViewController {
    
    override func viewDidLoad() {
        
    }
    
    @IBAction func close(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
