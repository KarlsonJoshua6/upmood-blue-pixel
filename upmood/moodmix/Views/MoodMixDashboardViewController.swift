//
//  ViewController.swift
//  moodmix
//
//  Created by John Paul Manoza on 11/08/2020.
//  Copyright © 2020 Joseph MIkko Manoza. All rights reserved.
//

import AWSS3
import AVFoundation
import KeychainSwift
import CoreBluetooth
import MediaPlayer
import MarqueeLabel
import PBPopupController
import SDWebImage
import UIKit

class MoodMixDashboardViewController: UIViewController {
    
    @IBOutlet weak var musicTitleLabel: UILabel!
    @IBOutlet weak var musicArtistLabel: UILabel!
    @IBOutlet weak var playIcon: UIImageView!
    @IBOutlet weak var musicAlbumCoverImage: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playView: UIView!
    @IBOutlet weak var bufferingIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var moodTableView: UITableView!
    @IBOutlet weak var themeCollectionView: UICollectionView!
    @IBOutlet weak var sleepButton: UIButton!
    @IBOutlet weak var subscriptionView: UIView!
    
    @IBOutlet weak var moodLabel: UILabel!
    @IBOutlet weak var themeLabel: UILabel!
    @IBOutlet weak var stresslvlLabel: UILabel!
    @IBOutlet weak var emotionLabel: UILabel!
    @IBOutlet weak var songsCount: UILabel!
    @IBOutlet weak var currentSongCount: UILabel!
    @IBOutlet weak var syncEmotionSwitchBar: UISwitch!
    @IBOutlet weak var debugView: UIView!
    @IBOutlet weak var debugButton: UIButton!
    
    //contraints
    @IBOutlet weak var moodTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var themeCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var subscriptionViewHeight: NSLayoutConstraint!
    
    // params
    var mood = ""
    var theme = ""
    var stressLevel = ""
    var emotion = ""
    
    // when double tap themes values
    var doubleTapTheme = ""
    var isDoubleTapTheme = true
    var doubleTapIndex = 0
    
    // stored values
    var musicUrl = [String]()
    var musicTitle = [String]()
    var musicArtist = [String]()
    var musicAlbumCover = [String]()
    var musicDuration = [Int]()
    var musicIndex = 0
    var currentPage = 1
    var id_list = ""
    var selectedIndex = 0
    var albumCoverURL: URL!
    var isPremium = false
    var doubleTapGesture: UITapGestureRecognizer!
    var doubleTapGestureMood: UITapGestureRecognizer!
    var doubleTapGestureButton: UITapGestureRecognizer!
    var selectedIndexPath1: IndexPath? = nil
    var trackElapsedTime = 0
    
    // mood
    var moodName = [String]()
    var moodDefaultImage = [String]()
    var moodSelectedImage = [String]()
    
    // themes
    var themeName = [String]()
    var themeDesc = [String]()
    var themeImg  = [String]()
    
    // band and speaker shared
    var bluetooManager = BluetoothManager.getInstance()
    let speakerManager = BluetoothManagerSpeaker.getInstance()
    var writeChannel: CBCharacteristic!
    var syncOnSpeaker = false
    
    // timer for sleep mood
    var timer: Timer!
    var hasClickCommandCenter = false
    
    //
    var myPlayer = Player.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()

        // init delegations
        moodTableView.delegate = self
        moodTableView.dataSource = self
        moodTableView.delaysContentTouches = true
        themeCollectionView.dataSource = self
        themeCollectionView.delegate = self
        themeCollectionView.delaysContentTouches = true
        tabBarController?.popupController.delegate = self
        tabBarController?.popupBar.dataSource = self
        
        // init UIs
        themeCollectionView.allowsMultipleSelection = false
        bufferingIndicatorView.isHidden = true
        setUpDoubleTap()
        
        // operations
        setupAVAudioSession()
        setUpCommandCenter()
        getMood()
        getTheme()
        isDoubleTapTheme = false
        
        if let popupBar = self.tabBarController?.popupBar {
            initMiniPlayer(popupBar: popupBar)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        // note: commend this is there's something wrong happen with constraints
        //self.moodTableViewHeight?.constant = self.moodTableView.contentSize.height
        self.moodTableViewHeight?.constant = 230.0
        self.themeCollectionViewHeight?.constant = self.themeCollectionView.contentSize.height
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isTranslucent = false
        tabBarController?.tabBar.barTintColor = .white
        if self.traitCollection.userInterfaceStyle == .dark {
            tabBarController?.tabBar.isTranslucent = false
            tabBarController?.tabBar.barTintColor = hexStringToUIColor(hex: "#2F3334")
        } else {
            tabBarController?.tabBar.isTranslucent = false
            tabBarController?.tabBar.barTintColor = .white
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //let hasTrial = UserDefaults.standard.integer(forKey: "has_trial")
        let subsciptionStatus = UserDefaults.standard.integer(forKey: "subscription_status")
        
        if subsciptionStatus == 1 || subsciptionStatus == 2 {
            isPremium = true
        } else {
            isPremium = false
        }
        
        self.setUpSubscriptionView()
        self.initObs()
        self.getSegueSync()
        self.navigationController?.navigationBar.isHidden = true
        tabBarController?.tabBar.isTranslucent = false
        tabBarController?.tabBar.barTintColor = hexStringToUIColor(hex: Constants.Color.MOOD_MIX_PRIMARY_COLOR)
        playPause()
    }
    
    @IBAction func debugMode(_ sender: UIButton) {
        if debugButton.titleLabel?.text == "Debug Mode?" {
            debugView.isHidden = false
            debugButton.setTitle("RMV Debug Mode?", for: .normal)
        } else {
            debugView.isHidden = true
            debugButton.setTitle("Debug Mode?", for: .normal)
        }
    }
    
    @IBAction func unwindColorWheel(_ segue: UIStoryboardSegue) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let playListViewController = segue.destination as? PlaylistViewController  {
            
            playListViewController.theme = theme
            playListViewController.mood = mood
            playListViewController.stressLevel = stressLevel
            playListViewController.emotion = emotion
            playListViewController.playlistDelegate = self
//            playListViewController.id_list = id_list
            
//            if theme != "sleep" {
                if isDoubleTapTheme {
                    playListViewController.doubleTapTheme = doubleTapTheme
                    playListViewController.playlistDeescription = themeDesc[doubleTapIndex]
                    playListViewController.playlistImageUrlString = themeImg[doubleTapIndex]
                    playListViewController.isDoubleTapTheme = isDoubleTapTheme
                } else {
                    playListViewController.playlistDeescription = themeDesc[selectedIndex]
                    playListViewController.playlistImageUrlString = themeImg[selectedIndex]
                }
                
                //if theme == doubleTapTheme {
                    //pass preload data
                    passDataToPlaylist(playListViewController: playListViewController)
                //}
//            }
        }
        
        if let colorWheelViewController = segue.destination as? ColorWheelViewController {
            
            colorWheelViewController.writeChannel = writeChannel
        }
    }
    
    @IBAction func btnOpenColorWheel(_ sender: Any) {
        if !speakerManager.connected {
              performSegue(withIdentifier: "connectSpeaker", sender: self)
        } else{
             performSegue(withIdentifier: "colorWheel", sender: self)
        }
    }
    
    @IBAction func nextSong(_ sender: Any) {
        
        if hasClickCommandCenter == false {
            let senderButton = sender as! UIButton
            senderButton.preventRepeatedPresses()
        }
        
        if !bluetooManager.connected {
            self.emotion = ""
            self.stressLevel = ""
        }
        
        myPath = nil
        
        if !musicUrl.isEmpty {
            Constants.User.isRepeatSong = false
            if musicIndex == musicUrl.count - 1 {
                // next song with pagination
//                musicIndex = musicUrl.count - 1 + 1
//                self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme, id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
                
                musicIndex = 0
                getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: true)
            } else {
                // next the song normally
                if emotion.isEmpty && stressLevel.isEmpty {
                    
                    if Constants.User.isRepeatSong == false {
                        musicIndex = musicIndex + 1
                    }
                    
                    getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: false)
                    
                } else {
                    // next requested song because changes of emotion/stress level
                    self.id_list = ""
                    self.musicIndex = 0
                    
                    self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme, id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
                }
            }
        }
    }
    
    @IBAction func previousSong(_ sender: Any) {
        
        if hasClickCommandCenter == false {
            let senderButton = sender as! UIButton
            senderButton.preventRepeatedPresses()
        }
        
        myPath = nil
        
        if !musicUrl.isEmpty {
            if trackElapsedTime < 3 {
                
                if syncEmotionSwitchBar.isOn {
                    presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: "Please turn off sync emotion to play previous song.")
                    getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: false)
                } else {
                    musicIndex = musicIndex - 1
                    if musicIndex <= -1 {
                        musicIndex = 0
                    }
                    
                    getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: false)
                }
                
            } else {
                getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: false)
            }
        }
    }
    
    @IBAction func playSong(_ sender: UIButton) {
        
        if myPlayer.player == nil {
            
            self.id_list = ""
            self.musicIndex = 0
            self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme, id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
        } else {
            if myPlayer.player.rate == 1 {
                myPlayer.player.pause()
                playIcon.image = UIImage(named: "pause")
                
                let playButtonItem = UIBarButtonItem(image: UIImage(named: "play"), style: .plain, target: self, action: #selector(playPauseAction))
                self.tabBarController?.popupBar.rightBarButtonItems = [playButtonItem]
                
            } else {
                myPlayer.player.play()
                playIcon.image = UIImage(named: "play")
                
                let pauseButtonItem = UIBarButtonItem(image: UIImage(named: "pause"), style: .plain, target: self, action: #selector(playPauseAction))
                self.tabBarController?.popupBar.rightBarButtonItems = [pauseButtonItem]
            }
        }
    }

    @IBAction func syncEmotion(_ sender: UISwitch) {
        if sender.isOn {
            if !bluetooManager.connected {
                showWarningDialog()
            } else {
                // SYNC EMOTION/STRESS BASE ON GATHER DATA FROM YOUR BAND
                self.syncEmotionSwitchBar.isOn = true
                Constants.syncBool = true
                Constants.User.isRepeatSong = false
                self.currentPage = 1
            }
        } else {
            self.emotion = ""
            self.stressLevel = ""
            self.stresslvlLabel.text = "stress lvl gathered: Off"
            self.emotionLabel.text = "Emotion gathered: Off"
            self.syncEmotionSwitchBar.isOn = false
            Constants.syncBool = false
        }
    }
    
    @IBAction func showSubcribe(_ sender: UIButton) {
        performSegue(withIdentifier: "showSubcribe", sender: self)
    }
    
    @IBAction func showSleep(_ sender: UIButton) {
        if isPremium {
            self.mood = ""
            self.theme = "sleep"
            myPath = nil
            Constants.didStartSleepMood = true
            
            self.reloadList()

            self.id_list = ""
            self.musicIndex = 0
            self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme, id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)

            sender.setBackgroundImage(UIImage(named: "sleep select"), for: .normal)
            
        } else {
            performSegue(withIdentifier: "showSubcribe", sender: self)
        }
    }
    
    @IBAction func unwindPlaylist(_ segue: UIStoryboardSegue) {
        //stops,clear and play new songs
        
        if self.theme != "sleep" {
            sleepButton.setBackgroundImage(UIImage(named: "Sleep default1"), for: .normal)
        } else {
            moodTableView.reloadData()
            sleepButton.setBackgroundImage(UIImage(named: "sleep select"), for: .normal)
        }
        
        
        self.id_list = ""
        //self.removeAllMusicArr()
        self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme, id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
    }
    
    @IBAction func reloadMoodMix(_ segue: UIStoryboardSegue) {
        myPlayer.resetPlayer()
        viewWillAppear(true)
        moodTableView.reloadData()
        themeCollectionView.reloadData()
    }
    
    private func setUpSubscriptionView() {
        if isPremium {
            subscriptionView.isHidden = true
            subscriptionViewHeight.constant = 0.0
        } else {
            subscriptionView.isHidden = false
            subscriptionViewHeight.constant = 140.0
        }
    }
    
    private func showWarningDialog() {
        let alertController = UIAlertController(title: Constants.MessageDialog.WARINING, message: "Please connect your band/watch first.", preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: "Close", style: .cancel)
        { action -> Void in
            self.syncEmotionSwitchBar.isOn = false
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func initObs() {
        // init observer
        NotificationCenter.default.removeObserver(self, name: .currentEmotion, object: nil)
        NotificationCenter.default.removeObserver(self, name: .currentStressLevel, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didDisconnectBand, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getEmotion(_:)), name: .currentEmotion, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getStressLevel(_:)), name: .currentStressLevel, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didDisconnectBand(_:)), name: .didDisconnectBand, object: nil)
    }
    
    private func reloadList() {
        self.moodTableView.reloadData()
        self.viewWillLayoutSubviews()
        
        self.themeCollectionView.reloadData()
        self.themeCollectionView.layoutIfNeeded()
        self.themeCollectionViewHeight.constant = self.themeCollectionView.contentSize.height
    }
}

// MARK: - PBPopupControllerDelegate, PBPopupBarDataSource

extension MoodMixDashboardViewController: PBPopupControllerDelegate, PBPopupBarDataSource {
    
    func popupController(_ popupController: PBPopupController, didClose popupContentViewController: UIViewController) {
        
        showPopUpMiniPlayer()
        
        if myPlayer.player.rate == 1 {
            playIcon.image = UIImage(named: "pause")
            let playButtonItem = UIBarButtonItem(image: UIImage(named: "pause"), style: .plain, target: self, action: #selector(playPauseAction))
            self.tabBarController?.popupBar.rightBarButtonItems = [playButtonItem]
        } else {
            playIcon.image = UIImage(named: "play")
            let pauseButtonItem = UIBarButtonItem(image: UIImage(named: "play"), style: .plain, target: self, action: #selector(playPauseAction))
            self.tabBarController?.popupBar.rightBarButtonItems = [pauseButtonItem]
        }
        
        NotificationCenter.default.post(name: .didCloseMiniPlayer, object: nil, userInfo: ["music_index": musicIndex])
    }
    
    private func passDataToPlaylist(playListViewController: PlaylistViewController) {
        playListViewController.musicIndex = musicIndex
        playListViewController.musicUrl = musicUrl
        playListViewController.musicTitle = musicTitle
        playListViewController.musicArtist = musicArtist
        playListViewController.musicAlbumCover = musicAlbumCover
        playListViewController.musicDuration = musicDuration
    }
    
    private func passDataToPlayer(playerViewController: PlayerViewController) {
        playerViewController.musicIndex = musicIndex
        playerViewController.musicUrl = musicUrl
        playerViewController.musicTitle = musicTitle
        playerViewController.musicArtist = musicArtist
        playerViewController.musicDuration = musicDuration
        playerViewController.albumCoverUrl = musicAlbumCover
    }
    
    func showPopUpMiniPlayer() {
        guard let popupBar = self.tabBarController?.popupBar else { return }
        
        initMiniPlayer(popupBar: popupBar)
        updatePopUpPlayer(popupBar: popupBar)
        
        if let player = tabBarController?.popupContentViewController as? PlayerViewController {
            passDataToPlayer(playerViewController: player)
            player.playerDelegate = self
        } else {
            let player = self.storyboard?.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
            player.playerDelegate = self
            passDataToPlayer(playerViewController: player)
            tabBarController?.presentPopupBar(withPopupContentViewController: player, animated: true, completion: nil)
        }
    }
    
    private func updatePopUpPlayer(popupBar: PBPopupBar) {
        popupBar.image = getAlbumCover(from: self.albumCoverURL)
        popupBar.title = musicTitle[musicIndex]
        popupBar.subtitle = musicArtist[musicIndex]
        popupBar.rightBarButtonItems = [
            UIBarButtonItem(image: UIImage(named: "pause"), style: .plain, target: self, action: #selector(playPauseAction))
        ]
    }
    
    private func initMiniPlayer(popupBar: PBPopupBar) {
        popupBar.popupBarStyle = .default
        popupBar.barTintColor = hexStringToUIColor(hex: Constants.Color.MOOD_MIX_PRIMARY_COLOR)
        popupBar.backgroundColor = hexStringToUIColor(hex: Constants.Color.MOOD_MIX_PRIMARY_COLOR)
        popupBar.tintColor = .white
        popupBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        popupBar.subtitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        popupBar.isTranslucent = false
    }
    
    @objc func playPauseAction() {
        if myPlayer.player != nil {
            if myPlayer.player.rate == 1 {
                let playButtonItem = UIBarButtonItem(image: UIImage(named: "play"), style: .plain, target: self, action: #selector(playPauseAction))
                self.tabBarController?.popupBar.rightBarButtonItems = [playButtonItem]
                myPlayer.player.pause()
            } else {
                let pauseButtonItem = UIBarButtonItem(image: UIImage(named: "pause"), style: .plain, target: self, action: #selector(playPauseAction))
                self.tabBarController?.popupBar.rightBarButtonItems = [pauseButtonItem]
                myPlayer.player.play()
            }
        }
    }
    
    func titleLabel(for popupBar: PBPopupBar) -> UILabel? {
        return label
    }
    
    func subtitleLabel(for popupBar: PBPopupBar) -> UILabel? {
         return sublabel
    }
    
    func playPause() {
        if myPlayer.player != nil {
            if myPlayer.player.rate == 1 {
                let playButtonItem = UIBarButtonItem(image: UIImage(named: "pause"), style: .plain, target: self, action: #selector(playPauseAction))
                self.tabBarController?.popupBar.rightBarButtonItems = [playButtonItem]
            } else {
                let pauseButtonItem = UIBarButtonItem(image: UIImage(named: "play"), style: .plain, target: self, action: #selector(playPauseAction))
                self.tabBarController?.popupBar.rightBarButtonItems = [pauseButtonItem]
            }
        }
    }
}

// MARK: - UI

extension MoodMixDashboardViewController {
    
    func setUpDoubleTap() {
        doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(didDoubleTapCollectionView))
        doubleTapGesture.numberOfTapsRequired = 2
        themeCollectionView.addGestureRecognizer(doubleTapGesture)
        doubleTapGesture.delaysTouchesBegan = true
        
        doubleTapGestureButton = UITapGestureRecognizer(target: self, action: #selector(didDoubleTapSleepButton))
        doubleTapGestureButton.numberOfTapsRequired = 2
        sleepButton.addGestureRecognizer(doubleTapGestureButton)
        doubleTapGestureButton.delaysTouchesBegan = true
        
        doubleTapGestureMood = UITapGestureRecognizer(target: self, action: #selector(didDouleTapMood))
        doubleTapGestureMood.numberOfTapsRequired = 2
        moodTableView.addGestureRecognizer(doubleTapGestureMood)
        doubleTapGestureMood.delaysTouchesBegan = true
    }
    
    @objc func didDoubleTapCollectionView() {
        
        let pointInCollectionView = doubleTapGesture.location(in: themeCollectionView)
        if let selectedIndexPath = themeCollectionView.indexPathForItem(at: pointInCollectionView) {
            isDoubleTapTheme = true
            doubleTapIndex = selectedIndexPath.row
        }
        
        if isPremium {
            doubleTapTheme = themeName[doubleTapIndex].replacingOccurrences(of: " ", with: "%20").lowercased()
            performSegue(withIdentifier: "showPlaylist", sender: self)
        } else {
            if doubleTapIndex == 0 {
                doubleTapTheme = themeName[doubleTapIndex].replacingOccurrences(of: " ", with: "%20").lowercased()
                performSegue(withIdentifier: "showPlaylist", sender: self)
            } else {
                performSegue(withIdentifier: "showSubcribe", sender: self)
            }
        }
    }
    
    @objc func didDoubleTapSleepButton() {
        if isPremium {
            isDoubleTapTheme = true
            doubleTapTheme = "sleep"
            performSegue(withIdentifier: "showPlaylist", sender: self)
        } else {
            performSegue(withIdentifier: "showSubcribe", sender: self)
        }
    }
    
    @objc func didDouleTapMood() {
        mood = ""
        
        if !theme.isEmpty {
            self.id_list = ""
            self.musicIndex = 0
            self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme.replacingOccurrences(of: " ", with: "%20").lowercased(), id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
        }
        
        self.moodTableView.reloadData()
        self.viewWillLayoutSubviews()
    }
    
    private func getSegueSync() {
        if Constants.syncBool == true {
            syncEmotionSwitchBar.isOn = true
        } else {
            syncEmotionSwitchBar.isOn = false
        }
    }
    
    private func showBuffering() {
        playView.isHidden = true
        playButton.isHidden = true
        bufferingIndicatorView.isHidden = false
    }
    
    private func hideBuffering() {
        playView.isHidden = false
        playButton.isHidden = false
        bufferingIndicatorView.isHidden = true
    }
    
    private func getMp3TrackTime() {
        if myPlayer.player != nil {
            myPlayer.player.addProgressObserver { _ in
                if let currentItem = self.myPlayer.playerItem {
                    let currentTime: Int = Int(currentItem.currentTime().seconds)
                    self.trackElapsedTime = 0
                    self.trackElapsedTime = currentTime
                }
            }
        }
    }
    
    private func setUpDebugLabels() {
        self.moodLabel.text      =  "Mood Selected: \(mood)"
        self.themeLabel.text     =  "Theme Selected: \(theme)"
        self.emotionLabel.text   =  "Emotion Gathered: \(emotion)"
        self.stresslvlLabel.text =  "StressLvl Gathered: \(stressLevel)"
        self.songsCount.text     =  "Total songs: \(musicTitle.count)"
        self.currentSongCount.text  =  "Current songs count: \(musicIndex+1)"
    }
    
    private func displayCurrentSongInfo(playedSongFromPlayerView: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.displayMusicDetails()
            if playedSongFromPlayerView == false {
                self?.showPopUpMiniPlayer()
                self?.setUpDebugLabels()
            }
            
            self?.updateNowPlayingInfo()
            self?.obsListener()
            self?.getMp3TrackTime()
        }
    }
    
    func getAndPlayMp3(withIndex: Int, playedSongFromPlayerView: Bool) {
        displayCurrentSongInfo(playedSongFromPlayerView: playedSongFromPlayerView)
        myPlayer.playWithURL(withUrl: musicUrl, index: musicIndex)
        hasClickCommandCenter = false
    }
    
    func displayMusicDetails() {
        self.musicTitleLabel.text = musicTitle[musicIndex]
        self.musicArtistLabel.text = musicArtist[musicIndex]
        
        if let imageURL = URL(string: AWSService().getPreSignedURL(S3DownloadKeyName: musicAlbumCover[musicIndex])) {
            self.albumCoverURL = nil
            self.albumCoverURL = imageURL
            self.musicAlbumCoverImage.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "default"), options: [], completed: nil)
        }
    }

    func obsListener() {
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying(note:)), name: .AVPlayerItemDidPlayToEndTime, object: myPlayer.playerItem)
         
         NotificationCenter.default.post(name: .getCurrentSongInfo, object: nil, userInfo: ["music_index": self.musicIndex, "music_title": musicTitle[musicIndex], "music_artist": self.musicArtist[musicIndex], "music_album": self.musicAlbumCover[musicIndex], "music_duration": musicDuration[musicIndex]])
         
         myPlayer.player.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: nil)
     }
    
    private func setupAVAudioSession() {
        do {
            // swift 4 syntx, swift 4.2 syntax AVAudioSession.Category.playback
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            UIApplication.shared.beginReceivingRemoteControlEvents()
        } catch {
            debugPrint("Error: \(error)")
        }
    }
    
    func updateNowPlayingInfo() {
        
        let nowPlayingInfo: [String: Any] = [
            MPMediaItemPropertyArtist: musicArtist[musicIndex],
            MPMediaItemPropertyTitle: musicTitle[musicIndex],
            MPMediaItemPropertyArtwork : getArtwork(from: self.albumCoverURL),
            MPNowPlayingInfoPropertyElapsedPlaybackTime: myPlayer.playerItem.currentTime(),
            MPMediaItemPropertyPlaybackDuration: myPlayer.playerItem.asset.duration.seconds,
            MPNowPlayingInfoPropertyPlaybackRate: 1
        ]
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
    
    func setUpCommandCenter() {
        let commandCenter = MPRemoteCommandCenter.shared()
        commandCenter.playCommand.isEnabled = true
        commandCenter.pauseCommand.isEnabled = true
        commandCenter.nextTrackCommand.isEnabled = true
        commandCenter.previousTrackCommand.isEnabled = true
        
        commandCenter.playCommand.addTarget { (_) -> MPRemoteCommandHandlerStatus in
            self.myPlayer.player.play()
            return .success
        }
        
        commandCenter.pauseCommand.addTarget { (_) -> MPRemoteCommandHandlerStatus in
            self.myPlayer.player.pause()
            return .success
        }
        
        commandCenter.nextTrackCommand.addTarget { (_) -> MPRemoteCommandHandlerStatus in
            self.hasClickCommandCenter = true
            self.nextSong(self)
            return .success
        }
        
        commandCenter.previousTrackCommand.addTarget { (_) -> MPRemoteCommandHandlerStatus in
            self.hasClickCommandCenter = true
            self.previousSong(self)
            return .success
        }
    }
    
    func checkIfSyncOn() {
        if self.syncEmotionSwitchBar.isOn == false {
            self.emotion = ""
            self.stressLevel = ""
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension MoodMixDashboardViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoodCell", for: indexPath) as! MoodCell
        cell.selectionStyle = .none
        cell.moodText.text = moodName[indexPath.row]
    
        if let url = URL(string: moodDefaultImage[indexPath.row].replacingOccurrences(of: " ", with: "%20")) {
            cell.moodImage.sd_setImage(with: url, completed: nil)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moodName.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !bluetooManager.connected {
            self.emotion = ""
            self.stressLevel = ""
        }
        myPath = nil
        self.didSelectMood(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MoodCell
        if isPremium {
            if let url = URL(string: moodDefaultImage[indexPath.row].replacingOccurrences(of: " ", with: "%20")) {
                cell.moodImage.sd_setImage(with: url, completed: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    private func didSelectMood(tableView: UITableView, indexPath: IndexPath) {
        
        if theme == "sleep" {
            theme = ""
        }
        
        Constants.didUserSetTimer = false
        isDoubleTapTheme = false
        
        if isPremium {
        
            self.mood = moodName[indexPath.row].lowercased()
    
            self.id_list = ""
            self.musicIndex = 0
            self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme, id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
 
            sleepButton.setBackgroundImage(UIImage(named: "Sleep default1"), for: .normal)
            
            let cell = tableView.cellForRow(at: indexPath) as! MoodCell
            if let url = URL(string: moodSelectedImage[indexPath.row].replacingOccurrences(of: " ", with: "%20")) {
                cell.moodImage.sd_setImage(with: url, completed: nil)
            }
            
        } else {
            
            if indexPath.row == 0 {
                 self.mood = moodName[0].lowercased()

                 self.id_list = ""
                 self.musicIndex = 0
                 self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme, id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)

                 sleepButton.setBackgroundImage(UIImage(named: "Sleep default1"), for: .normal)
                
                 let cell = tableView.cellForRow(at: indexPath) as! MoodCell
                 if let url = URL(string: moodSelectedImage[0].replacingOccurrences(of: " ", with: "%20")) {
                     cell.moodImage.sd_setImage(with: url, completed: nil)
                 }
                
            } else {
                performSegue(withIdentifier: "showSubcribe", sender: self)
            }
        }
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate

extension MoodMixDashboardViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    private func highlightAndSelectTheme(collectionView: UICollectionView, indexPath: IndexPath) {
        // deselect the previous doubled tapped theme
        let index = IndexPath(item: doubleTapIndex, section: 0)
        let themeCell1 = themeCollectionView.cellForItem(at: index) as! ThemeCell
        themeCell1.themeCellBackgroundView.backgroundColor = hexStringToUIColor(hex: "#353866")
        
        // select and hightlighted theme
        if let themeCell = collectionView.cellForItem(at: indexPath) as? ThemeCell {
            themeCell.toggleSelected()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThemeCell", for: indexPath) as! ThemeCell
        cell.themeText.text = themeName[indexPath.row]
        
        if let url = URL(string: themeImg[indexPath.row].replacingOccurrences(of: " ", with: "%20")) {
            cell.themeImage.sd_setImage(with: url, completed: nil)
        }
        
        cell.themeCellBackgroundView.backgroundColor = cell.isSelected ? hexStringToUIColor(hex: "#8C9CAB") : hexStringToUIColor(hex: "#353866")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return themeName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Constants.didUserSetTimer = false
        myPath = nil
        
        if !bluetooManager.connected {
            self.emotion = ""
            self.stressLevel = ""
        }
        
        didSelectTheme(indexPath: indexPath)
        
        if isPremium {
            highlightAndSelectTheme(collectionView: collectionView, indexPath: indexPath)
        } else {
            if indexPath.row == 0 {
                highlightAndSelectTheme(collectionView: collectionView, indexPath: indexPath)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if isPremium {
            if let themeCell = collectionView.cellForItem(at: indexPath) as? ThemeCell {
                themeCell.toggleSelected()
            }
        }
    }
    
    private func didSelectTheme(indexPath: IndexPath) {
        
        if theme == "sleep" {
            theme = ""
        }
        
        if isPremium {
        
            self.selectedIndex = indexPath.row
            self.theme = themeName[indexPath.row].replacingOccurrences(of: " ", with: "%20").lowercased()
            self.sleepButton.setBackgroundImage(UIImage(named: "Sleep default1"), for: .normal)
                
            self.id_list = ""
            self.musicIndex = 0
            
            self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme.replacingOccurrences(of: " ", with: "%20").lowercased(), id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
        } else {
            
            if indexPath.row == 0 {
                self.selectedIndex = 0
                self.theme = themeName[0].replacingOccurrences(of: " ", with: "%20").lowercased()
                self.sleepButton.setBackgroundImage(UIImage(named: "Sleep default1"), for: .normal)
                               
                self.id_list = ""
                self.musicIndex = 0
                
                self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme.replacingOccurrences(of: " ", with: "%20").lowercased(), id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
            } else {
                performSegue(withIdentifier: "showSubcribe", sender: self)
            }
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
//        let item = collectionView.cellForItem(at: indexPath)
//        if item?.isSelected ?? false {
//            collectionView.deselectItem(at: indexPath, animated: true)
//
//            let themeCell = collectionView.cellForItem(at: indexPath) as! ThemeCell
//            themeCell.themeCellBackgroundView.backgroundColor = hexStringToUIColor(hex: "#353866")
//            theme = ""
//
//            self.id_list = ""
//            self.musicIndex = 0
//
//            self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme.replacingOccurrences(of: " ", with: "%20").lowercased(), id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
//
//        } else {
//            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
//            return true
//        }
//
//        return false
//    }
}

// MARK: - Observer Helper Methods
extension MoodMixDashboardViewController {
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "timeControlStatus", let change = change, let newValue = change[NSKeyValueChangeKey.newKey] as? Int, let oldValue = change[NSKeyValueChangeKey.oldKey] as? Int {
            let oldStatus = AVPlayer.TimeControlStatus(rawValue: oldValue)
            let newStatus = AVPlayer.TimeControlStatus(rawValue: newValue)
            if newStatus != oldStatus {
                
                DispatchQueue.main.async { [weak self] in
                    if newStatus == .playing || newStatus == .paused {
                        self?.hideBuffering()
                        
                        if newStatus == .playing {
                            self?.playIcon.image = UIImage(named: "pause")
                        } else {
                            self?.playIcon.image = UIImage(named: "play")
                        }
                        
                    } else {
                        self?.showBuffering()
                    }
                }
                
            }
        }
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        
        if !bluetooManager.connected {
            self.emotion = ""
            self.stressLevel = ""
        }

        myPath = nil
        
        if musicIndex == musicUrl.count - 1 {
            print("finish song with pagination")
            // next song with pagination
            // note: commend this is there's something wrong happen with this method
//            musicIndex = musicUrl.count - 1 + 1
//            self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme, id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
            musicIndex = 0
            getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
        } else {
            // next the song normally
            if emotion.isEmpty && stressLevel.isEmpty {
                print("finish song normally")
                
                if Constants.User.isRepeatSong == false {
                    musicIndex = musicIndex + 1
                }

                getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
            } else {
                print("finish song request new playlist")
                // next requested song because changes of emotion/stress level
                self.id_list = ""
                self.musicIndex = 0
                
                self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme, id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
            }
        }
    }
    
    @objc func getEmotion(_ notification: Notification) {
        if syncEmotionSwitchBar.isOn == true {
            if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
                let emotion = dict["emotion"] as? String ?? ""
                self.emotion = emotion
                
                // setCommands for speaker
                if Constants.syncBool == true {
                    setCommands(withEmotion: emotion)
                }
            }
        } else {
            self.emotion = ""
        }
    }
    
    @objc func getStressLevel(_ notification: Notification) {
        if syncEmotionSwitchBar.isOn == true {
            if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
                let stressLevel = dict["stress_level"] as? String ?? ""
                self.stressLevel = stressLevel
            }
        } else {
            self.emotion = ""
        }
    }
    
    @objc func didDisconnectBand(_ notification: Notification) {
        self.emotion = ""
        self.stressLevel = ""
        self.syncEmotionSwitchBar.isOn = false
        Constants.syncBool = false
    }
}

// MARK: - Commands For Speaker

extension MoodMixDashboardViewController {
    func writeCommand(withHexUInt: [UInt8], manager: BluetoothManagerSpeaker) {
        if writeChannel != nil {
            print("write command here")
            manager.writeValue(withCommand: withHexUInt, forCharacteristic: writeChannel, type: .withResponse)
        } else {
            print("No channel found.")
        }
    }
    
    func setCommands(withEmotion: String) {
        switch withEmotion {
        case "Zen":
            writeCommand(withHexUInt: [0x05, 0x03, 0xFF, 0xFF, 0xFF], manager: speakerManager) // pure white
        case "Excitement":
            writeCommand(withHexUInt: [0x05, 0x03, 0x33, 0x99, 0xFF], manager: speakerManager) // blue
        case "Happy":
            writeCommand(withHexUInt: [0x05, 0x03, 0x66, 0xFF, 0xB2], manager: speakerManager)
        case "Pleasant":
            writeCommand(withHexUInt: [0x05, 0x03, 0x80, 0xFF, 0x00], manager: speakerManager) // green
        case "Calm":
            writeCommand(withHexUInt: [0x05, 0x03, 0xFF, 0x80, 0x00], manager: speakerManager) // yellow
        case "Unpleasant":
            writeCommand(withHexUInt: [0x05, 0x03, 0xFF, 0x45, 0x00], manager: speakerManager) // light blue
        case "Tense":
            writeCommand(withHexUInt: [0x05, 0x03, 0xFF, 0x11, 0x11], manager: speakerManager) // white
        case "Confused":
            writeCommand(withHexUInt: [0x05, 0x03, 0xFF, 0x00, 0x7F], manager: speakerManager) // light blue
        case "Challenged":
            writeCommand(withHexUInt: [0x05, 0x03, 0x7F, 0x00, 0xFF], manager: speakerManager) // brown
        case "Sad":
            writeCommand(withHexUInt: [0x05, 0x03, 0xCC, 0x00, 0x00], manager: speakerManager) // red
        case "Anxious":
            writeCommand(withHexUInt: [0x05, 0x03, 0x66, 0x00, 0x00], manager: speakerManager) // brown
        default:
            break
        }
    }
}

// MARK: - PlaylistDelegate

extension MoodMixDashboardViewController: PlaylistDelegate {
    func didSetCurrentSongs(withIndex: Int, musicUrl: [String], musicTitle: [String], musicArtist: [String], musicAlbumCover: [String], musicDuration: [Int], didUserSetTimer: Bool, userTimeSet: Int) {
        self.removeAllMusicArr()
        self.musicIndex = withIndex
        self.musicUrl = musicUrl
        self.musicTitle = musicTitle
        self.musicArtist = musicArtist
        self.musicAlbumCover = musicAlbumCover
        self.musicDuration = musicDuration
        
        self.getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: false)
    }

    func didSetCurrentTheme(withTheme: String) {
        theme = withTheme
        self.hideBuffering()
        self.playIcon.image = UIImage(named: "pause")
        sleepButton.setBackgroundImage(UIImage(named: "Sleep default1"), for: .normal)
        
        if withTheme == "sleep" {
            reloadList()
            sleepButton.setBackgroundImage(UIImage(named: "sleep select"), for: .normal)
        } else {
        
            self.themeCollectionView.reloadData()
            self.themeCollectionView.layoutIfNeeded()
            self.themeCollectionViewHeight.constant = self.themeCollectionView.contentSize.height
            
            let index = IndexPath(item: doubleTapIndex, section: 0)
            let themeCell = themeCollectionView.cellForItem(at: index) as! ThemeCell
            themeCell.themeCellBackgroundView.backgroundColor = hexStringToUIColor(hex: "#8C9CAB")
        }
    }
    
    func didSetTimerOnPlayer(withTime: Int) {
        if withTime != 0 {
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(withTime), target: self, selector: #selector(runTimerAndStopPlayer), userInfo: nil, repeats: true)
        }
    }
    
    @objc func runTimerAndStopPlayer() {
        myPlayer.resetPlayer()
        removeAllMusicArr()
        id_list = ""
        musicIndex = 0
        tabBarController?.dismissPopupBar(animated: true)
        
        mood = ""
        theme = ""
        stressLevel = ""
        emotion = ""
        
        musicTitleLabel.text = ""
        musicArtistLabel.text = ""
        musicAlbumCoverImage.image = UIImage(named: "default")
        sleepButton.setBackgroundImage(UIImage(named: "Sleep default1"), for: .normal)
        Constants.playerSetTime = ""
        
        getMood()
        getTheme()
        timer.invalidate()
    }
}


// MARK: - PlayerDelegate

extension MoodMixDashboardViewController: PlayerDelegate {
    func didClosePlayerView() {
        self.tabBarController?.closePopup(animated: true)
    }
    
    func didNext(withNextPage: Bool) {
        Constants.User.isRepeatSong = false
        if musicIndex == musicUrl.count - 1 {
            // next song with pagination
//            musicIndex = musicUrl.count - 1 + 1
//            self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme, id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
            musicIndex = 0
            getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: true)
        } else {
            // next the song normally
            if emotion.isEmpty && stressLevel.isEmpty {
                
                if Constants.User.isRepeatSong == false {
                    musicIndex = musicIndex + 1
                }
                
                getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: true)
                
            } else {
                // next requested song because changes of emotion/stress level
                if Constants.currentSongIsPlayingOnPlayerView == true {
                    self.tabBarController?.closePopup(animated: true)
                }
                
                self.getPlayList(emotion: self.emotion, stressLevel: self.stressLevel, mood: self.mood, theme: self.theme, id_list: self.id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
            }
        }
    }
    
    func didPrevious(withIndex: Int) {
        
        if trackElapsedTime < 3 {
            
            if syncEmotionSwitchBar.isOn {
                getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: false)
            } else {
                musicIndex = musicIndex - 1
                if musicIndex <= -1 {
                    musicIndex = 0
                }
                
                getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: false)
            }

        } else {
            getAndPlayMp3(withIndex: musicIndex, playedSongFromPlayerView: false)
        }
    }
}

class MoodCell: UITableViewCell {
    @IBOutlet weak var moodImage: UIImageView!
    @IBOutlet weak var moodText: UILabel!
}

class ThemeCell: UICollectionViewCell {
    @IBOutlet weak var themeImage: UIImageView!
    @IBOutlet weak var themeText: UILabel!
    @IBOutlet weak var themeCellBackgroundView: UIView!
    
    func toggleSelected() {
        themeCellBackgroundView.backgroundColor = hexStringToUIColor(hex: "#353866")
        if isSelected {
            themeCellBackgroundView.backgroundColor = hexStringToUIColor(hex: "#8C9CAB")
        }else {
            themeCellBackgroundView.backgroundColor = hexStringToUIColor(hex: "#353866")
        }
    }
}

// Marquee
var label: MarqueeLabel = {
    let marqueeLabel = MarqueeLabel(frame: .zero, rate: 15, fadeLength: 10)
    marqueeLabel.leadingBuffer = 0.0
    marqueeLabel.trailingBuffer = 5.0
    marqueeLabel.animationDelay = 1.0
    marqueeLabel.type = .continuous
    return marqueeLabel
}()

var sublabel: MarqueeLabel = {
    let marqueeLabel = MarqueeLabel(frame: .zero, rate: 15, fadeLength: 10)
    marqueeLabel.leadingBuffer = 0.0
    marqueeLabel.trailingBuffer = 5.0
    marqueeLabel.animationDelay = 1.0
    marqueeLabel.type = .continuous
    return marqueeLabel
}()

extension Notification.Name {
    static let currentEmotion = Notification.Name("currentEmotion")
    static let currentStressLevel = Notification.Name("currentStressLevel")
    static let didCloseMiniPlayer = Notification.Name("didCloseMiniPlayer")
}

extension UIButton {
    func preventRepeatedPresses(inNext seconds: Double = 1) {
        self.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            self.isUserInteractionEnabled = true
        }
    }
}
