//
//  File.swift
//  moodmix
//
//  Created by John Paul Manoza on 13/08/2020.
//  Copyright © 2020 Joseph MIkko Manoza. All rights reserved.
//

import UIKit
import Foundation
import AWSS3

let S3BucketName = "upmoods3"
class AWSService {
    var preSignedURLString = ""
    
    func getPreSignedURL( S3DownloadKeyName: String)->String{
        let getPreSignedURLRequest = AWSS3GetPreSignedURLRequest()
        getPreSignedURLRequest.httpMethod = AWSHTTPMethod.GET
        getPreSignedURLRequest.key = S3DownloadKeyName
        getPreSignedURLRequest.bucket = S3BucketName
        getPreSignedURLRequest.expires = Date(timeIntervalSinceNow: 3600)
        
        AWSS3PreSignedURLBuilder.default().getPreSignedURL(getPreSignedURLRequest).continueWith { (task:AWSTask<NSURL>) -> Any? in
            
            if let error = task.error as NSError? {
                print("Error: \(error)")
                return nil
            }
            
            self.preSignedURLString = (task.result?.absoluteString)!
            return nil
        }
        
        return self.preSignedURLString
    }
}
