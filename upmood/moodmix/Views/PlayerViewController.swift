//
//  PlayerViewController.swift
//  moodmix
//
//  Created by John Paul Manoza on 12/08/2020.
//  Copyright © 2020 Joseph MIkko Manoza. All rights reserved.
//

import AVFoundation
import MediaPlayer
import SDWebImage
import PBPopupController
import UIKit

protocol PlayerDelegate {
    func didNext(withNextPage: Bool)
    func didPrevious(withIndex: Int)
    func didClosePlayerView()
}

class PlayerViewController: UIViewController {
    
    @IBOutlet weak var musicTitleLabel: UILabel!
    @IBOutlet weak var musicArtistLabel: UILabel!
    @IBOutlet weak var elapsedTimeLabel: UILabel!
    @IBOutlet weak var remainingTimeLabel: UILabel!
    @IBOutlet weak var musicTrackSeekBar: UISlider!
    @IBOutlet weak var playIcon: UIImageView!
    @IBOutlet weak var musicAlbumCoverImage: UIImageView!
    @IBOutlet weak var repeatButton: UIButton!
    @IBOutlet weak var playView: UIView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var bufferingIndicatorView: UIActivityIndicatorView!
    
    // stored values
    var musicUrl = [String]()
    var musicTitle = [String]()
    var musicArtist = [String]()
    var musicDuration = [Int]()
    var albumCoverUrl = [String]()
    
    // params
    var musicIndex = 0
    var emotion = ""
    var stressLevel = ""

    var isRepeatSelected = true
    var trackElapsedTime = 0
    
    // delegate
    var playerDelegate: PlayerDelegate!

    var myPlayer = Player.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        musicTrackSeekBar.setThumbImage(UIImage(named: "thumb"), for: .normal)
        bufferingIndicatorView.isHidden = true
        
        NotificationCenter.default.removeObserver(self, name: .getCurrentSongInfo, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getCurrentPlayerInfo(_:)), name: .getCurrentSongInfo, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: .didFinishCurrentSongPlaying, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didFinishCurrentSongPlaying(_:)), name: .didFinishCurrentSongPlaying, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if myPlayer.player != nil {
            checkIfPlayerIsPlaying()
            setUpRepeatButton()
            
            Constants.currentSongIsPlayingOnPlayerView = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Constants.currentSongIsPlayingOnPlayerView = false
    }
    
    @IBAction func close(_ sender: UIButton) {
        playerDelegate.didClosePlayerView()
    }
    
    @IBAction func nextSong(_ sender: UIButton) {
        myPath = nil
        playerDelegate.didNext(withNextPage: false)
        setUpRepeatButton()
        playIcon.image = UIImage(named: "pause")
    }
    
    @IBAction func playSong(_ sender: UIButton) {
        if myPlayer.player.rate == 1 {
            myPlayer.player.pause()
            playIcon.image = UIImage(named: "play")
        } else {
            myPlayer.player.play()
            playIcon.image = UIImage(named: "pause")
        }
    }
    
    @IBAction func previousSong(_ sender: UIButton) {
        myPath = nil
        
        if trackElapsedTime < 3 {
            if Constants.syncBool == true {
                presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: "Please turn off sync emotion to play previous song.")
            }
        }
        
        playerDelegate.didPrevious(withIndex: musicIndex)
        Constants.User.isRepeatSong = false
        setUpRepeatButton()
        playIcon.image = UIImage(named: "pause")
    }
    
    @IBAction func repeatSong(_ sender: UIButton) {
        if Constants.syncBool == true {
            presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: "Please turn off sync emotion to repeat current song.")
        } else {
            if repeatButton.isSelected {
                let origImage = UIImage(named: "repeat")
                let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                sender.setImage(tintedImage, for: .normal)
                sender.tintColor = .lightGray
                Constants.User.isRepeatSong = false
            } else {
                 let origImage = UIImage(named: "repeat")
                 let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                 sender.setImage(tintedImage, for: .normal)
                 sender.tintColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                 Constants.User.isRepeatSong = true
            }
            repeatButton.isSelected = !repeatButton.isSelected
        }
    }
    
    @IBAction func seek(_ sender: UISlider) {
        if myPlayer.player != nil {
            if let duration = myPlayer.player.currentItem?.duration {
                let totalsecs = CMTimeGetSeconds(duration)
                let val = Float64(sender.value) * totalsecs
                let seekTime = CMTime(value: Int64(val), timescale: 1)
                myPlayer.player.seek(to: seekTime, completionHandler: { (completedSeek) in
                    DispatchQueue.main.async {
                        let seconds = Int(CMTimeGetSeconds(seekTime))
                        self.elapsedTimeLabel.text = "\(self.secondsToMinsAndSec(seconds: seconds))"
                    }
                })
            }
        }
    }
}

// MARK: - SetUp UIs

extension PlayerViewController {
    
    private func preloadUI() {
        self.musicTitleLabel.text = musicTitle[musicIndex]
        self.musicArtistLabel.text = musicArtist[musicIndex]
        self.remainingTimeLabel.text = self.secondsToMinsAndSec(seconds: musicDuration[musicIndex])
        
        if let imageURL = URL(string: AWSService().getPreSignedURL(S3DownloadKeyName: albumCoverUrl[musicIndex])) {
            self.musicAlbumCoverImage.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "default"), options: [], completed: nil)
        }
        
        self.getMp3TrackTime()
    }
    
    private func showBuffering() {
        playView.isHidden = true
        playButton.isHidden = true
        bufferingIndicatorView.isHidden = false
    }
    
    private func hideBuffering() {
        playView.isHidden = false
        playButton.isHidden = false
        bufferingIndicatorView.isHidden = true
    }
    
    private func observeBuffering() {
        // observe Buffering
        myPlayer.player.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: nil)
    }
    
    private func setUpRepeatButton() {
        if Constants.User.isRepeatSong == true {
            let origImage = UIImage(named: "repeat")
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            repeatButton.setImage(tintedImage, for: .normal)
            repeatButton.tintColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            repeatButton.isSelected = true
        } else {
            let origImage = UIImage(named: "repeat")
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            repeatButton.setImage(tintedImage, for: .normal)
            repeatButton.tintColor = .lightGray
            repeatButton.isSelected = false
        }
    }
    
    private func getMp3TrackTime() {
        if myPlayer.player != nil {
            myPlayer.player.addProgressObserver { progress in
                self.musicTrackSeekBar.value = Float(progress)
                if let currentItem = self.myPlayer.playerItem {
                    let currentTime: Int = Int(currentItem.currentTime().seconds)
                    self.elapsedTimeLabel.text = self.secondsToMinsAndSec(seconds: currentTime)
                    self.trackElapsedTime = currentTime
                }
            }
        }
    }
    
    func secondsToMinsAndSec(seconds: Int) -> String {
        let mins = (seconds % 3600) / 60
        let secs = String(format: "%02d", (seconds % 3600) % 60)
        return "\(mins):\(secs)"
    }
    
    private func checkIfPlayerIsPlaying() {
        if myPlayer.player != nil {
            if myPlayer.player.rate == 1 {
                playIcon.image = UIImage(named: "pause")
            } else {
                playIcon.image = UIImage(named: "play")
            }
        }
    }
}

extension AVPlayer {
    func addProgressObserver(action:@escaping ((Double) -> Void)) -> Any {
        return self.addPeriodicTimeObserver(forInterval: CMTime.init(value: 1, timescale: 1), queue: .main, using: { time in
            if let duration = self.currentItem?.duration {
                let duration = CMTimeGetSeconds(duration), time = CMTimeGetSeconds(time)
                let progress = (time/duration)
                action(progress)
            }
        })
    }
}

// MARK: - observer

extension PlayerViewController {
    @objc func didFinishCurrentSongPlaying(_ notification: Notification) {

        if musicIndex == musicUrl.count - 1 {
            playerDelegate.didNext(withNextPage: true)
        } else {
            // next the song normally
            if emotion.isEmpty && stressLevel.isEmpty {
                playerDelegate.didNext(withNextPage: false)
            } else {
                // next requested song because changes of emotion/stress level
            }
        }
        
        setUpRepeatButton()
    }
    
    @objc func getCurrentPlayerInfo(_ notification: Notification) {
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            let _ = dict["music_index"] as? Int ?? 0
            let music_title = dict["music_title"] as? String ?? ""
            let music_artist = dict["music_artist"] as? String ?? ""
            let music_album = dict["music_album"] as? String ?? ""
            let music_duration = dict["music_duration"] as? Int ?? 0
            
            self.musicTitleLabel.text = music_title
            self.musicArtistLabel.text = music_artist
            self.remainingTimeLabel.text = self.secondsToMinsAndSec(seconds: music_duration)
            
            if let imageURL = URL(string: AWSService().getPreSignedURL(S3DownloadKeyName: music_album)) {
                self.musicAlbumCoverImage.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "default"), options: [], completed: nil)
            }
            
            self.getMp3TrackTime()
        }
    }
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "timeControlStatus", let change = change, let newValue = change[NSKeyValueChangeKey.newKey] as? Int, let oldValue = change[NSKeyValueChangeKey.oldKey] as? Int {
            let oldStatus = AVPlayer.TimeControlStatus(rawValue: oldValue)
            let newStatus = AVPlayer.TimeControlStatus(rawValue: newValue)
            if newStatus != oldStatus {
                
                DispatchQueue.main.async { [weak self] in
                    if newStatus == .playing || newStatus == .paused {
                        self?.hideBuffering()
                        
                        if newStatus == .playing {
                            self?.playIcon.image = UIImage(named: "pause")
                        } else {
                            self?.playIcon.image = UIImage(named: "play")
                        }
                        
                    } else {
                        self?.showBuffering()
                    }
                }
                
            }
        }
    }
}

extension Notification.Name {
    static let didFinishCurrentSongPlaying = Notification.Name("didFinishCurrentSongPlaying")
    static let getCurrentSongInfo = Notification.Name("getCurrentSongInfo")
}
