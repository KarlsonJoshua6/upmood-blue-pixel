//
//  PlaylistViewController.swift
//  moodmix
//
//  Created by John Paul Manoza on 18/08/2020.
//  Copyright © 2020 Joseph MIkko Manoza. All rights reserved.
//

import AVFoundation
import KeychainSwift
import MediaPlayer
import SDWebImage
import PBPopupController
import UIKit

var myPath: IndexPath!

protocol PlaylistDelegate {
    func didSetTimerOnPlayer(withTime: Int)
    func didSetCurrentTheme(withTheme: String)
    func didSetCurrentSongs(withIndex: Int, musicUrl: [String], musicTitle: [String], musicArtist: [String], musicAlbumCover: [String], musicDuration: [Int], didUserSetTimer: Bool, userTimeSet: Int)
}

class PlaylistViewController: UIViewController {

    @IBOutlet weak var themeTtitleLabel: UILabel!
    @IBOutlet weak var themeDescriptionLabel: UILabel!
    @IBOutlet weak var themeImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sleepTimerButton: UIButton!
    @IBOutlet weak var playIcon: UIImageView!
    
    var musicUrl = [String]()
    var musicTitle = [String]()
    var musicArtist = [String]()
    var musicAlbumCover = [String]()
    var musicDuration = [Int]()
    var isSelected = [Bool]()

    var musicIndex = 0
    var id_list = ""
    
    var mood = ""
    var theme = ""
    var stressLevel = ""
    var emotion = ""
    var playlistDeescription = ""
    var playlistImageUrlString = ""
    let minutes = ["Off", "5 mins","10 mins","15 mins","20 mins","30 mins", "40 mins", "50 mins", "60 mins"]
    var selectedMinutes = ""
    var pickerView: UIPickerView?
    var sleepTimer: Timer?
    var setTimerSecond = 0
    var didUserSetTimer = false
    var userSetTime = 0
    
    // when double tap values
    var doubleTapTheme = ""
    var isDoubleTapTheme = true
    
    // delegate
    var playlistDelegate: PlaylistDelegate!
    
    var myPlayer = Player.sharedInstance
    
    var statusTapped = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.remembersLastFocusedIndexPath = true
        
        NotificationCenter.default.removeObserver(self, name: .didCloseMiniPlayer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didCloseMiniPlayer(_:)), name: .didCloseMiniPlayer, object: nil)
        
        displayUI()
        loadPlaylist()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isTranslucent = false
        tabBarController?.tabBar.barTintColor = hexStringToUIColor(hex: Constants.Color.MOOD_MIX_PRIMARY_COLOR)
        playPause()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isTranslucent = false
        tabBarController?.tabBar.barTintColor = .white
    }
    
    @IBAction func setSleepTimer(_ sender: UIButton) {
        let alert = UIAlertController(title: "Song stops in", message: "\n\n\n\n", preferredStyle: .alert)
        alert.isModalInPopover = true
        
        pickerView = UIPickerView(frame: CGRect(x: 5, y: 20, width: 250, height: 110))
        
        alert.view.addSubview(pickerView!)
        pickerView?.dataSource = self
        pickerView?.delegate = self
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            self.didUserSetTimer = false
        }))
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
            self.didUserSetTimer = true
            self.sleepTimer?.invalidate()
            
            if self.selectedMinutes != "Off" {
                self.sleepTimerButton.setTitle("Audio will end at \(self.selectedMinutes) after sleep playlist start", for: .normal)
            } else {
                self.sleepTimerButton.setTitle("Set sleep time", for: .normal)
            }
            
            self.setUpTimer(withMinutes: self.selectedMinutes)
            
            if Constants.didStartSleepMood == true {
                self.playlistDelegate.didSetTimerOnPlayer(withTime: self.userSetTime)
            }
            
        }))
        
        self.present(alert,animated: true, completion: nil)
    }
    
    @IBAction func playPlaylist(_ sender: UIButton) {
    
        if doubleTapTheme.replacingOccurrences(of: " ", with: "%20") == theme.replacingOccurrences(of: " ", with: "%20") {
            if myPlayer.player != nil {
                if myPlayer.player.rate == 1 {
                    myPlayer.player.pause()
                    playIcon.image = UIImage(named: "play")
                    let playButtonItem = UIBarButtonItem(image: UIImage(named: "play"), style: .plain, target: self, action: #selector(playPauseAction))
                    self.tabBarController?.popupBar.rightBarButtonItems = [playButtonItem]
                } else {
                    myPlayer.player.play()
                    playIcon.image = UIImage(named: "pause")
                    let pauseButtonItem = UIBarButtonItem(image: UIImage(named: "pause"), style: .plain, target: self, action: #selector(playPauseAction))
                    self.tabBarController?.popupBar.rightBarButtonItems = [pauseButtonItem]
                }
            }
        } else {
            
            if isDoubleTapTheme {
                playlistDelegate.didSetCurrentTheme(withTheme: doubleTapTheme.lowercased().replacingOccurrences(of: "%20", with: " "))
            } else {
                playlistDelegate.didSetCurrentTheme(withTheme: theme.lowercased().replacingOccurrences(of: "%20", with: " "))
            }
              
            playlistDelegate.didSetCurrentSongs(withIndex: 0, musicUrl: musicUrl, musicTitle: musicTitle, musicArtist: musicArtist, musicAlbumCover: musicAlbumCover, musicDuration: musicDuration, didUserSetTimer: didUserSetTimer, userTimeSet: userSetTime)
            
            playIcon.image = UIImage(named: "pause")
            myPath = IndexPath(row: 0, section: 0)
            theme = doubleTapTheme.replacingOccurrences(of: " ", with: "%20").lowercased()
            tableView.reloadData()
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func displayUI() {
        if doubleTapTheme == "sleep" {
            sleepTimerButton.isHidden = false
            
            if Constants.playerSetTime.isEmpty {
                sleepTimerButton.setTitle("Set sleep time", for: .normal)
            } else {
                sleepTimerButton.setTitle("Audio will end at \(Constants.playerSetTime) after sleep playlist start", for: .normal)
            }
            
        } else {
            
            sleepTimerButton.isHidden = true
            
            if isDoubleTapTheme {
                themeTtitleLabel.text = doubleTapTheme.capitalized.replacingOccurrences(of: "%20", with: " ")
            } else {
                themeTtitleLabel.text = theme.capitalized.replacingOccurrences(of: "%20", with: " ")
            }
            
            themeDescriptionLabel.text = playlistDeescription
            if let imageURL = URL(string: playlistImageUrlString) {
                themeImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "yoga"), options: .continueInBackground, completed: nil)
            }
        }
    }
    
    private func setUpTimer(withMinutes: String) {
        Constants.playerSetTime = withMinutes
        switch withMinutes {
        case "5 mins":
            userSetTime = 300
        case "10 mins":
            userSetTime = 600
        case "15 mins":
            userSetTime = 900
        case "20 mins":
            userSetTime = 1200
        case "30 mins":
            userSetTime = 1800
        case "40 mins":
            userSetTime = 2400
        case "50 mins":
            userSetTime = 3000
        case "60 mins":
            userSetTime = 3600
        default:
            Constants.playerSetTime = "Off"
        }
    }
    
    @objc func runTimedCode() {
        sleepTimer?.invalidate()
        myPlayer.resetPlayer()
    }
    
    @objc func didCloseMiniPlayer(_ notification: Notification) {
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            let index = dict["music_index"] as? Int ?? 0
            musicIndex = index
            tableView.reloadData()
        }
    }
}

extension PlaylistViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaylistTableViewCell", for: indexPath) as! PlaylistTableViewCell
        cell.musicTitleLabel.text = musicTitle[indexPath.row]
        cell.musicArtistLabel.text = musicArtist[indexPath.row]
        
        if let imageURL = URL(string: AWSService().getPreSignedURL(S3DownloadKeyName: musicAlbumCover[indexPath.row])) {
            cell.coverAlbumImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "default"), options: [], completed: nil)
        }
        
        cell.contentView.backgroundColor = cell.isSelected ? hexStringToUIColor(hex: "#8C9CAB") : hexStringToUIColor(hex: "#292B4D")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return musicTitle.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myPath = indexPath
        musicIndex = indexPath.row
        playIcon.image = UIImage(named: "pause")
        
        if isDoubleTapTheme {
            playlistDelegate.didSetCurrentTheme(withTheme: doubleTapTheme.lowercased().replacingOccurrences(of: "%20", with: " "))
            tableView.reloadData()
        } else {
            playlistDelegate.didSetCurrentTheme(withTheme: theme.lowercased().replacingOccurrences(of: "%20", with: " "))
        }
        
        playlistDelegate.didSetCurrentSongs(withIndex: musicIndex, musicUrl: musicUrl, musicTitle: musicTitle, musicArtist: musicArtist, musicAlbumCover: musicAlbumCover, musicDuration: musicDuration, didUserSetTimer: didUserSetTimer, userTimeSet: userSetTime)
        
        if self.didUserSetTimer == true {
            self.playlistDelegate.didSetTimerOnPlayer(withTime: self.userSetTime)
        }
    
        if let cell = tableView.cellForRow(at: indexPath) as? PlaylistTableViewCell {
            cell.contentView.backgroundColor = hexStringToUIColor(hex: "#8C9CAB")
        }
        
        theme = doubleTapTheme
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if doubleTapTheme.lowercased().replacingOccurrences(of: " ", with: "%20") == theme.lowercased().replacingOccurrences(of: " ", with: "%20") {
            if myPath != nil {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    if let cell = tableView.cellForRow(at: myPath) as? PlaylistTableViewCell {
                        cell.contentView.backgroundColor = hexStringToUIColor(hex: "#8C9CAB")
                    }
                }
            } else {
                let path = IndexPath(row: self.musicIndex, section: 0)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    if let cell = self.tableView.cellForRow(at: path) as? PlaylistTableViewCell {
                        cell.contentView.backgroundColor = hexStringToUIColor(hex: "#8C9CAB")
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? PlaylistTableViewCell {
            cell.contentView.backgroundColor = hexStringToUIColor(hex: "#292B4D")
        }
    }
}

extension PlaylistViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return minutes[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return minutes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedMinutes = minutes[row]
    }
}

extension PlaylistViewController {
    
    private func checkIfPlayerIsPlaying() {
        if myPlayer.player != nil {
            if myPlayer.player.rate == 1 {
                playIcon.image = UIImage(named: "pause")
            } else {
                playIcon.image = UIImage(named: "play")
            }
        }
    }
    
    private func setUpPlayButton() {
        if doubleTapTheme == theme.lowercased().replacingOccurrences(of: " ", with: "%20") {
            checkIfPlayerIsPlaying()
        } else {
            playIcon.image = UIImage(named: "play")
        }
    }
    
    private func loadPlaylist() {
    
        if doubleTapTheme != theme.lowercased().replacingOccurrences(of: " ", with: "%20") {
            getPlayList(emotion: emotion, stressLevel: stressLevel, mood: mood, theme: doubleTapTheme, id_list: id_list, playedSongFromPlayerView: Constants.currentSongIsPlayingOnPlayerView)
        }

        setUpPlayButton()
    }
    
    private func removeAllMusicArr() {
        self.musicTitle.removeAll()
        self.musicArtist.removeAll()
        self.musicUrl.removeAll()
        self.musicDuration.removeAll()
        self.musicAlbumCover.removeAll()
    }
    
    private func getPlayList(emotion: String, stressLevel: String, mood: String, theme: String, id_list: String, playedSongFromPlayerView: Bool) {
        self.removeAllMusicArr()
        var url: URL!
        if id_list.isEmpty {
            url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/moodmix/playlist?emotion=\(emotion)&stress_level=\(stressLevel)&mood=\(mood)&theme=\(theme)&limit=1000&id_list=")
        } else {
            url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/moodmix/playlist?emotion=\(emotion)&stress_level=\(stressLevel)&mood=\(mood)&theme=\(theme)&limit=1000&id_list=\(id_list)")
        }
        
        print("url: \(url)")
        
        var request = URLRequest(url: url)
        let userToken = KeychainSwift().get("apiToken") ?? ""
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(userToken)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
            guard let jsonDic = userObject as? [String: Any] else { return }
            let statusCode = jsonDic["status"] as? Int ?? 0
            let messsage = jsonDic["status"] as? Int ?? 0
    
            guard let data = jsonDic["data"] as? [String: Any] else { return }
            guard let dictionary = data["playlist"] as? [[String: Any]] else { return }
    
            switch statusCode {
            case 200:
                for dataInsideOfDictionary in dictionary {
                    let name = dataInsideOfDictionary["name"] as? String ?? ""
                    let artist = dataInsideOfDictionary["artist"] as? String ?? ""
                    let file = dataInsideOfDictionary["file_path"] as? String ?? ""
                    let image_path = dataInsideOfDictionary["image_path"] as? String ?? ""
                    let duration = dataInsideOfDictionary["duration"] as? Int ?? 0
                    self.musicTitle.append(name)
                    self.musicArtist.append(artist)
                    self.musicUrl.append(file)
                    self.musicDuration.append(duration)
                    self.musicAlbumCover.append(image_path)
                }
                
                self.id_list = data["id_list"] as? String ?? ""
                
                print("musicUrl counts: \(self.musicUrl.count)")
                if self.musicUrl.count < 10 {
                    self.id_list = ""
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
                break
            default:
                debugPrint("error : \(messsage)")
            }
                
        }}.resume()
    }
    
    func playPause() {
        if myPlayer.player != nil {
            if myPlayer.player.rate == 1 {
                let playButtonItem = UIBarButtonItem(image: UIImage(named: "pause"), style: .plain, target: self, action: #selector(playPauseAction))
                self.tabBarController?.popupBar.rightBarButtonItems = [playButtonItem]
            } else {
                let pauseButtonItem = UIBarButtonItem(image: UIImage(named: "play"), style: .plain, target: self, action: #selector(playPauseAction))
                self.tabBarController?.popupBar.rightBarButtonItems = [pauseButtonItem]
            }
        }
    }
    
    @objc func playPauseAction() {
        if myPlayer.player != nil {
            if myPlayer.player.rate == 1 {
                let playButtonItem = UIBarButtonItem(image: UIImage(named: "play"), style: .plain, target: self, action: #selector(playPauseAction))
                self.tabBarController?.popupBar.rightBarButtonItems = [playButtonItem]
                myPlayer.player.pause()
                playIcon.image = UIImage(named: "play")
            } else {
                let pauseButtonItem = UIBarButtonItem(image: UIImage(named: "pause"), style: .plain, target: self, action: #selector(playPauseAction))
                self.tabBarController?.popupBar.rightBarButtonItems = [pauseButtonItem]
                myPlayer.player.play()
                playIcon.image = UIImage(named: "pause")
            }
        }
    }
}
