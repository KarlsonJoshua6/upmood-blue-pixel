//
//  Player.swift
//  upmood
//
//  Created by John Paul Manoza on 03/09/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import AVFoundation

class Player {
    
    static let sharedInstance = Player()
    var player: AVPlayer!
    var playerItem: AVPlayerItem!

    func playOrPause() {
        if player != nil {
            if player.rate == 1 {
                player.pause()
            } else {
                player.play()
            }
        }
    }

    func playWithURL(withUrl : [String], index: Int) {
        resetPlayer()
        let url = URL(string: AWSService().getPreSignedURL(S3DownloadKeyName: withUrl[index]))
        player = AVPlayer.init(url: url!)
        player.play()
        player.automaticallyWaitsToMinimizeStalling = false
        playerItem = player.currentItem
    }
    
    func resetPlayer() {
        if player != nil {
            player.pause()
            player = nil
        }
    }
}
