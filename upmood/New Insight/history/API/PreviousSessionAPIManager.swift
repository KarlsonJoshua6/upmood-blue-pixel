//
//  APICallManager.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 13/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SwiftyJSON
import Foundation

class PreviousSessionAPIManager {
    
    var header: HTTPHeaders = [String:String]()
    let params: [String: Any] = ["keyword": "", "timezone": localTimeZoneName]
    
    static let instance = PreviousSessionAPIManager()
    
    enum RequestMethod {
        case get
        case post
    }
    
    enum Endpoint: String {
        case PreviousSession = "/api/v4/ios/insight/user/session/previous-session"
        case JoinPreviousSession = "/api/v4/ios/insight/user/session/join-session"
        case LeavePreviousSession = "/api/v4/ios/insight/user/session/leave-session"
    }
    
    // MARK: Public Session API
    func callAPIGetPreviousSession(onSuccess successCallback: ((_ session: [PreviousSessionModel]) -> Void)?,
                          onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.PreviousSession.rawValue
        let userToken = KeychainSwift().get("apiToken") ?? ""
        header =  ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
        print("ito ang url: \(url)")
        
        // call API
        self.createRequest(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                print("responseObject: \(responseObject)")
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let responseDict = responseObject["data"]["data"].arrayObject {
                       let previousSessionDict = responseDict as! [[String:AnyObject]]

                        // Create object
                        var data = [PreviousSessionModel]()
                        for item in previousSessionDict {
                            let single = PreviousSessionModel.build(item)
                            print("single 1: \(single)")
                            data.append(single)
                        }

                        // Fire callback
                        successCallback?(data)
                    } else {
                        failureCallback?("An error has occured, when parsing json array")
                    }
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    // MARK: - Join Session API
    func callAPIJoinPreviousSession(onSuccess successCallback: ((_ session: JoinPublicSessionModel) -> Void)?,
                                  onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.JoinPreviousSession.rawValue
        let params: [String: Any] = ["id": "\(id)"]
        let userToken = KeychainSwift().get("apiToken") ?? ""
        header = ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
        // call API
        self.createRequest(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let mainDataDict = responseObject["data"].dictionary {
                       let mainData = JoinPublicSessionModel.build(mainDataDict)
                       successCallback?(mainData)
                    }
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    // MARK: - Leave Session API
    // MARK: - Join Session API
    func callAPILeavePreviousSession(onSuccess successCallback: ((_ session: LeavePublicSessionModel) -> Void)?,
                                  onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.LeavePreviousSession.rawValue
        let params: [String: Any] = ["id": "\(id)"]
        let userToken = KeychainSwift().get("apiToken") ?? ""
        header = ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
        // call API
        self.createRequest(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let mainDataDict = responseObject["data"].dictionary {
                       let mainData = LeavePublicSessionModel.build(mainDataDict)
                       successCallback?(mainData)
                    }
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    // MARK: Request Handler
    // Create request
    func createRequest(
        _ url: String,
        method: HTTPMethod,
        headers: [String: String],
        parameters: [String: Any],
        encoding: JSONEncoding,
        onSuccess successCallback: ((JSON) -> Void)?,
        onFailure failureCallback: ((String) -> Void)?
        ) {
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                successCallback?(json)
            case .failure(let error):
                if let callback = failureCallback {
                    // Return
                    callback(error.localizedDescription)
                }
            }
        }
    }
}
