//
//  PublicSessionService.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

class PreviousSessionService {
    public func callAPIGetPreviousSession(onSuccess successCallback: ((_ session: [PreviousSessionModel]) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
    
        PreviousSessionAPIManager.instance.callAPIGetPreviousSession (
            onSuccess: { (session) in
                successCallback?(session)
            },
            onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
            }
        )
    }
    
    public func callAPIJoinPreviousSession(onSuccess successCallback: ((_ session: JoinPublicSessionModel) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int) {
        
        PreviousSessionAPIManager.instance.callAPIJoinPreviousSession(onSuccess: { (session) in
            successCallback?(session)
        }, onFailure: { (errorMessage) in
            failureCallback?(errorMessage)
        }, id: id)
    }
    
    public func callAPILeavePreviousSession(onSuccess successCallback: ((_ session: LeavePublicSessionModel) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int) {
        
        PreviousSessionAPIManager.instance.callAPILeavePreviousSession(onSuccess: { (session) in
            successCallback?(session)
        }, onFailure: { (errorMessage) in
            failureCallback?(errorMessage)
        }, id: id)
    }
}

