//
//  HistoryViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 06/05/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let presenter = PreviousSessionPresenter(previousSessionService: PreviousSessionService())
    var previousSessionToDisplay = [PreviousSessionViewData]()
    let cellIdentifier = "PreviousSessionCell"
    let segueIdentifier = "viewSession"
    let storyboardId = "Dialog"
    let dialogOneVCID = "DialogViewController"
    let dialogTwoVCID = "DialogTwoViewController"
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.title = NSLocalizedString("History", comment: "")
        
        tableView.dataSource = self
        tableView.delegate = self
        activityIndicator.hidesWhenStopped = true
        
        presenter.attachView(view: self)
        presenter.getPreviousSession()
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if let viewSession = segue.destination as? ViewSessionInsight  {
            viewSession.session_id = previousSessionToDisplay[selectedIndex].id
            viewSession.ViewVCID = Constants.ViewControllerId.HISTORY_SESSION
         }
     }
    
    @IBAction func reloadHistory(_ storyboardSegue: UIStoryboardSegue) {
        viewDidLoad()
        tableView.reloadData()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension HistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    private func setUpJoinedDate(dateString: String) -> String {
        let df = DateFormatter()
        df.timeZone = TimeZone.current
        setUpDateFormatterLocale(withDateFormatter: df)
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let yourDate = df.date(from: dateString)
        df.dateFormat = "MMM dd, yyyy hh:mm a"
        let dateStr = df.string(from: yourDate ?? Date())
        return dateStr
    }
    
    @objc func joinSession(sender: UIButton) {
        if let index = self.tableView.indexPathForView(sender) {
            switch previousSessionToDisplay[index.row].userStatus {
            case 0:
               presenter.getJoinedPreviousSession(withId: previousSessionToDisplay[index.row].id)
            case 1...2:
               presenter.getLeavePreviousSession(withId: previousSessionToDisplay[index.row].id)
            case 3:
               presenter.getJoinedPreviousSession(withId: previousSessionToDisplay[index.row].id)
            default:
                break
            }
        }
    }
    
    private func setUpSessionIcon(cell: PreviousSessionCell, indexPath: IndexPath) {
        switch previousSessionToDisplay[indexPath.row].sessionType {
        case Constants.SessionType.CORPORATE:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.CORPORATE)
        case Constants.SessionType.FITNESS:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.FITNESS)
            cell.joinButton.isUserInteractionEnabled = false
        case Constants.SessionType.FITNESS_TWO:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.FITNESS)
            cell.joinButton.isUserInteractionEnabled = false
        case Constants.SessionType.RESEARCH:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.RESEARCH)
        case Constants.SessionType.MOVIE:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.MOVIE)
        default:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.SOLUTION)
        }
    }
    
    private func setUpButton(cell: PreviousSessionCell, indexPath: IndexPath) {
        switch previousSessionToDisplay[indexPath.row].userStatus {
        case 0:
            cell.joinButton.setTitle(NSLocalizedString("Join", comment: ""), for: .normal)
        case 1...2:
            cell.joinButton.setTitle(NSLocalizedString("Leave", comment: ""), for: .normal)
        case 3:
            cell.joinButton.setTitle(NSLocalizedString("Return", comment: ""), for: .normal)
        default:
            break
        }
        
        if previousSessionToDisplay[indexPath.row].passcode == 1 {
            cell.joinButton.isUserInteractionEnabled = false
        }
    }
    
    private func setUpDateFormatterLocale(withDateFormatter: DateFormatter) {
        if UserDefaults.standard.string(forKey: "selectedLanguage") == Constants.Language.ENG {
            withDateFormatter.locale = Locale(identifier: Constants.Language.ENG)
        } else {
            withDateFormatter.locale = Locale(identifier: Constants.Language.JAP)
        }
    }
    
    private func setUpSessionCreatedDate(cell: PreviousSessionCell, indexPath: IndexPath) {

        let df = DateFormatter()
        setUpDateFormatterLocale(withDateFormatter: df)
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let yourDate = df.date(from: previousSessionToDisplay[indexPath.row].createdAt)
        df.dateFormat = "MMM dd, yyyy"
        let dateStr = df.string(from: yourDate ?? Date())
        
        cell.session_date.text = "\(dateStr)  ●  \(setUpSessionTime(cell: cell, indexPath: indexPath))"
    }
    
    private func setUpSessionTime(cell: PreviousSessionCell, indexPath: IndexPath) -> String {
        if checkIfSesionIsStarted(withIsStart: previousSessionToDisplay[indexPath.row].is_start) == true {
            if previousSessionToDisplay[indexPath.row].gathering_type == Constants.GatheringType.CONTINUOUS || previousSessionToDisplay[indexPath.row].gathering_type == Constants.GatheringType.INTERVAL {
                return computeElapsedTime(withDurationType: previousSessionToDisplay[indexPath.row].duration_type, duration: previousSessionToDisplay[indexPath.row].duration)
            }
        } else {
            return "Not Yet Started"
        }
        
        return "Not Yet Implemented"
    }
    
    private func setUpCell(cell: PreviousSessionCell, indexPath: IndexPath) {
        cell.session_name.text = previousSessionToDisplay[indexPath.row].sessionName
        cell.session_company.text = previousSessionToDisplay[indexPath.row].companyName
        setUpSessionIcon(cell: cell, indexPath: indexPath)
        setUpSessionCreatedDate(cell: cell, indexPath: indexPath)
        setUpButton(cell: cell, indexPath: indexPath)
        cell.joinButton.addTarget(self, action: #selector(joinSession(sender:)), for: .touchUpInside)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PreviousSessionCell
        setUpCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return previousSessionToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: segueIdentifier, sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 114.0
    }
}

// MARK: - Presenter Delagate

extension HistoryViewController: PreviousSessionView {
    
    func getError(withMessage: String) {
        self.presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: withMessage)
    }
    
    func getJoinedSession(session: JoinedPreviousSessionViewData) {
        print("joined Session Data: \(session)")
        viewDidLoad()
        tableView.reloadData()
        instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogOneVCID)
    }
    
    func getLeaveSesion(session: LeavePrevioousSessionViewData) {
        print("Leave Session Data: \(session)")
        viewDidLoad()
        tableView.reloadData()
    }
    
    private func instantiateViewController(withStoryboardName: String, viewControllerId: String) {
        let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! DialogViewController
        
        let df = DateFormatter()
        let dateStr = df.string(from: Date())
        controller.dateJoined = setUpJoinedDate(dateString: dateStr)
        
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: false, completion: nil)
    }
    
    func startLoading() {
        activityIndicator.startAnimating()
    }
    
    func finishLoading() {
        activityIndicator.stopAnimating()
        if previousSessionToDisplay.count == 0 {
            emptyStateView.isHidden = false
        }
    }
    
    func setPreviousSession(session: [PreviousSessionViewData]) {
        previousSessionToDisplay = session
        tableView.isHidden = false
        tableView.reloadData()
    }
    
    func setEmptyPreviousSession() {
        tableView.isHidden = true
    }
}

class PreviousSessionCell: UITableViewCell {
    @IBOutlet weak var session_icon: UIImageView!
    @IBOutlet weak var session_name: UILabel!
    @IBOutlet weak var session_company: UILabel!
    @IBOutlet weak var session_date: UILabel!
    @IBOutlet weak var joinButton: UIButton!
}


// MARK: Helper to Compute Remaining Time in Previous Session

extension HistoryViewController {
    private func checkIfSesionIsStarted(withIsStart: Int) -> Bool {
        if withIsStart == 1 {
            return true
        }
        return false
    }
    
    private func computeElapsedTime(withDurationType: Int, duration: Int) -> String {
        switch withDurationType {
        case 0:
            if duration == 0 {
                return "End"
            }
            return "\(duration) Minutes"
        case 1:
            return "\(duration) Hours"
        case 2:
            return "\(duration) Days"
        case 3:
            return "\(duration) Months"
        default: break
        }
        
        return "None"
    }
}
