//
//  PublicSessionPresenter.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

// MARK: - Setup View Data

struct PreviousSessionViewData {
    let id: Int
    let sessionName: String
    let sessionDesc: String
    let companyName: String
    let gatheringType: String
    let sessionType: String
    let userStatus: Int
    let createdAt: String
    let session_type: String
    var gathering_type: String
    var session_end: String
    var duration: Int
    var duration_type: Int
    var is_start: Int
    var passcode: Int
}

struct JoinedPreviousSessionViewData {
    let id: String
    let user_id: Int
    let session_name: String
    let session_end: String
}

struct LeavePrevioousSessionViewData {
    let id: Int
    let passcode: String
    let user_id: String
    let session_name: String
}

// MARK: - Setup View with Object Protocol

protocol PreviousSessionView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setPreviousSession(session: [PreviousSessionViewData])
    func getJoinedSession(session: JoinedPreviousSessionViewData)
    func getLeaveSesion(session: LeavePrevioousSessionViewData)
    func setEmptyPreviousSession()
    func getError(withMessage: String)
}

// MARK: - Setup Presenter

class PreviousSessionPresenter {
    
    private let previousSessionService: PreviousSessionService
    weak private var previousSessionView : PreviousSessionView?
    
    init(previousSessionService: PreviousSessionService) {
        self.previousSessionService = previousSessionService
    }
    
    func attachView(view: PreviousSessionView) {
        previousSessionView = view
    }
    
    func detachView() {
        previousSessionView = nil
    }
    
    func getPreviousSession() {
        self.previousSessionView?.startLoading()
        
        previousSessionService.callAPIGetPreviousSession(onSuccess: { (session) in
        
            if (session.count == 0) {
                self.previousSessionView?.setEmptyPreviousSession()
            } else {                
                let mappedSession = session.map {
                    return PreviousSessionViewData(id: $0.id ?? 0, sessionName: "\($0.sessionName ?? "")", sessionDesc: "\($0.sessionDesc ?? "")", companyName: "\($0.companyName ?? "")", gatheringType: "\($0.gatheringType ?? "")", sessionType: "\($0.sessionType ?? "")", userStatus: $0.userStatus ?? 0, createdAt: "\($0.createdAt ?? "")", session_type: "\($0.session_type ?? "")", gathering_type: "\($0.gathering_type ?? "")", session_end: "\($0.session_end ?? "")", duration: $0.duration ?? 0, duration_type: $0.duration_type ?? 0, is_start: $0.is_start ?? 0, passcode: $0.passcode ?? 0)
                }
                
                self.previousSessionView?.setPreviousSession(session: mappedSession)
            }
            
            self.previousSessionView?.finishLoading()
            
        }) { (errorMessage) in
            debugPrint(errorMessage)
            self.previousSessionView?.finishLoading()
        }
    }
    
    func getJoinedPreviousSession(withId: Int) {
        self.previousSessionView?.startLoading()
        
        previousSessionService.callAPIJoinPreviousSession(onSuccess: { (session) in
            
            self.previousSessionView?.finishLoading()
            
            if let sessionName = session.session_name {
                if sessionName.isEmpty {
                     debugPrint("can't mapped Empty response")
                } else {
                    let mappedSession = JoinedPreviousSessionViewData(id: "\(session.id ?? "")", user_id: session.user_id ?? 0, session_name: "\(session.session_name ?? "")", session_end: "\(session.session_end ?? "")")
                    self.previousSessionView?.getJoinedSession(session: mappedSession)
                }
            }
            
        }, onFailure: { (errorMessage) in
            debugPrint(errorMessage)
            self.previousSessionView?.getError(withMessage: errorMessage)
            self.previousSessionView?.finishLoading()
        }, id: withId)
    }
    
    func getLeavePreviousSession(withId: Int) {
        self.previousSessionView?.startLoading()
        
        previousSessionService.callAPILeavePreviousSession(onSuccess: { (session) in
            
            self.previousSessionView?.finishLoading()
            
            if let sessionName = session.session_name {
                if sessionName.isEmpty {
                     debugPrint("can't mapped Empty response")
                } else {
                    let mappedSession = LeavePrevioousSessionViewData(id: session.id ?? 0, passcode: "\(session.passcode ?? "")", user_id: "\(session.user_id ?? "")", session_name: "\(session.session_name ?? "")")
                    self.previousSessionView?.getLeaveSesion(session: mappedSession)
                }
            }
            
        }, onFailure: { (errorMessage) in
            debugPrint(errorMessage)
            self.previousSessionView?.finishLoading()
        }, id: withId)
    }
}
