//
//  PublicSession.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import SwiftyJSON

class CurrentSessionModel {
    var id: Int?
    var sessionName: String?
    var sessionDesc: String?
    var companyName: String?
    var gatheringType: String?
    var sessionType: String?
    var userStatus: Int?
    var createdAt: String?
    var session_type: String?
    var session_end: String?
    var duration: Int?
    var duration_type: Int?
    var is_start: Int?
    var passcode: Int?

    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: AnyObject]) {
        if let data = dict["id"] as? Int {
            self.id = data
        }
        if let data = dict["session_name"] as? String {
            self.sessionName = data
        }
        if let data = dict["description"] as? String {
            self.sessionDesc = data
        }
        if let data = dict["company_name"] as? String {
            self.companyName = data
        }
        if let data = dict["gathering_type"] as? String {
            self.gatheringType = data
        }
        if let data = dict["session_type"] as? String {
            self.sessionType = data
        }
        if let data = dict["user_status"] as? Int {
            self.userStatus = data
        }
        if let data = dict["created_at"] as? String {
            self.createdAt = data
        }
        if let data = dict["session_type"] as? String {
            self.session_type = data
        }
        if let data = dict["session_end"] as? String {
            self.session_end = data
        }
        if let data = dict["duration"] as? Int {
            self.duration = data
        }
        if let data = dict["duration_type"] as? Int {
            self.duration_type = data
        }
        if let data = dict["is_start"] as? Int {
            self.is_start = data
        }
        if let data = dict["passcode"] as? Int {
            self.passcode = data
        }
    }

    // MARK: Class Method
    class func build(_ dict: [String: AnyObject]) -> CurrentSessionModel {
        let session = CurrentSessionModel()
        session.loadFromDictionary(dict)
        return session
    }
}

//
//// temp
//class CurrentSessionModel {
//    var id: Int?
//    var sessionName: String?
//    var sessionDesc: String?
//    var companyName: String?
//    var gatheringType: String?
//    var sessionType: String?
//    var userStatus: Int?
//    var createdAt: String?
//    var session_type: String?
//
//    // MARK: Instance Method
//    func loadFromDictionary(_ dict: [String: JSON]) {
//        if let data = dict["id"]?.intValue {
//            self.id = data
//        }
//        if let data = dict["session_name"]?.stringValue {
//            self.sessionName = data
//        }
//        if let data = dict["description"]?.stringValue {
//            self.sessionDesc = data
//        }
//        if let data = dict["company_name"]?.stringValue {
//            self.companyName = data
//        }
//        if let data = dict["gathering_type"]?.stringValue {
//            self.gatheringType = data
//        }
//        if let data = dict["session_type"]?.stringValue {
//            self.sessionType = data
//        }
//        if let data = dict["user_status"]?.intValue{
//            self.userStatus = data
//        }
//        if let data = dict["created_at"]?.stringValue {
//            self.createdAt = data
//        }
//        if let data = dict["session_type"]?.stringValue {
//            self.session_type = data
//        }
//    }
//
//    // MARK: Class Method
//    class func build(_ dict: [String: JSON]) -> CurrentSessionModel {
//        let session = CurrentSessionModel()
//        session.loadFromDictionary(dict)
//        return session
//    }
//}
