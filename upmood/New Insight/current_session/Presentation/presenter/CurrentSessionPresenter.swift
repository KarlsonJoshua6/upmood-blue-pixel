//
//  PublicSessionPresenter.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

// MARK: - Setup View Data

struct CurrentSessionViewData {
    let id: Int
    let sessionName: String
    let sessionDesc: String
    let companyName: String
    let gatheringType: String
    let sessionType: String
    let userStatus: Int
    let createdAt: String
    let session_type: String
    let session_end: String
    var duration: Int
    var duration_type: Int
    var is_start: Int
    var passcode: Int
}

struct LeaveSessionViewData {
    let id: Int
    let passcode: String
    let user_id: String
    let session_name: String
}

// MARK: - Setup View with Object Protocol

protocol CurrentSessionView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setCurrentSession(session: [CurrentSessionViewData])
    func getLeaveSesion(session: LeaveSessionViewData)
    func setEmptyCurrentSession()
}

// MARK: - Setup Presenter

class CurrentSessionPresenter {
    
    private let currentSessionService: CurrentSessionService
    weak private var currentSessionView : CurrentSessionView?
    
    init(currentSessionService: CurrentSessionService) {
        self.currentSessionService = currentSessionService
    }
    
    func attachView(view: CurrentSessionView) {
        currentSessionView = view
    }
    
    func detachView() {
        currentSessionView = nil
    }
    
    func getCurrentSession() {
        self.currentSessionView?.startLoading()
        
        currentSessionService.callAPIGetPublicSession(onSuccess: { (session) in
            
            if session.count == 0 {
                self.currentSessionView?.setEmptyCurrentSession()
            } else {
                let mappedSession = session.map {
                    return CurrentSessionViewData(id: $0.id ?? 0, sessionName: "\($0.sessionName ?? "")", sessionDesc: "\($0.sessionDesc ?? "")", companyName: "\($0.companyName ?? "")", gatheringType: "\($0.gatheringType ?? "")", sessionType: "\($0.sessionType ?? "")", userStatus: $0.userStatus ?? 0, createdAt: "\($0.createdAt ?? "")", session_type: "\($0.session_type ?? "")", session_end: "\($0.session_end ?? "")", duration: $0.duration ?? 0, duration_type: $0.duration_type ?? 0, is_start: $0.is_start ?? 0, passcode: $0.passcode ?? 0)
                }
                
                self.currentSessionView?.setCurrentSession(session: mappedSession)
            }
            
            self.currentSessionView?.finishLoading()
            
        }) { (errorMessage) in
            debugPrint(errorMessage)
            self.currentSessionView?.setEmptyCurrentSession()
            self.currentSessionView?.finishLoading()
        }
    }
    
    func getLeaveSession(withId: Int) {
        self.currentSessionView?.startLoading()
        
        currentSessionService.callAPILeaveSession(onSuccess: { (session) in
            
            self.currentSessionView?.finishLoading()
            
            if let sessionName = session.session_name {
                if sessionName.isEmpty {
                     debugPrint("can't mapped Empty response")
                } else {
                    let mappedSession = LeaveSessionViewData(id: session.id ?? 0, passcode: "\(session.passcode ?? "")", user_id: "\(session.user_id ?? "")", session_name: "\(session.session_name ?? "")")
                    self.currentSessionView?.getLeaveSesion(session: mappedSession)
                }
            }
            
        }, onFailure: { (errorMessage) in
            debugPrint(errorMessage)
            self.currentSessionView?.finishLoading()
        }, id: withId)
    }
}
