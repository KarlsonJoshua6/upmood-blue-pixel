//
//  PrivateSession.swift
//  upmood
//
//  Created by John Paul Manoza on 06/05/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class CurrentSession: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let presenter = CurrentSessionPresenter(currentSessionService: CurrentSessionService())
    var currentSessionToDisplay = [CurrentSessionViewData]()
    let cellIdentifier = "CurrentSessionCell"
    var selectedIndex = 0
    let segueIdentifier = "viewSession"
    let storyboardId = "Dialog"
    let dialogOneVCID = "DialogViewController"
    let dialogTwoVCID = "DialogTwoViewController"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        activityIndicator.hidesWhenStopped = true
            
        presenter.attachView(view: self)
        presenter.getCurrentSession()
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewSession = segue.destination as? ViewSessionInsight  {
            viewSession.session_id = currentSessionToDisplay[selectedIndex].id
            viewSession.ViewVCID = Constants.ViewControllerId.CURRENT_SESSION
        }
    }
    
    @IBAction func reloadCurrentSession(_ storyboardSegue: UIStoryboardSegue) {
        viewDidLoad()
        tableView.reloadData()
    }
    
    @objc func leaveSession(sender: UIButton) {
        if let index = self.tableView.indexPathForView(sender) {
            switch currentSessionToDisplay[index.row].userStatus {
            case 0:
                break
             //   presenter.getJoinedPublicSession(withId: publicSessionToDisplay[index.row].id)
            case 1...2:
                presenter.getLeaveSession(withId: currentSessionToDisplay[index.row].id)
            case 3:
                break
            //    presenter.getJoinedPublicSession(withId: publicSessionToDisplay[index.row].id)
            default:
                break
            }
        }
    }
}

extension CurrentSession: UITableViewDataSource, UITableViewDelegate {
    
    private func instantiateViewController(withStoryboardName: String, viewControllerId: String) {
         let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
         let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId)
         controller.modalPresentationStyle = .fullScreen
         self.present(controller, animated: false, completion: nil)
    }
    
    private func setUpSessionIcon(cell: CurrentSessionCell, indexPath: IndexPath) {
        switch currentSessionToDisplay[indexPath.row].session_type {
        case Constants.SessionType.CORPORATE:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.CORPORATE)
        case Constants.SessionType.FITNESS:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.FITNESS)
        case Constants.SessionType.FITNESS_TWO:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.FITNESS)
        case Constants.SessionType.RESEARCH:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.RESEARCH)
        case Constants.SessionType.MOVIE:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.MOVIE)
        default:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.SOLUTION)
        }
    }
    
    private func setUpButton(cell: CurrentSessionCell, indexPath: IndexPath) {
        switch currentSessionToDisplay[indexPath.row].userStatus {
        case 0:
            cell.joinButton.setTitle(TranslationConstants.JOIN.LOCALIZED, for: .normal)
        case 1...2:
            cell.joinButton.setTitle(TranslationConstants.LEAVE.LOCALIZED, for: .normal)
        case 3:
            cell.joinButton.setTitle(TranslationConstants.RETURN.LOCALIZED, for: .normal)
        default:
            break
        }
        
        if currentSessionToDisplay[indexPath.row].passcode == 1 {
            cell.joinButton.isUserInteractionEnabled = false
        }
    }
    
    private func setUpDateFormatterLocale(withDateFormatter: DateFormatter) {
        if UserDefaults.standard.string(forKey: "selectedLanguage") == Constants.Language.ENG {
            withDateFormatter.locale = Locale(identifier: Constants.Language.ENG)
        } else {
            withDateFormatter.locale = Locale(identifier: Constants.Language.JAP)
        }
    }
    
    private func setUpSessionCreatedDate(cell: CurrentSessionCell, indexPath: IndexPath) {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        setUpDateFormatterLocale(withDateFormatter: df)
        let yourDate = df.date(from: currentSessionToDisplay[indexPath.row].createdAt)
        df.dateFormat = "MMM dd, yyyy"
        let dateStr = df.string(from: yourDate ?? Date())
        
        cell.session_date.text = "\(dateStr)  ●  \(setUpSessionTime(cell: cell, indexPath: indexPath))"
    }
    
    private func setUpSessionTime(cell: CurrentSessionCell, indexPath: IndexPath) -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if checkIfSesionIsStarted(withIsStart: currentSessionToDisplay[indexPath.row].is_start) == true {
            if currentSessionToDisplay[indexPath.row].gatheringType == Constants.GatheringType.CONTINUOUS || currentSessionToDisplay[indexPath.row].gatheringType == Constants.GatheringType.INTERVAL{
                 return computeElapsedTime(withDurationType: currentSessionToDisplay[indexPath.row].duration_type, duration: currentSessionToDisplay[indexPath.row].duration)
            }
        } else {
            return "Not Yet Started"
        }
        
        return "Not Yet Implemented"
    }
    
    private func setUpCell(cell: CurrentSessionCell, indexPath: IndexPath) {
       cell.session_name.text = currentSessionToDisplay[indexPath.row].sessionName
       cell.session_company.text = currentSessionToDisplay[indexPath.row].companyName
       setUpSessionIcon(cell: cell, indexPath: indexPath)
       setUpSessionCreatedDate(cell: cell, indexPath: indexPath)
       setUpButton(cell: cell, indexPath: indexPath)
       cell.joinButton.addTarget(self, action: #selector(leaveSession(sender:)), for: .touchUpInside)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CurrentSessionCell
        setUpCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentSessionToDisplay.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: segueIdentifier, sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 106.0
    }
}

extension CurrentSession: CurrentSessionView {
    
    func getLeaveSesion(session: LeaveSessionViewData) {
        print("Leave Session Data Test: \(session)")
        instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogTwoVCID)
        viewDidLoad()
        tableView.reloadData()
       
    }
    
    func     setCurrentSession(session: [CurrentSessionViewData]) {
        currentSessionToDisplay = session
        tableView.reloadData()
    }
    func startLoading() {
        activityIndicator.startAnimating()
    }
       
    func finishLoading() {
        activityIndicator.stopAnimating()
        if currentSessionToDisplay.count == 0 {
            emptyStateView.isHidden = false
        }
    }
    
    func setEmptyCurrentSession() {
        tableView.isHidden = true
        emptyStateView.isHidden = false
    }
}

class CurrentSessionCell: UITableViewCell {
    @IBOutlet weak var session_icon: UIImageView!
    @IBOutlet weak var session_name: UILabel!
    @IBOutlet weak var session_company: UILabel!
    @IBOutlet weak var session_date: UILabel!
    @IBOutlet weak var joinButton: UIButton!
}

// MARK: Helper to Compute Remaining Time in Current Session

extension CurrentSession {
   
    private func checkIfSesionIsStarted(withIsStart: Int) -> Bool {
        if withIsStart == 1 {
            return true
        }
        return false
    }
    
    private func computeElapsedTime(withDurationType: Int, duration: Int) -> String {
        switch withDurationType {
        case 0:
            if duration == 0 {
                return "End"
            }
            return "\(duration) Minutes"
        case 1:
            return "\(duration) Hours"
        case 2:
            return "\(duration) Days"
        case 3:
            return "\(duration) Months"
        default: break
        }
        
        return "None"
    }
}
