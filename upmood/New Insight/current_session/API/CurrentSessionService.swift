//
//  PublicSessionService.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

class CurrentSessionService {
    public func callAPIGetPublicSession(onSuccess successCallback: ((_ session: [CurrentSessionModel]) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
    
        CurrentSessionAPIManager.instance.callAPIGetCurrentSession (
            onSuccess: { (session) in
                successCallback?(session)
            },
            onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
            }
        )
    }
    
    public func callAPILeaveSession(onSuccess successCallback: ((_ session: LeavePublicSessionModel) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int) {
        
        CurrentSessionAPIManager.instance.callAPILeaveSession(onSuccess: { (session) in
            successCallback?(session)
        }, onFailure: { (errorMessage) in
            failureCallback?(errorMessage)
        }, id: id)
    }
}

