//
//  ScanIntroViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 10/05/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Lottie
import SwiftQRScanner
import UIKit

class ScanIntroViewController: UIViewController {

    @IBOutlet weak var scanAnimation: AnimationView!
    let segueIdentifier = "showViewSession"
    var didScan = false
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUserInterfaceStyleLight(self: self)
        initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        scanAnimation.isHidden = false
        scanAnimation.play()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        scanAnimation.isHidden = true
        scanAnimation.pause()
    }
    
    @IBAction func close(_ sender: UIButton) {
        UserDefaults.standard.set("", forKey: "QrCodeResult")
        dismiss(animated: true, completion: nil)
    }
 
    @IBAction func scan(_ sender: UIButton) {
        let scanner = QRCodeScannerController()
        scanner.delegate = self
        self.present(scanner, animated: true, completion: nil)
    }
    
    private func initUI() {
        lblTitle.text = TranslationConstants.INSIGHT_SESSION.LOCALIZED
        
        scanAnimation.contentMode = UIViewContentMode.scaleAspectFill
        scanAnimation.animation = Animation.named("scanQr")
        scanAnimation.loopMode = .loop
        scanAnimation.play()
    }
}

extension ScanIntroViewController: QRScannerCodeDelegate {
    func qrScanner(_ controller: UIViewController, scanDidComplete result: String) {
        DispatchQueue.main.async {
            UserDefaults.standard.set(result, forKey: "QrCodeResult")
            self.performSegue(withIdentifier: self.segueIdentifier, sender: self)
        }
    }

    func qrScannerDidFail(_ controller: UIViewController, error: String) {
        // fail
        didScan = false
        UserDefaults.standard.set("", forKey: "QrCodeResult")
        presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: error)
    }

    func qrScannerDidCancel(_ controller: UIViewController) {
        didScan = false
        // cancel
    }
}
