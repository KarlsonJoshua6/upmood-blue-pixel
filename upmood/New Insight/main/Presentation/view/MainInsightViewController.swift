//
//  MainInsightViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 07/05/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit
import Lottie

class MainInsightViewController: UIViewController {

    @IBOutlet weak var rightPublicView: AnimationView!
    @IBOutlet weak var rightHistoryView: AnimationView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var emptyStateHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblInsight: UILabel!
    @IBOutlet weak var lblCurrentSession: UILabel!
    @IBOutlet weak var lblListOfSession: UILabel!
    @IBOutlet weak var lblPublicSession: UILabel!
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var seeAllButton: UIButton!
    
    @IBOutlet weak var lblShareYour: UILabel!
    
    let currentSessionCell = "CurrentSessionMainCell"
    let presenter = CurrentSessionPresenter(currentSessionService: CurrentSessionService())
    var currentSessionToDisplay = [CurrentSessionViewData]()
    let cellIdentifier = "CurrentSessionMainCell"
    var selectedIndex = 0
    let segueIdentifier = "viewSession"
    let storyboardId = "Dialog"
    let dialogOneVCID = "DialogViewController"
    let dialogTwoVCID = "DialogTwoViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.sizeToFit()
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.attachView(view: self)
        presenter.getCurrentSession()
        initUI()
        navigationController?.navigationBar.isHidden = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableHeight?.constant = self.tableView.contentSize.height
        self.emptyStateHeight.constant = 700.0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewSession = segue.destination as? ViewSessionInsight  {
            viewSession.session_id = currentSessionToDisplay[selectedIndex].id
            viewSession.ViewVCID = Constants.ViewControllerId.MAIN_CURRENT_SESSION
        }
    }
    
    @IBAction func close(_ sender: UIButton) {
        performSegue(withIdentifier: "goBackFromInsight", sender: self)
//        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func reloadMainInsight(_ storyboardSegue: UIStoryboardSegue) {
        viewDidLoad()
        viewWillAppear(true)
        tableView.reloadData()
    }
    
    private func initUI() {
        lblInsight.text = TranslationConstants.INSIGHT_SESSION.LOCALIZED
        lblCurrentSession.text = TranslationConstants.CURRENT_SESSION.LOCALIZED
        lblPublicSession.text = TranslationConstants.PUBLIC_SESISON.LOCALIZED
        lblListOfSession.text = TranslationConstants.LIST_CURRENT_SESSION.LOCALIZED
        lblHistory.text = TranslationConstants.HISTORY.LOCALIZED
        
        rightPublicView.animation = Animation.named("ar_public")
        rightPublicView.contentMode = UIViewContentMode.scaleAspectFill
        rightPublicView.backgroundColor = hexStringToUIColor(hex: "#263D9A")
        rightPublicView.loopMode = .loop
        rightPublicView.play()
        
        rightHistoryView.animation = Animation.named("ar_history")
        rightHistoryView.contentMode = UIViewContentMode.scaleAspectFill
        rightHistoryView.backgroundColor = hexStringToUIColor(hex: "#3DBECC")
        rightHistoryView.loopMode = .loop
        rightHistoryView.play()
        
        lblShareYour.text = TranslationConstants.SHAREYOUREMOTION.LOCALIZED
        
    }
}

extension MainInsightViewController: CurrentSessionView {
    func startLoading() {
        //
    }
    
    func finishLoading() {
        if currentSessionToDisplay.count == 0 {
            emptyStateView.isHidden = false
        } else {
            emptyStateView.isHidden = true
        }
        
        if currentSessionToDisplay.count > 3 {
            seeAllButton.isHidden = false
        }
        if currentSessionToDisplay.count <= 3 {
            seeAllButton.isHidden = true
        }
    }
    
    func setCurrentSession(session: [CurrentSessionViewData]) {
        print("current session ito: \(session)")
        currentSessionToDisplay = session
        tableView.reloadData()
    }
    
    func getLeaveSesion(session: LeaveSessionViewData) {
        print("Leave Session Data: \(session)")
        viewDidLoad()
        viewWillAppear(true)
        tableView.reloadData()
        instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogTwoVCID)
    }
    
    func setEmptyCurrentSession() {
        self.currentSessionToDisplay = []
        DispatchQueue.main.async {
            self.emptyStateView.isHidden = false
            self.tableView.reloadData()
        }
    }
    
    private func instantiateViewController(withStoryboardName: String, viewControllerId: String) {
         let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
         let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId)
         controller.modalPresentationStyle = .fullScreen
         self.present(controller, animated: false, completion: nil)
    }
    
    private func setUpSessionIcon(cell: CurrentSessionMainCell, indexPath: IndexPath) {
        switch currentSessionToDisplay[indexPath.row].session_type {
        case Constants.SessionType.CORPORATE:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.CORPORATE)
        case Constants.SessionType.FITNESS:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.FITNESS)
        case Constants.SessionType.FITNESS_TWO:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.FITNESS)
        case Constants.SessionType.RESEARCH:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.RESEARCH)
        case Constants.SessionType.MOVIE:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.MOVIE)
        default:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.SOLUTION)
        }
    }
    
    private func setUpButton(cell: CurrentSessionMainCell, indexPath: IndexPath) {
        switch currentSessionToDisplay[indexPath.row].userStatus {
        case 0:
            cell.joinButton.setTitle(NSLocalizedString("Join", comment: ""), for: .normal)
        case 1...2:
            cell.joinButton.setTitle(NSLocalizedString("Leave", comment: ""), for: .normal)
        case 3:
            cell.joinButton.setTitle(NSLocalizedString("Return", comment: ""), for: .normal)
        default:
            break
        }
        
        if currentSessionToDisplay[indexPath.row].passcode == 1 {
            cell.joinButton.isUserInteractionEnabled = false
        }
    }
    
    private func setUpDateFormatterLocale(withDateFormatter: DateFormatter) {
        if UserDefaults.standard.string(forKey: "selectedLanguage") == Constants.Language.ENG {
            withDateFormatter.locale = Locale(identifier: Constants.Language.ENG)
        } else {
            withDateFormatter.locale = Locale(identifier: Constants.Language.JAP)
        }
    }
    
    private func setUpSessionCreatedDate(cell: CurrentSessionMainCell, indexPath: IndexPath) {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        setUpDateFormatterLocale(withDateFormatter: df)
        let yourDate = df.date(from: currentSessionToDisplay[indexPath.row].createdAt)
        df.dateFormat = "MMM dd, yyyy"
        let dateStr = df.string(from: yourDate ?? Date())
        cell.session_date.text = "\(dateStr)  ●  \(setUpSessionTime(cell: cell, indexPath: indexPath))"
    }
    
    private func setUpSessionTime(cell: CurrentSessionMainCell, indexPath: IndexPath) -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if checkIfSesionIsStarted(withIsStart: currentSessionToDisplay[indexPath.row].is_start) == true {
            if currentSessionToDisplay[indexPath.row].gatheringType == Constants.GatheringType.CONTINUOUS || currentSessionToDisplay[indexPath.row].gatheringType == Constants.GatheringType.INTERVAL{
                 return computeElapsedTime(withDurationType: currentSessionToDisplay[indexPath.row].duration_type, duration: currentSessionToDisplay[indexPath.row].duration)
            }
        } else {
            return "Not Yet Started"
        }
        
        return "Not Yet Implemented"
    }
    
    private func setUpCell(cell: CurrentSessionMainCell, indexPath: IndexPath) {
        cell.selectionStyle = .none
        cell.session_name.text = currentSessionToDisplay[indexPath.row].sessionName
        cell.session_company.text = currentSessionToDisplay[indexPath.row].companyName
        setUpSessionIcon(cell: cell, indexPath: indexPath)
        setUpSessionCreatedDate(cell: cell, indexPath: indexPath)
        setUpButton(cell: cell, indexPath: indexPath)
        cell.joinButton.addTarget(self, action: #selector(leaveSession(sender:)), for: .touchUpInside)
    }
    
    @objc func leaveSession(sender: UIButton) {
        if let index = self.tableView.indexPathForView(sender) {
            switch currentSessionToDisplay[index.row].userStatus {
            case 0:
                break
             //   presenter.getJoinedPublicSession(withId: publicSessionToDisplay[index.row].id)
            case 1...2:
                presenter.getLeaveSession(withId: currentSessionToDisplay[index.row].id)
            case 3:
                break
            //    presenter.getJoinedPublicSession(withId: publicSessionToDisplay[index.row].id)
            default:
                break
            }
        }
    }
}


extension MainInsightViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CurrentSessionMainCell
        setUpCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentSessionToDisplay.count >= 4 {
            return 3
        }
        
        return currentSessionToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 108.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: segueIdentifier, sender: self)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
}

struct System {
    static func clearNavigationBar(forBar navBar: UINavigationBar) {
        navBar.setBackgroundImage(UIImage(), for: .default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
    }
}

class CurrentSessionMainCell: UITableViewCell {
    @IBOutlet weak var session_icon: UIImageView!
    @IBOutlet weak var session_name: UILabel!
    @IBOutlet weak var session_company: UILabel!
    @IBOutlet weak var session_date: UILabel!
    @IBOutlet weak var joinButton: UIButton!
}


// MARK: Helper to Compute Remaining Time in Current Session

extension MainInsightViewController {
   
    private func checkIfSesionIsStarted(withIsStart: Int) -> Bool {
        if withIsStart == 1 {
            return true
        }
        return false
    }
    
    private func computeElapsedTime(withDurationType: Int, duration: Int) -> String {
        switch withDurationType {
        case 0:
            if duration == 0 {
                return "End"
            }
            return "\(duration) Minutes"
        case 1:
            return "\(duration) Hours"
        case 2:
            return "\(duration) Days"
        case 3:
            return "\(duration) Months"
        default: break
        }
        
        return "None"
    }
}
