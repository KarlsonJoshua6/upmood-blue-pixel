//
//  Helper.swift
//  upmood
//
//  Created by John Paul Manoza on 05/05/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import UIKit

struct Helper {
    static func instantiateViewController(withStoryboardName: String) {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
        delegate?.window?.rootViewController?.modalPresentationStyle = .overCurrentContext
        delegate?.window?.rootViewController = storyboard.instantiateInitialViewController()
    }
}
