//
//  PublicSessionService.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

class PublicSessionService {
    public func callAPIGetPublicSession(onSuccess successCallback: ((_ session: [PublicSessionModel]) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
    
        APICallManager.instance.callAPIGetPublicSession (
            onSuccess: { (session) in
                successCallback?(session)
            },
            onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
            }
        )
    }
    
    public func callAPIGetPublicSessionInt(onSuccess successCallback: ((_ session: [PublicSessionModel]) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
     
         APICallManager.instance.callAPIGetPublicSessionInt (
             onSuccess: { (session) in
                 successCallback?(session)
             },
             onFailure: { (errorMessage) in
                 failureCallback?(errorMessage)
             }
         )
     }
    
    public func callAPIJoinPublicSession(onSuccess successCallback: ((_ session: JoinPublicSessionModel) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int) {
        
        APICallManager.instance.callAPIJoinPublicSession(onSuccess: { (session) in
            successCallback?(session)
        }, onFailure: { (errorMessage) in
            failureCallback?(errorMessage)
        }, id: id)
    }
    
    public func callAPILeavePublicSession(onSuccess successCallback: ((_ session: LeavePublicSessionModel) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int) {
        
        APICallManager.instance.callAPILeavePublicSession(onSuccess: { (session) in
            successCallback?(session)
        }, onFailure: { (errorMessage) in
            failureCallback?(errorMessage)
        }, id: id)
    }
}

