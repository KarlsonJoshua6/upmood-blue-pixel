//
//  APICallManager.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 13/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import Foundation
import KeychainSwift
import SwiftyJSON

let API_BASE_URL = Constants.Routes.BASE

class APICallManager {
    
    
    var header: HTTPHeaders = [String: String]()
    let params: [String: Any] = ["keyword": "", "timezone": localTimeZoneName]
    
    static let instance = APICallManager()
    
    enum RequestMethod {
        case get
        case post
    }
    
    enum Endpoint: String {
        case PublicSession = "/api/v4/ios/insight/user/session/session-list"
        case JoinPublicSession = "/api/v4/ios/insight/user/session/join-session"
        case LeavePublicSession = "/api/v4/ios/insight/user/session/leave-session"
    }
    
    // MARK: Public Session API
    func callAPIGetPublicSession(onSuccess successCallback: ((_ session: [PublicSessionModel]) -> Void)?,
                          onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.PublicSession.rawValue
        let userToken = KeychainSwift().get("apiToken") ?? ""
        header = ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
        print("ito ang url: \(url)")
        print("api tokenTo: \(userToken)")
        
        // call API
        self.createRequest(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                switch statusCode {
                case 200:

                    // Create dictionary
                    if let responseDict = responseObject["data"]["data"].arrayObject {
                       let publicSessionDict = responseDict as! [[String:AnyObject]]

                        // Create object
                        var data = [PublicSessionModel]()
                        for item in publicSessionDict {
                            let single = PublicSessionModel.build(item)
                            print("single 1: \(single)")
                            data.append(single)
                        }

                        // Fire callback
                        successCallback?(data)
                    }
                    else {
                        failureCallback?(message)
                    }
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    
    // MARK: Public Session API
    func callAPIGetPublicSessionInt(onSuccess successCallback: ((_ session: [PublicSessionModel]) -> Void)?,
                          onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.PublicSession.rawValue + "?page=2"
        let userToken = KeychainSwift().get("apiToken") ?? ""
        header = ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
        print("ito ang url: \(url)")
        print("api tokenTo: \(userToken)")
        
        // call API
        self.createRequest(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                
                print("response to: \(responseObject)")
                
                switch statusCode {
                case 200:
                    if let data = responseObject["data"].dictionary {
                        let nextPage = data["next_page_url"]?.stringValue
                        if nextPage!.isEmpty {
                            Constants.didHaveNextPageInPublicSession = false
                        } else {
                            Constants.didHaveNextPageInPublicSession = true
                        }
                            print("next_page_url", nextPage)
                        }
                    // Create dictionary
                    if let responseDict = responseObject["data"]["data"].arrayObject {
                       let publicSessionDict = responseDict as! [[String:AnyObject]]
    
                        // Create object
                        var data = [PublicSessionModel]()
                        for item in publicSessionDict {
                            let single = PublicSessionModel.build(item)
                            print("single 1: \(single)")
                            data.append(single)
                        }
    
                        // Fire callback
                        successCallback?(data)
                    } else {
                        failureCallback?("An error has occured.")
                    }
                    break
                case 204:
                    failureCallback?("An error has occured. statusCode == 204")
                    break
                case 400...500:
                    failureCallback?("An error has occured. statusCode == 400...500")
                    break
                default:
                    failureCallback?("An error has occured.")
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    // MARK: - Join Session API
    func callAPIJoinPublicSession(onSuccess successCallback: ((_ session: JoinPublicSessionModel) -> Void)?,
                                  onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.JoinPublicSession.rawValue
        let params: [String: Any] = ["id": "\(id)"]
        let userToken = KeychainSwift().get("apiToken") ?? ""
        header = ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
        // call API
        self.createRequest(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let mainDataDict = responseObject["data"].dictionary {
                       let mainData = JoinPublicSessionModel.build(mainDataDict)
                       successCallback?(mainData)
                    }
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    // MARK: - Leave Session API
    // MARK: - Join Session API
    func callAPILeavePublicSession(onSuccess successCallback: ((_ session: LeavePublicSessionModel) -> Void)?,
                                  onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.LeavePublicSession.rawValue
        let params: [String: Any] = ["id": "\(id)"]
        let userToken = KeychainSwift().get("apiToken") ?? ""
        header = ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
        // call API
        self.createRequest(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let mainDataDict = responseObject["data"].dictionary {
                       let mainData = LeavePublicSessionModel.build(mainDataDict)
                       successCallback?(mainData)
                    }
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    
    // MARK: Request Handler
    // Create request
    func createRequest(
        _ url: String,
        method: HTTPMethod,
        headers: [String: String],
        parameters: [String: Any],
        encoding: JSONEncoding,
        onSuccess successCallback: ((JSON) -> Void)?,
        onFailure failureCallback: ((String) -> Void)?
        ) {
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                successCallback?(json)
            case .failure(let error):
                if let callback = failureCallback {
                    // Return
                    callback(error.localizedDescription)
                }
            }
        }
    }
}
