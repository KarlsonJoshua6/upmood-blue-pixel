//
//  PublicSessionPresenter.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

// MARK: - Setup View Data

struct PublicSessionViewData {
    let id: Int
    let sessionName: String
    let sessionDesc: String
    let companyName: String
    let gatheringType: String
    let sessionType: String
    let userStatus: Int
    let createdAt: String
    let session_type: String
    var gathering_type: String
    var session_end: String
    var duration: Int
    var duration_type: Int
    var is_start: Int
}

struct JoinedPublicSessionViewData {
    let id: String
    let user_id: Int
    let session_name: String
    let session_end: String
}

struct LeavePublicSessionViewData {
    let id: Int
    let passcode: String
    let user_id: String
    let session_name: String
}

// MARK: - Setup View with Object Protocol

protocol PublicSessionView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setPublicSession(session: [PublicSessionViewData])
    func getPublicSession(session: [PublicSessionViewData])
    func getJoinedSession(session: JoinedPublicSessionViewData)
    func getLeaveSesion(session: LeavePublicSessionViewData)
    func setEmptyPublicSession()
    func getError(withMessage: String)
}

// MARK: - Setup Presenter

class PublicSessionPresenter {
    
    private let publicSessionService: PublicSessionService
    weak private var publicSessionView : PublicSessionView?
    
    init(publicSessionService: PublicSessionService) {
        self.publicSessionService = publicSessionService
    }
    
    func attachView(view: PublicSessionView) {
        publicSessionView = view
    }
    
    func detachView() {
        publicSessionView = nil
    }
    
    func getPublicSession() {
        self.publicSessionView?.startLoading()
        
        publicSessionService.callAPIGetPublicSession(onSuccess: { (session) in
            
            if (session.count == 0) {
                self.publicSessionView?.setEmptyPublicSession()
            } else {
                let mappedSession = session.map {
                    return PublicSessionViewData(id: $0.id ?? 0, sessionName: "\($0.sessionName ?? "")", sessionDesc: "\($0.sessionDesc ?? "")", companyName: "\($0.companyName ?? "")", gatheringType: "\($0.gatheringType ?? "")", sessionType: "\($0.sessionType ?? "")", userStatus: $0.userStatus ?? 0, createdAt: "\($0.createdAt ?? "")", session_type: "\($0.session_type ?? "")", gathering_type: "\($0.gathering_type ?? "")", session_end: "\($0.session_end ?? "")", duration: $0.duration ?? 0, duration_type: $0.duration_type ?? 0, is_start: $0.is_start ?? 0)
                }
                
                self.publicSessionView?.setPublicSession(session: mappedSession)
            }
            
            self.publicSessionView?.finishLoading()
            
        }) { (errorMessage) in
            debugPrint(errorMessage)
            self.publicSessionView?.finishLoading()
        }
    }
    
    func getPublicSessionInt() {
          self.publicSessionView?.startLoading()
          
          publicSessionService.callAPIGetPublicSessionInt(onSuccess: { (session) in
              
              if (session.count == 0) {
                  self.publicSessionView?.setEmptyPublicSession()
              } else {
                  let mappedSession = session.map {
                      return PublicSessionViewData(id: $0.id ?? 0, sessionName: "\($0.sessionName ?? "")", sessionDesc: "\($0.sessionDesc ?? "")", companyName: "\($0.companyName ?? "")", gatheringType: "\($0.gatheringType ?? "")", sessionType: "\($0.sessionType ?? "")", userStatus: $0.userStatus ?? 0, createdAt: "\($0.createdAt ?? "")", session_type: "\($0.session_type ?? "")", gathering_type: "\($0.gathering_type ?? "")", session_end: "\($0.session_end ?? "")", duration: $0.duration ?? 0, duration_type: $0.duration_type ?? 0, is_start: $0.is_start ?? 0)
                  }
                  
                  self.publicSessionView?.getPublicSession(session: mappedSession)
              }
              
              self.publicSessionView?.finishLoading()
              
          }) { (errorMessage) in
              debugPrint(errorMessage)
              self.publicSessionView?.finishLoading()
          }
      }
    
    func getJoinedPublicSession(withId: Int) {
        self.publicSessionView?.startLoading()
        
        publicSessionService.callAPIJoinPublicSession(onSuccess: { (session) in
            
            self.publicSessionView?.finishLoading()
            
            if let sessionName = session.session_name {
                if sessionName.isEmpty {
                     debugPrint("can't mapped Empty response")
                } else {
                    let mappedSession = JoinedPublicSessionViewData(id: "\(session.id ?? "")", user_id: session.user_id ?? 0, session_name: "\(session.session_name ?? "")", session_end: "\(session.session_end ?? "")")
                    self.publicSessionView?.getJoinedSession(session: mappedSession)
                }
            }
            
        }, onFailure: { (errorMessage) in
            debugPrint(errorMessage)
            self.publicSessionView?.getError(withMessage: errorMessage)
            self.publicSessionView?.finishLoading()
        }, id: withId)
    }
    
    func getLeavePublicSession(withId: Int) {
        self.publicSessionView?.startLoading()
        
        publicSessionService.callAPILeavePublicSession(onSuccess: { (session) in
            
            self.publicSessionView?.finishLoading()
            
            if let sessionName = session.session_name {
                if sessionName.isEmpty {
                     debugPrint("can't mapped Empty response")
                } else {
                    let mappedSession = LeavePublicSessionViewData(id: session.id ?? 0, passcode: "\(session.passcode ?? "")", user_id: "\(session.user_id ?? "")", session_name: "\(session.session_name ?? "")")
                    self.publicSessionView?.getLeaveSesion(session: mappedSession)
                }
            }
            
        }, onFailure: { (errorMessage) in
            debugPrint(errorMessage)
            self.publicSessionView?.finishLoading()
        }, id: withId)
    }

}
