//
//  PublicSessionViewController.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import UIKit

class PublicSessionViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let presenter = PublicSessionPresenter(publicSessionService: PublicSessionService())
    var publicSessionToDisplay = [PublicSessionViewData]()
    let cellIdentifier = "PublicSessionCell"
    let segueIdentifier = "viewSession"
    let storyboardId = "Dialog"
    let dialogOneVCID = "DialogViewController"
    let dialogTwoVCID = "DialogTwoViewController"
    var selectedIndex = 0
    
    override func viewDidLoad() {
        self.title = NSLocalizedString("Public Session", comment: "")
        
        tableView.dataSource = self
        tableView.delegate = self
        activityIndicator.hidesWhenStopped = true
        
        presenter.attachView(view: self)
        presenter.getPublicSession()
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewSession = segue.destination as? ViewSessionInsight  {
            viewSession.session_id = publicSessionToDisplay[selectedIndex].id
            viewSession.ViewVCID = Constants.ViewControllerId.PUBLIC_SESSION
        }
    }
    
    @objc func joinSession(sender: UIButton) {
        if let index = self.tableView.indexPathForView(sender) {
            switch publicSessionToDisplay[index.row].userStatus {
            case 0:
                presenter.getJoinedPublicSession(withId: publicSessionToDisplay[index.row].id)
            case 1...2:
                presenter.getLeavePublicSession(withId: publicSessionToDisplay[index.row].id)
            case 3:
                presenter.getJoinedPublicSession(withId: publicSessionToDisplay[index.row].id)
            default:
                break
            }
        }
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func reloadPublicSesion(_ storyboardSegue: UIStoryboardSegue) {
        viewDidLoad()
        tableView.reloadData()
    }
}

extension PublicSessionViewController: UITableViewDataSource, UITableViewDelegate {
    
    private func setUpJoinedDate(dateString: String) -> String {
        let df = DateFormatter()
        df.timeZone = TimeZone.current
        setUpDateFormatterLocale(withDateFormatter: df)
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let yourDate = df.date(from: dateString)
        df.dateFormat = "MMM dd, yyyy hh:mm a"
        let dateStr = df.string(from: yourDate ?? Date())
        return dateStr
    }
    
    private func setUpSessionIcon(cell: PublicSessionCell, indexPath: IndexPath) {
        switch publicSessionToDisplay[indexPath.row].session_type {
        case Constants.SessionType.CORPORATE:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.CORPORATE)
        case Constants.SessionType.FITNESS:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.FITNESS)
            cell.joinButton.isUserInteractionEnabled = false
        case Constants.SessionType.FITNESS_TWO:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.FITNESS)
            cell.joinButton.isUserInteractionEnabled = false
        case Constants.SessionType.RESEARCH:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.RESEARCH)
        case Constants.SessionType.MOVIE:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.MOVIE)
        default:
            cell.session_icon.image = UIImage(named: Constants.SessionTypeIcon.SOLUTION)
        }
    }
    
    private func setUpButton(cell: PublicSessionCell, indexPath: IndexPath) {
        switch publicSessionToDisplay[indexPath.row].userStatus {
        case 0:
            cell.joinButton.setTitle(NSLocalizedString("Join", comment: ""), for: .normal)
        case 1...2:
            cell.joinButton.setTitle(NSLocalizedString("Leave", comment: ""), for: .normal)
        case 3:
            cell.joinButton.setTitle(NSLocalizedString("Return", comment: ""), for: .normal)
        default:
            break
        }
    }
    
    private func setUpDateFormatterLocale(withDateFormatter: DateFormatter) {
        if UserDefaults.standard.string(forKey: "selectedLanguage") == Constants.Language.ENG {
            withDateFormatter.locale = Locale(identifier: Constants.Language.ENG)
        } else {
            withDateFormatter.locale = Locale(identifier: Constants.Language.JAP)
        }
    }
    
    private func setUpSessionCreatedDate(cell: PublicSessionCell, indexPath: IndexPath) {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        setUpDateFormatterLocale(withDateFormatter: df)
        let yourDate = df.date(from: publicSessionToDisplay[indexPath.row].createdAt)
        df.dateFormat = "MMM dd, yyyy"
        let dateStr = df.string(from: yourDate ?? Date())
        cell.session_date.text = "\(dateStr)  ●  \(setUpSessionTime(cell: cell, indexPath: indexPath))"
    }
    
    private func setUpSessionTime(cell: PublicSessionCell, indexPath: IndexPath) -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if checkIfSesionIsStarted(withIsStart: publicSessionToDisplay[indexPath.row].is_start) == true {
            if publicSessionToDisplay[indexPath.row].gathering_type == Constants.GatheringType.CONTINUOUS || publicSessionToDisplay[indexPath.row].gathering_type == Constants.GatheringType.INTERVAL {
                return computeElapsedTime(withDurationType: publicSessionToDisplay[indexPath.row].duration_type, duration: publicSessionToDisplay[indexPath.row].duration)
            }
        } else {
            return "Not Yet Started"
        }
        
        return "Not Yet Implemented"
    }
    
    private func setUpCell(cell: PublicSessionCell, indexPath: IndexPath) {
        cell.session_name.text = publicSessionToDisplay[indexPath.row].sessionName
        cell.session_company.text = publicSessionToDisplay[indexPath.row].companyName
        setUpSessionIcon(cell: cell, indexPath: indexPath)
        setUpSessionCreatedDate(cell: cell, indexPath: indexPath)
        setUpButton(cell: cell, indexPath: indexPath)
        cell.joinButton.addTarget(self, action: #selector(joinSession(sender:)), for: .touchUpInside)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PublicSessionCell
        setUpCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
         
       if Constants.didHaveNextPageInPublicSession {
            if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                Constants.currentPageInPublicSession+=1
                presenter.getPublicSessionInt()
            }

        } else {
            self.tableView.tableFooterView?.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return publicSessionToDisplay.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 114.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: segueIdentifier, sender: self)
    }
}

extension PublicSessionViewController: PublicSessionView {
    
    func getError(withMessage: String) {
        presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: withMessage)
    }
    
    private func instantiateViewController(withStoryboardName: String, viewControllerId: String) {
        if viewControllerId == dialogOneVCID {
            let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! DialogViewController
            
            let df = DateFormatter()
            let dateStr = df.string(from: Date())
            controller.dateJoined = setUpJoinedDate(dateString: dateStr)
            
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: false, completion: nil)
        } else {
            let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId)
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: false, completion: nil)
        }
    }
    
    func getLeaveSesion(session: LeavePublicSessionViewData) {
        print("Leave Session Data: \(session)")
        viewDidLoad()
        tableView.reloadData()
        instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogTwoVCID)
    }
    
    func getJoinedSession(session: JoinedPublicSessionViewData) {
        print("joined Session Data: \(session)")
        viewDidLoad()
        tableView.reloadData()
        instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogOneVCID)
    }
    
    func startLoading() {
        activityIndicator.startAnimating()
    }
      
    func finishLoading() {
        activityIndicator.stopAnimating()
        if publicSessionToDisplay.count == 0 {
            emptyStateView.isHidden = false
        }
    }
    
    func setPublicSession(session: [PublicSessionViewData]) {
        publicSessionToDisplay = session
        tableView.isHidden = false
        tableView.reloadData()
    }
    func getPublicSession(session: [PublicSessionViewData]) {
//           publicSessionToDisplay = session
           tableView.isHidden = false
           tableView.reloadData()
    }
    
    func setEmptyPublicSession() {
        tableView.isHidden = true
    }
}

class PublicSessionCell: UITableViewCell {
    @IBOutlet weak var session_icon: UIImageView!
    @IBOutlet weak var session_name: UILabel!
    @IBOutlet weak var session_company: UILabel!
    @IBOutlet weak var session_date: UILabel!
    @IBOutlet weak var joinButton: UIButton!
}


// MARK: Helper to Compute Remaining Time in Public Session

extension PublicSessionViewController {
   
    private func checkIfSesionIsStarted(withIsStart: Int) -> Bool {
        if withIsStart == 1 {
            return true
        }
        return false
    }
    
    private func computeElapsedTime(withDurationType: Int, duration: Int) -> String {
        switch withDurationType {
        case 0:
            if duration == 0 {
                return "End"
            }
            return "\(duration) Minutes"
        case 1:
            return "\(duration) Hours"
        case 2:
            return "\(duration) Days"
        case 3:
            return "\(duration) Months"
        default: break
        }
        
        return "None"
    }
}
