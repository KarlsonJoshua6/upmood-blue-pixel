//
//  User.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 17/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import SwiftyJSON

class LeavePublicSessionModel {
    var id: Int?
    var passcode: String?
    var user_id: String?
    var session_name: String?
    
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: JSON]) {
        if let data = dict["id"]?.intValue {
            self.id = data
        }
        if let data = dict["passcode"]?.stringValue {
            self.passcode = data
        }
        if let data = dict["user_id"]?.stringValue {
            self.user_id = data
        }
        if let data = dict["session_name"]?.stringValue {
            self.session_name = data
        }
    }
    
    // MARK: Class Method
    class func build(_ dict: [String: JSON]) -> LeavePublicSessionModel {
        let sessionData = LeavePublicSessionModel()
        sessionData.loadFromDictionary(dict)
        return sessionData
    }
}
