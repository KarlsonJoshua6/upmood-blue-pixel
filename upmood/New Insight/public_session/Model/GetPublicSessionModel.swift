//
//  GetPublicSessionModel.swift
//  upmood
//
//  Created by Taison Digital on 06/07/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import SwiftyJSON

class GetPublicSessionModel {
    var next_page_url: String?
    var current_page: Int?
    var prev_page_url: String?
    var first_page_url: String?
    
    //MARK: Instance Method
    func loadFromDictionary(_ dict: [String: JSON]){
        if let next_page_url = dict["next_page_url"]?.stringValue{
            self.next_page_url = next_page_url
            print("AHAHAH",next_page_url )
        }
        if let current_page = dict["current_page"]?.intValue{
                   self.current_page = current_page
        }
        if let prev_page_url = dict["prev_page_url"]?.stringValue{
                   self.prev_page_url = prev_page_url
        }
        if let first_page_url = dict["first_page_url"]?.stringValue{
                   self.first_page_url = first_page_url
        }
    }
    // MARK: Class Method
    class func build(_ dict: [String: JSON]) -> GetPublicSessionModel {
        let session = GetPublicSessionModel()
        session.loadFromDictionary(dict)
        return session
    }
}
