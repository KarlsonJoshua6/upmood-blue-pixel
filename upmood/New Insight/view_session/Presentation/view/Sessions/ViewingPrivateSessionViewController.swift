//
//  ViewingPrivateSessionViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 12/05/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class ViewingPrivateSessionViewController: UIViewController {

    @IBOutlet weak var sessionNameLabel: UILabel!
    @IBOutlet weak var sessionCompanyLabel: UILabel!
    @IBOutlet weak var sessionStatusLabel: UILabel!
    @IBOutlet weak var warningTextField: UILabel!
    @IBOutlet weak var passcoodeTextField: UITextField!
    @IBOutlet weak var joinButton: UIButton!
    
    let presenter = ViewSessionPresenter(viewSessionService: ViewSessionServices())
    var sessionName: String?
    var sessionCompany: String?
    var sessionStatus: Int?
    var userStatus: Int?
    var sesionId: Int?
    var session_passcode: Int?
    let storyboardId = "Dialog"
    let storyboardId2 = "ViewSession"
    let dialogOneVCID = "DialogViewController"
    let dialogTwoVCID = "DialogTwoViewController"
    let dialogThreeVCID = "ViewingPrivateSessionViewController"
    var joinedDate = ""
    
    var passcode: String {
        return passcoodeTextField.text ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpUI()
        setUserInterfaceStyleLight(self: self)
    }
    
    private func setUpUI() {
        passcoodeTextField.delegate = self
        if sessionName != nil || sessionCompany != nil || sessionStatus != nil {
            sessionNameLabel.text = sessionName ?? ""
            sessionCompanyLabel.text = sessionCompany ?? ""
            setUpStatus(status: sessionStatus ?? 0)
            setUpButton(userStatus: userStatus ?? 0)
        }
    }
    
    private func setUpStatus(status: Int) {
        switch status {
        case 0:
            self.sessionStatusLabel.text = TranslationConstants.END_SESSION.LOCALIZED
            self.sessionStatusLabel.textColor = .red
        case 1:
            self.sessionStatusLabel.text = "In Progress"
        case 2:
            self.sessionStatusLabel.text = TranslationConstants.PAUSED_SESSION.LOCALIZED
            self.sessionStatusLabel.textColor = .orange
        default: break
        }
    }
    
    private func setUpButton(userStatus: Int) {
        switch userStatus {
        case 0:
            self.joinButton.setTitle(TranslationConstants.JOIN.LOCALIZED, for: .normal)
            self.warningTextField.text = "Enter passcode to join session"
        case 1...2:
            self.joinButton.setTitle(TranslationConstants.LEAVE.LOCALIZED, for: .normal)
            self.warningTextField.text = "Enter passcode to leave session"
        case 3:
            self.joinButton.setTitle(TranslationConstants.RETURN.LOCALIZED, for: .normal)
            self.warningTextField.text = "Enter passcode to return session"
        default:
            break
        }
    }
    
    @IBAction func close(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func join(_ sender: UIButton) {
        if passcode.isEmpty {
            presentDismissableAlertController(title: "Warning", message: Constants.MessageDialog.INCOMPLETE_FORM)
        } else {
            setUpSessionStatus(sessionStatus: userStatus ?? 0, withPasscode: session_passcode ?? 0)
        }
    }
    
    private func setUpSessionStatus(sessionStatus: Int, withPasscode: Int) {
        switch sessionStatus {
        case 0:
            // call join session api
            if withPasscode == 0 {
                presenter.getJoinedViewedSessionData(withId: sesionId ?? 0)
            } else {
                presenter.getJoinedViewedSessionData(withId: sesionId ?? 0, withPassCode: passcode)
            }
        case 1...2:
            // call leave session api
            if withPasscode == 0 {
                presenter.getLeaveViewedSessionData(withId: sesionId ?? 0)
            } else {
                presenter.getLeaveViewedSessionData(withId: sesionId ?? 0, withPassCode: passcode)
            }
        case 3:
             // call return session api
            if withPasscode == 0 {
                presenter.getJoinedViewedSessionData(withId: sesionId ?? 0)
            } else {
                presenter.getJoinedViewedSessionData(withId: sesionId ?? 0, withPassCode: passcode)
            }
        default:
            break
        }
    }
}

extension ViewingPrivateSessionViewController: ViewSessionView {
    
    func getError(withErrorMessage: String) {
        presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: withErrorMessage)
    }
    
    func getJoinedPublicFitnessSession(sessionData: JoinedPublicFitnessSessionViewData) {
        instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogOneVCID)
    }
    
    func getJoinedPrivateFitnessSession(sessionData: JoinedPrivateFitnessSessionViewData) {
        instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogOneVCID)
    }
    
    private func instantiateViewController(withStoryboardName: String, viewControllerId: String) {
        if viewControllerId == dialogOneVCID {
            let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! DialogViewController
            controller.dateJoined = joinedDate
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: false, completion: nil)
        } else {
            let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId)
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: false, completion: nil)
        }
    }
    
    func startLoading() {
        
    }
    
    func finishLoading() {
        
    }
    
    func getViewSession(session: ViewSessionViewData) {
        
    }
    
    func getJoinedViewedSession(sessionData: JoinedViewedSessionViewData) {
        instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogOneVCID)
    }
    
    func getLeaveViewedSession(sessionData: LeaveViewedSessionViewData) {
        instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogTwoVCID)
    }
    
    func setEmptyViewSession() {
        
    }
}

extension ViewingPrivateSessionViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == UIReturnKeyType.done {
            self.passcoodeTextField.resignFirstResponder()
        }
           
        return true
    }
}
