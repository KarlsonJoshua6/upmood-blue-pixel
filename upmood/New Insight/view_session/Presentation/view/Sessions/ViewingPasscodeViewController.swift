//
//  ViewingPasscodeViewController.swift
//  upmood
//
//  Created by Taison Digital on 01/07/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class ViewingPasscodeViewController: UIViewController {

    @IBOutlet weak var tfPasscode: UITextField!
    
    var sesionId: Int?
    var passcode: String {
        return tfPasscode.text ?? ""
    }
    var joinedDate = ""
    
    let presenter = ViewSessionPresenter(viewSessionService: ViewSessionServices())
    let storyboardId = "Dialog"
    let dialogTwoVCID = "DialogTwoViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUserInterfaceStyleLight(self: self)
    }
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnLeave(_ sender: Any) {
        if passcode.isEmpty {
            presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: Constants.MessageDialog.INCOMPLETE_FORM)
        } else {
           setUpLeaveSession()
        }
    }
}

extension ViewingPasscodeViewController: ViewSessionView {
    
    func getError(withErrorMessage: String) {
        presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: withErrorMessage)
    }
    
    private func instantiateViewController(withStoryboardName: String, viewControllerId: String) {
        let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId)
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: false, completion: nil)
    }
    
    private func setUpLeaveSession(){
        presenter.getLeaveViewedSessionData(withId: sesionId ?? 0, withPassCode: passcode)
    }
    
    func startLoading() {
       //
    }
    
    func finishLoading() {
        //
    }
    
    func getViewSession(session: ViewSessionViewData) {
        //
    }
    
    func getJoinedViewedSession(sessionData: JoinedViewedSessionViewData) {
        //
    }
    
    func getLeaveViewedSession(sessionData: LeaveViewedSessionViewData) {
       instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogTwoVCID)
    }
    
    func getJoinedPublicFitnessSession(sessionData: JoinedPublicFitnessSessionViewData) {
        //
    }
    
    func getJoinedPrivateFitnessSession(sessionData: JoinedPrivateFitnessSessionViewData) {
        //
    }
    
    func setEmptyViewSession() {
        //
    }
}
