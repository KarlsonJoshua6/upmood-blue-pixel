//
//  ViewingFitnessSessionViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 12/05/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class ViewingFitnessSessionViewController: UIViewController {

    @IBOutlet weak var sessionNameLabel: UILabel!
    @IBOutlet weak var sessionCompanyLabel: UILabel!
    @IBOutlet weak var sessionStatusLabel: UILabel!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var agePickerView: UIPickerView!
    @IBOutlet weak var genderPickerView: UIPickerView!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var passCodeTextField: UITextField!
    @IBOutlet weak var passcodeLabel: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var widthOfStatus: NSLayoutConstraint!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblPasscode: UILabel!
    @IBOutlet weak var btnBirthday: UIButton!
    @IBOutlet weak var lblBirthday: UILabel!
    
    
    let gender = [TranslationConstants.MALE.LOCALIZED, TranslationConstants.FEMALE.LOCALIZED]
    
    let age = Array(1...100)
    var sessionName: String?
    var sessionCompany: String?
    var sessionStatus: Int?
    var userStatus: Int?
    var sesionId: Int?
    var sessionPrivacy: Int?
    var session_passcode: Int?
    var genderStr = "Male"
    let presenter = ViewSessionPresenter(viewSessionService: ViewSessionServices())
    let storyboardId = "Dialog"
    let dialogOneVCID = "DialogViewController"
    let dialogTwoVCID = "DialogTwoViewController"
    
    var selectedAge = 4
    var selectedGender =  TranslationConstants.MALE.LOCALIZED
    var passcode: String {
        return passCodeTextField.text ?? ""
    }
    var weight: String {
        return weightTextField.text ?? ""
    }
    var birthdayStr: String {
        return lblBirthday.text ?? ""
    }
    var joinedDate = ""
    var selectedDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        initPickerView()
        setUpUI()
        setUserInterfaceStyleLight(self: self)
        getProfileFitness()
    }
    
    
    func getProfileFitness(){
        let gender = UserDefaults.standard.string(forKey: "fetchUserGender") ?? ""
        
        let weight1 = UserDefaults.standard.string(forKey: "fetchWeight") ?? ""
        
        if !weight1.isEmpty {
            weightTextField.text = weight1
        }
        
        let birthday = UserDefaults.standard.string(forKey: "fetchUserBirthday") ?? ""
        lblBirthday.text = convertBirthday(bday: birthday)
       
        let strDate = Date().today(format: "yyyy.MM.dd")
        let date = strDate.components(separatedBy: ".")
        let year = Int(date[0]) ?? 0
        
        let date2 = birthday.components(separatedBy: "/")
        let year2 = Int(date2[2]) ?? 0
        let birthYear = year - year2
        
        
        selectedAge = birthYear
        
     //   agePickerView.selectRow(birthYear-1, inComponent: 0, animated: true)
        genderPickerView.selectRow(genderToInt(gender: gender), inComponent: 0, animated: true)
    }
    
    @IBAction func btnBirthday(_ sender: Any) {
        datePickerTapped()
    }
    
    // setUP DatePicker
    fileprivate func datePickerTapped() {

//        limit to 18 year old and below
//
//        var components = DateComponents()
//        components.year = -100
//        let minDate = Calendar.current.date(byAdding: components, to: Date())
//
//        components.year = -5
//        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
        let alert = UIAlertController(style: .actionSheet, title: NSLocalizedString("Select date", comment: ""))
        
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: nil) { date in
            self.selectedDate = date
        }
        
        alert.addAction(image: nil, title: NSLocalizedString("Change", comment: ""), color: nil, style: .default, isEnabled: true) { [weak this = self] alert in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "ja-JP")
            formatter.dateFormat = "yyyy-MM-dd"
            let day = formatter.string(from: self.selectedDate)
            self.setUpDate(birthday: day)
        }
        
        alert.addAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func convertBirthday(bday: String) ->String{
        let d1 = bday.components(separatedBy: "/")
        
        var monthStr = ""
        var bdayStr = ""
        
        switch d1[0]{
        case "01":
            monthStr = "Jan"
        case "02":
            monthStr = "Feb"
        case "03":
            monthStr = "Mar"
        case "04":
            monthStr = "Apr"
        case "05":
            monthStr = "May"
        case "06":
            monthStr = "Jun"
        case "07":
            monthStr = "Jul"
        case "08":
            monthStr = "Aug"
        case "09":
            monthStr = "Sep"
        case "10":
            monthStr = "Oct"
        case "11":
            monthStr = "Nov"
        case "12":
            monthStr = "Dec"
        default:
        break
        }
        
        bdayStr = monthStr + " " + d1[1] + "," + " " + d1[2]
            
        return bdayStr
    }
    
    private func setUpDate(birthday: String) {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let oldDate = df.date(from: birthday)
        
        let df2 = DateFormatter()
        df2.dateFormat = "MM/dd/yyyy"
        let newDate = df2.string(from: oldDate ?? Date())
        print("new Date po ito: \(newDate)")
        UserDefaults.standard.set(newDate, forKey: "fetchUserBirthday")
        let birthday = UserDefaults.standard.string(forKey: "fetchUserBirthday") ?? ""
        lblBirthday.text = convertBirthday(bday: birthday)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tableVReloadData"), object: nil)
        
        let strDate = Date().today(format: "yyyy.MM.dd")
        let date = strDate.components(separatedBy: ".")
        let year = Int(date[0]) ?? 0
        
        let date2 = birthday.components(separatedBy: "/")
        let year2 = Int(date2[2]) ?? 0
        let birthYear = year - year2
        
        
        selectedAge = birthYear
        
    }
    
    @IBAction func join(_ sender: UIButton) {
        if session_passcode ?? 0 == 0 {
            if weight.isEmpty {
                presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: "Please enter weight.")
            } else {
                setUpJoinSession(withSessionPrivacy: sessionPrivacy ?? 0, sessionStatus: userStatus ?? 0)
            }
        } else {
            if passcode.isEmpty {
                presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: Constants.MessageDialog.INCOMPLETE_FORM)
            } else {
                setUpJoinSession(withSessionPrivacy: sessionPrivacy ?? 0, sessionStatus: userStatus ?? 0)
                
                
            }
        }
    }
    
    @IBAction func close(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

extension ViewingFitnessSessionViewController: ViewSessionView {
    
    func getError(withErrorMessage: String) {
        presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: withErrorMessage)
    }
    
    private func instantiateViewController(withStoryboardName: String, viewControllerId: String) {
        
        if viewControllerId == dialogOneVCID {
            let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! DialogViewController
            controller.dateJoined = joinedDate
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: false, completion: nil)
        } else {
            let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId)
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: false, completion: nil)
        }
     }
    
    private func setUpJoinSession(withSessionPrivacy: Int, sessionStatus: Int) {
        print("1: \(withSessionPrivacy), \(sessionStatus)")
        print("TEST BYEAR", selectedAge)
        switch sessionStatus {
        case 0:
            // call join session api
            
            if withSessionPrivacy == 0 {
                presenter.getJoinedPublicFitnessSessionData(withId: sesionId ?? 0, age: selectedAge, weight: Int(weight) ?? 0, sex: genderStr)
            } else {
                presenter.getJoinedPrivateFitnessSessionData(withId: sesionId ?? 0, age: selectedAge, weight: Int(weight) ?? 0, sex: genderStr, passcode: passcode)
                print("dito join dapat")
            }
        
        case 1...2:
            // call leave session api
            if withSessionPrivacy == 0 {
                presenter.getLeaveViewedSessionData(withId: sesionId ?? 0)
            } else {
                presenter.getLeaveViewedSessionData(withId: sesionId ?? 0, withPassCode: passcode)
            }
        case 3:
             // call return session api
            
            if withSessionPrivacy == 0 {
                presenter.getJoinedPublicFitnessSessionData(withId: sesionId ?? 0, age: selectedAge, weight: Int(weight) ?? 0, sex: genderStr)
            } else {
                presenter.getJoinedPrivateFitnessSessionData(withId: sesionId ?? 0, age: selectedAge, weight: Int(weight) ?? 0, sex: genderStr, passcode: passcode)
            }
        default:
            break
        }
        
        UserDefaults.standard.set(Int(weight) ?? 0, forKey: "fetchWeight")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tableVReloadData"), object: nil)
 
    }
    
    private func convertGender(gender: String){
        switch gender{
        case "男":
            genderStr = "Male"
            break
        case "女":
            genderStr = "Female"
            break
        case "Male":
            genderStr = "Male"
        case "Female":
            genderStr = "Female"
            break
        default: break
        }
        
        UserDefaults.standard.set(genderStr, forKey: "fetchUserGender")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tableVReloadData"), object: nil)
    }
    
    private func genderToInt(gender: String) -> Int{
        var genderInt = 0
        switch gender{
        case "Male":
            genderInt = 0
        case "Female":
            genderInt = 1
            break
        default: break
        }
        return genderInt
    }
    
    func startLoading() {
        //
    }
    
    func finishLoading() {
        //
    }
    
    func getViewSession(session: ViewSessionViewData) {
        //
    }
    
    func getJoinedViewedSession(sessionData: JoinedViewedSessionViewData) {
        // join
    }
    
    func getLeaveViewedSession(sessionData: LeaveViewedSessionViewData) {
        instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogTwoVCID)
    }
    
    func setEmptyViewSession() {
        //
    }
    
    func getJoinedPublicFitnessSession(sessionData: JoinedPublicFitnessSessionViewData) {
        instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogOneVCID)
    }
    
    func getJoinedPrivateFitnessSession(sessionData: JoinedPrivateFitnessSessionViewData) {
        instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogOneVCID)
    }
}


// MARK: - PickerView
extension ViewingFitnessSessionViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    private func setUpPasscodeTextField(withSessionPrivacy: Int) {
        if withSessionPrivacy == 0 {
            // public fitness session
            passCodeTextField.isHidden = true
            passCodeTextField.placeholder = Constants.EMPTY_STRING
            passcodeLabel.isHidden = false
        } else {
            // private fitness session
            passCodeTextField.isHidden = false
            passcodeLabel.isHidden = true
        }
    }
    
    private func setUpButton(userStatus: Int) {
        switch userStatus {
        case 0:
            self.joinButton.setTitle(TranslationConstants.JOIN.LOCALIZED, for: .normal)
        case 1...2:
            self.joinButton.setTitle(TranslationConstants.LEAVE.LOCALIZED, for: .normal)
        case 3:
            self.joinButton.setTitle(TranslationConstants.RETURN.LOCALIZED, for: .normal)
        default:
            break
        }
    }
    
    private func setUpStatus(status: Int) {
        switch status {
        case 0:
            self.sessionStatusLabel.text = TranslationConstants.END_SESSION.LOCALIZED
            self.sessionStatusLabel.textColor = .red
        case 1:
            self.sessionStatusLabel.text = "In Progress"
        case 2:
            self.sessionStatusLabel.text = TranslationConstants.PAUSED_SESSION.LOCALIZED
            self.sessionStatusLabel.textColor = .orange
        default: break
        }
    }
    
    private func setUpUI() {
        sessionNameLabel.text = sessionName ?? ""
        sessionCompanyLabel.text = sessionCompany ?? ""
        setUpButton(userStatus: userStatus ?? 0)
        setUpStatus(status: sessionStatus ?? 0)
        setUpPasscodeTextField(withSessionPrivacy: session_passcode ?? 0)
        lblStatus.text = TranslationConstants.STATUS.LOCALIZED + ":"
        lblAge.text = TranslationConstants.BIRTHDAY.LOCALIZED
        lblGender.text = TranslationConstants.GENDER.LOCALIZED
        lblPasscode.text = TranslationConstants.PASSCODE.LOCALIZED
        
        let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
        
        if selectedLanguage == "en" {
            widthOfStatus.constant = 130
        }
        
    }
    
    private func initPickerView() {
        agePickerView.delegate = self
        agePickerView.dataSource = self
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            selectedAge = age[row]
        } else {
            selectedGender = gender[row]
            convertGender(gender: selectedGender)
        }
    }
     
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            let ageArr = age.map { String($0) }
            return ageArr[row]
        } else {
            return gender[row]
        }
    }
     
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
     
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            let ageArr = age.map { String($0) }
            return ageArr.count
        } else {
            return gender.count
        }
    }
}
