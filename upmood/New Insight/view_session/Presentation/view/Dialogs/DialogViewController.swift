//
//  DialogViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 08/05/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Lottie
import UIKit

class DialogViewController: UIViewController {
    
    @IBOutlet weak var successAnimationView: AnimationView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    var dateJoined = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpdateJoined()
        setUpSuccessAnimation()
        setUserInterfaceStyleLight(self: self)
    }
    
    private func setUpdateJoined() {
        label2.text = "Session ends at \(dateJoined)"
    }
    
    @IBAction func Ok(_ sender: UIButton) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainInsightViewController") as? UINavigationController {
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: false, completion: nil)
        }
    }
    
    private func setUpSuccessAnimation() {
        successAnimationView.contentMode = UIViewContentMode.scaleAspectFill
        successAnimationView.animation = Animation.named("successAnimation")
        successAnimationView.loopMode = .playOnce
        successAnimationView.play()
    }
}
