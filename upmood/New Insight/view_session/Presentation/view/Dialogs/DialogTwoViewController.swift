//
//  DialogTwoViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 08/05/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Lottie
import UIKit

class DialogTwoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUserInterfaceStyleLight(self: self)
    }
    
    @IBAction func Ok(_ sender: UIButton) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainInsightViewController") as? UINavigationController {
            vc.modalPresentationStyle = .fullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: false, completion: nil)
        }
    }
}
