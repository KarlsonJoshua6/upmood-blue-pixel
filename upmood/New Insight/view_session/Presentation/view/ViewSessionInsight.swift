//
//  LoginViewController.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 17/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Spring
import UIKit

class ViewSessionInsight: UIViewController {
    
    @IBOutlet weak var sessionName: UILabel!
    @IBOutlet weak var sessionCompany: UILabel!
    @IBOutlet weak var sessionDescription: UILabel!
    @IBOutlet weak var sessionStatus: UILabel!
    @IBOutlet weak var sessionDate: UILabel!
    @IBOutlet weak var gathering_since_label: UILabel!
    @IBOutlet weak var joinButton: SpringButton!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var sessionDescriptionLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var sessionIcon: UIImageView!
    
    let presenter = ViewSessionPresenter(viewSessionService: ViewSessionServices())
    var viewedSssionDataToDisplay: ViewSessionViewData?
    var session_id = 0
    var localTimeZoneName: String { return TimeZone.current.identifier }
    let reloadPublicSesionStoryboardSegueId = "reloadPublicSesion"
    let reloadMainInsightStoryboardSegueId = "reloadMainInsight"
    let reloadCurrentSessionStoryboardSegueId = "reloadCurrentSession"
    let reloadHistoryStoryboardSegueId = "reloadHistory"
    var ViewVCID = ""
    let storyboardId = "Dialog"
    let storyboardId2 = "ViewSession"
    let dialogOneVCID = "DialogViewController"
    let dialogTwoVCID = "DialogTwoViewController"
    let dialogThreeVCID = "ViewingPrivateSessionViewController"
    let dialogFourVCID = "ViewingFitnessSessionViewController"
    let dialogFiveVCID = "viewingPasscodeViewController"
    let qrCodeString = "QrCodeResult"
    var didScan = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.attachView(view: self)
        setUpViewSession()
        initUI()
        setUserInterfaceStyleLight(self: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    private func setUpViewSession() {
        let qrCodeIdString = UserDefaults.standard.string(forKey: qrCodeString) ?? ""
        if qrCodeIdString.isEmpty {
            didScan = false
            presenter.getViewSession(withId: session_id, timeZone: localTimeZoneName)
        } else {
            didScan = true
            presenter.getViewSession(withId: Int(qrCodeIdString) ?? 0, timeZone: localTimeZoneName)
            UserDefaults.standard.set("", forKey: qrCodeString)
        }
    }
    
    @IBAction func closeViewController(_ sender: UIButton) {
        if didScan == true {
            dismiss(animated: true, completion: nil)
        } else {
            if ViewVCID == Constants.ViewControllerId.PUBLIC_SESSION {
                performSegue(withIdentifier: reloadPublicSesionStoryboardSegueId, sender: self)
            } else if ViewVCID == Constants.ViewControllerId.MAIN_CURRENT_SESSION {
                performSegue(withIdentifier: reloadMainInsightStoryboardSegueId, sender: self)
            } else if ViewVCID == Constants.ViewControllerId.CURRENT_SESSION {
                performSegue(withIdentifier: reloadCurrentSessionStoryboardSegueId, sender: self)
            } else if ViewVCID == Constants.ViewControllerId.HISTORY_SESSION {
                performSegue(withIdentifier: reloadHistoryStoryboardSegueId, sender: self)
            }
        }
    }
    
    @IBAction func joinSession(_ sender: UIButton) {
        if viewedSssionDataToDisplay != nil {
            setUpJoinSession(session: viewedSssionDataToDisplay!)
        }
    }
    
    private func initUI() {
        lblStatus.text = "\(TranslationConstants.STATUS.LOCALIZED):"
        lblStatus.sizeToFit()
    }
}

extension ViewSessionInsight: ViewSessionView {
    
    func getError(withErrorMessage: String) {
        emptyView.isHidden = false
        presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: withErrorMessage)
    }
    
    func getJoinedPublicFitnessSession(sessionData: JoinedPublicFitnessSessionViewData) {
        
    }
    
    func getJoinedPrivateFitnessSession(sessionData: JoinedPrivateFitnessSessionViewData) {
        
    }
    
    private func prepareDataToPassInFitnessSesion(withViewController: ViewingFitnessSessionViewController) {
        withViewController.sessionName = viewedSssionDataToDisplay!.sessionName
        withViewController.sessionCompany = viewedSssionDataToDisplay!.company_name
        withViewController.sessionStatus = viewedSssionDataToDisplay!.status
        withViewController.userStatus = viewedSssionDataToDisplay!.user_status
        withViewController.sesionId = viewedSssionDataToDisplay!.id
        withViewController.sessionPrivacy = viewedSssionDataToDisplay!.session_privacy
        withViewController.session_passcode = viewedSssionDataToDisplay!.passcode
        withViewController.joinedDate = setUpJoinedDate(session: viewedSssionDataToDisplay!)
    }
    
    private func prepareDataToPassInPrivateSession(withViewController: ViewingPrivateSessionViewController) {
        withViewController.sessionName = viewedSssionDataToDisplay!.sessionName
        withViewController.sessionCompany = viewedSssionDataToDisplay!.company_name
        withViewController.sessionStatus = viewedSssionDataToDisplay!.status
        withViewController.userStatus = viewedSssionDataToDisplay!.user_status
        withViewController.sesionId = viewedSssionDataToDisplay!.id
        withViewController.session_passcode = viewedSssionDataToDisplay!.passcode
        withViewController.joinedDate = setUpJoinedDate(session: viewedSssionDataToDisplay!)
    }
    
    private func preparedDataToPassInPasscodeSession(withViewController: ViewingPasscodeViewController){
        withViewController.joinedDate = setUpJoinedDate(session: viewedSssionDataToDisplay!)
        withViewController.sesionId = viewedSssionDataToDisplay!.id
    }
    
    private func instantiateViewController(withStoryboardName: String, viewControllerId: String) {
        
        if viewControllerId == dialogOneVCID {
            if viewedSssionDataToDisplay != nil {
                let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! DialogViewController
                controller.modalPresentationStyle = .fullScreen
                controller.dateJoined = self.setUpJoinedDate(session: self.viewedSssionDataToDisplay!)
                self.present(controller, animated: false, completion: nil)
            }
            
        } else if viewControllerId == dialogTwoVCID {
            if viewedSssionDataToDisplay != nil {
                let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! DialogTwoViewController
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: false, completion: nil)
            }
            
        } else if viewControllerId == dialogThreeVCID {
            if viewedSssionDataToDisplay != nil {
                let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! ViewingPrivateSessionViewController
                controller.modalPresentationStyle = .fullScreen
                prepareDataToPassInPrivateSession(withViewController: controller)
                self.present(controller, animated: false, completion: nil)
            }
            
        } else if viewControllerId == dialogFourVCID {
            if viewedSssionDataToDisplay != nil {
               let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! ViewingFitnessSessionViewController
               controller.modalPresentationStyle = .fullScreen
               prepareDataToPassInFitnessSesion(withViewController: controller)
               self.present(controller, animated: false, completion: nil)
            }
        }else if viewControllerId == dialogFiveVCID {
            if viewedSssionDataToDisplay != nil {
               let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! ViewingPasscodeViewController
               controller.modalPresentationStyle = .fullScreen
               preparedDataToPassInPasscodeSession(withViewController: controller)
               self.present(controller, animated: false, completion: nil)
            }
        }else {
            let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId)
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: false, completion: nil)
        }
    }
    
    private func setUpSessionIcon(session: ViewSessionViewData) {
        switch session.session_type {
        case Constants.SessionType.CORPORATE:
            sessionIcon.image = UIImage(named: Constants.SessionTypeIcon.CORPORATE)
        case Constants.SessionType.FITNESS:
            sessionIcon.image = UIImage(named: Constants.SessionTypeIcon.FITNESS)
        case Constants.SessionType.FITNESS_TWO:
            sessionIcon.image = UIImage(named: Constants.SessionTypeIcon.FITNESS)
        case Constants.SessionType.RESEARCH:
            sessionIcon.image = UIImage(named: Constants.SessionTypeIcon.RESEARCH)
        case Constants.SessionType.MOVIE:
            sessionIcon.image = UIImage(named: Constants.SessionTypeIcon.MOVIE)
        default:
            sessionIcon.image = UIImage(named: Constants.SessionTypeIcon.SOLUTION)
        }
    }
    
    func getLeaveViewedSession(sessionData: LeaveViewedSessionViewData) {
        
        print("testVCID", ViewVCID)
        
        if ViewVCID == Constants.ViewControllerId.PUBLIC_SESSION {
//            performSegue(withIdentifier: reloadPublicSesionStoryboardSegueId, sender: self)
            instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogTwoVCID)
        } else if ViewVCID == Constants.ViewControllerId.MAIN_CURRENT_SESSION {
//            performSegue(withIdentifier: reloadMainInsightStoryboardSegueId, sender: self)
            instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogTwoVCID)
        } else if ViewVCID == Constants.ViewControllerId.CURRENT_SESSION {
            performSegue(withIdentifier: reloadCurrentSessionStoryboardSegueId, sender: self)
        } else if ViewVCID == Constants.ViewControllerId.HISTORY_SESSION {
            performSegue(withIdentifier: reloadHistoryStoryboardSegueId, sender: self)
        }
    }
    
    func getJoinedViewedSession(sessionData: JoinedViewedSessionViewData) {
        
        if viewedSssionDataToDisplay != nil {
            instantiateViewController(withStoryboardName: storyboardId, viewControllerId: dialogOneVCID)
        }
    }
    
    private func setUpJoinSession(session: ViewSessionViewData) {
        if session.passcode == 0 {
            setUpJoinedSessionWith(sessionType: session)
          
        } else if session.passcode == 1 {
                     
            switch session.user_status {
            case 0:
                 // call join session api
                 //private session fitness/fitnessII set new VC
                   if session.session_type == Constants.SessionType.FITNESS || session.session_type == Constants.SessionType.FITNESS_TWO {
                       instantiateViewController(withStoryboardName: storyboardId2, viewControllerId: dialogFourVCID)
                   } else {
                       //private set new VC
                       instantiateViewController(withStoryboardName: storyboardId2, viewControllerId: dialogThreeVCID)
                   }
            case 1...2:
                  // call leave session api
                  //private session fitness/fitnessII set new VC
                   if session.session_type == Constants.SessionType.FITNESS || session.session_type == Constants.SessionType.FITNESS_TWO {
                       instantiateViewController(withStoryboardName: storyboardId2, viewControllerId: dialogFiveVCID)
                   } else {
                       //private set new VC
                       instantiateViewController(withStoryboardName: storyboardId2, viewControllerId: dialogFiveVCID)
                   }
                
            case 3:
                  // call return session api
                  //private session fitness/fitnessII set new VC
                   if session.session_type == Constants.SessionType.FITNESS || session.session_type == Constants.SessionType.FITNESS_TWO {
                       instantiateViewController(withStoryboardName: storyboardId2, viewControllerId: dialogFourVCID)
                   } else {
                       //private set new VC
                       instantiateViewController(withStoryboardName: storyboardId2, viewControllerId: dialogThreeVCID)
                   }
            default:
                break
            }            
        }
    }
    private func setUpJoinedSessionWith(sessionType: ViewSessionViewData) {
        print("sessionType.session_type", sessionType.session_type)
        switch sessionType.session_type {
        case Constants.SessionType.FITNESS:
            switch sessionType.user_status {
            case 1...2:
                setUpUserStatus(session: sessionType)
            default:
                instantiateViewController(withStoryboardName: storyboardId2, viewControllerId: dialogFourVCID)
            }
            
        case Constants.SessionType.FITNESS_TWO:
            switch sessionType.user_status {
            case 1...2:
                setUpUserStatus(session: sessionType)
            default:
                instantiateViewController(withStoryboardName: storyboardId2, viewControllerId: dialogFourVCID)
            }
            
        default:
            //join any other session except for fitness or fitness2
            setUpUserStatus(session: sessionType)
        }
    }
    
    private func setUpUserStatus(session: ViewSessionViewData) {
        print("session.user_status", session.user_status)
        switch session.user_status {
        case 0:
            // call join session api
            presenter.getJoinedViewedSessionData(withId: session.id)
        case 1...2:
            // call leave session api
            presenter.getLeaveViewedSessionData(withId: session.id)
        case 3:
             // call return session api
            presenter.getJoinedViewedSessionData(withId: session.id)
        default:
            break
        }
    }

    private func setUpButton(session: ViewSessionViewData) {
         print("testDITO", session.user_status)
        switch session.user_status {
        case 0:
            self.joinButton.setTitle(NSLocalizedString("Join", comment: ""), for: .normal)
            self.gathering_since_label.text = NSLocalizedString("By joining you agree to share your emotion, BPM, Stress Level, and Profile Details", comment: "")
        case 1...2:
            self.joinButton.setTitle(NSLocalizedString("Leave", comment: ""), for: .normal)
            self.gathering_since_label.text = "\(NSLocalizedString("Gathering since", comment: "")) \(setUpJoinedDate(session: session))"
        case 3:
            self.joinButton.setTitle(NSLocalizedString("Return", comment: ""), for: .normal)
            self.gathering_since_label.text = NSLocalizedString("By joining you agree to share your emotion, BPM, Stress Level, and Profile Details", comment: "")
        default:
            break
        }
    }
    
    private func setUpDateFormatterLocale(withDateFormatter: DateFormatter) {
        if UserDefaults.standard.string(forKey: "selectedLanguage") == Constants.Language.ENG {
            withDateFormatter.locale = Locale(identifier: Constants.Language.ENG)
        } else {
            withDateFormatter.locale = Locale(identifier: Constants.Language.JAP)
        }
    }
    
    private func setUpJoinedDate(session: ViewSessionViewData) -> String {
        let df = DateFormatter()
        df.timeZone = TimeZone.current
        setUpDateFormatterLocale(withDateFormatter: df)
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let yourDate = df.date(from: session.joined_date)
        df.dateFormat = "MMM dd, yyyy hh:mm a"
        let dateStr = df.string(from: yourDate ?? Date())
        return dateStr
    }
    
    private func setUpSessionStart(session: ViewSessionViewData) {
        let df = DateFormatter()
        // MARK: - TODO
        //setUpDateFormatterLocale(withDateFormatter: df)
        df.timeZone = TimeZone.current
        df.dateFormat = "yyyy-MM-dd h:mm a"
        let yourDate = df.date(from: session.session_start)
        
        df.dateFormat = "MMM dd, yyyy  ●  h:mm a"
        print("test your date: \(yourDate)")
        let dateStr = df.string(from: yourDate ?? Date())
        self.sessionDate.text = "\(dateStr)  ●  \(setUpSessionTime(session: session))"
    }
    
    private func setUpSessionTime(session: ViewSessionViewData) -> String {
        if checkIfSesionIsStarted(withIsStart: session.is_start) == true {
            if session.gathering_type == Constants.GatheringType.CONTINUOUS || session.gathering_type == Constants.GatheringType.INTERVAL {
                return computeElapsedTime(withDurationType: session.duration_type, duration: session.duration)
            }
        } else {
            return "Not Yet Started"
        }
        
        return "Not Yet Implemented"
    }
    
    private func setUpStatus(status: Int) {
        switch status {
        case 0:
            self.sessionStatus.text = TranslationConstants.END_SESSION.LOCALIZED
            self.sessionStatus.textColor = .red
        case 1:
            self.sessionStatus.text = "In Progress"
        case 2:
            self.sessionStatus.text = TranslationConstants.PAUSED_SESSION.LOCALIZED
            self.sessionStatus.textColor = .orange
        default: break
        }
    }
    
    private func setUpDescription(session: ViewSessionViewData) {
        sessionDescription.text = session.sessionDesc
        sessionDescription.lineBreakMode = .byWordWrapping
        sessionDescription.sizeToFit()
        sessionDescriptionLabelHeight.constant = sessionDescription.fs_height
    }
    
    private func setUpUI(session: ViewSessionViewData) {
        self.sessionName.text = session.sessionName
        self.sessionCompany.text = session.company_name
        self.setUpDescription(session: session)
        self.setUpStatus(status: session.status)
        self.setUpSessionIcon(session: session)
        self.setUpSessionStart(session: session)
        self.setUpButton(session: session)
    }
    
    func getViewSession(session: ViewSessionViewData) {
        viewedSssionDataToDisplay = session
        setUpUI(session: session)
        print("test viewed session: \(session)")
    }
    
    func setEmptyViewSession() {
        emptyView.isHidden = false
    }
    
    func startLoading() {
        emptyView.isHidden = false
    }
    
    func finishLoading() {
        emptyView.isHidden = true
        joinButton.animate()
    }
}

// MARK: Helper to Compute Remaining Time in View Session

extension ViewSessionInsight {
   
    private func checkIfSesionIsStarted(withIsStart: Int) -> Bool {
        if withIsStart == 1 {
            return true
        }
        return false
    }
    
    private func computeElapsedTime(withDurationType: Int, duration: Int) -> String {
        switch withDurationType {
        case 0:
            if duration == 0 {
                return "End"
            }
            return "\(duration) Minutes"
        case 1:
            return "\(duration) Hours"
        case 2:
            return "\(duration) Days"
        case 3:
            return "\(duration) Months"
        default: break
        }
        
        return "None"
    }
}
