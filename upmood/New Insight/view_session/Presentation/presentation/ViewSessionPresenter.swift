//
//  LoginPresenter.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 17/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

// MARK: - Setup View Data

struct ViewSessionViewData {
    let id: Int
    let sessionName: String
    let sessionDesc: String
    let status: Int
    let company_name: String
    let session_start: String
    let user_status: Int
    let session_privacy: Int
    let session_type: String
    var gathering_type: String
    var session_end: String
    var duration: Int
    var duration_type: Int
    var is_start: Int
    var joined_date: String
    var passcode: Int
    var created_at: String
}

struct JoinedPublicFitnessSessionViewData {
    let id: String
    let user_id: Int
    let session_name: String
    let session_end: String
    let age: String
    let weight: String
    let sex: String
}

struct JoinedPrivateFitnessSessionViewData {
    let id: String
    let user_id: Int
    let session_name: String
    let session_end: String
    let age: String
    let weight: String
    let sex: String
    let passcode: String
}

struct JoinedViewedSessionViewData {
    let id: String
    let user_id: Int
    let session_name: String
    let session_end: String
}

struct LeaveViewedSessionViewData {
    let id: Int
    let passcode: String
    let user_id: String
    let session_name: String
}

// MARK: - Setup View with Object Protocol

protocol ViewSessionView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func getViewSession(session: ViewSessionViewData)
    func getJoinedViewedSession(sessionData: JoinedViewedSessionViewData)
    func getLeaveViewedSession(sessionData: LeaveViewedSessionViewData)
    func getJoinedPublicFitnessSession(sessionData: JoinedPublicFitnessSessionViewData)
    func getJoinedPrivateFitnessSession(sessionData: JoinedPrivateFitnessSessionViewData)
    func setEmptyViewSession()
    func getError(withErrorMessage: String)
}

// MARK: - Setup Presenter

class ViewSessionPresenter {
    
    private let viewSessionService: ViewSessionServices
    weak private var viewSessionView : ViewSessionView?
    
    init(viewSessionService: ViewSessionServices) {
        self.viewSessionService = viewSessionService
    }
    
    func attachView(view: ViewSessionView) {
        viewSessionView = view
    }
    
    func detachView() {
        viewSessionView = nil
    }
    
    func getViewSession(withId: Int, timeZone: String) {
        self.viewSessionView?.startLoading()
        
        viewSessionService.callAPIViewSession(id: withId, timeZone: timeZone, onSuccess: { (session) in
            self.viewSessionView?.finishLoading()
            
            if let sessionName = session.session_name {
                if sessionName.isEmpty {
                    self.viewSessionView?.setEmptyViewSession()
                } else {
                    let mappedSession = ViewSessionViewData(id: session.id ?? 0, sessionName: session.session_name ?? "", sessionDesc: session.description ?? "", status: session.status ?? 0, company_name: session.company_name ?? "", session_start: "\(session.session_start ?? "")", user_status: session.user_status ?? 0, session_privacy: session.session_privacy ?? 0, session_type: session.session_type ?? "", gathering_type: session.gathering_type ?? "", session_end: session.session_end ?? "", duration: session.duration ?? 0, duration_type: session.duration_type ?? 0, is_start: session.is_start ?? 0, joined_date: "\(session.joined_date ?? "")", passcode: session.passcode ?? 0, created_at: session.created_at ?? "")
                    self.viewSessionView?.getViewSession(session: mappedSession)
                }
            }
            
        }) { (errorMessage) in
             self.viewSessionView?.getError(withErrorMessage: errorMessage)
             self.viewSessionView?.finishLoading()
        }
    }
    
    func getJoinedViewedSessionData(withId: Int, withPassCode: String? = nil) {
        self.viewSessionView?.startLoading()
        
        if withPassCode == nil {
            // join public session without passcode
            viewSessionService.callAPIJoinViewedSession(onSuccess: { (session) in
                
                self.viewSessionView?.finishLoading()
                
                if let sessionName = session.session_name {
                    if sessionName.isEmpty {
                         debugPrint("can't mapped Empty response")
                    } else {
                        let mappedSession = JoinedViewedSessionViewData(id: "\(session.id ?? "")", user_id: session.user_id ?? 0, session_name: "\(session.session_name ?? "")", session_end: "\(session.session_end ?? "")")
                        self.viewSessionView?.getJoinedViewedSession(sessionData: mappedSession)
                    }
                }
                
            }, onFailure: { (errorMessage) in
                debugPrint(errorMessage)
                self.viewSessionView?.getError(withErrorMessage: errorMessage)
                self.viewSessionView?.finishLoading()
            }, id: withId)
        } else {
            // join private session with passcode
            viewSessionService.callAPIJoinViewedSession(onSuccess: { (session) in
                
                self.viewSessionView?.finishLoading()
                
                if let sessionName = session.session_name {
                    if sessionName.isEmpty {
                         debugPrint("can't mapped Empty response")
                    } else {
                        let mappedSession = JoinedViewedSessionViewData(id: "\(session.id ?? "")", user_id: session.user_id ?? 0, session_name: "\(session.session_name ?? "")", session_end: "\(session.session_end ?? "")")
                        self.viewSessionView?.getJoinedViewedSession(sessionData: mappedSession)
                    }
                }
                
            }, onFailure: { (errorMessage) in
                debugPrint(errorMessage)
                self.viewSessionView?.getError(withErrorMessage: errorMessage)
                self.viewSessionView?.finishLoading()
            }, id: withId, withPassCode: withPassCode ?? "")
        }
    }
    
    func getLeaveViewedSessionData(withId: Int, withPassCode: String? = nil) {
        self.viewSessionView?.startLoading()

        if withPassCode == nil {
            viewSessionService.callAPILeaveViewedSession(onSuccess: { (session) in
                
                self.viewSessionView?.finishLoading()
                
                if let sessionName = session.session_name {
                    if sessionName.isEmpty {
                         debugPrint("can't mapped Empty response")
                    } else {
                       let mappedSession = LeaveViewedSessionViewData(id: session.id ?? 0, passcode: session.passcode ?? "", user_id: session.user_id ?? "", session_name: session.session_name ?? "")
                        self.viewSessionView?.getLeaveViewedSession(sessionData: mappedSession)
                    }
                }
                
            }, onFailure: { (errorMessage) in
                debugPrint(errorMessage)
                self.viewSessionView?.getError(withErrorMessage: errorMessage)
                self.viewSessionView?.finishLoading()
            }, id: withId)
        } else {
            viewSessionService.callAPILeaveViewedSession(onSuccess: { (session) in
                
                self.viewSessionView?.finishLoading()
                
                if let sessionName = session.session_name {
                    if sessionName.isEmpty {
                         debugPrint("can't mapped Empty response")
                    } else {
                       let mappedSession = LeaveViewedSessionViewData(id: session.id ?? 0, passcode: session.passcode ?? "", user_id: session.user_id ?? "", session_name: session.session_name ?? "")
                        self.viewSessionView?.getLeaveViewedSession(sessionData: mappedSession)
                    }
                }
                
            }, onFailure: { (errorMessage) in
                debugPrint(errorMessage)
                self.viewSessionView?.getError(withErrorMessage: errorMessage)
                self.viewSessionView?.finishLoading()
            }, id: withId, withPassCode: withPassCode ?? "")
        }
     }
    
    func getJoinedPublicFitnessSessionData(withId: Int, age: Int, weight: Int, sex: String) {
         self.viewSessionView?.startLoading()
  
         // join public session without passcode
        viewSessionService.callAPIJoinPublicFitnessSession(onSuccess: { (session) in
            self.viewSessionView?.finishLoading()
            
            if let sessionName = session.session_name {
                if sessionName.isEmpty {
                    debugPrint("can't mapped Empty response")
                } else {
                    let mappedSession = JoinedPublicFitnessSessionViewData(id: "\(session.id ?? "")", user_id: session.user_id ?? 0, session_name: "\(session.session_name ?? "")", session_end: "\(session.session_end ?? "")", age: "\(session.age ?? "")", weight: "\(session.weight ?? "")", sex: "\(session.sex ?? "")")
                    self.viewSessionView?.getJoinedPublicFitnessSession(sessionData: mappedSession)
                    
                }
            }
            
        }, onFailure: { (errorMessage) in
            debugPrint(errorMessage)
            self.viewSessionView?.getError(withErrorMessage: errorMessage)
            self.viewSessionView?.finishLoading()
        }, id: withId, age: age, weight: weight, sex: sex)
     }
    
    func getJoinedPrivateFitnessSessionData(withId: Int, age: Int, weight: Int, sex: String, passcode: String) {
           self.viewSessionView?.startLoading()
        
           // join public session without passcode
          viewSessionService.callAPIJoinPrivateFitnessSession(onSuccess: { (session) in
              self.viewSessionView?.finishLoading()
              
              if let sessionName = session.session_name {
                  if sessionName.isEmpty {
                      debugPrint("can't mapped Empty response")
                  } else {
                 
                    let mappedSession = JoinedPrivateFitnessSessionViewData(id: "\(session.id ?? "")", user_id: session.user_id ?? 0, session_name: "\(session.session_name ?? "")", session_end: "\(session.session_end ?? "")", age: "\(session.age ?? "")", weight: "\(session.weight ?? "")", sex: "\(session.sex ?? "")", passcode: "\(session.passcode ?? "")")
                      self.viewSessionView?.getJoinedPrivateFitnessSession(sessionData: mappedSession)
                  }
              }
              
          }, onFailure: { (errorMessage) in
              debugPrint(errorMessage)
              self.viewSessionView?.getError(withErrorMessage: errorMessage)
              self.viewSessionView?.finishLoading()
          }, id: withId, age: age, weight: weight, sex: sex, passcode: passcode)
    }
}
