//
//  APIService.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 20/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

class ViewSessionServices {
    public func callAPIViewSession(id: Int, timeZone: String,
                             onSuccess successCallback: ((_ session: ViewSessionModel) -> Void)?,
                             onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        ViewSessionAPIManager.instance.callAPIViewSession (
            id: id, timeZone: timeZone, onSuccess: { (session) in
                successCallback?(session)
            },
            onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
            }
        )
    }
    
    public func callAPIJoinViewedSession(onSuccess successCallback: ((_ session: JoinSessionModel) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int, withPassCode: String? = nil) {
        
        if withPassCode == nil {
            ViewSessionAPIManager.instance.callAPIJoinViewedSession(onSuccess: { (session) in
                successCallback?(session)
            }, onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
            }, id: id)
        } else {
            ViewSessionAPIManager.instance.callAPIJoinViewedSession(onSuccess: { (session) in
                successCallback?(session)
            }, onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
            }, id: id, withPassCode: withPassCode ?? "")
        }
    }
    
    public func callAPILeaveViewedSession(onSuccess successCallback: ((_ session: LeaveSessionModel) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int, withPassCode: String? = nil) {
        
        if withPassCode == nil {
            ViewSessionAPIManager.instance.callAPILeaveViewedSession(onSuccess: { (session) in
                successCallback?(session)
            }, onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
            }, id: id)
        } else {
            ViewSessionAPIManager.instance.callAPILeaveViewedSession(onSuccess: { (session) in
                successCallback?(session)
            }, onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
            }, id: id, withPassCode: withPassCode ?? "")
        }
    }
    
    // public fitness
    public func callAPIJoinPublicFitnessSession(onSuccess successCallback: ((_ session: JoinPublicFitnessSesionModel) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int, age: Int, weight: Int, sex: String) {
            ViewSessionAPIManager.instance.callAPIJoinPublicFitnessSession(onSuccess: { (session) in
                successCallback?(session)
            }, onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
            }, id: id, age: age, weight: weight, sex: sex)
    }
    
    // private fitness
    public func callAPIJoinPrivateFitnessSession(onSuccess successCallback: ((_ session: JoinPrivateFitnessSesionModel) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int, age: Int, weight: Int, sex: String, passcode: String) {
            ViewSessionAPIManager.instance.callAPIJoinPrivateFitnessSession(onSuccess: { (session) in
                successCallback?(session)
            }, onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
            }, id: id, age: age, weight: weight, sex: sex, passcode: passcode)
    }
}
