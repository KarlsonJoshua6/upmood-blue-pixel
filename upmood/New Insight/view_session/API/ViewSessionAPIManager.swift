//
//  LoginAPICallManager.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 17/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import Foundation
import KeychainSwift
import SwiftyJSON

class ViewSessionAPIManager {
    
    var header: HTTPHeaders = [String:String]()
    var params: [String: Any] = [:]
    
    static let instance = ViewSessionAPIManager()
    
    enum RequestMethod {
        case post
    }
    
    enum Endpoint: String {
        case ViewSession = "/api/v4/ios/insight/user/session/view"
        case JoinSession = "/api/v4/ios/insight/user/session/join-session"
        case LeavePublicSession = "/api/v4/ios/insight/user/session/leave-session"
    }
    
    // MARK: Public Session API
    func callAPIViewSession(id: Int, timeZone: String,
                          onSuccess successCallback: ((_ session: ViewSessionModel) -> Void)?,
                          onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.ViewSession.rawValue
        
        params = ["id": id, "timezone": timeZone]
        let userToken = KeychainSwift().get("apiToken") ?? ""
        header =  ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
        // call API
        self.createRequest(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let dataDict = responseObject["data"].dictionary {
                       let mainData = ViewSessionModel.build(dataDict)
                       successCallback?(mainData)
                    }
                    
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    // MARK: - Join Session API
       func callAPIJoinViewedSession(onSuccess successCallback: ((_ session: JoinSessionModel) -> Void)?,
                                     onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int, withPassCode: String? = nil) {
           
           // Build URL
           let url = API_BASE_URL + Endpoint.JoinSession.rawValue
           var params: [String: Any] = [String:Any]()
           let userToken = KeychainSwift().get("apiToken") ?? ""
           header =  ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
            if withPassCode == nil {
                params = ["id": "\(id)"]
            } else {
                params = ["id": "\(id)", "passcode": withPassCode ?? ""]
            }
           
           // call API
           self.createRequest(
               url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
               onSuccess: {(responseObject: JSON) -> Void in
                   
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                   switch statusCode {
                   case 200:
                       // Create dictionary
                       if let mainDataDict = responseObject["data"].dictionary {
                          let mainData = JoinSessionModel.build(mainDataDict)
                          successCallback?(mainData)
                       }
                       break
                   case 204:
                       failureCallback?(message)
                       break
                   case 400...500:
                       failureCallback?(message)
                       break
                   default:
                       failureCallback?(message)
                   }
               },
               onFailure: {(errorMessage: String) -> Void in
                   failureCallback?(errorMessage)
               }
           )
       }
    
    // MARK: - Leave Session API
    func callAPILeaveViewedSession(onSuccess successCallback: ((_ session: LeaveSessionModel) -> Void)?,
                                  onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int, withPassCode: String? = nil) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.LeavePublicSession.rawValue
        var params: [String: Any] = [String:Any]()
        let userToken = KeychainSwift().get("apiToken") ?? ""
        header =  ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
        if withPassCode == nil {
            params = ["id": "\(id)"]
        } else {
            params = ["id": "\(id)", "passcode": withPassCode ?? ""]
        }
        
        // call API
        self.createRequest(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let mainDataDict = responseObject["data"].dictionary {
                       let mainData = LeaveSessionModel.build(mainDataDict)
                       successCallback?(mainData)
                    }
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    
    // MARK: - Join Public Fitness Session API
    func callAPIJoinPublicFitnessSession(onSuccess successCallback: ((_ session: JoinPublicFitnessSesionModel) -> Void)?,
                                   onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int, age: Int, weight: Int, sex: String) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.JoinSession.rawValue
        let params: [String: Any] = ["id": "\(id)", "age": age, "weight": weight, "sex": sex]
        let userToken = KeychainSwift().get("apiToken") ?? ""
        header =  ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
        // call API
        self.createRequest(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let mainDataDict = responseObject["data"].dictionary {
                       let mainData = JoinPublicFitnessSesionModel.build(mainDataDict)
                       successCallback?(mainData)
                    }
                    UserDefaults.standard.set(weight, forKey: "fetchWeight")
                    UserDefaults.standard.set(sex, forKey: "fetchUserGender")
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    // MARK: - Join Private Fitness Session API
    func callAPIJoinPrivateFitnessSession(onSuccess successCallback: ((_ session: JoinPrivateFitnessSesionModel) -> Void)?,
                                          onFailure failureCallback: ((_ errorMessage: String) -> Void)?, id: Int, age: Int, weight: Int, sex: String, passcode: String) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.JoinSession.rawValue
        let params: [String: Any] = ["id": "\(id)", "age": age, "weight": weight, "sex": sex, "passcode": passcode]
        let userToken = KeychainSwift().get("apiToken") ?? ""
        header =  ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
        // call API
        self.createRequest(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let mainDataDict = responseObject["data"].dictionary {
                       let mainData = JoinPrivateFitnessSesionModel.build(mainDataDict)
                       successCallback?(mainData)
                    }
                    UserDefaults.standard.set(weight, forKey: "fetchWeight")
                    UserDefaults.standard.set(sex, forKey: "fetchUserGender")
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    
    // MARK: Request Handler
    // Create request
    func createRequest(
        _ url: String,
        method: HTTPMethod,
        headers: [String: String],
        parameters: [String: Any],
        encoding: JSONEncoding,
        onSuccess successCallback: ((JSON) -> Void)?,
        onFailure failureCallback: ((String) -> Void)?
        ) {
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                successCallback?(json)
            case .failure(let error):
                if let callback = failureCallback {
                    // Return
                    callback(error.localizedDescription)
                }
            }
        }
    }
}
