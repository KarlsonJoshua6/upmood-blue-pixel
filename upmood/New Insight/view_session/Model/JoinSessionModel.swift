//
//  JoinSessionModel.swift
//  upmood
//
//  Created by John Paul Manoza on 11/05/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import SwiftyJSON

class JoinSessionModel {
    var id: String?
    var user_id: Int?
    var session_name: String?
    var session_end: String?
    
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: JSON]) {
        if let data = dict["id"]?.stringValue {
            self.id = data
        }
        if let data = dict["user_id"]?.intValue {
            self.user_id = data
        }
        if let data = dict["session_name"]?.stringValue {
            self.session_name = data
        }
        if let data = dict["session_end"]?.stringValue {
            self.session_end = data
        }
    }
    
    // MARK: Class Method
    class func build(_ dict: [String: JSON]) -> JoinSessionModel {
        let sessionData = JoinSessionModel()
        sessionData.loadFromDictionary(dict)
        return sessionData
    }
}
