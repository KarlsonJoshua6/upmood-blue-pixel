//
//  JoinFitnessSesionModel.swift
//  upmood
//
//  Created by John Paul Manoza on 14/05/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import SwiftyJSON

class JoinPublicFitnessSesionModel {
    var id: String?
    var user_id: Int?
    var session_name: String?
    var session_end: String?
    var age: String?
    var weight: String?
    var sex: String?
    
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: JSON]) {
        if let data = dict["id"]?.stringValue {
            self.id = data
        }
        if let data = dict["user_id"]?.intValue {
            self.user_id = data
        }
        if let data = dict["session_name"]?.stringValue {
            self.session_name = data
        }
        if let data = dict["session_end"]?.stringValue {
            self.session_end = data
        }
        if let data = dict["age"]?.stringValue {
            self.age = data
        }
        if let data = dict["weight"]?.stringValue {
            self.weight = data
        }
        if let data = dict["sex"]?.stringValue {
            self.sex = data
        }
    }
    
    // MARK: Class Method
    class func build(_ dict: [String: JSON]) -> JoinPublicFitnessSesionModel {
        let sessionData = JoinPublicFitnessSesionModel()
        sessionData.loadFromDictionary(dict)
        return sessionData
    }
}
