//
//  User.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 17/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import SwiftyJSON

class ViewSessionModel {
    var id: Int?
    var session_name: String?
    var description: String?
    var status: Int?
    var company_name: String?
    var session_start: String?
    var user_status: Int?
    var session_privacy: Int?
    var session_type: String?
    var gathering_type: String?
    var session_end: String?
    var duration: Int?
    var duration_type: Int?
    var is_start: Int?
    var joined_date: String?
    var passcode: Int?
    var created_at: String?
    
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: JSON]) {
        if let data = dict["id"]?.intValue {
            self.id = data
        }
        if let data = dict["session_name"]?.stringValue {
            self.session_name = data
        }
        if let data = dict["description"]?.stringValue {
            self.description = data
        }
        if let data = dict["status"]?.intValue {
            self.status = data
        }
        if let data = dict["company_name"]?.stringValue {
            self.company_name = data
        }
        if let data = dict["session_start"]?.stringValue {
            self.session_start = data
        }
        if let data = dict["user_status"]?.intValue {
            self.user_status = data
        }
        if let data = dict["session_privacy"]?.intValue {
            self.session_privacy = data
        }
        if let data = dict["session_type"]?.stringValue {
            self.session_type = data
        }
        if let data = dict["gathering_type"]?.stringValue {
            self.gathering_type = data
        }
        if let data = dict["session_end"]?.stringValue {
            self.session_end = data
        }
        if let data = dict["duration"]?.intValue {
            self.duration = data
        }
        if let data = dict["duration_type"]?.intValue {
            self.duration_type = data
        }
        if let data = dict["is_start"]?.intValue {
            self.is_start = data
        }
        if let data = dict["joined_date"]?.stringValue {
            self.joined_date = data
        }
        if let data = dict["passcode"]?.intValue {
            self.passcode = data
        }
        if let data = dict["created_at"]?.stringValue {
            self.created_at = data
        }
    }
    
    // MARK: Class Method
    class func build(_ dict: [String: JSON]) -> ViewSessionModel {
        let sessionData = ViewSessionModel()
        sessionData.loadFromDictionary(dict)
        return sessionData
    }
}
