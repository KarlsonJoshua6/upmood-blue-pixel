//
//  TranslationConstants.swift
//  upmood
//
//  Created by John Paul Manoza on 15/04/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

extension String {
    var LOCALIZED: String {
        return NSLocalizedString(self, comment: "")
    }
}

struct TranslationConstants {
    
    static let TAP_DEVICE_TO_CONNECT_BAND = "Tap Device to connect band"
    static let SESSION_CREATED = "Session Created"
    static let ERRORTITLE = NSLocalizedString("Error, something is wrong!", comment: "")
    static let CANCEL = NSLocalizedString("Cancel", comment: "")
    static let BPM = NSLocalizedString("BPM", comment: "")
    static let DAYS_LEFT = NSLocalizedString("days left", comment: "")
    
    static let EXCITEMENT = NSLocalizedString("Excitement", comment: "")
    static let HAPPY = NSLocalizedString("Happy", comment: "")
    static let ZEN = NSLocalizedString("Zen", comment: "")
    static let PLEASANT = NSLocalizedString("Pleasant", comment: "")
    static let CALM = NSLocalizedString("Calm", comment: "")
    static let UNPLEASANT = NSLocalizedString("Unpleasant", comment: "")
    static let CONFUSED = NSLocalizedString("Confused", comment: "")
    static let CHALLENGED = NSLocalizedString("Challenged", comment: "")
    static let TENSE = NSLocalizedString("Tense", comment: "")
    static let SAD = NSLocalizedString("Sad", comment: "")
    static let ANXIOUS = NSLocalizedString("Anxious", comment: "")
    static let LOADING = NSLocalizedString("Loading", comment: "")
    
    
    // post session
    static let WORK = NSLocalizedString("Work", comment: "")
    static let WORKING = NSLocalizedString("Working", comment: "")
    static let DESIGNING = NSLocalizedString("Designing", comment: "")
    static let MEETING = NSLocalizedString("Meeting", comment: "")
    static let ANALYZING = NSLocalizedString("Analyzing", comment: "")
    static let MONITORING = NSLocalizedString("Monitoring", comment: "")
    static let TRACKING = NSLocalizedString("Tracking", comment: "")
    static let AUDITING = NSLocalizedString("Auditing", comment: "")
    static let ORGANIZING = NSLocalizedString("Organizing", comment: "")
    static let EVALUATING = NSLocalizedString("Evaluating", comment: "")
    static let TESTING = NSLocalizedString("Testing", comment: "")
    static let THINKING = NSLocalizedString("Thinking", comment: "")
    static let PROGRAMMING = NSLocalizedString("Programming", comment: "")
    static let INTERPRETING = NSLocalizedString("Interpreting", comment: "")
    static let IDENTIFYING = NSLocalizedString("Identifying", comment: "")
    static let LEISURE = NSLocalizedString("Leisure", comment: "")
    static let PLAYING = NSLocalizedString("Playing", comment: "")
    static let EXERCISING = NSLocalizedString("Exercising", comment: "")
    static let READING = NSLocalizedString("Reading", comment: "")
    static let WATCHING = NSLocalizedString("Watching", comment: "")
    static let SHOPPING = NSLocalizedString("Shopping", comment: "")
    static let DANCING = NSLocalizedString("Dancing", comment: "")
    static let SINGING = NSLocalizedString("Singing", comment: "")
    static let PARTYING = NSLocalizedString("Partying", comment: "")
    static let GAMING = NSLocalizedString("Gaming", comment: "")
    static let LISTENING = NSLocalizedString("Listening", comment: "")
    static let CELEBRATING = NSLocalizedString("Celebrating", comment: "")
    static let SLEEPING = NSLocalizedString("Sleeping", comment: "")
    static let RESTING = NSLocalizedString("Resting", comment: "")
    static let RELAXING = NSLocalizedString("Relaxing", comment: "")
    static let MEDITATING = NSLocalizedString("Meditating", comment: "")
    static let HOME = NSLocalizedString("Home", comment: "")
    static let BAKING = NSLocalizedString("Baking", comment: "")
    static let COOKING = NSLocalizedString("Cooking", comment: "")
    static let EATING = NSLocalizedString("Eating", comment: "")
    static let CLEANING = NSLocalizedString("Cleaning", comment: "")
    static let VACATION = NSLocalizedString("Vacation", comment: "")
    static let TRAVELLING = NSLocalizedString("Travelling", comment: "")
    static let DRIVING = NSLocalizedString("Driving", comment: "")
    static let JOGGING = NSLocalizedString("Jogging", comment: "")
    static let COMMUTING = NSLocalizedString("Commuting", comment: "")
    static let VISITING = NSLocalizedString("Visiting", comment: "")
    static let RIDING = NSLocalizedString("Riding", comment: "")
    static let SCHOOL = NSLocalizedString("School", comment: "")
    static let STUDYING = NSLocalizedString("Studying", comment: "")
    static let DISCOVERING = NSLocalizedString("Discovering", comment: "")
    static let RESOLVING = NSLocalizedString("Resolving", comment: "")
    static let BUILDING = NSLocalizedString("Building", comment: "")
    static let CREATING = NSLocalizedString("Creating", comment: "")
    static let ATTENDING = NSLocalizedString("Attending", comment: "")
    static let DECORATING = NSLocalizedString("Decorating", comment: "")
    static let WRITING = NSLocalizedString("Writing", comment: "")
    static let REPORTING = NSLocalizedString("Reporting", comment: "")
    static let SHAREWHAT = NSLocalizedString("Share what you're doing", comment: "")
    
    // New Insight
    static let INSIGHT_SESSION = "Insight Session"
    static let CURRENT_SESSION = "Current Session"
    static let PUBLIC_SESISON =  "Public Session"
    static let LIST_CURRENT_SESSION = "list of your joined sessions"
    static let HISTORY = "History"
    static let JOIN = "Join"
    static let LEAVE = "Leave"
    static let RETURN = "Return"
    static let END_SESSION = "End"
    static let PAUSED_SESSION = "Paused"
    static let STATUS = "Status"
    static let AGE = "Age"
    static let GENDER = "Gender"
    static let MALE = "Male"
    static let FEMALE = "Female"
    static let PASSCODE = "Passcode"
    static let BIRTHDAY = "Birthday"
    static let SHAREYOUREMOTION = "Share your emotion by joining a session. Help creators develop an insight with you."
    
    // Login Screen
    static let LOGINASGUEST = "Login as guest"
    static let CREATEACCOUNT = "Create Account"
    
    // Create new user
    static let SIGNIN = "Sign In"
}
