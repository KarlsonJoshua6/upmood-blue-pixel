//
//  FriendMoodStreamViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 20/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SVProgressHUD
import UIKit

class FriendMoodStreamViewController: UIViewController {

    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var emoticonImageView: UIImageView!
    @IBOutlet weak var emotionLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // helper value
    var selectedDate: String!
    var friendId: Int!
    var selectedIndex = 0
    
    // controls values
    var dayInt = 0
    var monthInt = 0
    var yearInt = 0
    var maxNumberOfMonth = 0
    var month: String!
    
    // data values
    private var time = [String]()
    private var bpm = [Int]()
    private var stressLevel = [String]()
    private var emotion = [String]()
    private var moodmeter = 0
    private var emojiPath: String!
    private var id = [Int]()
    private var reaction = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = true
        displayUI()
        loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: false)
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParentViewController {
            performSegue(withIdentifier: "reload", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? FriendReactionHistoryViewController {
            itemDetails.id = id[selectedIndex]
        }
    }
    
    private func displayUI() {
        
        monthLabel.isHidden = true
        dayLabel.isHidden = true
        nextButton.isHidden = true
        previousButton.isHidden = true
        
        // trim selected day string
        
        let day = String(selectedDate!.dropFirst(8))
        let trimMonth = String(selectedDate!.dropFirst(5))
        let month = String(trimMonth.dropLast(3))
        let year = String(selectedDate!.dropLast(6))
        var displayDayOnly = Int(day) ?? 0
        
        if displayDayOnly < 10 {
            displayDayOnly = Int(day.dropFirst()) ?? 0
        }
        
        dayLabel.text = "\(displayDayOnly)"
        monthLabel.text = "\(convert(month: Int(month) ?? 0).uppercased()) \(year)"
        dayInt = Int(day) ?? 0
        monthInt = Int(month) ?? 0
        yearInt = Int(year) ?? 0
    }
    
    private func setUp(cell: FriendMoodSteamTableViewCell, indexPath: IndexPath) {
        cell.timeLabel.text = time[indexPath.row]
        cell.heartBeatLabel.text = "\(bpm[indexPath.row])"
        cell.stressLevelLabel.text = stressLevel[indexPath.row]
        cell.emotionLabel.text = emotion[indexPath.row]
        cell.reactionLabel.text = reaction.joined(separator: ",")
        cell.showReactionHistoryButton.addTarget(self, action: #selector(self.showReactionHistory(_:)), for: .touchUpInside)
        stressLevelTriangle(indicator: stressLevel[indexPath.row], cell: cell)
    }
    
    @IBAction func next(_ sender: UIButton) {
        dayInt+=1
        
        // next Button func
        if dayInt > maxNumberOfMonth {
            dayInt = 0
            monthInt+=1
            dayInt+=1
            self.dayLabel.text = "\(dayInt)"
            self.monthLabel.text = "\(convert(month: monthInt).uppercased()) \(yearInt)"
            reload()
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: String(dayInt))
        } else {
            self.dayLabel.text = "\(dayInt)"
            self.monthLabel.text = "\(convert(month: monthInt).uppercased()) \(yearInt)"
        }
        
        // API Call when Next Button is Clicked
        if monthInt < 10 && dayInt < 10  {
            let month = String(format: "%02d", monthInt)
            let day = String(format: "%02d", dayInt)
            reload()
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: month, day: day)
        } else if monthInt < 10 {
            let month = String(format: "%02d", monthInt)
            reload()
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: month, day: String(dayInt))
        } else if dayInt < 10 {
            let day = String(format: "%02d", dayInt)
            reload()
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: day)
        } else {
            reload()
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: String(dayInt))
        }
    }
    
    @IBAction func previous(_ sender: UIButton) {
        dayInt-=1
        
        if dayInt == 0 {
            monthInt-=1
            self.convertMonth(maxNumber: monthInt)
            dayInt = maxNumberOfMonth
        }
        
        self.dayLabel.text = "\(dayInt)"
        self.monthLabel.text = "\(convert(month: monthInt).uppercased()) \(String(yearInt))"
        self.loadMoodStream(dateSelected: selectedDate, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: String(dayInt))
        
        // API Call when Next Button is Clicked
        if monthInt < 10 && dayInt < 10  {
            let month = String(format: "%02d", monthInt)
            let day = String(format: "%02d", dayInt)
            reload()
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: month, day: day)
        } else if monthInt < 10 {
            let month = String(format: "%02d", monthInt)
            reload()
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: month, day: String(dayInt))
        } else if dayInt < 10 {
            
            let day = String(format: "%02d", dayInt)
            print("jhagf :\(day)")
            
            reload()
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: day)
        } else {
            reload()
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: String(dayInt))
        }
    }
}

extension FriendMoodStreamViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FriendMoodSteamTableViewCell", for: indexPath) as? FriendMoodSteamTableViewCell {
            self.setUp(cell: cell, indexPath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return time.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 227.0
    }
}

// MARK: - Network request

extension FriendMoodStreamViewController {
    private func loadMoodStream(dateSelected: String, isMoodStreamReload: Bool, year: String? = nil, month: String? = nil, day: String? = nil) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let token = KeychainSwift().get("apiToken") ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        var url = URL(string: "")
        
        if isMoodStreamReload == true {
            url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record/\(self.friendId!)?sort=date&timezone=\(localTimeZoneName)&date=\(String(describing: year!))-\(String(describing: month!))-\(String(describing: day!))")
        } else {
            url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record/\(self.friendId!)?sort=date&timezone=\(localTimeZoneName)&date=\(dateSelected)")
        }
        
        print("url to ng friendMoodStream: \(url!)")
        
        var request = URLRequest(url: url!)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                guard let json = userObject as? [String: Any] else { return  }
                guard let jsonData = json["data"] as? [String: Any] else { return }
                let statusCode = json["status"] as? Int ?? 0
                let moodMeter = jsonData["upmood_meter"] as? Int ?? 0
                let emojiFilePath = jsonData["filepath"] as? String ?? ""
                
                switch statusCode {
                case 200:
                    SVProgressHUD.dismiss(completion: {
                        let _ = jsonData["upmood_meter"] as? Int ?? 0
                        guard let recordData = jsonData["records"] as? [String: Any] else { return }
                        guard let insideRecord = recordData["data"] as? [[String: Any]] else { return }
                        
                        for record in insideRecord {
                            
                            let id = record["id"] as? Int ?? 0
                            let time = record["time_created"] as? String ?? ""
                            let bpm = record["heartbeat_count"] as? Int ?? 0
                            let stressLevel = record["stress_level"] as? String ?? ""
                            let emotion = record["emotion_value"] as? String ?? ""
                            self.id.append(id)
                            self.time.append(time)
                            self.bpm.append(bpm)
                            self.stressLevel.append(stressLevel.capitalized)
                            self.emotion.append(emotion.capitalized)
                            
                            if let reactions = record["reactions"] as? [[String: Any]]  {
                                for fileNameOfEmoji in reactions {
                                    let emojiReaction = fileNameOfEmoji["filename"] as? String ?? ""
                                    self.reaction.append(emojiReaction)
                                } // end of sub loop
                            }
                            
                        } // end of main loop
                        
                        DispatchQueue.main.async { [weak this = self] in
                            this?.unhideUI()
                            
                            // display mood and emoji
                            if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(emojiFilePath.replacingOccurrences(of: " ", with: "%20"))") {
                                this?.emoticonImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_fourth_sticker"), options: [], completed: nil)
                            }
                            
                            this?.emotionLabel.text = self.displayEmoji(numberOf: moodMeter)
                            
                            // reload
                            this?.tableView.reloadData()
                        }
                    })
                    break
                case 204:
                    SVProgressHUD.dismiss(completion: {
                        self.unhideUI()
                        self.tableView.reloadData()
                    })
                    break
                case 401:
                    SVProgressHUD.dismiss(completion: {
                        self.unhideUI()
                        self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                    break
                    
                default:
                    SVProgressHUD.dismiss(completion: {
                        self.unhideUI()
                        self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error?.localizedDescription)
                })
                    
        }}}.resume()
    }
}

// helper functions
extension FriendMoodStreamViewController {
    
    @objc fileprivate func showReactionHistory(_ sender: UIButton) {
        if let indexPath = self.tableView.indexPathForView(sender) {
            selectedIndex = indexPath.row
            performSegue(withIdentifier: "showReactionHistory", sender: self)
        }
    }
    
    fileprivate func unhideUI() {
        self.monthLabel.isHidden = false
        self.dayLabel.isHidden = false
        self.nextButton.isHidden = false
        self.previousButton.isHidden = false
        self.tableView.isHidden = false
        self.emoticonImageView.image = UIImage(named: "ic_fourth_sticker")
        self.emotionLabel.text = "Calm"
    }
    
    private func stressLevelTriangle(indicator: String, cell: FriendMoodSteamTableViewCell) {
        if indicator == "low" {
            cell.triangleIndicator.image = UIImage(named: "ic_triangle_blue")
        } else if indicator == "Mild" {
            cell.triangleIndicator.image = UIImage(named: "ic_triangle_green")
        } else if indicator == "Moderate" {
            cell.triangleIndicator.image = UIImage(named: "ic_triangle_yellow")
        } else if indicator == "Severe" {
            cell.triangleIndicator.image = UIImage(named: "ic_triangle_red")
        } else {
            cell.triangleIndicator.image = UIImage(named: "ic_triangle_blue")
        }
    }
    
    private func reload() {
        self.time.removeAll()
        self.bpm.removeAll()
        self.stressLevel.removeAll()
        self.emotion.removeAll()
        self.id.removeAll()
        self.tableView.reloadData()
    }
    
    fileprivate func convertMonth(maxNumber: Int) {
        switch maxNumber {
        case 1:
            maxNumberOfMonth = 31
        case 2:
            maxNumberOfMonth = 28
        case 3:
            maxNumberOfMonth = 31
        case 4:
            maxNumberOfMonth = 30
        case 5:
            maxNumberOfMonth = 31
        case 6:
            maxNumberOfMonth = 30
        case 7:
            maxNumberOfMonth = 31
        case 8:
            maxNumberOfMonth = 31
        case 9:
            maxNumberOfMonth = 30
        case 10:
            maxNumberOfMonth = 31
        case 11:
            maxNumberOfMonth = 30
        case 12:
            maxNumberOfMonth = 31
        default:
            break
        }
    }
    
    fileprivate func convert(month: Int) -> String {
        switch month {
        case 1:
            maxNumberOfMonth = 31
            return NSLocalizedString("January", comment: "")
        case 2:
            maxNumberOfMonth = 28
            return NSLocalizedString("February", comment: "")
        case 3:
            maxNumberOfMonth = 31
            return NSLocalizedString("March", comment: "")
        case 4:
            maxNumberOfMonth = 30
            return NSLocalizedString("April", comment: "")
        case 5:
            maxNumberOfMonth = 31
            return NSLocalizedString("May", comment: "")
        case 6:
            maxNumberOfMonth = 30
            return NSLocalizedString("June", comment: "")
        case 7:
            maxNumberOfMonth = 31
            return NSLocalizedString("July", comment: "")
        case 8:
            maxNumberOfMonth = 31
            return NSLocalizedString("August", comment: "")
        case 9:
            maxNumberOfMonth = 30
            return NSLocalizedString("September", comment: "")
        case 10:
            maxNumberOfMonth = 31
            return NSLocalizedString("October", comment: "")
        case 11:
            maxNumberOfMonth = 30
            return NSLocalizedString("November", comment: "")
        case 12:
            maxNumberOfMonth = 31
            return NSLocalizedString("December", comment: "")
        default:
            break;
        }
        
        return NSLocalizedString("January", comment: "")
    }
    
    fileprivate func displayEmoji(numberOf: Int) -> String {
        switch numberOf {
        case 0:
            return "Sad"
        case 1:
            return "Mid-Sad"
        case 2:
            return "Unpleasant"
        case 3:
            return "Mid-Unpleasant"
        case 4:
            return "Calm"
        case 5:
            return "Mid-Pleasant"
        case 6:
            return "Pleasant"
        case 7:
            return "Mid-Happy"
        case 8:
            return "Happy"
        default: return "Calm"
        }
    }
}
