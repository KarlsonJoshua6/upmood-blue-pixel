//
//  SearchFriendTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 21/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class SearchFriendTableViewCell: UITableViewCell {

    @IBOutlet weak var noUserLabel: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var friendUsernameLabel: UILabel!
    @IBOutlet weak var friendProfilePhotoImageView: ClipToBoundsImageView!
    @IBOutlet weak var addFriendBtutton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
