
//
//  FriendDashboardViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 19/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import CLWaterWaveView
import KeychainSwift
import FSCalendar
import GradientProgressBar
import SDWebImage
import SVProgressHUD
import UIKit

class FriendDashboardViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var calendar: FSCalendar!
    
    // helper IBOutlet
    @IBOutlet weak var backWave: CLWaterWaveView!
    @IBOutlet weak var midWave: CLWaterWaveView!
    @IBOutlet weak var frontWave: CLWaterWaveView!
    @IBOutlet weak var gradientView: GradientProgressBar!
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    // primary IBOutlets
    @IBOutlet weak var sendReaction: UIButton!
    @IBOutlet weak var friendProfilePhotoImageView: ClipToBoundsImageView!
    @IBOutlet weak var friendUsernameLabel: UILabel!
    @IBOutlet weak var friendStatusPostLabel: UILabel!
    
    // secondary IBOutlets
    @IBOutlet weak var stressLevelIndicator: UIImageView!
    @IBOutlet weak var friendEmotionImageView: ClipToBoundsImageView!
    @IBOutlet weak var bpmLabel: UILabel!
    @IBOutlet weak var emotionValueLabel: UILabel!
    
    // mood meter indicators
    @IBOutlet weak var firstMeterIndicator: UIImageView!
    @IBOutlet weak var secondMeterIndicator: UIImageView!
    @IBOutlet weak var thirdMeterIndicator: UIImageView!
    @IBOutlet weak var fourthMeterIndicator: UIImageView!
    @IBOutlet weak var fifthMeterIndicator: UIImageView!
    @IBOutlet weak var sixthMeterIndicator: UIImageView!
    @IBOutlet weak var seventhMeterIndicator: UIImageView!
    @IBOutlet weak var eighthMeterIndicator: UIImageView!
    @IBOutlet weak var ninthMeterIndicator: UIImageView!
    @IBOutlet weak var onlineIndicator: UIImageView!
    
    // helper value
    var friendId: Int!
    var selectedDate: String!
    var bpm: String!
    var bpmIndicatorImageStr: String!
    var emotionValue: String!
    var emotionValueImageStr: String!
    var profilePhoto: String!
    var username: String!
    var postId: Int!
    var recordId: Int!
    var connectionStatus: String!
    var post_status: String!
    
    var didClickSearchResult: Bool!
    
    //privacy_settings
    var privacy_settings_emotion: Int!
    var privacy_settings_heartbeat: Int!
    var privacy_settings_stress_level: Int!
    
    // data
    private var finalDate = [String]()
    private var calendar_date = [String]()
    private var calendar_month = [String]()
    private var upmood_meter = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // calendar Delegate
        self.calendar.dataSource = self
        self.calendar.delegate   = self
        self.calendar.today = nil
        self.calendar.appearance.eventDefaultColor = UIColor.orange
        self.calendar.appearance.selectionColor = UIColor.white
        
        // calendar locale
        let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
        if selectedLanguage == "en" {
            self.calendar.locale = Locale(identifier: "en_US")
        } else if selectedLanguage == "ja" {
            self.calendar.locale = Locale(identifier: "ja")
        } else {
            //
        }
        
        // main view
        mainView.isHidden = true
        
        // progress view
        gradientView.gradientColorList = [hexStringToUIColor(hex: "#CA4F51"), hexStringToUIColor(hex: "#EDA05A"), hexStringToUIColor(hex: "#EDA05A"), hexStringToUIColor(hex: "#3DBFCD")]
        gradientView.setProgress(1.0, animated: true)
        
        // water waves
        frontWave.amplitude = 33
        frontWave.speed = 0.020
        frontWave.angularVelocity = 0.31
        frontWave.depth = 0.7
        frontWave.startAnimation()
        
        midWave.amplitude = 35
        midWave.speed = 0.030
        midWave.angularVelocity = 0.28
        midWave.depth = 0.7
        midWave.startAnimation()

        // friend profile
        loadFriendProfile(friendId: friendId)
        
        // Calendar And Mood Meter with API Calls
        hideAllIndicators()
        displayUpmoodMeter()
        setUpCalendarEvents(calendar: self.calendar)
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? FriendMoodStreamViewController {
            itemDetails.selectedDate = selectedDate
            itemDetails.friendId = friendId
        } else if let itemDetails2 = segue.destination as? SendReactionViewController {
            itemDetails2.bpm = bpmLabel.text!
            itemDetails2.bpmIndicatorImageStr = bpmIndicatorImageStr
            itemDetails2.emotionValue = emotionValueLabel.text!
            itemDetails2.emotionValueImageStr = emotionValueImageStr
            itemDetails2.profilePhoto = profilePhoto
            itemDetails2.username = username
            itemDetails2.recordId = recordId
            itemDetails2.friendId = friendId
            itemDetails2.postId = postId
            itemDetails2.profPost = post_status
        }
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    //    private func displayPrimaryUI(withFriendName: String, friendProfilePic: String, friendStatusPost: String, isOnline: Int) {
//
//        if isOnline == 0 {
//
//            // cant send reaction if offline
//            self.sendReaction.backgroundColor = UIColor.gray
//            self.sendReaction.isEnabled = false
//            self.sendReaction.setTitleColor(UIColor.white, for: .normal)
//
//            //
//            self.emotionValueLabel.text = "Inactive"
//
//        } else if privacy_settings_emotion == 0 {
//            self.sendReaction.backgroundColor = UIColor.gray
//            self.sendReaction.isEnabled = false
//            self.sendReaction.setTitleColor(UIColor.white, for: .normal)
//        }
//
//        self.friendUsernameLabel.text = withFriendName
//        self.friendStatusPostLabel.text = friendStatusPost
//        guard let url = URL(string: friendProfilePic) else { return }
//        self.friendProfilePhotoImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], completed: nil)
//
//        // setup data to be pass to next vc
//        self.username = withFriendName
//        self.profilePhoto = friendProfilePic
//    }
    
    //* Displaying UI
    
    private func displayUI(withFriendName: String, friendProfilePic: String, friendStatusPost: String) {
        // setup data to be pass to next vc
        
        if connectionStatus ?? "" == "Friend" {
            self.sendReaction.isHidden = false
        } else if connectionStatus ?? "" == "" {
            self.sendReaction.isHidden = false
        } else {
            self.sendReaction.isHidden = true
        }
        
        self.username = withFriendName
        self.profilePhoto = friendProfilePic
        self.friendUsernameLabel.text = withFriendName
        self.friendStatusPostLabel.text = friendStatusPost
        self.post_status = friendStatusPost
        guard let url = URL(string: friendProfilePic) else { return }
        self.friendProfilePhotoImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], completed: nil)
    }
    
    private func displayUI(withBPM: Int, emotionImage: String, stressLevel: String, emotionValue: String, isOnline: Int) {
        
        self.bpmIndicatorImageStr = stressLevel
        self.emotionValueImageStr = "\(Constants.Routes.BASE_URL_RESOURCE)/\(emotionImage)"
        
        //  privacy_settings_emotion
        
        print("checkmate", privacy_settings_emotion )
        
        if privacy_settings_emotion == 0 {
            self.emotionValueLabel.text = "\(NSLocalizedString("Private", comment: ""))"
            self.friendEmotionImageView.image = UIImage(named: "protection")

            if isOnline == 0 {
                self.sendReaction.backgroundColor = UIColor.gray
                self.sendReaction.isEnabled = false
                self.sendReaction.setTitleColor(UIColor.white, for: .normal)
                self.onlineIndicator.image = UIImage(named: "ic_is_offline_indicator")
            } else {
                self.sendReaction.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                self.sendReaction.isEnabled = true
                self.sendReaction.setTitleColor(UIColor.white, for: .normal)
                self.onlineIndicator.image = UIImage(named: "ic_is_online_dot")
            }

        } else {

            // is_online

            if isOnline == 0 {
                self.emotionValueLabel.text = "\(NSLocalizedString("Inactive", comment: ""))"
                if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)/\(emotionImage.replacingOccurrences(of: " ", with: "%20"))") {
                    self.friendEmotionImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_no_emotion_value"), options: [], completed: nil)
                    self.sendReaction.backgroundColor = UIColor.gray
                    self.sendReaction.isEnabled = false
                    self.sendReaction.setTitleColor(UIColor.white, for: .normal)
                    self.onlineIndicator.image = UIImage(named: "ic_is_offline_indicator")
                }

            } else {
                self.setUpCurrentMood(emotion: emotionValue.capitalized)
                if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)/\(emotionImage.replacingOccurrences(of: " ", with: "%20"))") {
                    self.friendEmotionImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_no_emotion_value"), options: [], completed: nil)
                    self.sendReaction.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                    self.sendReaction.isEnabled = true
                    self.sendReaction.setTitleColor(UIColor.white, for: .normal)
                    self.onlineIndicator.image = UIImage(named: "ic_is_online_dot")
                }
            }
        }
        
        //  privacy_settings_stress_level
        
        if privacy_settings_stress_level == 0 {
            self.bpmLabel.isHidden = true
            self.stressLevelIndicator.image = UIImage(named: "private-friends-heart")
        } else {
            self.bpmLabel.isHidden = false
            
            if withBPM == 0 {
                self.bpmLabel.text = "No BPM"
            } else {
                self.bpmLabel.text = "\(withBPM) \(NSLocalizedString("BPM", comment: ""))"
            }
            
            self.setUpStressLevelHeart(stressLevel: stressLevel)
        }
    }
    
    private func setUpCurrentMood(emotion: String) {
        switch emotion {
           case "Excitement":
               self.emotionValueLabel.text = NSLocalizedString("Excitement", comment: "")
           case "Happy":
               self.emotionValueLabel.text = NSLocalizedString("Happy", comment: "")
           case "Zen":
               self.emotionValueLabel.text = NSLocalizedString("Zen", comment: "")
           case "Pleasant":
               self.emotionValueLabel.text = NSLocalizedString("Pleasant", comment: "")
           case "Calm":
               self.emotionValueLabel.text = NSLocalizedString("Calm", comment: "")
           case "Unpleasant":
               self.emotionValueLabel.text = NSLocalizedString("Unpleasant", comment: "")
           case "Confused":
               self.emotionValueLabel.text = NSLocalizedString("Confused", comment: "")
           case "Challenged":
               self.emotionValueLabel.text = NSLocalizedString("Challenged", comment: "")
           case "Tense":
               self.emotionValueLabel.text = NSLocalizedString("Tense", comment: "")
           case "Sad":
               self.emotionValueLabel.text = NSLocalizedString("Sad", comment: "")
           case "Anxious":
               self.emotionValueLabel.text = NSLocalizedString("Anxious", comment: "")
           case "Loading":
               self.emotionValueLabel.text = NSLocalizedString("Loading", comment: "")
        default:
            break
        }
    }
    
    private func privacy_settings(withHeartBeat: Int, emotion: Int, stress_level: Int) {
        if withHeartBeat == 0 {
            
        }
    }
    
//    private func displaySecondaryUI(withBPM: Int, emotionImage: String, stressLevel: String, emotionValue: String, isOnline: Int) {
//
//        if isOnline == 0 {
//            self.emotionValueLabel.text = "Inactive"
//        } else {
//            self.emotionValueLabel.text = emotionValue.capitalized
//        }
//
//        if didClickSearchResult == true {
//            self.sendReaction.isHidden = true
//        }
//
//        self.bpmLabel.text = "\(withBPM) BPM"
//
//        guard let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)/\(emotionImage)") else { return }
//        self.friendEmotionImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_no_emotion_value"), options: [], completed: nil)
//        self.setUpStressLevelHeart(stressLevel: stressLevel)
//
//        // setup data to be pass to next vc
//        self.bpm = "\(withBPM)"
//        self.emotionValue = emotionValue
//        self.emotionValueImageStr = "\(Constants.Routes.BASE_URL_RESOURCE)/\(emotionImage)"
//        self.bpmIndicatorImageStr = stressLevel
//    }
    
    
    // MARK: - IBActions
    @IBAction func sendReaction(_ sender: UIButton) {
        performSegue(withIdentifier: "showSendReaction", sender: self)
    }
    
    @IBAction func reload(_ sender: UIStoryboardSegue) {
        self.viewDidLoad()
        DispatchQueue.main.async { [weak this = self] in
            for date: Date in self.calendar.selectedDates {
                this?.calendar.deselect(date)
                this?.calendar.reloadData()
            }
        }
    }
}

// MARK: - Network Request Native with Query

extension FriendDashboardViewController {
    fileprivate func setUpCalendarEvents(calendar: FSCalendar) {
        let currentPageDate = calendar.currentPage
        let month = Calendar.current.component(.month, from: currentPageDate)
        let year = Calendar.current.component(.year, from: currentPageDate)
        if month < 10 {
            let monthInt = String(format: "%02d", month)
            loadCalendarEvents(year: year, month: monthInt)
        } else {
            loadCalendarEvents(year: year, month: String(month))
        }
    }
    
    private func loadCalendarEvents(year: Int, month: String) {
        let token = KeychainSwift().get("apiToken") ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record/\(self.friendId!)?sort=monthly&timezone=\(localTimeZoneName)&date=\(year)-\(month)") else { return }
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                guard let json = userObject as? [String: Any] else { return  }
                guard let jsonData = json["data"] as? [[String: Any]] else { return }
                for data in jsonData {
                    let calendarDate = data["calendar_date"] as? String ?? ""
                    let calendarMonth = data["calendar_month"] as? String ?? ""
                    let upmoodMeter = data["upmood_meter"] as? Int ?? 5
                    self.calendar_date.append(calendarDate)
                    self.calendar_month.append(calendarMonth)
                    self.upmood_meter.append(upmoodMeter)
                    
                    // SetUp Calendar Events
                    let currentPageDate = self.calendar.currentPage
                    let year = Calendar.current.component(.year, from: currentPageDate)
                    
                    let calendarMonthInt = Int(calendarMonth) ?? 0
                    let calendarDateInt = Int(calendarDate) ?? 0
                    
                    if calendarMonthInt < 10 && calendarDateInt < 10 {
                        let value = "\(String(year))-0\(String(calendarMonth))-0\(String(calendarDate))";
                        self.finalDate.append(value)
                    } else if calendarMonthInt < 10 {
                        let value = "\(String(year))-0\(String(calendarMonth))-\(String(calendarDate))";
                        self.finalDate.append(value)
                    } else if calendarDateInt < 10 {
                        let value = "\(String(year))-\(String(calendarMonth))-0\(String(calendarDate))";
                        self.finalDate.append(value)
                    } else {
                        let value = "\(String(year))-\(String(calendarMonth))-\(String(calendarDate))";
                        self.finalDate.append(value)
                    }
                }
                // end loop
                
                DispatchQueue.main.async { [weak this = self] in
                    this?.calendar.reloadData()
                }
                
        }}.resume()
    }
    
    private func loadMoodMeter(year: String, month: String, day: String) {
        let token = KeychainSwift().get("apiToken") ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record/\(self.friendId!)?sort=daily&timezone=\(localTimeZoneName)&date=\(year)-\(month)-\(day)") else { return }
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                guard let json = userObject as? [String: Any] else { return  }
                let statusCode = json["status"] as? Int ?? 0
                guard let jsonData = json["data"] as? [String: Any] else { return }
                let upmoodMeter = jsonData["upmood_meter"] as? Int ?? 0
                
                DispatchQueue.main.async { [weak this = self] in
                    if statusCode == 200 || statusCode == 204 {
                        this?.setUp(moodMeter: upmoodMeter)
                    } else {
                        //this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    }
                }
                
        }}.resume()
    }
    
    private func displayUpmoodMeter() {
        let currentPageDate = self.calendar.currentPage
        let calendarDay = Calendar.current.component(.day, from: Date())
        let calendarMonth = Calendar.current.component(.month, from: currentPageDate)
        let year = Calendar.current.component(.year, from: currentPageDate)
        
        let calendarMonthInt = Int(calendarMonth)
        let calendarDateInt = Int(calendarDay)
        
        if calendarMonthInt < 10 && calendarDateInt < 10 {
            loadMoodMeter(year: String(year), month: "0\(String(calendarMonthInt))", day: "0\(String(calendarDay))")
        } else if calendarMonthInt < 10 {
            loadMoodMeter(year: String(year), month: "0\(String(calendarMonthInt))", day: String(calendarDay))
        } else if calendarDateInt < 10 {
            loadMoodMeter(year: String(year), month: String(calendarMonthInt), day: "0\(String(calendarDay))")
        } else {
            loadMoodMeter(year: String(year), month: String(calendarMonthInt), day: String(calendarDateInt))
        }
    }
}

// MARK - Calendar Delegate

extension FriendDashboardViewController: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let dtStr = self.dateFormatter2.string(from: date)
        let indexOfZero = self.upmood_meter.indexes(of: 0)
        let indexOfOne = self.upmood_meter.indexes(of: 1)
        let indexOfTwo = self.upmood_meter.indexes(of: 2)
        let indexOfThree = self.upmood_meter.indexes(of: 3)
        let indexOfFour = self.upmood_meter.indexes(of: 4)
        
        for index0 in indexOfZero {
            if self.finalDate.indices.contains(index0) {
                if self.finalDate[index0].contains(dtStr) {
                    cell.eventIndicator.color = hexStringToUIColor(hex: Constants.Color.CALM)
                }
            } else {
                // crash
            }
        }
        
        for index1 in indexOfOne {
            if self.finalDate.indices.contains(index1) {
                if self.finalDate[index1].contains(dtStr) {
                    cell.eventIndicator.color = hexStringToUIColor(hex: Constants.Color.SAD)
                }
            } else {
                // crash
            }
        }
        
        for index2 in indexOfTwo {
            if self.finalDate.indices.contains(index2) {
                if self.finalDate[index2].contains(dtStr) {
                    cell.eventIndicator.color = hexStringToUIColor(hex: Constants.Color.HAPPY)
                }
            } else {
                // crash
            }
        }
        
        for index3 in indexOfThree {
            if self.finalDate.indices.contains(index3) {
                if self.finalDate[index3].contains(dtStr) {
                    cell.eventIndicator.color = hexStringToUIColor(hex: Constants.Color.MID_UNPLESANT)
                }
            } else {
                // crash
            }
        }
        
        for index4 in indexOfFour {
            if self.finalDate.indices.contains(index4) {
                if self.finalDate[index4].contains(dtStr) {
                    cell.eventIndicator.color = hexStringToUIColor(hex: Constants.Color.MID_PLEASANT)
                }
            } else {
                // crash
            }
        }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        DispatchQueue.main.async { [weak this = self] in
            this?.finalDate.removeAll()
            this?.upmood_meter.removeAll()
            this?.setUpCalendarEvents(calendar: calendar)
        }
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateString = self.dateFormatter2.string(from: date)
        if finalDate.contains(dateString) {
            return 1
        }
        
        return 0
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
//        let dateString = self.dateFormatter2.string(from: date)
//        self.selectedDate = dateString
//        performSegue(withIdentifier: "showMoodStream", sender: self)
        
        for date: Date in self.calendar.selectedDates {
            self.calendar.deselect(date)
            self.calendar.reloadData()
        }
    }
}

// MARK - Network request
extension FriendDashboardViewController {
    
    private func setUpStressLevelHeart(stressLevel: String) {
        switch stressLevel {
        case "Low":
            self.stressLevelIndicator.image = UIImage(named: "ic_heart_blue")
            break
        case "Mild":
            self.stressLevelIndicator.image = UIImage(named: "ic_heart_green")
            break
        case "Moderate":
            self.stressLevelIndicator.image = UIImage(named: "ic_heart_yellow")
            break
        case "Severe":
            self.stressLevelIndicator.image = UIImage(named: "ic_heart_red")
            break
        default:
            self.stressLevelIndicator.image = UIImage(named: "heart")
            break
        }
    }

    private func loadFriendProfile(friendId: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.viewFriendProfile(friendId: friendId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        guard let jsonData = JSON["data"] as? [String: Any] else { return }
                        
                        // primary
                        let profileName = jsonData["name"] as? String ?? ""
                        let profilePhoto = jsonData["image"] as? String ?? ""
                        let statusPost = jsonData["profile_post"] as? String ?? ""
                        
                        this?.displayUI(withFriendName: profileName, friendProfilePic: profilePhoto, friendStatusPost: statusPost)

                        // record secondary
                        if let record = jsonData["record"] as? [String: Any] {
                            print("record", record)
                            let is_online = jsonData["is_online"] as? Int ?? 0
                            let emotionPic = record["filepath"] as? String ?? ""
                            let bpm = record["heartbeat_count"] as? Int ?? 0
                            let stressLevel = record["stress_level"] as? String ?? ""
                            let emotionValue = record["emotion_value"] as? String ?? ""
                            
                            print("ito ang response stress_Level: \(stressLevel)")
                            print("ito ang response emotionValue: \(emotionValue)")
                            
                            this?.displayUI(withBPM: bpm, emotionImage: emotionPic, stressLevel: stressLevel.capitalized, emotionValue: emotionValue, isOnline: is_online)
                            this?.mainView.isHidden = false
                        }
                        
                        // privacy_settings
                        if let privacy_settings = jsonData["privacy_settings"] as? [String: Any]  {
                            let privacy_emotion = privacy_settings["emotion"] as? Int ?? 0
                            let privacy_heartbeat = privacy_settings["heartbeat"] as? Int ?? 0
                            let privacy_stress_level = privacy_settings["emotistress_levelon"] as? Int ?? 0
                            this?.privacy_settings(withHeartBeat: privacy_heartbeat, emotion: privacy_emotion, stress_level: privacy_stress_level)
                        }
                        
                        // record
                        if let record = jsonData["record"] as? [String: Any]  {
                            let record_id = record["id"] as? Int ?? 0
                            let post_id = record["post_id"] as? Int ?? 0
                            self.recordId = record_id
                            self.postId = post_id
                        }
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        //this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found.")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        //this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    //this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
}

// mood meter
extension FriendDashboardViewController {
    
    private func hideAllIndicators() {
        firstMeterIndicator.isHidden = true
        secondMeterIndicator.isHidden = true
        thirdMeterIndicator.isHidden = true
        fourthMeterIndicator.isHidden = true
        fifthMeterIndicator.isHidden = true
        sixthMeterIndicator.isHidden = true
        seventhMeterIndicator.isHidden = true
        eighthMeterIndicator.isHidden = true
        ninthMeterIndicator.isHidden = true
    }
    
    fileprivate func setUp(moodMeter: Int) {
        switch moodMeter {
        case 0:
            firstMeterIndicator.isHidden = false
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 1:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = false
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 2:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = false
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 3:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = false
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 4:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = false
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 5:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = false
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 6:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = false
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 7:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = false
            ninthMeterIndicator.isHidden = true
            break
        case 8:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = false
            break
        default:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = false
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        }
    }
}
