//
//  FriendDashboardNavigationViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 19/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class FriendDashboardNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        tabBarController?.tabBar.isHidden = false
    }
}
