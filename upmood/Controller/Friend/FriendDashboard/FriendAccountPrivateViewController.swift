//
//  FriendAccountPrivateViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 19/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import SDWebImage
import UIKit

class FriendAccountPrivateViewController: UIViewController {
    
    var friendUsername: String!
    var friendProfilePicStr: String!

    @IBOutlet weak var friendProfilePic: ClipToBoundsImageView!
    @IBOutlet weak var friendUserName: UILabel!
    @IBOutlet weak var friendDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        displayUI()
    }
    
    private func displayUI() {
        let name = friendUsername ?? ""
        friendUserName.text = name
        
        if UserDefaults.standard.string(forKey: "selectedLanguage") == "en" {
            friendDetails.text = "\(name)'s account is in private mode."
        } else {
            friendDetails.text = "\(name) \(NSLocalizedString("account is in private mode.", comment: ""))"
        }
        
        if let url2 = URL(string: self.friendProfilePicStr.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: ".svg", with: ".png")) {
            friendProfilePic.sd_setImage(with: url2, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], completed: nil)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
}
