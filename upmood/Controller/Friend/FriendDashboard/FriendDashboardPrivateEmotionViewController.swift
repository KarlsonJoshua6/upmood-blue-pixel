//
//  FriendDashboardPrivateEmotionViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 22/01/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import CLWaterWaveView
import SDWebImage
import SVProgressHUD
import UIKit

class FriendDashboardPrivateEmotionViewController: UIViewController {

    // wave
    @IBOutlet weak var firstWave: CLWaterWaveView!
    @IBOutlet weak var secondWave: CLWaterWaveView!
    @IBOutlet weak var thirdWave: CLWaterWaveView!
    
    @IBOutlet weak var profilePhoto: ClipToBoundsImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var emotionImage: UIImageView!
    @IBOutlet weak var emotion: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var bpm: UILabel!
    @IBOutlet weak var stressLevelImage: UIImageView!
    
    var friendId: Int!
    var privacy_settings_heartbeat: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startWave()
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func updateUI() {
        loadFriendProfile(friendId: friendId ?? 0)
        emotionImage.image = UIImage(named: "protection")
    }
    
    private func setUpStressLevelHeart(stressLevel: String) {
        switch stressLevel {
        case "Low":
            self.stressLevelImage.image = UIImage(named: "ic_heart_blue")
            break
        case "Mild":
            self.stressLevelImage.image = UIImage(named: "ic_heart_green")
            break
        case "Moderate":
            self.stressLevelImage.image = UIImage(named: "ic_heart_yellow")
            break
        case "Severe":
            self.stressLevelImage.image = UIImage(named: "ic_heart_red")
            break
        default:
            self.stressLevelImage.image = UIImage(named: "heart")
            break
        }
    }
    
    private func startWave() {
        firstWave.amplitude = 20
        firstWave.speed = 0.02
        firstWave.angularVelocity = 0.31
        firstWave.depth = 0.80
        firstWave.startAnimation()
        
        secondWave.amplitude = 20
        secondWave.speed = 0.01
        secondWave.angularVelocity = 0.28
        secondWave.depth = 0.80
        secondWave.startAnimation()
        
        thirdWave.amplitude = 20
        thirdWave.speed = 0.001
        thirdWave.angularVelocity = 0.25
        thirdWave.depth = 0.80
        thirdWave.startAnimation()
    }
}

// MARK: -- REST

extension FriendDashboardPrivateEmotionViewController {
    private func loadFriendProfile(friendId: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.viewFriendProfile(friendId: friendId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        guard let jsonData = JSON["data"] as? [String: Any] else { return }
                        
                        // primary
                        let profileName = jsonData["name"] as? String ?? ""
                        let profilePhoto = jsonData["image"] as? String ?? ""
                        let statusPost = jsonData["profile_post"] as? String ?? ""
                        self.username.text = profileName
                        self.status.text = statusPost
                        if let profilePhotoURL = URL(string: profilePhoto.replacingOccurrences(of: " ", with: "%20")) {
                            self.profilePhoto.sd_setImage(with: profilePhotoURL, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], completed: nil)
                        }
                        
                        // record secondary
                        if let record = jsonData["record"] as? [String: Any]  {
                            let emotionPic = record["filepath"] as? String ?? ""
                            let bpmInt = record["heartbeat_count"] as? Int ?? 0
                            let stressLevel = record["stress_level"] as? String ?? ""
                            self.emotion.text = "\(NSLocalizedString("Private", comment: ""))"
        
                            if self.privacy_settings_heartbeat ?? 0 == 0 {
                                self.bpm.isHidden = true
                                self.stressLevelImage.image = UIImage(named: "private-friends-heart")
                            } else {
                                self.bpm.isHidden = false
                                
                                if bpmInt == 0 {
                                    self.bpm.text = "No BPM"
                                } else {
                                    self.bpm.text = "\(bpmInt) \(NSLocalizedString("BPM", comment: ""))"
                                }
                                
                                self.setUpStressLevelHeart(stressLevel: stressLevel.capitalized)
                            }
                        }
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found.")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
}
