//
//  FriendReactionHistoryViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 20/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import CLWaterWaveView
import KeychainSwift
import SVProgressHUD
import UIKit

class FriendReactionHistoryViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var emotionImageView: UIImageView!
    @IBOutlet weak var emotionLabel: UILabel!
    @IBOutlet weak var waveView: CLWaterWaveView!
    @IBOutlet weak var secondWaveView: CLWaterWaveView!
    
    var senderId: Int!
    var id: Int!
    var alamoFireManager : SessionManager?
    
    fileprivate var userName = [String]()
    fileprivate var userProfilePhoto = [String]()
    fileprivate var reaction = [String]()
    fileprivate var reactionStickerPath = [String]()
    fileprivate var emotion_value: String!
    fileprivate var emotion_sticker_filepath: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        gradientView.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
        
        // water waves
        waveView.amplitude = 20
        waveView.speed = 0.030
        waveView.angularVelocity = 0.31
        waveView.depth = 0.9
        waveView.startAnimation()
        
        secondWaveView.amplitude = 20
        secondWaveView.speed = 0.010
        secondWaveView.angularVelocity = 0.28
        secondWaveView.depth = 0.9
        secondWaveView.startAnimation()
        
        tableView.delegate = self
        tableView.dataSource = self
        loadReactionHistory(id: self.id!)
        setUserInterfaceStyleLight(self: self)
    }

    override func viewDidDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    private func displayEmotion(emotion_sticker_filepath: String, emotion_value: String) {
        self.emotionLabel.text = self.setUpCurrentMood(emotion: emotion_value.capitalized)
        if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(emotion_sticker_filepath.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: ".svg", with: ".png"))") {
            self.emotionImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_fourth_sticker"), options: [], completed: nil)
        }
    }
    
    private func setUpCurrentMood(emotion: String) -> String {
        switch emotion {
           case "Excitement":
              return NSLocalizedString("Excitement", comment: "")
          case "Happy":
              return NSLocalizedString("Happy", comment: "")
          case "Zen":
              return NSLocalizedString("Zen", comment: "")
          case "Pleasant":
              return NSLocalizedString("Pleasant", comment: "")
          case "Calm":
              return NSLocalizedString("Calm", comment: "")
          case "Unpleasant":
              return NSLocalizedString("Unpleasant", comment: "")
          case "Confused":
              return NSLocalizedString("Confused", comment: "")
          case "Challenged":
              return NSLocalizedString("Challenged", comment: "")
          case "Tense":
              return NSLocalizedString("Tense", comment: "")
          case "Sad":
              return NSLocalizedString("Sad", comment: "")
          case "Anxious":
              return NSLocalizedString("Anxious", comment: "")
          case "Loading":
              return NSLocalizedString("Loading", comment: "")
        default:
            return ""
        }
    }
}

// MARK: - UItableViewDelegate, UItableViewDataSource

extension FriendReactionHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FriendReactionHistoryTableViewCell", for: indexPath) as? FriendReactionHistoryTableViewCell {
            self.setUpCell(cell: cell, indexPath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userName.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

// MARK: - Helper Method

extension FriendReactionHistoryViewController {
    
    private func setUpImages(cell: FriendReactionHistoryTableViewCell, indexPath: IndexPath) {
        
        if reactionStickerPath[indexPath.row].isEmpty {
            cell.userProfilePic.image = UIImage(named: "ic_no_emotion_value")
        }
        
        if userProfilePhoto[indexPath.row].isEmpty {
            cell.userProfilePic.image = UIImage(named: "ic_empty_state_profile")
        }
        
        if let url = URL(string: "\(userProfilePhoto[indexPath.row].replacingOccurrences(of: " ", with: "%20"))") {
            print("urls : \(url)")
            cell.userProfilePic.sd_setImage(with: url, completed: nil)
        }
        
        if let url2 = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(reactionStickerPath[indexPath.row].replacingOccurrences(of: ".svg", with: ".png").replacingOccurrences(of: " ", with: "%20"))") {
            cell.emotionStickerImageView.sd_setImage(with: url2, completed: nil)
        }
    }
    
    fileprivate func setUpCell(cell: FriendReactionHistoryTableViewCell, indexPath: IndexPath) {
       cell.reactionHistoryLabel.text = "\(self.userName[indexPath.row]) \(NSLocalizedString("reacted", comment: "")) \(self.reaction[indexPath.row]) \(NSLocalizedString("on your mood", comment: ""))."
        self.setUpImages(cell: cell, indexPath: indexPath)
    }
}

// MARK: - Network Request

extension FriendReactionHistoryViewController {
    
    private func loadReactionHistory(id: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        
        let userId = UserDefaults.standard.integer(forKey: "fetchUserId")
        
        print("these data: \(id), \(userId)")
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 3
        configuration.timeoutIntervalForResource = 3
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        alamoFireManager?.request(APIClient.fetchReactionHistory(id: id, userId: userId)).responseJSON { [weak this = self] response in
            
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                guard let data = jsonData["data"] as? [String: Any] else { return }
                guard let dataInside = data["data"] as? [[String: Any]] else { return }
                
                SVProgressHUD.dismiss(completion: {
                    if statusCode == 200 {
                        for jsondata in dataInside {
                            let name = jsondata["name"] as? String ?? ""
                            let profilePhoto = jsondata["image"] as? String ?? ""
                            let reaction = jsondata["reaction"] as? String ?? ""
                            let reactionStickerPath = jsondata["reaction_path"] as? String ?? ""
                            this?.userName.append(name)
                            this?.userProfilePhoto.append(profilePhoto)
                            this?.reaction.append(reaction)
                            this?.reactionStickerPath.append(reactionStickerPath)
                        }
                        
                        DispatchQueue.main.async { [weak this = self] in
                            if let record = data["records"] as? [String: Any] {
                                let emotion_value = record["emotion_value"] as? String ?? ""
                                let emotion_sticker_filepath = record["filepath"] as? String ?? ""
                                this?.displayEmotion(emotion_sticker_filepath: emotion_sticker_filepath, emotion_value: emotion_value)
                                this?.tableView.reloadData()
                            }
                        }
                        
                    } else if statusCode == 204 {
                        SVProgressHUD.dismiss(completion: {
                            this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No Record Found.")
                        })
                    } else {
                        SVProgressHUD.dismiss(completion: {
                            this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                        })
                    }
                })
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
}

