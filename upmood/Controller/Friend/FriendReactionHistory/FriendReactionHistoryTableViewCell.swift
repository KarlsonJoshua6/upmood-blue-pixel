//
//  FriendReactionHistoryTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 20/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class FriendReactionHistoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var reactionHistoryLabel: UILabel!
    @IBOutlet weak var userProfilePic: ClipToBoundsImageView!
    @IBOutlet weak var emotionStickerImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
