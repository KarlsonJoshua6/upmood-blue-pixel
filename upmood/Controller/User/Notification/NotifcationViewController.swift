//
//  NotifcationViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 11/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import CRRefresh
import KeychainSwift
import SDWebImage
import SVProgressHUD
import UIKit

class NotifcationViewController: UIViewController {

    // -- IBOutlets
    
    @IBOutlet weak var blankView: UIVisualEffectView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyState: UIView!
    
    // -- Vars
    
    private var refreshControl: UIRefreshControl!
    private var profilePic = [String]()
    private var username = [String]()
    private var emotionValueFilePath = [String]()
    private var type_id = [String]()
    private var emotionValue = [String]()
    private var limit = 20
    private var nextUrl: String!
    private var didHaveNextPage = true
    private var currentPage = 0
    private var createdDate = [String]()
    private var groupId = [Int]()
    private var selectedIndex = 0
    private var senderId = [Int]()
    private var id = [Int]()
    private var mood = [String]()
    private var emoji_path = [String]()
    private var reaction_path = [String]()
    private var record_id = [Int]()
    private var privacy_settings_emotion = [Int]()
    private var privacy_settings_heartbeat = [Int]()
    private var privacy_settings_privacy = [Int]()
    
    // -- Override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
       
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? ViewGroupViewController  {
            print("to be past - \(groupId[selectedIndex])")
            itemDetails.groupId = groupId[selectedIndex]
            itemDetails.didUserTappedOnNotificationTab = true
        } else if let itemDetails2 = segue.destination as? FriendDashboardViewController {
            itemDetails2.friendId = senderId[selectedIndex]
        } else if let itemDetails3 = segue.destination as? FriendReactionHistoryViewController {
            itemDetails3.id = record_id[selectedIndex]
            
        } else if let itemDetails3 = segue.destination as? FriendDashboardPrivateEmotionViewController {
            itemDetails3.friendId = senderId[selectedIndex]
            itemDetails3.privacy_settings_heartbeat = privacy_settings_heartbeat[selectedIndex]
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // guest
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
            DispatchQueue.main.async {
                self.blankView.backgroundColor = UIColor.black.withAlphaComponent(0.75)
                self.navigationController?.setNavigationBarHidden(true, animated: false)
                self.blankView.isHidden = false
            }
            
        } else {
            DispatchQueue.main.async {
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                self.blankView.isHidden = true
            }
        }
        
        DispatchQueue.main.async { [weak self] in
            self?.emptyState.isHidden = true
            self?.tableView.isHidden = true
            self?.reload()
            self?.loadNotification()
        }
        
        navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        NotificationCenter.default.addObserver(self, selector: #selector(NotifcationViewController.timeChangedNotification), name: NSNotification.Name.NSSystemTimeZoneDidChange, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NSSystemTimeZoneDidChange, object: nil)
    }
    
    @objc func timeChangedNotification(notification: NSNotification) {
     self.navigationController?.popToRootViewController(animated: false)
     let delegate = UIApplication.shared.delegate as? AppDelegate
     let storyboardName = "DashboardScreen"
     let storybaord = UIStoryboard(name: storyboardName, bundle: nil)
     delegate?.window?.rootViewController = storybaord.instantiateInitialViewController()
    }
    
    private func warningDialogForGuest() {
        let alert = UIAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString("You're on Guest mode. Please sign up to Upmood", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (alert) in
            self.tabBarController?.selectedIndex = 0
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // -- Methods
    
    private func setUpTimeTamp(cell: NotificationTableViewCell, indexpath: IndexPath) {
        // setting of timestamp texts
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let date = dateFormatter.date(from: createdDate[indexpath.row]) {
            cell.timeAgoLabel.text = date.timeAgoSinceDate()
        }
    }
    
    private func setUpTextLabel(cell: NotificationTableViewCell, indexpath: IndexPath) {
        switch type_id[indexpath.row] {
        case "1":
            cell.notificationMessage.text = "\(self.username[indexpath.row]) \(NSLocalizedString("sent a friend request.", comment: ""))"
            break
        case "2":
            cell.notificationMessage.text = "\(self.username[indexpath.row]) \(NSLocalizedString("approved your request.", comment: ""))"
            break
        case "3":
            cell.notificationMessage.text = "\(self.username[indexpath.row]) \(NSLocalizedString("is feeling", comment: "")) \(self.setUpCurrentMood(emotion: self.emotionValue[indexpath.row]))"
            break
        case "4":
            cell.notificationMessage.text = "\(self.username[indexpath.row]) \(NSLocalizedString("reacted", comment: "")) \(self.mood[indexpath.row]) \(NSLocalizedString("on your mood", comment: ""))."
            break
        default:
            break
        }
    }
    
    private func setUpCurrentMood(emotion: String) -> String {
          switch emotion {
          case "excitement":
              return NSLocalizedString("excitement", comment: "")
          case "happy":
              return NSLocalizedString("happy", comment: "")
          case "zen":
              return NSLocalizedString("zen", comment: "")
          case "pleasant":
              return NSLocalizedString("pleasant", comment: "")
          case "calm":
              return NSLocalizedString("calm", comment: "")
          case "unpleasant":
              return NSLocalizedString("unpleasant", comment: "")
          case "confused":
              return NSLocalizedString("confused", comment: "")
          case "challenged":
              return NSLocalizedString("challenged", comment: "")
          case "tense":
              return NSLocalizedString("tense", comment: "")
          case "sad":
              return NSLocalizedString("sad", comment: "")
          case "anxious":
              return NSLocalizedString("anxious", comment: "")
          case "loading":
              return NSLocalizedString("loading", comment: "")
          default:
              return ""
          }
      }
    
    private func setUpImages(cell: NotificationTableViewCell, indexpath: IndexPath) {
        
        if profilePic[indexpath.row].isEmpty {
            cell.notificationProfilePicImageView.image = UIImage(named: "ic_empty_state_profile")
        }
        
        if emotionValueFilePath[indexpath.row].isEmpty {
            cell.emotionValueImageView.image = UIImage(named: "ic_no_emotion_value")
        }
        
        if let url = URL(string: "\(profilePic[indexpath.row].replacingOccurrences(of: " ", with: "%20"))") {
            cell.notificationProfilePicImageView.sd_setImage(with: url, completed: nil)
        }
        
        if let url2 = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(emotionValueFilePath[indexpath.row].replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: ".svg", with: ".png"))") {
            print("checkURL", url2)
            cell.emotionValueImageView.sd_setImage(with: url2, completed: nil)
        }
    }
    
    private func setUp(cell: NotificationTableViewCell, indexpath: IndexPath) {
        
        // setting of time
        
        self.setUpTimeTamp(cell: cell, indexpath: indexpath)
        
        // setting of texts
        
        self.setUpTextLabel(cell: cell, indexpath: indexpath)
        
        // setting of images
    
        self.setUpImages(cell: cell, indexpath: indexpath)
    }
    
    private func reload() {
        record_id.removeAll()
        reaction_path.removeAll()
        emoji_path.removeAll()
        mood.removeAll()
        groupId.removeAll()
        emotionValue.removeAll()
        profilePic.removeAll()
        username.removeAll()
        emotionValueFilePath.removeAll()
        type_id.removeAll()
        privacy_settings_emotion.removeAll()
        privacy_settings_heartbeat.removeAll()
        privacy_settings_privacy.removeAll()
        createdDate.removeAll()
        tableView.reloadData()
        senderId.removeAll()
    }
}

// MARK: -- UITableViewDelegate, UITableViewDataSource

extension NotifcationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as? NotificationTableViewCell {
            setUp(cell: cell, indexpath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return username.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 79.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if self.didHaveNextPage {
            if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                currentPage+=1
                self.loadNotificationPagination(currentPage: currentPage)
            }
        
        } else {
            self.tableView.tableFooterView?.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        
        if type_id[indexPath.row] == "1" {
            performSegue(withIdentifier: "showPending", sender: self)
        } else if type_id[indexPath.row] == "2" {
            performSegue(withIdentifier: "showFriendDashboard", sender: self)
        } else if type_id[indexPath.row] == "3" {
            performSegue(withIdentifier: "showGroup", sender: self)
        } else if type_id[indexPath.row] == "4" {
            performSegue(withIdentifier: "showReactionHistory", sender: self)
            print("selected to: \(self.senderId[indexPath.row]), \(self.id[indexPath.row])")
        } else {
            
        }
        
//        if self.privacy_settings_emotion[indexPath.row] == 0 {
//            performSegue(withIdentifier: "showDashboardInPrivate", sender: self)
//        }
    }
}

// -- MARK: Netwotk Request

extension NotifcationViewController {
    
    // Notification Request With Pagination
    
    private func loadNotificationPagination(currentPage: Int) {
        
        let token = KeychainSwift().get("apiToken") ?? ""
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/notification?page=\(currentPage)") else { return }
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                guard let jsonDic = userObject as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                guard let data = jsonDic["data"] as? [String: Any] else { return }
                guard let jsondata = data["data"] as? [[String: Any]] else { return }
                
                
                
                if statusCode == 200 {
                    
                    SVProgressHUD.dismiss()
                        
                    for json in jsondata {
                        
                        if let dateCreatedAt = json["created_at"] as? String {
                            self.createdDate.append(dateCreatedAt)
                        }
                        
                        let nextPage = data["next_page_url"] as? String ?? ""
                        if nextPage.isEmpty {
                            self.didHaveNextPage = false
                        } else {
                            self.didHaveNextPage = true
                        }
                        
                        let id = json["id"] as? Int ?? 0
                        self.id.append(id)
                        
                        let senderId = json["sender_id"] as? Int ?? 0
                        self.senderId.append(senderId)
                        
                        
                        // content
                        if let content = json["content"] as? [String: Any] {
                            let groupId = content["group_id"] as? Int ?? 0
                            let profilePic = content["image_from"] as? String ?? ""
                            let name = content["name_from"] as? String ?? ""
                            let emotionValuePath = content["filepath"] as? String ?? ""
                            let type_id = content["type_id"] as? String ?? ""
                            let emotionValue = content["emotion_value"] as? String ?? ""
                            let mood = content["mood"] as? String ?? ""
                            let emoji_path = content["emoji_path"] as? String ?? ""
                            let reaction_path = content["reaction_path"] as? String ?? ""
                            let record_id = content["record_id"] as? Int ?? 0
                            self.record_id.append(record_id)
                            self.reaction_path.append(reaction_path)
                            self.emoji_path.append(emoji_path)
                            self.mood.append(mood)
                            self.groupId.append(groupId)
                            self.emotionValue.append(emotionValue)
                            self.profilePic.append(profilePic)
                            self.username.append(name)
                            self.emotionValueFilePath.append(emotionValuePath)
                            self.type_id.append(type_id)
                        }
                        
                        if let privacy_settings = json["settings"] as? [String: Any]  {
                            let privacy_settings_emotion = privacy_settings["emotion"] as? Int ?? 0
                            let privacy_settings_heartbeat = privacy_settings["heartbeat"] as? Int ?? 0
                            let privacy_settings_stress_privacy = privacy_settings["privacy"] as? Int ?? 0
                            self.privacy_settings_emotion.append(privacy_settings_emotion)
                            self.privacy_settings_heartbeat.append(privacy_settings_heartbeat)
                            self.privacy_settings_privacy.append(privacy_settings_stress_privacy)
                        }
                    }
                
                    // operations
                    DispatchQueue.main.async {
                        self.tabBarController?.tabBar.items?[3].badgeValue = nil
                        self.tableView.reloadData()
                    }
                    
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
            }}.resume()
    }
    
    // Notification Request Without Pagination
    
    private func loadNotification() {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.notificationList()).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                
                switch statusCode {
                case 200:
                    
                    this?.tableView.isHidden = true
                    SVProgressHUD.dismiss()
                    this?.tableView.cr.endHeaderRefresh()
                    
                    guard let data = jsonDic["data"] as? [String: Any] else { return }
                    guard let jsondata = data["data"] as? [[String: Any]] else { return }
                    
                    for json in jsondata {
                        let currentpage = data["current_page"] as? Int ?? 0
                        self.currentPage = currentpage
                        
                        // content
                        if let dateCreatedAt = json["created_at"] as? String {
                            self.createdDate.append(dateCreatedAt)
                        }
                        
                        let nextPage = data["next_page_url"] as? String ?? ""
                        if nextPage.isEmpty {
                            self.didHaveNextPage = false
                        } else {
                            self.didHaveNextPage = true
                        }
                        
                        let id = json["id"] as? Int ?? 0
                        self.id.append(id)
                        
                        let senderId = json["sender_id"] as? Int ?? 0
                        self.senderId.append(senderId)
                        
                        if let content = json["content"] as? [String: Any] {
                            let groupId = content["group_id"] as? Int ?? 0
                            let profilePic = content["image_from"] as? String ?? ""
                            let name = content["name_from"] as? String ?? ""
                            let emotionValuePath = content["filepath"] as? String ?? ""
                            let type_id = content["type_id"] as? String ?? ""
                            let emotionValue = content["emotion_value"] as? String ?? ""
                            let mood = content["mood"] as? String ?? ""
                            let emoji_path = content["emoji_path"] as? String ?? ""
                            let reaction_path = content["reaction_path"] as? String ?? ""
                            let record_id = content["record_id"] as? Int ?? 0
                            this?.record_id.append(record_id)
                            this?.reaction_path.append(reaction_path)
                            this?.emoji_path.append(emoji_path)
                            this?.mood.append(mood)
                            this?.groupId.append(groupId)
                            this?.emotionValue.append(emotionValue)
                            this?.profilePic.append(profilePic)
                            
                            this?.username.append(name)
                            this?.emotionValueFilePath.append(emotionValuePath)
                            this?.type_id.append(type_id)
                        }
                        
                        if let privacy_settings = json["settings"] as? [String: Any]  {
                            let privacy_settings_emotion = privacy_settings["emotion"] as? Int ?? 0
                            let privacy_settings_heartbeat = privacy_settings["heartbeat"] as? Int ?? 0
                            let privacy_settings_stress_privacy = privacy_settings["privacy"] as? Int ?? 0
                            self.privacy_settings_emotion.append(privacy_settings_emotion)
                            self.privacy_settings_heartbeat.append(privacy_settings_heartbeat)
                            self.privacy_settings_privacy.append(privacy_settings_stress_privacy)
                        }
                    }
                        
                    // operations
                    DispatchQueue.main.async {
                        this?.emptyState.isHidden = true
                        this?.tableView.isHidden = false
                        this?.tabBarController?.tabBar.items?[3].badgeValue = nil
                        this?.tableView.reloadData()
                    }
                    
                    break
                case 204:
                    SVProgressHUD.dismiss(completion: {
                        this?.emptyState.isHidden = false
                        this?.tabBarController?.tabBar.items?[3].badgeValue = nil
                        this?.tableView.reloadData()
                    })
                    break
                case 422:
                    break
                default:
                    this?.emptyState.isHidden = true
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                    break
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
}

extension UITableViewCell {

  func hideSeparator() {
    self.separatorInset = UIEdgeInsets(top: 0, left: self.bounds.size.width, bottom: 0, right: 0)
  }

  func showSeparator() {
    self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
  }
}
