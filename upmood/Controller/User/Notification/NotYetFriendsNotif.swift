//
//  NotYetFriendsNotif.swift
//  upmood
//
//  Created by Taison Digital on 14/08/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import KeychainSwift

class NotYetFriendsNotif: UIViewController {
    
    var friendUsername: String!
    var friendProfilePicStr: String!
    var notificationId: Int!
    var friendId: Int!
    
    @IBOutlet weak var dividerView: UIView!
    
    @IBOutlet weak var lblNotConnetedYet: UILabel!
    @IBOutlet weak var lblFriendsUserName: UILabel!
    @IBOutlet weak var imgFriendsPhoto: ClipToBoundsImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        displayUI()
        getDarkmode()
        print("notificationId", notificationId)
        print("friendId", friendId)

    }
    
    override func viewWillAppear(_ animated: Bool) {
           getDarkmode()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?){
        super.traitCollectionDidChange(previousTraitCollection)
           getDarkmode()
    }
    
    @IBAction func btnSend(_ sender: Any) {
           acceptPendingFriendRequest(friendId: friendId, notificationId: notificationId)
    }
    
    @IBAction func btnClose(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    private func getDarkmode(){
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .light {
                lblFriendsUserName.textColor
                    = UIColor.darkGray
                dividerView.backgroundColor = UIColor.black
                lblNotConnetedYet.textColor = UIColor.darkGray
            } else {
                lblFriendsUserName.textColor
                 = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_MOODSTREAM)
                dividerView.backgroundColor =  hexStringToUIColor(hex: Constants.Color.BACKGROUND_COLOR_SEPERATOR)
                lblNotConnetedYet.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_NOTFRIENDSYET)

            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func displayUI() {
        let name = friendUsername ?? ""
        lblFriendsUserName.text = name
        
        if let url2 = URL(string: self.friendProfilePicStr.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: ".svg", with: ".png")) {
            imgFriendsPhoto.sd_setImage(with: url2, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], completed: nil)
        }
    }
    
    private func acceptPendingFriendRequest(friendId: Int, notificationId: Int) {
            print("friendId: \(friendId), \(notificationId)")
            SVProgressHUD.show()
            SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
            Alamofire.request(APIClient.approvePendingRequestWith(friendId: friendId)).responseJSON { [weak this = self] response in
                switch response.result {
                case .success(let json):
                    guard let JSON = json as? [String: Any] else { return }
                    let statusCode = JSON["status"] as? Int ?? 0
                    
                    print("JSON Outside: \(JSON)")
                    
                    if statusCode == 200 {
                        SVProgressHUD.dismiss(completion: {
                            DispatchQueue.main.async {
                                print("resPonse 200: \(JSON)")
                                this?.markAsSeen(id: notificationId)
                                self.dismiss(animated: false, completion: nil)
                                
                            }
                            
                        })
                    } else if statusCode == 204 {
                        SVProgressHUD.dismiss(completion: {
    //                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found.")
                        })
                    } else {
                        SVProgressHUD.dismiss(completion: {
                            this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                        })
                    }
                    
                    break
                case .failure(let error):
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    })
                    break
                }
            }
        }
    
    private func markAsSeen(id: Int) {
        let token = KeychainSwift().get("apiToken") ?? ""
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/notification/seen/\(id)") else { return }
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
        if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
            DispatchQueue.main.async {
                print("response as markAsSeen: \(userObject)")
               
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
               
            }
        }}.resume()
    }

}
