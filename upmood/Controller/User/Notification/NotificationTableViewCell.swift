//
//  NotificationTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 11/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeAgoLabel: UILabel!
    @IBOutlet weak var notificationProfilePicImageView: ClipToBoundsImageView!
    @IBOutlet weak var notificationTimeLabel: UILabel!
    @IBOutlet weak var emotionValueImageView: UIImageView!
    @IBOutlet weak var notificationMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
