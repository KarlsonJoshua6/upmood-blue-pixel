//
//  OnlineGathering.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 22/01/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import UIKit

struct GlobalVariable {
    static var reachable: Bool = false
    static var didUserChangeTimeZone: Bool = false    
}

extension DashboardViewController {
    
    // MARK: - TODO
        
    func monitorNetworkConnection() {
        print("call me montitor")
        self.connectionBannerView.isHidden = true
        reachability?.whenReachable = { reachability in
             // this block triggered multiple times, thats the problem.
             GlobalVariable.reachable = true
             
             print("reachable na ako ulit")
             self.checkSavedOfflineGatheredData()
             self.checkSavedSleepModeGatheredData()
             self.connectionBannerView.isHidden = true
         }
        
        reachability?.whenUnreachable = { _ in
             GlobalVariable.reachable = false

             print("Unreachable na ako ulit")
             self.connectionBannerView.isHidden = false
         }
        
        notifyClass()
     }
     
     func checkSavedOfflineGatheredData() {
        let offlineData = UserDefaults.standard.string(forKey: "offlineCalculation") ?? ""
        if !offlineData.isEmpty {
            self.savePendingRecord()
        }
     }
    
    func checkSavedSleepModeGatheredData() {
       let sleepModeData = UserDefaults.standard.string(forKey: "sleepModeGatheredData") ?? ""
       if !sleepModeData.isEmpty {
           self.savePendingSleepModeRecord()
       }
    }
     
     func notifyClass() {
         do {
             try reachability?.startNotifier()
         } catch {
             print("Unable to start notifier")
         }
     }
    
    
    // MARK: - Handling End Of gathering
    
    /* note if the user is done with the gathering, this func process
        
        1 check if the gathering is complete
        2 check if the device has internet connection
            -- if no internet
               * -- check if user is using sleep mode
                    - false - keep the gathered data and saved to nsuserdefaults and set "isSleepMode" = false -- offline data
                    - true - keep the gathered data and saved to nsuserdefaults and set "isSleepMode" = true -- sleep mode data
            -- if with internet
               * -- check if user is using sleep mode
                    - false - send the gathered data to regular batch store API -- online data
                    - true - keep the gathered data and saved to userdefaults and set "isSleepMode" = true -- sleep mode data
    */
    
    func endOfMoodGathering() {
        if Constants.didEndCalculation == true {
            if GlobalVariable.reachable == false {
                if UserDefaults.standard.bool(forKey: "didUserIsInSleepMode") == false {
                    let emotionValue = UserDefaults.standard.string(forKey: "emotionValueString") ?? ""
                    self.saveOfflineGatheredData(isSleepMode: false)
                    NotificationCenter.default.post(name: .didUserReceiveNewEmotionInOffline, object: nil, userInfo: ["emotion_in_offline": emotionValue.capitalized])
                } else {
                    let emotionValue = UserDefaults.standard.string(forKey: "emotionValueString") ?? ""
                    self.saveOfflineGatheredData(isSleepMode: true)
                    NotificationCenter.default.post(name: .didUserReceiveNewEmotionInOffline, object: nil, userInfo: ["emotion_in_offline": emotionValue.capitalized])
                }
            } else {
                if UserDefaults.standard.bool(forKey: "didUserIsInSleepMode") == false {
                    self.sendOnlineBatchStore()
                    self.displayTodayMoodStream()
                } else {
                    let emotionValue = UserDefaults.standard.string(forKey: "emotionValueString") ?? ""
                    self.saveOfflineGatheredData(isSleepMode: true)
                    NotificationCenter.default.post(name: .didUserReceiveNewEmotionInOffline, object: nil, userInfo: ["emotion_in_offline": emotionValue.capitalized])
                }
            }
            
            Constants.didEndCalculation = false
        }
    }
    
    // MARK: - Handling Online gathering 
    
    @objc func sendOnlineBatchStore() {
        let user_id = UserDefaults.standard.integer(forKey: "fetchUserId")
        let emotionSet = UserDefaults.standard.string(forKey: "currentTheme") ?? "Regular"
        let bpm = UserDefaults.standard.integer(forKey: "integerBPM")
        let emotionValue = UserDefaults.standard.string(forKey: "emotionValueString") ?? ""
        let stressLevel = UserDefaults.standard.string(forKey: "stressLevelString") ?? ""
        let ppi = UserDefaults.standard.string(forKey: "fetchPPI") ?? ""
        let totalPPI = UserDefaults.standard.integer(forKey: "totalPPI")
        let emotionLevel = UserDefaults.standard.double(forKey: "emotionLevel")
        let stress_level_value = UserDefaults.standard.double(forKey: "stress_level_value")
        let profile_post = UserDefaults.standard.string(forKey: "profile_post") ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        self.storedRecord(heartbeat_count: bpm, stress_level: stressLevel, type: "automated", emotion_set: emotionSet.capitalized, emotion_value: emotionValue.lowercased(), emotion_level: emotionLevel, longitude: 123, latitude: 124, ppi: ppi, total_ppi: totalPPI, created_at: now, timezone: localTimeZoneName, old_emotion: self.oldEmotion, stress_level_value: stress_level_value)
    }
    
    // MARK: - Sending/Storing Online gathered data to API (REST)
    
    func storedRecord(heartbeat_count: Int, stress_level: String, type: String, emotion_set: String, emotion_value: String, emotion_level: Double, longitude: Int, latitude: Int, ppi: String, total_ppi: Int, created_at: String, timezone: String, old_emotion: String, stress_level_value: Double) {
       
       let token = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
       guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record") else { return }
       
       var parameters = ""
       
       if UserDefaults.standard.bool(forKey: "didUserIsInSleepMode") == true {
           parameters = "heartbeat_count=\(heartbeat_count)&stress_level=\(stress_level)&type=\(type)&emotion_set=\(emotion_set)&emotion_value=\(emotion_value)&emotion_level=\(emotion_level)&longitude=\(longitude)&latitude=\(latitude)&ppi=\(ppi)&total_ppi=\(total_ppi)&created_at=\(created_at)&timezone=\(timezone)&old_emotion=\(old_emotion)&stress_level_value=\(stress_level_value))"
       } else {
           parameters = "heartbeat_count=\(heartbeat_count)&stress_level=\(stress_level)&type=\(type)&emotion_set=\(emotion_set)&emotion_value=\(emotion_value)&emotion_level=\(emotion_level)&longitude=\(longitude)&latitude=\(latitude)&ppi=\(ppi)&total_ppi=\(total_ppi)&created_at=\(created_at)&timezone=\(timezone)&old_emotion=\(old_emotion)&stress_level_value=\(stress_level_value)"
       }
       
       print("ITO ANG PARAMETERS SA STORE RECORD: ---- \(parameters)")
       
       var request = URLRequest(url: url)
       request.setValue("application/json", forHTTPHeaderField: "Accept")
       request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
       request.httpMethod = "POST"
       request.httpBody = parameters.data(using: .utf8)
       
       URLSession.shared.dataTask(with: request) {(data, response, error) in
           
           if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
               
               guard let jsonDic = userObject as? [String: Any] else { return }
               let statusCode = jsonDic["status"] as? Int ?? 0
               
               switch statusCode {
               case 200:
                   
                   guard let jsonData = jsonDic["data"] as? [String: Any] else { return }
                   let filePath = jsonData["filepath"] as? String ?? ""
                   let stress_level = jsonData["stress_level"] as? String ?? ""
                   let emotionValue = jsonData["emotion_value"] as? String ?? ""
                   
                   print("RESULT NG GATHERING -- \(jsonData)")
                   
                   NotificationCenter.default.post(name: .didUserReceiveNewEmotion, object: nil, userInfo: ["emotion_image_filepath": filePath])
                   
                   DispatchQueue.main.async {
                       self.setUpStressLevelHeart(stressLevel: stress_level.capitalized)
                       self.setUpCalendarEvents(calendar: self.calendar)
                       self.oldEmotion = emotionValue
                       self.setUpTranslationMoodString(emotion: emotionValue)
                       
                       // should be in async
                       // use this in upmood color
//                       let customSchemeUrl = URL(string: "upmood://?emval=\(self.oldEmotion)")
//                       if(UIApplication.shared.canOpenURL(customSchemeUrl!)){
//                           UIApplication.shared.open(customSchemeUrl!) { (result) in
//                               if result {
//                                   // The URL was delivered successfully!
//                               }else{
//                                   // The URL was not delivered
//                               }
//                           }
//                       } else {
//                           // Receiver app not ready to receive the data
//                       }
                   }
                   
                   /*
                    
                    THIS SHOULD BE IMPLEMENTED BY RECEIVING APP

                    // Determine who sent the URL.
                    let sendingAppID = options[.sourceApplication]
                    print("source application = \(sendingAppID ?? "Unknown")")
                    
                    // Process the URL.
                    guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true),
                    let emotionPath = components.path,
                    let params = components.queryItems else {
                        print("Invalid URL or emotion path missing")
                        return false
                    }
                    
                    if let emotionValue = params.first(where: { $0.name == "emval" })?.value {
                        print("emotionPath = \(emotionPath)")
                        print("emotionValue = \(emotionValue)")
                        return true
                    } else {
                        print("emotion value missing")
                        return false
                    }
                    
                   */
                   self.resetSharedPref()
                   break
               case 204:
                   print("response ako nasa 204: \(String(describing: userObject))")
                   break
               case 404:
                   print("response ako nasa 404: \(String(describing: userObject))")
                   break
               default:
                   print("response ako nasa default: \(String(describing: userObject))")
                   break
               }
               
       }}.resume()
   }
    
    // MARK: - Handling Translation string per emotion
    
    func setUpTranslationMoodString(emotion: String) {
        NotificationCenter.default.post(name: .currentEmotion, object: nil, userInfo: ["emotion": emotion])
        switch emotion {
           case "Excitement":
               self.currentMoodLabel.text = NSLocalizedString("Excitement", comment: "")
           case "Happy":
               self.currentMoodLabel.text = NSLocalizedString("Happy", comment: "")
           case "Zen":
               self.currentMoodLabel.text = NSLocalizedString("Zen", comment: "")
           case "Pleasant":
               self.currentMoodLabel.text = NSLocalizedString("Pleasant", comment: "")
           case "Calm":
               self.currentMoodLabel.text = NSLocalizedString("Calm", comment: "")
           case "Unpleasant":
               self.currentMoodLabel.text = NSLocalizedString("Unpleasant", comment: "")
           case "Confused":
               self.currentMoodLabel.text = NSLocalizedString("Confused", comment: "")
           case "Challenged":
               self.currentMoodLabel.text = NSLocalizedString("Challenged", comment: "")
           case "Tense":
               self.currentMoodLabel.text = NSLocalizedString("Tense", comment: "")
           case "Sad":
               self.currentMoodLabel.text = NSLocalizedString("Sad", comment: "")
           case "Anxious":
               self.currentMoodLabel.text = NSLocalizedString("Anxious", comment: "")
           case "Loading":
               self.currentMoodLabel.text = NSLocalizedString("Loading", comment: "")
        default:
            break
        }
    }
}
