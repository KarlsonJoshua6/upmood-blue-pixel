//
//  DashboardMethods.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 22/01/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import CLWaterWaveView
import Firebase
import KeychainSwift
import FSCalendar
import GradientProgressBar
import FirebaseMessaging
import SVProgressHUD
import Reachability
import UICircularProgressRing
import SwiftyJSON
import UIKit

extension DashboardViewController {
    
    // MARK: - Preload User Status
    func preloadUserStatus() {
        let status = UserDefaults.standard.string(forKey: "profile_post") ?? ""
        //self.statusLabel.text = status
        let getTranslate = convertion(lang: status)
        
        let lang = UserDefaults.standard.string(forKey: "selectedLanguage")
        if lang == "ja" {
            self.statusLabel.text = NSLocalizedString(status, comment: "")
        } else {
            self.statusLabel.text = getTranslate
        }
    }
    
    // MARK: - Setup/Prepare Language
    
    func prepareLanguage() {
        if Constants.didUserLogin == true {
           // UserDefaults.standard.set("en", forKey: "selectedLanguage")
            let delegate = UIApplication.shared.delegate as? AppDelegate
            let storyboardName = "DashboardScreen"
            let storybaord = UIStoryboard(name: storyboardName, bundle: nil)
            delegate?.window?.rootViewController = storybaord.instantiateInitialViewController()
            UserDefaults.standard.set(false, forKey: "didUserChangeLanguage")
            Constants.didUserLogin = false
        } else {
            // nothing
        }
    }
    
    func setUpLanguage() {

        let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
        
        print("anu ang selected language sa setUpLanguage: \(selectedLanguage)")
        
        if selectedLanguage == "en" {
            Bundle.set(language: "en")
            self.calendar.locale = Locale(identifier: "en_US")
        } else if selectedLanguage == "ja" {
            Bundle.set(language: "ja")
            self.calendar.locale = Locale(identifier: "ja_JP")
        } else {
            //
        }
        
        if UserDefaults.standard.bool(forKey: "didUserChangeLanguage") == true {
            let storyboard = UIStoryboard.init(name: "DashboardScreen", bundle: nil)
            UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
            UserDefaults.standard.set(false, forKey: "didUserChangeLanguage")
            
            self.childViewControllers.forEach({vc in
                vc.dismiss(animated: false, completion: nil)
            })
        }
    }
    
    // MARK: - Handing NSUserDefaults
    
    func resetSharedPref() {
        UserDefaults.standard.set(0, forKey: "integerBPM")
        UserDefaults.standard.set("", forKey: "emotionValueString")
        UserDefaults.standard.set("", forKey: "stressLevelString")
        UserDefaults.standard.set("", forKey: "fetchPPI")
        UserDefaults.standard.set(0, forKey: "totalPPI")
        UserDefaults.standard.set(0.0, forKey: "emotionLevel")
//        UserDefaults.standard.set("", forKey: "offlineCalculation")
//        UserDefaults.standard.set("", forKey: "sleepModeGatheredData")
        UserDefaults.standard.set(0.0, forKey: "stress_level_value")
    }
    
    // MARK: - Handling Guest mode UI
    
    func setUpDashboardForGuest() {
        self.calendar.isHidden = true
        self.moodCalendarLabel.isHidden = true
        self.mainView.fs_height = 900
        self.heightContraintsOfCalendarLabel.constant = 0.0
        self.heightContraintsOfCalendar.constant = 0.0
    }
    
    func deInitDashboardForGuest() {
        self.calendar.isHidden = false
        self.moodCalendarLabel.isHidden = false
        self.mainView.fs_height = 1361
        self.heightContraintsOfCalendarLabel.constant = 29.0
        self.heightContraintsOfCalendar.constant = 396.0
    }
    
    // MARK: - Handling add status uibutton
    
    func setUpAddStatus(_ sender: UIButton) {
        let status = UserDefaults.standard.string(forKey: "profile_post") ?? ""
        if status.isEmpty {
            sender.setTitle(NSLocalizedString("Add Status", comment: ""), for: .normal)
        } else {
            sender.setTitle("\(NSLocalizedString("Edit Status", comment: "")) ✎", for: .normal)
        }
        
        if !upmoodBandConnected {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(initWave), userInfo: nil, repeats: false)
        } else {
            self.startWave()
        }
    }
    
    // MARK: - Handling Circle ProgressBar (with Firebase)
    
    func startProgressBar(withPercent: Int) {
        
        // for beta, live and test only
//        if !viewModel.getSessionName().isEmpty {
//            if viewModel.getUserStatus() == 1 {
//                // old firebase
//                // for beta or test
//                let totalPPI = UserDefaults.standard.integer(forKey: "totalPPI")
//                let bpm = UserDefaults.standard.integer(forKey: "integerBPM")
//
//                firebaseService.setMoodFeedProgress(sum: totalPPI, sessionId: viewModel.getSessionId(), userId: viewModel.getCurrentUserId())
//                firebaseService.setHeartBeat(heartBeatValue: String(bpm), sessionId: viewModel.getSessionId(), userId: viewModel.getCurrentUserId())
//            }
//        }
        
        self.circularProgressBar.startProgress(to: UICircularProgressRing.ProgressValue(withPercent), duration: 0)
    }
    
    func storeProgressValue() {
        let totalPPI = UserDefaults.standard.integer(forKey: "totalPPI")
        let bpm = UserDefaults.standard.integer(forKey: "integerBPM")
        
        print("data to be past: \(totalPPI), \(bpm), \(viewModel.getCurrentUserId())")
                     
         if bpm == 0 {
             // do nothing
         } else {
            
            let token = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
            let urlString = "\(Constants.Routes.BASE)/api/v4/ios/insight/user/session/progress-value?&progress_value=\(totalPPI)&bpm=\(bpm)"
            guard let url = URL(string: "\(urlString)") else { return }
            
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            request.httpMethod = "GET"
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                    
                    guard let jsonDic = userObject as? [String: Any] else { return }
                    let statusCode = jsonDic["status"] as? Int ?? 0
                    
                    if statusCode == 200 {
                        print("Sent BPM Data...... \(jsonDic)")
                    } else {
                        print("Failure on sending BPM Data...... \(jsonDic)")
                    }
                    
              }}.resume()
          }
    }
    
    // MARK: - Handling LogOut and Force LogOut
    
    func logOut() {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        
        view.isUserInteractionEnabled = false
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.logOut()).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                
                removeAllSharedPref()
                this?.performSegue(withIdentifier: "showLogOut", sender: self)
                
                switch statusCode {
                case 200:
                    SVProgressHUD.dismiss(completion: {
                        removeAllSharedPref()
                        this?.performSegue(withIdentifier: "showLogOut", sender: self)
                    })
                    break
                case 204:
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Can't Logged Out, Please check your internet connection.")
                    })
                    break
                case 422:
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "")
                    })
                    break
                default:
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "")
                    })
                    break
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                
                break
            }
        }
    }
    
    func forceLogOutFailedToFetchToken() {
        let alert = UIAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Failed to fetch device token.\nPlease relogin.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
            self.logOut()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Handling MoodMeter and Stress level UI
    
    func loadMoodMeter(year: String, month: String, day: String) {
        
        let token = KeychainSwift().get("apiToken") ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record?sort=daily&timezone=\(localTimeZoneName)&date=\(year)-\(month)-\(day)") else { return }
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                guard let json = userObject as? [String: Any] else { return  }
                let statusCode = json["status"] as? Int ?? 0
                guard let jsonData = json["data"] as? [String: Any] else { return }
                let upmoodMeter = jsonData["upmood_meter"] as? Int ?? 0
                
                print("current MoodMeter Value: - \(upmoodMeter)")
                
                if statusCode == 200 || statusCode == 204 {
                    DispatchQueue.main.async {
                        self.setUp(moodMeter: upmoodMeter)
                        self.moodMeeterDarkmode()
                    }
                } else {
                    print("failed to load mood meter")
                }
                
        }}.resume()
    }
    
    func displayTodayMoodStream() {
        let currentPageDate = self.calendar.currentPage
        let calendarDay = Calendar.current.component(.day, from: Date())
        let calendarMonth = Calendar.current.component(.month, from: currentPageDate)
        let year = Calendar.current.component(.year, from: currentPageDate)
        
        let calendarMonthInt = Int(calendarMonth)
        let calendarDateInt = Int(calendarDay)
        
        if calendarMonthInt < 10 && calendarDateInt < 10 {
            loadTodayMoodStream(year: String(year), month: "0\(String(calendarMonthInt))", day: "0\(String(calendarDay))")
        } else if calendarMonthInt < 10 {
            loadTodayMoodStream(year: String(year), month: "0\(String(calendarMonthInt))", day: String(calendarDay))
        } else if calendarDateInt < 10 {
            loadTodayMoodStream(year: String(year), month: String(calendarMonthInt), day: "0\(String(calendarDay))")
        } else {
            loadTodayMoodStream(year: String(year), month: String(calendarMonthInt), day: String(calendarDateInt))
        }
    }
    
    func loadTodayMoodStream(year: String, month: String, day: String) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let token = KeychainSwift().get("apiToken") ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record/sleep-list?date=\(String(describing: year))-\(String(describing: month))-\(String(describing: day))&timezone=\(localTimeZoneName)")
   
        print("test url: \(url!)")

        var request = URLRequest(url: url!)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
            guard let json = userObject as? [String: Any] else { return  }
            let statusCode = json["status"] as? Int ?? 0
                
                switch statusCode {
                case 200:
                    self.displayUpmoodMeter()
                case 204:
                    DispatchQueue.main.async {
                        self.hideAllMoodMeter()
                    }
                default: break
                }
                
        SVProgressHUD.dismiss()

        }}.resume()
    }
    
    func displayUpmoodMeter() {
         let currentPageDate = self.calendar.currentPage
         let calendarDay = Calendar.current.component(.day, from: Date())
         let calendarMonth = Calendar.current.component(.month, from: currentPageDate)
         let year = Calendar.current.component(.year, from: currentPageDate)
         
         let calendarMonthInt = Int(calendarMonth)
         let calendarDateInt = Int(calendarDay)
         
         if calendarMonthInt < 10 && calendarDateInt < 10 {
             loadMoodMeter(year: String(year), month: "0\(String(calendarMonthInt))", day: "0\(String(calendarDay))")
         } else if calendarMonthInt < 10 {
             loadMoodMeter(year: String(year), month: "0\(String(calendarMonthInt))", day: String(calendarDay))
         } else if calendarDateInt < 10 {
             loadMoodMeter(year: String(year), month: String(calendarMonthInt), day: "0\(String(calendarDay))")
         } else {
             loadMoodMeter(year: String(year), month: String(calendarMonthInt), day: String(calendarDateInt))
         }
     }
    
    func hideAllMoodMeter() {
        firstMeterIndicator.isHidden = true
        secondMeterIndicator.isHidden = true
        thirdMeterIndicator.isHidden = true
        fourthMeterIndicator.isHidden = true
        fifthMeterIndicator.isHidden = true
        sixthMeterIndicator.isHidden = true
        seventhMeterIndicator.isHidden = true
        eighthMeterIndicator.isHidden = true
        ninthMeterIndicator.isHidden = true
    }
    
    func setUpStressLevelHeart(stressLevel: String) {
        NotificationCenter.default.post(name: .currentStressLevel, object: nil, userInfo: ["stress_level": stressLevel])
        switch stressLevel {
        case "Low":
            self.stressLevelIndicatorImageView.image = UIImage(named: "ic_heart_blue")
            break
        case "Mild":
            self.stressLevelIndicatorImageView.image = UIImage(named: "ic_heart_green")
            break
        case "Moderate":
            self.stressLevelIndicatorImageView.image = UIImage(named: "ic_heart_yellow")
            break
        case "Severe":
            self.stressLevelIndicatorImageView.image = UIImage(named: "ic_heart_red")
            break
        default:
            self.stressLevelIndicatorImageView.image = UIImage(named: "heart")
            break
        }
    }
    
    // MARK: - Initializer for UIs such as (CLWaterWaveView, FSCalendar, GradientProgressBar)
    
     func startWave() {
        waveView.amplitude = 20
        waveView.speed = 0.02
        waveView.angularVelocity = 0.31
        waveView.depth = 0.80
        waveView.startAnimation()
        
        secondWaveView.amplitude = 20
        secondWaveView.speed = 0.01
        secondWaveView.angularVelocity = 0.28
        secondWaveView.depth = 0.80
        secondWaveView.startAnimation()
        
        thirdWaveView.amplitude = 20
        thirdWaveView.speed = 0.001
        thirdWaveView.angularVelocity = 0.25
        thirdWaveView.depth = 0.80
        thirdWaveView.startAnimation()
    }
    
    @objc func initWave() {
        // water waves
        waveView.amplitude = 33
        waveView.speed = 0.008
        waveView.angularVelocity = 0.31
        waveView.depth = 0.7
        waveView.stopAnimation()
        
        secondWaveView.amplitude = 35
        secondWaveView.speed = 0.01
        secondWaveView.angularVelocity = 0.28
        secondWaveView.depth = 0.7
        secondWaveView.stopAnimation()
        
        thirdWaveView.amplitude = 20
        thirdWaveView.speed = 0.02
        thirdWaveView.angularVelocity = 0.25
        thirdWaveView.depth = 0.5
        thirdWaveView.stopAnimation()
        self.timer.invalidate()
    }
    
    func initCalendar() {
        // calendar
        self.calendar.dataSource = self
        self.calendar.delegate   = self
        self.calendar.today = nil
        self.calendar.placeholderType = .none
        self.calendar.appearance.selectionColor = UIColor.white
    }
    
     func initProgressBar() {
        // progress view
        self.hideAllMoodMeter()
        progressView.gradientColorList = [hexStringToUIColor(hex: "#CA4F51"), hexStringToUIColor(hex: "#EDA05A"), hexStringToUIColor(hex: "#EDA05A"), hexStringToUIColor(hex: "#3DBFCD")]
        progressView.setProgress(1.0, animated: true)
        self.calendar.placeholderType = .none
        self.currentSessionView.dropShadow()
    }
    
    func initDisplayEmotionButton() {
        self.circularProgressBar.delegate = self
        self.currentCircularProgressBarValueLabel.isHidden = true
    }
    
    func initMoveButton() {
        if self.upmoodBandConnected == true {
            self.moveTo.isUserInteractionEnabled = false
        } else {
            self.moveTo.isUserInteractionEnabled = true
            self.moveTo.addTarget(self, action: #selector(self.moveToDeviceTab(_:)), for: .touchUpInside)
       }
    }
}

extension DashboardViewController {
    private func convertion(lang: String) -> String{
        switch lang {
        case ("仕事中"):
            return "Working"
        case "デザイン中":
            return "Designing"
        case "打合せ中":
            return "Meeting"
        case "分析中":
            return "Analyzing"
        case "観察中":
            return "Monitoring"
        case "追跡中":
            return "Tracking"
        case "編集中":
            return "Auditing"
        case "整理中":
            return "Organizing"
        case "評価中":
            return "Evaluating"
        case "試験中":
            return "Testing"
        case "考え中":
            return "Thinking"
        case "プログラム中":
            return "Programming"
        case "翻訳中":
            return "Interpreting"
        case "確認中":
            return "Identifying"
        case "試合中":
            return "Playing"
        case "エクササイズ中":
            return "Exercising"
        case "読書中":
            return "Reading"
        case "鑑賞中":
            return "Watching"
        case "買い物中":
            return "Shopping"
        case "ダンス":
            return "Dancing"
        case "カラオケ中":
            return "Singing"
        case "パーティー中":
            return "Partying"
        case "ゲーム中":
            return "Gaming"
        case "音楽鑑賞中":
            return "Listening"
        case "お祝い中":
            return "Celebrating"
        case "睡眠中":
            return "Sleeping"
        case "休憩中":
            return "Resting"
            break
        case "リラックス中":
            return "Relaxing"
        case "瞑想中":
            return "Meditating"
        case "おやつ作り中":
            return "Baking"
        case "料理中":
            return "Cooking"
        case "食事中":
            return "Eating"
        case "掃除中":
            return "Cleaning"
        case "旅行中":
            return "Travelling"
        case "運転中":
            return "Driving"
        case "ジョギング中":
            return "Jogging"
        case "通勤中":
            return "Commuting"
        case "訪問中":
            return "Visiting"
        case "乗車中":
            return "Riding"
        case "勉強中":
            return "Studying"
        case "発見中":
            return "Discovering"
        case "問題解決中":
            return "Resolving"
        case "建設中":
            return "Building"
        case "創造中":
            return "Creating"
        case "出席中":
            return "Attending"
        case "装飾中":
            return "Decorating"
        case "執筆中":
            return "Writing"
        case "レポート作成中":
            return "Reporting"
        default:
            return lang
        }
    }
    func moodMeeterDarkmode(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
                case .light, .unspecified:
                // light mode detected
                print("light mode detected")
                firstMeterIndicator.image = UIImage(named: "ic_mood_meter_indicator")
                secondMeterIndicator.image = UIImage(named: "ic_mood_meter_indicator")
                thirdMeterIndicator.image = UIImage(named: "ic_mood_meter_indicator")
                fourthMeterIndicator.isHidden = true
                fifthMeterIndicator.image = UIImage(named: "ic_mood_meter_indicator")
                sixthMeterIndicator.image = UIImage(named: "ic_mood_meter_indicator")
                seventhMeterIndicator.image = UIImage(named: "ic_mood_meter_indicator")
                eighthMeterIndicator.image = UIImage(named: "ic_mood_meter_indicator")
                ninthMeterIndicator.image = UIImage(named: "ic_mood_meter_indicator")

                case .dark:
                // dark mode detected
                print("dark mode detected")
                
                firstMeterIndicator.image = UIImage(named: "ic_mood_meter")
                secondMeterIndicator.image = UIImage(named: "ic_mood_meter")
                thirdMeterIndicator.image = UIImage(named: "ic_mood_meter")
                fourthMeterIndicator.image = UIImage(named: "ic_mood_meter")
                fifthMeterIndicator.image = UIImage(named: "ic_mood_meter")
                sixthMeterIndicator.image = UIImage(named: "ic_mood_meter")
                seventhMeterIndicator.image = UIImage(named: "ic_mood_meter")
                eighthMeterIndicator.image = UIImage(named: "ic_mood_meter")
                ninthMeterIndicator.image = UIImage(named: "ic_mood_meter")
                         
                firstMeterIndicator.setImageColor(color: hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
                secondMeterIndicator.setImageColor(color: hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
                thirdMeterIndicator.setImageColor(color: hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
                fourthMeterIndicator.setImageColor(color: hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
                fifthMeterIndicator.setImageColor(color: hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
                sixthMeterIndicator.setImageColor(color: hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
                seventhMeterIndicator.setImageColor(color: hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
                eighthMeterIndicator.setImageColor(color: hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
                ninthMeterIndicator.setImageColor(color: hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))

            }
        } else {
            // Fallback on earlier versions
        }       
    }
    
    func setUp(moodMeter: Int) {
        switch moodMeter {
        case 0:
            firstMeterIndicator.isHidden = false
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 1:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = false
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 2:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = false
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 3:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = false
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 4:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = false
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 5:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = false
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 6:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = false
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        case 7:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = false
            ninthMeterIndicator.isHidden = true
            break
        case 8:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = true
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = false
            break
        default:
            firstMeterIndicator.isHidden = true
            secondMeterIndicator.isHidden = true
            thirdMeterIndicator.isHidden = true
            fourthMeterIndicator.isHidden = true
            fifthMeterIndicator.isHidden = false
            sixthMeterIndicator.isHidden = true
            seventhMeterIndicator.isHidden = true
            eighthMeterIndicator.isHidden = true
            ninthMeterIndicator.isHidden = true
            break
        }
    }

}
