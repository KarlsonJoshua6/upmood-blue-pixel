//
//  DashboardViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 14/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import CLWaterWaveView
import Firebase
import KeychainSwift
import FSCalendar
import GradientProgressBar
import FirebaseMessaging
import SVProgressHUD
import Reachability
import UICircularProgressRing
import SwiftyJSON
import UIKit

var reachability = Reachability()

class DashboardViewController: UIViewController, UICircularProgressRingDelegate {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var emotionImageView: UIImageView!
    @IBOutlet weak var editorAddStatusButton: UIButton!
    @IBOutlet weak var firstMeterIndicator: UIImageView!
    @IBOutlet weak var secondMeterIndicator: UIImageView!
    @IBOutlet weak var thirdMeterIndicator: UIImageView!
    @IBOutlet weak var fourthMeterIndicator: UIImageView!
    @IBOutlet weak var fifthMeterIndicator: UIImageView!
    @IBOutlet weak var sixthMeterIndicator: UIImageView!
    @IBOutlet weak var seventhMeterIndicator: UIImageView!
    @IBOutlet weak var eighthMeterIndicator: UIImageView!
    @IBOutlet weak var ninthMeterIndicator: UIImageView!
    @IBOutlet weak var currentMoodLabel: UILabel!
    @IBOutlet var calendar: FSCalendar!
    @IBOutlet weak var progressView: GradientProgressBar!
    @IBOutlet weak var waveView: CLWaterWaveView!
    @IBOutlet weak var secondWaveView: CLWaterWaveView!
    @IBOutlet weak var thirdWaveView: CLWaterWaveView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var heartRateBpmLabel: UILabel!
    @IBOutlet weak var stressLevelIndicatorImageView: UIImageView!
    @IBOutlet weak var circularProgressBar: UICircularProgressRing!
    @IBOutlet weak var currentCircularProgressBarValueLabel: UILabel!
    @IBOutlet weak var moveTo: UIButton!
    @IBOutlet weak var currentSessionView: UIView!
    @IBOutlet weak var connectionBannerView: UIView!
    @IBOutlet weak var lblCurrentMood: UILabel!
    
    @IBOutlet weak var sleepModeView: UIView!
    @IBOutlet weak var fitnessModeView: UIView!
    
    // workspace
    
    @IBOutlet var viewModel: DisplayCurrentSessionViewModel!
    @IBOutlet weak var session_name_label: UILabel!
    @IBOutlet weak var currentSessionButton: UIButton!
    @IBOutlet weak var current_session: UILabel!
    @IBOutlet weak var imgSession: UIImageView!
    @IBOutlet weak var btnSleepMode: UIButton!
    

    @IBOutlet weak var moodCalendarLabel: UILabel!
    @IBOutlet weak var heightContraintsOfCalendarLabel: NSLayoutConstraint!
    @IBOutlet weak var heightContraintsOfCalendar: NSLayoutConstraint!
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var lblCalories: UILabel!
    @IBOutlet weak var lblSteps: UILabel!
    
    
    // new current sessions
    let presenter = CurrentSessionPresenter(currentSessionService: CurrentSessionService())
    var currentSessionToDisplay = [CurrentSessionViewData]()
    
    // MARK: - Stored Value
    
    var selectedDate: String!
    var timer = Timer()
    var timer2 = Timer()
    var oldEmotion = ""
    var calendar_date = [String]()
    var calendar_month = [String]()
    var upmood_meter = [Int]()
    var color: UIColor!
    var finalDate = [String]()
    lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    var savedJSONDataOfflineMode = [JSON]()
    var jsonStringArrOfflineMode = [String]()
    var savedJSONDataSleepMode = [JSON]()
    var jsonStringArrSleepMode = [String]()
    
    var upmoodBandConnected = false
    var didEndCalc = false
    
    var firebaseService = FirebaseService()
    var typeId = ""
    let hostNames = [nil, "google.com", "invalidhost"]
    var hostIndex = 0
    var bluetooManager = BluetoothManager.getInstance()
    var didSuccessLoadCurrentSession = false
    var seconds = 480
    

    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        
        UIApplication.shared.registerForRemoteNotifications()

        // guest
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
            self.setUpDashboardForGuest()
        } else {
            self.deInitDashboardForGuest()
        }
        
        if Constants.notifBool == true {
            self.tabBarController?.selectedIndex = 3
            
            Constants.notifBool = false
        }
        
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true

        // Initialization Methods

        startWave()
        preloadUserStatus()
        initDisplayEmotionButton()
        initProgressBar()
        getDarkMode()

        // Prio Methods

        setUpAddStatus(editorAddStatusButton)
        //setUpCalendarEvents(calendar: self.calendar)

        // etc

        circularProgressBar.shouldShowValueText = false
        prepareLanguage()
        checkBuildVersion()
        displayTodayMoodStream()
        
        sleepModeView.layer.cornerRadius = 10
        fitnessModeView.layer.cornerRadius = 10
        subscribeToFcm()
        
//        calendar.appearance.weekdayTextColor = hexStringToUIColor(hex: "#00A3AD")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        reachability?.stopNotifier()
    }
    private func getDarkMode(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
               case .light, .unspecified:
                   // light mode detected
                   print("light mode detected")
                   currentSessionView.backgroundColor = UIColor.white
                   currentSessionView.layer.borderWidth = 1
                   currentSessionView.layer.borderColor = UIColor.white.cgColor
                   imgSession.setImageColor(color: UIColor.darkGray)
                        
                   session_name_label.textColor = UIColor.darkGray
                    current_session.textColor = UIColor.darkGray
                
                   calendar.appearance.weekdayTextColor = UIColor.black
                   calendar.appearance.titleDefaultColor = UIColor.black
                   lblCurrentMood.textColor = UIColor.black
                   calendar.appearance.headerTitleColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_PRIMARY_COLOR)
                
                   progressView.layer.cornerRadius = 0

               case .dark:
                   // dark mode detected
                   print("dark mode detected")
                   currentSessionView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_SESSION_DASHBOARD)
                   currentSessionView.layer.borderWidth = 1

                   currentSessionView.layer.borderColor = UIColor.black.cgColor
                   session_name_label.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_SESSION_DASHBOARD)
                   current_session.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_SESSION_DASHBOARD)
                   imgSession.setImageColor(color: hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_SESSION_DASHBOARD))
                   calendar.appearance.weekdayTextColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_WEEKDAYS_CALENDAR)
                   calendar.appearance.titleDefaultColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_PRIMARY_COLOR)
                   lblCurrentMood.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_MOODSTREAM)
                   calendar.appearance.headerTitleColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_DATE_MOODSTREAM)
                    
                    progressView.layer.cornerRadius = 10
                progressView.layer.masksToBounds = true
                
                
               }
           } else {
               // Fallback on earlier versions
           }
    }
    
    func updateCurrentSessionUI() {
        currentSessionView.isHidden = false
        currentSessionButton.isHidden = false
        if currentSessionToDisplay.count > 1 {
            self.session_name_label.text = "\(currentSessionToDisplay.count) Sessions"
        } else {
            self.session_name_label.text = currentSessionToDisplay[0].sessionName
        }
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?)
    {
        super.traitCollectionDidChange(previousTraitCollection)
        getDarkMode()
        moodMeeterDarkmode()
    }

    override func viewWillAppear(_ animated: Bool) {
        getDarkMode()
        SVProgressHUD.dismiss()
        initCalendar()
        initObserver()
        setUpLanguage()
        initMoveButton()
        checkIfCurrentSetThemeIsEmpty()
        setUpCalendarEvents(calendar: self.calendar)
        UserDefaults.standard.set(false, forKey: "didUserIsInSleepMode")
        self.navigationController?.navigationBar.isHidden = true
        circularProgressBar.continueProgress()
        monitorNetworkConnection()
        NotificationCenter.default.addObserver(self, selector: #selector(self.didConnectToUpMoodBand(_:)), name: .didConnectToUpMoodBand, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DashboardViewController.timeChangedNotification), name: NSNotification.Name.NSSystemTimeZoneDidChange, object: nil)
        
        DispatchQueue.main.async { [weak this = self] in
            for date: Date in self.calendar.selectedDates {
                this?.calendar.deselect(date)
                this?.calendar.reloadData()
                this?.currentSessionView.isHidden = true
                this?.currentSessionButton.isHidden = true
            }
        }
        // new current sessions
        
        presenter.attachView(view: self)
        presenter.getCurrentSession()
        
        if Constants.didUserChangeLanguage == true {
            bluetooManager.disconnectPeripheral()
            Constants.didUserChangeLanguage = false
        }
        
        getFitnessProfile()
    }
    
    func getFitnessProfile(){
        let calStr = UserDefaults.standard.string(forKey: "profileCalories") ?? "0.0"
        let stepStr = UserDefaults.standard.string(forKey: "profileSteps") ?? "0.0"
        
        lblCalories.text = calStr
        lblSteps.text = stepStr
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? NewMoodStreamViewController {
            itemDetails.selectedDate = selectedDate
        }
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
        NotificationCenter.default.addObserver(self, selector: #selector(self.didTappedLogOut), name: .didTappedLogOut, object: nil)
        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NSSystemTimeZoneDidChange, object: nil)
       // circularProgressBar.pauseProgress()
//        unsubscribeObserver()
    }
    
    // MARK: Stored - IBActions
    
    @IBAction func addStatus(_ sender: UIButton) {
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
            self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "You're in guest mode, please sign up to access this menu.")
        } else {
            setUpAddStatus(sender)
            performSegue(withIdentifier: "addStatus", sender: self)
        }
    }
    
    @IBAction func reloadParentView(_ segue: UIStoryboardSegue) {
        self.viewDidLoad()
        DispatchQueue.main.async { [weak this = self] in
            for date: Date in self.calendar.selectedDates {
                this?.calendar.deselect(date)
                this?.calendar.reloadData()
            }
        }
    }
    
    @IBAction func goBackFromInsight(_ segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func showSleepMode(_ sender: UIButton) {
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
            performSegue(withIdentifier: "showSleepMode", sender: self)
        } else {
            if self.upmoodBandConnected == true {
                UserDefaults.standard.set(UUID().uuidString, forKey: "sleep_code")
                let vc = storyboard!.instantiateViewController(withIdentifier: "testViewController") as! testViewController
                vc.sleepModeDelegate = self
                present(vc, animated: true, completion: nil)
            } else {
                self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "You can't open sleep mode when band is not connected.")
            }
        }
    }
    
    @objc func timeChangedNotification(notification: NSNotification) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.initCalendar()
            self.calendar.reloadData()
        }
    }
    
    private func checkBuildVersion() {
        let standardUserDefaults = UserDefaults.standard
        let shortVersionKey = "CFBundleShortVersionString"
        let currentVersion = Bundle.main.infoDictionary![shortVersionKey] as! String
        let previousVersion = standardUserDefaults.object(forKey: shortVersionKey) as? String
        if previousVersion == currentVersion {
            // same version
        } else {
            // replace with `if let previousVersion = previousVersion {` if you need the exact value
            if previousVersion != nil {
                self.forceLogOutFailedToFetchToken()
            } else {
                // first launch
            }
            standardUserDefaults.set(currentVersion, forKey: shortVersionKey)
            standardUserDefaults.synchronize()
        }
    }
    
    private func checkIfCurrentSetThemeIsEmpty() {
        if UserDefaults.standard.string(forKey: "currentTheme") == "" || UserDefaults.standard.object(forKey: "currentTheme") == nil {
            UserDefaults.standard.set("Regular", forKey: "currentTheme")
        }
    }
}

// MARK: - Handling UserSleepModeDelegate

extension DashboardViewController: UserSleepModeDelegate {
    func didUserStopSleepMode(bool: Bool) {
       if bool == true {
          self.checkSavedSleepModeGatheredData()
       }
    }
}

extension DashboardViewController: CurrentSessionView {
    func startLoading() {
        //
    }
    
    func finishLoading() {

    }
    
    func setCurrentSession(session: [CurrentSessionViewData]) {
        currentSessionToDisplay = session
        updateCurrentSessionUI()
    }
    
    func getLeaveSesion(session: LeaveSessionViewData) {
        //
    }
    
    func setEmptyCurrentSession() {
        self.currentSessionView.isHidden = true
    }
}

// MARK: - Request and Register FCM Token to Back

extension DashboardViewController {
    func subscribeToFcm() {
        
        //get application instance ID
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
            let fcmtoken = UserDefaults.standard.string(forKey: "fcmToken") ?? ""
            let deviceId = UIDevice.current.identifierForVendor!.uuidString
            self?.sendFcmToken(withDeviceId: deviceId, token: fcmtoken)
        }
    }
    
    func sendFcmToken(withDeviceId: String, token: String) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.sendFcmToken(withDeviceId: withDeviceId, fcmToken: token, device_type: "ios")).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                SVProgressHUD.dismiss(completion: {
                    guard let jsonDic = json as? [String: Any] else { return }
                    let statusCode = jsonDic["status"] as? Int ?? 0
                    if statusCode == 200 {
                        print("json success: \(json)")
                    } else if statusCode == 204 {
                        print("json success but 204: \(json)")
                    } else if statusCode == 401 {
//                        self.forceLogOutFailedToFetchToken()
                        //self.subscribeToFcm()
                    } else {
//                         self.forceLogOutFailedToFetchToken()
                        //self.subscribeToFcm()
                    }
                })
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
}
