//
//  DisplayEmotionViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 21/06/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//
import Alamofire
import Firebase
import FirebaseMessaging
import SVProgressHUD
import SDWebImage
import UIKit

extension Notification.Name {
    static let didUserReceiveNewEmotion = Notification.Name(rawValue: "didUserReceiveNewEmotion")
    static let didUserReceiveNewEmotionInOffline = Notification.Name(rawValue: "didUserReceiveNewEmotionInOffline")
}

class DisplayEmotionViewController: UIViewController {

    @IBOutlet weak var emotionImage: UIImageView!
    var bluetooth = BluetoothManager.getInstance()

    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(didUserReceiveNewEmotion(_:)), name: .didUserReceiveNewEmotion, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setUpOfflineEmotionImage(_:)), name: .didUserReceiveNewEmotionInOffline, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didDisconnectBand(_:)), name: .didDisconnectBand, object: nil)
    }
    
    func setUpOnlineEmotionImage(withFilePath: String) {
        let imageURLString = "\(Constants.Routes.BASE_URL_RESOURCE)\(withFilePath.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: ".svg", with: ".png"))"
        
        print("imageURL: --- \(imageURLString)")
        
        DispatchQueue.main.async {
            self.emotionImage.sd_setImage(with: URL(string: imageURLString)!, completed: nil)
        }
    }
}

extension DisplayEmotionViewController {
    
    private func logOut() {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        
        view.isUserInteractionEnabled = false
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.logOut()).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                
                removeAllSharedPref()
                self.performSegue(withIdentifier: "showLogOut", sender: self)
                
                switch statusCode {
                case 200:
                    SVProgressHUD.dismiss(completion: {
                        removeAllSharedPref()
                        self.performSegue(withIdentifier: "showLogOut", sender: self)
                    })
                    break
                case 204:
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Can't Logged Out, Please check your internet connection.")
                    })
                    break
                case 422:
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "")
                    })
                    break
                default:
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "")
                    })
                    break
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                
                break
            }
        }
    }

    private func forceLogOutFailedToFetchToken() {
        let alert = UIAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Failed to fetch device token.\nPlease relogin.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
            self.logOut()
        }))
 
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func didUserReceiveNewEmotion(_ notification: Notification) {
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            let imageFilePath = dict["emotion_image_filepath"] as? String ?? ""
            setUpOnlineEmotionImage(withFilePath: imageFilePath)
        }
    }
    
    @objc func setUpOfflineEmotionImage(_ notification: Notification) {
        
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            let emotion = dict["emotion_in_offline"] as? String ?? ""
            
            DispatchQueue.main.async {
                switch emotion {
                case "Anxious":
                    self.emotionImage.image = UIImage(named: "Emoji/regular/anxious")
                case "Calm":
                    self.emotionImage.image = UIImage(named: "Emoji/regular/calm")
                case "challenged":
                    self.emotionImage.image = UIImage(named: "Emoji/regular/challenged")
                case "Confused":
                    self.emotionImage.image = UIImage(named: "Emoji/regular/confused")
                case "Excited":
                    self.emotionImage.image = UIImage(named: "Emoji/regular/excited")
                case "Happy":
                    self.emotionImage.image = UIImage(named: "Emoji/regular/happy")
                case "Tense":
                    self.emotionImage.image = UIImage(named: "Emoji/regular/hyped")
                case "Loading":
                    self.emotionImage.image = UIImage(named: "Emoji/regular/loading")
                case "Pleasant":
                    self.emotionImage.image = UIImage(named: "Emoji/regular/pleasant")
                case "Sad":
                    self.emotionImage.image = UIImage(named: "Emoji/regular/sad")
                case "Unpleasant":
                    self.emotionImage.image = UIImage(named: "Emoji/regular/unpleasant")
                case "Zen":
                    self.emotionImage.image = UIImage(named: "Emoji/regular/zen")
                default:
                    break
                }
            }
        }
    }
    
    @objc func didDisconnectBand(_ notification: Notification) {
        DispatchQueue.main.async {
            self.emotionImage.image = UIImage(named: "")
        }
    }
}
