//
//  DashboardFSCalendarDelegate.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 22/01/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import FSCalendar
import Foundation
import KeychainSwift

// MARK: - Handling FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance

extension DashboardViewController: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        
        let calendarDay = Calendar.current.component(.day, from: Date())
        let calendarMonth = Calendar.current.component(.month, from: Date())
        let year = Calendar.current.component(.year, from: Date())
        var stringArr = ["\(year)-\(calendarMonth)-\(calendarDay)"]
        let calendarMonthInt = Int(calendarMonth)
        let calendarDateInt = Int(calendarDay)
        
        if calendarMonthInt < 10 && calendarDateInt < 10 {
            stringArr = ["\(year)-0\(calendarMonth)-0\(calendarDay)"]
        } else if calendarMonthInt < 10 {
            stringArr = ["\(year)-0\(calendarMonth)-\(calendarDay)"]
        } else if calendarDateInt < 10 {
            stringArr = ["\(year)-\(calendarMonth)-0\(calendarDay)"]
        } else {
            stringArr = ["\(year)-\(calendarMonth)-\(calendarDay)"]
        }
        
        let dateString = self.dateFormatter2.string(from: date)
        if stringArr.contains(dateString) {
            return UIColor.blue
        }
        
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let dtStr = self.dateFormatter2.string(from: date)
        let indexOfZero = self.upmood_meter.indexes(of: 0)
        let indexOfOne = self.upmood_meter.indexes(of: 1)
        let indexOfTwo = self.upmood_meter.indexes(of: 2)
        let indexOfThree = self.upmood_meter.indexes(of: 3)
        let indexOfFour = self.upmood_meter.indexes(of: 4)
        
        for index0 in indexOfZero {
            if self.finalDate.indices.contains(index0) {
                if self.finalDate[index0].contains(dtStr) {
                    cell.eventIndicator.color = hexStringToUIColor(hex: Constants.Color.CALM)
                }
            } else {
                // crash
            }
        }
        
        for index1 in indexOfOne {
            if self.finalDate.indices.contains(index1) {
                if self.finalDate[index1].contains(dtStr) {
                    cell.eventIndicator.color = hexStringToUIColor(hex: Constants.Color.SAD)
                }
            } else {
                // crash
            }
        }
        
        for index2 in indexOfTwo {
            if self.finalDate.indices.contains(index2) {
                if self.finalDate[index2].contains(dtStr) {
                    cell.eventIndicator.color = hexStringToUIColor(hex: Constants.Color.HAPPY)
                }
            } else {
                // crash
            }
        }
        
        for index3 in indexOfThree {
            if self.finalDate.indices.contains(index3) {
                if self.finalDate[index3].contains(dtStr) {
                    cell.eventIndicator.color = hexStringToUIColor(hex: Constants.Color.MID_UNPLESANT)
                }
            } else {
                // crash
            }
        }
        
        for index4 in indexOfFour {
            if self.finalDate.indices.contains(index4) {
                if self.finalDate[index4].contains(dtStr) {
                    cell.eventIndicator.color = hexStringToUIColor(hex: Constants.Color.MID_PLEASANT)
                }
            } else {
                // crash
            }
        }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        DispatchQueue.main.async {
            self.finalDate.removeAll()
            self.upmood_meter.removeAll()
            self.setUpCalendarEvents(calendar: calendar)
        }
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateString = self.dateFormatter2.string(from: date)
        if finalDate.contains(dateString) {
            return 1
        }
        
        return 0
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let dateString = self.dateFormatter2.string(from: date)
        self.selectedDate = dateString
        performSegue(withIdentifier: "showMoodStream", sender: self)
    }
    
    // MARK: - Handling setups event in calendar
    
    func setUpCalendarEvents(calendar: FSCalendar) {
        let currentPageDate = calendar.currentPage
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = TimeZone.current
        
        let final = df.string(from: currentPageDate)
        let finalDate = df.date(from: final)
        
        let month = Calendar.current.component(.month, from: finalDate!)
        let year = Calendar.current.component(.year, from: finalDate!)
        
        if month < 10 {
            let monthInt = String(format: "%02d", month)
            loadCalendarEvents(year: year, month: monthInt)
        } else {
            loadCalendarEvents(year: year, month: String(month))
        }
    }
    
    // MARK: - Handling loads events in calendar (REST)
    
    func loadCalendarEvents(year: Int, month: String) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        let token = KeychainSwift().get("apiToken") ?? ""
        
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record?sort=monthly&timezone=\(localTimeZoneName)&date=\(year)-\(month)") else { return }
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                guard let json = userObject as? [String: Any] else { return  }
                
                let statusCode = json["status"] as? Int ?? 0
                
                guard let jsonData = json["data"] as? [[String: Any]] else {
                    return }
                
                for data in jsonData {
                    let calendarDate = data["calendar_date"] as? String ?? ""
                    let calendarMonth = data["calendar_month"] as? String ?? ""
                    let upmoodMeter = data["upmood_meter"] as? Int ?? 5
                    self.calendar_date.append(calendarDate)
                    self.calendar_month.append(calendarMonth)
                    self.upmood_meter.append(upmoodMeter)
                    
                    // SetUp Calendar Events
                    let currentPageDate = self.calendar.currentPage
                    let year = Calendar.current.component(.year, from: currentPageDate)
                    
                    let calendarMonthInt = Int(calendarMonth) ?? 0
                    let calendarDateInt = Int(calendarDate) ?? 0
                    
                    if calendarMonthInt < 10 && calendarDateInt < 10 {
                        let value = "\(String(year))-0\(String(calendarMonth))-0\(String(calendarDate))";
                        self.finalDate.append(value)
                    } else if calendarMonthInt < 10 {
                        let value = "\(String(year))-0\(String(calendarMonth))-\(String(calendarDate))";
                        self.finalDate.append(value)
                    } else if calendarDateInt < 10 {
                        let value = "\(String(year))-\(String(calendarMonth))-0\(String(calendarDate))";
                        self.finalDate.append(value)
                    } else {
                        let value = "\(String(year))-\(String(calendarMonth))-\(String(calendarDate))";
                        self.finalDate.append(value)
                    }
                }
                // end loop
                
                DispatchQueue.main.async {
                    self.calendar.reloadData()
                }
                
            } else {
                DispatchQueue.main.async {
                    self.connectionBannerView.isHidden = false
                }
            }
        }.resume()
    }
}
