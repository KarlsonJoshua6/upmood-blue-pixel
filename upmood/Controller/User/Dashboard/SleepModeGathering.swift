//
//  SleepModeGathering.swift
//  upmood
//
//  Created by John Paul Manoza on 20/01/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import Foundation
import SVProgressHUD
import UIKit

extension DashboardViewController {
    
    // MARK: - Sending/Storing sleep mode gathered data to API (REST)
    
    func sendOfflineBatchStoreSleepMode(data: String) {
         view.isUserInteractionEnabled = false
         SVProgressHUD.show()
         Alamofire.request(APIClient.sendDataOffline(data: "[\(data)]")).responseJSON { response in
             switch response.result {
             case .success(let json):
                 
                 guard let json = json as? [String: Any] else { return  }
                 let statusCode = json["status"] as? Int ?? 0
                 
                 print("converted: [\(data)]")
                 print("JSON ITO SA OFFLINE BATCHSTORE: \(json)")
                 
                 switch statusCode {
                 case 200:
                     SVProgressHUD.dismiss(completion: {
                         self.view.isUserInteractionEnabled = true
                         self.jsonStringArrSleepMode.removeAll()
                         self.savedJSONDataSleepMode.removeAll()
                         
                         DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                             UserDefaults.standard.set("", forKey: "sleepModeGatheredData")
                         }
                     })
                    
                     self.checkSavedOfflineGatheredData()
                    
                 case 204:
                     SVProgressHUD.dismiss(completion: {
                         self.view.isUserInteractionEnabled = true
                         self.savePendingSleepModeRecord()
                     })
                 default:
                     SVProgressHUD.dismiss(completion: {
                         self.view.isUserInteractionEnabled = true
                         self.savePendingSleepModeRecord()
                     })
                 }
             case .failure(let error):
                 SVProgressHUD.dismiss(completion: {
                     self.savePendingSleepModeRecord()
                     self.view.isUserInteractionEnabled = true
                     self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                 })
             }
         }
     }
    
    // MARK: - Handling Pop up dialog in saving sleep mode gathered data
    
    func savePendingSleepModeRecord() {
        let alert = UIAlertController(title: NSLocalizedString("You've collected data while you in sleep mode.", comment: ""), message: NSLocalizedString("Would you like to sync your gathered data?", comment: ""), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default, handler: { action in
            let retriveJSONString = UserDefaults.standard.string(forKey: "sleepModeGatheredData") ?? ""
            self.sendOfflineBatchStoreSleepMode(data: retriveJSONString)
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler: { alert in
            UserDefaults.standard.set("", forKey: "sleepModeGatheredData")
            self.jsonStringArrSleepMode.removeAll()
            self.savedJSONDataSleepMode.removeAll()
        }))
        
        self.present(alert, animated: true)
    }
}
