//
//  DashboardViewController+.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 07/06/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import CLWaterWaveView
import Firebase
import KeychainSwift
import FSCalendar
import GradientProgressBar
import FirebaseMessaging
import SVProgressHUD
import UICircularProgressRing
import Reachability
import SwiftyJSON
import UIKit

// MARK: Stored - NotificationCenter

extension Notification.Name {
    static let batteryPercent = Notification.Name(rawValue: "batteryPercent")
    static let stressLevelReceived = Notification.Name(rawValue: "stressLevelReceived")
    static let emotionValueReceived = Notification.Name(rawValue: "emotionValueReceived")
    static let bpmReceived = Notification.Name(rawValue: "bpmReceived")
    static let ppiReceived = Notification.Name(rawValue: "ppiReceived")
    static let totalPPI = Notification.Name(rawValue: "totalPPI")
    static let emotionLevel = Notification.Name(rawValue: "emotionLevel")
    static let didEndCalculation = Notification.Name(rawValue: "endCalculation")
    static let didTappedLogOut = Notification.Name(rawValue: "didTappedLogOut")
    static let didConnectToUpMoodBand = Notification.Name(rawValue: "didConnectToUpMoodBand")
    static let didDisconnectBand = Notification.Name(rawValue: "didDisconnectBand")
    static let didUserReceiveNotif = Notification.Name(rawValue: "didUserReceiveNotif")
    static let didUpdateUserPostStatus = Notification.Name(rawValue: "didUpdateUserPostStatus")
}

// MARK: Stored - NotificationCenter Observer

extension DashboardViewController {
    
    func didLoadNotification(notification: Notification) {
        if let notif_data = notification.userInfo as? [String: Any] {
            let notif_type_id = notif_data["type_id"] as? String ?? ""
            let type_id = Int(notif_type_id) ?? 0
            
            switch type_id {
            case 1...4:
                tabBarController?.tabBar.items?[3].badgeValue = ""
                tabBarController?.tabBar.items?[3].badgeColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            default:
                break
            }
        }
    }

     @objc func moveToDeviceTab(_ sender: UIButton) {
         //self.tabBarController?.selectedIndex = 4
        performSegue(withIdentifier: "showDevice", sender: self)
     }
       
     @objc func moveToSelectionTheme(_ sender: UIButton) {
         self.performSegue(withIdentifier: "showSelectionTheme", sender: self)
     }
    
    func unsubscribeObserver() {
        NotificationCenter.default.removeObserver(self, name: .batteryPercent, object: nil)
        NotificationCenter.default.removeObserver(self, name: .stressLevelReceived, object: nil)
        NotificationCenter.default.removeObserver(self, name: .bpmReceived, object: nil)
        NotificationCenter.default.removeObserver(self, name: .ppiReceived, object: nil)
        NotificationCenter.default.removeObserver(self, name: .totalPPI, object: nil)
        NotificationCenter.default.removeObserver(self, name: .emotionLevel, object: nil)
        NotificationCenter.default.removeObserver(self, name: .emotionValueReceived, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didEndCalculation, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didDisconnectBand, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didUserReceiveNotif, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didUpdateUserPostStatus, object: nil)
    }
    
     func initObserver() {
        unsubscribeObserver()
        NotificationCenter.default.addObserver(self, selector: #selector(displayBatteryPercent(_:)), name: .batteryPercent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(displayStressLevel(_:)), name: .stressLevelReceived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(displayBPM(_:)), name: .bpmReceived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(displayPPI(_:)), name: .ppiReceived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(displayTotalPPI(_:)), name: .totalPPI, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(displayEmotionLevel(_:)), name: .emotionLevel, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(displayEmotionValue(_:)), name: .emotionValueReceived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEndCalculation(_:)), name: .didEndCalculation, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didDisconnectBand(_:)), name: .didDisconnectBand, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didUserReceiveNotif(_:)), name: .didUserReceiveNotif, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didUpdateUserPostStatus(_:)), name: .didUpdateUserPostStatus, object: nil)
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    @objc func didDisconnectBand(_ notification: Notification) {
        self.loadingLabel.text = TranslationConstants.TAP_DEVICE_TO_CONNECT_BAND.LOCALIZED
        self.currentMoodLabel.text = self.oldEmotion
        self.circularProgressBar.startProgress(to: 0, duration: 0)
        self.currentMoodLabel.text = ""
        self.heartRateBpmLabel.text = "0 \("BPM".LOCALIZED)"
        self.upmoodBandConnected = false
        self.currentCircularProgressBarValueLabel.isHidden = true
        // temp lang to
        //self.checkBandAutoReconTimeOut()
    }
    
    private func checkBandAutoReconTimeOut() {
        let fetchAutoReconnect = UserDefaults.standard.integer(forKey:  "fetchAutoReconnect")
        
        switch fetchAutoReconnect {
        case 0:
            print("Auto Reconnect Disabled")
        case 1:
            print("Auto Reconnect Enabled")
            
            self.timer2 = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.countTimer), userInfo: nil, repeats: true)
            
        default:
            break
        }
    }
    
    @objc func countTimer() {
       if(seconds > 0) {
            seconds-=1
        }
        
        print("counting from - \(seconds)")
        
        if seconds == 0 {
            print("time na ako hahaha")
            Constants.didUserDisconnectedToBand = true
        }
    }
    
    @objc func displayEmotionLevel(_ notification: Notification) {
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            guard let emotionLevel = dict["data"] as? Float else { return }
            let emotionLevelDouble = Double(emotionLevel)
            UserDefaults.standard.set(emotionLevelDouble, forKey: "emotionLevel")
        }
    }
    
    @objc func didUserReceiveNotif(_ notification: Notification) {
        
        viewModel.fetchCurrentSessionList {
            DispatchQueue.main.async { [weak self] in
                self?.updateCurrentSessionUI()
            }
        }
        
        didLoadNotification(notification: notification)
        
//        // notification onClick redirection page
//        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
//            let type_id = dict["type_id"] as? String ?? ""
//            self.typeId = type_id
//            UserDefaults.standard.set(type_id, forKey: "type_id_notif")
//
//            if type_id == "1" || type_id == "2" || type_id == "3" || type_id == "3" || type_id == "4" {
//                self.tabBarController?.selectedIndex = 3
//            }
//        }
    }
    
    @objc func didUpdateUserPostStatus(_ notification: Notification) {
        preloadUserStatus()
    }
    
    @objc func displayTotalPPI(_ notification: Notification) {
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            guard let total = dict["data"] as? Float else { return }
            let totalPPI = Int(total)
            UserDefaults.standard.set(totalPPI, forKey: "totalPPI")
            self.endOfMoodGathering()
            let currentValue = Int(self.circularProgressBar.currentValue ?? 0)
            let percent = 100 * totalPPI/180000
            self.currentCircularProgressBarValueLabel.isHidden = false
            self.currentCircularProgressBarValueLabel.text = "\(currentValue)%"
            self.startProgressBar(withPercent: percent)
        }
    }
    
    @objc func displayPPI(_ notification: Notification) {
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            guard let ppi = dict["data"] as? [Float] else { return }
            let stringArray = ppi.map { String($0) }
            let ppiString = stringArray.joined(separator: ",")
            UserDefaults.standard.set(ppiString, forKey: "fetchPPI")
        }
    }
    
    @objc func displayBPM(_ notification: Notification) {
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            let bpm = dict["data"] as? Double ?? 0.0
            let integerBPM = Int(bpm)
            
            self.heartRateBpmLabel.text = "\(integerBPM) \(NSLocalizedString("BPM", comment: ""))"
            UserDefaults.standard.set(integerBPM, forKey: "integerBPM")
            
            // added new implementation of progress value
            // for versioning only
            
            if Constants.didUserStopSleepMode == false {
                if self.currentSessionToDisplay.count >= 1 {
                    if self.currentSessionToDisplay[0].userStatus == 1 {
                        storeProgressValue()
                    }
                }
            } else {
                print("user is in sleep mode, ")
            }
        }
    }
    
    @objc func displayEmotionValue(_ notification: Notification) {
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            let emotionValue = dict["data"] as? String ?? ""
            self.setUpTranslationMoodString(emotion: emotionValue.capitalized)
            UserDefaults.standard.set(emotionValue.capitalized, forKey: "emotionValueString")
            self.displayUpmoodMeter()
        }
    }
    
    @objc func displayStressLevel(_ notification: Notification) {
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            let stressLevel = dict["data"] as? String ?? ""
            self.setUpStressLevelHeart(stressLevel: stressLevel)
            UserDefaults.standard.set(stressLevel, forKey: "stressLevelString")
            
            guard let stress_level_value = dict["stress_level_value"] as? Float else { return }
            let stress_level_value_double = Double(stress_level_value)
            UserDefaults.standard.set(stress_level_value_double, forKey: "stress_level_value")
            print("displayStressLevel: \(stress_level_value)")
        }
    }
    
    @objc func didTappedLogOut(_ notification: Notification) {
        let bpm = UserDefaults.standard.integer(forKey: "integerBPM")
        let emotionValue = UserDefaults.standard.string(forKey: "emotionValueString") ?? ""
        let stressLevel = UserDefaults.standard.string(forKey: "stressLevelString") ?? ""
        self.loadingLabel.text = TranslationConstants.TAP_DEVICE_TO_CONNECT_BAND.LOCALIZED
        self.currentMoodLabel.text = self.oldEmotion
        self.heartRateBpmLabel.text = "\(bpm) \(NSLocalizedString("BPM", comment: ""))"
        self.setUpStressLevelHeart(stressLevel: stressLevel)
        self.upmoodBandConnected = false
        self.initWave()
        
        // remove all data when logging out
        UserDefaults.standard.set("", forKey: "offlineCalculation")
        UserDefaults.standard.set("", forKey: "sleepModeGatheredData")
        self.jsonStringArrSleepMode.removeAll()
        self.savedJSONDataSleepMode.removeAll()
    }
    
    @objc func didEndCalculation(_ notification: Notification) {
        
    }
    
    @objc func didConnectToUpMoodBand(_ notification: Notification) {
        self.loadingLabel.text = NSLocalizedString("Gathering mood data...", comment: "")
        self.currentCircularProgressBarValueLabel.isHidden = false
        self.currentMoodLabel.text = self.oldEmotion
        self.upmoodBandConnected = true
        self.currentCircularProgressBarValueLabel.isHidden = false
        self.startWave()
        self.initMoveButton()
        self.seconds = 480
        self.timer.invalidate()
        self.timer2.invalidate()
    }
    
    @objc  func displayBatteryPercent(_ notification: Notification) {
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            let _ = dict["percent"] as? UInt8 ?? 0
        }
    }
}
