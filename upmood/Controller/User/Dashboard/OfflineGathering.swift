//
//  OfflineGathering.swift
//  upmood
//
//  Created by John Paul Manoza on 20/01/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.


import Alamofire
import SwiftyJSON
import SVProgressHUD
import Foundation

extension DashboardViewController {
    
    // MARK: - Sending/Storing Offline gathered data to API (REST)
    
    func sendOfflineBatchStore(data: String) {
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        Alamofire.request(APIClient.sendDataOffline(data: "[\(data)]")).responseJSON { response in
              
          print("converted: [\(data)]")
          print("response: - \(response.result.value)")
          
          switch response.result {
          case .success(let json):
              
              guard let json = json as? [String: Any] else { return  }
              let statusCode = json["status"] as? Int ?? 0
              
              switch statusCode {
              case 200:
                  SVProgressHUD.dismiss(completion: {
                      self.view.isUserInteractionEnabled = true
                      self.jsonStringArrOfflineMode.removeAll()
                      self.savedJSONDataOfflineMode.removeAll()
                    
                      DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                          UserDefaults.standard.set("", forKey: "offlineCalculation")
                      }
                  })
                  
              case 204:
                  SVProgressHUD.dismiss(completion: {
                    self.view.isUserInteractionEnabled = true
                    self.savePendingRecord()
                  })
              default:
                  SVProgressHUD.dismiss(completion: {
                      self.view.isUserInteractionEnabled = true
                      self.savePendingRecord()
                  })
              }
          case .failure(let error):
              SVProgressHUD.dismiss(completion: {
                  self.view.isUserInteractionEnabled = true
                  self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
              })
          }
       }
    }
    
    // MARK: - Save Offline gathered data
    
    func saveOfflineGatheredData(isSleepMode: Bool) {
        
       let user_id = UserDefaults.standard.integer(forKey: "fetchUserId")
       let emotionSet = UserDefaults.standard.string(forKey: "currentTheme") ?? "Regular"
       let bpm = UserDefaults.standard.integer(forKey: "integerBPM")
       let emotionValue = UserDefaults.standard.string(forKey: "emotionValueString") ?? ""
       let stressLevel = UserDefaults.standard.string(forKey: "stressLevelString") ?? ""
       let ppi = UserDefaults.standard.string(forKey: "fetchPPI") ?? ""
       let totalPPI = UserDefaults.standard.integer(forKey: "totalPPI")
       let emotionLevel = UserDefaults.standard.double(forKey: "emotionLevel")
       let stress_level_value = UserDefaults.standard.double(forKey: "stress_level_value")
       let profile_post = UserDefaults.standard.string(forKey: "profile_post") ?? ""
       
       let dateFormatter = DateFormatter()
       dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
       let now = dateFormatter.string(from: Date())
       var localTimeZoneName: String { return TimeZone.current.identifier }
       
       if isSleepMode == true {
           // save offline gathered data but in sleep mode
           self.saveGatheredData(withUserId: user_id, BPM: String(bpm), emotionSet: emotionSet.capitalized, emotionValue: emotionValue.lowercased(), emotionLevel: String(emotionLevel), stressLevel: stressLevel, ppi: String(ppi), totalPPI: String(totalPPI), now: now, localTimeZoneName: localTimeZoneName,stress_level_value: String(stress_level_value), post: profile_post, isSleepMode: true)
       } else {
           // save offline gathered data but in regular gathering
           self.saveGatheredData(withUserId: user_id, BPM: String(bpm), emotionSet: emotionSet.capitalized, emotionValue: emotionValue.lowercased(), emotionLevel: String(emotionLevel), stressLevel: stressLevel, ppi: String(ppi), totalPPI: String(totalPPI), now: now, localTimeZoneName: localTimeZoneName,stress_level_value: String(stress_level_value), post: profile_post, isSleepMode: false)
       }
           
       // preparing ui to display (mood ui)
       NotificationCenter.default.post(name: .didUserReceiveNewEmotionInOffline, object: nil, userInfo: ["emotion_in_offline": emotionValue.capitalized])
       DispatchQueue.main.async {
           self.setUpTranslationMoodString(emotion: emotionValue.capitalized)
       }
    }
    
    // MARK: - Handling Pop up dialog in saving offline gathered data
    
    func savePendingRecord() {
      let alert = UIAlertController(title: NSLocalizedString("You've collected data while offline", comment: ""), message: NSLocalizedString("Would you like to sync your offline data?", comment: ""), preferredStyle: .alert)
      
      alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default, handler: { action in
          let retriveJSONString = UserDefaults.standard.string(forKey: "offlineCalculation") ?? ""
          self.sendOfflineBatchStore(data: retriveJSONString)
      }))
      
      alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler: { alert in
          UserDefaults.standard.set("", forKey: "offlineCalculation")
          self.jsonStringArrOfflineMode.removeAll()
          self.savedJSONDataOfflineMode.removeAll()
      }))
      
      self.present(alert, animated: true)
    }
    
    // MARK: - Handling saved gathered data (offline and sleep mode)
    
    func saveGatheredData(withUserId: Int, BPM: String, emotionSet: String, emotionValue: String, emotionLevel: String, stressLevel: String, ppi: String, totalPPI: String, now: String, localTimeZoneName: String, stress_level_value: String, post: String, isSleepMode: Bool) {
        
        var setOfArray: [String: Any] = [:]
        
        if isSleepMode == true {
            // sleep mode on
            if self.oldEmotion.isEmpty {
                setOfArray = ["user_id": withUserId, "heartbeat_count": BPM, "emotion_set": emotionSet.capitalized,
                              "emotion_value": emotionValue, "emotion_level": emotionLevel, "stress_level": stressLevel,
                              "type": "automated", "longitude": "123", "latitude": "456", "ppi": ppi,
                              "total_ppi": totalPPI, "created_at": now, "timezone": localTimeZoneName,
                              "stress_level_value": stress_level_value, "post": post
                             ]
            } else {
                setOfArray = ["user_id": withUserId, "heartbeat_count": BPM, "emotion_set": emotionSet.capitalized,
                              "emotion_value": emotionValue, "emotion_level": emotionLevel, "stress_level": stressLevel,
                              "type": "automated", "longitude": "123", "latitude": "456",
                              "ppi": ppi, "total_ppi": totalPPI, "created_at": now, "timezone": localTimeZoneName,
                              "stress_level_value": stress_level_value, "post": post
                             ]
            }
            
            self.setUpsaveGatheredData(setOfArray: setOfArray, isSleepMode: true)
            
        } else {
            // sleep mode off
            setOfArray = ["user_id": withUserId, "heartbeat_count": BPM, "emotion_set": emotionSet.capitalized,
                          "emotion_value": emotionValue, "emotion_level": emotionLevel, "stress_level": stressLevel,
                          "type": "automated", "longitude": "123", "latitude": "456",
                          "ppi": ppi, "total_ppi": totalPPI, "created_at": now,
                          "timezone": localTimeZoneName, "stress_level_value": stress_level_value, "post": post
                         ]
            self.setUpsaveGatheredData(setOfArray: setOfArray, isSleepMode: false)
        }
    }
    
    private func setUpsaveGatheredData(setOfArray: [String: Any], isSleepMode: Bool) {
        
        if isSleepMode == true {
            
           let jsonData = try! JSONSerialization.data(withJSONObject: setOfArray)
           let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) ?? ""
           let final = JSON.init(parseJSON: String(jsonString))

           self.savedJSONDataSleepMode.append(final)
            
            var stringF = ""
            var stringS = ""
            
            for json in self.savedJSONDataSleepMode {
                stringS = json.rawString([:]) ?? ""
            }
            
            self.jsonStringArrSleepMode.append(stringS)
            stringF = self.jsonStringArrSleepMode.joined(separator: ",\n")
            UserDefaults.standard.set(stringF, forKey: "sleepModeGatheredData")
            
            print("test isSleepMode = true \(stringF)")
            
        } else {
            
            
            let jsonData = try! JSONSerialization.data(withJSONObject: setOfArray)
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) ?? ""
            let final = JSON.init(parseJSON: String(jsonString))

            self.savedJSONDataOfflineMode.append(final)
            
            var stringY = ""
            var stringX = ""
            
            for json in self.savedJSONDataOfflineMode {
                stringX = json.rawString([:]) ?? ""
            }
            
            self.jsonStringArrOfflineMode.append(stringX)
            stringY = self.jsonStringArrOfflineMode.joined(separator: ",\n")
            UserDefaults.standard.set(stringY, forKey: "offlineCalculation")
            
            print("test isSleepMode = false \(stringY)")
        }
    }
}
