//
//  EditGroupVC.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 05/04/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import SDWebImage
import SkyFloatingLabelTextField
import UIKit

class EditGroupVC: UIViewController {
    
    // Stored Outlets
    
    @IBOutlet weak var offIndicatorLabel: UILabel!
    @IBOutlet weak var groupNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var tableView: UITableView!
    
    // Stored Values
    
    var groupName: String!
    var groupId: Int!
    var memberName = [String]()
    var memberProfilePic = [String]()
    var memberId = [Int]()
    var selectedIndex = 0
    
    // stored Values - didTappedManagePrivacy
    
    var privacyEmotion: Int!
    var privacyProfileDetails: Int!
    var privacyHeartBeat: Int!
    
    // Overriding
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        loadSpecificGroupList(withId: self.groupId ?? 0)
        setUpUI()
        
        if UserDefaults.standard.string(forKey: "selectedLanguage") == "en" {
            groupNameTextField.title = "Group Name"
        } else {
            groupNameTextField.title = "グループ名"
        }
        
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = NSLocalizedString("Edit Group", comment: "")
        navigationController?.navigationBar.isHidden = false
    }
    
    private func setUpUI() {
        self.groupNameTextField.text = self.groupName
        
        let notificationType = UserDefaults.standard.string(forKey: "fetchNotificationType") ?? ""
        if notificationType == "live" {
            self.offIndicatorLabel.text = NSLocalizedString("On", comment: "")
        } else {
            self.offIndicatorLabel.text = NSLocalizedString("Off", comment: "")
        }
    }
    
    // IBActions
    
    @IBAction func didTappedReceiveNotification(_ sender: UIButton) {
        let nextVC = storyboard!.instantiateViewController(withIdentifier: "ReceiveNotificationViewController") as! ReceiveNotificationViewController
        present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func didTappedManagePrivacy(_ sender: UIButton) {
        let nextVC = storyboard!.instantiateViewController(withIdentifier: "GroupPrivacyViewController") as! GroupPrivacyViewController
        nextVC.privacyEmotion = privacyEmotion
        nextVC.privacyProfileDetails = privacyProfileDetails
        nextVC.privacyHeartBeat = privacyHeartBeat
        nextVC.groupId = groupId
        present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func didTappedAddMember(_ sender: UIButton) {
        let nextVC = storyboard!.instantiateViewController(withIdentifier: "AddNewMemberInEditVC") as! AddNewMemberInEditVC
        nextVC.groupIdSelected = self.groupId!
        nextVC.oldFriendIds = self.memberId
        present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func done(_ sender: UIButton) {
        
        let fetchPrivacyProfileDetails = UserDefaults.standard.integer(forKey: "fetchPrivacyProfileDetails")
        let fetchPrivacyEmotion = UserDefaults.standard.integer(forKey: "fetchPrivacyEmotion")
        let fetchPrivacyHeartBeat = UserDefaults.standard.integer(forKey: "fetchPrivacyHeartBeat")
        let selectedEmotions = UserDefaults.standard.string(forKey: "fetchEmotions") ?? ""
        
        if isKeyPresentInUserDefaults(key: "savedPrivacyGroupPrivacy") {
            let savedPrivacyGroupPrivacy = UserDefaults.standard.integer(forKey: "savedPrivacyGroupPrivacy")
            let savedEmotionGroupPrivacy = UserDefaults.standard.integer(forKey: "savedEmotionGroupPrivacy")
            let savedHeartRateGroupPrivacy = UserDefaults.standard.integer(forKey: "savedHeartRateGroupPrivacy")
            self.editGroup(withGroupId: self.groupId!, type: "name", value: self.groupNameTextField.text ?? "", type_data: "", emotions: selectedEmotions, heartbeat: savedHeartRateGroupPrivacy, stress_level: 1, emotion: savedEmotionGroupPrivacy, my_mood: savedPrivacyGroupPrivacy)
        } else {
            self.editGroup(withGroupId: self.groupId!, type: "name", value: self.groupNameTextField.text ?? "", type_data: "", emotions: selectedEmotions, heartbeat: fetchPrivacyHeartBeat, stress_level: 1, emotion: fetchPrivacyEmotion, my_mood: fetchPrivacyProfileDetails)
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "didEditGroup")
        UserDefaults.standard.removeObject(forKey: "savedPrivacyGroupPrivacy")
        UserDefaults.standard.removeObject(forKey: "savedEmotionGroupPrivacy")
        UserDefaults.standard.removeObject(forKey: "savedHeartRateGroupPrivacy")
        performSegue(withIdentifier: "reloadViewGroup", sender: self)
    }
}

// MARK -- UITableViewDelegate, UITableVIewDataSource

extension EditGroupVC: UITableViewDelegate, UITableViewDataSource {
    
    // -- UITableViewDelegate
    
    private func setUpCell(cell: EditGroupSelectedMemberTableViewCell, indexPath: IndexPath) {
        cell.groupNameLabel.text = memberName[indexPath.row]
        cell.userProfilePicImageView.sd_setImage(with: URL(string: self.memberProfilePic[indexPath.row]), placeholderImage: UIImage(named: "ic_empty_state_profile"), options: .continueInBackground, completed: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "EditGroupSelectedMemberTableViewCell", for: indexPath) as? EditGroupSelectedMemberTableViewCell {
            self.setUpCell(cell: cell, indexPath: indexPath)
            return cell
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memberName.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    // -- UITableViewDataSource
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        self.removeFriendOnGroup(withId: self.memberId[indexPath.row], andIndexPath: indexPath)
    }
}

// MARK -- Alamofire.Requests

extension EditGroupVC {
    
    @IBAction func reloadEditVC(_ sender: UIStoryboardSegue) {
        privacyEmotion = 0
        privacyProfileDetails = 0
        privacyHeartBeat = 0
        tableView.reloadData()
        viewDidLoad()
    }
    
    private func reload() {
        memberName.removeAll()
        memberProfilePic.removeAll()
        memberId.removeAll()
        tableView.reloadData()
    }
    
    private func loadSpecificGroupList(withId: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        self.reload()
        Alamofire.request(APIClient.loadSpecificGroupList(withId: withId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                switch statusCode {
                case 200:
                    
                    guard let data = JSON["data"] as? [String: Any] else { return }
                    guard let jsonData = data["data"] as? [[String: Any]] else { return }
                    
                    for jsondata in jsonData {
                        
                        let privacyEmotion = jsondata["emotion"] as? Int ?? 0
                        let privacyProfileDetails = jsondata["my_mood"] as? Int ?? 0
                        let privacyHeartBeat = jsondata["heartbeat"] as? Int ?? 0
                        
                        self.privacyEmotion = privacyEmotion
                        self.privacyProfileDetails = privacyProfileDetails
                        self.privacyHeartBeat = privacyHeartBeat
                        
                        let emotions = jsondata["emotions"] as? String ?? ""
                        let notificationType = jsondata["notification_type"] as? String ?? ""
                        
                        // Saved In UserDefaults
                        UserDefaults.standard.set(emotions, forKey: "fetchEmotions")
                        UserDefaults.standard.set(notificationType, forKey: "fetchNotificationType")
                        UserDefaults.standard.set(self.groupId ?? 0, forKey: "fetchGroupIdInEdit")
                        UserDefaults.standard.set(privacyEmotion, forKey: "fetchPrivacyEmotion")
                        UserDefaults.standard.set(privacyProfileDetails, forKey: "fetchPrivacyProfileDetails")
                        UserDefaults.standard.set(privacyHeartBeat, forKey: "fetchPrivacyHeartBeat")
                        
                        let groupname = jsondata["name"] as? String ?? ""
                        self.groupName = groupname
                        
                        // Group Member Details
                        if let groupMember = jsondata["members"] as? [[String: Any]] {
                     
                            for member in groupMember {
                                
                                let id = member["id"] as? Int ?? 0
                                let memberName = member["name"] as? String ?? ""
                                let memberProfilePic = member["image"] as? String ?? ""
                                let _ = member["is_online"] as? Int ?? 0
                                
                                this?.memberId.append(id)
                                this?.memberName.append(memberName)
                                this?.memberProfilePic.append(memberProfilePic.replacingOccurrences(of: " ", with: "%20"))
                            }
                        }
                    }
                    
                    SVProgressHUD.dismiss(completion: {
                        DispatchQueue.main.async {
                            this?.tableView.reloadData()
                        }
                    })
                    
                    break
                case 204:
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    break
                case 401:
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    break
                default:
                    break
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    
    private func addFriendToGroup(withGroupId: Int, members: String) {
        Alamofire.request(APIClient.addFriendToGroup(members: members, group_id: withGroupId)).responseJSON { [weak this = self] response in
            
            print("response to - \(response.result.value)")
            
            switch response.result {
            case .success(let json):
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                if statusCode == 200 {
                    print("success edit: \(jsonDic)")
                    UserDefaults.standard.removeObject(forKey: "savedPrivacyGroupPrivacy")
                    UserDefaults.standard.removeObject(forKey: "savedEmotionGroupPrivacy")
                    UserDefaults.standard.removeObject(forKey: "savedHeartRateGroupPrivacy")
                    this?.reload()
                    this?.performSegue(withIdentifier: "reloadViewGroup", sender: self)
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func editGroup(withGroupId: Int, type: String, value: String, type_data: String, emotions: String, heartbeat: Int, stress_level: Int, emotion: Int, my_mood: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.editGroup(withGroupId: withGroupId, type: type, value: value, type_data: type_data, emotions: emotions, heartbeat: heartbeat, stress_level: stress_level, emotion: emotion, my_mood: my_mood)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        UserDefaults.standard.removeObject(forKey: "savedPrivacyGroupPrivacy")
                        UserDefaults.standard.removeObject(forKey: "savedEmotionGroupPrivacy")
                        UserDefaults.standard.removeObject(forKey: "savedHeartRateGroupPrivacy")
                        this?.reload()
                        this?.performSegue(withIdentifier: "reloadViewGroup", sender: self)
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func deleteFriendInGroup(withId: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.removeFriendToGroup(withId: withId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        self.memberName.remove(at: self.selectedIndex)
                        self.memberId.remove(at: self.selectedIndex)
                        self.memberProfilePic.remove(at: self.selectedIndex)
                        self.tableView.reloadData()
                        self.loadSpecificGroupList(withId: self.groupId ?? 0)
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        self.memberName.remove(at: self.selectedIndex)
                        self.memberId.remove(at: self.selectedIndex)
                        self.memberProfilePic.remove(at: self.selectedIndex)
                        self.tableView.reloadData()
                        self.viewDidLoad()
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    private func removeFriendOnGroup(withId: Int, andIndexPath: IndexPath) {
        let alert = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Remove", comment: ""), style: .default , handler:{ alert in
            self.deleteFriendInGroup(withId: self.memberId[andIndexPath.row])
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler:{ alert in
            
        }))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
}
