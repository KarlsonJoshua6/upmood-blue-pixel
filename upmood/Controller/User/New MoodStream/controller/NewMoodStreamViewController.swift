//
//  NewMoodStreamViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 23/09/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import Charts
import KeychainSwift
import SVProgressHUD
import UIKit

class NewMoodStreamViewControllerCell: UITableViewCell {
    // sleep record
    @IBOutlet weak var sleepRecordView: UIView!
    @IBOutlet weak var sleepQualityLabel: UILabel!
    @IBOutlet weak var sleepDurationLabel: UILabel!
    @IBOutlet weak var actualSleepTimeLabel: UILabel!
    @IBOutlet weak var bedtimeLabel: UILabel!
    @IBOutlet weak var toFallAsleepLabel: UILabel!
    @IBOutlet weak var sleepCycleLabel: UILabel!
    @IBOutlet weak var wokeUpLabel: UILabel!
    @IBOutlet weak var emotionBeforeSleepLabel: UILabel!
    @IBOutlet weak var dateSleepRecordGathered: UILabel!
    
    // sleep record non value label and images
    @IBOutlet weak var nonValueSleepQualityLabel: UILabel!
    @IBOutlet weak var nonValueSleepQualityImageView: UIImageView!
    @IBOutlet weak var nonValueSleepDutaionLabel: UILabel!
    @IBOutlet weak var nonValueSleepDurationImageView: UIImageView!
    @IBOutlet weak var nonValueActualSleepTimeLabel: UILabel!
    @IBOutlet weak var nonValueActualSleepTimeImageView: UIImageView!
    @IBOutlet weak var nonValueToFallAsleepLabel: UILabel!
    @IBOutlet weak var nonValueToFallAsleepImageView: UIImageView!
    @IBOutlet weak var nonValueSleepCycleLabel: UILabel!
    @IBOutlet weak var nonValueSleepCycleImageView: UIImageView!
    @IBOutlet weak var nonValueEmptyLabel: UILabel!
    @IBOutlet weak var nonValueWokeUpImageView: UIImageView!
    @IBOutlet weak var nonValueWokeUpLabel: UILabel!
    @IBOutlet weak var nonValueEmotionBeforeSleepImageView: UIImageView!
    @IBOutlet weak var nonValueEmotionBeforeSleepLabel: UILabel!
    
    // constraints
    @IBOutlet weak var emotionBeforeSleepImageHeight: NSLayoutConstraint!
    @IBOutlet weak var emotionBeforeSleepLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var emotionBeforeSleepWithValeHeight: NSLayoutConstraint!
}

class NewMoodStreamViewController: UIViewController {
    
    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var recordView: UIView!
    
    // record View
    @IBOutlet weak var emotionLabel: UILabel!
    @IBOutlet weak var stressLevelLabel: UILabel!
    @IBOutlet weak var activityStatusLabel: UILabel!
    @IBOutlet weak var emotionImageView: UIImageView!
    @IBOutlet weak var stressLevelImageView: UIImageView!
    
    //sleeprecord
    @IBOutlet weak var sleepRecordLabel: UILabel!
    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet weak var dayRecordView: RoundUIView!
    @IBOutlet weak var dayRecordLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var emptyDayRecordView: UIView!
    
    
    
    // records arr
    var emotionValue = [String]()
    var Created_at = [String]()
    var content = [String]()
    var yValueFinal = [Double]()
    
    var sleepQuality = [String]()
    var sleepDuration = [String]()
    var actualSleepTime = [String]()
    var bedtime = [String]()
    var tofallAsleep = [String]()
    var sleepCycle = [String]()
    var emotionBeforeSleep = [String]()
    var sleepCountCycle = [Int]()
    
    var created_at = [String]()
    var start = [String]()
    var end = [String]()
    var sleepTime = [String]()
    var recordsCount = 0
    
    var selectedDate: String!
    var dayInt = 0
    var monthInt = 0
    var yearInt = 0
    var maxNumberOfMonth = 0
    var month: String!
    
    var targetDate: Date?
    var labelUpdateTimer: Timer?
    var timeLeftFormatter: DateComponentsFormatter?
    
    var months: [String]!
    var currentEmotion = ""
    var currentEmotionImageUrlStr = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpRecordView()
        tableView.delegate = self
        tableView.dataSource = self
        reload()
        loadMoodStream(dateSelected: self.selectedDate, isMoodStreamReload: false)
        self.emptyStateView.isHidden = true
        blankView.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
         getDarkmode()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? MoodStreamViewController {
            itemDetails.selectedDate = selectedDate
            itemDetails.currentEmotion = self.currentEmotion
            itemDetails.currentEmotionImageUrlStr = self.currentEmotionImageUrlStr
        }
    }
    
    @IBAction func viewRecord(_ sender: UIButton) {
        performSegue(withIdentifier: "showViewRecord", sender: self)
    }
    
    @IBAction func nextDay(_ sender: UIButton) {
        blankView.isHidden = false
        reload()
        dayInt+=1
        chartView.leftAxis.axisMaximum = 6.0

        // next Button func
        if dayInt > maxNumberOfMonth {
            dayInt = 0
            monthInt+=1
            dayInt+=1
            self.dayLabel.text = "\(dayInt)"
            self.monthLabel.text = "\(convert(month: monthInt).uppercased()) \(yearInt)"
            reload()
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: String(dayInt))
        } else {
            self.dayLabel.text = "\(dayInt)"
            self.monthLabel.text = "\(convert(month: monthInt).uppercased()) \(yearInt)"
        }

        // API Call when Next Button is Clicked
        if monthInt < 10 && dayInt < 10  {
            let month = String(format: "%02d", monthInt)
            let day = String(format: "%02d", dayInt)
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: month, day: day)
        } else if monthInt < 10 {
            let month = String(format: "%02d", monthInt)
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: month, day: String(dayInt))
        } else if dayInt < 10 {
            let day = String(format: "%02d", dayInt)
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: day)
        } else {
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: String(dayInt))
        }
    }
    
    @IBAction func previous(_ sender: UIButton) {
        blankView.isHidden = false
        reload()
        dayInt-=1
        
        if dayInt == 0 {
            monthInt-=1
            self.convertMonth(maxNumber: monthInt)
            dayInt = maxNumberOfMonth
        }
        
        self.dayLabel.text = "\(dayInt)"
        self.monthLabel.text = "\(convert(month: monthInt).uppercased()) \(String(yearInt))"
        
        // API Call when Next Button is Clicked
        if monthInt < 10 && dayInt < 10  {
            let month = String(format: "%02d", monthInt)
            let day = String(format: "%02d", dayInt)
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: month, day: day)
        } else if monthInt < 10 {
            let month = String(format: "%02d", monthInt)
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: month, day: String(dayInt))
        } else if dayInt < 10 {
            let day = String(format: "%02d", dayInt)
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: day)
        } else {
            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: String(dayInt))
        }
    }
}

extension NewMoodStreamViewController {
    private func setUpRecordView() {
        // trim selected day string
        let day = String(selectedDate!.dropFirst(8))
        let trimMonth = String(selectedDate!.dropFirst(5))
        let month = String(trimMonth.dropLast(3))
        let year = String(selectedDate!.dropLast(6))
        var displayDayOnly = Int(day) ?? 0
        
        if displayDayOnly < 10 {
            displayDayOnly = Int(day.dropFirst()) ?? 0
        }
        
        dayLabel.text = "\(displayDayOnly)"
        monthLabel.text = "\(convert(month: Int(month) ?? 0).uppercased()) \(year)"
        dayInt = Int(day) ?? 0
        monthInt = Int(month) ?? 0
        yearInt = Int(year) ?? 0
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?){
        super.traitCollectionDidChange(previousTraitCollection)
        getDarkmode()
    }
    
    private func getDarkmode(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                recordView.backgroundColor = UIColor.white
                dayRecordView.backgroundColor = UIColor.white
                dayLabel.textColor = UIColor.black
                monthLabel.textColor = UIColor.black
                emotionLabel.textColor = UIColor.black
                activityStatusLabel.textColor = hexStringToUIColor(hex: "#8395A7")
                stressLevelLabel.textColor = UIColor.black
                dayRecordLabel.textColor = UIColor.black
                sleepRecordLabel.textColor = UIColor.black
                nextButton.setImage(UIImage(named: "ic_next_button.png"), for: .normal)
                previousButton.setImage(UIImage(named: "ic_previous_button.png"), for: .normal)
                break
            case .dark:
                // dark mode detected
                 recordView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)
                dayRecordView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)
                dayLabel.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_DATE_MOODSTREAM)
                monthLabel.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_DATE_MOODSTREAM)
                emotionLabel.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_MOODSTREAM)
                activityStatusLabel.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_MOODSTREAM)
                stressLevelLabel.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_MOODSTREAM)
                dayRecordLabel.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_MOODSTREAM)
                sleepRecordLabel.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_MOODSTREAM)
                nextButton.setImage(UIImage(named: "ic_arrow_left_circle.png"), for: .normal)
                previousButton.setImage(UIImage(named: "ic_arrow_right_circle.png"), for: .normal)
                 
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func convert(month: Int) -> String {
        switch month {
        case 1:
            maxNumberOfMonth = 31
            return NSLocalizedString("January", comment: "")
        case 2:
            maxNumberOfMonth = 28
            return NSLocalizedString("February", comment: "")
        case 3:
            maxNumberOfMonth = 31
            return NSLocalizedString("March", comment: "")
        case 4:
            maxNumberOfMonth = 30
            return NSLocalizedString("April", comment: "")
        case 5:
            maxNumberOfMonth = 31
            return NSLocalizedString("May", comment: "")
        case 6:
            maxNumberOfMonth = 30
            return NSLocalizedString("June", comment: "")
        case 7:
            maxNumberOfMonth = 31
            return NSLocalizedString("July", comment: "")
        case 8:
            maxNumberOfMonth = 31
            return NSLocalizedString("August", comment: "")
        case 9:
            maxNumberOfMonth = 30
            return NSLocalizedString("September", comment: "")
        case 10:
            maxNumberOfMonth = 31
            return NSLocalizedString("October", comment: "")
        case 11:
            maxNumberOfMonth = 30
            return NSLocalizedString("November", comment: "")
        case 12:
            maxNumberOfMonth = 31
            return NSLocalizedString("December", comment: "")
        default:
            break;
        }
        
        return NSLocalizedString("January", comment: "")
    }
    
    private func convertMonth(maxNumber: Int) {
        switch maxNumber {
        case 1:
            maxNumberOfMonth = 31
        case 2:
            maxNumberOfMonth = 28
        case 3:
            maxNumberOfMonth = 31
        case 4:
            maxNumberOfMonth = 30
        case 5:
            maxNumberOfMonth = 31
        case 6:
            maxNumberOfMonth = 30
        case 7:
            maxNumberOfMonth = 31
        case 8:
            maxNumberOfMonth = 31
        case 9:
            maxNumberOfMonth = 30
        case 10:
            maxNumberOfMonth = 31
        case 11:
            maxNumberOfMonth = 30
        case 12:
            maxNumberOfMonth = 31
        default:
            break
        }
    }
    
    private func reload() {
        self.created_at.removeAll()
        self.end.removeAll()
        self.sleepTime.removeAll()
        self.sleepQuality.removeAll()
        self.sleepDuration.removeAll()
        self.actualSleepTime.removeAll()
        self.bedtime.removeAll()
        self.tofallAsleep.removeAll()
        self.sleepCycle.removeAll()
        self.recordsCount = 0
        self.emotionValue.removeAll()
        self.Created_at.removeAll()
        self.content.removeAll()
        self.yValueFinal.removeAll()
        self.emotionBeforeSleep.removeAll()
        self.sleepCountCycle.removeAll()
        self.tableView.reloadData()
    }
}

// MARK: -- CHART GRAPH
extension NewMoodStreamViewController {
    
    private func setUpChart() {
        print("after : \(self.Created_at)")
        prepareChart(yValue: self.yValueFinal, xValue: self.Created_at)
    }
    
    private func setUpEmotionValue(emotionValue: String) {
        switch emotionValue {
        case "Sad":
            self.yValueFinal.append(1.0)
        case "Unplesant":
            self.yValueFinal.append(2.0)
        case "Calm":
            self.yValueFinal.append(3.0)
        case "Pleasant":
            self.yValueFinal.append(4.0)
        case "Happy":
            self.yValueFinal.append(5.0)
        default:
            self.yValueFinal.append(3.0)
        }
    }
    
    private func prepareChart(yValue : [Double], xValue: [String]) {
        
        let xAxis = self.chartView.xAxis
        xAxis.labelPosition = .bottom
        chartView.rightAxis.enabled = false
        chartView.leftAxis.axisMinimum = 0.0
        chartView.leftAxis.axisMaximum = 5.2
    //    chartView.leftAxis.axisMaximum = 6.0
        chartView.leftAxis.valueFormatter = MyLeftAxisFormatter()
        chartView.leftAxis.drawLabelsEnabled = false
        chartView.isUserInteractionEnabled = false

        var lineChartEntry = [ChartDataEntry]()
        
        for i in 0..<yValue.count {
            let value = ChartDataEntry(x: Double(i), y: yValue[i])
            lineChartEntry.append(value)
        }
        
        let line1 = LineChartDataSet(entries: lineChartEntry, label: nil)
        
        // setting of gradient color
        let gradientColors = [UIColor.cyan.cgColor, UIColor.clear.cgColor] as CFArray
        let colorLocations:[CGFloat] = [1.0, 0.0]
        let gradient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations)
        line1.fill = Fill.fillWithLinearGradient(gradient!, angle: 90.0)
        line1.drawFilledEnabled = true
        
        line1.drawValuesEnabled = false
        line1.drawCirclesEnabled = false
        line1.colors = [hexStringToUIColor(hex: "#54D8FF")]
        line1.mode = .cubicBezier
        line1.cubicIntensity = 0.2
        
        let data = LineChartData()
        data.addDataSet(line1)
        
        chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: xValue)
        chartView.xAxis.granularity = 1
        chartView.xAxis.labelFont = UIFont(name: "HelveticaNeue-Light", size: 7.5)!
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                break
            case .dark:
                // dark mode detected
                chartView.xAxis.labelTextColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_MOODSTREAM)
                break
            }
        } else {
            // Fallback on earlier versions
        }
        chartView.data = data
        chartView.xAxis.labelRotationAngle = 270
        self.chartView.notifyDataSetChanged()
       
//        chartView.leftAxis.drawLabelsEnabled = false
//        chartView.zoom(scaleX: 0.0, scaleY: 0.0, x: 0.0, y: 0.0)
//        chartView.setVisibleXRangeMaximum(10)
    }
}
// MARK: -- CHART GRAPH 2
class MyLeftAxisFormatter: NSObject, IAxisValueFormatter {
    var labels: [Double : String] = [1.0 : "", 2.0 : "", 3.0 : "", 4.0 : "", 5.0 : ""]
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return labels[Double((value).rounded())] ?? ""
    }
}

extension NewMoodStreamViewController {
    
    private func loadMoodStream(dateSelected: String, isMoodStreamReload: Bool, year: String? = nil, month: String? = nil, day: String? = nil) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let token = KeychainSwift().get("apiToken") ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        var url = URL(string: "")
        if isMoodStreamReload == true {
            selectedDate = "\(year!)-\(month!)-\(day!)"
            url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record/sleep-list?date=\(String(describing: year!))-\(String(describing: month!))-\(String(describing: day!))&timezone=\(localTimeZoneName)")
        } else {
            url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record/sleep-list?date=\(dateSelected)&timezone=\(localTimeZoneName)")
        }
        
        print("test url: \(url!)")

        var request = URLRequest(url: url!)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                guard let json = userObject as? [String: Any] else { return  }
                let statusCode = json["status"] as? Int ?? 0
                
                // record data
                guard let jsonData = json["data"] as? [String: Any] else { return }
                let stressLevel = jsonData["average_stress"] as? String ?? ""
                let emotion = jsonData["average_emotion"] as? String ?? ""
                let imagefilePath =  jsonData["average_emotion_path"] as? String ?? ""
                let posts = jsonData["posts"] as? [String] ?? [""]
                
                print("stressLevel", stressLevel)

                // sleep mode record data
                if let sleep_mode = jsonData["sleep_mode"] as? [[String: Any]] {
                    for records in sleep_mode {
                        let sleep_quality = records["quality"] as? String ?? ""
                        let sleep_time = records["sleep_time"] as? String ?? ""
                        let cycles = records["cycles"] as? String ?? ""
                        let end = records["end"] as? String ?? ""
                        let created_at = records["created_at"] as? String ?? ""
                        let to_fall_asleep = records["to_fall_asleep"] as? String ?? ""
                        let emotion_before_sleep = records["emotion_before_sleep"] as? String ?? "".capitalized
                        let sleep_cycle_count = records["sleep_cycle_count"] as? Int ?? 0
                        
                        self.created_at.append(created_at)
                        self.end.append(end)
                        self.sleepQuality.append(sleep_quality)
                        self.sleepCycle.append(cycles)
                        self.sleepTime.append(sleep_time)
                        self.tofallAsleep.append(to_fall_asleep)
                        self.emotionBeforeSleep.append(emotion_before_sleep)
                        self.sleepCountCycle.append(sleep_cycle_count)
                        
                        
                    }
                }
                
                if let records = jsonData["records"] as? [[String: Any]] {
                    for recordData in records {
                        let emotion_value = recordData["emotion_value"] as? String ?? ""
                        let created_at = recordData["created_at"] as? String ?? ""
                        let content = recordData["content"] as? String ?? ""
                        let newStr = String(created_at.suffix(8))
                        
                        self.setUpEmotionValue(emotionValue: emotion_value)
                        self.emotionValue.append(emotion_value)
                        self.content.append(content)
                        self.recordsCount = recordData.count
                        
                        // old solution
                        let dateAsString = newStr
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.timeZone = TimeZone.current
                        dateFormatter.dateFormat = "HH:mm:ss"
                        let date = dateFormatter.date(from: dateAsString)

                        print("before 1: \(date)")
                        print("timezone 1: \(dateFormatter.timeZone)")

                        let newDf = DateFormatter()
                        newDf.timeZone = TimeZone.current
                        newDf.dateFormat = "h:mm a"
                        let DateFinal = newDf.string(from: date!)

                        print("before 2: \(DateFinal)")
                        print("timezone 2: \(newDf.timeZone)")
                        self.Created_at.append(DateFinal)
                    }
                }

                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: nil)
                    DispatchQueue.main.async {
                        self.blankView.isHidden = true
                        self.setUpChart()
                        self.hideSleepRecordText()
                        self.setUpEmptyStateOfRecord()
                        self.tableView.reloadData()
                        self.setUpRecordView(emotion: emotion, stressLevel: stressLevel, imageFilePath: imagefilePath, posts: posts)
                    }
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        DispatchQueue.main.async {
                            self.blankView.isHidden = true
                            self.setUpEmptyStateOfRecord()
                        }
                    })
                } else if statusCode == 401 {
                    SVProgressHUD.dismiss(completion: {
                        self.blankView.isHidden = true
                        self.emptyStateView.isHidden = false
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        self.blankView.isHidden = true
                        self.emptyStateView.isHidden = false
                    })
                }
                
        }}.resume()
    }
    
    private func setUpStressLevel(indicator: String) {
        if indicator == "Low" {
            self.stressLevelLabel.text = NSLocalizedString("Low", comment: "")
        } else if indicator == "Mild" {
            self.stressLevelLabel.text = NSLocalizedString("Mild", comment: "")
        } else if indicator == "Normal" {
            self.stressLevelLabel.text = NSLocalizedString("Normal", comment: "")
        } else if indicator == "High" {
           self.stressLevelLabel.text = NSLocalizedString("High", comment: "")
        } else {
            //
        }
    }
    
    private func setUpRecordView(emotion: String, stressLevel: String, imageFilePath: String, posts: [String]) {
        let imageUrlString = "\(Constants.Routes.BASE_URL_RESOURCE)\(imageFilePath.replacingOccurrences(of: " ", with: "%20"))"
        self.setUpCurrentMood(emotion: emotion.capitalized)
        self.setUpStressLevel(indicator: stressLevel.capitalized)
        self.activityStatusLabel.text = posts.joined(separator: ",")
        self.emotionImageView.sd_setImage(with: URL(string: imageUrlString), completed: nil)
        stressLevelHeart(indicator: stressLevel.capitalized)
        
        self.currentEmotion = emotion.capitalized
        self.currentEmotionImageUrlStr = imageUrlString
    }
    
    private func setUpCurrentMood(emotion: String) {
        
        if UserDefaults.standard.string(forKey: "selectedLanguage") == "en" {
            
            switch emotion {
            case "Excitement":
                self.emotionLabel.text = "You were mostly \(NSLocalizedString("Excitement", comment: ""))"
            case "Happy":
                self.emotionLabel.text = "You were mostly \(NSLocalizedString("Happy", comment: ""))"
            case "Zen":
                self.emotionLabel.text = "You were mostly \(NSLocalizedString("Zen", comment: ""))"
            case "Pleasant":
                self.emotionLabel.text = "You were mostly \(NSLocalizedString("Pleasant", comment: ""))"
            case "Calm":
                self.emotionLabel.text = "You were mostly \(NSLocalizedString("Calm", comment: ""))"
            case "Unpleasant":
                self.emotionLabel.text = "You were mostly \(NSLocalizedString("Unpleasant", comment: ""))"
            case "Confused":
                self.emotionLabel.text = "You were mostly \(NSLocalizedString("Confused", comment: ""))"
            case "Challenged":
                self.emotionLabel.text = "You were mostly \(NSLocalizedString("Challenged", comment: ""))"
            case "Tense":
                self.emotionLabel.text = "You were mostly \(NSLocalizedString("Tense", comment: ""))"
            case "Sad":
                self.emotionLabel.text = "You were mostly \(NSLocalizedString("Sad", comment: ""))"
            case "Anxious":
                self.emotionLabel.text = "You were mostly \(NSLocalizedString("Anxious", comment: ""))"
            case "Loading":
                self.emotionLabel.text = "You were mostly \(NSLocalizedString("Loading", comment: ""))"
            default:
                break
            }
        } else {
            
        switch emotion {
           case "Excitement":
               self.emotionLabel.text = "\(NSLocalizedString("You were mostly", comment: "")) \(NSLocalizedString("Excitement", comment: "")) \(NSLocalizedString("It was an emotion", comment: ""))"
           case "Happy":
               self.emotionLabel.text = "\(NSLocalizedString("You were mostly", comment: "")) \(NSLocalizedString("Happy", comment: "")) \(NSLocalizedString("It was an emotion", comment: ""))"
           case "Zen":
               self.emotionLabel.text = "\(NSLocalizedString("You were mostly", comment: "")) \(NSLocalizedString("Zen", comment: "")) \(NSLocalizedString("It was an emotion", comment: ""))"
           case "Pleasant":
               self.emotionLabel.text = "\(NSLocalizedString("You were mostly", comment: "")) \(NSLocalizedString("Pleasant", comment: "")) \(NSLocalizedString("It was an emotion", comment: ""))"
           case "Calm":
               self.emotionLabel.text = "\(NSLocalizedString("You were mostly", comment: "")) \(NSLocalizedString("Calm", comment: "")) \(NSLocalizedString("It was an emotion", comment: ""))"
           case "Unpleasant":
               self.emotionLabel.text = "\(NSLocalizedString("You were mostly", comment: "")) \(NSLocalizedString("Unpleasant", comment: "")) \(NSLocalizedString("It was an emotion", comment: ""))"
           case "Confused":
               self.emotionLabel.text = "\(NSLocalizedString("You were mostly", comment: "")) \(NSLocalizedString("Confused", comment: "")) \(NSLocalizedString("It was an emotion", comment: ""))"
           case "Challenged":
               self.emotionLabel.text = "\(NSLocalizedString("You were mostly", comment: "")) \(NSLocalizedString("Challenged", comment: "")) \(NSLocalizedString("It was an emotion", comment: ""))"
           case "Tense":
               self.emotionLabel.text = "\(NSLocalizedString("You were mostly", comment: "")) \(NSLocalizedString("Tense", comment: "")) \(NSLocalizedString("It was an emotion", comment: ""))"
           case "Sad":
               self.emotionLabel.text = "\(NSLocalizedString("You were mostly", comment: "")) \(NSLocalizedString("Sad", comment: "")) \(NSLocalizedString("It was an emotion", comment: ""))"
           case "Anxious":
               self.emotionLabel.text = "\(NSLocalizedString("You were mostly", comment: "")) \(NSLocalizedString("Anxious", comment: "")) \(NSLocalizedString("It was an emotion", comment: ""))"
           case "Loading":
               self.emotionLabel.text = "\(NSLocalizedString("You were mostly", comment: "")) \(NSLocalizedString("Loading", comment: "")) \(NSLocalizedString("It was an emotion", comment: ""))"
           default:
               break
           }
        }
    }
    
    private func setUpEmptyStateOfRecord() {
        
        if self.recordsCount == 0 {
            DispatchQueue.main.async {
                self.emptyStateView.isHidden = false
            }
        } else {
            DispatchQueue.main.async {
                self.emptyStateView.isHidden = true
            }
        }
    
        if self.Created_at.count == 0 {
            DispatchQueue.main.async {
                self.dayRecordView.isHidden = true
                self.emptyDayRecordView.isHidden = false
            }
        } else {
            DispatchQueue.main.async {
                self.dayRecordView.isHidden = false
                self.emptyDayRecordView.isHidden = true
            }
        }
        
        if self.recordsCount == 0 && self.Created_at.count == 0 && self.created_at.count == 0 {
            DispatchQueue.main.async {
//                self.tableView.isScrollEnabled = false
                self.dayRecordView.isHidden = true
                self.emptyDayRecordView.isHidden = true
                self.dayRecordLabel.isHidden = true
                self.sleepRecordLabel.isHidden = true
            }
        }
    }
    
    private func stressLevelHeart(indicator: String) {
        if indicator == "Low" {
            self.stressLevelImageView.image = UIImage(named: "ic_heart_blue")
        } else if indicator == "Mild" {
            self.stressLevelImageView.image = UIImage(named: "ic_heart_green")
        } else if indicator == "Normal" {
            self.stressLevelImageView.image = UIImage(named: "ic_heart_yellow")
        } else if indicator == "High" {
            self.stressLevelImageView.image = UIImage(named: "ic_heart_red")
        } else {
            self.stressLevelImageView.image = UIImage(named: "ic_heart_blue")
        }
    }
    
    private func setUpBedTime(cell: NewMoodStreamViewControllerCell, indexPath: IndexPath) {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = TimeZone.current
        df.locale = Locale.current
        
        if let date2 = df.date(from: self.created_at[indexPath.row]) {
            print("bedTime: - \(date2)")
            cell.bedtimeLabel.text = "\(date2.toString(dateFormat: "hh:mm a"))"
        }
    }
    
    private func setUpActualSleepTime(cell: NewMoodStreamViewControllerCell, indexPath: IndexPath) {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = TimeZone.current
        df.locale = Locale.current
        
        if self.sleepTime[indexPath.row].count == 0 {
            //
        } else {
            if let date2 = df.date(from: self.sleepTime[indexPath.row]) {
                cell.actualSleepTimeLabel.text = "\(date2.toString(dateFormat: "hh:mm a"))"
            }
        }
    }
    
    private func setUpSleepDuration(cell: NewMoodStreamViewControllerCell, indexPath: IndexPath) {
        if self.created_at[indexPath.row].count == 0 {
            //
        } else if self.end[indexPath.row].count == 0 {
            //
        } else {
            cell.sleepDurationLabel.text = self.mathToSleepDuration(start: self.created_at[indexPath.row], end: self.end[indexPath.row])
        }
    }
    
//    private func setUpSleepCycle(cell: NewMoodStreamViewControllerCell, indexPath: IndexPath) {
//        if self.sleepCountCycle.count < 2 {
//            cell.sleepCycleLabel.text = "\(self.sleepCountCycle[indexPath.row]) cycle"
//        } else {
//            cell.sleepCycleLabel.text = "\(self.sleepCountCycle[indexPath.row]) cycles"
//        }
//    }
    
    private func setUpSleepQuality(cell: NewMoodStreamViewControllerCell, indexPath: IndexPath) {
        
        let fl = Float(self.sleepQuality[indexPath.row]) ?? 0.0
        
        if (fl > 0 && fl < 0.04) {
            cell.sleepQualityLabel.text = NSLocalizedString("Perfect", comment: "")
            cell.sleepQualityLabel.textColor = .green
        } else if (fl > 0.04 && fl <= 0.08) {
            cell.sleepQualityLabel.text = NSLocalizedString("Good", comment: "")
            cell.sleepQualityLabel.textColor = .blue
        } else if (fl > 0.08 && fl <= 0.12) {
            cell.sleepQualityLabel.text = NSLocalizedString("Bad", comment: "")
            cell.sleepQualityLabel.textColor = .yellow
        } else {
            cell.sleepQualityLabel.text = NSLocalizedString("Worst", comment: "")
            cell.sleepQualityLabel.textColor = .red
        }
    }
    
    private func mathToFallAsSleep(start: String, end: String) -> String {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = TimeZone.current
        df.locale = Locale.current
        
        let date1 = df.date(from: start)
        let date2 = df.date(from: end)

        if date1 == nil || date2 == nil {
            // do nothing
        } else {
            let d1 = date1!.timeIntervalSince1970
            let d2 = date2!.timeIntervalSince1970
            
            let diff = Int(d2) - Int(d1)
            let hours = diff / 3600
            let minutes = (diff - hours * 3600) / 60
            
            if hours == 0 {
                if minutes == 1 {
                    return("\(minutes) \(NSLocalizedString("min", comment: ""))")
                } else {
                    return("\(minutes) \(NSLocalizedString("mins", comment: ""))")
                }
            } else if minutes == 0 {
                return "0 \(NSLocalizedString("mins", comment: ""))"
            } else {
                if hours == 1 {
                    return("\(hours) \(NSLocalizedString("hr", comment: "")) \(minutes) \(NSLocalizedString("min", comment: ""))")
                } else {
                    return("\(hours) \(NSLocalizedString("hrs", comment: "")) \(minutes) \(NSLocalizedString("mins", comment: ""))")
                }
            }
        }
        
        return ""
    }
    
    private func mathToSleepDuration(start: String, end: String) -> String {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = TimeZone.current
        df.locale = Locale.current
        
        let endDateFormatter = DateFormatter()
        endDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        endDateFormatter.timeZone = TimeZone.current
        endDateFormatter.locale = Locale.current
        
        let date1 = df.date(from: start)
        let date2 = endDateFormatter.date(from: end)
        
        if date1 == nil || date2 == nil {
            // do nothing
        } else {
            let d1 = date1!.timeIntervalSince1970
            let d2 = date2!.timeIntervalSince1970
            
            let diff = Int(d2) - Int(d1)
            let hours = diff / 3600
            let minutes = (diff - hours * 3600) / 60
            
            if hours == 0 {
                if minutes == 1 {
                    return("\(minutes) \(NSLocalizedString("min", comment: ""))")
                } else {
                    return("\(minutes) \(NSLocalizedString("mins", comment: ""))")
                }
            } else if minutes == 0 {
                return "0 \(NSLocalizedString("mins", comment: ""))"
            } else {
                if hours == 1 {
                    return("\(hours) \(NSLocalizedString("hr", comment: "")) \(minutes) \(NSLocalizedString("min", comment: ""))")
                } else {
                    return("\(hours) \(NSLocalizedString("hrs", comment: "")) \(minutes) \(NSLocalizedString("mins", comment: ""))")
                }
            }
        }
        
        return ""
    }
    
    private func setUpTimeToFallAsleep(cell: NewMoodStreamViewControllerCell, indexPath: IndexPath) {
        cell.toFallAsleepLabel.text = self.tofallAsleep[indexPath.row]
    }
    
    private func hideSleepRecordText() {
        if sleepCountCycle.count == 0 {
            DispatchQueue.main.async {
                self.sleepRecordLabel.isHidden = true
            }
        } else {
            DispatchQueue.main.async {
                self.sleepRecordLabel.isHidden = false
            }
        }
    }
    
    private func setUpWokeUp(cell: NewMoodStreamViewControllerCell, indexPath: IndexPath) {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = TimeZone.current
        df.locale = Locale.current
        
        if let date = df.date(from: self.end[indexPath.row]) {
            cell.wokeUpLabel.text = "\(date.toString(dateFormat: "hh:mm a"))"
        }
    }
    
    private func setUpEmotionBeforeSleep(cell: NewMoodStreamViewControllerCell, indexPath: IndexPath) {
        // do constraints
        DispatchQueue.main.async {
            if self.emotionBeforeSleep.isEmpty {
                cell.emotionBeforeSleepImageHeight.constant = 0.0
                cell.emotionBeforeSleepLabelHeight.constant = 0.0
                cell.emotionBeforeSleepWithValeHeight.constant = 0.0
            } else {
                cell.emotionBeforeSleepImageHeight.constant = 25.0
                cell.emotionBeforeSleepLabelHeight.constant = 25.0
                cell.emotionBeforeSleepWithValeHeight.constant = 25.0
                cell.emotionBeforeSleepLabel.text = self.currentMood(emotion: self.emotionBeforeSleep[indexPath.row].capitalized)
            }
        }
    }
    
    private func currentMood(emotion: String) -> String {
        switch emotion {
        case "Excitement":
            return NSLocalizedString("Excitement", comment: "")
        case "Happy":
            return NSLocalizedString("Happy", comment: "")
        case "Zen":
            return NSLocalizedString("Zen", comment: "")
        case "Pleasant":
            return NSLocalizedString("Pleasant", comment: "")
        case "Calm":
            return NSLocalizedString("Calm", comment: "")
        case "Unpleasant":
            return NSLocalizedString("Unpleasant", comment: "")
        case "Confused":
            return NSLocalizedString("Confused", comment: "")
        case "Challenged":
            return NSLocalizedString("Challenged", comment: "")
        case "Tense":
            return NSLocalizedString("Tense", comment: "")
        case "Sad":
            return NSLocalizedString("Sad", comment: "")
        case "Anxious":
            return NSLocalizedString("Anxious", comment: "")
        case "Loading":
            return NSLocalizedString("Loading", comment: "")
        default:
            return ""
        }
    }
    
    private func setUpDateGather(cell: NewMoodStreamViewControllerCell, indexPath: IndexPath) {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = TimeZone.current
        df.locale = Locale.current

        if let date1 = df.date(from: self.created_at[indexPath.row]) {
            if let date2 = df.date(from: self.end[indexPath.row]) {
                let dateStart = date1.toString(dateFormat: "MMMM dd yyyy")
                let dateEnd = date2.toString(dateFormat: "MMMM dd yyyy")
                
                if dateStart == dateEnd {
                    if let date = df.date(from: self.end[indexPath.row]) {
                        cell.dateSleepRecordGathered.text = "\(date.toString(dateFormat: "MMMM dd yyyy"))"
                    }
                } else {
                    let dateOne = "\(date1.toString(dateFormat: "dd"))"
                    let dateTwo = "\(date2.toString(dateFormat: "dd"))"
                    let dateThree = "\(date1.toString(dateFormat: "yyyy"))"
                    let dateFour = "\(date1.toString(dateFormat: "MMMM"))"
                    cell.dateSleepRecordGathered.text = "\(dateFour) \(dateOne)-\(dateTwo), \(dateThree)"
                }
            }
        }
    }
    
    private func hideSleepRecordTexts(cell: NewMoodStreamViewControllerCell, indexPath: IndexPath) {
        if self.sleepCycle[indexPath.row] == "0" {
            cell.sleepQualityLabel.isHidden = true
            cell.sleepDurationLabel.isHidden = true
            cell.actualSleepTimeLabel.isHidden = true
            cell.toFallAsleepLabel.isHidden = true
//            cell.sleepCycleLabel.isHidden = true
            cell.wokeUpLabel.isHidden = true
            cell.emotionBeforeSleepLabel.isHidden = true
            cell.nonValueSleepQualityLabel.isHidden = true
            cell.nonValueSleepQualityImageView.isHidden = true
            cell.nonValueSleepDutaionLabel.isHidden = true
            cell.nonValueSleepDurationImageView.isHidden = true
            cell.nonValueActualSleepTimeLabel.isHidden = true
            cell.nonValueActualSleepTimeImageView.isHidden = true
            cell.nonValueToFallAsleepLabel.isHidden = true
            cell.nonValueToFallAsleepImageView.isHidden = true
//            cell.nonValueSleepCycleLabel.isHidden = true
//            cell.nonValueSleepCycleImageView.isHidden = true
            cell.nonValueWokeUpImageView.isHidden = true
            cell.nonValueWokeUpLabel.isHidden = true
            cell.nonValueEmotionBeforeSleepImageView.isHidden = true
            cell.nonValueEmotionBeforeSleepLabel.isHidden = true
            cell.nonValueEmptyLabel.isHidden = false
            
        } else {
            cell.sleepQualityLabel.isHidden = false
            cell.sleepDurationLabel.isHidden = false
            cell.actualSleepTimeLabel.isHidden = false
            cell.toFallAsleepLabel.isHidden = false
//            cell.sleepCycleLabel.isHidden = false
            cell.wokeUpLabel.isHidden = false
            cell.emotionBeforeSleepLabel.isHidden = true
            cell.nonValueSleepQualityLabel.isHidden = false
            cell.nonValueSleepQualityImageView.isHidden = false
            cell.nonValueSleepDutaionLabel.isHidden = false
            cell.nonValueSleepDurationImageView.isHidden = false
            cell.nonValueActualSleepTimeLabel.isHidden = false
            cell.nonValueActualSleepTimeImageView.isHidden = false
            cell.nonValueToFallAsleepLabel.isHidden = false
            cell.nonValueToFallAsleepImageView.isHidden = false
//            cell.nonValueSleepCycleLabel.isHidden = false
//            cell.nonValueSleepCycleImageView.isHidden = false
            cell.nonValueWokeUpImageView.isHidden = false
            cell.nonValueWokeUpLabel.isHidden = false
            cell.nonValueEmotionBeforeSleepImageView.isHidden = false
            cell.nonValueEmotionBeforeSleepLabel.isHidden = false
            cell.nonValueEmptyLabel.isHidden = true
            
//            if UserDefaults.standard.string(forKey: "selectedLanguage") == "en" {
//                cell.nonValueSleepCycleLabel.text = "Sleep Cycle"
//            } else {
//                cell.nonValueSleepCycleLabel.text = "睡眠サイクル"
//            }
        }
        
        if self.emotionBeforeSleep[indexPath.row].isEmpty {
            cell.nonValueEmotionBeforeSleepLabel.isHidden = true
            cell.nonValueEmotionBeforeSleepImageView.isHidden = true
            cell.emotionBeforeSleepLabel.isHidden = true
        } else {
            cell.nonValueEmotionBeforeSleepLabel.isHidden = false
            cell.nonValueEmotionBeforeSleepImageView.isHidden = false
            cell.emotionBeforeSleepLabel.isHidden = false
        }
    }
    private func getDarkmodeInCell(cell : NewMoodStreamViewControllerCell){
        
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                break
            case .dark:
                // dark mode detected
                cell.sleepRecordView.backgroundColor  = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
}

extension NewMoodStreamViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewMoodStreamViewControllerCell", for: indexPath) as! NewMoodStreamViewControllerCell
        
        self.hideSleepRecordTexts(cell: cell, indexPath: indexPath)
        self.setUpBedTime(cell: cell, indexPath: indexPath)
        self.setUpSleepQuality(cell: cell, indexPath: indexPath)
        self.setUpSleepDuration(cell: cell, indexPath: indexPath)
        self.setUpActualSleepTime(cell: cell, indexPath: indexPath)
        self.setUpTimeToFallAsleep(cell: cell, indexPath: indexPath)
//        self.setUpSleepCycle(cell: cell, indexPath: indexPath)
        self.setUpWokeUp(cell: cell, indexPath: indexPath)
        self.setUpEmotionBeforeSleep(cell: cell, indexPath: indexPath)
        self.setUpDateGather(cell: cell, indexPath: indexPath)
        self.getDarkmodeInCell(cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return created_at.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.sleepCycle[indexPath.row] == "0" {
            return 170.0
        }
        
        if self.emotionBeforeSleep[indexPath.row].isEmpty {
            return 305.0
        } else {
            return 348.0
        }
    }
}
