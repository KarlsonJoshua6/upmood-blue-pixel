//
//  ChangePasswordViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 30/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var oldPasswordTextField: ACFloatingTextfield!
    @IBOutlet weak var newPasswordTextField: ACFloatingTextfield!
    @IBOutlet weak var confirmPasswordTextField: ACFloatingTextfield!
    
    // computed
    
    var oldPassword: String {
        return oldPasswordTextField.text ?? ""
    }
    
    var newPassword: String {
        return newPasswordTextField.text ?? ""
    }
    
    var confirmPassword: String {
        return confirmPasswordTextField.text ?? ""
    }
    
    var didClickShowOldPassword = true
    var didClickShowNewPassword = true
    var didClickShowConfirmPassword = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        oldPasswordTextField.delegate = self
        newPasswordTextField.delegate = self
        confirmPasswordTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }

    @IBAction func showOldPassword(_ sender: UIButton) {
        if didClickShowOldPassword == true {
            oldPasswordTextField.isSecureTextEntry = false
            sender.setTitle("HIDE", for: .normal)
            
            if oldPasswordTextField.isFirstResponder {
                oldPasswordTextField.becomeFirstResponder()
                oldPasswordTextField.layoutIfNeeded()
            }
            
        } else {
            oldPasswordTextField.isSecureTextEntry = true
            sender.setTitle(NSLocalizedString("SHOW", comment: ""), for: .normal)
        }
        
        didClickShowOldPassword = !didClickShowOldPassword
    }
    
    @IBAction func showNewPassword(_ sender: UIButton) {
        if didClickShowNewPassword == true {
            newPasswordTextField.isSecureTextEntry = false
            sender.setTitle(NSLocalizedString("HIDE", comment: ""), for: .normal)
            
            if newPasswordTextField.isFirstResponder {
                newPasswordTextField.becomeFirstResponder()
                newPasswordTextField.layoutIfNeeded()
            }
            
        } else {
            newPasswordTextField.isSecureTextEntry = true
            sender.setTitle(NSLocalizedString("SHOW", comment: ""), for: .normal)
        }
        
        didClickShowNewPassword = !didClickShowNewPassword
    }
    
    @IBAction func showConfirmPassword(_ sender: UIButton) {
        if didClickShowConfirmPassword == true {
            confirmPasswordTextField.isSecureTextEntry = false
            sender.setTitle(NSLocalizedString("HIDE", comment: ""), for: .normal)
            
            if confirmPasswordTextField.isFirstResponder {
                confirmPasswordTextField.becomeFirstResponder()
                confirmPasswordTextField.layoutIfNeeded()
            }
            
        } else {
            confirmPasswordTextField.isSecureTextEntry = true
            sender.setTitle(NSLocalizedString("SHOW", comment: ""), for: .normal)
        }
        
        didClickShowConfirmPassword = !didClickShowConfirmPassword
    }
    
    @IBAction func ChangePassword(_ sender: RoundButton) {
        if oldPassword.isEmpty || newPassword.isEmpty || confirmPassword.isEmpty {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString(Constants.MessageDialog.INCOMPLETE_FORM, comment: ""))
        } else if newPassword != confirmPassword {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: Constants.MessageDialog.PASSWORD_MISMATCH)
        } else if !newPassword.isValidPassword() || !confirmPassword.isValidPassword() {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "your password must include at least one symbol and be 8 more characters long ")
        } else {
            changePassword(oldPassword: oldPassword, newPassword: newPassword, confirmPassword: confirmPassword)
        }
    }
}

// MARK - Stored Network Request

extension ChangePasswordViewController {
    fileprivate func changePassword(oldPassword: String, newPassword: String, confirmPassword: String) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.updateUserPassword(oldPassword: oldPassword, newPassword: newPassword, confirmPassword: confirmPassword)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: "Alright!", message: "Successfully changed password.")
                        this?.oldPasswordTextField.text = ""
                        this?.newPasswordTextField.text = ""
                        this?.confirmPasswordTextField.text = ""
                    })
                } else if statusCode == 422 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Old Password not match")
                    })
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
}

extension ChangePasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.oldPasswordTextField {
            self.newPasswordTextField.becomeFirstResponder()
        }
        
        if textField == self.newPasswordTextField {
            self.confirmPasswordTextField.becomeFirstResponder()
        }
        
        if textField.returnKeyType == UIReturnKeyType.done {
            view.endEditing(true)
        }
        
        return true
    }
}
