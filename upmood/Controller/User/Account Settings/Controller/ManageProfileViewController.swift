//
//  ManageProfileViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 28/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import UIKit

class ManageProfileViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var menu:[[String:String]] = []
    private var selectedRows:[IndexPath] = []
    private var didUserSelectOnGroupPrivacy = false
    
    // helper
    var privacyProfileDetails: Int!
    var privacyEmotion: Int!
    var privacyHeartBeat: Int!
    var privacyStressLevel: Int!
    var groupId: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        privacyProfileDetails = UserDefaults.standard.integer(forKey: "privacyManagePrivacy")
        privacyEmotion = UserDefaults.standard.integer(forKey: "emotionManagePrivacy")
        privacyHeartBeat = UserDefaults.standard.integer(forKey: "heartbeatManagePrivacy")
        privacyStressLevel = UserDefaults.standard.integer(forKey: "stressLevelManagerPrivacy")
        
        menu = [["menu": NSLocalizedString("Profile Details", comment: "")],["menu": NSLocalizedString("Current Emotion", comment: "")],["menu":NSLocalizedString("Heart rate", comment: "")], ["menu":NSLocalizedString("Stress level", comment: "")], ["menu": NSLocalizedString("Show all", comment: "")],["menu":NSLocalizedString("Hide all", comment: "")]]
      
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelection = true
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParentViewController {
            performSegue(withIdentifier: "reload", sender: self)
        }
    }
    
    // MARK - Actions
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Functions
    
    func removeAllIndexPath() -> [IndexPath] {
        var indexPaths: [IndexPath] = []
        for j in 0..<tableView.numberOfRows(inSection: 0) - 5 {
            indexPaths.append(IndexPath(row: j, section: 0))
        }
        
        return indexPaths
    }
    
    func getAllIndexPaths() -> [IndexPath] {
        var indexPaths: [IndexPath] = []
        for j in 0..<tableView.numberOfRows(inSection: 0) - 2 {
            indexPaths.append(IndexPath(row: j, section: 0))
        }
        
        return indexPaths
    }
    
    private func loadCell(cell: AccountSettingTableViewCell, indexpath: IndexPath) {
        if self.privacyProfileDetails ?? 0 == 1 {
            if indexpath.row == 0 {
                cell.checkMark.isHidden = false
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if self.privacyEmotion ?? 0 == 1 {
            if indexpath.row == 1 {
                cell.checkMark.isHidden = false
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if self.privacyHeartBeat ?? 0 == 1 {
            if indexpath.row == 2 {
                cell.checkMark.isHidden = false
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if self.privacyStressLevel ?? 0 == 1 {
            if indexpath.row == 3 {
                cell.checkMark.isHidden = false
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
    }
    
    private func setUpcell(cell: AccountSettingTableViewCell, indexPath: IndexPath) {
        cell.checkMark.isHidden = true
        loadCell(cell: cell, indexpath: indexPath)
        
        if indexPath.row == 4 && indexPath.row == 5 {
            cell.checkMark.isHidden = true
        }
    }
    
    private func manageDidDeSelect(cell: AccountSettingTableViewCell, index: IndexPath) {
        
        let fetchAutoReconnect = UserDefaults.standard.integer(forKey: "fetchAutoReconnect")
        switch index.row {
        case 0:
            cell.checkMark.isHidden = true
            cell.checkMark.image = UIImage(named: "")
            self.privacyProfileDetails = 0
            self.updateManageProfile(privacy: self.privacyProfileDetails, emotion: self.privacyEmotion, heartbeat: self.privacyHeartBeat, stress_level: self.privacyStressLevel, notification_type: "live", auto_reconnect: fetchAutoReconnect)
            break
        case 1:
            cell.checkMark.isHidden = true
            cell.checkMark.image = UIImage(named: "")
            self.privacyEmotion = 0
            self.updateManageProfile(privacy: self.privacyProfileDetails, emotion: self.privacyEmotion, heartbeat: self.privacyHeartBeat, stress_level: self.privacyStressLevel, notification_type: "live", auto_reconnect: fetchAutoReconnect)
            break
        case 2:
            cell.checkMark.isHidden = true
            cell.checkMark.image = UIImage(named: "")
            self.privacyHeartBeat = 0
            self.updateManageProfile(privacy: self.privacyProfileDetails, emotion: self.privacyEmotion, heartbeat: self.privacyHeartBeat, stress_level: self.privacyStressLevel, notification_type: "live", auto_reconnect: fetchAutoReconnect)
            break
        case 3:
            cell.checkMark.isHidden = true
            cell.checkMark.image = UIImage(named: "")
            self.privacyStressLevel = 0
            self.updateManageProfile(privacy: self.privacyProfileDetails, emotion: self.privacyEmotion, heartbeat: self.privacyHeartBeat, stress_level: self.privacyStressLevel, notification_type: "live", auto_reconnect: fetchAutoReconnect)
            break
        case 4:
            self.selectedRows = getAllIndexPaths()
            
            if index.row == 0 && index.row == 1 && index.row == 2 && index.row == 3{
                cell.checkMark.isHidden = false
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
            
            self.privacyProfileDetails = 1
            self.privacyEmotion = 1
            self.privacyHeartBeat = 1
            self.privacyStressLevel = 1
            self.updateManageProfile(privacy: self.privacyProfileDetails, emotion: self.privacyEmotion, heartbeat: self.privacyHeartBeat, stress_level: self.privacyStressLevel, notification_type: "live", auto_reconnect: fetchAutoReconnect)
            
            break
        case 5:
            self.selectedRows = removeAllIndexPath()
            
            if index.row == 0 && index.row == 1 && index.row == 2 && index.row == 3{
                cell.checkMark.isHidden = true
                cell.checkMark.image = UIImage(named: "")
            }
            
            self.privacyProfileDetails = 0
            self.privacyEmotion = 0
            self.privacyHeartBeat = 0
            self.privacyStressLevel = 0
            self.updateManageProfile(privacy: self.privacyProfileDetails, emotion: self.privacyEmotion, heartbeat: self.privacyHeartBeat, stress_level: self.privacyStressLevel, notification_type: "live", auto_reconnect: fetchAutoReconnect)
            self.tableView.reloadData()
            
            break
            
        default:
            break
        }
    }
    
    private func manageSelection(cell: AccountSettingTableViewCell, index: IndexPath) {
        let fetchAutoReconnect = UserDefaults.standard.integer(forKey: "fetchAutoReconnect")
        switch index.row {
        case 0:
            cell.checkMark.isHidden = false
            cell.checkMark.image = UIImage(named: "ic_check_mark")
            self.privacyProfileDetails = 1
            self.updateManageProfile(privacy: self.privacyProfileDetails, emotion: self.privacyEmotion, heartbeat: self.privacyHeartBeat, stress_level:  self.privacyStressLevel, notification_type: "live", auto_reconnect: fetchAutoReconnect)
            break
        case 1:
            cell.checkMark.isHidden = false
            cell.checkMark.image = UIImage(named: "ic_check_mark")
            self.privacyEmotion = 1
            self.updateManageProfile(privacy: self.privacyProfileDetails, emotion: self.privacyEmotion, heartbeat: self.privacyHeartBeat, stress_level:  self.privacyStressLevel, notification_type: "live", auto_reconnect: fetchAutoReconnect)
            break
        case 2:
            cell.checkMark.isHidden = false
            cell.checkMark.image = UIImage(named: "ic_check_mark")
            self.privacyHeartBeat = 1
            self.updateManageProfile(privacy: self.privacyProfileDetails, emotion: self.privacyEmotion, heartbeat: self.privacyHeartBeat, stress_level:  self.privacyStressLevel, notification_type: "live", auto_reconnect: fetchAutoReconnect)
            break
        case 3:
            cell.checkMark.isHidden = false
            cell.checkMark.image = UIImage(named: "ic_check_mark")
            self.privacyStressLevel = 1
            self.updateManageProfile(privacy: self.privacyProfileDetails, emotion: self.privacyEmotion, heartbeat: self.privacyHeartBeat, stress_level:  self.privacyStressLevel, notification_type: "live", auto_reconnect: fetchAutoReconnect)
            break
        default:
            break
        }
    }
}

extension ManageProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountSettingTableViewCell", for: indexPath) as? AccountSettingTableViewCell {
            cell.managePrivacyLabel.text = menu[indexPath.row]["menu"]
            setUpcell(cell: cell, indexPath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /// working

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? AccountSettingTableViewCell {
            if cell.checkMark.image == UIImage(named: "") {
                self.manageSelection(cell: cell, index: indexPath)
            } else {
                self.manageDidDeSelect(cell: cell, index: indexPath)
            }
        }
    }
}

extension ManageProfileViewController {
    private func updateManageProfile(privacy: Int, emotion: Int, heartbeat: Int, stress_level: Int, notification_type: String, auto_reconnect: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.updateManagePrivacy(privacy: privacy, emotion: emotion, heartbeat: heartbeat, stress_level: stress_level, notification_type: notification_type, auto_reconnect: auto_reconnect, auto_renew: 0)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                let message = jsonDic["message"] as? String ?? ""
                
                print("response updateManageProfile - \(jsonDic)")
                
                switch statusCode {
                case 200:
                
                    SVProgressHUD.dismiss(completion: {
                        UserDefaults.standard.set(privacy, forKey: "privacyManagePrivacy")
                        UserDefaults.standard.set(emotion, forKey: "emotionManagePrivacy")
                        UserDefaults.standard.set(heartbeat, forKey: "heartbeatManagePrivacy")
                        UserDefaults.standard.set(stress_level, forKey: "stressLevelManagerPrivacy")
                        
                        self.privacyProfileDetails = privacy
                        self.privacyEmotion = emotion
                        self.privacyHeartBeat = heartbeat
                        self.privacyStressLevel = stress_level
                        
                        print("success: \(privacy), \(emotion), \(heartbeat), \(stress_level)")
                        self.tableView.reloadData()
                    })
                    break
                case 204:
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Manage Privacy did not successfully update, Please try again.")
                    })
                    break
                default:
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: message)
                    })
                    break
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
}
