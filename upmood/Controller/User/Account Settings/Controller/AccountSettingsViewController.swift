//
//  AccountSettingsViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 24/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SDWebImage
import SVProgressHUD
import Firebase
import UIKit

let bluetoothManager = BluetoothManager.getInstance()

class AccountSettingsViewController: UIViewController {

    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    let myPlayer = Player.sharedInstance
    let langs = ["Please Select", "English", "Japanese"]
    var pickerView: UIPickerView?
    var selectedLanguage = ""
    
    fileprivate var menu = [NSLocalizedString("Device", comment: ""), "Watch Settings", NSLocalizedString("Manage Stickers", comment: ""), NSLocalizedString("Account Settings", comment: ""), NSLocalizedString("About Upmood", comment: ""), NSLocalizedString("Log Out", comment: "")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        displayUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.setUpSwitch()
        self.setUpLanguage()
        let guestUserName = UserDefaults.standard.string(forKey: "getGuestUserName") ?? ""
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
            DispatchQueue.main.async {
                self.userName.text = guestUserName
                self.profilePhoto.image = UIImage(named: "ic_empty_state_profile")
            }
        }
        
        navigationController?.navigationBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(AccountSettingsViewController.timeChangedNotification), name: NSNotification.Name.NSSystemTimeZoneDidChange, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NSSystemTimeZoneDidChange, object: nil)
    }
    
    @objc func timeChangedNotification(notification: NSNotification) {
       self.navigationController?.popToRootViewController(animated: false)
       let delegate = UIApplication.shared.delegate as? AppDelegate
       let storyboardName = "DashboardScreen"
       let storybaord = UIStoryboard(name: storyboardName, bundle: nil)
       delegate?.window?.rootViewController = storybaord.instantiateInitialViewController()
    }
    
    private func setUpLanguage() {
        
        let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
        
        if selectedLanguage == "en" {
            Bundle.set(language: "en")
        } else if selectedLanguage == "ja" {
            Bundle.set(language: "ja")
        } else {
            //
        }
    }

    private func displayUI() {
        let name = UserDefaults.standard.string(forKey: "fetchName") ?? ""
        userName.text = "Hello, \(name)!"
        guard let imageURL = URL(string: UserDefaults.standard.string(forKey: "fetchProfilePic") ?? "") else { return }
        profilePhoto.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], completed: nil)
    }
    
    private func setUpLogOut() {
    
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            
                let alert = UIAlertController(title: nil, message: NSLocalizedString("Are you sure you want to log out?", comment: ""), preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Log Out", comment: ""), style: .destructive , handler:{ [weak this = self] alert in
                    this?.logOut()
                }))
        
                alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler:{ (UIAlertAction)in
                    print("User click Dismiss button")
                }))
            
                self.present(alert, animated: true, completion: nil)
            
        case .pad:
            self.logOut()
        case .unspecified:
            break
        case .tv:
            break
        case .carPlay:
            break
//        case .mac:
//            break
        }
    }
    
    private func logOut() {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        
        view.isUserInteractionEnabled = false
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.logOut()).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                
                // temp lang to
//                removeAllSharedPref()
//                self.performSegue(withIdentifier: "LogOut", sender: self)
                
                switch statusCode {
                case 200:
                    SVProgressHUD.dismiss(completion: {
                        
                        if self.myPlayer.player != nil {
                            self.myPlayer.player.pause()
                            self.myPlayer.resetPlayer()
                        }
                        
                        self.view.isUserInteractionEnabled = true
                        removeAllSharedPref()
                        bluetoothManager.disconnectPeripheral()
                        self.performSegue(withIdentifier: "LogOut", sender: self)
                    })
                    break
                case 204:
                    SVProgressHUD.dismiss(completion: {
                        this?.view.isUserInteractionEnabled = true
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Can't Logged Out, Please check your internet connection.")
                    })
                    break
                case 422:
                    SVProgressHUD.dismiss(completion: {
                        this?.view.isUserInteractionEnabled = true
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "")
                    })
                    break
                default:
                    SVProgressHUD.dismiss(completion: {
                        this?.view.isUserInteractionEnabled = true
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "")
                    })
                    break
                }
        
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    this?.view.isUserInteractionEnabled = true
                })
                
                break
            }
        }
    }
    
    @IBAction func viewAndEditAccount(_ sender: UIButton) {
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
           presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "You're in guest mode, please sign up to access this menu.")
        } else {
            performSegue(withIdentifier: "showAndEditAccount", sender: self)
        }
    }
    
    @IBAction func reload(_ segue: UIStoryboardSegue) {
        self.viewDidLoad()
    }
    
    private func setUpSwitch() {
        let userNotificationType = UserDefaults.standard.string(forKey: "fetchUserNotiificationType") ?? ""
        if userNotificationType == "live" {
            self.notificationSwitch.isOn = true
        } else {
            self.notificationSwitch.isOn = false
        }
    }
    
    @IBAction func switchNotif(_ sender: UISwitch) {
        let privacy = UserDefaults.standard.integer(forKey: "privacyManagePrivacy")
        let emotion = UserDefaults.standard.integer(forKey: "emotionManagePrivacy")
        let heartBeat = UserDefaults.standard.integer(forKey: "heartbeatManagePrivacy")
        let auto_reconnect = UserDefaults.standard.integer(forKey: "fetchAutoReconnect")
        let stressLevel = UserDefaults.standard.integer(forKey: "stressLevelManagerPrivacy")
        
        if sender.isOn {
            switchNotification(withPrivacy: privacy, emotion: emotion, heartbeat: heartBeat, stress_level: stressLevel, notification_type: "live", auto_reconnect: auto_reconnect)
        } else {
            switchNotification(withPrivacy: privacy, emotion: emotion, heartbeat: heartBeat, stress_level: stressLevel, notification_type: "Off", auto_reconnect: auto_reconnect)
        }
    }
    
    @IBAction func showDevice(_ sender: UIButton) {
        performSegue(withIdentifier: "showDevice", sender: self)
    }
    
    @IBAction func showWatchSettings(_ sender: UIButton) {
        performSegue(withIdentifier: "showWatchSettings", sender: self)
    }
    
    @IBAction func showAccountSettings(_ sender: UIButton) {
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
           presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "You're in guest mode, please sign up to access this menu.")
        } else {
            performSegue(withIdentifier: "showAndEditAccount", sender: self)
        }
    }
    
    @IBAction func showStickers(_ sender: UIButton) {
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "You're in guest mode, please sign up to access this menu.")
        } else {
            let storyboard = UIStoryboard(name: "FriendScreen", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "StickerNavigationViewController")
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func showPrivacy(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "didClickAccountSetting")
        performSegue(withIdentifier: "showAccount", sender: self)
    }
    
    @IBAction func showLanguage(_ sender: UIButton) {
        let alert = UIAlertController(title: "Change Language", message: "\n\n\n\n", preferredStyle: .alert)
        alert.isModalInPopover = true
        
        pickerView = UIPickerView(frame: CGRect(x: 5, y: 20, width: 250, height: 110))
        
        alert.view.addSubview(pickerView!)
        pickerView?.dataSource = self
        pickerView?.delegate = self
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
        
        }))
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
            self.changeLanguage()
        }))
        
        self.present(alert,animated: true, completion: nil)
    }
    
    @IBAction func showAboutUpmood(_ sender: UIButton) {
        performSegue(withIdentifier: "About", sender: self)
    }
    
    @IBAction func LogOut(_ sender: UIButton) {
        setUpLogOut()
    }
}

extension AccountSettingsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return langs[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return langs.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedLanguage = langs[row]
    }
    
    private func changeLanguage() {
        
        let alert = UIAlertController(title: "Change language", message: "The connected band will be disconnected. Do you want continue?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
            UserDefaults.standard.set(true, forKey: "didUserChangeLanguage")
            
            if self.selectedLanguage != "Please Select" {
                if self.selectedLanguage == "English" {
                    Bundle.set(language: "en")
                    UserDefaults.standard.set("en", forKey: "selectedLanguage")
                } else {
                    Bundle.set(language: "ja")
                    UserDefaults.standard.set("ja", forKey: "selectedLanguage")
                }
            }

            NotificationCenter.default.post(name: .didTappedLogOut, object: nil, userInfo: ["data":"yes"])
            
            let storyboard = UIStoryboard.init(name: "DashboardScreen", bundle: nil)
            UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
            UserDefaults.standard.set(false, forKey: "didUserChangeLanguage")
            Constants.didUserChangeLanguage = true
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { (alert) in
            UserDefaults.standard.set(false, forKey: "didUserChangeLanguage")
            Constants.didUserChangeLanguage = false
            self.viewDidLoad()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func switchNotification(withPrivacy: Int, emotion: Int, heartbeat: Int, stress_level: Int, notification_type: String, auto_reconnect: Int) {
        Alamofire.request(APIClient.updateManagePrivacy(privacy: withPrivacy, emotion: emotion, heartbeat: heartbeat, stress_level: stress_level, notification_type: notification_type, auto_reconnect: auto_reconnect, auto_renew: 0)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                this?.dismiss(animated: true, completion: {
                    UserDefaults.standard.set(notification_type, forKey: "fetchUserNotiificationType")
                    print("success updated Account: \(json)")
                })
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
            }
        }
    }
}

public func publicLogOut() {
   let manager = Alamofire.SessionManager.default
   manager.session.configuration.timeoutIntervalForRequest = 3
   manager.request(APIClient.logOut()).responseJSON { response in
       switch response.result {
       case .success(let json):
           guard let jsonData = json as? [String: Any] else { return }
           let statusCode = jsonData["status"] as? Int ?? 0
           
           switch statusCode {
           case 200:
                debugPrint("successfully public logout - \(jsonData)")
               break
           default:
               break
           }
   
           break
       case .failure(let error):
           debugPrint(error.localizedDescription)
           break
       }
   }
}

public func removeAllSharedPref() {
    deleteFCMToken()
    KeychainSwift().delete("apiToken")
    UserDefaults.standard.removeObject(forKey: "fetchName")
    UserDefaults.standard.removeObject(forKey: "fetchEmail")
    UserDefaults.standard.removeObject(forKey: "fetchUserId")
    UserDefaults.standard.removeObject(forKey: "fetchUserToken")
    UserDefaults.standard.removeObject(forKey: "fetchUserGender")
    UserDefaults.standard.removeObject(forKey: "fetchUserBirthday")
    UserDefaults.standard.removeObject(forKey: "fetchUserCountry")
    UserDefaults.standard.removeObject(forKey: "fetchUsername")
    UserDefaults.standard.removeObject(forKey: "fetchWeight")
    UserDefaults.standard.removeObject(forKey: "fetchFacebookId")
    UserDefaults.standard.removeObject(forKey: "fetchStatus")
    UserDefaults.standard.removeObject(forKey: "profile_post")
    UserDefaults.standard.removeObject(forKey: "currentTheme")
    UserDefaults.standard.removeObject(forKey: "fetchNotificationType")
    UserDefaults.standard.removeObject(forKey: "didFinishloadNotification")
    UserDefaults.standard.removeObject(forKey: "privacyManagePrivacy")
    UserDefaults.standard.removeObject(forKey: "emotionManagePrivacy")
    UserDefaults.standard.removeObject(forKey: "heartbeatManagePrivacy")
    UserDefaults.standard.removeObject(forKey: "stressLevelManagerPrivacy")
    UserDefaults.standard.removeObject(forKey: "fetchGroupIdInEdit")
    UserDefaults.standard.removeObject(forKey: "fetchPrivacyEmotion")
    UserDefaults.standard.removeObject(forKey: "fetchPrivacyProfileDetails")
    UserDefaults.standard.removeObject(forKey: "fetchPrivacyHeartBeat")
    UserDefaults.standard.removeObject(forKey: "emotionSet")
    UserDefaults.standard.removeObject(forKey: "integerBPM")
    UserDefaults.standard.removeObject(forKey: "emotionValueString")
    UserDefaults.standard.removeObject(forKey: "stressLevelString")
    UserDefaults.standard.removeObject(forKey: "fetchPPI")
    UserDefaults.standard.removeObject(forKey: "totalPPI")
    UserDefaults.standard.removeObject(forKey: "fetchUserNotiificationType")
    UserDefaults.standard.removeObject(forKey: "fetchAutoReconnect")
    UserDefaults.standard.removeObject(forKey: "fetchStressLevel")
    UserDefaults.standard.removeObject(forKey: "fetchEmotions")
    UserDefaults.standard.removeObject(forKey: "selectedEmotions")
    UserDefaults.standard.removeObject(forKey: "basic_emoji_set")
    UserDefaults.standard.removeObject(forKey: "offlineCalculation")
    UserDefaults.standard.removeObject(forKey: "has_trial")
    UserDefaults.standard.removeObject(forKey: "subscription_status")
    //        UserDefaults.standard.removeObject(forKey: "notificationValue")
    NotificationCenter.default.post(name: .didTappedLogOut, object: nil, userInfo: ["data":"yes"])
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "clearDataFitness"), object: nil)
    //bluetoothManager.disconnectPeripheral()
    UserDefaults.standard.removeObject(forKey: "isGuest")
    UserDefaults.standard.removeObject(forKey: "getGuestUserName")
    UserDefaults.standard.removeObject(forKey: "status")
    UserDefaults.standard.removeObject(forKey: "lover_name")
    UserDefaults.standard.removeObject(forKey: "profileCalories")
    UserDefaults.standard.removeObject(forKey: "profileSteps")
    UserDefaults.standard.removeObject(forKey: "lastDateCalories")
    UserDefaults.standard.removeObject(forKey: "profileCalories")
    UserDefaults.standard.removeObject(forKey: "lastDateStep")
    UserDefaults.standard.removeObject(forKey: "profileSteps")
    
    UIApplication.shared.unregisterForRemoteNotifications()
    
    if Constants.firstTimeUser == true {
        Constants.firstTimeUser = false
    }
    
    Constants.nativeUser = false
    Constants.didUserDisconnectedToBand = false
    deleteFCMToken()
}

public func deleteFCMToken() {
    let instance = InstanceID.instanceID()
    instance.deleteID { (error) in
        print(error.debugDescription)
    }
    
    UserDefaults.standard.removeObject(forKey: "fcmToken")
}
