//
//  ProfileViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 28/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import CountryList
import DatePickerDialog
import FBSDKCoreKit
import FBSDKLoginKit
import KeychainSwift
import SDWebImage
import SVProgressHUD
import SwiftEntryKit
import RLBAlertsPickers
import UIKit

class ProfileViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableView2: UITableView!
    @IBOutlet weak var facebookButton: RoundButton!
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var profilePhotoThumb: UIImageView!
    @IBOutlet weak var imgEmpty: UIImageView!
    
    fileprivate var menu1 = [NSLocalizedString("Name", comment: ""), NSLocalizedString("Gender", comment: ""), NSLocalizedString("Birthday", comment: ""),NSLocalizedString("Weight", comment: ""), NSLocalizedString("Select a Country", comment: "")]
    //fileprivate var menu1 = [NSLocalizedString("Name", comment: ""), NSLocalizedString("Gender", comment: ""), NSLocalizedString("Birthday", comment: ""), NSLocalizedString("Select a Country", comment: "")]
    
    fileprivate var menu2 = [NSLocalizedString("Username", comment: ""), NSLocalizedString("Email Address", comment: ""), NSLocalizedString("Password", comment: "")]
    
    var countryList = CountryList()
    var imagePickedBlock: ((UIImage) -> Void)?
    var selectedDate = Date()
    let imagePicker = UIImagePickerController()
    let maleStr = NSLocalizedString("Male", comment: "")
    let femaleStr =  NSLocalizedString("Female", comment: "")
    var genderPickerData = [String]()
    var selectedGender: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter()
        tableView.delegate = self
        tableView.dataSource = self
        tableView2.delegate  = self
        tableView2.dataSource = self
        displayUI()
        genderPickerData = ["Please Select Gender", maleStr, femaleStr]
        
        let facebookId = UserDefaults.standard.string(forKey: "fetchFacebookId") ?? ""
        checkIfUserLoggedToFacebook(facebookId: facebookId, sender: facebookButton)
        countryList.delegate = self
        customView.isHidden = true
        setUserInterfaceStyleLight(self: self)
    }
    
    func NotificationCenter(){
        
        Foundation.NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "tableVReloadData"), object: nil)
        
        Foundation.NotificationCenter.default.addObserver(self, selector: #selector(self.tableVReload), name: NSNotification.Name(rawValue: "tableVReloadData"), object: nil)
    }
    
    @objc func tableVReload(_ notification: NSNotification) {
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
//        if Constants.nativeUser == false {
//            // facebook users
//            facebookButton.isHidden = true
//        } else {
//            // native users
//
//        }
        
        facebookButton.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParentViewController {
            if !UserDefaults.standard.bool(forKey: "didClickAccountSetting") {
                performSegue(withIdentifier: "reload", sender: self)
            }
            
            UserDefaults.standard.set(false, forKey: "didClickAccountSetting")
        }
    }
    
    private func displayToast() {
//        customView.isHidden = false
//        var attributes = EKAttributes.topToast
//        attributes.entryBackground = .color(color: .white)
//        attributes.entranceAnimation = .translation
//        attributes.exitAnimation = .translation
//        SwiftEntryKit.display(entry: self.customView, using: attributes)
//        guard let imageURL = URL(string: UserDefaults.standard.string(forKey: "fetchProfilePic") ?? "") else { return }
//        profilePhotoThumb.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], completed: nil)
    }
    
    private func displayUI() {
        
        let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
        let fetchUserGender = UserDefaults.standard.string(forKey: "fetchUserGender")
        
        if selectedLanguage == "en" {
           if fetchUserGender == "男" {
               UserDefaults.standard.set("Male", forKey: "fetchUserGender")
           } else if fetchUserGender == "女" {
               UserDefaults.standard.set("Female", forKey: "fetchUserGender")
           } else {
               debugPrint("do nothing")
           }
        } else {
            if fetchUserGender == "Male" {
                UserDefaults.standard.set("男", forKey: "fetchUserGender")
            } else if fetchUserGender == "Female"{
                UserDefaults.standard.set("女", forKey: "fetchUserGender")
            } else {
                debugPrint("do nothing")
            }
        }
        
        
        let filePath = UserDefaults.standard.string(forKey: "fetchProfilePic") ?? ""
        if let imageURL = URL(string: "\(filePath)") {
            profilePhoto.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], completed: nil)
            imgEmpty.isHidden = true
            
        }else{
            print("empty gaga")
            imgEmpty.isHidden = false
        }
    }
    
    // setUP Facebook Button
    private func checkIfUserLoggedToFacebook(facebookId: String, sender: RoundButton) {
        if facebookId.isEmpty {
            sender.setTitle(NSLocalizedString("Facebook Connect", comment: ""), for: .normal)
        } else {
            sender.setTitle("\(NSLocalizedString("Disconnect", comment: "")) Facebook", for: .normal)
        }
    }
    
    private func checkEmail(email: String) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: "#0099A3"))
        Alamofire.request(APIClient.checkEmail(email: email)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: Constants.MessageDialog.EMAIL_ALREADY_TAKEN)
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        self.update(email: email)
                    })
                } else if statusCode == 419 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Invalid email address.")
                    })
                } else if statusCode == 426 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: Constants.MessageDialog.EMAIL_ALREADY_TAKEN)
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    // setUP Change Email
    fileprivate func changeEmail() {
        let alert = UIAlertController(title: NSLocalizedString("Email", comment: ""), message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            let email = UserDefaults.standard.string(forKey: "fetchEmail") ?? ""
            textField.text = email
            textField.placeholder = "Enter Email"
        }
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Change", comment: ""), style: .default, handler: { [weak alert, weak this = self] (_) in
            let email = alert?.textFields![0].text ?? ""
            if email.isEmpty {
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString(Constants.MessageDialog.INCOMPLETE_FORM, comment: ""))
            } else if !email.isValidEmail() {
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: Constants.MessageDialog.INVALID_EMAIL_FORMAT)
            } else {
                this?.checkEmail(email: email)
            }
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // setUP Change Name
    fileprivate func changeName() {
        let alert = UIAlertController(title: NSLocalizedString("Profile Name", comment: ""), message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            let name = UserDefaults.standard.string(forKey: "fetchName") ?? ""
            textField.text = name
            textField.placeholder = "Enter Username"
        }

        alert.addAction(UIAlertAction(title: NSLocalizedString("Change", comment: ""), style: .default, handler: { [weak alert, weak this = self] (_) in
            let name = alert?.textFields![0].text ?? ""
            this?.change(name: name)
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // setUP Change UserName
    fileprivate func changeUsername() {
        let alert = UIAlertController(title: NSLocalizedString("Username", comment: ""), message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            let name = UserDefaults.standard.string(forKey: "fetchUsername") ?? ""
            textField.text = name
            textField.placeholder = "Enter Name"
        }
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Change", comment: ""), style: .default, handler: { [weak alert, weak this = self] (_) in
            let username = alert?.textFields![0].text ?? ""
            this?.checkAvailablilityOfUser(name: username)
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // setUP Country Picker
    fileprivate func countryPicker() {
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
        
    }
    
    // setUP Gender Picker
    fileprivate func genderPicker() {
        let alertView = UIAlertController(title: NSLocalizedString("Select Gender", comment: ""), message: "\n\n\n\n\n\n", preferredStyle: .alert)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 50, width: 260, height: 80))
        pickerView.dataSource = self
        pickerView.delegate = self
        
        alertView.view.addSubview(pickerView)
        let action = UIAlertAction(title: NSLocalizedString("Change", comment: ""), style: UIAlertActionStyle.default, handler: { [weak this = self] alertView in
            
            let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
            if selectedLanguage == "ja" {
               if self.selectedGender == "男" {
                   self.selectedGender = "Male"
               } else if self.selectedGender == "女" {
                   self.selectedGender = "Female"
               } else {
                    debugPrint("do nothing")
               }
            }
            
            this?.changeUserGender(gender: self.selectedGender ?? "Please Select Gender")
            pickerView.reloadAllComponents()
        })
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        
        alertView.addAction(action)
        alertView.addAction(cancel)
        
        present(alertView, animated: true, completion: {
            pickerView.frame.size.width = alertView.view.frame.size.width
        })
    }
    
    // setUP change weight
       fileprivate func changeWeight() {
           let alert = UIAlertController(title: NSLocalizedString("Weight", comment: ""), message: nil, preferredStyle: .alert)
           alert.addTextField { (textField) in
               let weight = UserDefaults.standard.string(forKey: "fetchWeight") ?? ""
               textField.keyboardType = .numberPad
               textField.text = weight
               textField.delegate = self
               textField.placeholder = "Enter Weight"
           }

           alert.addAction(UIAlertAction(title: NSLocalizedString("Change", comment: ""), style: .default, handler: { [weak alert, weak this = self] (_) in
               let weight = alert?.textFields![0].text ?? ""
               this?.changeWeight(weight: weight)
           }))
           
           alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }
    
    // setUP DatePicker
    fileprivate func datePickerTapped() {

//        limit to 18 year old and below
//
//        var components = DateComponents()
//        components.year = -100
//        let minDate = Calendar.current.date(byAdding: components, to: Date())
//
//        components.year = -5
//        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
        let alert = UIAlertController(style: .actionSheet, title: NSLocalizedString("Select date", comment: ""))
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: nil) { date in
            self.selectedDate = date
        }
        
        alert.addAction(image: nil, title: NSLocalizedString("Change", comment: ""), color: nil, style: .default, isEnabled: true) { [weak this = self] alert in
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "ja-JP")
            formatter.dateFormat = "yyyy-MM-dd"
            let day = formatter.string(from: self.selectedDate)
            this?.changeUserBirth(day: day)
        }
        
        alert.addAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // setUP TableViews
    fileprivate func setUpTableView(cell: AcountSettingsTableViewCell, indexPath: IndexPath) {
        let name = UserDefaults.standard.string(forKey: "fetchName") ?? ""
        let gender = UserDefaults.standard.string(forKey: "fetchUserGender") ?? ""
        let birthday = UserDefaults.standard.string(forKey: "fetchUserBirthday") ?? ""
        let weight = UserDefaults.standard.string(forKey: "fetchWeight") ?? ""
        let country = UserDefaults.standard.string(forKey: "fetchUserCountry") ?? ""
        let dataToDisplay = [name, gender, convertBirthday(bday: birthday), weight + " Kg", country]
//        let dataToDisplay = [name, gender, birthday, country]
        
        print("TEST ITO KUNG NAWALA: \(dataToDisplay[indexPath.row])")
        
        cell.personalInformationDataLabel.text = dataToDisplay[indexPath.row]
    }
    
    func convertBirthday(bday: String) ->String{
        let d1 = bday.components(separatedBy: "/")
        
        var monthStr = ""
        var bdayStr = ""
        
        switch d1[0]{
        case "01":
            monthStr = "Jan"
        case "02":
            monthStr = "Feb"
        case "03":
            monthStr = "Mar"
        case "04":
            monthStr = "Apr"
        case "05":
            monthStr = "May"
        case "06":
            monthStr = "Jun"
        case "07":
            monthStr = "Jul"
        case "08":
            monthStr = "Aug"
        case "09":
            monthStr = "Sep"
        case "10":
            monthStr = "Oct"
        case "11":
            monthStr = "Nov"
        case "12":
            monthStr = "Dec"
        default:
        break
        }
        
        bdayStr = monthStr + " " + d1[1] + "," + " " + d1[2]
            
        return bdayStr
    }
    
    fileprivate func setUpTableView2(cell: AccountSettingTableViewCell, indexPath: IndexPath) {
        let username = UserDefaults.standard.string(forKey: "fetchUsername") ?? ""
        let email = UserDefaults.standard.string(forKey: "fetchEmail") ?? ""
        let dataToDisplay = [username, email, ""]
        cell.profileDataLabel.text = dataToDisplay[indexPath.row]
    }
    
    @IBAction func didTappedUserProfilePic(_ sender: UIButton) {
        let alert = UIAlertController(title: NSLocalizedString("Please Select", comment: ""), message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Take Photo", comment: ""), style: .default , handler:{ [weak this = self] UIAlertAction in
            this?.showCamera(picker: self.imagePicker)
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Browse Photo", comment: ""), style: .default , handler:{ [weak this = self] UIAlertAction in
            this?.showPhotoGallery(picker: self.imagePicker)
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func connectToFacebook(_ sender: RoundButton) {
        
        let facebookId = UserDefaults.standard.string(forKey: "fetchFacebookId") ?? ""
        
        if facebookId.isEmpty {
            sender.setTitle(NSLocalizedString("Facebook Connect", comment: ""), for: .normal)
            self.linkAccountToFacebook()
        } else {
            sender.setTitle("\(NSLocalizedString("Disconnect", comment: "")) Facebook", for: .normal)
            self.unlinkAccountToFacebook()
        }
    }
    
    @IBAction func deactivateAccount(_ sender: RoundButton) {
        let alert = UIAlertController(title: "Deactivate Account", message: "We are sad that you want to leave us, But please note that your account will be deactivated. When you log in again, your account will be automatically reactivated. Do you want to continue?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default, handler: { [weak this = self] alert in
            this?.deactivateAccount()
        }))
        
        present(alert, animated: true, completion: nil)
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "AcountSettingsTableViewCell", for: indexPath) as? AcountSettingsTableViewCell {
                cell.personalInformationLabel.text = menu1[indexPath.row]
                setUpTableView(cell: cell, indexPath: indexPath)
                return cell
            }
            break
        case 2:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountSettingTableViewCell", for: indexPath) as? AccountSettingTableViewCell {
                cell.profileLabel.text = menu2[indexPath.row]
                setUpTableView2(cell: cell, indexPath: indexPath)
                return cell
            }
            break
        default: break
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 1:
            return menu1.count;
        case 2:
            return menu2.count;
        default: return 0;
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 1:
            return 1
        case 2:
            return 1
        default: return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 1:
            
            switch indexPath.row {
            case 0:
                changeName()
                break
            case 1:
                genderPicker()
                break
            case 2:
                datePickerTapped()
                break
            case 3:
                changeWeight()
                break
            case 4:
                countryPicker()
                break
            default: break
            }
            
            break
        case 2:
            
            switch indexPath.row {
            case 0:
                changeUsername()
                break
            case 1:
                changeEmail()
                break
            case 2:
                performSegue(withIdentifier: "changePassword", sender: self)
                break
            default: break
                
            }
            
        default: break
        }
    }
}

// MARK - Stored Country Picker

extension ProfileViewController: CountryListDelegate {
    func selectedCountry(country: Country) {
        self.changeUserCountry(place: country.name ?? "")
    }
}

// MARK: - Stored UIPickerViewDelegate, UIPickerViewDataSource For Gender Picker

extension ProfileViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedGender = genderPickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderPickerData[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderPickerData.count
    }
}

// MARK: - Stored UIImagePickerControllerDelegate,

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if var image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            image = self.imageOrientation(image)
//            self.imagePickedBlock?(image)
            self.uploadSelectedImage(image: image, url: "\(Constants.Routes.BASE)/api/v4/ios/user/profile/updateProfile")
        } else {
            print("Something went wrong")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK - Stored ImagePicker Extensions Func

extension ProfileViewController {
    fileprivate func showCamera(picker: UIImagePickerController) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            picker.delegate = self;
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    fileprivate func showPhotoGallery(picker: UIImagePickerController) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            picker.delegate = self;
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }
    }
}

// MARK - Stored Network Request

extension ProfileViewController {
    
    fileprivate func deactivateAccount() {
        view.isUserInteractionEnabled = true
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.deactivateAccount()).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                SVProgressHUD.dismiss(completion: {
                    print("success: \(json)")
                    this?.performSegue(withIdentifier: "logOut", sender: self)
                    this?.view.isUserInteractionEnabled = true
                })
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    this?.view.isUserInteractionEnabled = true
                })
                
                break
            }
        }
    }
    
    fileprivate func unlinkAccountToFacebook() {
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.unlinkAccountToFacebook()).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                SVProgressHUD.dismiss(completion: {
                    print("success: \(json)")
                    UserDefaults.standard.set("", forKey: "fetchFacebookId")
                    this?.viewDidLoad()
                    this?.displayToast()
                    this?.view.isUserInteractionEnabled = true
                })
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    this?.view.isUserInteractionEnabled = true
                })
                
                break
            }
        }
    }
    
    fileprivate func linkAccountToFacebook() {
        LoginManager().logOut()
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: self) { loginResult in
            switch loginResult {
            case .success(_):
                GraphRequest(graphPath: "me", parameters: ["fields": "email, id, name, picture.type(large)"]).start(completionHandler: { [weak this = self] (connection, result, error) -> Void in
                    if error == nil {
                        guard let resultDictionary = result as? [String: Any] else { return }
                        let email = resultDictionary["email"] as? String ?? ""
                        let userId = resultDictionary["id"] as? String ?? ""
                        print("data from facebook: \(email), \(userId)")
                        this?.connectAccountToFacebookToApi(facebookId: Int(userId) ?? 0, email: email)
                    }
                })
                
                break
            case .cancelled:
                print("cancelled")
                break
            case .failed(let error):
                print("error \(error.localizedDescription)")
                break
            }
        }
    }
    
    fileprivate func connectAccountToFacebookToApi(facebookId: Int, email: String) {
        if email.isEmpty {
            view.isUserInteractionEnabled = false
            presentDismissableAlertController(title: Constants.MessageDialog.errorTitle, message: "Failed to link Facebook, Facebook Account has no email")
        } else {
            view.isUserInteractionEnabled = false
            SVProgressHUD.show()
            SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: "#0099A3"))
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 3
            manager.request(APIClient.linkAccountToFacebook(facebookId: facebookId, email: email)).responseJSON { [weak this = self] response in
                switch response.result {
                case .success(let json):
                    guard let jsonData = json as? [String: Any] else { return }
                    let statusCode = jsonData["status"] as? Int ?? 0
                    guard let data = jsonData["data"] as? [String: Any] else { return }
                    
                    if statusCode == 200 {
                        SVProgressHUD.dismiss(completion: {
                            UserDefaults.standard.set(facebookId, forKey: "fetchFacebookId")
                            this?.viewDidLoad()
                            this?.displayToast()
                            this?.view.isUserInteractionEnabled = true
                        })
                    } else if statusCode == 204 {
                        SVProgressHUD.dismiss(completion: {
                            this?.view.isUserInteractionEnabled = true
                            this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No user existed")
                        })
                    } else if statusCode == 432 {
                        SVProgressHUD.dismiss {
                            this?.view.isUserInteractionEnabled = true
                            this?.showForceLinkToFacebookDialog(message: "FB account already linked to another account, do you want to force link?", facebookId: facebookId, email: email)
                        }
                    } else if statusCode == 433 {
                        SVProgressHUD.dismiss {
                            this?.view.isUserInteractionEnabled = true
                            this?.showForceLinkToFacebookDialog(message: "Have FB upmood account, do you want to force link?", facebookId: facebookId, email: email)
                        }
                    } else {
                        debugPrint("what happen: - \(json)")
                    }
                
                    break
                case .failure(let error):
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                        this?.view.isUserInteractionEnabled = true
                    })
                     
                    break
                }
            }
        }
    }
    
    private func showForceLinkToFacebookDialog(message: String, facebookId: Int, email: String) {
        
       let alert = UIAlertController(title: "Warning", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            SVProgressHUD.dismiss(completion: { [weak this = self] in
                this?.forceLinkNativeAccountToFbAccount(withFacebookId: facebookId, email: email)
            })
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { _ in
            // do nothing
        }))

        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func forceLinkNativeAccountToFbAccount(withFacebookId: Int, email: String) {
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: "#0099A3"))
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.forceLinkAccountToFacebook(facebookId: withFacebookId, email: email)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        UserDefaults.standard.set(withFacebookId, forKey: "fetchFacebookId")
                        this?.viewDidLoad()
                        this?.displayToast()
                        this?.view.isUserInteractionEnabled = true
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.view.isUserInteractionEnabled = true
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                print("JSON: \(json)")
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    this?.view.isUserInteractionEnabled = true
                })
                
                break
            }
        }
    }
    
    
    private func forceLogOut() {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.logOut()).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                
                switch statusCode {
                case 200:
                    SVProgressHUD.dismiss(completion: {
                        this?.performSegue(withIdentifier: "logOut", sender: self)
                    })
                    break
                case 204:
                    SVProgressHUD.dismiss(completion: {
                        this?.view.isUserInteractionEnabled = true
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Can't Logged Out, Please check your internet connection.")
                    })
                    break
                case 422:
                    SVProgressHUD.dismiss(completion: {
                        this?.view.isUserInteractionEnabled = true
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "")
                    })
                    break
                default:
                    SVProgressHUD.dismiss(completion: {
                        this?.view.isUserInteractionEnabled = true
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "")
                    })
                    break
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    this?.view.isUserInteractionEnabled = true
                })
                
                break
            }
        }
    }

    
    fileprivate func changeUserGender(gender: String) {
        if gender == "Please Select Gender" {
            
        } else {
            view.isUserInteractionEnabled = false
            SVProgressHUD.show()
            SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 3
            manager.request(APIClient.updateProfileGender(gender: gender)).responseJSON { [weak this = self] response in
                switch response.result {
                case .success(let json):
                    guard let jsonData = json as? [String: Any] else { return }
                    //guard let data = jsonData["data"] as? [String: Any] else { return }
                    
                    SVProgressHUD.dismiss(completion: {
                        var genderParam = gender
                        let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
                        if selectedLanguage == "ja" {
                           if self.selectedGender == "Male" {
                               genderParam = "男"
                           } else if self.selectedGender == "Female" {
                               genderParam = "女"
                           } else {
                               debugPrint("do nothing")
                           }
                        }
                        
                        UserDefaults.standard.set(genderParam, forKey: "fetchUserGender")
                        this?.tableView.reloadData()
                        this?.displayToast()
                        this?.view.isUserInteractionEnabled = true
                        self.selectedGender = "Please Select Gender"
                    })
                    break
                case .failure(let error):
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                        this?.view.isUserInteractionEnabled = true
                    })
                    
                    break
                }
            }
        }
    }
    
    fileprivate func changeUserCountry(place: String) {
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.updateProfileCountry(place: place)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                //guard let data = jsonData["data"] as? [String: Any] else { return }
                
                SVProgressHUD.dismiss(completion: {
                    UserDefaults.standard.set(place, forKey: "fetchUserCountry")
                    this?.tableView.reloadData()
                    this?.displayToast()
                    this?.view.isUserInteractionEnabled = true
                })
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    this?.view.isUserInteractionEnabled = true
                })
               
                break
            }
        }
    }
    
    private func setUpDate(birthday: String) {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let oldDate = df.date(from: birthday)
        
        let df2 = DateFormatter()
        df2.dateFormat = "MM/dd/yyyy"
        let newDate = df2.string(from: oldDate ?? Date())
        print("new Date po ito: \(newDate)")
        UserDefaults.standard.set(newDate, forKey: "fetchUserBirthday")
    }
    
    fileprivate func changeUserBirth(day: String) {
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.udpateProfileUser(birthday: day)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
               // guard let data = jsonData["data"] as? [String: Any] else { return }
//                let statusCode = data["status"] as? Int ?? 0
//                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        this?.setUpDate(birthday: day)
                        this?.tableView.reloadData()
                        this?.displayToast()
                        this?.view.isUserInteractionEnabled = true
                    })
                
                print("json response update birthday: \(jsonData)")
                
//                } else {
//                    SVProgressHUD.dismiss(completion: {
//                        this?.view.isUserInteractionEnabled = true
//                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
//                    })
//                }

                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    this?.view.isUserInteractionEnabled = true
                })
                
                break
            }
        }
    }
    
    fileprivate func changeUser(name: String) {
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.updateProfileUser(name: name)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                //guard let data = jsonData["data"] as? [String: Any] else { return }
                print("test here po: \(jsonData)")
                
                switch statusCode {
                case 200:
                    SVProgressHUD.dismiss(completion: {
                        UserDefaults.standard.set(name, forKey: "fetchUsername")
                        this?.tableView2.reloadData()
                        this?.displayToast()
                        this?.view.isUserInteractionEnabled = true
                    })
                default:
                    break
                }
                
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    this?.view.isUserInteractionEnabled = true
                })
                
                break
            }
        }
    }
    
    fileprivate func change(name: String) {
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.updateProfile(name: name)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                //guard let _ = jsonData["data"] as? [String: Any] else { return }
                
                SVProgressHUD.dismiss(completion: {
                    UserDefaults.standard.set(name, forKey: "fetchName")
                    this?.tableView.reloadData()
                    this?.displayToast()
                    this?.view.isUserInteractionEnabled = true
                })
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    this?.view.isUserInteractionEnabled = true
                })
                break
            }
        }
    }
    
    fileprivate func changeWeight(weight: String) {
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.updateProfileWeight(weight: weight)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                //guard let _ = jsonData["data"] as? [String: Any] else { return }
                
                SVProgressHUD.dismiss(completion: {
                    UserDefaults.standard.set(weight, forKey: "fetchWeight")
                    this?.tableView.reloadData()
                    this?.displayToast()
                    this?.view.isUserInteractionEnabled = true
                })
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    this?.view.isUserInteractionEnabled = true
                })
                break
            }
        }
    }
    
    private func update(email: String) {
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let manager = Alamofire.SessionManager.default
        manager.request(APIClient.updateProfileEmail(email: email)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                //guard let data = jsonData["data"] as? [String: Any] else { return }
                
                switch statusCode {
                case 200:
                    SVProgressHUD.dismiss(completion: {
                        UserDefaults.standard.set(email, forKey: "fetchEmail")
                        this?.tableView2.reloadData()
                        this?.displayToast()
                        this?.view.isUserInteractionEnabled = true
                    })
                    break
                case 422:
                    this?.view.isUserInteractionEnabled = true
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Opps!. Email Already in use. Please try again.")
                    })
                    break
                default:
                    this?.view.isUserInteractionEnabled = true
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                    break
                }
                
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    this?.view.isUserInteractionEnabled = true
                })
                break
            }
        }
    }
    
    fileprivate func uploadSelectedImage(image: UIImage, url: String) {
        
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let keychain = KeychainSwift()
        let token = keychain.get("apiToken") ?? ""
        
        let headers = ["Accept":"application/json", "Authorization":"Bearer \(token)"]
        let parameters = ["type": "image"] as [String : Any]
        
        guard let imageData = UIImageJPEGRepresentation(image, 1.0) else { return }
        guard let URL = try? URLRequest(url: url, method: .post, headers: headers) else { return }
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        manager.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                multipartFormData.append(imageData, withName: "value", fileName: "image.jpg", mimeType: "image/jpeg")
            }
            
        }, with: URL) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { [weak this = self] response in
                    switch response.result {
                    case .success(let json):
                        guard let jsonData = json as? [String: Any] else { return }
                        guard let data = jsonData["data"] as? [String: Any] else { return }
                        
                        SVProgressHUD.dismiss(completion: {
                            let image = data["image"] as? String ?? ""
                            UserDefaults.standard.set(image, forKey: "fetchProfilePic")
                            print("success upload :\(data)")
                            this?.view.isUserInteractionEnabled = true
                            this?.viewDidLoad()
                            this?.displayToast()
                        })
                        break
                    case .failure(let error):
                        SVProgressHUD.dismiss(completion: {
                            this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                            this?.view.isUserInteractionEnabled = true
                        })
                        
                        break
                    }
                }
                
            case .failure(let encodingError):
                SVProgressHUD.dismiss(completion: {
                    self.view.isUserInteractionEnabled = true
                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: encodingError.localizedDescription)
                })
            }
        }
    }
    
    private func checkAvailablilityOfUser(name: String) {
        if name.isEmpty {
            presentDismissableAlertController(title: Constants.MessageDialog.errorTitle, message: "Cannot be blank")
        } else {
            SVProgressHUD.show()
            SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
            Alamofire.request(APIClient.checkUser(name: name)).responseJSON { [weak this = self] response in
                switch response.result {
                case .success(let json):
                    guard let JSON = json as? [String: Any] else { return }
                    let statusCode = JSON["status"] as? Int ?? 0
                    
                    print("checkAvailablilityOfUser : \(JSON)")
                    
                    if statusCode == 200 {
//                        SVProgressHUD.dismiss(completion: {
//                            this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Username Already Exist.")
//                        })
                        
                        // for versioning only
                        SVProgressHUD.dismiss(completion: {
                            this?.changeUser(name: name)
                        })
                    } else if statusCode == 204 {
//                        SVProgressHUD.dismiss(completion: {
//                            this?.changeUser(name: name)
//                        })
                    }
                    // for versioning only
                    else if statusCode == 422 {
                        SVProgressHUD.dismiss(completion: {
                            this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Username Already Exist.")
                        })
                    }
                    
                    break
                case .failure(let error):
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    })
                    break
                }
            }
        }
    }
    
    private func imageOrientation(_ src:UIImage)-> UIImage {
        if src.imageOrientation == UIImageOrientation.up {
            return src
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch src.imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2))
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-Double.pi / 2))
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
}
