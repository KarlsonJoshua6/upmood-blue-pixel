//
//  NotificationSettingViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 28/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import UIKit

class NotificationSettingViewController: UIViewController {

    @IBOutlet weak var switchControl: UISwitch!
    @IBOutlet weak var tableView: UITableView!
    
    private var notificationValue = "live"
    private var didSelectSwitch = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSwitch()
        setUserInterfaceStyleLight(self: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        let privacy = UserDefaults.standard.integer(forKey: "privacyManagePrivacy")
        let emotion = UserDefaults.standard.integer(forKey: "emotionManagePrivacy")
        let heartBeat = UserDefaults.standard.integer(forKey: "heartbeatManagePrivacy")
        let auto_reconnect = UserDefaults.standard.integer(forKey: "fetchAutoReconnect")
        let stressLevel = UserDefaults.standard.integer(forKey: "stressLevelManagerPrivacy")
        if didSelectSwitch == true {
            switchNotification(withPrivacy: privacy, emotion: emotion, heartbeat: heartBeat, stress_level: stressLevel, notification_type: self.notificationValue, auto_reconnect: auto_reconnect)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func switchNotification(_ sender: UISwitch) {
        self.didSelectSwitch = true
        if sender.isOn {
            self.notificationValue = "live"
        } else {
            self.notificationValue = "off"
        }
    }
    
    private func setUpSwitch() {
        let userNotificationType = UserDefaults.standard.string(forKey: "fetchUserNotiificationType") ?? ""
        if userNotificationType == "live" {
            self.switchControl.isOn = true
        } else {
            self.switchControl.isOn = false
        }
    }
    
    private func switchNotification(withPrivacy: Int, emotion: Int, heartbeat: Int, stress_level: Int, notification_type: String, auto_reconnect: Int) {
        Alamofire.request(APIClient.updateManagePrivacy(privacy: withPrivacy, emotion: emotion, heartbeat: heartbeat, stress_level: stress_level, notification_type: notification_type, auto_reconnect: auto_reconnect, auto_renew: 0)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                this?.dismiss(animated: true, completion: {
                    UserDefaults.standard.set(notification_type, forKey: "fetchUserNotiificationType")
                    print("success updated Account: \(json)")
                })
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
            }
        }
    }
}
