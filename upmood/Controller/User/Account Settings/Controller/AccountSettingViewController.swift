//
//  AccountSettingViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 28/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import UIKit

class AccountSettingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var switchControl: UISwitch!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
//    private var menu = [NSLocalizedString("Manage Privacy", comment: ""), NSLocalizedString("Profile", comment: "")]
    private var menu = [NSLocalizedString("Manage Privacy", comment: "")]
    private var auto_reconnect = 0
    private var didUserChangeLanguage = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if UserDefaults.standard.string(forKey: "selectedLanguage") == "en" {
//            self.segmentControl.selectedSegmentIndex = 0
//            self.languageLabel.text = "Language"
//        } else if UserDefaults.standard.string(forKey: "selectedLanguage") == "ja" {
//            self.segmentControl.selectedSegmentIndex = 1
//            self.languageLabel.text = "言語"
//        } else {
//            //
//        }
        
        displayUI()
        setUserInterfaceStyleLight(self: self)
    }
    
    private func displayUI() {
        tableView.delegate = self
        tableView.dataSource = self
        
        let fetchAutoReconnect = UserDefaults.standard.integer(forKey: "fetchAutoReconnect")
        
        if fetchAutoReconnect == 1 {
            self.switchControl.isOn = true
        } else {
            self.switchControl.isOn = false
        }
    
        SVProgressHUD.dismiss()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func tappedAutoReconnect(_ sender: UISwitch) {
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
            if sender.isOn {
                UserDefaults.standard.set(1, forKey: "fetchAutoReconnect")
            } else {
                UserDefaults.standard.set(0, forKey: "fetchAutoReconnect")
            }
        } else {
            let privacy = UserDefaults.standard.integer(forKey: "privacyManagePrivacy")
            let emotion = UserDefaults.standard.integer(forKey: "emotionManagePrivacy")
            let heartBeat = UserDefaults.standard.integer(forKey: "heartbeatManagePrivacy")
            let stressLevel = UserDefaults.standard.integer(forKey: "stressLevelManagerPrivacy")
            let fetchUserNotiificationType = UserDefaults.standard.string(forKey: "fetchUserNotiificationType") ?? ""
            
            if sender.isOn {
                self.updateManageProfile(privacy: privacy, emotion: emotion, heartbeat: heartBeat, stress_level: stressLevel, notification_type: fetchUserNotiificationType, auto_reconnect: 1)
            } else {
                self.updateManageProfile(privacy: privacy, emotion: emotion, heartbeat: heartBeat, stress_level: stressLevel, notification_type: fetchUserNotiificationType, auto_reconnect: 0)
            }
        }
    }
    
    @IBAction func selectLanguage(_ sender: UISegmentedControl) {
        
        let alert = UIAlertController(title: "Change language", message: "The connected band will be disconnected. Do you want continue?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
            UserDefaults.standard.set(true, forKey: "didUserChangeLanguage")
            self.didUserChangeLanguage = true

            if sender.selectedSegmentIndex == 0 {
                Bundle.set(language: "en")
                UserDefaults.standard.set("en", forKey: "selectedLanguage")
            } else {
                Bundle.set(language: "ja")
                UserDefaults.standard.set("ja", forKey: "selectedLanguage")
            }
            
            NotificationCenter.default.post(name: .didTappedLogOut, object: nil, userInfo: ["data":"yes"])
            
            if self.didUserChangeLanguage == true {
                let storyboard = UIStoryboard.init(name: "DashboardScreen", bundle: nil)
                UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
                UserDefaults.standard.set(false, forKey: "didUserChangeLanguage")
                self.didUserChangeLanguage = false
            }
            
            self.viewDidLoad()
            Constants.didUserChangeLanguage = true
        }))
        
        let dontShowTxt = NSLocalizedString("Cancel", comment: "")
        alert.addAction(UIAlertAction(title: dontShowTxt, style: .cancel, handler: { (alert) in
            self.viewDidLoad()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension AccountSettingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountSettingTableViewCell", for: indexPath) as? AccountSettingTableViewCell {
            cell.settingLabel.text = menu[indexPath.row]
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        switch indexPath.row {
        case 0:
            if isGuest == 1 {
                presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "You're in guest mode, please sign up to access this menu.")
            } else {
                performSegue(withIdentifier: "showManagePrivacy", sender: self)
            }
            break
//        case 1:
//            if isGuest == 1 {
//                presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "You're in guest mode, please sign up to access this menu.")
//            } else {
//                performSegue(withIdentifier: "showProfile", sender: self)
//            }
//            break
        case 2:
            break
        default: break
        }
    }
}

extension AccountSettingViewController {
    private func updateManageProfile(privacy: Int, emotion: Int, heartbeat: Int, stress_level: Int, notification_type: String, auto_reconnect: Int) {
        Alamofire.request(APIClient.updateManagePrivacy(privacy: privacy, emotion: emotion, heartbeat: heartbeat, stress_level: stress_level, notification_type: notification_type, auto_reconnect: auto_reconnect, auto_renew: 0)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                
                switch statusCode {
                case 200:
                    UserDefaults.standard.set(auto_reconnect, forKey: "fetchAutoReconnect")
                    print("success updated auto_reconnect \n\n\n \(json) \n\n\n")
                    break
                case 204:
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Please check your internet connection and try again.")
                    break
                default:
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Please check your internet connection and try again.")
                    break
                }
                
                break
            case .failure(let error):
                UserDefaults.standard.set(1, forKey: "fetchAutoReconnect")
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
}
