//
//  AcountSettingsTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 24/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class AcountSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var personalInformationDataLabel: UILabel!
    @IBOutlet weak var personalInformationLabel: UILabel!
    @IBOutlet weak var accountSettingTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
