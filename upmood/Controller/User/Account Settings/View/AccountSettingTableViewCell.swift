//
//  AccountSettingTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 28/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class AccountSettingTableViewCell: UITableViewCell {

    @IBOutlet weak var checkMark: UIImageView!
    @IBOutlet weak var profileDataLabel: UILabel!
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var managePrivacyLabel: UILabel!
    @IBOutlet weak var settingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
