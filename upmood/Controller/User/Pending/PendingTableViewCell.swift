//
//  PendingTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 18/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class PendingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var approveButton: UIButton!
    @IBOutlet weak var denyButton: UIButton!
    @IBOutlet weak var friendUserName: UILabel!
    @IBOutlet weak var friendProfilePic: ClipToBoundsImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        darkmodeCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?)
    {
        super.traitCollectionDidChange(previousTraitCollection)
        darkmodeCell()
    }
        
    private func darkmodeCell(){
         if #available(iOS 12.0, *) {
             if traitCollection.userInterfaceStyle == .light {
                denyButton.setTitleColor(hexStringToUIColor(hex: Constants.Color.NEW_COLOR), for: .normal)
                friendUserName.textColor = UIColor.black
             } else {
                 denyButton.setTitleColor(UIColor.white, for: .normal)
                 friendUserName.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_MOODSTREAM)
             }
         } else {
             // Fallback on earlier versions
         }
     }

}
