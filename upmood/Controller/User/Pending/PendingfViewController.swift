//
//  PendingfViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 18/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SVProgressHUD
import UIKit

class PendingfViewController: UIViewController {

    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private var notificationId = [Int]()
    private var friendId = [Int]()
    private var friendUserName = [String]()
    private var friendProfilePhoto = [String]()
    private var index = 0
    
    @IBOutlet weak var labelNoRequire: UILabel!
    @IBOutlet weak var labelTry: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emptyStateView.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        loadPendingList()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
        
    }
    @objc func loadList(notification: NSNotification){
        //load data here
        self.reload()
    }
    private func getDarkmode(){
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .light {
                labelNoRequire.textColor = UIColor.black
                labelTry.textColor = UIColor.black
            } else {
                labelNoRequire.textColor = UIColor.white
                labelTry.textColor = UIColor.white
            }
        } else {
        // Fallback on earlier versions
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        getDarkmode()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?)
    {
        super.traitCollectionDidChange(previousTraitCollection)
        getDarkmode()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? NotYetFriends  {
            itemDetails.friendUsername = friendUserName[self.index]
            itemDetails.friendProfilePicStr = friendProfilePhoto[self.index]
            itemDetails.notificationId = notificationId[self.index]
            itemDetails.friendId = friendId[self.index]
        }else if let itemDetails = segue.destination as? NotYetFriendsNotif {
            itemDetails.friendUsername = friendUserName[self.index]
            itemDetails.friendProfilePicStr = friendProfilePhoto[self.index]
            itemDetails.notificationId = notificationId[self.index]
            itemDetails.friendId = friendId[self.index]
        }
        else{
            
        }
    }
}

// MARK - Network request
extension PendingfViewController {
    
    @objc func reload() {
        self.notificationId.removeAll()
        self.friendId.removeAll()
        self.friendUserName.removeAll()
        self.friendProfilePhoto.removeAll()
        self.tableView.reloadData()
        self.loadPendingList()
    }
    
    private func loadPendingList() {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.pendingList()).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let status = JSON["status"] as? Int ?? 0
                guard let data = JSON["data"] as? [String: Any] else { return }
                guard let jsonData = data["data"] as? [[String: Any]] else { return }
                
                if status == 200 {
                    SVProgressHUD.dismiss(completion: {
                        for data in jsonData {
                            let notificationId = data["id"] as? Int ?? 0
                            this?.notificationId.append(notificationId)
                            
                            // notification
                            guard let pushNotificationContent = data["content"] as? [String: Any] else { return }
                            let friendUserName = pushNotificationContent["name_from"] as? String ?? ""
                            let friendProfilePhoto = pushNotificationContent["image_from"] as? String ?? ""
                            let id_from = pushNotificationContent["id_from"] as? Int ?? 0
                            this?.friendId.append(id_from)
                            this?.friendUserName.append(friendUserName)
                            this?.friendProfilePhoto.append(friendProfilePhoto)
                        } // end of fucking loop
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss(completion: {
                                this?.tableView.reloadData()
                            })
                        }
                    })
                    
                } else if status == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.emptyStateView.isHidden = false
//                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found.")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func denyPendingFriendRequest(friendId: Int, notificationId: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.denyPendingrequestWith(FriendId: friendId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                print("JSON Outside: \(JSON)")
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        DispatchQueue.main.async {
                            print("resPonse 200: \(JSON)")
                            this?.markAsSeen(id: notificationId)
                        }
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
//                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found.")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func acceptPendingFriendRequest(friendId: Int, notificationId: Int) {
        print("friendId: \(friendId), \(notificationId)")
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.approvePendingRequestWith(friendId: friendId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                print("JSON Outside: \(JSON)")
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        DispatchQueue.main.async {
                            print("resPonse 200: \(JSON)")
                            this?.markAsSeen(id: notificationId)
                        }
                        
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
//                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found.")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func markAsSeen(id: Int) {
        let token = KeychainSwift().get("apiToken") ?? ""
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/notification/seen/\(id)") else { return }
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
        if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
            DispatchQueue.main.async {
                print("response as markAsSeen: \(userObject)")
                self.reload()
            }
        }}.resume()
    }
 
    
    private func setUp(cell: PendingTableViewCell, indexPath: IndexPath) {
        
        // setUP Button
        cell.approveButton.addTarget(self, action: #selector(self.acceptFriendRequest(_:)), for: .touchUpInside)
        cell.denyButton.addTarget(self, action: #selector(self.denyFriendRequest(_:)), for: .touchUpInside)
        
        cell.friendUserName.text = friendUserName[indexPath.row]
        
        if self.friendProfilePhoto[indexPath.row].isEmpty {
            cell.friendProfilePic.image = UIImage(named: "ic_empty_state_profile")
        }
        
        if let url = URL(string: "\(self.friendProfilePhoto[indexPath.row])") {
            cell.friendProfilePic.sd_setImage(with: url, completed: nil)
        }
    }
    
    @objc private func acceptFriendRequest(_ sender: UIButton) {
        if let indexpath = self.tableView.indexPathForView(sender) {
            print("selected to: \(friendId[indexpath.row])")
            acceptPendingFriendRequest(friendId: friendId[indexpath.row], notificationId: notificationId[indexpath.row])
        }
    }
    
    @objc private func denyFriendRequest(_ sender: UIButton) {
        if let indexpath = self.tableView.indexPathForView(sender) {
            denyPendingFriendRequest(friendId: friendId[indexpath.row], notificationId: notificationId[indexpath.row])
        }
    }
}

extension PendingfViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PendingTableViewCell", for: indexPath) as? PendingTableViewCell {
            setUp(cell: cell, indexPath: indexPath)
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendUserName.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.index = indexPath.row
        performSegue(withIdentifier: "notyetfriends", sender: self)
    }
}
