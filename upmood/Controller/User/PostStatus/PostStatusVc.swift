//
//  PostStatusVc.swift
//  upmood
//
//  Created by Arvin Cabunoc on 19/03/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//
import Alamofire
import SVProgressHUD
import UIKit
import Reachability

class PostStatusVc: UIViewController {
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var tfStatus: UITextField!
    // WORK
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnErase: UIButton!
    @IBOutlet weak var btnMeeting: UIButton!
    @IBOutlet weak var btnAnalyzing: UIButton!
    @IBOutlet weak var btnMaintenance: UIButton!
    @IBOutlet weak var btnTracking: UIButton!
    @IBOutlet weak var btnAuditing: UIButton!
    @IBOutlet weak var btnOrganizing: UIButton!
    @IBOutlet weak var btnEvaluating: UIButton!
    @IBOutlet weak var btnTesting: UIButton!
    @IBOutlet weak var btnThinking: UIButton!
    @IBOutlet weak var btnProgramming: UIButton!
    @IBOutlet weak var btnInterpreting: UIButton!
    @IBOutlet weak var btnIdentifying: UIButton!
    // Leisure
    @IBOutlet weak var btnPlaying: UIButton!
    @IBOutlet weak var btnExercising: UIButton!
    @IBOutlet weak var btnReading: UIButton!
    @IBOutlet weak var btnWatching: UIButton!
    @IBOutlet weak var btnShopping: UIButton!
    @IBOutlet weak var btnDancing: UIButton!
    @IBOutlet weak var btnSinging: UIButton!
    @IBOutlet weak var btnPartying: UIButton!
    @IBOutlet weak var btnGaming: UIButton!
    @IBOutlet weak var btnListening: UIButton!
    @IBOutlet weak var btnCelebrating: UIButton!
    @IBOutlet weak var btnSleeping: UIButton!
    @IBOutlet weak var btnResting: UIButton!
    @IBOutlet weak var btnRelaxing: UIButton!
    @IBOutlet weak var btnMeditating: UIButton!
    // Home
    @IBOutlet weak var btnBaking: UIButton!
    @IBOutlet weak var btnCooking: UIButton!
    @IBOutlet weak var btnEating: UIButton!
    @IBOutlet weak var btnCleaning: UIButton!
    // Vacation
    @IBOutlet weak var btnTravelling: UIButton!
    @IBOutlet weak var btnDriving: UIButton!
    @IBOutlet weak var btnJogging: UIButton!
    @IBOutlet weak var btnCommuting: UIButton!
    @IBOutlet weak var btnVisiting: UIButton!
    @IBOutlet weak var btnRiding: UIButton!
    // School
    @IBOutlet weak var btnStudying: UIButton!
    @IBOutlet weak var btnDiscovering: UIButton!
    @IBOutlet weak var btnResolving: UIButton!
    @IBOutlet weak var btnBuilding: UIButton!
    @IBOutlet weak var btnCreating: UIButton!
    @IBOutlet weak var btnAttending: UIButton!
    @IBOutlet weak var btnDecorating: UIButton!
    @IBOutlet weak var btnWriting: UIButton!
    @IBOutlet weak var btnReporting: UIButton!
    
    @IBOutlet weak var btnPost: UIButton!
    
    @IBOutlet weak var connectionView: UIView!
    
    @IBOutlet weak var lblWork: UILabel!
    @IBOutlet weak var lblLeisure: UILabel!
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblVacation: UILabel!
    @IBOutlet weak var lblSchool: UILabel!
    
    @IBOutlet weak var leftConstLeisure: NSLayoutConstraint!
    @IBOutlet weak var lefConstVacation: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblNo: UILabel!
    
    var btnSelected: UIButton?
    var getTextFieldStatus: String = ""
    // buttons
    @IBOutlet weak var btnWorking: UIButton!
    @IBOutlet weak var btnDesigning: UIButton!
    
    var buttonStatus = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        btnEditIsTrue()
        tfStatus.addTarget(self, action: #selector(PostStatusVc.textFieldDidChange(_:)),
                           for: UIControlEvents.editingChanged)
        connectionView.isHidden = true
        postJap()
        getDarkmode()
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?){
          super.traitCollectionDidChange(previousTraitCollection)
        darkmodeAllButton()
        getStatusUserDefault()
       
        getAllButtonDarkmode()
        getDarkmode()
      }
    
    private func getDarkmode(){
        if #available(iOS 12.0, *) {
        switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
            // light mode detected
                tfStatus.textColor = UIColor.darkGray
            break
            case .dark:
            // dark mode detected
                tfStatus.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_TFSTATUS)
                
            break
            }
        } else {
        // Fallback on earlier versions
        }

    }
    private func getStatusUserDefault(){
        
        let statusSaved = UserDefaults.standard.string(forKey: "profile_post") ?? ""
        let getTranslate = convertion(lang: statusSaved)
        if statusSaved.isEmpty {
            
        } else {
            let lang = UserDefaults.standard.string(forKey: "selectedLanguage")
            if lang == "ja" {
                japToEng(lang: statusSaved)
                tfStatus.text = NSLocalizedString(statusSaved, comment: "")
            } else {
                statusUserDefault(status: getTranslate)
                tfStatus.text = getTranslate
            }
        }
        
        guard let length = tfStatus.text?.lengthOfBytes(using: String.Encoding.utf8) else { return }

        if length >= 51 {
            lblNo.text = "50/50"
        } else if length == 0 || length-1 == 0{
            tfStatus.placeholder = NSLocalizedString("Share what you're doing", comment: "")
            tfStatus.text = nil
            btnEditIsTrue()
        } else {
            lblNo.text = "\(length)/50"
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        monitorNetworkConnection()
        darkmodeAllButton()
        getDarkmode()
        getStatusUserDefault()
    }
    override func viewDidDisappear(_ animated: Bool) {
        reachability?.stopNotifier()
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let length = tfStatus.text?.lengthOfBytes(using: String.Encoding.utf8) else { return }
        print("buttonStatus", buttonStatus)
        print("haha", length)
        
        boolCall()
        
        let getButtonState = UserDefaults.standard.string(forKey: "buttonStateStatus")
        
        if buttonStatus == true || getButtonState == "true"{
            print("buttonStatus", "true1")
            tfStatus.text = nil
            btnEditIsTrue()
            buttonStatus = false
            UserDefaults.standard.set("false", forKey: "buttonStateStatus")
            buttonBackgroundRemove(btn: btnSelected!)
        }else{
            if length == 0 || length-1 == 0{
                btnEditIsTrue()
                tfStatus.placeholder = NSLocalizedString("Share what you're doing", comment: "")
            }else{
                btnEraseIsTrue()
                if length >= 52 {
                    tfStatus.isUserInteractionEnabled = false
                    lblNo.text = "50/50"
                }else{
                    tfStatus.isUserInteractionEnabled = true
                    if length >= 51{
                        lblNo.text = "50/50"
                    }else{
                        lblNo.text = "\(length)/50"
                    }
                    
                }
                
            }
        }
        
       
    }

    @IBAction func btnPost(_ sender: Any) {
        //view.isUserInteractionEnabled = false
        getTextFieldStatus = tfStatus.text!
        //        if !getTextFieldStatus.isEmpty {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.add(status: getTextFieldStatus)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                SVProgressHUD.dismiss(completion: {
                    if statusCode == 200 {
                        //this?.view.isUserInteractionEnabled = true
                        guard let JSON = jsonData["data"] as? [String: Any] else { return }
                        let status = JSON["post"] as? String ?? ""
                        UserDefaults.standard.set(status, forKey: "profile_post")
                        self.dismiss(animated: true, completion: nil)
                        NotificationCenter.default.post(name: .didUpdateUserPostStatus, object: nil, userInfo: nil)
                        
                        UserDefaults.standard.set("false", forKey: "buttonStateStatus")
                        
                    } else {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                        //this?.view.isUserInteractionEnabled = true
                    }})
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }}
        //        } else {
        //            toast(message: "Please share what you're doing")
        //            view.isUserInteractionEnabled = true
        //        }
    }
    
    private func btnEditIsTrue() {
        btnEdit.isHidden = false
        btnErase.isHidden = true
        lblNo.text = "0/50"
    }
    
    func btnEraseIsTrue(){
        btnEdit.isHidden = true
        btnErase.isHidden = false
    }
    @IBAction func btnErase(_ sender: Any) {
        let getButtonState = UserDefaults.standard.string(forKey: "buttonStateStatus")
        if buttonStatus == true || getButtonState == "true" {
            buttonBackgroundRemove(btn: btnSelected!)
            delete()
            deselectButton()
            darkmodeAllButton()
        }else{
            tfStatus.placeholder = NSLocalizedString("Share what you're doing", comment: "")
            tfStatus.text = nil
            btnEditIsTrue()
            buttonStatus = false
            //        UserDefaults.standard.set("false", forKey: "buttonStateStatus")
            print("buttonStatus", buttonStatus)
            boolCall()
            
        }
        
        
    }
  
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
        UserDefaults.standard.set("false", forKey: "buttonStateStatus")
    }
    
    func delete() { 
        if btnSelected != nil {
            buttonBackgroundRemove(btn: btnSelected!)
            tfStatus.placeholder = NSLocalizedString("Share what you're doing", comment: "")
            tfStatus.text = nil
            btnEditIsTrue()
            buttonStatus = false
            print("Deselected button")
        }
    }
    
    // MARK: - WORKING
    @IBAction func btnWorking(_ sender: Any) {
        boolOfWork()
        if Constant.btnWorkBool == true{
            buttonText(message: "Working")
            buttonBackground(btn: btnWorking)
            print("Selected button")
            Constant.btnWorkBool = false
        }else{
            Constant.btnWorkBool = true
            delete()
        }
    }
    @IBAction func btnDesignning(_ sender: Any) {
        boolOfDesign()
        if Constant.bntDesignBool == true{
            buttonText(message: "Designing")
            buttonBackground(btn: btnDesigning)
            Constant.bntDesignBool = false
        }else{
            Constant.bntDesignBool = true
            delete()
            
        }
    }
    @IBAction func btnMeeting(_ sender: Any) {
        boolOfMeet()
        if Constant.btnMeetBool == true{
            buttonText(message: "Meeting")
            buttonBackground(btn: btnMeeting)
            Constant.btnMeetBool = false
        }else{
            Constant.btnMeetBool = true
            delete()
        }
    }
    @IBAction func btnAnalyzing(_ sender: Any) {
        boolOfAnalyze()
        if Constant.btnAnalyzeBool == true{
            buttonText(message: "Analyzing")
            buttonBackground(btn: btnAnalyzing)
            Constant.btnAnalyzeBool = false
        }else{
            Constant.btnAnalyzeBool = true
            delete()
        }
    }
    @IBAction func btnMaintenance(_ sender: Any) {
        boolOfMonitor()
        if Constant.btnMonitorBool == true{
            buttonText(message: "Monitoring")
            buttonBackground(btn: btnMaintenance)
            Constant.btnMonitorBool = false
        }else{
            Constant.btnMonitorBool = true
            delete()
        }
    }
    @IBAction func btnTracking(_ sender: Any) {
        boolOfTrack()
        if Constant.btnTrackBool == true{
            buttonText(message: "Tracking")
            buttonBackground(btn: btnTracking)
            Constant.btnTrackBool = false
        }else{
            Constant.btnTrackBool = true
            delete()
        }
    }
    @IBAction func btnAuditing(_ sender: Any) {
        boolOfAudit()
        if Constant.btnAuditBool == true{
            buttonText(message: "Auditing")
            buttonBackground(btn: btnAuditing)
            Constant.btnAuditBool = false
        }else{
            Constant.btnAuditBool = true
            delete()
        }
    }
    @IBAction func btnOrganizing(_ sender: Any) {
        boolOfOrganize()
        if Constant.btnOrganizeBool == true{
            buttonText(message: "Organizing")
            buttonBackground(btn: btnOrganizing)
            Constant.btnOrganizeBool = false
        }else{
            Constant.btnOrganizeBool = true
            delete()
        }
    }
    @IBAction func btnEvaluating(_ sender: Any) {
        boolOfEvalute()
        if Constant.btnEvaluteBool == true{
            buttonText(message: "Evaluating")
            buttonBackground(btn: btnEvaluating)
            Constant.btnEvaluteBool = false
        }else{
            Constant.btnEvaluteBool = true
            delete()
        }
    }
    @IBAction func btnTesting(_ sender: Any) {
        boolOfTest()
        if Constant.btnTestBool == true{
            buttonText(message: "Testing")
            buttonBackground(btn: btnTesting)
            Constant.btnTestBool = false
        }else{
            Constant.btnTestBool = true
            delete()
        }
    }
    @IBAction func btnThinking(_ sender: Any) {
        boolOfThink()
        if Constant.btnThinkBool == true{
            buttonText(message: "Thinking")
            buttonBackground(btn: btnThinking)
            Constant.btnThinkBool = false
        }else{
            Constant.btnThinkBool = true
            delete()
        }
    }
    @IBAction func btnProgramming(_ sender: Any) {
        boolOfProgram()
        if Constant.btnProgramBool == true{
            buttonText(message: "Programming")
            buttonBackground(btn: btnProgramming)
            Constant.btnProgramBool = false
        }else{
            Constant.btnProgramBool = true
            delete()
        }
    }
    @IBAction func btnInterpreting(_ sender: Any) {
        boolOfInterpret()
        if Constant.btnInterpretBool == true{
            buttonText(message: "Interpreting")
            buttonBackground(btn: btnInterpreting)
            Constant.btnInterpretBool = false
        }else{
            Constant.btnInterpretBool = true
            delete()
        }
    }
    @IBAction func btnIdentifying(_ sender: Any) {
        boolOfIndentify()
        if Constant.btnIndentifyBool == true{
            buttonText(message: "Identifying")
            buttonBackground(btn: btnIdentifying)
            Constant.btnIndentifyBool = false
        }else{
            Constant.btnIndentifyBool = true
            delete()
        }
    }
    
    // MARK: - LEISURE
    @IBAction func btnPlaying(_ sender: Any) {
        boolOfPlay()
        if Constant.btnPlayBool == true{
            buttonText(message: "Playing")
            buttonBackground(btn: btnPlaying)
            Constant.btnPlayBool = false
        }else{
            Constant.btnPlayBool = true
            delete()
        }
    }
    @IBAction func btnExercising(_ sender: Any) {
        boolOfExercise()
        if Constant.btnExerciseBool == true{
            buttonText(message: "Exercising")
            buttonBackground(btn: btnExercising)
            Constant.btnExerciseBool = false
        }else{
            Constant.btnExerciseBool = true
            delete()
        }
    }
    @IBAction func btnReading(_ sender: Any) {
        boolOfRead()
        if Constant.btnReadBool == true{
            buttonText(message: "Reading")
            buttonBackground(btn: btnReading)
            Constant.btnReadBool = false
        }else{
            Constant.btnReadBool = true
            delete()
        }
    }
    @IBAction func btnWatching(_ sender: Any) {
        boolOfWatch()
        if Constant.btnWatchBool == true{
            buttonText(message: "Watching")
            buttonBackground(btn: btnWatching)
            Constant.btnWatchBool = false
        }else{
            Constant.btnWatchBool = true
            delete()
        }
    }
    @IBAction func btnShopping(_ sender: Any) {
        boolOfShop()
        if Constant.btnShopBool == true{
            buttonText(message: "Shopping")
            buttonBackground(btn: btnShopping)
            Constant.btnShopBool = false
        }else{
            Constant.btnShopBool = true
            delete()
        }
    }
    @IBAction func btnDancing(_ sender: Any) {
        boolOfDance()
        if Constant.btnDanceBool == true{
            buttonText(message: "Dancing")
            buttonBackground(btn: btnDancing)
            Constant.btnDanceBool = false
        }else{
            Constant.btnDanceBool = true
            delete()
        }
        
    }
    @IBAction func btnSinging(_ sender: Any) {
        boolOfSing()
        if Constant.btnSingBool == true{
            buttonText(message: "Singing")
            buttonBackground(btn: btnSinging)
            Constant.btnSingBool = false
        }else{
            Constant.btnSingBool = true
            delete()
        }
        
    }
    @IBAction func btnPartying(_ sender: Any) {
        boolOfParty()
        if Constant.btnPartyBool == true{
            buttonText(message: "Partying")
            buttonBackground(btn: btnPartying)
            Constant.btnPartyBool = false
        }else{
            Constant.btnPartyBool = true
            delete()
        }
    }
    @IBAction func btnGaming(_ sender: Any) {
        boolOfGame()
        if Constant.btnGameBool == true {
            buttonText(message: "Gaming")
            buttonBackground(btn: btnGaming)
            Constant.btnGameBool = false
        }else{
            Constant.btnGameBool = true
            delete()
        }
    }
    @IBAction func btnListening(_ sender: Any) {
        boolOfListen()
        if Constant.btnListenBool == true{
            buttonText(message: "Listening")
            buttonBackground(btn: btnListening)
            Constant.btnListenBool = false
        }else{
            Constant.btnListenBool = true
            delete()
        }
    }
    @IBAction func btnCelebrating(_ sender: Any) {
        boolOfCelebrate()
        if Constant.btnCelebrateBool == true{
            buttonText(message: "Celebrating")
            buttonBackground(btn: btnCelebrating)
            Constant.btnCelebrateBool = false
        }else{
            Constant.btnCelebrateBool = true
            delete()
        }
        
    }
    @IBAction func btnSleeping(_ sender: Any) {
        boolOfSleep()
        if Constant.btnSleepBool == true{
            buttonText(message: "Sleeping")
            buttonBackground(btn: btnSleeping)
            Constant.btnSleepBool = false
        }else{
            Constant.btnSleepBool = true
            delete()
        }
    }
    @IBAction func btnResting(_ sender: Any) {
        boolOfRest()
        if Constant.btnRestBool == true{
            buttonText(message: "Resting")
            buttonBackground(btn: btnResting)
            Constant.btnRestBool = false
        }else{
            Constant.btnRestBool = true
            delete()
        }
    }
    @IBAction func btnRelaxing(_ sender: Any) {
        boolOfRelax()
        if Constant.btnRelaxBool == true{
            buttonText(message: "Relaxing")
            buttonBackground(btn: btnRelaxing)
            Constant.btnRelaxBool = false
        }else{
            Constant.btnRelaxBool = true
            delete()
        }
    }
    @IBAction func btnMeditating(_ sender: Any) {
        boolOfMeditate()
        if Constant.btnMeditateBool == true{
            buttonText(message: "Meditating")
            buttonBackground(btn: btnMeditating)
            Constant.btnMeditateBool = false
        }else{
            Constant.btnMeditateBool = true
            delete()
        }
    }
    
    // MARK: - HOME
    @IBAction func btnBaking(_ sender: Any) {
        boolOfBake()
        if Constant.btnBakeBool == true{
            buttonText(message: "Baking")
            buttonBackground(btn: btnBaking)
            Constant.btnBakeBool = false
        }else{
            Constant.btnBakeBool = true
            delete()
        }
    }
    @IBAction func btnCooking(_ sender: Any) {
        boolOfCook()
        if Constant.btnCookBool == true{
            buttonText(message: "Cooking")
            buttonBackground(btn: btnCooking)
            Constant.btnCookBool = false
        }else{
            Constant.btnCookBool = true
            delete()
        }
    }
    @IBAction func btnEating(_ sender: Any) {
        boolOfEat()
        if Constant.btnEatBool == true{
            buttonText(message: "Eating")
            buttonBackground(btn: btnEating)
            Constant.btnEatBool = false
        }else{
            Constant.btnEatBool = true
            delete()
        }
    }
    @IBAction func btnCleaning(_ sender: Any) {
        boolOfClean()
        if Constant.btnCleanBool == true{
            buttonText(message: "Cleaning")
            buttonBackground(btn: btnCleaning)
            Constant.btnCleanBool = false
        }else{
            Constant.btnCleanBool = true
            delete()
        }
    }
    // MARK: - Vacation
    @IBAction func btnTravelling(_ sender: Any) {
        boolOfTravel()
        if Constant.btnTravelBool == true{
            buttonText(message: "Travelling")
            buttonBackground(btn: btnTravelling)
            Constant.btnTravelBool = false
        }else{
            Constant.btnTravelBool = true
            delete()
        }
    }
    @IBAction func btnDriving(_ sender: Any) {
        boolOfDrive()
        if Constant.btnDriveBool == true{
            buttonText(message: "Driving")
            buttonBackground(btn: btnDriving)
            Constant.btnDriveBool = false
        }else{
            Constant.btnDriveBool = true
            delete()
        }
    }
    @IBAction func btnJogging(_ sender: Any) {
        boolOfJog()
        if Constant.btnJogBool == true{
            buttonText(message: "Jogging")
            buttonBackground(btn: btnJogging)
            Constant.btnJogBool = false
        }else{
            Constant.btnJogBool = true
            delete()
        }
    }
    @IBAction func btnCommuting(_ sender: Any) {
        boolOfCommute()
        if Constant.btnCommuteBool == true{
            buttonText(message: "Commuting")
            buttonBackground(btn: btnCommuting)
            Constant.btnCommuteBool = false
        }else{
            Constant.btnCommuteBool = true
            delete()
        }
    }
    @IBAction func btnVisiting(_ sender: Any) {
        boolOfVisit()
        if Constant.btnVisitBool == true{
            buttonText(message: "Visiting")
            buttonBackground(btn: btnVisiting)
            Constant.btnVisitBool = false
        }else{
            Constant.btnVisitBool = true
            delete()
        }
        
    }
    @IBAction func btnRiding(_ sender: Any) {
        boolOfRide()
        if Constant.btnRidingBool == true{
            buttonText(message: "Riding")
            buttonBackground(btn: btnRiding)
            Constant.btnRidingBool = false
        }else{
            Constant.btnRidingBool = true
            delete()
        }
    }
    // MARK: - School
    @IBAction func btnStudying(_ sender: Any) {
        boolOfStudy()
        if Constant.btnStudyBool == true{
            buttonText(message: "Studying")
            buttonBackground(btn: btnStudying)
            Constant.btnStudyBool = false
        }else{
            Constant.btnStudyBool = true
            delete()
        }
    }
    @IBAction func btnDiscovering(_ sender: Any) {
        boolOfDiscover()
        if Constant.btnDiscoverBool == true {
            buttonText(message: "Discovering")
            buttonBackground(btn: btnDiscovering)
            Constant.btnDiscoverBool = false
        }else{
            Constant.btnDiscoverBool = true
            delete()
        }
    }
    @IBAction func btnResolving(_ sender: Any) {
        boolOfResolve()
        if Constant.btnResolveBool == true{
            buttonText(message: "Resolving")
            buttonBackground(btn: btnResolving)
            Constant.btnResolveBool = false
        }else{
            Constant.btnResolveBool = true
            delete()
        }
        
    }
    @IBAction func btnBuilding(_ sender: Any) {
        boolOfBuild()
        if Constant.btnBuildBool == true{
            buttonText(message: "Building")
            buttonBackground(btn: btnBuilding)
            Constant.btnBuildBool = false
        }else{
            Constant.btnBuildBool = true
            delete()
        }
    }
    @IBAction func btnCreating(_ sender: Any) {
        boolOfCreate()
        if Constant.btnCreateBool == true{
            buttonText(message: "Creating")
            buttonBackground(btn: btnCreating)
            Constant.btnCreateBool = false
        }else{
            Constant.btnCreateBool = true
            delete()
        }
    }
    @IBAction func btnAttending(_ sender: Any) {
        boolOfAttend()
        if Constant.btnAttendBool == true{
            buttonText(message: "Attending")
            buttonBackground(btn: btnAttending)
            Constant.btnAttendBool = false
        }else{
            Constant.btnAttendBool = true
            delete()
        }
    }
    @IBAction func btnDecorating(_ sender: Any) {
        boolOfDecorate()
        if Constant.btnDecorateBool == true{
            buttonText(message: "Decorating")
            buttonBackground(btn: btnDecorating)
            Constant.btnDecorateBool = false
        }else{
            Constant.btnDecorateBool = true
            delete()
        }
    }
    @IBAction func btnWriting(_ sender: Any) {
        boolOfWrite()
        if Constant.btnWriteBool == true{
            buttonText(message: "Writing")
            buttonBackground(btn: btnWriting)
            Constant.btnWriteBool = false
        }else{
            Constant.btnWriteBool = true
            delete()
        }
    }
    @IBAction func btnReporting(_ sender: Any) {
        boolOfReport()
        if Constant.btnReportBool == true{
            buttonText(message: "Reporting")
            buttonBackground(btn: btnReporting)
            Constant.btnReportBool = false
        }else{
            Constant.btnReportBool = true
            delete()
        }
    }
    
    private func buttonText(message: String){
        buttonStatus = true
        tfStatus.text = nil
        tfStatus.text = NSLocalizedString(message, comment: "")
        btnEraseIsTrue()
        
        UserDefaults.standard.set("true", forKey: "buttonStateStatus")
        
        guard let length = tfStatus.text?.lengthOfBytes(using: String.Encoding.utf8) else { return }
        
        lblNo.text = "\(length)/50"
        
    }
    private func buttonBackgroundRemove(btn :UIButton){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
               btn.backgroundColor = UIColor.white
               btn.setTitleColor(UIColor.lightGray, for: .normal)
               
               break
                
            case .dark:
                // dark mode detected
                btn.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btn.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break

            }
        } else {
            // Fallback on earlier versions
        }
    }
    func buttonBackground(btn: UIButton){
        btnSelected = btn
        switch btn {
        //MARK: - WORK
        case btnWorking:
            btnWorking.setTitleColor(UIColor.white, for: .normal)
            btnWorking.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "working")
            
            firstLine()
            break
        case btnDesigning:
            btnDesigning.setTitleColor(UIColor.white, for: .normal)
            btnDesigning.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "designing")
            
            firstLine()
            break
        case btnMeeting:
            
            btnMeeting.setTitleColor(UIColor.white, for: .normal)
            btnMeeting.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "meeting")
                        
            secondLine()
            break
            
        case btnAnalyzing:
            
            btnAnalyzing.setTitleColor(UIColor.white, for: .normal)
            btnAnalyzing.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "analyzing")
            
            secondLine()
            break
            
        case btnMaintenance:
            btnMaintenance.setTitleColor(UIColor.white, for: .normal)
            btnMaintenance.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "monitoring")
                        
            secondLine()
            break
        case btnTracking:
            
            btnTracking.setTitleColor(UIColor.white, for: .normal)
            btnTracking.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "tracking")
            
            thirdLine()
            break
        case btnAuditing:
            
            btnAuditing.setTitleColor(UIColor.white, for: .normal)
            btnAuditing.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "audit")
                        
            thirdLine()
            break
        case btnOrganizing:
            btnOrganizing.setTitleColor(UIColor.white, for: .normal)
            btnOrganizing.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "organizing")
            
            fourthLine()
            break
        case btnEvaluating:
            btnEvaluating.setTitleColor(UIColor.white, for: .normal)
            btnEvaluating.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "evaluating")
           
            fourthLine()
            break
        case btnTesting:
            btnTesting.setTitleColor(UIColor.white, for: .normal)
            btnTesting.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "testing")
            
            fourthLine()
            break
        case btnThinking:
            btnThinking.setTitleColor(UIColor.white, for: .normal)
            btnThinking.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "thinking")
                      
            fifthLine()
            break
        case btnProgramming:
            btnProgramming.setTitleColor(UIColor.white, for: .normal)
            btnProgramming.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "programming")
                    
            fifthLine()
            break
        case btnInterpreting:
            btnInterpreting.setTitleColor(UIColor.white, for: .normal)
            btnInterpreting.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "interpreting")
            
            sixthLine()
            break
        case btnIdentifying:
            btnIdentifying.setTitleColor(UIColor.white, for: .normal)
            btnIdentifying.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "identifying")
            
            sixthLine()
            break
        //MARK: - Leisure
        case btnPlaying:
            btnPlaying.setTitleColor(UIColor.white, for: .normal)
            btnPlaying.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "playing")
            
            seventhLine()
            break
        case btnExercising:
            btnExercising.setTitleColor(UIColor.white, for: .normal)
            btnExercising.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "exercising")
                        
            seventhLine()
            break
        case btnReading:
            btnReading.setTitleColor(UIColor.white, for: .normal)
            btnReading.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "reading")
                     
            eightLine()
            break
        case btnWatching:
            btnWatching.setTitleColor(UIColor.white, for: .normal)
            btnWatching.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "watching")

            eightLine()
            break
        case btnShopping:
            btnShopping.setTitleColor(UIColor.white, for: .normal)
            btnShopping.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "shopping")
            
            eightLine()
            break
        case btnDancing:
            btnDancing.setTitleColor(UIColor.white, for: .normal)
            btnDancing.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "dancing")

            nineLine()
            break
        case btnSinging:
            btnSinging.setTitleColor(UIColor.white, for: .normal)
            btnSinging.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "singing")

            nineLine()
            break
        case btnPartying:
            btnPartying.setTitleColor(UIColor.white, for: .normal)
            btnPartying.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "partying")
              
            tenLine()
            break
        case btnGaming:
            btnGaming.setTitleColor(UIColor.white, for: .normal)
            btnGaming.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "gaming")
                   
            tenLine()
            break
        case btnListening:
            btnListening.setTitleColor(UIColor.white, for: .normal)
            btnListening.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "listening")
            
            tenLine()
            break
        case btnCelebrating:
            btnCelebrating.setTitleColor(UIColor.white, for: .normal)
            btnCelebrating.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "celebrating")
          
            elevenLine()
            break
        case btnSleeping:
            btnSleeping.setTitleColor(UIColor.white, for: .normal)
            btnSleeping.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "sleeping")
                       
            elevenLine()
            break
        case btnResting:
            btnResting.setTitleColor(UIColor.white, for: .normal)
            btnResting.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "resting")
                      
            tweleveLine()
            break
        case btnRelaxing:
            btnRelaxing.setTitleColor(UIColor.white, for: .normal)
            btnRelaxing.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "relaxing")
            
            tweleveLine()
            break
        case btnMeditating:
            btnMeditating.setTitleColor(UIColor.white, for: .normal)
            btnMeditating.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "meditating")
            
            tweleveLine()
            break
        // Home
        case btnBaking:
            btnBaking.setTitleColor(UIColor.white, for: .normal)
            btnBaking.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "baking")
            
            thirteenLine()
            break
        case btnCooking:
            btnCooking.setTitleColor(UIColor.white, for: .normal)
            btnCooking.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "cooking")
            
            thirteenLine()
            break
        case btnEating:
            btnEating.setTitleColor(UIColor.white, for: .normal)
            btnEating.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "eating")
            
            fourtheenLine()
            break
        case btnCleaning:
            btnCleaning.setTitleColor(UIColor.white, for: .normal)
            btnCleaning.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "cleaning")
            
            fourtheenLine()
            break
        // MARK: - Vacation
        case btnTravelling:
            btnTravelling.setTitleColor(UIColor.white, for: .normal)
            btnTravelling.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "travelling")
            
            fifteenLine()
            break
        case btnDriving:
            btnDriving.setTitleColor(UIColor.white, for: .normal)
            btnDriving.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "driving")
            
            fifteenLine()
            break
        case btnJogging:
            btnJogging.setTitleColor(UIColor.white, for: .normal)
            btnJogging.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "jogging")
            
            fifteenLine()
            break
        case btnCommuting:
            btnCommuting.setTitleColor(UIColor.white, for: .normal)
            btnCommuting.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "commuting")
            
            sixteenLine()
            break
        case btnVisiting:
            btnVisiting.setTitleColor(UIColor.white, for: .normal)
            btnVisiting.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "visiting")
            
            sixteenLine()
            break
        case btnRiding:
            btnRiding.setTitleColor(UIColor.white, for: .normal)
            btnRiding.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "riding")
            
            sixteenLine()
            break
        // MARK: - School
        case btnStudying:
            btnStudying.setTitleColor(UIColor.white, for: .normal)
            btnStudying.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "studying")
            
            seventeenLine()
            break
        case btnDiscovering:
            btnDiscovering.setTitleColor(UIColor.white, for: .normal)
            btnDiscovering.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "discovering")
            
            seventeenLine()
            break
        case btnResolving:
            btnResolving.setTitleColor(UIColor.white, for: .normal)
            btnResolving.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "resolving")
            
            eighteenLine()
            break
        case btnBuilding:
            btnBuilding.setTitleColor(UIColor.white, for: .normal)
            btnBuilding.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "building")
            
            eighteenLine()
            break
        case btnCreating:
            btnCreating.setTitleColor(UIColor.white, for: .normal)
            btnCreating.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "creating")
            
            eighteenLine()
            break
        case btnAttending:
            btnAttending.setTitleColor(UIColor.white, for: .normal)
            btnAttending.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "attending")
            
            nineteenLine()
            break
        case btnDecorating:
            btnDecorating.setTitleColor(UIColor.white, for: .normal)
            btnDecorating.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "decorating")
            
            nineteenLine()
            break
        case btnWriting:
            btnWriting.setTitleColor(UIColor.white, for: .normal)
            btnWriting.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "writing")
            
            twentyLine()
            break
        case btnReporting:
            btnReporting.setTitleColor(UIColor.white, for: .normal)
            btnReporting.backgroundColor = UIColor(red: 53/255.0, green: 183/255.0, blue: 198/255.0, alpha: 1)
            
            darkmodeButton(buttonName: "reporting")
            
            twentyLine()
            break
            
        default: break
            
        }
    }
    private func firstLine(){
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func secondLine(){
        firstRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func thirdLine(){
        firstRow()
        secondRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func fourthLine(){
        firstRow()
        secondRow()
        thirdRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func fifthLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func sixthLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func seventhLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func eightLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func nineLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func tenLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func elevenLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func tweleveLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func thirteenLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func fourtheenLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func fifteenLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func sixteenLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func seventeenLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func eighteenLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        nineteenthRow()
        twentyRow()
    }
    private func nineteenLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        twentyRow()
    }
    private func twentyLine(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
    }
    func firstRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnDesigning.backgroundColor = UIColor.white
                btnDesigning.setTitleColor(UIColor.lightGray, for: .normal)
                           
                btnWorking.setTitleColor(UIColor.lightGray, for: .normal)
                btnWorking.backgroundColor = UIColor.white
                break
            case .dark:
                // dark mode detected
                btnWorking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnWorking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                      
                btnDesigning.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnDesigning.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
                }
            } else {
                // Fallback on earlier versions
        }
    }
    func secondRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
               btnMeeting.backgroundColor = UIColor.white
               btnAnalyzing.backgroundColor = UIColor.white
               btnMaintenance.backgroundColor = UIColor.white
                    
               btnMeeting.setTitleColor(UIColor.lightGray, for: .normal)
               btnAnalyzing.setTitleColor(UIColor.lightGray, for: .normal)
               btnMaintenance.setTitleColor(UIColor.lightGray, for: .normal)
              break
                
            case .dark:
                // dark mode detected
                btnMeeting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnAnalyzing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnMaintenance.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                                   
                btnMeeting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnAnalyzing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnMaintenance.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func thirdRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnTracking.backgroundColor = UIColor.white
                btnTracking.setTitleColor(UIColor.lightGray, for: .normal)
                     
                btnAuditing.setTitleColor(UIColor.lightGray, for: .normal)
                btnAuditing.backgroundColor = UIColor.white
                break
            case .dark:
                // dark mode detected
                btnAuditing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnTracking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                                   
                btnAuditing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnTracking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func fourthRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
               btnOrganizing.backgroundColor = UIColor.white
                btnEvaluating.backgroundColor = UIColor.white
                btnTesting.backgroundColor = UIColor.white
                
                btnOrganizing.setTitleColor(UIColor.lightGray, for: .normal)
                btnEvaluating.setTitleColor(UIColor.lightGray, for: .normal)
                btnTesting.setTitleColor(UIColor.lightGray, for: .normal)
                break
            case .dark:
                // dark mode detected
                btnOrganizing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnEvaluating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnTesting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnOrganizing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnEvaluating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnTesting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                break
            }
        } else {
            // Fallback on earlier versions
        }

    }
    func fifthRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnThinking.backgroundColor = UIColor.white
                btnProgramming.backgroundColor = UIColor.white
                
                btnThinking.setTitleColor(UIColor.lightGray, for: .normal)
                btnProgramming.setTitleColor(UIColor.lightGray, for: .normal)
                break
            case .dark:
                // dark mode detected
                btnThinking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                 btnProgramming.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                 btnProgramming.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                 btnThinking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func sixthRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnIdentifying.backgroundColor = UIColor.white
                btnInterpreting.backgroundColor = UIColor.white
                
                btnIdentifying.setTitleColor(UIColor.lightGray, for: .normal)
                btnInterpreting.setTitleColor(UIColor.lightGray, for: .normal)
                break
            case .dark:
                // dark mode detected
                btnIdentifying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnInterpreting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnIdentifying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnInterpreting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    func seventhRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnPlaying.backgroundColor = UIColor.white
                btnPlaying.setTitleColor(UIColor.lightGray, for: .normal)
                
                btnExercising.setTitleColor(UIColor.lightGray, for: .normal)
                btnExercising.backgroundColor = UIColor.white
                break
            case .dark:
                // dark mode detected
                btnPlaying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnPlaying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnExercising.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnExercising.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func eightRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnReading.backgroundColor = UIColor.white
                btnWatching.backgroundColor = UIColor.white
                btnShopping.backgroundColor = UIColor.white
                
                btnReading.setTitleColor(UIColor.lightGray, for: .normal)
                btnWatching.setTitleColor(UIColor.lightGray, for: .normal)
                btnShopping.setTitleColor(UIColor.lightGray, for: .normal)
                break
            case .dark:
                // dark mode detected
                btnReading.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnWatching.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnShopping.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnReading.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnWatching.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnShopping.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func ninethRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnSinging.backgroundColor = UIColor.white
                btnDancing.backgroundColor = UIColor.white
                
                
                btnSinging.setTitleColor(UIColor.lightGray, for: .normal)
                btnDancing.setTitleColor(UIColor.lightGray, for: .normal)
                break
            case .dark:
                // dark mode detected
                btnSinging.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnDancing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnSinging.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnDancing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func tenthRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnPartying.backgroundColor = UIColor.white
                btnGaming.backgroundColor = UIColor.white
                btnListening.backgroundColor = UIColor.white
                
                btnPartying.setTitleColor(UIColor.lightGray, for: .normal)
                btnGaming.setTitleColor(UIColor.lightGray, for: .normal)
                btnListening.setTitleColor(UIColor.lightGray, for: .normal)
                break
            case .dark:
                // dark mode detected
                btnPartying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnGaming.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnListening.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnPartying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnGaming.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnListening.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func eleventhRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnCelebrating.backgroundColor = UIColor.white
                btnCelebrating.setTitleColor(UIColor.lightGray, for: .normal)
                
                btnSleeping.setTitleColor(UIColor.lightGray, for: .normal)
                btnSleeping.backgroundColor = UIColor.white
                break
            case .dark:
                // dark mode detected
                btnCelebrating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnSleeping.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnCelebrating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnSleeping.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func twelvethRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnResting.backgroundColor = UIColor.white
                btnRelaxing.backgroundColor = UIColor.white
                btnMeditating.backgroundColor = UIColor.white
                
                btnResting.setTitleColor(UIColor.lightGray, for: .normal)
                btnRelaxing.setTitleColor(UIColor.lightGray, for: .normal)
                btnMeditating.setTitleColor(UIColor.lightGray, for: .normal)
                break
            case .dark:
                // dark mode detected
                btnResting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnRelaxing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnMeditating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnResting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnRelaxing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnMeditating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func thirteenthRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnBaking.backgroundColor = UIColor.white
                btnCooking.backgroundColor = UIColor.white
                
                btnBaking.setTitleColor(UIColor.lightGray, for: .normal)
                btnCooking.setTitleColor(UIColor.lightGray, for: .normal)
                break
            case .dark:
                // dark mode detected
                btnBaking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnCooking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnBaking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnCooking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func fourteenthRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnEating.backgroundColor = UIColor.white
                btnCleaning.backgroundColor = UIColor.white
                
                btnEating.setTitleColor(UIColor.lightGray, for: .normal)
                btnCleaning.setTitleColor(UIColor.lightGray, for: .normal)
                break
            case .dark:
                // dark mode detected
                btnEating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnCleaning.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnEating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnCleaning.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
                   }
        } else {
            // Fallback on earlier versions
        }
    }
    func fiftheenthRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                 btnTravelling.backgroundColor = UIColor.white
                 btnTravelling.setTitleColor(UIColor.lightGray, for: .normal)
                       
                 btnDriving.setTitleColor(UIColor.lightGray, for: .normal)
                 btnDriving.backgroundColor = UIColor.white
                       
                 btnJogging.setTitleColor(UIColor.lightGray, for: .normal)
                 btnJogging.backgroundColor = UIColor.white
                break
            case .dark:
                // dark mode detected
                btnTravelling.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnTravelling.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                       
                btnDriving.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnDriving.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                       
                btnJogging.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnJogging.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                break
                   }
        } else {
            // Fallback on earlier versions
        }
    }
    func sixteenthRow(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
                case .light, .unspecified:
                // light mode detected
                    btnCommuting.backgroundColor = UIColor.white
                    btnCommuting.setTitleColor(UIColor.lightGray, for: .normal)
                               
                    btnVisiting.setTitleColor(UIColor.lightGray, for: .normal)
                    btnVisiting.backgroundColor = UIColor.white
                               
                    btnRiding.setTitleColor(UIColor.lightGray, for: .normal)
                    btnRiding.backgroundColor = UIColor.white
                    break
                case .dark:
                // dark mode detected
                btnCommuting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnCommuting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                                                  
                btnVisiting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnVisiting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                                                  
                btnRiding.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnRiding.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                break
            }
        } else {
                // Fallback on earlier versions
        }
    }
    func seventeenthRow(){
        if #available(iOS 12.0, *) {
        switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnStudying.backgroundColor = UIColor.white
                btnStudying.setTitleColor(UIColor.lightGray, for: .normal)
                       
                btnDiscovering.setTitleColor(UIColor.lightGray, for: .normal)
                btnDiscovering.backgroundColor = UIColor.white
                break
            case .dark:
                // dark mode detected
                btnStudying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnStudying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                   
                btnDiscovering.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnDiscovering.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                break
                }
            
            }else{
            // Fallback on earlier versions
        }
    }
    func eighteenthRow(){
        if #available(iOS 12.0, *) {
        switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnResolving.backgroundColor = UIColor.white
                btnResolving.setTitleColor(UIColor.lightGray, for: .normal)
                
                btnBuilding.setTitleColor(UIColor.lightGray, for: .normal)
                btnBuilding.backgroundColor = UIColor.white
                
                btnCreating.setTitleColor(UIColor.lightGray, for: .normal)
                btnCreating.backgroundColor = UIColor.white
                break
            case .dark:
                // dark mode detected
                
                btnResolving.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnResolving.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnBuilding.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnBuilding.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnCreating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnCreating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                break
            }
        }else{
            // Fallback on earlier versions
        }
    }
    func nineteenthRow(){
        if #available(iOS 12.0, *) {
        switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnAttending.setTitleColor(UIColor.lightGray, for: .normal)
                btnAttending.backgroundColor = UIColor.white
                
                btnDecorating.setTitleColor(UIColor.lightGray, for: .normal)
                btnDecorating.backgroundColor = UIColor.white
                break
            case .dark:
                // dark mode detected
                btnAttending.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnAttending.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnDecorating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnDecorating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                break
            }
            
        }else{
            // Fallback on earlier versions
        }
    }
    func twentyRow(){
        if #available(iOS 12.0, *) {
        switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnWriting.setTitleColor(UIColor.lightGray, for: .normal)
                btnWriting.backgroundColor = UIColor.white
                
                btnReporting.setTitleColor(UIColor.lightGray, for: .normal)
                btnReporting.backgroundColor = UIColor.white
                break
            case .dark:
                // dark mode detected
                btnWriting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnWriting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnReporting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnReporting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                break
            }
        }else{
            // Fallback on earlier versions
        }
        
    }
}

private var kAssociationKeyMaxLength: Int = 0

extension UITextField {
    
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            self.addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    //The method is used to cancel the check when use Chinese Pinyin input method.
    //Becuase the alphabet also appears in the textfield when inputting, we should cancel the check.
    func isInputMethod() -> Bool {
        if let positionRange = self.markedTextRange {
            if let _ = self.position(from: positionRange.start, offset: 0) {
                return true
            }
        }
        return false
    }
    @objc func checkMaxLength(textField: UITextField) {
        
        guard !self.isInputMethod(), let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = prospectiveText.substring(to: maxCharIndex)
        selectedTextRange = selection
    }
}

extension PostStatusVc {
    func monitorNetworkConnection() {
        
        reachability?.whenReachable = { reachability in
            // this block triggered multiple times, thats the problem.
            print("reachable")
            DispatchQueue.main.async {
                self.connectionView.isHidden = true
            }
        }
        
        reachability?.whenUnreachable = { _ in
            
            
            print("unreachable")
            
            DispatchQueue.main.async {
                self.connectionView.isHidden = false
            }
        }
        notifyClass()
    }
    func notifyClass() {
        do {
            try reachability?.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    func toast(message: String){
        
        let alertDisapperTimeInSeconds = 2.0
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        self.present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + alertDisapperTimeInSeconds) {
            alert.dismiss(animated: true)
        }
    }
    func statusUserDefault(status: String){
        btnEraseIsTrue()
        switch status {
        case "Working":
            
            buttonBackground(btn: btnWorking)
            btnSelected = btnWorking
            Constant.btnWorkBool = false
            break
        case "Designing":
            buttonBackground(btn: btnDesigning)
            btnSelected = btnDesigning
            Constant.bntDesignBool = false
            break
        case "Meeting":
            buttonBackground(btn: btnMeeting)
            btnSelected = btnMeeting
            Constant.btnMeetBool = false
            break
        case "Analyzing":
            buttonBackground(btn: btnAnalyzing)
            Constant.btnAnalyzeBool = false
            break
        case "Monitoring":
            buttonBackground(btn: btnMaintenance)
            btnSelected = btnMaintenance
            Constant.btnMonitorBool = false
            break
        case "Tracking":
            buttonBackground(btn: btnTracking)
            btnSelected = btnTracking
            Constant.btnTrackBool = false
            break
        case "Auditing":
            buttonBackground(btn: btnAuditing)
            btnSelected = btnAuditing
            Constant.btnAuditBool = false
            break
        case "Organizing":
            buttonBackground(btn: btnOrganizing)
            btnSelected = btnOrganizing
            Constant.btnOrganizeBool = false
            break
        case "Evaluating":
            buttonBackground(btn: btnEvaluating)
            btnSelected = btnEvaluating
            Constant.btnEvaluteBool = false
            break
        case "Testing":
            buttonBackground(btn: btnTesting)
            btnSelected = btnTesting
            Constant.btnTestBool = false
            break
        case "Thinking":
            buttonBackground(btn: btnThinking)
            btnSelected = btnThinking
            Constant.btnThinkBool = false
            break
        case "Programming":
            buttonBackground(btn: btnProgramming)
            btnSelected = btnProgramming
            Constant.btnProgramBool = false
            break
        case "Interpreting":
            buttonBackground(btn: btnInterpreting)
            btnSelected = btnInterpreting
            Constant.btnInterpretBool = false
            break
        case "Identifying":
            buttonBackground(btn: btnIdentifying)
            btnSelected = btnIdentifying
            Constant.btnIndentifyBool = false
            break
        case "Playing":
            buttonBackground(btn: btnPlaying)
            btnSelected = btnPlaying
            Constant.btnPlayBool = false
            break
        case "Exercising":
            buttonBackground(btn: btnExercising)
            btnSelected = btnExercising
            Constant.btnExerciseBool = false
            break
        case "Reading":
            buttonBackground(btn: btnReading)
            btnSelected = btnReading
            Constant.btnReadBool = false
            break
        case "Watching":
            buttonBackground(btn: btnWatching)
            btnSelected = btnWatching
            Constant.btnWatchBool = false
            break
        case "Shopping":
            buttonBackground(btn: btnShopping)
            btnSelected = btnShopping
            Constant.btnShopBool = false
            break
        case "Dancing":
            buttonBackground(btn: btnDancing)
            btnSelected = btnShopping
            Constant.btnDanceBool = false
            break
        case "Singing":
            buttonBackground(btn: btnSinging)
            btnSelected = btnSinging
            Constant.btnSingBool = false
            break
        case "Partying":
            buttonBackground(btn: btnPartying)
            btnSelected = btnPartying
            Constant.btnPartyBool = false
            break
        case "Gaming":
            buttonBackground(btn: btnGaming)
            btnSelected = btnGaming
            Constant.btnGameBool = false
            break
        case "Listening":
            buttonBackground(btn: btnListening)
            btnSelected = btnListening
            Constant.btnListenBool = false
            break
        case "Celebrating":
            buttonBackground(btn: btnCelebrating)
            btnSelected = btnCelebrating
            break
        case "Sleeping":
            buttonBackground(btn: btnSleeping)
            btnSelected = btnSleeping
            Constant.btnCelebrateBool = false
            break
        case "Resting":
            buttonBackground(btn: btnResting)
            btnSelected = btnResting
            Constant.btnRestBool = false
            break
        case "Relaxing":
            buttonBackground(btn: btnRelaxing)
            btnSelected = btnRelaxing
            Constant.btnRelaxBool = false
            break
        case "Meditating":
            buttonBackground(btn: btnMeditating)
            btnSelected = btnMeditating
            Constant.btnMeditateBool = false
            break
        case "Baking":
            buttonBackground(btn: btnBaking)
            btnSelected = btnBaking
            Constant.btnBakeBool = false
            break
        case "Cooking":
            buttonBackground(btn: btnCooking)
            btnSelected = btnCooking
            Constant.btnCookBool = false
            break
        case "Eating":
            buttonBackground(btn: btnEating)
            btnSelected = btnEating
            Constant.btnEatBool = false
            break
        case "Cleaning":
            buttonBackground(btn: btnCleaning)
            btnSelected = btnCleaning
            Constant.btnCleanBool = false
            break
        case "Travelling":
            buttonBackground(btn: btnTravelling)
            btnSelected = btnTravelling
            Constant.btnTravelBool = false
            break
        case "Driving":
            buttonBackground(btn: btnDriving)
            btnSelected = btnDriving
            Constant.btnDriveBool = false
            break
        case "Jogging":
            buttonBackground(btn: btnJogging)
            btnSelected = btnJogging
            Constant.btnJogBool = false
            break
        case "Commuting":
            buttonBackground(btn: btnCommuting)
            btnSelected = btnCommuting
            Constant.btnCommuteBool = false
            break
        case "Visiting":
            buttonBackground(btn: btnVisiting)
            btnSelected = btnVisiting
            Constant.btnVisitBool = false
            break
        case "Riding":
            buttonBackground(btn: btnRiding)
            btnSelected = btnRiding
            Constant.btnRidingBool = false
            break
        case "Studying":
            buttonBackground(btn: btnStudying)
            btnSelected = btnStudying
            Constant.btnStudyBool = false
            break
        case "Discovering":
            buttonBackground(btn: btnDiscovering)
            btnSelected = btnDiscovering
            Constant.btnDiscoverBool = false
            break
        case "Resolving":
            buttonBackground(btn: btnResolving)
            btnSelected = btnResolving
            Constant.btnResolveBool = false
            break
        case "Building":
            buttonBackground(btn: btnBuilding)
            btnSelected = btnBuilding
            Constant.btnBuildBool = false
            break
        case "Creating":
            buttonBackground(btn: btnCreating)
            btnSelected = btnCreating
            Constant.btnCreateBool = false
            break
        case "Attending":
            buttonBackground(btn: btnAttending)
            btnSelected = btnAttending
            Constant.btnAttendBool = false
            break
        case "Decorating":
            buttonBackground(btn: btnDecorating)
            btnSelected = btnDecorating
            Constant.btnDecorateBool = false
            break
        case "Writing":
            buttonBackground(btn: btnWriting)
            btnSelected = btnWriting
            Constant.btnWriteBool = false
            break
        case "Reporting":
            buttonBackground(btn: btnReporting)
            btnSelected = btnReporting
            Constant.btnReportBool = false
            break
        default: break
            
        }
    }
    private func japToEng(lang: String){
        let trans = NSLocalizedString(lang, comment: "")
        switch trans {
        case ("仕事中"):
            statusUserDefault(status: "Working")
            Constant.btnWorkBool = false
            break
        case "デザイン中":
            statusUserDefault(status: "Designing")
            Constant.bntDesignBool = false
            break
        case "打合せ中":
            statusUserDefault(status: "Meeting")
            Constant.btnMeetBool = false
            break
        case "分析中":
            statusUserDefault(status: "Analyzing")
            Constant.btnAnalyzeBool = false
            break
        case "観察中":
            statusUserDefault(status: "Monitoring")
            Constant.btnMonitorBool = false
            break
        case "追跡中":
            statusUserDefault(status: "Tracking")
            Constant.btnTrackBool = false
            break
        case "編集中":
            statusUserDefault(status: "Auditing")
            Constant.btnAuditBool = false
            break
        case "整理中":
            statusUserDefault(status: "Organizing")
            Constant.btnOrganizeBool = false
            break
        case "評価中":
            statusUserDefault(status: "Evaluating")
            Constant.btnEvaluteBool = false
            break
        case "試験中":
            statusUserDefault(status: "Testing")
            Constant.btnTestBool = false
            break
        case "考え中":
            statusUserDefault(status: "Thinking")
            Constant.btnThinkBool = false
            break
        case "プログラム中":
            statusUserDefault(status: "Programming")
            Constant.btnProgramBool = false
            break
        case "翻訳中":
            statusUserDefault(status: "Interpreting")
            Constant.btnInterpretBool = false
            break
        case "確認中":
            statusUserDefault(status: "Identifying")
            Constant.btnIndentifyBool = false
            break
        case "試合中":
            statusUserDefault(status: "Playing")
            Constant.btnPlayBool = false
            break
        case "エクササイズ中":
            statusUserDefault(status: "Exercising")
            Constant.btnExerciseBool = false
            break
        case "読書中":
            statusUserDefault(status: "Reading")
            Constant.btnReadBool = false
            break
        case "鑑賞中":
            statusUserDefault(status: "Watching")
            Constant.btnWatchBool = false
            break
        case "買い物中":
            statusUserDefault(status: "Shopping")
            Constant.btnShopBool = false
            break
        case "ダンス":
            statusUserDefault(status: "Dancing")
            Constant.btnDanceBool = false
            break
        case "カラオケ中":
            statusUserDefault(status: "Singing")
            Constant.btnSingBool = false
            break
        case "パーティー中":
            statusUserDefault(status: "Partying")
            Constant.btnPartyBool = false
            break
        case "ゲーム中":
            statusUserDefault(status: "Gaming")
            Constant.btnGameBool = false
            break
        case "音楽鑑賞中":
            statusUserDefault(status: "Listening")
            Constant.btnListenBool = false
            break
        case "お祝い中":
            statusUserDefault(status: "Celebrating")
            Constant.btnCelebrateBool = false
            break
        case "睡眠中":
            statusUserDefault(status: "Sleeping")
            Constant.btnSleepBool = false
            break
        case "休憩中":
            statusUserDefault(status: "Resting")
            Constant.btnRestBool = false
            break
        case "リラックス中":
            statusUserDefault(status: "Relaxing")
            Constant.btnRelaxBool = false
            break
        case "瞑想中":
            statusUserDefault(status: "Meditating")
            Constant.btnMeditateBool = false
            break
        case "おやつ作り中":
            statusUserDefault(status: "Baking")
            Constant.btnBakeBool = false
            break
        case "料理中":
            statusUserDefault(status: "Cooking")
            Constant.btnCookBool = false
            break
        case "食事中":
            statusUserDefault(status: "Eating")
            Constant.btnEatBool = false
            break
        case "掃除中":
            statusUserDefault(status: "Cleaning")
            Constant.btnCleanBool = false
            break
        case "旅行中":
            statusUserDefault(status: "Travelling")
            Constant.btnTravelBool = false
            break
        case "運転中":
            statusUserDefault(status: "Driving")
            Constant.btnDriveBool = false
            break
        case "ジョギング中":
            statusUserDefault(status: "Jogging")
            Constant.btnJogBool = false
            break
        case "通勤中":
            statusUserDefault(status: "Commuting")
            Constant.btnCommuteBool = false
            break
        case "訪問中":
            statusUserDefault(status: "Visiting")
            Constant.btnVisitBool = false
            break
        case "乗車中":
            statusUserDefault(status: "Riding")
            Constant.btnRidingBool = false
            break
        case "勉強中":
            statusUserDefault(status: "Studying")
            Constant.btnStudyBool = false
            break
        case "発見中":
            statusUserDefault(status: "Discovering")
            Constant.btnDiscoverBool = false
            break
        case "問題解決中":
            statusUserDefault(status: "Resolving")
            Constant.btnResolveBool = false
            break
        case "建設中":
            statusUserDefault(status: "Building")
            Constant.btnBuildBool = false
            break
        case "創造中":
            statusUserDefault(status: "Creating")
            Constant.btnCreateBool = false
            break
        case "出席中":
            statusUserDefault(status: "Attending")
            Constant.btnAttendBool = false
            break
        case "装飾中":
            statusUserDefault(status: "Decorating")
            Constant.btnDecorateBool = false
            break
        case "執筆中":
            statusUserDefault(status: "Writing")
            Constant.btnWriteBool = false
            break
        case "レポート作成中":
            statusUserDefault(status: "Reporting")
            Constant.btnReportBool = false
            break
        default:
            break
        }
    }
    private func convertion(lang: String) -> String{
        switch lang {
        case ("仕事中"):
            return "Working"
        case "デザイン中":
            return "Designing"
        case "打合せ中":
            return "Meeting"
        case "分析中":
            return "Analyzing"
        case "観察中":
            return "Monitoring"
        case "追跡中":
            return "Tracking"
        case "編集中":
            return "Auditing"
        case "整理中":
            return "Organizing"
        case "評価中":
            return "Evaluating"
        case "試験中":
            return "Testing"
        case "考え中":
            return "Thinking"
        case "プログラム中":
            return "Programming"
        case "翻訳中":
            return "Interpreting"
        case "確認中":
            return "Identifying"
        case "試合中":
            return "Playing"
        case "エクササイズ中":
            return "Exercising"
        case "読書中":
            return "Reading"
        case "鑑賞中":
            return "Watching"
        case "買い物中":
            return "Shopping"
        case "ダンス":
            return "Dancing"
        case "カラオケ中":
            return "Singing"
        case "パーティー中":
            return "Partying"
        case "ゲーム中":
            return "Gaming"
        case "音楽鑑賞中":
            return "Listening"
        case "お祝い中":
            return "Celebrating"
        case "睡眠中":
            return "Sleeping"
        case "休憩中":
            return "Resting"
        case "リラックス中":
            return "Relaxing"
        case "瞑想中":
            return "Meditating"
        case "おやつ作り中":
            return "Baking"
        case "料理中":
            return "Cooking"
        case "食事中":
            return "Eating"
        case "掃除中":
            return "Cleaning"
        case "旅行中":
            return "Travelling"
        case "運転中":
            return "Driving"
        case "ジョギング中":
            return "Jogging"
        case "通勤中":
            return "Commuting"
        case "訪問中":
            return "Visiting"
        case "乗車中":
            return "Riding"
        case "勉強中":
            return "Studying"
        case "発見中":
            return "Discovering"
        case "問題解決中":
            return "Resolving"
        case "建設中":
            return "Building"
        case "創造中":
            return "Creating"
        case "出席中":
            return "Attending"
        case "装飾中":
            return "Decorating"
        case "執筆中":
            return "Writing"
        case "レポート作成中":
            return "Reporting"
        default:
            return lang
        }
    }

}




