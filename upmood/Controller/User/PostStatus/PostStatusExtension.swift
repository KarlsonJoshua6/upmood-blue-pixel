//
//  PostStatusExtension.swift
//  upmood
//
//  Created by jomarie madijanon on 22/05/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import UIKit

extension PostStatusVc{
    func deselectButton(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolCall(){
        boolOfWork()
        boolOfDesign()
        boolOfMeet()
        boolOfAnalyze()
        boolOfMonitor()
        boolOfTrack()
        boolOfAudit()
        boolOfOrganize()
        boolOfEvalute()
        boolOfTest()
        boolOfThink()
        boolOfProgram()
        boolOfInterpret()
        boolOfIndentify()
        
        boolOfPlay()
        boolOfExercise()
        boolOfRead()
        boolOfWatch()
        boolOfShop()
        boolOfDance()
        boolOfSing()
        boolOfParty()
        boolOfGame()
        boolOfListen()
        boolOfCelebrate()
        boolOfSleep()
        boolOfRest()
        boolOfRelax()
        boolOfMeditate()
        
        boolOfBake()
        boolOfCook()
        boolOfEat()
        boolOfClean()
        
        boolOfTravel()
        boolOfDrive()
        boolOfJog()
        boolOfCommute()
        boolOfVisit()
        boolOfRide()
        
        boolOfStudy()
        boolOfDiscover()
        boolOfResolve()
        boolOfBuild()
        boolOfCreate()
        boolOfAttend()
        boolOfDecorate()
        boolOfWrite()
        boolOfReport()
    }
    func boolOfWork(){
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfDesign(){
        Constant.btnWorkBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfMeet(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfAnalyze(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfMonitor(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfTrack(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfAudit(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfOrganize(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfEvalute(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfTest(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfThink(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfProgram(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfInterpret(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfIndentify(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfPlay(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfExercise(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfRead(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfWatch(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfShop(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfDance(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfSing(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfParty(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfGame(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfListen(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfCelebrate(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfSleep(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfRest(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfRelax(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfMeditate(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfBake(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfCook(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfEat(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfClean(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfTravel(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfDrive(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfJog(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfCommute(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfVisit(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfRide(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfStudy(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfDiscover(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfResolve(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfBuild(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfCreate(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfAttend(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfDecorate(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnWriteBool = true
        Constant.btnReportBool = true
    }
    func boolOfWrite(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnReportBool = true
    }
    func boolOfReport(){
        Constant.btnWorkBool = true
        Constant.bntDesignBool = true
        Constant.btnMeetBool = true
        Constant.btnAnalyzeBool = true
        Constant.btnMonitorBool = true
        Constant.btnTrackBool = true
        Constant.btnAuditBool = true
        Constant.btnOrganizeBool = true
        Constant.btnEvaluteBool = true
        Constant.btnTestBool = true
        Constant.btnThinkBool = true
        Constant.btnProgramBool = true
        Constant.btnInterpretBool = true
        Constant.btnIndentifyBool = true
        
        Constant.btnPlayBool = true
        Constant.btnExerciseBool = true
        Constant.btnReadBool = true
        Constant.btnWatchBool = true
        Constant.btnShopBool = true
        Constant.btnDanceBool = true
        Constant.btnSingBool = true
        Constant.btnPartyBool = true
        Constant.btnGameBool = true
        Constant.btnListenBool = true
        Constant.btnCelebrateBool = true
        Constant.btnSleepBool = true
        Constant.btnRestBool = true
        Constant.btnRelaxBool = true
        Constant.btnMeditateBool = true
        
        Constant.btnBakeBool = true
        Constant.btnCookBool = true
        Constant.btnEatBool = true
        Constant.btnCleanBool = true
        
        Constant.btnTravelBool = true
        Constant.btnDriveBool = true
        Constant.btnJogBool = true
        Constant.btnCommuteBool = true
        Constant.btnVisitBool = true
        Constant.btnRidingBool = true
        
        Constant.btnStudyBool = true
        Constant.btnDiscoverBool = true
        Constant.btnResolveBool = true
        Constant.btnBuildBool = true
        Constant.btnCreateBool = true
        Constant.btnAttendBool = true
        Constant.btnDecorateBool = true
        Constant.btnWriteBool = true
    }
    
    func postJap(){
        // WORK
        lblWork.text = NSLocalizedString("Work", comment: "")
        btnWorking.setTitle(NSLocalizedString("Working", comment: ""), for: .normal)
        btnDesigning.setTitle(NSLocalizedString("Designing", comment: ""), for: .normal)
        btnMeeting.setTitle(NSLocalizedString("Meeting", comment: ""), for: .normal)
        btnAnalyzing.setTitle(NSLocalizedString("Analyzing", comment: ""), for: .normal)
        btnMaintenance.setTitle(NSLocalizedString("Monitoring", comment: ""), for: .normal)
        btnTracking.setTitle(NSLocalizedString("Tracking", comment: ""), for: .normal)
        btnAuditing.setTitle(NSLocalizedString("Auditing", comment: ""), for: .normal)
        btnOrganizing.setTitle(NSLocalizedString("Organizing", comment: ""), for: .normal)
        btnEvaluating.setTitle(NSLocalizedString("Evaluating", comment: ""), for: .normal)
        btnTesting.setTitle(NSLocalizedString("Testing", comment: ""), for: .normal)
        btnThinking.setTitle(NSLocalizedString("Thinking", comment: ""), for: .normal)
        btnProgramming.setTitle(NSLocalizedString("Programming", comment: ""), for: .normal)
        btnInterpreting.setTitle(NSLocalizedString("Interpreting", comment: ""), for: .normal)
        btnIdentifying.setTitle(NSLocalizedString("Identifying", comment: ""), for: .normal)
        
        // Leisure
        lblLeisure.text = NSLocalizedString("Leisure", comment: "")
        btnPlaying.setTitle(NSLocalizedString("Playing", comment: ""), for: .normal)
        btnExercising.setTitle(NSLocalizedString("Exercising", comment: ""), for: .normal)
        btnReading.setTitle(NSLocalizedString("Reading", comment: ""), for: .normal)
        btnWatching.setTitle(NSLocalizedString("Watching", comment: ""), for: .normal)
        btnShopping.setTitle(NSLocalizedString("Shopping", comment: ""), for: .normal)
        btnDancing.setTitle(NSLocalizedString("Dancing", comment: ""), for: .normal)
        btnSinging.setTitle(NSLocalizedString("Singing", comment: ""), for: .normal)
        btnPartying.setTitle(NSLocalizedString("Partying", comment: ""), for: .normal)
        btnGaming.setTitle(NSLocalizedString("Gaming", comment: ""), for: .normal)
        btnListening.setTitle(NSLocalizedString("Listening", comment: ""), for: .normal)
        btnCelebrating.setTitle(NSLocalizedString("Celebrating", comment: ""), for: .normal)
        btnSleeping.setTitle(NSLocalizedString("Sleeping", comment: ""), for: .normal)
        btnResting.setTitle(NSLocalizedString("Resting", comment: ""), for: .normal)
        btnRelaxing.setTitle(NSLocalizedString("Relaxing", comment: ""), for: .normal)
        btnMeditating.setTitle(NSLocalizedString("Meditating", comment: ""), for: .normal)
        //Home
        lblHome.text = NSLocalizedString("Home", comment: "")
        btnBaking.setTitle(NSLocalizedString("Baking", comment: ""), for: .normal)
        btnCooking.setTitle(NSLocalizedString("Cooking", comment: ""), for: .normal)
        btnEating.setTitle(NSLocalizedString("Eating", comment: ""), for: .normal)
        btnCleaning.setTitle(NSLocalizedString("Cleaning", comment: ""), for: .normal)
        //Vactioin
        lblVacation.text = NSLocalizedString("Vacation", comment: "")
        btnTravelling.setTitle(NSLocalizedString("Travelling", comment: ""), for: .normal)
        btnDriving.setTitle(NSLocalizedString("Driving", comment: ""), for: .normal)
        btnJogging.setTitle(NSLocalizedString("Jogging", comment: ""), for: .normal)
        btnCommuting.setTitle(NSLocalizedString("Commuting", comment: ""), for: .normal)
        btnVisiting.setTitle(NSLocalizedString("Visiting", comment: ""), for: .normal)
        btnRiding.setTitle(NSLocalizedString("Riding", comment: ""), for: .normal)
        
        // School
        lblSchool.text = NSLocalizedString("School", comment: "")
        btnStudying.setTitle(NSLocalizedString("Studying", comment: ""), for: .normal)
        btnDiscovering.setTitle(NSLocalizedString("Discovering", comment: ""), for: .normal)
        btnResolving.setTitle(NSLocalizedString("Resolving", comment: ""), for: .normal)
        btnBuilding.setTitle(NSLocalizedString("Building", comment: ""), for: .normal)
        btnCreating.setTitle(NSLocalizedString("Creating", comment: ""), for: .normal)
        btnAttending.setTitle(NSLocalizedString("Attending", comment: ""), for: .normal)
        btnDecorating.setTitle(NSLocalizedString("Decorating", comment: ""), for: .normal)
        btnWriting.setTitle(NSLocalizedString("Writing", comment: ""), for: .normal)
        btnReporting.setTitle(NSLocalizedString("Reporting", comment: ""), for: .normal)
        
        btnPost.setTitle(NSLocalizedString("Post", comment: ""), for: .normal)
        
        let lang = UserDefaults.standard.string(forKey: "selectedLanguage")
        
        if lang == "ja" {
            lefConstVacation.constant = 100
            leftConstLeisure.constant = 100
        }
    }
    
    func darkmodeButton(buttonName: String){
        
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                switch buttonName{
                case "working":
                    btnDesigning.backgroundColor = UIColor.white
                    btnDesigning.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "designing":
                    btnWorking.setTitleColor(UIColor.lightGray, for: .normal)
                    btnWorking.backgroundColor = UIColor.white
                    break
                case "meeting":
                    btnAnalyzing.backgroundColor = UIColor.white
                    btnMaintenance.backgroundColor = UIColor.white
                               
                    btnAnalyzing.setTitleColor(UIColor.lightGray, for: .normal)
                    btnMaintenance.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "analyzing":
                    btnMeeting.backgroundColor = UIColor.white
                    btnMaintenance.backgroundColor = UIColor.white
                    
                    btnMeeting.setTitleColor(UIColor.lightGray, for: .normal)
                    btnMaintenance.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "monitoring":
                    btnMeeting.backgroundColor = UIColor.white
                    btnAnalyzing.backgroundColor = UIColor.white
                    
                    btnMeeting.setTitleColor(UIColor.lightGray, for: .normal)
                    btnAnalyzing.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "tracking":
                    btnAuditing.backgroundColor = UIColor.white
                    btnAuditing.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "audit":
                    btnTracking.backgroundColor = UIColor.white
                    btnTracking.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "organizing":
                    btnEvaluating.backgroundColor = UIColor.white
                    btnTesting.backgroundColor = UIColor.white
                    
                    btnEvaluating.setTitleColor(UIColor.lightGray, for: .normal)
                    btnTesting.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "evaluating":
                    btnOrganizing.backgroundColor = UIColor.white
                    btnTesting.backgroundColor = UIColor.white
                               
                    btnOrganizing.setTitleColor(UIColor.lightGray, for: .normal)
                    btnTesting.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "testing":
                    btnOrganizing.backgroundColor = UIColor.white
                    btnEvaluating.backgroundColor = UIColor.white
                               
                    btnOrganizing.setTitleColor(UIColor.lightGray, for: .normal)
                    btnEvaluating.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "thinking":
                    btnProgramming.backgroundColor = UIColor.white
                    btnProgramming.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "programming":
                    btnThinking.backgroundColor = UIColor.white
                    btnThinking.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "interpreting":
                    btnIdentifying.backgroundColor = UIColor.white
                    btnIdentifying.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "identifying":
                    btnInterpreting.backgroundColor = UIColor.white
                    btnInterpreting.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "playing":
                    btnExercising.backgroundColor = UIColor.white
                    btnExercising.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "exercising":
                    btnPlaying.backgroundColor = UIColor.white
                    btnPlaying.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "reading":
                    btnWatching.backgroundColor = UIColor.white
                    btnWatching.setTitleColor(UIColor.lightGray, for: .normal)
                    btnShopping.backgroundColor = UIColor.white
                    btnShopping.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "watching":
                    btnReading.backgroundColor = UIColor.white
                    btnReading.setTitleColor(UIColor.lightGray, for: .normal)
                    btnShopping.backgroundColor = UIColor.white
                    btnShopping.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "shopping":
                    btnReading.backgroundColor = UIColor.white
                    btnReading.setTitleColor(UIColor.lightGray, for: .normal)
                    btnWatching.backgroundColor = UIColor.white
                    btnWatching.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "dancing":
                    btnSinging.backgroundColor = UIColor.white
                    btnSinging.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "singing":
                    btnDancing.backgroundColor = UIColor.white
                    btnDancing.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "partying":
                    btnGaming.backgroundColor = UIColor.white
                    btnGaming.setTitleColor(UIColor.lightGray, for: .normal)
                    btnListening.backgroundColor = UIColor.white
                    btnListening.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "gaming":
                    btnPartying.backgroundColor = UIColor.white
                    btnPartying.setTitleColor(UIColor.lightGray, for: .normal)
                    btnListening.backgroundColor = UIColor.white
                    btnListening.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "listening":
                    btnPartying.backgroundColor = UIColor.white
                    btnPartying.setTitleColor(UIColor.lightGray, for: .normal)
                    btnGaming.backgroundColor = UIColor.white
                    btnGaming.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "celebrating":
                    btnSleeping.backgroundColor = UIColor.white
                    btnSleeping.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "sleeping":
                    btnCelebrating.backgroundColor = UIColor.white
                    btnCelebrating.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "resting":
                    btnRelaxing.backgroundColor = UIColor.white
                    btnRelaxing.setTitleColor(UIColor.lightGray, for: .normal)
                    btnMeditating.backgroundColor = UIColor.white
                    btnMeditating.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "relaxing":
                    btnResting.backgroundColor = UIColor.white
                    btnResting.setTitleColor(UIColor.lightGray, for: .normal)
                    btnMeditating.backgroundColor = UIColor.white
                    btnMeditating.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "meditating":
                    btnResting.backgroundColor = UIColor.white
                    btnResting.setTitleColor(UIColor.lightGray, for: .normal)
                    btnRelaxing.backgroundColor = UIColor.white
                    btnRelaxing.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "baking":
                    btnCooking.backgroundColor = UIColor.white
                    btnCooking.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "cooking":
                    btnBaking.backgroundColor = UIColor.white
                    btnBaking.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "eating":
                    btnCleaning.backgroundColor = UIColor.white
                    btnCleaning.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "cleaning":
                    btnEating.backgroundColor = UIColor.white
                    btnEating.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "travelling":
                    btnDriving.backgroundColor = UIColor.white
                    btnDriving.setTitleColor(UIColor.lightGray, for: .normal)
                    btnJogging.backgroundColor = UIColor.white
                    btnJogging.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "driving":
                    btnTravelling.backgroundColor = UIColor.white
                    btnTravelling.setTitleColor(UIColor.lightGray, for: .normal)
                    btnJogging.backgroundColor = UIColor.white
                    btnJogging.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "jogging":
                    btnDriving.backgroundColor = UIColor.white
                    btnDriving.setTitleColor(UIColor.lightGray, for: .normal)
                    btnTravelling.backgroundColor = UIColor.white
                    btnTravelling.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "commuting":
                    btnVisiting.backgroundColor = UIColor.white
                    btnVisiting.setTitleColor(UIColor.lightGray, for: .normal)
                    btnRiding.backgroundColor = UIColor.white
                    btnRiding.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "visiting":
                    btnCommuting.backgroundColor = UIColor.white
                    btnCommuting.setTitleColor(UIColor.lightGray, for: .normal)
                    btnRiding.backgroundColor = UIColor.white
                    btnRiding.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "riding":
                    btnCommuting.backgroundColor = UIColor.white
                    btnCommuting.setTitleColor(UIColor.lightGray, for: .normal)
                    btnVisiting.backgroundColor = UIColor.white
                    btnVisiting.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "studying":
                    btnDiscovering.backgroundColor = UIColor.white
                    btnDiscovering.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "discovering":
                    btnStudying.backgroundColor = UIColor.white
                    btnStudying.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "resolving":
                    btnBuilding.backgroundColor = UIColor.white
                    btnBuilding.setTitleColor(UIColor.lightGray, for: .normal)
                    btnCreating.backgroundColor = UIColor.white
                    btnCreating.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "building":
                    btnResolving.backgroundColor = UIColor.white
                    btnResolving.setTitleColor(UIColor.lightGray, for: .normal)
                    btnCreating.backgroundColor = UIColor.white
                    btnCreating.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "creating":
                    btnResolving.backgroundColor = UIColor.white
                    btnResolving.setTitleColor(UIColor.lightGray, for: .normal)
                    btnBuilding.backgroundColor = UIColor.white
                    btnBuilding.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "attending":
                    btnDecorating.backgroundColor = UIColor.white
                    btnDecorating.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "decorating":
                    btnAttending.backgroundColor = UIColor.white
                    btnAttending.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "writing":
                    btnReporting.backgroundColor = UIColor.white
                    btnReporting.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                case "reporting":
                    btnWriting.backgroundColor = UIColor.white
                    btnWriting.setTitleColor(UIColor.lightGray, for: .normal)
                    break
                default: break
                }
            
               break
                
            case .dark:
                // dark mode detected
                switch buttonName{
                case "working":
                    btnDesigning.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnDesigning.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "designing":
                    btnWorking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnWorking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    break
                case "meeting":
                    btnAnalyzing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnMaintenance.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                                                  
                    btnAnalyzing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnMaintenance.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "analyzing":
                    btnMeeting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnMaintenance.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    
                    btnMeeting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnMaintenance.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "monitoring":
                    btnMeeting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnAnalyzing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    
                    btnMeeting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnAnalyzing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "tracking":
                    btnAuditing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnAuditing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "audit":
                    btnTracking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnTracking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "organizing":
                    btnEvaluating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnTesting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    
                    btnEvaluating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnTesting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "evaluating":
                    btnOrganizing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnTesting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                               
                    btnOrganizing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnTesting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "testing":
                    btnOrganizing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnEvaluating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                                                  
                    btnOrganizing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnEvaluating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "thinking":
                    btnProgramming.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnProgramming.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "programming":
                    btnThinking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnThinking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "interpreting":
                    btnIdentifying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnIdentifying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "identifying":
                    btnInterpreting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnInterpreting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "playing":
                    btnExercising.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnExercising.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "exercising":
                    btnPlaying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnPlaying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "reading":
                    btnWatching.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnWatching.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnShopping.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnShopping.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "watching":
                    btnReading.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnReading.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnShopping.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnShopping.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "shopping":
                    btnReading.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnReading.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnWatching.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnWatching.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "dancing":
                    btnSinging.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnSinging.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "singing":
                    btnDancing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnDancing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "partying":
                    btnGaming.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnGaming.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnListening.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnListening.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "gaming":
                    btnPartying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnPartying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnListening.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnListening.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "listening":
                    btnPartying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnPartying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnGaming.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnGaming.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "celebrating":
                    btnSleeping.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnSleeping.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "sleeping":
                    btnCelebrating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnCelebrating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "resting":
                    btnRelaxing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnRelaxing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnMeditating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnMeditating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "relaxing":
                    btnResting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnResting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnMeditating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnMeditating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "meditating":
                    btnResting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnResting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnRelaxing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnRelaxing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "baking":
                    btnCooking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnCooking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "cooking":
                    btnBaking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnBaking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "eating":
                    btnCleaning.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnCleaning.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "cleaning":
                    btnEating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnEating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "travelling":
                    btnDriving.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnDriving.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnJogging.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnJogging.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "driving":
                    btnTravelling.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnTravelling.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnJogging.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnJogging.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "jogging":
                    btnDriving.backgroundColor =  hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnDriving.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnTravelling.backgroundColor =  hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnTravelling.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "commuting":
                    btnVisiting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnVisiting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnRiding.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnRiding.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "visiting":
                    btnCommuting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnCommuting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnRiding.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnRiding.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "riding":
                    btnCommuting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnCommuting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnVisiting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnVisiting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "studying":
                    btnDiscovering.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnDiscovering.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "discovering":
                    btnStudying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnStudying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "resolving":
                    btnBuilding.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnBuilding.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnCreating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnCreating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "building":
                    btnResolving.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnResolving.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    btnCreating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnCreating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
               case "creating":
                   btnResolving.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                   btnResolving.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                   btnBuilding.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                   btnBuilding.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                   break
                case "attending":
                   btnDecorating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                   btnDecorating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                   break
                case "decorating":
                    btnAttending.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnAttending.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                case "writing":
                    btnReporting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnReporting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
                case "reporting":
                    btnWriting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                    btnWriting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                    break
               default: break
                }
                break

            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    func getAllButtonDarkmode(){
        firstRow()
        secondRow()
        thirdRow()
        fourthRow()
        fifthRow()
        sixthRow()
        seventhRow()
        eightRow()
        ninethRow()
        tenthRow()
        eleventhRow()
        twelvethRow()
        thirteenthRow()
        fourteenthRow()
        fiftheenthRow()
        sixteenthRow()
        seventeenthRow()
        eighteenthRow()
        nineteenthRow()
        twentyRow()
    }
    func darkmodeAllButton(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                btnDesigning.backgroundColor = UIColor.white
                btnDesigning.setTitleColor(UIColor.lightGray, for: .normal)
                             
                btnWorking.setTitleColor(UIColor.lightGray, for: .normal)
                btnWorking.backgroundColor = UIColor.white
                              
                btnAnalyzing.backgroundColor = UIColor.white
                btnMaintenance.backgroundColor = UIColor.white
                                              
                btnAnalyzing.setTitleColor(UIColor.lightGray, for: .normal)
                btnMaintenance.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnMeeting.backgroundColor = UIColor.white
                btnMaintenance.backgroundColor = UIColor.white
                                   
                btnMeeting.setTitleColor(UIColor.lightGray, for: .normal)
                btnMaintenance.setTitleColor(UIColor.lightGray, for: .normal)
                                 
                btnMeeting.backgroundColor = UIColor.white
                btnAnalyzing.backgroundColor = UIColor.white
                                   
                btnMeeting.setTitleColor(UIColor.lightGray, for: .normal)
                btnAnalyzing.setTitleColor(UIColor.lightGray, for: .normal)
                
                btnAuditing.backgroundColor = UIColor.white
                btnAuditing.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnTracking.backgroundColor = UIColor.white
                btnTracking.setTitleColor(UIColor.lightGray, for: .normal)
                              
                btnEvaluating.backgroundColor = UIColor.white
                btnTesting.backgroundColor = UIColor.white
                                   
                btnEvaluating.setTitleColor(UIColor.lightGray, for: .normal)
                btnTesting.setTitleColor(UIColor.lightGray, for: .normal)
                                
                btnOrganizing.backgroundColor = UIColor.white
                btnTesting.backgroundColor = UIColor.white
                                              
                btnOrganizing.setTitleColor(UIColor.lightGray, for: .normal)
                btnTesting.setTitleColor(UIColor.lightGray, for: .normal)
                             
                btnOrganizing.backgroundColor = UIColor.white
                btnEvaluating.backgroundColor = UIColor.white
                                              
                btnOrganizing.setTitleColor(UIColor.lightGray, for: .normal)
                btnEvaluating.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnProgramming.backgroundColor = UIColor.white
                btnProgramming.setTitleColor(UIColor.lightGray, for: .normal)
                              
                btnThinking.backgroundColor = UIColor.white
                btnThinking.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnIdentifying.backgroundColor = UIColor.white
                btnIdentifying.setTitleColor(UIColor.lightGray, for: .normal)
                              
                btnInterpreting.backgroundColor = UIColor.white
                btnInterpreting.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnExercising.backgroundColor = UIColor.white
                btnExercising.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnPlaying.backgroundColor = UIColor.white
                btnPlaying.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnWatching.backgroundColor = UIColor.white
                btnWatching.setTitleColor(UIColor.lightGray, for: .normal)
                btnShopping.backgroundColor = UIColor.white
                btnShopping.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnReading.backgroundColor = UIColor.white
                btnReading.setTitleColor(UIColor.lightGray, for: .normal)
                btnShopping.backgroundColor = UIColor.white
                btnShopping.setTitleColor(UIColor.lightGray, for: .normal)
                              
                btnReading.backgroundColor = UIColor.white
                btnReading.setTitleColor(UIColor.lightGray, for: .normal)
                btnWatching.backgroundColor = UIColor.white
                btnWatching.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnSinging.backgroundColor = UIColor.white
                btnSinging.setTitleColor(UIColor.lightGray, for: .normal)
                                
                btnDancing.backgroundColor = UIColor.white
                btnDancing.setTitleColor(UIColor.lightGray, for: .normal)
                             
                btnGaming.backgroundColor = UIColor.white
                btnGaming.setTitleColor(UIColor.lightGray, for: .normal)
                btnListening.backgroundColor = UIColor.white
                btnListening.setTitleColor(UIColor.lightGray, for: .normal)
                              
                btnPartying.backgroundColor = UIColor.white
                btnPartying.setTitleColor(UIColor.lightGray, for: .normal)
                btnListening.backgroundColor = UIColor.white
                btnListening.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnPartying.backgroundColor = UIColor.white
                btnPartying.setTitleColor(UIColor.lightGray, for: .normal)
                btnGaming.backgroundColor = UIColor.white
                btnGaming.setTitleColor(UIColor.lightGray, for: .normal)
                              
                btnSleeping.backgroundColor = UIColor.white
                btnSleeping.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnCelebrating.backgroundColor = UIColor.white
                btnCelebrating.setTitleColor(UIColor.lightGray, for: .normal)
                                
                btnRelaxing.backgroundColor = UIColor.white
                btnRelaxing.setTitleColor(UIColor.lightGray, for: .normal)
                btnMeditating.backgroundColor = UIColor.white
                btnMeditating.setTitleColor(UIColor.lightGray, for: .normal)
                              
                btnResting.backgroundColor = UIColor.white
                btnResting.setTitleColor(UIColor.lightGray, for: .normal)
                btnMeditating.backgroundColor = UIColor.white
                btnMeditating.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnResting.backgroundColor = UIColor.white
                btnResting.setTitleColor(UIColor.lightGray, for: .normal)
                btnRelaxing.backgroundColor = UIColor.white
                btnRelaxing.setTitleColor(UIColor.lightGray, for: .normal)
                             
                btnCooking.backgroundColor = UIColor.white
                btnCooking.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnBaking.backgroundColor = UIColor.white
                btnBaking.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnCleaning.backgroundColor = UIColor.white
                btnCleaning.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnEating.backgroundColor = UIColor.white
                btnEating.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnDriving.backgroundColor = UIColor.white
                btnDriving.setTitleColor(UIColor.lightGray, for: .normal)
                btnJogging.backgroundColor = UIColor.white
                btnJogging.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnTravelling.backgroundColor = UIColor.white
                btnTravelling.setTitleColor(UIColor.lightGray, for: .normal)
                btnJogging.backgroundColor = UIColor.white
                btnJogging.setTitleColor(UIColor.lightGray, for: .normal)
                              
                btnDriving.backgroundColor = UIColor.white
                btnDriving.setTitleColor(UIColor.lightGray, for: .normal)
                btnTravelling.backgroundColor = UIColor.white
                btnTravelling.setTitleColor(UIColor.lightGray, for: .normal)
                            
                btnVisiting.backgroundColor = UIColor.white
                btnVisiting.setTitleColor(UIColor.lightGray, for: .normal)
                btnRiding.backgroundColor = UIColor.white
                btnRiding.setTitleColor(UIColor.lightGray, for: .normal)
                             
                btnCommuting.backgroundColor = UIColor.white
                btnCommuting.setTitleColor(UIColor.lightGray, for: .normal)
                btnRiding.backgroundColor = UIColor.white
                btnRiding.setTitleColor(UIColor.lightGray, for: .normal)
                             
                btnCommuting.backgroundColor = UIColor.white
                btnCommuting.setTitleColor(UIColor.lightGray, for: .normal)
                btnVisiting.backgroundColor = UIColor.white
                btnVisiting.setTitleColor(UIColor.lightGray, for: .normal)
                             
                btnDiscovering.backgroundColor = UIColor.white
                btnDiscovering.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnStudying.backgroundColor = UIColor.white
                btnStudying.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnBuilding.backgroundColor = UIColor.white
                btnBuilding.setTitleColor(UIColor.lightGray, for: .normal)
                btnCreating.backgroundColor = UIColor.white
                btnCreating.setTitleColor(UIColor.lightGray, for: .normal)
                                
                btnResolving.backgroundColor = UIColor.white
                btnResolving.setTitleColor(UIColor.lightGray, for: .normal)
                btnCreating.backgroundColor = UIColor.white
                btnCreating.setTitleColor(UIColor.lightGray, for: .normal)
                              
                btnResolving.backgroundColor = UIColor.white
                btnResolving.setTitleColor(UIColor.lightGray, for: .normal)
                btnBuilding.backgroundColor = UIColor.white
                btnBuilding.setTitleColor(UIColor.lightGray, for: .normal)
                             
                btnDecorating.backgroundColor = UIColor.white
                btnDecorating.setTitleColor(UIColor.lightGray, for: .normal)
                                
                btnAttending.backgroundColor = UIColor.white
                btnAttending.setTitleColor(UIColor.lightGray, for: .normal)
                               
                btnReporting.backgroundColor = UIColor.white
                btnReporting.setTitleColor(UIColor.lightGray, for: .normal)
                            
                btnWriting.backgroundColor = UIColor.white
                btnWriting.setTitleColor(UIColor.lightGray, for: .normal)
                break
                
            case .dark:
                // dark mode detected
                btnWorking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                btnWorking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                
                btnDesigning.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnDesigning.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                 btnMeeting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                 btnMeeting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnAnalyzing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnAnalyzing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnMaintenance.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnMaintenance.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnAuditing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnAuditing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnTracking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnTracking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnOrganizing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnOrganizing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnEvaluating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnEvaluating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
              
                btnTesting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnTesting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnThinking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnThinking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnProgramming.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnProgramming.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnInterpreting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnInterpreting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnIdentifying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnIdentifying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnExercising.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnExercising.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnPlaying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnPlaying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnReading.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnReading.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnWatching.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnWatching.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnShopping.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnShopping.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnDancing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnDancing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnSinging.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnSinging.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnPartying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnPartying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnGaming.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnGaming.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnListening.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnListening.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnCelebrating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnCelebrating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnSleeping.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnSleeping.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnResting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnResting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnRelaxing.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnRelaxing.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnMeditating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnMeditating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnBaking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnBaking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnCooking.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnCooking.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnEating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnEating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnCleaning.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnCleaning.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnTravelling.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnTravelling.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnDriving.backgroundColor =  hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnDriving.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnJogging.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnJogging.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnCommuting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnCommuting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnVisiting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnVisiting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnRiding.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnRiding.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnStudying.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnStudying.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnDiscovering.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnDiscovering.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnResolving.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnResolving.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnBuilding.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnBuilding.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                                   
                btnCreating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnCreating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnAttending.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnAttending.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnDecorating.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                   btnDecorating.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                
                btnReporting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnReporting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                            
                btnWriting.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_BUTTON_POSTSTATUS)
                btnWriting.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_POSTSTATUS), for: .normal)
                break
              
          }
        } else {
            // Fallback on earlier versions
        }
    }
    
}
