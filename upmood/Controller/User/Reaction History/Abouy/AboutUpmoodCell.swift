//
//  AboutUpmoodCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 28/03/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class AboutUpmoodCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
