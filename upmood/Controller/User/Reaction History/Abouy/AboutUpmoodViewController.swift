//
//  AboutUpmoodViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 28/03/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class AboutUpmoodViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let cellIdentifier = "AboutUpmoodCell"
    private var titles = [NSLocalizedString("About Us", comment: ""), NSLocalizedString("Terms and Condition", comment: ""), NSLocalizedString("Privacy Policy", comment: ""), NSLocalizedString("Frequently Asked Question", comment: "")]
    private var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? AboutUpmoodWebViewViewController  {
            itemDetails.titleOfView = titles[selectedIndex]
        }
    }
}

extension AboutUpmoodViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? AboutUpmoodCell {
            cell.titleLabel.text = titles[indexPath.row]
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        self.didSelect(with: indexPath)
    }
    
    private func didSelect(with: IndexPath) {
        
        switch with.row {
        case 0:
            performSegue(withIdentifier: "showAboutUs", sender: self)
            break
        case 1:
            performSegue(withIdentifier: "showWebView", sender: self)
            break
        case 2:
            performSegue(withIdentifier: "showWebView", sender: self)
            break
        case 3:
            performSegue(withIdentifier: "showFAQ", sender: self)
            break
        default:
            break
        }
    }
}
