//
//  AboutUpmoodWebViewViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 29/03/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit
import WebKit

class AboutUpmoodWebViewViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    var titleOfView = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if titleOfView == "Terms and Condition" || titleOfView == "利用規約"{
            webView.load(URLRequest(url: URL(string: "http://upmood.com/TermsAndConditions")!))
        } else if titleOfView == "Privacy Policy" || titleOfView == "プライバシーポリシー" {
            webView.load(URLRequest(url: URL(string: "http://upmood.com/PrivacyPolicy")!))
        } else {
            print("nothing to show")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationItem.title = titleOfView
    }
}
