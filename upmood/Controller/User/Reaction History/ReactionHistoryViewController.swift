
//
//  ReactionHistoryViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 24/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import CLWaterWaveView
import SDWebImage
import SVProgressHUD
import UIKit

class ReactionHistoryViewController: UIViewController {

    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var emotionImageView: UIImageView!
    @IBOutlet weak var tableVIew: UITableView!
    @IBOutlet weak var emotionLabel: UILabel!
    @IBOutlet weak var waveView: CLWaterWaveView!
    @IBOutlet weak var secondWaveView: CLWaterWaveView!
    
    var id: Int!
    var alamoFireManager : SessionManager?
    
    fileprivate var userName = [String]()
    fileprivate var userProfilePhoto = [String]()
    fileprivate var reaction = [String]()
    fileprivate var reactionStickerPath = [String]()
    fileprivate var emotion_value: String!
    fileprivate var emotion_sticker_filepath: String!
    
    var currentEmotion = ""
    var currentEmotionImageUrlStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gradientView.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
        
//        // water waves
//        waveView.amplitude = 20
//        waveView.speed = 0.008
//        waveView.angularVelocity = 0.31
//        waveView.depth = 0.9
//        waveView.startAnimation()
//
//        secondWaveView.amplitude = 20
//        secondWaveView.speed = 0.01
//        secondWaveView.angularVelocity = 0.28
//        secondWaveView.depth = 0.9
//        secondWaveView.startAnimation()
        
        // water waves
        waveView.amplitude = 20
        waveView.speed = 0.008
        waveView.angularVelocity = 0.31
        waveView.depth = 0.93
        waveView.startAnimation()
        
        secondWaveView.amplitude = 20
        secondWaveView.speed = 0.01
        secondWaveView.angularVelocity = 0.28
        secondWaveView.depth = 0.93
        secondWaveView.startAnimation()
        
        tableVIew.delegate = self
        tableVIew.dataSource = self
        loadReactionHistory(id: self.id ?? 0)
        displayEmotion(emotion_sticker_filepath: self.currentEmotionImageUrlStr, emotion_value: self.currentEmotion)
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }

    private func loadReactionHistory(id: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        
        let userId = UserDefaults.standard.integer(forKey: "fetchUserId")
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 3
        configuration.timeoutIntervalForResource = 3
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        alamoFireManager?.request(APIClient.fetchReactionHistory(id: id, userId: userId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                guard let data = jsonData["data"] as? [String: Any] else { return }
                guard let dataInside = data["data"] as? [[String: Any]] else { return }
                
                SVProgressHUD.dismiss(completion: {
                    if statusCode == 200 {
                        for jsondata in dataInside {
                            let name = jsondata["name"] as? String ?? ""
                            let profilePhoto = jsondata["image"] as? String ?? ""
                            let reaction = jsondata["reaction"] as? String ?? ""
                            let reactionStickerPath = jsondata["reaction_path"] as? String ?? ""
                            this?.userName.append(name)
                            this?.userProfilePhoto.append(profilePhoto)
                            this?.reaction.append(reaction)
                            this?.reactionStickerPath.append(reactionStickerPath)
                        }
                        
                        DispatchQueue.main.async { [weak this = self] in
                            this?.tableVIew.reloadData()
                        }
                        
                    } else if statusCode == 204 {
                        SVProgressHUD.dismiss(completion: {
                            this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No Record Found.")
                        })
                    } else {
                        SVProgressHUD.dismiss(completion: {
                            this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                        })
                    }
                })
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func displayEmotion(emotion_sticker_filepath: String, emotion_value: String) {
        guard let url = URL(string: emotion_sticker_filepath) else { return }
        self.emotionLabel.text = self.setUpCurrentMood(emotion: emotion_value.capitalized)
        self.emotionImageView.sd_setImage(with: url)
    }
    
    fileprivate func setUpCell(cell: ReactionHistoryTableViewCell, indexPath: IndexPath) {
        guard let url = URL(string: "\(userProfilePhoto[indexPath.row].replacingOccurrences(of: " ", with: "%20"))") else { return }
        guard let url2 = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(reactionStickerPath[indexPath.row].replacingOccurrences(of: ".svg", with: ".png").replacingOccurrences(of: " ", with: "%20"))") else { return }
        cell.userProfilePic.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_oops"), options: [], completed: nil)
        cell.emotionStickerImageView.sd_setImage(with: url2, placeholderImage: UIImage(named: "ic_oops"), options: [], completed: nil)
        cell.reactionHistoryLabel.text = "\(self.userName[indexPath.row]) \(NSLocalizedString("reacted", comment: "")) \(self.reaction[indexPath.row]) \(NSLocalizedString("on your mood", comment: ""))."
    }
    
    private func setUpCurrentMood(emotion: String) -> String {
        switch emotion {
           case "Excitement":
              return NSLocalizedString("Excitement", comment: "")
          case "Happy":
              return NSLocalizedString("Happy", comment: "")
          case "Zen":
              return NSLocalizedString("Zen", comment: "")
          case "Pleasant":
              return NSLocalizedString("Pleasant", comment: "")
          case "Calm":
              return NSLocalizedString("Calm", comment: "")
          case "Unpleasant":
              return NSLocalizedString("Unpleasant", comment: "")
          case "Confused":
              return NSLocalizedString("Confused", comment: "")
          case "Challenged":
              return NSLocalizedString("Challenged", comment: "")
          case "Tense":
              return NSLocalizedString("Tense", comment: "")
          case "Sad":
              return NSLocalizedString("Sad", comment: "")
          case "Anxious":
              return NSLocalizedString("Anxious", comment: "")
          case "Loading":
              return NSLocalizedString("Loading", comment: "")
        default:
            return ""
        }
    }
}

extension ReactionHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableVIew.dequeueReusableCell(withIdentifier: "ReactionHistoryTableViewCell", for: indexPath) as? ReactionHistoryTableViewCell {
            setUpCell(cell: cell, indexPath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userName.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
