//
//  MoodStreamTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 20/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class MoodStreamTableViewCell: UITableViewCell {

    @IBOutlet weak var triangleIndicator: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var heartBeatLabel: UILabel!
    @IBOutlet weak var stressLevelLabel: UILabel!
    @IBOutlet weak var emotionLabel: UILabel!
    @IBOutlet weak var showReactionHistoryButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var reactionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
