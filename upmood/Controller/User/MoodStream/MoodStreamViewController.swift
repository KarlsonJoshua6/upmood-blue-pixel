//
//  MoodStreamViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 20/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import CLWaterWaveView
import KeychainSwift
import SVProgressHUD
import SDWebImage
import UIKit

// MARK: Stored - NotificationCenter

class MoodStreamViewController: UIViewController {

    @IBOutlet weak var triangleIndicator: UIImageView!
    @IBOutlet weak var emotionLabel: UILabel!
    @IBOutlet weak var emoticonImageView: UIImageView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var waveView: CLWaterWaveView!
    @IBOutlet weak var secondWaveView: CLWaterWaveView!
    @IBOutlet weak var emptyState: UIView!
    
    var selectedDate: String!
    var dayInt = 0
    var monthInt = 0
    var yearInt = 0
    var maxNumberOfMonth = 0
    var month: String!
    var selectedIndex = 0
    
    fileprivate var time = [String]()
    fileprivate var bpm = [Int]()
    fileprivate var stressLevel = [String]()
    fileprivate var emotion = [String]()
    fileprivate var statusContent = [String]()
    fileprivate var moodmeter = 0
    fileprivate var emojiPath: String!
    fileprivate var id = [Int]()
    fileprivate var reaction = [String]()
    private var moodMeterArr = [Int]()

    // pagination
    private var didHaveNextPage = true
    private var currentPage = 0
    private var reactionDict = [String: Any]()
    
    @IBOutlet weak var tableView: UITableView!
    
    var currentEmotion = ""
    var currentEmotionImageUrlStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // tableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = true
        
        // navigation
        self.navigationController?.navigationBar.isHidden = false
        loadMoodStream(dateSelected: selectedDate ?? "", isMoodStreamReload: false)
        self.emptyState.isHidden = true
        displayUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didConnectToUpMoodBand(_:)), name: .didConnectToUpMoodBand, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? ReactionHistoryViewController  {
            itemDetails.id = id[selectedIndex]
            itemDetails.currentEmotion = self.currentEmotion
            itemDetails.currentEmotionImageUrlStr = self.currentEmotionImageUrlStr
        }
    }
    
    @objc private func didConnectToUpMoodBand(_ notification: Notification) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    private func displayUI() {
        
//       monthLabel.isHidden = true
//       dayLabel.isHidden = true
//       nextButton.isHidden = true
//       previousButton.isHidden = true
        
        // trim selected day string
        let day = String(selectedDate!.dropFirst(8))
        let trimMonth = String(selectedDate!.dropFirst(5))
        let month = String(trimMonth.dropLast(3))
        let year = String(selectedDate!.dropLast(6))
        var displayDayOnly = Int(day) ?? 0
        
        if displayDayOnly < 10 {
            displayDayOnly = Int(day.dropFirst()) ?? 0
        }
        
        dayLabel.text = "\(displayDayOnly)"
        monthLabel.text = "\(convert(month: Int(month) ?? 0).uppercased()) \(year)"
        dayInt = Int(day) ?? 0
        monthInt = Int(month) ?? 0
        yearInt = Int(year) ?? 0
    }
    
    private func stressLevelTriangle(indicator: String, cell: MoodStreamTableViewCell) {
        if indicator == "low" {
            cell.triangleIndicator.image = UIImage(named: "ic_triangle_blue")
        } else if indicator == "Mild" {
            cell.triangleIndicator.image = UIImage(named: "ic_triangle_green")
        } else if indicator == "Moderate" {
            cell.triangleIndicator.image = UIImage(named: "ic_triangle_yellow")
        } else if indicator == "Severe" {
            cell.triangleIndicator.image = UIImage(named: "ic_triangle_red")
        } else {
            cell.triangleIndicator.image = UIImage(named: "ic_triangle_blue")
        }
    }

    private func loadMoodStream(dateSelected: String, isMoodStreamReload: Bool, year: String? = nil, month: String? = nil, day: String? = nil) {

        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let token = KeychainSwift().get("apiToken") ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        var url = URL(string: "")
        
        if isMoodStreamReload == true {
            url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record?sort=date&timezone=\(localTimeZoneName)&date=\(String(describing: year!))-\(String(describing: month!))-\(String(describing: day!))")
        } else {
            url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record?sort=date&timezone=\(localTimeZoneName)&date=\(dateSelected)")
        }
        
        print("urls moodstream: \(url!)")
        
        var request = URLRequest(url: url!)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                guard let json = userObject as? [String: Any] else { return  }
                let statusCode = json["status"] as? Int ?? 0
    
                guard let jsonData = json["data"] as? [String: Any] else { return }
                let moodMeter = jsonData["upmood_meter"] as? Int ?? 0
                let emojiFilePath = jsonData["filepath"] as? String ?? ""

                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        
                        let _ = jsonData["upmood_meter"] as? Int ?? 0
                        self.moodMeterArr.append(moodMeter)
                        
                        guard let recordData = jsonData["records"] as? [String: Any] else { return }
                        
                        // pagination
                        let currentpage1 = recordData["current_page"] as? Int ?? 0
                        self.currentPage = currentpage1
                        
                        let nextPage1 = recordData["next_page_url"] as? String ?? ""
                        if nextPage1.isEmpty {
                            self.didHaveNextPage = false
                        }
                        
                        print("these data: \(currentpage1), \(nextPage1)")
                        
                        guard let insideRecord = recordData["data"] as? [[String: Any]] else { return }
                        
                        for record in insideRecord {
                            
                            let reaction_path = record["reaction"] as? String ?? ""
                            self.reaction.append(reaction_path)

                            let id = record["id"] as? Int ?? 0
                            let time = record["time_created"] as? String ?? ""
                            let bpm = record["heartbeat_count"] as? Int ?? 0
                            let stressLevel = record["stress_level"] as? String ?? ""
                            let emotion = record["emotion_value"] as? String ?? ""
                            let statusContent = record["content"] as? String ?? ""
                            
                            self.id.append(id)
                            self.time.append(time)
                            self.bpm.append(bpm)
                            self.stressLevel.append(stressLevel.capitalized)
                            self.emotion.append(emotion.capitalized)
                            self.statusContent.append(statusContent)
                        }
                        
                        DispatchQueue.main.async {
                            self.emptyState.isHidden = true
                            self.tableView.isHidden = false
                            self.tableView.reloadData()
                        }
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
//                        self.emotionLabel.text = ""
//                        self.emoticonImageView.image = UIImage(named: "")
                        self.emptyState.isHidden = false
                    })
                } else if statusCode == 401 {
                    SVProgressHUD.dismiss(completion: {
//                        self.emotionLabel.text = ""
//                        self.emoticonImageView.image = UIImage(named: "")
                        self.emptyState.isHidden = false
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
//                        self.emotionLabel.text = ""
//                        self.emoticonImageView.image = UIImage(named: "")
                        self.emptyState.isHidden = false
                        self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error?.localizedDescription ?? "")
                    })
                }
            
        }}.resume()
    }
    
//    override func viewWillDisappear(_ animated : Bool) {
//        super.viewWillDisappear(animated)
//        if self.isMovingFromParentViewController {
//            performSegue(withIdentifier: "reloadParentView", sender: self)
//        }
//    }
    
//    @IBAction func nextDay(_ sender: UIButton) {
//        dayInt+=1
//
//        // next Button func
//        if dayInt > maxNumberOfMonth {
//            dayInt = 0
//            monthInt+=1
//            dayInt+=1
//            self.dayLabel.text = "\(dayInt)"
//            self.monthLabel.text = "\(convert(month: monthInt).uppercased()) \(yearInt)"
//            reload()
//            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: String(dayInt))
//        } else {
//            self.dayLabel.text = "\(dayInt)"
//            self.monthLabel.text = "\(convert(month: monthInt).uppercased()) \(yearInt)"
//        }
//
//        // API Call when Next Button is Clicked
//        if monthInt < 10 && dayInt < 10  {
//            let month = String(format: "%02d", monthInt)
//            let day = String(format: "%02d", dayInt)
//            reload()
//            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: month, day: day)
//        } else if monthInt < 10 {
//            let month = String(format: "%02d", monthInt)
//            reload()
//            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: month, day: String(dayInt))
//        } else if dayInt < 10 {
//            let day = String(format: "%02d", dayInt)
//            reload()
//            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: day)
//        } else {
//            reload()
//            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: String(dayInt))
//        }
//    }
//
//    @IBAction func previousDay(_ sender: UIButton) {
//        dayInt-=1
//
//        if dayInt == 0 {
//            monthInt-=1
//            self.convertMonth(maxNumber: monthInt)
//            dayInt = maxNumberOfMonth
//        }
//
//        self.dayLabel.text = "\(dayInt)"
//        self.monthLabel.text = "\(convert(month: monthInt).uppercased()) \(String(yearInt))"
//        self.loadMoodStream(dateSelected: selectedDate, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: String(dayInt))
//
//         // API Call when Next Button is Clicked
//        if monthInt < 10 && dayInt < 10  {
//            let month = String(format: "%02d", monthInt)
//            let day = String(format: "%02d", dayInt)
//            reload()
//            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: month, day: day)
//        } else if monthInt < 10 {
//            let month = String(format: "%02d", monthInt)
//            reload()
//            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: month, day: String(dayInt))
//        } else if dayInt < 10 {
//            let day = String(format: "%02d", dayInt)
//            reload()
//            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: day)
//        } else {
//            reload()
//            loadMoodStream(dateSelected: selectedDate!, isMoodStreamReload: true, year: String(yearInt), month: String(monthInt), day: String(dayInt))
//        }
//    }
    
    @objc fileprivate func showReactionHistory(_ sender: UIButton) {
        if let indexPath = self.tableView.indexPathForView(sender) {
            selectedIndex = indexPath.row
            performSegue(withIdentifier: "showReactionHistory", sender: self)
        }
    }
    
    private func setUpStressLevelLabel(indicator: String, cell: MoodStreamTableViewCell, indexPath: IndexPath) {
        if indicator == "low" {
            cell.stressLevelLabel.text = NSLocalizedString("Low", comment: "")
        } else if indicator == "Mild" {
            cell.stressLevelLabel.text = NSLocalizedString("Mild", comment: "")
        } else if indicator == "Moderate" {
            cell.stressLevelLabel.text = NSLocalizedString("Normal", comment: "")
        } else if indicator == "Severe" {
            cell.stressLevelLabel.text = NSLocalizedString("High", comment: "")
        } else {
            cell.stressLevelLabel.text = ""
        }
    }
    
    private func setUpTime(timeString: String, cell: MoodStreamTableViewCell) {
        let dateAsString = timeString
        let dateFormatter = DateFormatter()
        // temp for versioning only, change to this if beta or test - HH:mm:ss
        dateFormatter.dateFormat = "HH:mm:ss aa"

        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "h:mm a"
        let DateFinal = dateFormatter.string(from: date!)

        cell.timeLabel.text = DateFinal
    }
    
    private func setUpCurrentMood(cell: MoodStreamTableViewCell, emotion: String) {
        switch emotion {
           case "Excitement":
               cell.emotionLabel.text = NSLocalizedString("Excitement", comment: "")
           case "Happy":
               cell.emotionLabel.text = NSLocalizedString("Happy", comment: "")
           case "Zen":
               cell.emotionLabel.text = NSLocalizedString("Zen", comment: "")
           case "Pleasant":
               cell.emotionLabel.text = NSLocalizedString("Pleasant", comment: "")
           case "Calm":
               cell.emotionLabel.text = NSLocalizedString("Calm", comment: "")
           case "Unpleasant":
               cell.emotionLabel.text = NSLocalizedString("Unpleasant", comment: "")
           case "Confused":
               cell.emotionLabel.text = NSLocalizedString("Confused", comment: "")
           case "Challenged":
               cell.emotionLabel.text = NSLocalizedString("Challenged", comment: "")
           case "Tense":
               cell.emotionLabel.text = NSLocalizedString("Tense", comment: "")
           case "Sad":
               cell.emotionLabel.text = NSLocalizedString("Sad", comment: "")
           case "Anxious":
               cell.emotionLabel.text = NSLocalizedString("Anxious", comment: "")
           case "Loading":
               cell.emotionLabel.text = NSLocalizedString("Loading", comment: "")
        default:
            break
        }
    }
    
//    private func setUpStressLevel(indicator: String) {
//        if indicator == "Low" {
//            self.stressLevelLabel.text = NSLocalizedString("Low", comment: "")
//        } else if indicator == "Mild" {
//            self.stressLevelLabel.text = NSLocalizedString("Mild", comment: "")
//        } else if indicator == "Normal" {
//            self.stressLevelLabel.text = NSLocalizedString("Normal", comment: "")
//        } else if indicator == "High" {
//            self.stressLevelLabel.text = NSLocalizedString("High", comment: "")
//        } else {
//            //
//        }
//    }
}

extension MoodStreamViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MoodStreamTableViewCell", for: indexPath) as? MoodStreamTableViewCell {
            
            //setUpTime(timeString: time[indexPath.row], cell: cell)
            cell.timeLabel.text = time[indexPath.row]
            cell.heartBeatLabel.text = "\(bpm[indexPath.row])"
            setUpCurrentMood(cell: cell, emotion: emotion[indexPath.row])
            cell.statusLabel.text = statusContent[indexPath.row]
            
            self.setUpStressLevelLabel(indicator: self.stressLevel[indexPath.row], cell: cell, indexPath: indexPath)
            
            if self.reaction[indexPath.row].isEmpty {
                cell.showReactionHistoryButton.setTitleColor(.gray, for: .normal)
            } else {
                cell.reactionLabel.text = reaction[indexPath.row]
                cell.showReactionHistoryButton.setTitleColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR), for: .normal)
                cell.showReactionHistoryButton.addTarget(self, action: #selector(self.showReactionHistory(_:)), for: .touchUpInside)
            }
            
            stressLevelTriangle(indicator: stressLevel[indexPath.row], cell: cell)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return time.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            if self.didHaveNextPage {

                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))

                self.tableView.tableFooterView = spinner
                self.tableView.tableFooterView?.isHidden = false

                self.tableView.reloadData()

                currentPage+=1
                self.loadMoodStreamWithPagination(dateSelected: selectedDate ?? "", withPage: self.currentPage)
            } else {
                self.tableView.tableFooterView?.isHidden = true
            }
        }
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        print("self.loadingData: \(self.didHaveNextPage)")
//
//        let lastSectionIndex = tableView.numberOfSections - 1
//        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
//
//        if self.didHaveNextPage {
//
//            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
//                // print("this is the last cell")
//                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//                spinner.startAnimating()
//                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//
//                self.tableView.tableFooterView = spinner
//                self.tableView.tableFooterView?.isHidden = false
//
//                self.tableView.reloadData()
//
//                currentPage+=1
//                self.loadMoodStreamWithPagination(dateSelected: selectedDate ?? "", withPage: self.currentPage)
//            }
//
//        } else {
//            self.tableView.tableFooterView?.isHidden = true
//        }
//
////        if self.didHaveNextPage {
////            if indexPath.row == self.time.count - 1 {
//////                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .white)
//////                spinner.color = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
//////                spinner.startAnimating()
//////                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(22))
//////
//////                self.tableView.tableFooterView = spinner
//////                self.tableView.tableFooterView?.isHidden = false
////                self.tableView.reloadData()
////
////                currentPage+=1
////                self.loadMoodStreamWithPagination(dateSelected: selectedDate ?? "", withPage: self.currentPage)
//////                self.loadNotificationPagination(currentPage: currentPage)
////            }
////        } else {
////            self.tableView.tableFooterView?.isHidden = true
////        }
//    }
}

extension MoodStreamViewController {
    
    fileprivate func unhideUI() {
        self.monthLabel.isHidden = false
        self.dayLabel.isHidden = false
//        self.nextButton.isHidden = false
//        self.previousButton.isHidden = false
//        self.emoticonImageView.image = UIImage(named: "ic_calm_default")
//        self.emotionLabel.text = "Calm"
    }
    
    fileprivate func convertMonth(maxNumber: Int) {
        switch maxNumber {
        case 1:
            maxNumberOfMonth = 31
        case 2:
            maxNumberOfMonth = 28
        case 3:
            maxNumberOfMonth = 31
        case 4:
            maxNumberOfMonth = 30
        case 5:
            maxNumberOfMonth = 31
        case 6:
            maxNumberOfMonth = 30
        case 7:
            maxNumberOfMonth = 31
        case 8:
            maxNumberOfMonth = 31
        case 9:
            maxNumberOfMonth = 30
        case 10:
            maxNumberOfMonth = 31
        case 11:
            maxNumberOfMonth = 30
        case 12:
            maxNumberOfMonth = 31
        default:
            break
        }
    }
    
    fileprivate func convert(month: Int) -> String {
        switch month {
        case 1:
            maxNumberOfMonth = 31
            return NSLocalizedString("January", comment: "")
        case 2:
            maxNumberOfMonth = 28
            return NSLocalizedString("February", comment: "")
        case 3:
            maxNumberOfMonth = 31
            return NSLocalizedString("March", comment: "")
        case 4:
            maxNumberOfMonth = 30
            return NSLocalizedString("April", comment: "")
        case 5:
            maxNumberOfMonth = 31
            return NSLocalizedString("May", comment: "")
        case 6:
            maxNumberOfMonth = 30
            return NSLocalizedString("June", comment: "")
        case 7:
            maxNumberOfMonth = 31
            return NSLocalizedString("July", comment: "")
        case 8:
            maxNumberOfMonth = 31
            return NSLocalizedString("August", comment: "")
        case 9:
            maxNumberOfMonth = 30
            return NSLocalizedString("September", comment: "")
        case 10:
            maxNumberOfMonth = 31
            return NSLocalizedString("October", comment: "")
        case 11:
            maxNumberOfMonth = 30
            return NSLocalizedString("November", comment: "")
        case 12:
            maxNumberOfMonth = 31
            return NSLocalizedString("December", comment: "")
        default:
            break;
        }
        
        return NSLocalizedString("January", comment: "")
    }
    
    fileprivate func reload() {
        self.time.removeAll()
        self.bpm.removeAll()
        self.stressLevel.removeAll()
        self.emotion.removeAll()
        self.id.removeAll()
        self.tableView.reloadData()
    }
    
    fileprivate func displayEmoji(numberOf: Int) -> String {
        switch numberOf {
        case 0:
            return "Sad"
        case 1:
            return "Mid-Sad"
        case 2:
            return "Unpleasant"
        case 3:
            return "Mid-Unpleasant"
        case 4:
            return "Calm"
        case 5:
            return "Mid-Pleasant"
        case 6:
            return "Pleasant"
        case 7:
            return "Mid-Happy"
        case 8:
            return "Happy"
        default: return "Calm"
        }
    }
    
    // pagination
    private func loadMoodStreamWithPagination(dateSelected: String, withPage: Int) {
   
        let token = KeychainSwift().get("apiToken") ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/record?sort=date&timezone=\(localTimeZoneName)&date=\(dateSelected)&page=\(withPage)")
        
        var request = URLRequest(url: url!)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                guard let json = userObject as? [String: Any] else { return  }
                let statusCode = json["status"] as? Int ?? 0
                
                guard let jsonData = json["data"] as? [String: Any] else { return }
                let moodMeter = jsonData["upmood_meter"] as? Int ?? 0
                let emojiFilePath = jsonData["filepath"] as? String ?? ""
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
       
                        let _ = jsonData["upmood_meter"] as? Int ?? 0
                        self.moodMeterArr.append(moodMeter)
                        
                        guard let recordData = jsonData["records"] as? [String: Any] else { return }
                        
                        // pagination
                        let currentpage1 = recordData["current_page"] as? Int ?? 0
                        self.currentPage = currentpage1
                        
                        let nextPage1 = recordData["next_page_url"] as? String ?? ""
                        if nextPage1.isEmpty {
                            self.didHaveNextPage = false
                        }

                        guard let insideRecord = recordData["data"] as? [[String: Any]] else { return }
                        
                        for record in insideRecord {
                            
                            let reaction_path = record["reaction"] as? String ?? ""
                            self.reaction.append(reaction_path)
                            
                            let id = record["id"] as? Int ?? 0
                            let time = record["time_created"] as? String ?? ""
                            let bpm = record["heartbeat_count"] as? Int ?? 0
                            let stressLevel = record["stress_level"] as? String ?? ""
                            let emotion = record["emotion_value"] as? String ?? ""
                            let statusContent = record["content"] as? String ?? ""
                            
                            self.id.append(id)
                            self.time.append(time)
                            self.bpm.append(bpm)
                            self.stressLevel.append(stressLevel.capitalized)
                            self.emotion.append(emotion.capitalized)
                            self.statusContent.append(statusContent)
                        }
                        
                        DispatchQueue.main.async {
                            self.emptyState.isHidden = true
                            // display mood and emoji
//                            if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(emojiFilePath.replacingOccurrences(of: " ", with: "%20"))") {
//                                self.emoticonImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_fourth_sticker"), options: [], completed: nil)
//                            }
                            
                            self.tableView.isHidden = false
//                            self.emotionLabel.text = self.displayEmoji(numberOf: moodMeter)
                            self.tableView.reloadData()
                        }
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
//                        self.emotionLabel.text = ""
//                        self.emoticonImageView.image = UIImage(named: "")
                        self.emptyState.isHidden = false
                    })
                } else if statusCode == 401 {
                    SVProgressHUD.dismiss(completion: {
//                        self.emotionLabel.text = ""
//                        self.emoticonImageView.image = UIImage(named: "")
                        self.emptyState.isHidden = false
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
//                        self.emotionLabel.text = ""
//                        self.emoticonImageView.image = UIImage(named: "")
                        self.emptyState.isHidden = false
                        self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error?.localizedDescription ?? "")
                    })
                }
                
        }}.resume()
    }
}
