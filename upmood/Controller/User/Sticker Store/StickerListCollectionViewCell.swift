//
//  StickerListCollectionViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 26/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class StickerListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var stickerImageView: UIImageView!
    @IBOutlet weak var stickerNameLabel: UILabel!
}
