//
//  StickerDetailsViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 26/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SDWebImage
import SVProgressHUD
import UIKit

extension StickerDetailsViewController {
    private func loadSelectedSetOf(sticker: String) {
        spinnerOnButton.startAnimating()
        let token = KeychainSwift().get("apiToken") ?? ""
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/resources/all?set=\(sticker.replacingOccurrences(of: " ", with: "%20"))") else { return }
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                guard let JSON = userObject as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let jsonData = JSON["data"] as? [[String: Any]] else { return }
                
                if statusCode == 200 {
                    for json in jsonData {
                        guard let emojiSet = json["emoji"] as? [[[String: Any]]] else { return }
                        for emoji in emojiSet {
                            for emojiMenu in emoji {
                                let isOwned = emojiMenu["owned"] as? Int ?? 0
                                let description = emojiMenu["description"] as? String ?? ""
                                let imageFilePath = emojiMenu["filepath"] as? String ?? ""
                                self.isOwned.append(isOwned)
                                self.imageDescription.append(description)
                                self.imageFilePath.append(imageFilePath)
                            }
                        }
                    } // end of fucking loop
                    
                    DispatchQueue.main.async { [weak this = self] in
                        this?.displayDownloadButton(isOwned: self.isOwned[0])
                        this?.spinnerOnButton.stopAnimating()
                        this?.imageDescriptionLabel.text = self.imageDescription[0]
                    }
                    
                } else if statusCode == 204 {
                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                } else {
                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                }
                
        }}.resume()
    }
    
    private func displayDownloadButton(isOwned: Int) {
        
        self.setThemeOrDownload.titleLabel?.adjustsFontSizeToFitWidth = true
        
        // Display Image
        if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(self.imageFilePath[0].replacingOccurrences(of: " ", with: "%20"))") {
            print("url ng image: \(url)")
            self.emojiImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_account_created"), options: SDWebImageOptions.continueInBackground, completed: nil)
        }
        
        let currentThemeSelected = UserDefaults.standard.string(forKey: "currentTheme") ?? ""
        if currentThemeSelected == self.setName!.lowercased() {
            DispatchQueue.main.async { [weak this = self] in
                this?.setThemeOrDownload.isHidden = false
                this?.setThemeOrDownload.isEnabled = false
                this?.setThemeOrDownload.backgroundColor = UIColor.lightGray
                this?.setThemeOrDownload.setTitle("Selected", for: .normal)
                this?.navigationItem.rightBarButtonItem = nil
                print("old vs new: \(currentThemeSelected), \(self.setName!.lowercased())")
            }
        } else {
            if isOwned == 1 {
                DispatchQueue.main.async { [weak this = self] in
                    this?.navigationItem.rightBarButtonItem = self.moreOptionButton
                    this?.setThemeOrDownload.isHidden = false
                    this?.spinnerOnButton.stopAnimating()
                    this?.setThemeOrDownload.setTitle(NSLocalizedString("Set as theme", comment: ""), for: .normal)
                    this?.isPurchasedLabel.text = NSLocalizedString("Purchased", comment: "")
                    this?.isPurchasedLabel.textColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                }
            } else if isOwned == 0 {
                DispatchQueue.main.async { [weak this = self] in
                    this?.navigationItem.rightBarButtonItem = nil
                    this?.setThemeOrDownload.isHidden = false
                    this?.spinnerOnButton.stopAnimating()
                    this?.setThemeOrDownload.setTitle(NSLocalizedString("Buy", comment: ""), for: .normal)
                    this?.isPurchasedLabel.text = NSLocalizedString("Free", comment: "")
                    this?.isPurchasedLabel.textColor = UIColor.red
                }
            }
        }
    }
    
    private func donwload(sticker: String) {
        spinnerOnButton.startAnimating()
        setThemeOrDownload.isHidden = true
        Alamofire.request(APIClient.download(sticker: sticker)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let jsonData = JSON["data"] as? [String: Any] else { return }
                
                if statusCode == 200 {
                    DispatchQueue.main.async { [weak this = self] in
                        print("donwload: \(JSON)")
                        this?.reload()
                        this?.spinnerOnButton.stopAnimating()
                        
                        let paid_emoji_set = jsonData["paid_emoji_set"] as? String ?? ""
                        UserDefaults.standard.set(paid_emoji_set, forKey: "paid_emoji_set")
                    }
                    
                } else if statusCode == 204 {
                    this?.setThemeOrDownload.isHidden = false
                    this?.spinnerOnButton.stopAnimating()
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                } else {
                    this?.setThemeOrDownload.isHidden = false
                    this?.spinnerOnButton.stopAnimating()
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                }
                
                break
            case .failure(let error):
                this?.setThemeOrDownload.isHidden = false
                this?.spinnerOnButton.stopAnimating()
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    private func setTheme(sticker: String) {
        spinnerOnButton.startAnimating()
        setThemeOrDownload.isHidden = true
        Alamofire.request(APIClient.setTheme(sticker: sticker)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let jsonData = JSON["data"] as? [String: Any] else { return }
                
                if statusCode == 200 {
                    DispatchQueue.main.async { [weak this = self] in
                        UserDefaults.standard.set(sticker.lowercased(), forKey: "currentTheme")
                        this?.reload()
                        this?.spinnerOnButton.stopAnimating()
                    }
                    
                } else if statusCode == 204 {
                    this?.setThemeOrDownload.isHidden = false
                    this?.spinnerOnButton.stopAnimating()
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                } else {
                    this?.setThemeOrDownload.isHidden = false
                    this?.spinnerOnButton.stopAnimating()
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                }
                
                break
            case .failure(let error):
                this?.setThemeOrDownload.isHidden = false
                this?.spinnerOnButton.stopAnimating()
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    private func deleteSticker(withSet: String) {

        let paid_emoji = UserDefaults.standard.string(forKey: "paid_emoji_set") ?? ""
        
        spinnerOnButton.startAnimating()
        setThemeOrDownload.isHidden = true
        Alamofire.request(APIClient.deleteSticker(withSet: withSet)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let _ = JSON["data"] as? [String: Any] else { return }
                
                if statusCode == 200 {
                    DispatchQueue.main.async { [weak this = self] in
                        print("nagdelete kaba: \(JSON)")
                        this?.reload()
                        this?.spinnerOnButton.stopAnimating()
                        
                        // remove selected emoji in local array
                        let paid_emoji_inside = UserDefaults.standard.string(forKey: "paid_emoji_set") ?? ""
                        var paidEmoji = paid_emoji_inside.components(separatedBy: ",")
                        paidEmoji.removeAll { $0 == withSet }
               
                        UserDefaults.standard.set(paidEmoji.joined(separator: ","), forKey: "paid_emoji_set")
                        UserDefaults.standard.synchronize()
                    }
                    
                } else if statusCode == 204 {
                    this?.setThemeOrDownload.isHidden = false
                    this?.spinnerOnButton.stopAnimating()
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                } else {
                    this?.setThemeOrDownload.isHidden = false
                    this?.spinnerOnButton.stopAnimating()
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                }
                
                break
            case .failure(let error):
                this?.setThemeOrDownload.isHidden = false
                this?.spinnerOnButton.stopAnimating()
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    private func reload() {
        self.imageFilePath.removeAll()
        self.isOwned.removeAll()
        self.viewDidLoad()
    }
}

class StickerDetailsViewController: UIViewController {
    
    @IBOutlet weak var isPurchasedLabel: UILabel!
    @IBOutlet weak var imageDescriptionLabel: UILabel!
    @IBOutlet var moreOptionButton: UIBarButtonItem!
    @IBOutlet weak var spinnerOnButton: UIActivityIndicatorView!
    @IBOutlet weak var emojiImageView: UIImageView!
    @IBOutlet weak var themeNameLabel: UILabel!
    @IBOutlet weak var setThemeOrDownload: UIButton!
    
    // helper preset
    var setName: String!
    var filePath: String!
    let viewControllerIdentifiers = ["EmojiList", "StickerList"]
    private var isOwned = [Int]()
    private var imageFilePath = [String]()
    private var imageDescription = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        displayPrimaryUI()
        setUpSegmentControl()
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setThemeOrDownload.isHidden = true
    }

    private func displayPrimaryUI() {
        themeNameLabel.text = setName
        
        if self.setName.isEmpty {
            emojiImageView.image = UIImage(named: "ic_account_created")
        }
        
        print("this is setName selected: \(self.setName)")
        
        loadSelectedSetOf(sticker: setName)
        
        UserDefaults.standard.set(setName, forKey: "StickerSetName")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UserDefaults.standard.removeObject(forKey: "StickerSetName")
    }
    
    private func setUpSegmentControl() {
        let newController = storyboard!.instantiateViewController(withIdentifier: viewControllerIdentifiers[0])
        let oldController = childViewControllers.last!
        
        oldController.willMove(toParentViewController: nil)
        addChildViewController(newController)
        newController.view.frame = oldController.view.frame
        
        transition(from: oldController, to: newController, duration: 0.25, options: .transitionCrossDissolve, animations: {
            // nothing needed here
        }, completion: { _ -> Void in
            oldController.removeFromParentViewController()
            newController.didMove(toParentViewController: self)
        })
    }

    @IBAction func selectSticker(_ sender: UISegmentedControl) {
        let newController = storyboard!.instantiateViewController(withIdentifier: viewControllerIdentifiers[sender.selectedSegmentIndex])
        let oldController = childViewControllers.last!
        
        oldController.willMove(toParentViewController: nil)
        addChildViewController(newController)
        newController.view.frame = oldController.view.frame
        
        transition(from: oldController, to: newController, duration: 0.25, options: .transitionCrossDissolve, animations: {
            // nothing needed here
        }, completion: { _ -> Void in
            oldController.removeFromParentViewController()
            newController.didMove(toParentViewController: self)
        })
    }
    
    @IBAction func setThemeAndDownload(_ sender: UIButton) {
        if isOwned[0] == 0 {
            self.donwload(sticker: self.setName)
        } else if isOwned[0] == 1 {
            self.setTheme(sticker: self.setName)
        }
    }
    
    @IBAction func moreOptions(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Delete Sticker Set", style: .default , handler:{ [weak this = self] alert in
            this?.deleteSticker(withSet: self.setName)
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler:{ alert in
            
        }))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
}
