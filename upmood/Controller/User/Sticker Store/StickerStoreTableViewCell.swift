//
//  StickerStoreTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 26/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class StickerStoreTableViewCell: UITableViewCell {

    @IBOutlet weak var selectedThemeLabel: UILabel!
    @IBOutlet weak var setSetNameLabel: UILabel!
    @IBOutlet weak var themeImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
