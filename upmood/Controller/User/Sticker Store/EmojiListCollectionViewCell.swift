//
//  EmojiListCollectionViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 26/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class EmojiListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var emojiImageView: UIImageView!
    @IBOutlet weak var emojiValueLabel: UILabel!
}
