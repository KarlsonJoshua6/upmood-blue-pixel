//
//  EmojiListViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 26/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SDWebImage
import SVProgressHUD
import UIKit

extension EmojiListViewController {
    private func loadSelectedSetOf(sticker: String) {
        spinner.startAnimating()
        let token = KeychainSwift().get("apiToken") ?? ""
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/resources/all?set=\(sticker.replacingOccurrences(of: " ", with: "%20"))") else { return }
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                guard let JSON = userObject as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let jsonData = JSON["data"] as? [[String: Any]] else { return }
                
                if statusCode == 200 {
                    for json in jsonData {
                        guard let emojiSet = json["emoji"] as? [[[String: Any]]] else { return }
                        for emoji in emojiSet {
                            for emojiMenu in emoji {
                                let emotion = emojiMenu["emotion"] as? String ?? ""
                                let imageFilePath = emojiMenu["filepath"] as? String ?? ""
                                self.emojiName.append(emotion.capitalized)
                                self.emojiImage.append(imageFilePath.replacingOccurrences(of: ".svg", with: ".png"))
                            }
                        }
                    } // end of fucking loop
                    
                    DispatchQueue.main.async {
                        self.spinner.stopAnimating()
                        self.collectionView.reloadData()
                    }
                    
                } else if statusCode == 204 {
                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                } else {
                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                }
                
        }}.resume()
    }
    
    private func setUp(cell: EmojiListCollectionViewCell, indexpath: IndexPath) {
        if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(self.emojiImage[indexpath.row].replacingOccurrences(of: " ", with: "%20"))") {
            
            print("emojis' url: \(url)")
            
            self.setUpCurrentMood(emotion: emojiName[indexpath.row], cell: cell, indexpath: indexpath)
            cell.emojiImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_account_created"), options: [], completed: nil)
        }
    }
    
    private func setUpCurrentMood(emotion: String, cell: EmojiListCollectionViewCell, indexpath: IndexPath) {
        switch emotion {
            case "Excitement":
               cell.emojiValueLabel.text = NSLocalizedString("Excitement", comment: "")
           case "Happy":
               cell.emojiValueLabel.text = NSLocalizedString("Happy", comment: "")
           case "Zen":
               cell.emojiValueLabel.text = NSLocalizedString("Zen", comment: "")
           case "Pleasant":
               cell.emojiValueLabel.text = NSLocalizedString("Pleasant", comment: "")
           case "Calm":
               cell.emojiValueLabel.text = NSLocalizedString("Calm", comment: "")
           case "Unpleasant":
               cell.emojiValueLabel.text = NSLocalizedString("Unpleasant", comment: "")
           case "Confused":
               cell.emojiValueLabel.text = NSLocalizedString("Confused", comment: "")
           case "Challenged":
               cell.emojiValueLabel.text = NSLocalizedString("Challenged", comment: "")
           case "Tense":
               cell.emojiValueLabel.text = NSLocalizedString("Tense", comment: "")
           case "Sad":
               cell.emojiValueLabel.text = NSLocalizedString("Sad", comment: "")
           case "Anxious":
               cell.emojiValueLabel.text = NSLocalizedString("Anxious", comment: "")
           case "Loading":
               cell.emojiValueLabel.text = NSLocalizedString("Loading", comment: "")
        default:
            break
        }
    }
}

class EmojiListViewController: UIViewController {

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var emojiImage = [String]()
    private var emojiName = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        let set = UserDefaults.standard.string(forKey: "StickerSetName") ?? ""
        loadSelectedSetOf(sticker: set)
    }
}

extension EmojiListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmojiListCollectionViewCell", for: indexPath) as? EmojiListCollectionViewCell {
            setUp(cell: cell, indexpath: indexPath)
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return emojiName.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}
