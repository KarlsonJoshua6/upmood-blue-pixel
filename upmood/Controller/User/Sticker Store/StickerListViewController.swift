//
//  StickerListViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 26/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SDWebImage
import SVProgressHUD
import UIKit

extension StickerListViewController {
    private func loadSelectedSetOf(sticker: String) {
        spinner.startAnimating()
        let token = KeychainSwift().get("apiToken") ?? ""
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/resources/all?set=\(sticker.replacingOccurrences(of: " ", with: "%20"))") else { return }
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                guard let JSON = userObject as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let jsonData = JSON["data"] as? [[String: Any]] else { return }
                
                if statusCode == 200 {
                    
                    for json in jsonData {
                        if let emojiSet = json["sticker"] as? [[[String: Any]]] {
                            for emoji in emojiSet {
                                for emojiMenu in emoji {
                                    let emotion = emojiMenu["emotion"] as? String ?? ""
                                    let imageFilePath = emojiMenu["filepath"] as? String ?? ""
                                    self.stickerName.append(emotion.capitalized)
                                    self.stickerImage.append(imageFilePath.replacingOccurrences(of: ".svg", with: ".png"))
                                }
                            }
                        } else {
                            self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No stickers found")
                        }
                    } // end of fucking loop
                    
                    DispatchQueue.main.async {
                        self.spinner.stopAnimating()
                        self.collectionView.reloadData()
                    }
                    
                } else if statusCode == 204 {
                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                } else {
                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                }
                
        }}.resume()
    }
    
    private func setUp(cell: StickerListCollectionViewCell, indexpath: IndexPath) {
        if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(self.stickerImage[indexpath.row].replacingOccurrences(of: " ", with: "%20"))") {
            cell.stickerNameLabel.text = stickerName[indexpath.row]
            cell.stickerImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_account_created"), options: [], completed: nil)
            print("url to ng sticker: \(url)")
        }
    }
    
    private func setUpCurrentMood(emotion: String, cell: StickerListCollectionViewCell, indexpath: IndexPath) {
        switch emotion {
            case "Excitement":
               cell.stickerNameLabel.text = NSLocalizedString("Excitement", comment: "")
           case "Happy":
               cell.stickerNameLabel.text = NSLocalizedString("Happy", comment: "")
           case "Zen":
               cell.stickerNameLabel.text = NSLocalizedString("Zen", comment: "")
           case "Pleasant":
               cell.stickerNameLabel.text = NSLocalizedString("Pleasant", comment: "")
           case "Calm":
               cell.stickerNameLabel.text = NSLocalizedString("Calm", comment: "")
           case "Unpleasant":
               cell.stickerNameLabel.text = NSLocalizedString("Unpleasant", comment: "")
           case "Confused":
               cell.stickerNameLabel.text = NSLocalizedString("Confused", comment: "")
           case "Challenged":
               cell.stickerNameLabel.text = NSLocalizedString("Challenged", comment: "")
           case "Tense":
               cell.stickerNameLabel.text = NSLocalizedString("Tense", comment: "")
           case "Sad":
               cell.stickerNameLabel.text = NSLocalizedString("Sad", comment: "")
           case "Anxious":
               cell.stickerNameLabel.text = NSLocalizedString("Anxious", comment: "")
           case "Loading":
               cell.stickerNameLabel.text = NSLocalizedString("Loading", comment: "")
        default:
            break
        }
    }
}

class StickerListViewController: UIViewController {

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var stickerImage = [String]()
    private var stickerName = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        let set = UserDefaults.standard.string(forKey: "StickerSetName") ?? ""
        loadSelectedSetOf(sticker: set)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension StickerListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerListCollectionViewCell", for: indexPath) as? StickerListCollectionViewCell {
            setUp(cell: cell, indexpath: indexPath)
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stickerName.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

