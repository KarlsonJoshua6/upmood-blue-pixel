//
//  StickerStoreViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 26/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//
import Alamofire
import KeychainSwift
import SDWebImage
import SVProgressHUD
import UIKit

class StickerStoreViewController: UIViewController {
    
    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var setName = [String]()
    private var imageFilePath = [String]()
    private var isOwned = [Int]()
    private var uniqueSetname = [String]()
    private var selectedSetName = ""
    
    // helper
    private var selectedIndex = 0
    private var timer = Timer()
    private var searchText = ""
    private var newEmoji = [String]()
    private var newEmojiArr = [[[String: Any]]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.placeholder = NSLocalizedString("Search Sticker", comment: "")
        searchBar.searchBarStyle = .prominent
        searchBar.tintColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        
        emptyStateView.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        loadStickerMenu()
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        
        self.searchBar.setShowsCancelButton(false, animated: true)
        self.tableView.reloadData()
        self.reload()
        self.loadStickerMenu()
        self.searchBar.endEditing(true)
        self.searchBar.text = ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? StickerDetailsViewController  {
            itemDetails.setName = self.selectedSetName
        }
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        let didClickBuy = UserDefaults.standard.bool(forKey: "didClickBuySticker")
        if !didClickBuy {
            self.dismiss(animated: true, completion: nil)
            UserDefaults.standard.set(false, forKey: "didClickBuySticker")
        } else {
            performSegue(withIdentifier: "reloadStickersOnSendReaction", sender: self)
            UserDefaults.standard.set(false, forKey: "didClickBuySticker")
        }
    }
    
    private func setUpThemeImage(item: String, cell: StickerStoreTableViewCell) {
        if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(item.replacingOccurrences(of: "ic_account_created", with: "%20"))") {
            cell.themeImageView.sd_setImage(with: url, placeholderImage: UIImage(named: ""), options: SDWebImageOptions.continueInBackground, completed: nil)
        }
    }
    
    private func initSetThemeLabel(cell: StickerStoreTableViewCell, indexPath: IndexPath, uniqueSetName: [String]) {
        let currentThemeSelected = UserDefaults.standard.string(forKey: "currentTheme") ?? "Regular"
        if currentThemeSelected.lowercased() == uniqueSetName[indexPath.row].lowercased() {
            cell.selectedThemeLabel.isHidden = false
        } else {
            cell.selectedThemeLabel.isHidden = true
        }
    }
    
    private func setUp(cell: StickerStoreTableViewCell, indexPath: IndexPath) {
        
        let urlString = "\(Constants.Routes.BASE_URL_RESOURCE)\(setName[indexPath.row].capitalized)/emoji/\(imageFilePath[indexPath.row])".replacingOccurrences(of: " ", with: "%20")
    
        self.initSetThemeLabel(cell: cell, indexPath: indexPath, uniqueSetName: self.setName)
        cell.setSetNameLabel.text = self.setName[indexPath.row]
        
        cell.themeImageView.sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "ic_account_created"), options: SDWebImageOptions.continueInBackground, completed: nil)
//
//        if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(setName[indexPath.row].capitalized)/emoji/\(imageFilePath[indexPath.row])") {
//            cell.themeImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_account_created"), options: SDWebImageOptions.continueInBackground, completed: nil)
//
//            print("emotion set: \(url)")
//        }
    }
    
    func setUpThemeImageDisplay(cell: StickerStoreTableViewCell, emotionSet: String) {
        cell.themeImageView.image = UIImage(named: "Emoji/\(emotionSet.lowercased())/calm")
    }
}

extension StickerStoreViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "StickerStoreTableViewCell", for: indexPath) as? StickerStoreTableViewCell {
            self.setUp(cell: cell, indexPath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let unique = Array(Set(self.setName))
        return unique.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        self.selectedSetName = self.setName[indexPath.row]
        performSegue(withIdentifier: "showStickerDetails", sender: self)
    }
}

// MARK: - UISearchBarDelegate

extension StickerStoreViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.tableView.reloadData()
        self.reload()
        
        guard let firstSubview = searchBar.subviews.first else { return }
        firstSubview.subviews.forEach { ($0 as? UITextField)?.clearButtonMode = .never }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.searchBar.endEditing(true)
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.emptyStateView.isHidden = true
        self.tableView.reloadData()
        self.loadStickerMenu()
        self.searchBar.endEditing(true)
        self.searchBar.text = ""
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == self.searchBar {
            if searchBar.returnKeyType == .search {
                self.tableView.reloadData()
                self.reload()
                searchBar.setShowsCancelButton(false, animated: true)
                searchBar.resignFirstResponder()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        self.tableView.reloadData()
        self.reload()
        
        let typeCasteToStringFirst = searchBar.text as NSString?
        let newString = typeCasteToStringFirst?.replacingCharacters(in: range, with: text)
        let finalSearchString = newString ?? ""
        self.searchText = finalSearchString.lowercased()
        
        timer = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { [weak self] _ in
            self?.runDelay()
        }
        
        return true
    }
    
    private func reload() {
        self.setName.removeAll()
        self.isOwned.removeAll()
        self.imageFilePath.removeAll()
        self.selectedIndex = 0
        self.tableView.reloadData()
    }
    
    @objc private func runDelay() {
        self.searchSticker(withKeyword: self.searchText)
        self.timer.invalidate()
    }
}

extension StickerStoreViewController {
    
    private func searchSticker(withKeyword: String) {
        self.reload()
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        
        let token = KeychainSwift().get("apiToken") ?? ""
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/resources/setname-list?set_name=\(withKeyword.lowercased())") else { return }
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                guard let JSON = userObject as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                guard let jsonData = JSON["data"] as? [[String: Any]] else {
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.searchBar.endEditing(true)
                        self.emptyStateView.isHidden = false
                    }
                    
                    return
                }
                
                if statusCode == 200 {
                    
                    SVProgressHUD.dismiss(completion: { [weak self] in
                        
                        self?.reload()
                        
                        for json in jsonData {
                            let set_name = json["set_name"] as? String ?? ""
                            let filename = json["filename"] as? String ?? ""
                            self?.setName.append(set_name)
                            self?.imageFilePath.append(filename)
                        }
                        
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                        }
                    })
                    
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        self.emptyStateView.isHidden = false
                    })
                    
                } else {
                    SVProgressHUD.dismiss(completion: {
                        self.emptyStateView.isHidden = false
                    })
                }
                
            } else {
                print("\(String(describing: error?.localizedDescription))")
                SVProgressHUD.dismiss(completion: {
                    self.emptyStateView.isHidden = false
                })
            }
            
        }.resume()
    }
    
    private func loadStickerMenu() {
        self.reload()
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.stickerList()).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let jsonData = JSON["data"] as? [[String: Any]] else { return }
            
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: { [weak self] in

                        self?.reload()
                        
                        for json in jsonData {
                            let set_name = json["set_name"] as? String ?? ""
                            let filename = json["filename"] as? String ?? ""
                            self?.setName.append(set_name)
                            self?.imageFilePath.append(filename)
                        }
                        
                        DispatchQueue.main.async {
                            this?.tableView.reloadData()
                        }
                    })

                } else if statusCode == 204 {
                    this?.emptyStateView.isHidden = false
                } else {
                    this?.emptyStateView.isHidden = false
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
}
