//
//  CBPeripheralDelegateExtension.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 22/07/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import CoreBluetooth
import Foundation
import SVProgressHUD
import Pulsator
import UserNotifications
import UIKit

// MARK: - DeviceTableViewCell

class DeviceTableViewCell: UITableViewCell {
    @IBOutlet weak var DeviceImage: UIImageView!
    @IBOutlet weak var DeviceName: UILabel!
    @IBOutlet weak var DeviceDescription: UILabel!
}

// MARK: - DeviceScreenViewController

class DeviceScreenViewController: UIViewController {
    
    @IBOutlet weak var remainingLabel: UILabel!
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var bluetoothLogo: UIImageView!
    @IBOutlet weak var scanDevice: UIButton!
    @IBOutlet weak var pulseViewHolder: UIView!
    @IBOutlet weak var deviceList: UITableView!
    @IBOutlet weak var findDevice: UILabel!
    @IBOutlet weak var didConnectBandView: UIView!
    @IBOutlet weak var wave: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var turnOffSensorButton: RoundButton!
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var turnOnSensorButton: UIButton!
    @IBOutlet weak var bandNameLabel: UILabel!
    
    var backgroundUUID: UUID!
    let cellIdentifier = "CellIdentifier"
    let charRead = "0000FFF1-1000-1000-8000-00805F9B34FB"
    let charNotif = "0000FFF3-1000-1000-8000-00805F9B34FB"
    let charWrite = "0000FFF2-1000-1000-8000-00805F9B34FB"
    let pulsator = Pulsator()
    //let dataProcessor = HRMDataProcessor()

    var centralManager: CBCentralManager!
    var heartRatePeripheral: CBPeripheral!
    
    var initWave = SPWaterProgressIndicatorView.init(frame: .zero)
    var watches : Array<String> = Array<String>()
    var watchesPeriperhalCode : [String : CBPeripheral] = [:]
    var selectedBand = ""
    
    var currentCell: DeviceTableViewCell!
    var percent_batt = 0
    var timer: Timer!
    var isReconnecting = false
    
    var writeChannel: CBCharacteristic!
    var flag = false
    
    // MARK: - Overide
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initObserver()
        deviceList.backgroundColor = UIColor.clear
        centralManager = CBCentralManager(delegate: self, queue: nil)
        deviceList.delegate = self
        deviceList.dataSource = self
        didConnectBandView.isHidden = true
        percentLabel.isHidden = true
        remainingLabel.isHidden = true
        bandNameLabel.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupPulsator()
        NotificationCenter.default.addObserver(self, selector: #selector(DeviceScreenViewController.timeChangedNotification), name: NSNotification.Name.NSSystemTimeZoneDidChange, object: nil)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NSSystemTimeZoneDidChange, object: nil)
    }
    
    // MARK: - IBActions
    
    @IBAction func didTappedSensor(_ sender: UIButton) {
        if sender.titleLabel?.text == NSLocalizedString("Enable Power Saving", comment: "") {
            self.turnOnSensorButton.setTitle(NSLocalizedString("Disable Power Saving", comment: ""), for: .normal)
            self.turnOnSensorButton.backgroundColor = UIColor.red
            self.writeCharacteristic(commandType: 1)
        } else {
            self.turnOnSensorButton.setTitle(NSLocalizedString("Enable Power Saving", comment: ""), for: .normal)
            self.turnOnSensorButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            self.writeCharacteristic(commandType: 0)
        }
    }
    
    @IBAction func disconnectBand(_ sender: UIButton) {
        if let fetchAutoReconnect = UserDefaults.standard.object(forKey: "fetchAutoReconnect") {
            let autoReconnect = fetchAutoReconnect as? Int ?? 0
            if autoReconnect == 1 {
                presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString("Please turn off the auto reconnect before disconnecting.", comment: ""))
            } else {
                self.isReconnecting = false
                self.bandDisconnected(peripheral: heartRatePeripheral)
                centralManager.cancelPeripheralConnection(heartRatePeripheral)
            }
        }
    }
    
    @IBAction func onDeviceScan(_ sender: Any) {
        if(centralManager.isScanning) {
            scanDevice.setTitle(NSLocalizedString("SCAN", comment: ""), for: .normal)
            findDevice.text = NSLocalizedString("Scan to find Devices", comment: "")
            centralManager.stopScan()
            pulsator.isHidden = true
            pulsator.stop()
            deviceList.isHidden = false
        } else {
            scanDevice.setTitle(NSLocalizedString("STOP", comment: ""), for: .normal)
            centralManager.scanForPeripherals(withServices: nil)
            deviceList.isHidden = false
            pulsator.isHidden = false
            pulsator.start()
            watches.removeAll()
            deviceList.reloadData()
        }
    }
    
    @objc func timeChangedNotification(notification: NSNotification) {
     self.navigationController?.popToRootViewController(animated: false)
     let delegate = UIApplication.shared.delegate as? AppDelegate
     let storyboardName = "DashboardScreen"
     let storybaord = UIStoryboard(name: storyboardName, bundle: nil)
     delegate?.window?.rootViewController = storybaord.instantiateInitialViewController()
    }
}

// MARK: - Handling TableView And its Delegate

extension DeviceScreenViewController: UITableViewDataSource, UITableViewDelegate {
    
    func setUp(cell: DeviceTableViewCell, indexPath: IndexPath) {
        self.currentCell = cell
        
        let filtered = self.watches.filter { $0.contains("UpMood") }
        let watch = filtered[indexPath.row].replacingOccurrences(of: "UpMood", with: "Upmood")
        
        cell.DeviceName.text = watch
        cell.DeviceDescription.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let filtered = self.watches.filter { $0.contains("UpMood") }
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DeviceTableViewCell {
            self.setUp(cell: cell, indexPath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let filtered = self.watches.filter { $0.contains("UpMood") }
        centralManager.stopScan()
        
        if watchesPeriperhalCode[filtered[indexPath.row]] != nil {
            self.selectedBand = filtered[indexPath.row].replacingOccurrences(of: "UpMood", with: "Upmood")
            heartRatePeripheral = watchesPeriperhalCode[filtered[indexPath.row]]!
            heartRatePeripheral.delegate = self
            centralManager.connect(heartRatePeripheral)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
}
