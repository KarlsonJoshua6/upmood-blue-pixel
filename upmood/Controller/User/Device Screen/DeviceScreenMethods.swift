
//
//  File.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 28/01/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import CoreBluetooth
import Foundation
import SVProgressHUD
import Pulsator
import UserNotifications
import UIKit

extension DeviceScreenViewController {
    
    // MARK: - Handling UI
    
    func timeRemain(withPercent: Int) -> String {
        switch withPercent {
        case 100:
            return "\(NSLocalizedString("Remaining", comment: "")): 12 \(NSLocalizedString("Hours Left", comment: ""))"
        case 75:
            return "\(NSLocalizedString("Remaining", comment: "")): 9 \(NSLocalizedString("Hours Left", comment: ""))"
        case 50:
            return "\(NSLocalizedString("Remaining", comment: "")): 6 \(NSLocalizedString("Hours Left", comment: ""))"
        case 25:
            return "\(NSLocalizedString("Remaining", comment: "")): 3 \(NSLocalizedString("Hours Left", comment: ""))"
        case 5:
            return "\(NSLocalizedString("Remaining", comment: "")): 36 Minutes Left"
        default:
            return "\(NSLocalizedString("Remaining", comment: "")): Unknown"
        }
    }
    
    @objc func showBatteryPercent() {
        DispatchQueue.main.async {
            self.updateWave(withPercent: self.percent_batt)
            self.updateTimeRemaining(withPercent: self.percent_batt)
        }
    }
    
    func updateTimeRemaining(withPercent: Int) {
        remainingLabel.isHidden = false
        bandNameLabel.isHidden = false
        remainingLabel.text = timeRemain(withPercent: withPercent)
        bandNameLabel.text = selectedBand
    }
    
    func updateWave(withPercent: Int) {
        self.containerView.isHidden = false
        self.percentLabel.isHidden = false
        self.remainingLabel.isHidden = false
        self.turnOffSensorButton.isHidden = false
        self.disconnectButton.isHidden = false
        self.bandNameLabel.isHidden = false
        
        self.initWave = SPWaterProgressIndicatorView(frame: self.containerView.bounds)
        self.percentLabel.text = "\(withPercent)%"
        self.initWave.completionInPercent = withPercent
        self.initWave.waveColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        self.containerView.addSubview(self.initWave)
    }
    
    func deInitWave() {
        self.containerView.isHidden = true
        self.remainingLabel.isHidden = true
        self.percentLabel.isHidden = true
        self.turnOffSensorButton.isHidden = true
        self.disconnectButton.isHidden = true
        self.bandNameLabel.isHidden = true
        self.initWave.removeFromSuperview()
    }
    
    func setupPulsator() {
        pulsator.radius = 140.0
        pulsator.numPulse = 5
        pulsator.animationDuration = 5
        pulsator.backgroundColor = UIColor.white.cgColor
        pulsator.position = CGPoint(x: pulseViewHolder.fs_height/2, y: pulseViewHolder.fs_width/2)
        pulseViewHolder.layer.addSublayer(pulsator)
    }
    
    // MARK: - Handling Band Connect/Disconnect
    
    func bandDisconnected(peripheral: CBPeripheral) {
        print("Disconnected to \(peripheral)")
        self.watches.removeAll()
        findDevice.text = NSLocalizedString("Scan to find Devices", comment: "")
        self.deviceList.isHidden = false
        self.didConnectBandView.isHidden = true
        self.scanDevice.setTitle(NSLocalizedString("SCAN", comment: ""), for: .normal)
        self.pulsator.stop()
        self.deInitWave()
        NotificationCenter.default.post(name: .didTappedLogOut, object: nil, userInfo: ["data":"yes"])
    }
    
    func bandConnected(peripheral: CBPeripheral) {
        self.deInitWave()
        self.tabBarController?.selectedIndex = 0
        NotificationCenter.default.post(name: .didConnectToUpMoodBand, object: nil, userInfo: ["data":"yes"])
        self.heartRatePeripheral.discoverServices(nil)
        self.findDevice.text = "Connected to: \(peripheral)"
        self.deviceList.isHidden = true
        self.watches.removeAll()
        self.deviceList.reloadData()
        self.didConnectBandView.isHidden = false
        self.backgroundUUID = peripheral.identifier
        self.flag = true
        self.writeCharacteristic(commandType: 1)
    }
}
    
// MARK: - Handling Local Notification
    
extension DeviceScreenViewController: UNUserNotificationCenterDelegate {
    
    func scheduleNotification() {
        let center = UNUserNotificationCenter.current()
        
        let content = UNMutableNotificationContent()
        content.title = "Upmood"
        content.body = NSLocalizedString("Your band has been disconnected", comment: "")
        content.categoryIdentifier = "alarm"
        content.userInfo = ["customData": "fizzbuzz"]
        content.sound = UNNotificationSound.default()
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.add(request)
    }
    
    func registerCategories() {
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        
        let show = UNNotificationAction(identifier: "show", title: "Tell me more…", options: .foreground)
        let category = UNNotificationCategory(identifier: "alarm", actions: [show], intentIdentifiers: [])
        
        center.setNotificationCategories([category])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // pull out the buried userInfo dictionary
        let userInfo = response.notification.request.content.userInfo
        
        if let customData = userInfo["customData"] as? String {
            print("Custom data received: \(customData)")
            
            switch response.actionIdentifier {
            case UNNotificationDefaultActionIdentifier:
                print("Default identifier")
                
            case "show":
                print("Show more information…")
                break
                
            default:
                break
            }
        }
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}
    
// MARK: - Handling Observer

extension DeviceScreenViewController {
        
    func initObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(displayBatteryPercent(_:)), name: .batteryPercent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.didTappedLogOut), name: .didTappedLogOut, object: nil)
    }
    
    @objc func didTappedLogOut(_ notification: Notification) {
        if heartRatePeripheral == nil {
            
        } else {
            centralManager.cancelPeripheralConnection(heartRatePeripheral)
        }
    }
    
    @objc func displayBatteryPercent(_ notification: Notification) {
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            let batteryLevel = dict["percent"] as? UInt8 ?? 0
            let batteryInt = Int(batteryLevel)
            self.percent_batt = batteryInt
            
            if self.flag == true {
                self.flag = false
                showBatteryPercent()
            }
        }
    }
}
