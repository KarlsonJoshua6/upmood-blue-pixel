//
//  DeviceScreenViewController.swift
//  upmood
//
//  Created by Jason Juliane on 29/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

//import CoreBluetooth
//import UIKit
//
//// MARK: - CBPeripheralDelegate. - CBCentralManagerDelegate
//
//extension DeviceScreenViewController: CBPeripheralDelegate, CBCentralManagerDelegate {
//    
//    // MARK: - CBPeripheralDelegate
//    
//    func writeCharacteristic(commandType: Int) {
//        
//        if heartRatePeripheral != nil {
//            
//            var command:[UInt8]
//            
//            if commandType == 0 {
//                command = [0xA3]
//            } else {
//                command = [0xA2]
//            }
//            
//            if writeChannel != nil {
//                heartRatePeripheral.writeValue(Data(command), for: writeChannel!, type: .withResponse)
//            } else {
//                print("No channel found.")
//            }
//            
//        } else {
//            print("Not connected to any device.")
//        }
//    }
//    
//    /*** Bluetooth Devices Discovered ***/
//    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String: Any], rssi RSSI: NSNumber) {
//        if peripheral.name != nil{
//            watches.append(peripheral.name!)
//            watchesPeriperhalCode = [peripheral.name! : peripheral]
//            deviceList.reloadData()
//        }
//
//        if (peripheral == heartRatePeripheral) {
//            print("Previous Peripheral Discovered, Trying to Reconnect...")
//            if heartRatePeripheral != nil {
//                if let fetchAutoReconnect = UserDefaults.standard.object(forKey: "fetchAutoReconnect") {
//                    if (fetchAutoReconnect as? Int == 1) {
//                        centralManager.connect(heartRatePeripheral)
//                        centralManager.stopScan()
//                    } else {
//                        print("Auto Reconnect Disabled")
//                        if self.centralManager.isScanning {
//                            self.centralManager.stopScan()
//                            print("Reconnect Cancelled")
//                        }
//                    }
//                }
//            }
//        }
//    }
//    
//    /*** Connected to Peripheral ***/
//    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
//        self.bandConnected(peripheral: peripheral)
//    }
//    
//    /*** Disconnected to Peripheral ***/
//    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
//        
//        print("Disconnected to \(peripheral)")
//        heartRatePeripheral = peripheral
//        deviceList.isHidden = false
//        bandDisconnected(peripheral: peripheral)
//        
//        centralManager.scanForPeripherals(withServices: nil)
//        findDevice.text = "Scanning"
//        self.flag = false
//
//        if let fetchAutoReconnect = UserDefaults.standard.object(forKey: "fetchAutoReconnect") {
//            registerCategories()
//            scheduleNotification()
//            if (fetchAutoReconnect as? Int == 1) {
//                self.centralManager.connect(peripheral, options: nil)
//            } else {
//                print("Auto Reconnect Disabled")
//            }
//        }
//    }
//    
//    /*** Discovered Peripheral Services ***/
//    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
//        guard let services = peripheral.services else { return }
//        
//        for service in services {
//            peripheral.discoverCharacteristics(nil, for: service)
//        }
//    }
//    
//    /*** Discovered Services Characteristics ***/
//    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService,
//                    error: Error?) {
//        guard let characteristics = service.characteristics else { return }
//        
//        for characteristic in characteristics {
//            
//            if characteristic.properties.contains(.read) {
//                peripheral.readValue(for: characteristic)
//            }
//            
//            if characteristic.properties.contains(.notify) {
//                peripheral.setNotifyValue(true, for: characteristic)
//            }
//            
//            if characteristic.properties.contains(.write) {
//                print("Write Channel Discovered")
//                writeChannel = characteristic
//            }
//        }
//    }
//    
//    /*** Characteristic Command Receiver ***/
//    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
//        print("Command Received: \(String(describing: String(bytes: characteristic.value!, encoding: String.Encoding.utf8)))")
//    }
//    
//    /*** Services Characteristics Changed ***/
//    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic,
//                    error: Error?) {
//        switch characteristic.uuid.uuidString {
//        case charNotif:
//            let ppi = getPPI(from: characteristic)
//            onPPIReceived(ppi)
//            
//        default:
//            ()
//        }
//    }
//    
//    func onPPIReceived(_ ppi: [UInt8]) {
//        dataProcessor.convertBytesToHex(byteArray: ppi)
//    }
//    
//    func getPPI(from characteristic: CBCharacteristic) -> [UInt8] {
//        guard let characteristicData = characteristic.value else { return [] }
//        return [UInt8](characteristicData)
//    }
//    
//    // MARK: - CBCentralManagerDelegate
//    
//    func centralManagerDidUpdateState(_ central: CBCentralManager) {
//        switch central.state {
//        case .unknown:
//            print("central.state is .unknown")
//        case .resetting:
//            print("central.state is .resetting")
//        case .unsupported:
//            print("central.state is .unsupported")
//            findDevice.text = "Bluetooth not supported"
//            pulsator.isHidden = true
//            scanDevice.isEnabled = false
//            scanDevice.alpha = 0.5
//        case .unauthorized:
//            print("central.state is .unauthorized")
//        case .poweredOff:
//            print("central.state is .poweredOff")
//        case .poweredOn:
//            print("central.state is .poweredOn")
//            centralManager.scanForPeripherals(withServices: nil)
//        }
//    }
//}
