//
//  UITableViewDataSourceUITableViewDelegateExtension.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 22/07/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

//import UIKit
//
//// MARK: - UITableViewDataSource, UITableViewDelegate
//
//extension DeviceScreenViewController: UITableViewDataSource, UITableViewDelegate {
//    
//    func setUp(cell: DeviceTableViewCell, indexPath: IndexPath) {
//        self.currentCell = cell
//        
//        let filtered = self.watches.filter { $0.contains("UpMood") }
//        let watch = filtered[indexPath.row].replacingOccurrences(of: "UpMood", with: "Upmood")
//        
//        cell.DeviceName.text = watch
//        cell.DeviceDescription.isHidden = true
//    }
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        let filtered = self.watches.filter { $0.contains("UpMood") }
//        return filtered.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DeviceTableViewCell {
//            self.setUp(cell: cell, indexPath: indexPath)
//            return cell
//        }
//        
//        return UITableViewCell()
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        let filtered = self.watches.filter { $0.contains("UpMood") }
//        centralManager.stopScan()
//        
//        if watchesPeriperhalCode[filtered[indexPath.row]] != nil {
//            selectedBand = filtered[indexPath.row].replacingOccurrences(of: "UpMood", with: "Upmood")
//            heartRatePeripheral = watchesPeriperhalCode[filtered[indexPath.row]]!
//            heartRatePeripheral.delegate = self
//            centralManager.connect(heartRatePeripheral)
//        }
//        
//        tableView.deselectRow(at: indexPath, animated: true)
//    }
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 80.0
//    }
//}
