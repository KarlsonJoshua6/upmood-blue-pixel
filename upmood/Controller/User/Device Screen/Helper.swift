//
//  Helper.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 22/07/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

extension DeviceScreenViewController {
    
    /*** NotificationCenter Observer ***/
    
    func initObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(displayBatteryPercent(_:)), name: .batteryPercent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.didTappedLogOut), name: .didTappedLogOut, object: nil)
    }
    
    @objc func didTappedLogOut(_ notification: Notification) {
        if heartRatePeripheral == nil {
            
        } else {
            centralManager.cancelPeripheralConnection(heartRatePeripheral)
        }
    }
    
    @objc func displayBatteryPercent(_ notification: Notification) {
        
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            
            let batteryLevel = dict["percent"] as? UInt8 ?? 0
            let batteryInt = Int(batteryLevel)
            self.percent_batt = batteryInt
            
            if self.flag == true {
                self.flag = false
                showBatteryPercent()
            }
            
            print("TEST displayBatteryPercent: \(batteryInt)")
        }
    }
}

extension DeviceScreenViewController {
    
    /*** Battery Usage ***/
    
    func timeRemain(withPercent: Int) -> String {
        switch withPercent {
        case 100:
            return "Remaining: 12 Hours Left"
        case 75:
            return "Remaining: 9 Hours Left"
        case 50:
            return "Remaining: 6 Hours Left"
        case 25:
            return "Remaining: 3 Hours Left"
        case 5:
            return "Remaining: 36 Minutes Left"
        default:
            return "Remaining: Unknown"
        }
    }
    
    @objc func showBatteryPercent() {
        self.updateWave(withPercent: self.percent_batt)
        self.updateTimeRemaining(withPercent: self.percent_batt)
    }
    
    func updateTimeRemaining(withPercent: Int) {
        remainingLabel.isHidden = false
        bandNameLabel.isHidden = false
        remainingLabel.text = timeRemain(withPercent: withPercent)
        bandNameLabel.text = selectedBand
    }
    
    func updateWave(withPercent: Int) {
        
        // unhide stuff
        self.containerView.isHidden = false
        self.percentLabel.isHidden = false
        self.remainingLabel.isHidden = false
        self.turnOffSensorButton.isHidden = false
        self.disconnectButton.isHidden = false
        self.bandNameLabel.isHidden = false
        
        // wave
        self.initWave = SPWaterProgressIndicatorView(frame: self.containerView.bounds)
        self.percentLabel.text = "\(withPercent)%"
        self.initWave.completionInPercent = withPercent
        self.initWave.waveColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        self.containerView.addSubview(self.initWave)
    }
    
    func deInitWave() {
        self.containerView.isHidden = true
        self.remainingLabel.isHidden = true
        self.percentLabel.isHidden = true
        self.turnOffSensorButton.isHidden = true
        self.disconnectButton.isHidden = true
        self.bandNameLabel.isHidden = true
        self.initWave.removeFromSuperview()
    }
}
