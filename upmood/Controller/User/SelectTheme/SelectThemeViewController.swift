//
//  SelectThemeViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 06/12/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//
import Alamofire
import SVProgressHUD
import SDWebImage
import UIKit

class SelectThemeViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    private var setName = [String]()
    private var imageFilePath = [String]()
    private var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        loadStickerMenu()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setUp(cell: SelectThemeCollectionViewCell, indexPath: IndexPath) {
        let uniqueSetName = Array(Set(self.setName))
        cell.stickerNameLabel.text = uniqueSetName[indexPath.row]
        if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(uniqueSetName[indexPath.row].replacingOccurrences(of: " ", with: "%20"))/emoji/happy.png") {
            cell.sticker.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_account_created"), options: [], completed: nil)
        }
    }
    
    @IBAction func selectTheme(_ sender: UIButton) {
        let uniqueSetName = Array(Set(self.setName))
        self.setTheme(sticker: uniqueSetName[selectedIndex])
    }
    
    @IBAction func moreSticker(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "FriendScreen", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "StickerNavigationViewController")
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
    }
}

extension SelectThemeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: - UICollectionViewDelegate, UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectThemeCollectionViewCell",for: indexPath) as? SelectThemeCollectionViewCell {
            setUp(cell: cell, indexPath: indexPath)
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let uniqueSetName = Array(Set(self.setName))
        return uniqueSetName.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/3.0
        let height = width
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

// MARK: - REST

extension SelectThemeViewController {
    
    private func setTheme(sticker: String) {
        Alamofire.request(APIClient.setTheme(sticker: sticker)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let jsonData = JSON["data"] as? [String: Any] else { return }
                
                if statusCode == 200 {
                    this?.dismiss(animated: true, completion: {
                        let _ = jsonData["basic_emoji_set"] as? String ?? ""
                        UserDefaults.standard.set(sticker.lowercased(), forKey: "currentTheme")
                        print("success ba: \(jsonData)")
                    })
                    
                } else if statusCode == 204 {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                    print("204 to")
                } else {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    private func loadStickerMenu() {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.loadDownloadedTheme()).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                
                print("JSON To: \(json)")
                
                
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let jsonData = JSON["data"] as? [[String: Any]] else { return }
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        for json in jsonData {
                            guard let emojiSet = json["emoji"] as? [[[String: Any]]] else { return }
                            for emoji in emojiSet {
                                for emojiMenu in emoji {
                                    let emojiSetStr = emojiMenu["set_name"] as? String ?? ""
                                    let imageFilePath = emojiMenu["filepath"] as? String ?? ""
                                    self.setName.append(emojiSetStr)
                                    self.imageFilePath.append(imageFilePath)
                                }
                            }
                        } // end of fucking loop

                        DispatchQueue.main.async {
                            this?.collectionView.reloadData()
                        }
                    })
                    
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
}
