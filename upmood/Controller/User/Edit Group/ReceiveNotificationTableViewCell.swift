//
//  ReceiveNotificationTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 05/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class ReceiveNotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var checkMark: UIImageView!
    @IBOutlet weak var emotionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.checkMark.image = UIImage(named: "")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
