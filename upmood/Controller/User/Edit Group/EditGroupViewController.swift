//
//  EditGroupViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 05/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import SDWebImage
import SkyFloatingLabelTextField
import UIKit

extension EditGroupViewController {
    
    private func loadSpecificGroupList(withId: Int) {
        Alamofire.request(APIClient.loadSpecificGroupList(withId: withId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                switch statusCode {
                case 200:
                
                    guard let data = JSON["data"] as? [String: Any] else { return }
                    guard let jsonData = data["data"] as? [[String: Any]] else { return }
                    
                    for jsondata in jsonData {
                        
                        let privacyEmotion = jsondata["emotion"] as? Int ?? 0
                        let privacyProfileDetails = jsondata["my_mood"] as? Int ?? 0
                        let privacyHeartBeat = jsondata["heartbeat"] as? Int ?? 0
                        self.privacyEmotion = privacyEmotion
                        self.privacyProfileDetails = privacyProfileDetails
                        self.privacyHeartBeat = privacyHeartBeat
                        
                        let emotions = jsondata["emotions"] as? String ?? ""
                        let notificationType = jsondata["notification_type"] as? String ?? ""

                        UserDefaults.standard.set(emotions, forKey: "fetchEmotions")
                        UserDefaults.standard.set(notificationType, forKey: "fetchNotificationType")
                        UserDefaults.standard.set(self.groupId ?? 0, forKey: "fetchGroupIdInEdit")
                        UserDefaults.standard.set(privacyEmotion, forKey: "fetchPrivacyEmotion")
                        UserDefaults.standard.set(privacyProfileDetails, forKey: "fetchPrivacyProfileDetails")
                        UserDefaults.standard.set(privacyHeartBeat, forKey: "fetchPrivacyHeartBeat")
                    
                    } // end of fucking loop
                    
                    DispatchQueue.main.async {
                        this?.tableView.reloadData()
                    }
                    
                    break
                case 204:
                    break
                case 401:
                    break
                default:
                    break
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    
    private func addFriendToGroup(withGroupId: Int, members: String) {
        Alamofire.request(APIClient.addFriendToGroup(members: members, group_id: withGroupId)).responseJSON { [weak this = self] response in
            
            print("response to 2 - \(response.result.value)")
            
            switch response.result {
            case .success(let json):
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                if statusCode == 200 {
                    print("success edit: \(jsonDic)")
                    UserDefaults.standard.removeObject(forKey: "savedPrivacyGroupPrivacy")
                    UserDefaults.standard.removeObject(forKey: "savedEmotionGroupPrivacy")
                    UserDefaults.standard.removeObject(forKey: "savedHeartRateGroupPrivacy")
                    this?.reload()
                    this?.performSegue(withIdentifier: "reloadViewGroup", sender: self)
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func editGroup(withGroupId: Int, type: String, value: String, type_data: String, emotions: String, heartbeat: Int, stress_level: Int, emotion: Int, my_mood: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.editGroup(withGroupId: withGroupId, type: type, value: value, type_data: type_data, emotions: emotions, heartbeat: heartbeat, stress_level: stress_level, emotion: emotion, my_mood: my_mood)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        print("success edit: \(jsonDic)")
                        let uniqueMemberId = Array(Set(self.memberId))
                        let stringArray = uniqueMemberId.map { String($0) }
                        this?.addFriendToGroup(withGroupId: withGroupId, members: stringArray.joined(separator: ","))
                        print("ito ang mga selected Id: \(stringArray.joined(separator: ","))")
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func deleteFriendInGroup(withId: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.removeFriendToGroup(withId: withId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        self.memberName.remove(at: self.selectedIndex)
                        self.memberId.remove(at: self.selectedIndex)
                        self.memberProfilePic.remove(at: self.selectedIndex)
                        self.tableView.reloadData()
                        self.loadSpecificGroupList(withId: self.groupId ?? 0)
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        self.memberName.remove(at: self.selectedIndex)
                        self.memberId.remove(at: self.selectedIndex)
                        self.memberProfilePic.remove(at: self.selectedIndex)
                        self.tableView.reloadData()
                        self.viewDidLoad()
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    @IBAction func reloadEditVC(_ sender: UIStoryboardSegue) {
//        memberName.removeAll()
//        memberProfilePic.removeAll()
//        memberId.removeAll()
        privacyEmotion = 0
        privacyProfileDetails = 0
        privacyHeartBeat = 0
        tableView.reloadData()
        viewDidLoad()
    }
    
    private func reload() {
        memberName.removeAll()
        memberProfilePic.removeAll()
        memberId.removeAll()
        tableView.reloadData()
        viewDidLoad()
    }
}

class EditGroupViewController: UIViewController {
    
    @IBOutlet weak var offIndicatorLabel: UILabel!
    @IBOutlet weak var groupNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var tableView: UITableView!
    
    var groupName: String!
    var memberName = [String]()
    var memberProfilePic = [String]()
    var memberId = [Int]()
    var groupId: Int!
    
    var selectedNotificationTypeValue: String!
    var selectedEmotions = [String]()
    
    var privacyEmotion: Int!
    var privacyProfileDetails: Int!
    var privacyHeartBeat: Int!
    var selectedIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        displayUI()
        loadSpecificGroupList(withId: self.groupId ?? 0)
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        if self.isMovingFromParentViewController {
//            performSegue(withIdentifier: "reloadViewGroup", sender: self)
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = NSLocalizedString("Edit Group", comment: "")
        navigationController?.navigationBar.isHidden = false
    }
    
    private func displayUI() {
        self.groupNameTextField.text = groupName
        let notificationType = UserDefaults.standard.string(forKey: "fetchNotificationType") ?? ""
        if notificationType == "live" {
            self.offIndicatorLabel.text = NSLocalizedString("On", comment: "")
        } else {
            self.offIndicatorLabel.text = NSLocalizedString("Off", comment: "")
        }
    }
    
    private func setUp(cell: EditGroupSelectedMemberTableViewCell, indexpath: IndexPath) {
        
        let uniqueMemberName = Array(Set(self.memberName))
        let uniqueProfilePic = Array(Set(self.memberProfilePic))
        
        cell.groupNameLabel.text = uniqueMemberName[indexpath.row]
        let urlString = uniqueProfilePic[indexpath.row]
        
        if let url = URL(string: urlString) {
            cell.userProfilePicImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], completed: nil)
        }
    }
    
    @objc private func seeMore(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Remove", comment: ""), style: .default , handler:{ [weak this = self] alert in
            if let indexpath = self.tableView.indexPathForView(sender) {
                this?.deleteFriendInGroup(withId: self.memberId[indexpath.row])
            }
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addMember(_ sender: UIButton) {
        let nextVC = storyboard!.instantiateViewController(withIdentifier: "ChooseMemberViewController") as! ChooseMemberViewController
        nextVC.groupIdSelected = self.groupId!
        nextVC.delegate = self
        present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func done(_ sender: UIButton) {
        
        let fetchPrivacyProfileDetails = UserDefaults.standard.integer(forKey: "fetchPrivacyProfileDetails")
        let fetchPrivacyEmotion = UserDefaults.standard.integer(forKey: "fetchPrivacyEmotion")
        let fetchPrivacyHeartBeat = UserDefaults.standard.integer(forKey: "fetchPrivacyHeartBeat")
        let selectedEmotions = UserDefaults.standard.string(forKey: "fetchEmotions") ?? ""

        if isKeyPresentInUserDefaults(key: "savedPrivacyGroupPrivacy") {
            let savedPrivacyGroupPrivacy = UserDefaults.standard.integer(forKey: "savedPrivacyGroupPrivacy")
            let savedEmotionGroupPrivacy = UserDefaults.standard.integer(forKey: "savedEmotionGroupPrivacy")
            let savedHeartRateGroupPrivacy = UserDefaults.standard.integer(forKey: "savedHeartRateGroupPrivacy")
            self.editGroup(withGroupId: self.groupId!, type: "name", value: self.groupNameTextField.text ?? "", type_data: "", emotions: selectedEmotions, heartbeat: savedHeartRateGroupPrivacy, stress_level: 1, emotion: savedEmotionGroupPrivacy, my_mood: savedPrivacyGroupPrivacy)
        } else {
            self.editGroup(withGroupId: self.groupId!, type: "name", value: self.groupNameTextField.text ?? "", type_data: "", emotions: selectedEmotions, heartbeat: fetchPrivacyHeartBeat, stress_level: 1, emotion: fetchPrivacyEmotion, my_mood: fetchPrivacyProfileDetails)
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true) {
            UserDefaults.standard.set(false, forKey: "didEditGroup")
            UserDefaults.standard.removeObject(forKey: "savedPrivacyGroupPrivacy")
            UserDefaults.standard.removeObject(forKey: "savedEmotionGroupPrivacy")
            UserDefaults.standard.removeObject(forKey: "savedHeartRateGroupPrivacy")
        }
    }
    
    @IBAction func receiveNotification(_ sender: UIButton) {
        let nextVC = storyboard!.instantiateViewController(withIdentifier: "ReceiveNotificationViewController") as! ReceiveNotificationViewController
        nextVC.delegate = self
        present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func mangaePrivacySetting(_ sender: UIButton) {
        let nextVC = storyboard!.instantiateViewController(withIdentifier: "GroupPrivacyViewController") as! GroupPrivacyViewController
        nextVC.privacyEmotion = privacyEmotion
        nextVC.privacyProfileDetails = privacyProfileDetails
        nextVC.privacyHeartBeat = privacyHeartBeat
        nextVC.groupId = groupId
        present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func reload(_ sender: UIStoryboardSegue) {
        self.memberName.removeAll()
        self.memberProfilePic.removeAll()
        self.memberId.removeAll()
        self.tableView.reloadData()
        displayUI()
    }
}   

extension EditGroupViewController: ReceiveNotificationViewControllerDelegate {
    func didEditReceiveNotificationSetting(withEmotion: [String], notificationTypeValue: String) {
        self.selectedNotificationTypeValue = notificationTypeValue
        self.selectedEmotions.append(contentsOf: withEmotion)
        self.viewDidLoad()
        self.tableView.reloadData()
    }
}

extension EditGroupViewController: ChooseMemberViewControllerDelegate {
    func didSelectMember(withMemberId: [Int], memberProfilePic: [String], memberName: [String]) {
        self.memberName.append(contentsOf: memberName)
        self.memberProfilePic.append(contentsOf: memberProfilePic)
        self.memberId.append(contentsOf: withMemberId)
        self.tableView.reloadData()
    }
}

extension EditGroupViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "EditGroupSelectedMemberTableViewCell", for: indexPath) as? EditGroupSelectedMemberTableViewCell {
            setUp(cell: cell, indexpath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let uniqueMemberName = Array(Set(self.memberName))
        return uniqueMemberName.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Remove", comment: ""), style: .default , handler:{ alert in
            self.selectedIndex = indexPath.row
            // TODO
//            self.deleteFriendInGroup(withId: self.memberId[indexPath.row])
            
            self.memberName.remove(at: self.selectedIndex)
            self.memberId.remove(at: self.selectedIndex)
            self.memberProfilePic.remove(at: self.selectedIndex)
            self.tableView.reloadData()
            self.viewDidLoad()
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler:{ alert in
            
        }))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
}
