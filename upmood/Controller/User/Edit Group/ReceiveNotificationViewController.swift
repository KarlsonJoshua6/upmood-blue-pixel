//
//  ReceiveNotificationViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 05/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import UIKit

protocol ReceiveNotificationViewControllerDelegate {
    func didEditReceiveNotificationSetting(withEmotion: [String], notificationTypeValue: String)
}

class ReceiveNotificationViewController: UIViewController {

    @IBOutlet weak var switchNotification: UISwitch!
    @IBOutlet weak var tableView: UITableView!
    
    private var selectedEmotion = [String]()
    var delegate: ReceiveNotificationViewControllerDelegate!
    private var notificationValue = "live"
    
    private var menu:[[String:String]] = []
    private var selectedRows:[IndexPath] = []
    private var didUserSelectOnGroupPrivacy = false
    private var currentCell: ReceiveNotificationTableViewCell!
    private var currentIndexpath: IndexPath!
    
    var newEmotion = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        newEmotion = UserDefaults.standard.string(forKey: "fetchEmotions") ?? "".lowercased()
        self.newEmotion.replacingOccurrences(of: "unzen", with: "")
        
        menu = [["menu":NSLocalizedString("anxious", comment: "")],["menu":NSLocalizedString("calm", comment: "")],["menu":NSLocalizedString("challenged", comment: "")],["menu":NSLocalizedString("confused", comment: "")],["menu":NSLocalizedString("excitement", comment: "")],["menu":NSLocalizedString("happy", comment: "")],["menu":NSLocalizedString("tense", comment: "")],["menu":NSLocalizedString("pleasant", comment: "")],["menu":NSLocalizedString("sad", comment: "")],["menu":NSLocalizedString("unpleasant", comment: "")], ["menu":NSLocalizedString("zen", comment: "")]]
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelection = true
        setUserInterfaceStyleLight(self: self)
        setUpSwitch()
    }
    
    @IBAction func cancel(_ sender: UIButton) {
       self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func OnAndOffNotication(_ sender: UISwitch) {
        
        let groupid = UserDefaults.standard.integer(forKey: "fetchGroupIdInEdit")
        let privacyProfileDetails = UserDefaults.standard.integer(forKey: "fetchPrivacyProfileDetails")
        let privacyEmotion = UserDefaults.standard.integer(forKey: "fetchPrivacyEmotion")
        let privacyHeartBeat = UserDefaults.standard.integer(forKey: "fetchPrivacyHeartBeat")
        
        if sender.isOn {
            self.tableView.isHidden = false
            self.updateReceiveNotification(withGroupId: groupid, value: "live", type_data: "", emotions: "anxious,calm,challenged,confused,excitement,happy,tense,pleasant,sad,unpleasant,zen", heartbeat: privacyHeartBeat, stress_level: 1, emotion: privacyEmotion, my_mood: privacyProfileDetails)
        } else {
            self.tableView.isHidden = true
            self.newEmotion = ""
            UserDefaults.standard.set("", forKey: "fetchEmotions")
            self.updateReceiveNotification(withGroupId: groupid, value: "Off", type_data: "", emotions: self.newEmotion, heartbeat: privacyHeartBeat, stress_level: 1, emotion: privacyEmotion, my_mood: privacyProfileDetails)
        }
    }
    
    @IBAction func done(_ sender: UIButton) {
        self.performSegue(withIdentifier: "reloadEditVC", sender: self)
    }
    
    private func setUpSwitch() {
        let notificationType = UserDefaults.standard.string(forKey: "fetchNotificationType") ?? ""
        if notificationType == "live" {
            switchNotification.isOn = true
            tableView.isHidden = false
        } else {
            switchNotification.isOn = false
            tableView.isHidden = true
        }
    }
}

extension ReceiveNotificationViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiveNotificationTableViewCell", for: indexPath) as? ReceiveNotificationTableViewCell {
            cell.emotionLabel.text = menu[indexPath.row]["menu"]
            self.setUpcell(cell: cell, indexPath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? ReceiveNotificationTableViewCell {
            if cell.checkMark.image == UIImage(named: "") {
                self.manageSelection(cell: cell, index: indexPath)
            } else {
                self.manageDidDeSelection(cell: cell, index: indexPath)
            }
        }
    }
}

extension ReceiveNotificationViewController {
    
    private func updateReceiveNotification(withGroupId: Int, value: String, type_data: String, emotions: String, heartbeat: Int, stress_level: Int, emotion: Int, my_mood: Int) {
        self.tableView.isUserInteractionEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.editGroup(withGroupId: withGroupId, type: "notification_type", value: value, type_data: type_data, emotions: emotions, heartbeat: heartbeat, stress_level: stress_level, emotion: emotion, my_mood: my_mood)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                
                switch statusCode {
                case 200:
                    SVProgressHUD.dismiss(completion: {
                        
                        UserDefaults.standard.set(value, forKey: "fetchNotificationType")
                        self.newEmotion = emotions
                        
                        DispatchQueue.main.async {
                            self.tableView.isUserInteractionEnabled = true
                            self.tableView.reloadData()
                        }
                    })
                   
                    break
                case 204:
                    SVProgressHUD.dismiss(completion: {
                        self.tableView.isUserInteractionEnabled = true
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Please Try Again.")
                    })
                    break
                default:
                    SVProgressHUD.dismiss(completion: {
                        self.tableView.isUserInteractionEnabled = true
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Please Try Again.")
                    })
                    break
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    self.tableView.isUserInteractionEnabled = true
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
}

// MARK: -- UITableView Helpers

extension ReceiveNotificationViewController {
    
    private func setUpcell(cell: ReceiveNotificationTableViewCell, indexPath: IndexPath) {
        loadCell(cell: cell, indexpath: indexPath)
    }
    
    private func manageSelection(cell: ReceiveNotificationTableViewCell, index: IndexPath) {
        
        cell.checkMark.image = UIImage(named: "ic_check_mark")
        
        let groupid = UserDefaults.standard.integer(forKey: "fetchGroupIdInEdit")
        let privacyProfileDetails = UserDefaults.standard.integer(forKey: "fetchPrivacyProfileDetails")
        let privacyEmotion = UserDefaults.standard.integer(forKey: "fetchPrivacyEmotion")
        let privacyHeartBeat = UserDefaults.standard.integer(forKey: "fetchPrivacyHeartBeat")
        
        switch index.row {
        case 0:
            let emotion = "anxious," + newEmotion
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 1:
            let emotion = "calm," + newEmotion
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 2:
            let emotion = "challenged," + newEmotion
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 3:
            let emotion = "confused," + newEmotion
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 4:
            let emotion = "excitement," + newEmotion
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 5:
            let emotion = "happy," + newEmotion
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 6:
            let emotion = "tense," + newEmotion
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 7:
            let emotion = "pleasant," + newEmotion
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 8:
            let emotion = "sad," + newEmotion
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 9:
            let emotion = "unpleasant," + newEmotion
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 10:
            let emotion = "zen," + newEmotion
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        default:
            break
        }
    }

    private func manageDidDeSelection(cell: ReceiveNotificationTableViewCell, index: IndexPath) {
        
        cell.checkMark.image = UIImage(named: "")
        
        let groupid = UserDefaults.standard.integer(forKey: "fetchGroupIdInEdit")
        let privacyProfileDetails = UserDefaults.standard.integer(forKey: "fetchPrivacyProfileDetails")
        let privacyEmotion = UserDefaults.standard.integer(forKey: "fetchPrivacyEmotion")
        let privacyHeartBeat = UserDefaults.standard.integer(forKey: "fetchPrivacyHeartBeat")
        
        switch index.row {
        case 0:
            let emotion = self.newEmotion.replacingOccurrences(of: "anxious,", with: "")
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 1:
            let emotion = self.newEmotion.replacingOccurrences(of: "calm,", with: "")
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 2:
            let emotion = self.newEmotion.replacingOccurrences(of: "challenged,", with: "")
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            
            break
        case 3:
            let emotion = self.newEmotion.replacingOccurrences(of: "confused,", with: "")
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 4:
            let emotion = self.newEmotion.replacingOccurrences(of: "excitement,", with: "")
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 5:
            let emotion = self.newEmotion.replacingOccurrences(of: "happy,", with: "")
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 6:
            let emotion = self.newEmotion.replacingOccurrences(of: "tense,", with: "")
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 7:
            let emotion = self.newEmotion.replacingOccurrences(of: "pleasant,", with: "")
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 8:
            let emotion = self.newEmotion.replacingOccurrences(of: "sad,", with: "")
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 9:
            let emotion = self.newEmotion.replacingOccurrences(of: "unpleasant,", with: "")
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 10:
            let emotion = self.newEmotion.replacingOccurrences(of: "zen", with: "")
            self.updateReceiveNotification(withGroupId: groupid, value: self.notificationValue, type_data: "", emotions: emotion, heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        default:
            break
        }
    }
    
    private func loadCell(cell: ReceiveNotificationTableViewCell, indexpath: IndexPath) {
        
        let arr = newEmotion.components(separatedBy: ",")
        
        if arr.isEmpty {
            cell.checkMark.image = UIImage(named: "")
        }
        
        if arr.contains("anxious") {
            if indexpath.row == 0 {
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if arr.contains("calm") {
            if indexpath.row == 1 {
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if arr.contains("challenged") {
            if indexpath.row == 2 {
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if arr.contains("confused") {
            if indexpath.row == 3 {
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if arr.contains("excitement") {
            if indexpath.row == 4 {
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if arr.contains("happy") {
            if indexpath.row == 5 {
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if arr.contains("tense") {
            if indexpath.row == 6 {
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if arr.contains("pleasant") {
            if indexpath.row == 7 {
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if arr.contains("sad") {
            if indexpath.row == 8 {
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if arr.contains("unpleasant") {
            if indexpath.row == 9 {
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if arr.contains("zen") {
            if indexpath.row == 10 {
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
    }
}
