//
//  ViewGroupViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 28/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import UIKit

class ViewGroupViewController: UIViewController {
    
    @IBOutlet weak var emptyState: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var groupMemberLabel: UILabel!
    
    var groupId: Int!
    var didUserTappedOnNotificationTab = false
    private var groupName: String!
    private var memberId = [Int]()
    private var memberName = [String]()
    private var memberProfilePic = [String]()
    private var filePath = [String]()
    private var stress_level = [String]()
    private var emotion_value = [String]()
    private var isOnline = [Int]()
    private var privacy = [Int]()
    private var privacy_emotion = [Int]()
    private var privacy_heartbeat = [Int]()
    private var privacy_stress_level = [Int]()
    private var heartbeat_count = [Int]()
    private var didReload = false
    private var memberCount = 0
    
    // helper
//    var privacy_emotion_helper: Int!
//    var is_online: Int!
//    var new_is_online = 0
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        emptyState.isHidden = true
        loadSpecificGroupList(withId: self.groupId ?? 0)
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = NSLocalizedString("Group Details", comment: "")
        navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if didUserTappedOnNotificationTab {
            self.didReload = false
        } else {
            self.didReload = true
        }
        
        if self.isMovingFromParentViewController {
            if didReload == true {
                UserDefaults.standard.set(true, forKey: "didReloadParentGroupVC")
                performSegue(withIdentifier: "reloadGroupFromDelete", sender: self)
                self.didReload = false
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? EditGroupVC  {
            itemDetails.groupName = groupName
            itemDetails.memberName = memberName
            itemDetails.memberProfilePic = memberProfilePic
            itemDetails.groupId = groupId
            itemDetails.memberId = memberId
        } else if let itemDetails2 = segue.destination as? FriendDashboardViewController {
            itemDetails2.friendId = memberId[self.selectedIndex]
        } else if let itemDetails3 = segue.destination as? FriendAccountPrivateViewController {
            itemDetails3.friendUsername = memberName[self.selectedIndex]
            itemDetails3.friendProfilePicStr = memberProfilePic[self.selectedIndex]
        } else if let itemDetails4 = segue.destination as? FriendDashboardPrivateEmotionViewController {
            itemDetails4.friendId = memberId[selectedIndex]
            itemDetails4.privacy_settings_heartbeat = privacy_emotion[selectedIndex]
        }
    }
    
    @IBAction func seeMoreOptions(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Delete Group", comment: ""), style: .default , handler:{ [weak this = self] alert in
            this?.deleteGroup(withId: self.groupId)
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler:{ alert in
            
        }))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func editGroup(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "didEditGroup")
        performSegue(withIdentifier: "showEditGroup", sender: self)
    }
    
    private func reload() {
        groupName = ""
        memberCount = 0
        memberName.removeAll()
        filePath.removeAll()
        stress_level.removeAll()
        emotion_value.removeAll()
        isOnline.removeAll()
        privacy.removeAll()
        privacy_emotion.removeAll()
        privacy_heartbeat.removeAll()
        privacy_stress_level.removeAll()
        heartbeat_count.removeAll()
        loadSpecificGroupList(withId: groupId ?? 0)
    }
    
    @IBAction func reloadViewGroup(_ sender: UIStoryboardSegue) {
        reload()
    }
}

extension ViewGroupViewController {
    private func deleteFriendInGroup(withId: Int) {
        Alamofire.request(APIClient.removeFriendToGroup(withId: withId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        this?.reload()
                        print("success remove: \(json)")
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    private func deleteGroup(withId: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.deleteGroup(withId: withId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        this?.didReload = true
                        this?.navigationController?.popViewController(animated: false)
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                         this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    private func loadSpecificGroupList(withId: Int) {
        spinner.startAnimating()
        Alamofire.request(APIClient.loadSpecificGroupList(withId: withId)).responseJSON { [weak this = self] response in
            
            print("load view group: \(response.result.value), \(withId)")
            
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                switch statusCode {
                case 200:
                    this?.spinner.stopAnimating()
                    guard let data = JSON["data"] as? [String: Any] else { return }
                    guard let jsonData = data["data"] as? [[String: Any]] else { return }
                    
                    for jsondata in jsonData {
                        
                        let groupname = jsondata["name"] as? String ?? ""
                        let emotions = jsondata["emotions"] as? String ?? ""
                        let notificationType = jsondata["notification_type"] as? String ?? ""
                        UserDefaults.standard.set(emotions, forKey: "fetchEmotions")
                        UserDefaults.standard.set(notificationType, forKey: "fetchNotificationType")
                        
                        if let groupMember = jsondata["members"] as? [[String: Any]] {
                            this?.groupMemberLabel.text = "\(groupMember.count) \(NSLocalizedString("Member", comment: ""))"
                            this?.groupName = groupname

                            for member in groupMember {
                                
                                let id = member["id"] as? Int ?? 0
                                let memberName = member["name"] as? String ?? ""
                                let memberProfilePic = member["image"] as? String ?? ""
                                let isOnline = member["is_online"] as? Int ?? 0
                                
                                this?.memberId.append(id)
                                this?.memberName.append(memberName)
                                this?.memberProfilePic.append(memberProfilePic.replacingOccurrences(of: " ", with: "%20"))
                                this?.isOnline.append(isOnline)
                                
                                // privacy_setting arr
                                if let privacy_settings = member["privacy_settings"] as? [String: Any] {
                                    let privacy_emotion = privacy_settings["emotion"] as? Int ?? 0
                                    let privacy_heartbeat = privacy_settings["heartbeat"] as? Int ?? 0
                                    let privacy_stress_level = privacy_settings["stress_level"] as? Int ?? 0
                                    let privacy = privacy_settings["privacy"] as? Int ?? 0
                                    this?.privacy_emotion.append(privacy_emotion)
                                    this?.privacy_heartbeat.append(privacy_heartbeat)
                                    this?.privacy_stress_level.append(privacy_stress_level)
                                    this?.privacy.append(privacy)
                                }
                                
                                // record arr
                                if let record = member["record"] as? [String: Any] {
                                    let filePath = record["filepath"] as? String ?? ""
                                    let stress_level = record["stress_level"] as? String ?? ""
                                    let emotion_value = record["emotion_value"] as? String ?? ""
                                    let heartbeat_count = record["heartbeat_count"] as? Int ?? 0
                                    this?.filePath.append(filePath.replacingOccurrences(of: " ", with: "%20"))
                                    this?.stress_level.append(stress_level)
                                    this?.emotion_value.append(emotion_value)
                                    this?.heartbeat_count.append(heartbeat_count)
                                } else {
                                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No mood")
                                }
                                
                            }
                        }
                    } // end of fucking loop
                    
                    DispatchQueue.main.async {
                        self.groupNameLabel.text = self.groupName
                        
                        this?.emptyState.isHidden = true
                        this?.tableView.reloadData()
                    }
                    
                    break
                case 204:
                    DispatchQueue.main.async {
                        self.navigationController?.navigationItem.rightBarButtonItem = nil
                        self.spinner.stopAnimating()
                        self.emptyState.isHidden = false
                    }
                    break
                case 401:
                    break
                default:
                    break
                }

                break
            case .failure(let error):
                this?.spinner.startAnimating()
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    private func isPrivateEmotion(cell: ViewGroupTableViewCell, indexpath: IndexPath) {
        // Private == EMOTION
        
        if privacy_emotion[indexpath.row] == 0 {
            cell.emotionValueLabel.text = "\(NSLocalizedString("Private", comment: ""))"
            cell.emojiImageView.image = UIImage(named: "protection")
        } else {
            self.setUpCurrentMood(emotion: self.emotion_value[indexpath.row], cell: cell, indexpath: indexpath)
            if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(self.filePath[indexpath.row])") {
                cell.emojiImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_no_emotion_value"), options: [], completed: nil)
            }
        }
    }
    
    private func isPrivateBPM(cell: ViewGroupTableViewCell, indexpath: IndexPath) {
        // Private == HEART_BEATS
         
        print("hello \(self.privacy_heartbeat)")
        
        if privacy_heartbeat[indexpath.row] == 0 {
//            cell.heartRateIndicatorImageView.image = UIImage(named: "private-friends-heart")
            cell.heartRateLabel.text = "0"
        } else {
            if heartbeat_count[indexpath.row] == 0 {
                cell.heartRateLabel.text = "No BPM"
            } else {
                colorStress(level: self.stress_level[indexpath.row], cell: cell)
                cell.heartRateLabel.text = "\(self.heartbeat_count[indexpath.row])"
            }
        }
    }
    
    private func setUp(cell: ViewGroupTableViewCell, indexpath: IndexPath) {

        // IsOnline ==
        
        if isOnline[indexpath.row] == 0 {
            cell.onlineIndicator.image = UIImage(named: "ic_is_offline_indicator")
            cell.emotionValueLabel.text = NSLocalizedString("Inactive", comment: "")
            cell.heartRateLabel.text = "♡"
            self.isPrivateEmotion(cell: cell, indexpath: indexpath)
        } else {
            cell.onlineIndicator.image = UIImage(named: "ic_is_online_dot")
            self.isPrivateEmotion(cell: cell, indexpath: indexpath)
            self.isPrivateBPM(cell: cell, indexpath: indexpath)
        }
        
        // DEFAULT - Complete Data Display
        
        cell.groupMemberNameLabel.text = memberName[indexpath.row]
        
        if self.memberProfilePic[indexpath.row].isEmpty {
            cell.groupMemberProfilePic.image = UIImage(named: "ic_empty_state_profile")
        }
        
        if let url = URL(string: "\(self.memberProfilePic[indexpath.row])") {
            cell.groupMemberProfilePic.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], completed: nil)
        }
        
        // setUP See More Option Button
        cell.seeMoreButton.addTarget(self, action: #selector(self.seeMore(_:)), for: .touchUpInside)
    }
    
    private func colorStress(level: String, cell: ViewGroupTableViewCell) {
        switch level {
        case "Low":
            cell.circleView.backgroundColor = UIColor.blue
            break
        case "Mild":
            cell.circleView.backgroundColor = UIColor.green
            break
        case "Moderate":
            cell.circleView.backgroundColor = UIColor.yellow
            break
        case "Severe":
            cell.circleView.backgroundColor = UIColor.red
            break
        default:
            cell.circleView.backgroundColor = UIColor.lightGray
            break
        }
    }
    
    @objc private func seeMore(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Remove", comment: ""), style: .default , handler:{ [weak this = self] alert in
            if let indexpath = self.tableView.indexPathForView(sender) {
                this?.deleteFriendInGroup(withId: self.memberId[indexpath.row])
            }
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler:{ alert in
            
        }))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func setUpCurrentMood(emotion: String, cell: ViewGroupTableViewCell, indexpath: IndexPath) {
        switch emotion {
           case "Excitement":
               cell.emotionValueLabel.text = NSLocalizedString("Excitement", comment: "")
           case "Happy":
               cell.emotionValueLabel.text = NSLocalizedString("Happy", comment: "")
           case "Zen":
               cell.emotionValueLabel.text = NSLocalizedString("Zen", comment: "")
           case "Pleasant":
               cell.emotionValueLabel.text = NSLocalizedString("Pleasant", comment: "")
           case "Calm":
               cell.emotionValueLabel.text = NSLocalizedString("Calm", comment: "")
           case "Unpleasant":
               cell.emotionValueLabel.text = NSLocalizedString("Unpleasant", comment: "")
           case "Confused":
               cell.emotionValueLabel.text = NSLocalizedString("Confused", comment: "")
           case "Challenged":
               cell.emotionValueLabel.text = NSLocalizedString("Challenged", comment: "")
           case "Tense":
               cell.emotionValueLabel.text = NSLocalizedString("Tense", comment: "")
           case "Sad":
               cell.emotionValueLabel.text = NSLocalizedString("Sad", comment: "")
           case "Anxious":
               cell.emotionValueLabel.text = NSLocalizedString("Anxious", comment: "")
           case "Loading":
               cell.emotionValueLabel.text = NSLocalizedString("Loading", comment: "")
        default:
            break
        }
    }
}

extension ViewGroupViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ViewGroupTableViewCell", for: indexPath) as? ViewGroupTableViewCell {
            setUp(cell: cell, indexpath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memberName.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 107.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath.row
        
        if self.privacy_emotion[indexPath.row] == 0 {
            performSegue(withIdentifier: "privateEmotionProfile", sender: self)
        } else if self.privacy[indexPath.row] == 0 {
            performSegue(withIdentifier: "showFriendDashboardPrivate", sender: self)
        } else {
            performSegue(withIdentifier: "showFriendDashboard", sender: self)
        }
    }
}
