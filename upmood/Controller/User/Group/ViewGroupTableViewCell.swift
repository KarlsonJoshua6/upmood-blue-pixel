//
//  ViewGroupTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 28/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class ViewGroupTableViewCell: UITableViewCell {

    @IBOutlet weak var onlineIndicator: UIImageView!
    @IBOutlet weak var circleView: circleUIView!
    @IBOutlet weak var heartRateLabel: UILabel!
    @IBOutlet weak var groupMemberNameLabel: UILabel!
    @IBOutlet weak var groupMemberProfilePic: ClipToBoundsImageView!
    @IBOutlet weak var emotionValueLabel: UILabel!
    @IBOutlet weak var heartRateIndicator: UILabel!
    @IBOutlet weak var heartRateIndicatorImageView: UIImageView!
    @IBOutlet weak var emojiImageView: UIImageView!
    @IBOutlet weak var seeMoreButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
