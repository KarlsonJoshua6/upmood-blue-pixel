//
//  GroupViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 18/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import JJFloatingActionButton
import KeychainSwift
import SDWebImage
import SVProgressHUD
import UIKit

class GroupViewController: UIViewController {

    @IBOutlet var emptyState: UIView!
    @IBOutlet weak var tableView: UITableView!
    fileprivate let actionButton = JJFloatingActionButton()
    
    @IBOutlet weak var labelAddnew: UILabel!
    @IBOutlet weak var labelCreate: UILabel!
    
    private var groupId = [Int]()
    private var groupName = [String]()
    private var groupMemberCount = [Int]()
    private var selectedIndex = 0
    private var currentPage = 0
    private var didHaveNextPage = true
    var groups = [Group]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = true
        emptyState.isHidden = true
        
        UserDefaults.standard.set(false, forKey: "didReloadParentGroupVC")
        
        if UserDefaults.standard.bool(forKey: "didReloadAddGroupViewController") == true {
            UserDefaults.standard.set(false, forKey: "didReloadAddGroupViewController")
        } else {
            instantiateActionButton()
        }
        
        loadGroup()
        getDarkmode()
        
    }
    private func getDarkmode(){
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .light {
               
            } else {
                labelAddnew.textColor = UIColor.white
                labelCreate.textColor = UIColor.white
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // guest
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
            self.actionButton.isUserInteractionEnabled = false
            self.tableView.isUserInteractionEnabled = false
        } else {
            self.actionButton.isUserInteractionEnabled = true
            self.tableView.isUserInteractionEnabled = true
        }
        
        if UserDefaults.standard.bool(forKey: "didReloadParentGroupVC") == true {
            print("hahahahah \(UserDefaults.standard.bool(forKey: "didReloadParentGroupVC"))")
            self.groupMemberCount.removeAll()
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.isHidden = true
            self.emptyState.isHidden = true
            self.tableView.reloadData()
            self.removeAllObjectsInArray()
            self.tableView.reloadData()
            loadGroup()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UserDefaults.standard.set("", forKey: "fetchEmotions")
        UserDefaults.standard.set("", forKey: "selectedEmotions")
        UserDefaults.standard.set("", forKey: "fetchNotificationType")
        UserDefaults.standard.set(0, forKey: "fetchGroupIdInEdit")
        UserDefaults.standard.set(0, forKey: "fetchPrivacyEmotion")
        UserDefaults.standard.set(0, forKey: "fetchPrivacyProfileDetails")
        UserDefaults.standard.set(0, forKey: "fetchPrivacyHeartBeat")
//        UserDefaults.standard.set("", forKey: "notificationValue")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? ViewGroupViewController  {
            itemDetails.groupId = groupId[selectedIndex]
//            itemDetails.groupName = groupName[selectedIndex]
        }
    }
    
    private func instantiateActionButton() {
        actionButton.buttonColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        actionButton.addItem(title: "Create Group", image: #imageLiteral(resourceName: "ic_plus_sign")) { [weak this = self] item in
            let storyboard = UIStoryboard(name: "AddGroup", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AddGroup")
            controller.modalPresentationStyle = .fullScreen
            this?.present(controller, animated: true, completion: nil)
        }
        
        actionButton.display(inViewController: self)
    }
    
    private func removeAllObjectsInArray() {
        self.groupId.removeAll()
        self.groupName.removeAll()
        self.groupMemberCount.removeAll()
        self.selectedIndex = 0
        self.tableView.reloadData()
    }
    
    @objc private func reload() {
        self.groupMemberCount.removeAll()
        self.groupId.removeAll()
        self.groupName.removeAll()
        self.groupMemberCount.removeAll()
        self.selectedIndex = 0
        self.tableView.reloadData()
        self.viewDidLoad()
    }
    
    @IBAction func reloadGroupFromDelete(_ sender: UIStoryboardSegue) {
        self.groupId.removeAll()
        self.groupName.removeAll()
        self.groupMemberCount.removeAll()
        self.selectedIndex = 0
        self.tableView.reloadData()
    }
    
    @IBAction func reloadGroup(_ sender: UIStoryboardSegue) {
        self.reload()
    }
    
    private func notificationListener() {
        
        print("im called")
        
        let type_id_notif = UserDefaults.standard.string(forKey: "type_id_notif")
        
        if type_id_notif == "1" {
            //
        } else if type_id_notif == "3" {
            performSegue(withIdentifier: "showGroup", sender: self)
        }
    }
}

extension GroupViewController {
    
    private func loadGroupWithPagination(withPageOf: Int) {
        let token = KeychainSwift().get("apiToken") ?? ""
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/group?page=\(withPageOf)") else { return }
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
            guard let jsonDic = userObject as? [String: Any] else { return }
            let statusCode = jsonDic["status"] as? Int ?? 0
            guard let data = jsonDic["data"] as? [String: Any] else { return }
            guard let jsonData = data["data"] as? [[String: Any]] else { return }
            
            if statusCode == 200 {
                
                let currentpage = data["current_page"] as? Int ?? 0
                self.currentPage = currentpage
                let nextPage = data["next_page_url"] as? String ?? ""
                if nextPage.isEmpty {
                    self.didHaveNextPage = false
                } else {
                    self.didHaveNextPage = true
                }
                
                for jsonDataArr in jsonData {
                    let groupId = jsonDataArr["id"] as? Int ?? 0
                    let groupName = jsonDataArr["name"] as? String ?? ""
                    self.groupId.append(groupId)
                    self.groupName.append(groupName)
                    
                    if let groupsMember = jsonDataArr["members"] as? [[String: Any]] {
                        self.groupMemberCount.append(groupsMember.count)
                    } else {
                        SVProgressHUD.dismiss(completion: {
                            self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                        })
                    }
                }
                
                // operations
                DispatchQueue.main.async {
                    self.tableView.isHidden = false
                    self.emptyState.isHidden = true
                    self.tableView.reloadData()
                }
                
            } else if statusCode == 204 {
//                SVProgressHUD.dismiss(completion: {
//                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
//                })
            } else {
                SVProgressHUD.dismiss(completion: {
                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                })
            }
            
        }}.resume()
    }
    
    
    private func loadGroup() {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.loadGroupList()).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                if let data = JSON["data"] as? [String: Any] {
                    
                    let currentpage = data["current_page"] as? Int ?? 0
                    self.currentPage = currentpage
                    let nextPage = data["next_page_url"] as? String ?? ""
                    if nextPage.isEmpty {
                        self.didHaveNextPage = false
                    } else {
                        self.didHaveNextPage = true
                    }

                    if let jsonData = data["data"] as? [[String: Any]] {
                        
                        if statusCode == 200 {
                            SVProgressHUD.dismiss(completion: {
                                for jsonDataArr in jsonData {
                                    let groupId = jsonDataArr["id"] as? Int ?? 0
                                    let groupName = jsonDataArr["name"] as? String ?? ""
                                    this?.groupId.append(groupId)
                                    this?.groupName.append(groupName)
                                    
                                    if let groupsMember = jsonDataArr["members"] as? [[String: Any]] {
                                        self.groupMemberCount.append(groupsMember.count)
                                    } else {
                                        SVProgressHUD.dismiss(completion: {
                                            this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                                        })
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    self.tableView.isHidden = false
                                    self.emptyState.isHidden = true
                                    this?.tableView.reloadData()
                                }
                            })
                            
                        } else if statusCode == 204 {
                            SVProgressHUD.dismiss(completion: {
                                DispatchQueue.main.async {
                                    self.tableView.isHidden = true
                                    self.emptyState.isHidden = false
                                }
                            })
                        } else {
                            SVProgressHUD.dismiss(completion: {
                                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                            })
                        }
                    }
                } // end of
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
}

extension GroupViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "GroupTableViewCell", for: indexPath) as? GroupTableViewCell {

            if self.groupMemberCount[indexPath.row] == 1 {
                cell.memberCountLabel.text = "\(self.groupMemberCount[indexPath.row]) \(NSLocalizedString("Member", comment: ""))"
            } else {
                cell.memberCountLabel.text = "\(self.groupMemberCount[indexPath.row]) \(NSLocalizedString("Members", comment: ""))"
            }
            
            cell.groupNameLabel.text = groupName[indexPath.row]
            cell.accessoryType = .disclosureIndicator
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupName.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        performSegue(withIdentifier: "showGroup", sender: self)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1

        if self.didHaveNextPage {
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                currentPage+=1
                self.loadGroupWithPagination(withPageOf: currentPage)
            }

        } else {
            self.tableView.tableFooterView?.isHidden = true
        }
    }
}
