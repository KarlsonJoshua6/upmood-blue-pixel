//
//  StickerMenuCollectionViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 24/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class StickerMenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var emotionStickerMenu: UIImageView!
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                self.isOpaque = true
                self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            } else {
                self.isOpaque = false
                self.transform = CGAffineTransform.identity
            }
        }
    }
}
