//
//  StickerCollectionViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 25/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import WebKit
import UIKit

class StickerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var stickerImageView: UIImageView!
    @IBOutlet weak var stickerNameLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                self.isOpaque = true
                self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                self.stickerNameLabel.textColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            } else {
                self.isOpaque = false
                self.transform = CGAffineTransform.identity
                self.stickerNameLabel.textColor = UIColor.darkGray
            }
        }
    }
}
