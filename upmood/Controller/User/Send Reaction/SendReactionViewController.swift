
//
//  SendReactionViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 24/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SDWebImage
import SVProgressHUD
import WebKit
import UIKit

// MARK: - Network request

extension SendReactionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        if collectionView == self.stickerMenuCollectionView {
            stickerMenuCollectionView.selectItem(at: selectedIndexPath, animated: true, scrollPosition: [])
        } else if collectionView == self.stickerCollectionView {
            stickerCollectionView.selectItem(at: selectedIndexPath, animated: true, scrollPosition: [])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == self.stickerMenuCollectionView {
            return 20.0
        } else if collectionView == self.stickerCollectionView {
            return 5.0
        }
        
        return 10.0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == self.stickerMenuCollectionView {
            return 1
        } else if collectionView == self.stickerCollectionView {
            return 1
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.stickerMenuCollectionView {
            let paid_emoji = UserDefaults.standard.string(forKey: "paid_emoji_set") ?? ""
            let paid_emoji_arr = paid_emoji.components(separatedBy: ",")
            return paid_emoji_arr.count
        } else if collectionView == self.stickerCollectionView {
            return sticker.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.stickerMenuCollectionView {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerMenuCollectionViewCell", for: indexPath) as? StickerMenuCollectionViewCell {
                
                let paid_emoji = UserDefaults.standard.string(forKey: "paid_emoji_set") ?? ""
                let paid_emoji_arr = paid_emoji.components(separatedBy: ",")
                
                if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(paid_emoji_arr[indexPath.row].replacingOccurrences(of: " ", with: "%20"))/emoji/happy.png") {
                    cell.emotionStickerMenu.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_no_emotion_value"), options: [], completed: nil)
                }
                
                return cell
            }
        } else if collectionView == self.stickerCollectionView {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerCollectionViewCell", for: indexPath) as? StickerCollectionViewCell {
                
                cell.stickerNameLabel.text = self.stickerName[indexPath.row]
                if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(self.sticker[indexPath.row].replacingOccurrences(of: " ", with: "%20"))") {
                    cell.stickerImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_no_emotion_value"), options: [], completed: nil)
                }
                
                return cell
            }
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.stickerMenuCollectionView {
            let paid_emoji = UserDefaults.standard.string(forKey: "paid_emoji_set") ?? ""
            let paid_emoji_arr = paid_emoji.components(separatedBy: ",")
            reloadStickers()
            loadSelectedSetOf(sticker: paid_emoji_arr[indexPath.row])
            sendButton.backgroundColor = .lightGray
        } else if collectionView == self.stickerCollectionView {
            self.sendButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            self.selectedIndexOfSticker = indexPath.row
        }
    }
    
    private func loadSelectedSetOf(sticker: String) {
        let token = KeychainSwift().get("apiToken") ?? ""
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/resources/all?set=\(sticker.replacingOccurrences(of: " ", with: "%20"))") else { return }
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                guard let JSON = userObject as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let jsonData = JSON["data"] as? [[String: Any]] else { return }
                
                if statusCode == 200 {
                   
                    for json in jsonData {
                        if let emojiSet = json["sticker"] as? [[[String: Any]]] {
                            for emoji in emojiSet {
                                for emojiMenu in emoji {
                                    let emojiId = emojiMenu["id"] as? Int ?? 0
                                    let emotion = emojiMenu["emotion"] as? String ?? ""
                                    let imageFilePath = emojiMenu["filepath"] as? String ?? ""
                                    self.emojiId.append(emojiId)
                                    self.stickerName.append(emotion.capitalized)
                                    self.sticker.append(imageFilePath.replacingOccurrences(of: ".svg", with: ".png"))
                                }
                            }
                            
                             self.enableButtonSend()
                            
                        } else {
                            self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No stickers found")
                        }
                    } // end of fucking loop
                    
                    DispatchQueue.main.async {
                        self.stickerCollectionView.reloadData()
                    }
                    
                } else if statusCode == 204 {
                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                } else {
                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                }
                
        }}.resume()
    }
    
    private func enableButtonSend() {
        DispatchQueue.main.async {
            self.btnSend.isUserInteractionEnabled = true
        }
    }
    
    private func sendReaction(withId: Int, post_id: Int, reaction_resource_id: Int, record_id: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.sendReaction(id: withId, post_id: post_id, reaction_resource_id: reaction_resource_id, record_id: record_id)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    
                    print("JSON: \(json)")
                    
                    SVProgressHUD.dismiss(completion: {
                        this?.dismiss(animated: true, completion: nil)
                    })
                    
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func loadStickerMenu() {
        
        self.stickerMenuActivityIndicator.startAnimating()

        DispatchQueue.main.async {
            self.stickerMenuActivityIndicator.stopAnimating()
            self.displayAllOwnedStickers()
            self.stickerMenuCollectionView.reloadData()
        }
       
//        Alamofire.request(APIClient.loadStickerMenu()).responseJSON { [weak this = self] response in
//            switch response.result {
//            case .success(let json):
//                guard let JSON = json as? [String: Any] else { return }
//                let statusCode = JSON["status"] as? Int ?? 0
//                guard let jsonData = JSON["data"] as? [[String: Any]] else { return }
//
//                if statusCode == 200 {
//
//                    for json in jsonData {
//                        guard let emojiSet = json["emoji"] as? [[[String: Any]]] else { return }
//                        for emoji in emojiSet {
//                            for emojiMenu in emoji {
//                                let emojiSetStr = emojiMenu["set_name"] as? String ?? ""
//                                let imageFilePath = emojiMenu["filepath"] as? String ?? ""
//                                self.setNames.append(emojiSetStr)
//                                self.imageFilePath.append(imageFilePath.replacingOccurrences(of: " ", with: "%20"))
//                            }
//                        }
//                    } // end of fucking loop
//
//                    DispatchQueue.main.async {
//                        this?.stickerMenuActivityIndicator.stopAnimating()
//                        this?.displayAllOwnedStickers()
//                        this?.stickerMenuCollectionView.reloadData()
//                    }
//
//                } else if statusCode == 204 {
//                    SVProgressHUD.dismiss(completion: {
//                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
//                    })
//                } else {
//                    SVProgressHUD.dismiss(completion: {
//                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
//                    })
//                }
//
//                break
//            case .failure(let error):
//                SVProgressHUD.dismiss(completion: {
//                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
//                })
//                break
//            }
//        }
    }
}



class SendReactionViewController: UIViewController {
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var stickerActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var stickerMenuActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var stickerCollectionView: UICollectionView!
    @IBOutlet weak var stickerMenuCollectionView: UICollectionView!
    @IBOutlet weak var friendProfilePicImageView: ClipToBoundsImageView!
    @IBOutlet weak var friendUserNameLabel: UILabel!
    @IBOutlet weak var lblStressLevel: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var friendEmotionValue: UILabel!
    @IBOutlet weak var friendEmotionPicImageView: UIImageView!
    @IBOutlet weak var friendBpmIndicatorImageView: UIImageView!
    @IBOutlet weak var lblProfilePost: UILabel!
    @IBOutlet weak var lblMood: UILabel!
    @IBOutlet weak var viewMood: UIView!
    
    @IBOutlet weak var lineView1: UIView!
    @IBOutlet weak var lineVIew2: UIView!
    @IBOutlet weak var viewEmoji: UIView!
    @IBOutlet weak var sendreactionView: UIView!
    @IBOutlet weak var sendreactionView2: UIView!
    
    
    @IBOutlet weak var btnSend: UIButton!
    
    var bpm = ""
    var bpmIndicatorImageStr = ""
    var emotionValue = ""
    var emotionValueImageStr = ""
    var profPost = ""
    var profilePhoto = ""
    var username = ""
    
    var postId = 0
    var recordId = 0
    var friendId = 0
    var stressLevel = 0
    var dateCreated = ""
    
    private var setNames = [String]()
    private var imageFilePath = [String]()
    private var sticker = [String]()
    private var stickerName = [String]()
    private var emojiId = [Int]()
        
    var selectedIndexOfSticker = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stickerMenuCollectionView.delegate = self
        stickerMenuCollectionView.dataSource = self
        stickerCollectionView.delegate = self
        stickerCollectionView.dataSource = self
        displayUI()
        loadStickerMenu()
        viewMoodLayout()
        setUpCurrentTime()
  
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        view.isOpaque = false
        navigationController?.navigationBar.isHidden = true
        getDarkmode()
        
    }
    
    private func setUpCurrentTime() {
        var currentDateTime = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.timeStyle = .short
        lblTime.text = dateFormatter.string(from: currentDateTime)
    }
    
    private func viewMoodLayout(){
        viewMood.layer.borderWidth = 1
        viewMood.layer.cornerRadius = 8
        viewMood.clipsToBounds = true
        viewMood.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?)
    {
        super.traitCollectionDidChange(previousTraitCollection)
        getDarkmode()
    }
    
    private func getDarkmode(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                sendreactionView2.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_SENDREACTION)
                
                viewEmoji.backgroundColor = UIColor.white
                stickerMenuCollectionView.backgroundColor = UIColor.white
                sendreactionView.backgroundColor = UIColor.white
                      
                lineView1.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_SENDREACTION)
                lineVIew2.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_SENDREACTION)
                
                friendUserNameLabel.textColor = UIColor.darkGray
                lblProfilePost.textColor = UIColor.darkGray
                lblMood.textColor = UIColor.darkGray
                lblTime.textColor = UIColor.darkGray
                friendEmotionValue.textColor = UIColor.darkGray
                lblStressLevel.textColor = UIColor.darkGray
                
                break
            case .dark:
                // dark mode detected
                darkmodeColor()
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func setUpStressLevel(indicator: String) {
        if indicator == "Low" {
            lblStressLevel.text = "\(NSLocalizedString("Stress level", comment: "")): \(NSLocalizedString("Low", comment: ""))"
        } else if indicator == "Mild" {
            lblStressLevel.text = "\(NSLocalizedString("Stress level", comment: "")): \(NSLocalizedString("Mild", comment: ""))"
        } else if indicator == "Moderate" {
            lblStressLevel.text = "\(NSLocalizedString("Stress level", comment: "")): \(NSLocalizedString("Normal", comment: ""))"
        } else if indicator == "Severe" {
            lblStressLevel.text = "\(NSLocalizedString("Stress level", comment: "")): \(NSLocalizedString("High", comment: ""))"
        } else {
        
        }
        
        setUpStressLevelHeart(stressLevel: indicator)
    }
    
    private func reload() {
        stickerName.removeAll()
        emojiId.removeAll()
        sticker.removeAll()
        emojiId.removeAll()
        selectedIndexOfSticker = 0
        self.stickerCollectionView.reloadData()
        
    }
    
    private func displayUI() {
        
        let bpmStr = self.bpm
        let emotionValueStr = self.emotionValue
        let profilePhotoStr = self.profilePhoto
        
        let emotionValueImageStrConverted = self.emotionValueImageStr
        let displayUsername = self.username
        let bpmIndicator = self.bpmIndicatorImageStr
        let profPostStr = self.profPost
        
//        if bpmStr.contains("BPM") {
//            self.friendBpmLabel.text = "\(bpmStr)"
//        } else {
//            self.friendBpmLabel.text = "\(self.bpm!) \(TranslationStr.bpm)"
//        }
        
        setUpStressLevel(indicator: bpmIndicator)
        
        self.friendEmotionValue.text = "\(self.setUpCurrentMood(emotion: emotionValueStr.capitalized))"
        self.friendUserNameLabel.text = displayUsername
        self.lblProfilePost.text = profPostStr
        
        // setUp Images
        
        if let url = URL(string: profilePhotoStr) {
            self.friendProfilePicImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], completed: nil)
        }
        
        if let url2 = URL(string: emotionValueImageStrConverted.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: ".svg", with: ".png")) {
            self.friendEmotionPicImageView.sd_setImage(with: url2, placeholderImage: UIImage(named: "ic_no_emotion_value"), options: [], completed: nil)
        }
    }
    
    private func setUpCurrentMood(emotion: String) -> String {
        switch emotion {
           case "Excitement":
              return NSLocalizedString("Excitement", comment: "")
          case "Happy":
              return NSLocalizedString("Happy", comment: "")
          case "Zen":
              return NSLocalizedString("Zen", comment: "")
          case "Pleasant":
              return NSLocalizedString("Pleasant", comment: "")
          case "Calm":
              return NSLocalizedString("Calm", comment: "")
          case "Unpleasant":
              return NSLocalizedString("Unpleasant", comment: "")
          case "Confused":
              return NSLocalizedString("Confused", comment: "")
          case "Challenged":
              return NSLocalizedString("Challenged", comment: "")
          case "Tense":
              return NSLocalizedString("Tense", comment: "")
          case "Sad":
              return NSLocalizedString("Sad", comment: "")
          case "Anxious":
              return NSLocalizedString("Anxious", comment: "")
          case "Loading":
              return NSLocalizedString("Loading", comment: "")
        default:
            break
        }
        
        return ""
     }
    
    private func displayAllOwnedStickers() {
        let paid_emoji = UserDefaults.standard.string(forKey: "paid_emoji_set") ?? ""
        let paid_emoji_arr = paid_emoji.components(separatedBy: ",")
        loadSelectedSetOf(sticker: paid_emoji_arr[0].replacingOccurrences(of: " ", with: "%20"))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendReaction(_ sender: UIButton) {
        sendReaction(withId: friendId, post_id: postId, reaction_resource_id: emojiId[selectedIndexOfSticker], record_id: recordId)
    }
    
    @IBAction func dismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buyMoreSticker(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "didClickBuySticker")
        performSegue(withIdentifier: "showStickerStore", sender: self)
    }
    
    @IBAction func reloadStickersOnSendReaction(_ sender: UIStoryboardSegue) {
        setNames.removeAll()
        imageFilePath.removeAll()
        sticker.removeAll()
        stickerName.removeAll()
        emojiId.removeAll()
        self.stickerCollectionView.reloadData()
        self.stickerMenuCollectionView.reloadData()
        displayUI()
        loadStickerMenu()
    }
    
    private func darkmodeColor(){
        viewEmoji.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_EMOJI_SENDREACTION)
        stickerMenuCollectionView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_EMOJI_SENDREACTION)
        sendreactionView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)
        sendreactionView2.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)
        lineView1.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)
        lineVIew2.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_COLOR_DARKMODE)
        friendUserNameLabel.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_MOODSTREAM)
        lblProfilePost.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_MOODSTREAM)
        lblMood.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_SENDREACTION)
        lblTime.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_SENDREACTION)
        friendEmotionValue.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_EXCITEMENT)
        lblStressLevel.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_MOODSTREAM)
       }
}

extension SendReactionViewController {
    
    private func reloadStickers() {
        self.emojiId.removeAll()
        self.stickerName.removeAll()
        self.sticker.removeAll()
        self.stickerCollectionView.reloadData()
        self.btnSend.isUserInteractionEnabled = false
    }
    
    private func setUpStressLevelHeart(stressLevel: String) {
        switch stressLevel {
        case "Low":
            self.friendBpmIndicatorImageView.image = UIImage(named: "ic_heart_blue")
            break
        case "Mild":
            self.friendBpmIndicatorImageView.image = UIImage(named: "ic_heart_green")
            break
        case "Moderate":
            self.friendBpmIndicatorImageView.image = UIImage(named: "ic_heart_yellow")
            break
        case "Severe":
            self.friendBpmIndicatorImageView.image = UIImage(named: "ic_heart_red")
            break
        default:
            self.friendBpmIndicatorImageView.image = UIImage(named: "heart")
            break
        }
    }
}
