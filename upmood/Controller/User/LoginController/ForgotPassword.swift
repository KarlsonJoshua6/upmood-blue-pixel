//
//  SpinnerViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 07/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import SkyFloatingLabelTextField
import UIKit

class ForgotPassword: UIViewController {

    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    
    var emailLinkString: String {
        return emailTextField.text ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        emailTextField.becomeFirstResponder()
        setUpTextField()
        navigationController?.navigationBar.isHidden = false
    }
    
    private func setUpTextField() {
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .light {
                emailTextField.lineColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                emailTextField.placeholderColor = .darkGray
            } else {
                emailTextField.lineColor = .white
                emailTextField.placeholderColor = .white
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setUpTextField()
    }
    
    fileprivate func submitForgotPassword() {
        view.endEditing(true)
        if emailLinkString.isEmpty {
            self.presentDismissableAlertController(title: NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), message: "Please complete form")
        } else if !emailLinkString.contains(".com") || !emailLinkString.contains("@") {
            self.presentDismissableAlertController(title: NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), message: "The email must be a valid email address.")
        } else {
            self.forgotPassword(email: self.emailLinkString)
        }
    }
    
    @IBAction func submit(_ sender: UIButton) {
       submitForgotPassword()
    }
    
    private func forgotPassword(email: String) {
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: "#0099A3"))
        Alamofire.request(APIClient.forgotPassword(email: email)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                let message = jsonData["message"] as? String ?? ""
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: "Change password request sent", message: "Please check your email for the link to reset your password.")
                        this?.view.isUserInteractionEnabled = true
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), message: message)
                        this?.view.isUserInteractionEnabled = true
                    })
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), message: error.localizedDescription)
                this?.view.isUserInteractionEnabled = true
                break
            }
        }
    }
}


extension ForgotPassword: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == UIReturnKeyType.go {
            submitForgotPassword()
        }
        
        return true
    }
}
