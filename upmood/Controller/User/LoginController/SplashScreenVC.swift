//
//  FacebookLoginViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 07/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import FBSDKCoreKit
import FBSDKLoginKit
import FSPagerView
import SVProgressHUD
import RealmSwift
import UIKit

class SplashScreenVC: UIViewController {
    
    var images = [UIImage(named: "ic_account_created")!, UIImage(named: "ic_oops")!]
    var timer: Timer!
    var token = ""
    @IBOutlet weak var lblLanguage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initObserver()
        getLabelLanguage()
        //setLang()
    }
    
    private func getLabelLanguage(){
        let lang = UserDefaults.standard.string(forKey: "selectedLanguage")
        
        if lang == "en" {
           lblLanguage.text = " English"
        }else if lang == "ja"{
           lblLanguage.text = " Japanese"
        }else{
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    private func initObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(toastTerms), name: Notification.Name("toast1"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toastPrivacy), name: Notification.Name("toast2"), object: nil)
    }
    
    @objc func toastTerms(){
        toast(message: "You have declined to accept the Terms and Conditions")
    }
    @objc func toastPrivacy(){
        toast(message: "You have declined to accept the Privacy Policy Statement")
    }
    
    private func toast(message: String){
       
        let alertDisapperTimeInSeconds = 2.0
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + alertDisapperTimeInSeconds) {
            alert.dismiss(animated: true)
        }
    }
    
    @IBAction func reloadSplashScreen(_ sender: UIStoryboardSegue) {
        // reload this view controller
    }
    
    @IBAction func btnCreateAccount(_ sender: Any) {
        Constants.condiPriStr = "signUp"
//        NotificationCenter.default.post(name: Notification.Name("toast1"), object: nil)
    }
    
    private func checkUserStatus() {
        let status = UserDefaults.standard.integer(forKey: "status")
        switch status {
        case 3:
            self.setUpLanguage()
        default:
            performSegue(withIdentifier: "showEmoji", sender: self)
        }
    }
    
    private func setLang(){
        let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage")
        print("gagoka", selectedLanguage)
        switch selectedLanguage {
        case "en":
            Bundle.set(language: "en")
            print("english to")
            break
        case "ja":
            Bundle.set(language: "ja")
            print("japan to")
            break
        default:
            break
        }
    }
    
    private func setUpLanguage() {
    
       let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage")
       print("pota", selectedLanguage)
       if selectedLanguage == "en" {
           Bundle.set(language: "en")
       } else if selectedLanguage == "ja" {
           Bundle.set(language: "ja")
       } else {
           //
       }
       
       let storyboard = UIStoryboard.init(name: "DashboardScreen", bundle: nil)
       UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
       UserDefaults.standard.set(selectedLanguage, forKey: "selectedLanguage")
    }
    
    private func checkIsVerified(isVerified: Int) {
        if isVerified == 1 {
            checkUserStatus()
        } else {
            if self.token.isEmpty {
                presentDismissableAlertController(title: Constants.MessageDialog.errorTitle, message: "Failed to fetch token, Please try to re login.")
            } else {
                performSegue(withIdentifier: "showValidation", sender: self)
            }
        }
    }
    
    @IBAction func SignInWithFacebook(_ sender: UIButton) {
        
    }
}
