//
//  LoginViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 01/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SwiftEntryKit
import SVProgressHUD
import SkyFloatingLabelTextField
import RealmSwift
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AuthenticationServices

class LoginViewController: UIViewController, UIViewControllerTransitioningDelegate {

    @IBOutlet weak var successDot: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var loginButton: TKTransitionSubmitButton!
    @IBOutlet weak var usernameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var btnLoginAsGues: RoundButton!
    @IBOutlet weak var lblCreateAccount: UILabel!
    
    var window: UIWindow?
    
    var userName: String {
        return usernameTextField.text ?? ""
    }
    
    var password: String {
        return passwordTextField.text ?? ""
    }
    
    var didClickShowPassword = true
    private var userId = 0
    
    var didUserIdSavedSuccessfully = true
    var realm: Realm!
    var objectArray: Results<Item> {
        get {
            return realm.objects(Item.self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try! Realm()
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        successDot.isHidden = true
        spinner.stopAnimating()
        getDarkMode()
        getJapTranslation()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        usernameTextField.becomeFirstResponder()
        setUpTextField()
        spinner.isHidden = true
        navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func btnTerms(_ sender: Any) {
        Constants.condiPriStr = "signUp"
    }
    
    private func setUpView() {
        if #available(iOS 13.0, *) {
            let appleButton = ASAuthorizationAppleIDButton()
            appleButton.translatesAutoresizingMaskIntoConstraints = false
            appleButton.addTarget(self, action: #selector(didTapAppleButton), for: .touchUpInside)
            
            view.addSubview(appleButton)
            NSLayoutConstraint.activate([
                appleButton.topAnchor.constraint(equalTo: btnLoginAsGues.bottomAnchor, constant: 8),
                appleButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
                appleButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
                appleButton.heightAnchor.constraint(equalTo: btnLoginAsGues.heightAnchor, constant: 0)
            ])
            
        } else {
           
        }
    }
    
    @objc
    func didTapAppleButton() {
        if #available(iOS 13.0, *) {
            let provider = ASAuthorizationAppleIDProvider()
            let request = provider.createRequest()
            request.requestedScopes = [.email, .fullName]
            
            let controller = ASAuthorizationController(authorizationRequests: [request])
            controller.delegate = self
            controller.presentationContextProvider = self
            controller.performRequests()
            
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func setUpTextField() {
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .light {
                usernameTextField.lineColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                passwordTextField.lineColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                usernameTextField.placeholderColor = .darkGray
                passwordTextField.placeholderColor = .darkGray
            } else {
                usernameTextField.lineColor = .white
                passwordTextField.lineColor = .white
                usernameTextField.placeholderColor = .white
                passwordTextField.placeholderColor = .white
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setUpTextField()
    }
    
    private func getJapTranslation(){
        lblCreateAccount.text = TranslationConstants.CREATEACCOUNT.LOCALIZED
        btnLoginAsGues.setTitle(TranslationConstants.LOGINASGUEST.LOCALIZED, for: .normal)
    }

    private func getDarkMode(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                print("light mode detected")
                
            case .dark:
                // dark mode detected
                print("dark mode detected")
                usernameTextField.placeholderColor = UIColor.white
                passwordTextField.placeholderColor = UIColor.white
                lblLogin.textColor = UIColor.white
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    private func checkIsVerified(isVerified: Int) {
        if isVerified == 1 {
            checkUserStatus()
        } else if isVerified == 0 {
           performSegue(withIdentifier: "showValidationEmail", sender: self)
        }
    }
    
    private func setUpDate(birthday: String) {
        let df = DateFormatter()
        df.dateFormat = "yyyy/MM/dd"
        let oldDate = df.date(from: birthday)
        
        let df2 = DateFormatter()
        df2.dateFormat = "MM/dd/yyyy"
        let newDate = df2.string(from: oldDate ?? Date())
        print("new Date po ito: \(newDate)")
        UserDefaults.standard.set(newDate, forKey: "fetchUserBirthday")
    }
    
    private func nativeLogin(userName: String, password: String) {
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: "#0099A3"))
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 3
        manager.request(APIClient.login(email: userName, password: password)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                let message = jsonData["message"] as? String ?? ""
                
                print("test json parse", jsonData)
                
                if statusCode == 200 {
                    
                    guard let userData = jsonData["data"] as? [String: Any] else { return }
    
                    let isVerified = userData["account_native_verify"] as? Int ?? 0
                    let email = userData["email"] as? String ?? ""
                    let userId = userData["id"] as? Int ?? 0
                    self.userId = userId
                    let token = userData["api_token"] as? String ?? ""
                    let name = userData["name"] as? String ?? ""
                    let profilePic = userData["image"] as? String ?? ""
                    let gender = userData["gender"] as? String ?? ""
                    let birthday = userData["birthday"] as? String ?? ""
                    let weight = userData["weight"] as? String ?? ""
                    let country = userData["country"] as? String ?? ""
                    let username =  userData["username"] as? String ?? ""
                    let facebookId = userData["facebook_id"] as? String ?? ""
                    let profilePost = userData["profile_post"] as? String ?? ""
                    let basic_emoji_set = userData["basic_emoji_set"] as? String ?? ""
                    let paid_emoji_set = userData["paid_emoji_set"] as? String ?? ""
                    let status = userData["status"] as? Int ?? 0
                    let has_trial = userData["has_trial"] as? Int ?? 0
                    let subscription_status = userData["subscription_status"] as? Int ?? 0
                    
                    if let settings = userData["settings"] as? [String: Any] {
                        let privacy = settings["privacy"] as? Int ?? 0
                        let emotion = settings["emotion"] as? Int ?? 0
                        let heartbeat = settings["heartbeat"] as? Int ?? 0
                        let stress_level = settings["stress_level"] as? Int ?? 0
                        let notification_type = settings["notification_type"] as? String ?? ""
                        let auto_reconnect = settings["auto_reconnect"] as? Int ?? 0
                        UserDefaults.standard.set(privacy, forKey: "privacyManagePrivacy")
                        UserDefaults.standard.set(emotion, forKey: "emotionManagePrivacy")
                        UserDefaults.standard.set(heartbeat, forKey: "heartbeatManagePrivacy")
                        UserDefaults.standard.set(notification_type, forKey: "fetchUserNotiificationType")
                        UserDefaults.standard.set(auto_reconnect, forKey: "fetchAutoReconnect")
                        UserDefaults.standard.set(stress_level, forKey: "stressLevelManagerPrivacy")
                    }
                    
                    if let loverDict = userData["lover"] as? [String: Any] {
                        let loverName = loverDict["name"] as? String ?? "No lover's yet."
                        UserDefaults.standard.set(loverName, forKey: "lover_name")
                    }
                    
                    SVProgressHUD.dismiss(completion: {
                        let keychain = KeychainSwift()
                        keychain.set(token, forKey: "apiToken")
                        UserDefaults.standard.set(profilePic, forKey: "fetchProfilePic")
                        UserDefaults.standard.set(name, forKey: "fetchName")
                        UserDefaults.standard.set(email, forKey: "fetchEmail")
                        UserDefaults.standard.set(userId, forKey: "fetchUserId")
                        UserDefaults.standard.set(token, forKey: "fetchUserToken")
                        UserDefaults.standard.set(self.genderCase(gender: gender), forKey: "fetchUserGender")
                        UserDefaults.standard.set(weight, forKey: "fetchWeight")
                        UserDefaults.standard.set(country, forKey: "fetchUserCountry")
                        UserDefaults.standard.set(username, forKey: "fetchUsername")
                        UserDefaults.standard.set(facebookId, forKey: "fetchFacebookId")
                        UserDefaults.standard.set(profilePost, forKey: "profile_post")
                        UserDefaults.standard.set(status, forKey: "status")
                        UserDefaults.standard.set(basic_emoji_set.lowercased(), forKey: "currentTheme")
                        UserDefaults.standard.set(paid_emoji_set, forKey: "paid_emoji_set")
                        UserDefaults.standard.set(has_trial, forKey: "has_trial")
                        UserDefaults.standard.set(subscription_status, forKey: "subscription_status")
                        
                        this?.checkIsVerified(isVerified: isVerified)
                        this?.view.isUserInteractionEnabled = true
                        Constants.didUserLogin = true
                        Constants.nativeUser = true
                        
                        if !birthday.isEmpty {
                            this?.setUpDate(birthday: birthday)
                        }
                    })
                    
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), message: message)
                        this?.view.isUserInteractionEnabled = true
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                    this?.view.isUserInteractionEnabled = true
                })
                
                break
            }
        }
    }
    
    fileprivate func login() {
        view.endEditing(true)
        if userName.isEmpty || password.isEmpty {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString(Constants.MessageDialog.INCOMPLETE_FORM, comment: ""))
        } else {
            nativeLogin(userName: userName, password: password)
        }
    }
    
    private func checkEmail(email: String) {
        Alamofire.request(APIClient.checkEmail(email: email)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let data = json as? [String: Any] else { return }
                let statusCode = data["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    this?.successDot.isHidden = false
                    this?.successDot.image = UIImage(named: "ic_success_email_verify")
                    this?.usernameTextField.errorMessage = ""
//                    this?.spinner.stopAnimating()
                } else {
                    this?.successDot.isHidden = true
                    //                        this?.successDot.image = UIImage(named: "ic_error_email_verify")
                    this?.usernameTextField.errorColor = UIColor.red
                    this?.usernameTextField.errorMessage = "email/username does not match"
                    //                        this?.spinner.stopAnimating()
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    @IBAction func login(_ sender: UIButton) {
        login()
    }
    
    @IBAction func signInWithFacebook(_ sender: UIButton) {
        loginWithFacebook()
    }
    
    @IBAction func showPassword(_ sender: UIButton) {
        if didClickShowPassword == true {
            passwordTextField.isSecureTextEntry = false
            let hideTxt = NSLocalizedString("HIDE", comment: "")
            sender.setTitle(hideTxt, for: .normal)
            
            if passwordTextField.isFirstResponder {
                passwordTextField.becomeFirstResponder()
                passwordTextField.layoutIfNeeded()
            }

        } else {
            passwordTextField.isSecureTextEntry = true
            let showTxt = NSLocalizedString("SHOW", comment: "")
            sender.setTitle(showTxt, for: .normal)
        }
        
        didClickShowPassword = !didClickShowPassword
    }
    
    private func setUpLanguage() {
        
        let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage")
        
        if selectedLanguage == "en" {
            Bundle.set(language: "en")
            print("pota1")
        } else if selectedLanguage == "ja" {
            Bundle.set(language: "ja")
            print("pota2")
        } else {
            //
        }
        
        let storyboard = UIStoryboard.init(name: "DashboardScreen", bundle: nil)
        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
        UserDefaults.standard.set(selectedLanguage, forKey: "selectedLanguage")
    }
    
    func genderCase(gender: String) -> String{
        var genderStr = ""
        switch gender {
        case "male":
            genderStr = "Male"
        case "female":
            genderStr = "Female"
        default:
            break
        }
        return genderStr
    }
}

extension LoginViewController: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return view.window!
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let appleIDToken = appleIDCredential.identityToken else {
                presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: "Unable to fetch identity token")
                return
            }
            
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: appleIDToken.debugDescription)
                return
            }
            
            let token = idTokenString.split{$0 == ","}.map(String.init)
            let name = "\(appleIDCredential.fullName?.givenName ?? "") \(appleIDCredential.fullName?.familyName ?? "")"
            print("apple token : \(token[0])")
            loginApple(name: name, jwt: token[0])
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: error.localizedDescription)
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.usernameTextField {
            checkEmail(email: textField.text!)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.usernameTextField {
            if textField.text! == "" {
                self.successDot.isHidden = true
                self.usernameTextField.errorMessage = ""
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.usernameTextField {
           self.passwordTextField.becomeFirstResponder()
           checkEmail(email: textField.text!)
        }
        
        if textField.returnKeyType == UIReturnKeyType.go {
            login()
        } else if textField.returnKeyType == UIReturnKeyType.next {
            checkEmail(email: textField.text!)
        }
        
      return true
    }
    
    private func checkUserStatus() {
        let status = UserDefaults.standard.integer(forKey: "status")
        switch status {
        case 3:
            self.setUpLanguage()
        default:
            performSegue(withIdentifier: "showEmoji", sender: self)
        }
    }
}

// MARK: - Facebook Sign In

extension LoginViewController {
    
    private func loginApple(name: String, jwt: String) {
           SVProgressHUD.show()
           SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
           
           let token = Constants.Routes.ADMIN_TOKEN
           guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/authenticate/apple") else { return }
           
           let parameters = "name=\(name)&jwt=\(jwt)"
           var request = URLRequest(url: url)
           request.setValue("application/json", forHTTPHeaderField: "Accept")
           request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
           request.httpMethod = "POST"
           request.httpBody = parameters.data(using: .utf8)
           
           URLSession.shared.dataTask(with: request) { (data, response, error) in
               if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                   
               guard let jsonData = userObject as? [String: Any] else { return }
               let statusCode = jsonData["status"] as? Int ?? 0
               let message = jsonData["message"] as? String ?? ""
                   
               switch statusCode {
               case 200:

                   guard let userData = jsonData["data"] as? [String: Any] else { return }
                   
                   let token = userData["api_token"] as? String ?? ""
                   
                   if token.isEmpty {
                       self.presentDismissableAlertController(title: Constants.MessageDialog.errorTitle, message: "Failed to login with apple, Please try again")
                   } else {
                       let isVerified = userData["account_link_verify"] as? Int ?? 0
                       let email = userData["email"] as? String ?? ""
                       let userid = userData["id"] as? Int ?? 0
                       let name = userData["name"] as? String ?? ""
                       let basic_emoji_set = userData["basic_emoji_set"] as? String ?? ""
                       let paid_emoji_set = userData["paid_emoji_set"] as? String ?? ""
                       let profilePic = userData["image"] as? String ?? ""
                       let gender = userData["gender"] as? String ?? ""
                       let birthday = userData["birthday"] as? String ?? ""
                       let country = userData["country"] as? String ?? ""
                       let username =  userData["username"] as? String ?? ""
                       let facebookId = userData["facebook_id"] as? String ?? ""
                       let profilePost = userData["profile_post"] as? String ?? ""
                       let status = userData["status"] as? Int ?? 0
                       let weight = userData["weight"] as? String ?? ""
                       let has_trial = userData["has_trial"] as? Int ?? 0
                       let subscription_status = userData["subscription_status"] as? Int ?? 0
                       
                       if let settings = userData["settings"] as? [String: Any] {
                           let privacy = settings["privacy"] as? Int ?? 0
                           let emotion = settings["emotion"] as? Int ?? 0
                           let heartbeat = settings["heartbeat"] as? Int ?? 0
                           let stress_level = settings["stress_level"] as? Int ?? 0
                           let notification_type = settings["notification_type"] as? String ?? ""
                           let auto_reconnect = settings["auto_reconnect"] as? Int ?? 0
                           UserDefaults.standard.set(privacy, forKey: "privacyManagePrivacy")
                           UserDefaults.standard.set(emotion, forKey: "emotionManagePrivacy")
                           UserDefaults.standard.set(heartbeat, forKey: "heartbeatManagePrivacy")
                           UserDefaults.standard.set(stress_level, forKey: "stressLevelManagerPrivacy")
                           UserDefaults.standard.set(notification_type, forKey: "fetchUserNotiificationType")
                           UserDefaults.standard.set(auto_reconnect, forKey: "fetchAutoReconnect")
                           UserDefaults.standard.set(stress_level, forKey: "fetchStressLevel")
                       }
                    
                    if let loverDict = userData["lover"] as? [String: Any] {
                        let loverName = loverDict["name"] as? String ?? "No lover's yet."
                        UserDefaults.standard.set(loverName, forKey: "lover_name")
                    }
                       
                       SVProgressHUD.dismiss(completion: {
                           let keychain = KeychainSwift()
                           keychain.set(token, forKey: "apiToken")
                           UserDefaults.standard.set(name, forKey: "fetchName")
                           UserDefaults.standard.set(email, forKey: "fetchEmail")
                           UserDefaults.standard.set(userid, forKey: "fetchUserId")
                           UserDefaults.standard.set(token, forKey: "fetchUserToken")
                           UserDefaults.standard.set(self.genderCase(gender: gender), forKey: "fetchUserGender")
                           UserDefaults.standard.set(birthday, forKey: "fetchUserBirthday")
                           UserDefaults.standard.set(country, forKey: "fetchUserCountry")
                           UserDefaults.standard.set(username, forKey: "fetchUsername")
                           UserDefaults.standard.set(facebookId, forKey: "fetchFacebookId")
                           UserDefaults.standard.set(profilePost, forKey: "profile_post")
                           UserDefaults.standard.set(status, forKey: "status")
                           UserDefaults.standard.set(basic_emoji_set.lowercased(), forKey: "currentTheme")
                           UserDefaults.standard.set(paid_emoji_set, forKey: "paid_emoji_set")
                           UserDefaults.standard.set(0, forKey: "isGuest")
                           UserDefaults.standard.set(profilePic, forKey: "fetchProfilePic")
                           UserDefaults.standard.set(weight, forKey: "fetchWeight")
                           UserDefaults.standard.set(has_trial, forKey: "has_trial")
                           UserDefaults.standard.set(subscription_status, forKey: "subscription_status")
                           
       //                    self.token = token
                           self.checkIsVerified(isVerified: isVerified)
                           Constants.nativeUser = false
                       })
                   }

                   break
               case 204:
                   SVProgressHUD.dismiss(completion: { [weak this = self] in
                       this?.presentDismissableAlertController(title: NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), message: "Already Logged in on this account")
                       this?.view.isUserInteractionEnabled = true
                   })
                   break
               case 422:
                   SVProgressHUD.dismiss(completion: { [weak this = self] in
                       this?.presentDismissableAlertController(title: NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), message: message)
                       this?.view.isUserInteractionEnabled = true
                   })
                   break
               case 423:
                   SVProgressHUD.dismiss(completion: { [weak this = self] in
                       print("response to: \(jsonData)")
                       this?.presentDismissableAlertController(title: NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), message: message)
                       this?.view.isUserInteractionEnabled = true
                   })
                   break
               default:
                   SVProgressHUD.dismiss(completion: { [weak this = self] in
                       this?.presentDismissableAlertController(title: NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), message: message)
                       this?.view.isUserInteractionEnabled = true
                   })
                   break
               }
                   
           }}.resume()
       }
    
    
    private func loginFacebook(userName: String, UserId: Int, email: String, imageUrl: String) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        
        let token = Constants.Routes.ADMIN_TOKEN
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/authenticate/facebook") else { return }
        
        let parameters = "facebook_id=\(UserId)&email=\(email)&name=\(userName)&image=\(imageUrl)"
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        request.httpBody = parameters.data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
            guard let jsonData = userObject as? [String: Any] else { return }
            let statusCode = jsonData["status"] as? Int ?? 0
            let message = jsonData["message"] as? String ?? ""
                
            print("userData: \(jsonData)")
            
            switch statusCode {
            case 200:

                guard let userData = jsonData["data"] as? [String: Any] else { return }
                
                let token = userData["api_token"] as? String ?? ""
                
                if token.isEmpty {
                    self.presentDismissableAlertController(title: Constants.MessageDialog.errorTitle, message: "Failed to login with facebook, Please try again")
                } else {
                    let isVerified = userData["account_link_verify"] as? Int ?? 0
                    let email = userData["email"] as? String ?? ""
                    let userid = userData["id"] as? Int ?? 0
                    let name = userData["name"] as? String ?? ""
                    let basic_emoji_set = userData["basic_emoji_set"] as? String ?? ""
                    let paid_emoji_set = userData["paid_emoji_set"] as? String ?? ""
                    let profilePic = userData["image"] as? String ?? ""
                    let gender = userData["gender"] as? String ?? ""
                    let birthday = userData["birthday"] as? String ?? ""
                    let country = userData["country"] as? String ?? ""
                    let username =  userData["username"] as? String ?? ""
                    let facebookId = userData["facebook_id"] as? String ?? ""
                    let profilePost = userData["profile_post"] as? String ?? ""
                    let status = userData["status"] as? Int ?? 0
                    let weight = userData["weight"] as? String ?? ""
                    let has_trial = userData["has_trial"] as? Int ?? 0
                    let subscription_status = userData["subscription_status"] as? Int ?? 0
                    
                    if let settings = userData["settings"] as? [String: Any] {
                        let privacy = settings["privacy"] as? Int ?? 0
                        let emotion = settings["emotion"] as? Int ?? 0
                        let heartbeat = settings["heartbeat"] as? Int ?? 0
                        let stress_level = settings["stress_level"] as? Int ?? 0
                        let notification_type = settings["notification_type"] as? String ?? ""
                        let auto_reconnect = settings["auto_reconnect"] as? Int ?? 0
                        UserDefaults.standard.set(privacy, forKey: "privacyManagePrivacy")
                        UserDefaults.standard.set(emotion, forKey: "emotionManagePrivacy")
                        UserDefaults.standard.set(heartbeat, forKey: "heartbeatManagePrivacy")
                        UserDefaults.standard.set(stress_level, forKey: "stressLevelManagerPrivacy")
                        UserDefaults.standard.set(notification_type, forKey: "fetchUserNotiificationType")
                        UserDefaults.standard.set(auto_reconnect, forKey: "fetchAutoReconnect")
                        UserDefaults.standard.set(stress_level, forKey: "fetchStressLevel")
                        UserDefaults.standard.set(username, forKey: "fetchUsername")
                    }
                    
                    if let loverDict = userData["lover"] as? [String: Any] {
                        let loverName = loverDict["name"] as? String ?? "No lover's yet."
                        UserDefaults.standard.set(loverName, forKey: "lover_name")
                    }
                    
                    SVProgressHUD.dismiss(completion: {
                        let keychain = KeychainSwift()
                        keychain.set(token, forKey: "apiToken")
                        UserDefaults.standard.set(name, forKey: "fetchName")
                        UserDefaults.standard.set(email, forKey: "fetchEmail")
                        UserDefaults.standard.set(userid, forKey: "fetchUserId")
                        UserDefaults.standard.set(token, forKey: "fetchUserToken")
                        print("loginGENDER", gender)
                        UserDefaults.standard.set(self.genderCase(gender: gender), forKey: "fetchUserGender")
                        UserDefaults.standard.set(birthday, forKey: "fetchUserBirthday")
                        UserDefaults.standard.set(country, forKey: "fetchUserCountry")
                        UserDefaults.standard.set(username, forKey: "fetchUsername")
                        UserDefaults.standard.set(facebookId, forKey: "fetchFacebookId")
                        UserDefaults.standard.set(profilePost, forKey: "profile_post")
                        UserDefaults.standard.set(status, forKey: "status")
                        UserDefaults.standard.set(basic_emoji_set.lowercased(), forKey: "currentTheme")
                        UserDefaults.standard.set(paid_emoji_set, forKey: "paid_emoji_set")
                        UserDefaults.standard.set(0, forKey: "isGuest")
                        UserDefaults.standard.set(has_trial, forKey: "has_trial")
                        UserDefaults.standard.set(subscription_status, forKey: "subscription_status")
                        
    //                    self.token = token
    //                    self.checkIsVerified(isVerified: isVerified)
                        Constants.nativeUser = false
                    })
                    
                    if profilePic.isEmpty {
                        UserDefaults.standard.set(imageUrl, forKey: "fetchProfilePic")
                    } else if imageUrl.isEmpty {
                        UserDefaults.standard.set(profilePic, forKey: "fetchProfilePic")
                    } else {
                        UserDefaults.standard.set(profilePic, forKey: "fetchProfilePic")
                    }
                    
                    Constants.condiPriStr = "facebook"
                    
                    DispatchQueue.main.async {
                         let storyBoard: UIStoryboard = UIStoryboard(name: "TermsAndConditionScreen", bundle: nil)
                         let newViewController = storyBoard.instantiateViewController(withIdentifier: "TermsAndCondition") as! TermsAndConditionVc
                         self.present(newViewController, animated: true, completion: nil)
                    }
                }
                break
            default:
                SVProgressHUD.dismiss(completion: { [weak this = self] in
                    this?.presentDismissableAlertController(title: NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), message: message)
                    this?.view.isUserInteractionEnabled = true
                })
                break
            }
                
        }}.resume()
    }
    
    private func loginWithFacebook() {
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: self) { loginResult in
            switch loginResult {
            case .success(_):
                GraphRequest(graphPath: "me", parameters: ["fields": "email, id, name, picture.type(large)"]).start(completionHandler: { [weak this = self] (connection, result, error) -> Void in
                        if error == nil {
                            guard let resultDictionary = result as? [String: Any] else { return }
                            let email = resultDictionary["email"] as? String ?? ""
                            let userName = resultDictionary["name"] as? String ?? ""
                            let userId = resultDictionary["id"] as? String ?? ""
                            let facebookProfileUrl = "http://graph.facebook.com/\(userId)/picture?type=large"
                            let user = User(userName: userName, UserId: userId, email: email, image: facebookProfileUrl)
                            
                            if email.isEmpty {
                                self.presentDismissableAlertController(title: Constants.MessageDialog.errorTitle, message: "Failed to Sign in to Facebook, Facebook Account has no email")
                            } else {
                                self.loginFacebook(userName: user.userName, UserId: Int(user.UserId) ?? 0, email: user.email, imageUrl: user.image)
                            }
                            
                            print("credentials: \(user.UserId), \(user.email), \(facebookProfileUrl)")
                        }
                    })
                
                break
            case .cancelled:
                print("cancelled")
                break
            case .failed(let error):
                print("error \(error.localizedDescription)")
                break
            }
        }
//        let loginManager = LoginManager()
//        loginManager.logOut()
//        loginManager.logIn(permissions: [.publicProfile, .email], viewController: self) { loginResult in
//
//            print("test 1234: \(loginResult)")
//
//            switch loginResult {
//            case .success(_):
//                GraphRequest(graphPath: "me", parameters: ["fields": "email, id, name, picture.type(large)"]).start(completionHandler: { [weak this = self] (connection, result, error) -> Void in
//                        if error == nil {
//                            guard let resultDictionary = result as? [String: Any] else { return }
//                            let email = resultDictionary["email"] as? String ?? ""
//                            let userName = resultDictionary["name"] as? String ?? ""
//                            let userId = resultDictionary["id"] as? String ?? ""
//                            let facebookProfileUrl = "http://graph.facebook.com/\(userId)/picture?type=large"
//                            let user = User(userName: userName, UserId: userId, email: email, image: facebookProfileUrl)
//
//                            if email.isEmpty {
//                                self.presentDismissableAlertController(title: Constants.MessageDialog.errorTitle, message: "Failed to Sign in to Facebook, Facebook Account has no email")
//                            } else {
//                                self.loginFacebook(userName: user.userName, UserId: Int(user.UserId) ?? 0, email: user.email, imageUrl: user.image)
//                            }
//
//                            print("credentials: \(user.UserId), \(user.email), \(facebookProfileUrl)")
//                        }
//                    })
//
//                break
//            case .cancelled:
//                print("cancelled")
//                break
//            case .failed(let error):
//                print("error \(error.localizedDescription)")
//                break
//            }
//        }
    }
}
