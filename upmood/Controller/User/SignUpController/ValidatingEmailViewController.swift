//
//  ValidatingEmailViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 08/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import UIKit

class ValidatingEmailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func resendLink(_ sender: UIButton) {
        sendLink()
    }
    
    @IBAction func skip(_ sender: UIButton) {
//        UserDefaults.standard.removeObject(forKey: "credentialsEmail")
//        let status = UserDefaults.standard.integer(forKey: "status")
//
//        if Constants.firstTimeUser == true {
//            self.performSegue(withIdentifier: "showEmoji", sender: self)
//        } else {
//            if status == 3 {
//                self.performSegue(withIdentifier: "showEmoji", sender: self)
//            } else {
//                self.setUpLanguage()
//            }
//        }
        
        sendLink()
    }
    
    private func sendLink() {
       let email = UserDefaults.standard.string(forKey: "credentialsEmail") ?? ""
       let userId = UserDefaults.standard.string(forKey: "credentialsUserId") ?? ""
       let token = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
       
       SVProgressHUD.show()
       SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
       Alamofire.request(APIClient.sendValidationEmail(email: email, id: Int(userId) ?? 0, token: token)).responseJSON { [weak this = self] response in
           switch response.result {
           case .success(let json):
               guard let jsonData = json as? [String: Any] else { return }
               let statusCode = jsonData["status"] as? Int ?? 0
               
               if statusCode == 200 {
                   SVProgressHUD.dismiss(completion: {
                       if Constants.firstTimeUser == true {
                           self.performSegue(withIdentifier: "showEmoji", sender: self)
                       } else {
                           
                           let status = UserDefaults.standard.integer(forKey: "status")
                           if status == 3 {
                               self.performSegue(withIdentifier: "showEmoji", sender: self)
                           } else {
                               self.setUpLanguage()
                           }
                       }
                   })
                   
               } else {
                   SVProgressHUD.dismiss(completion: {
                       this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                   })
               }
               
               break
           case .failure(let error):
               this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
               break
           }
       }
    }
    
    private func setUpLanguage() {
        let storyboard = UIStoryboard.init(name: "DashboardScreen", bundle: nil)
        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()

    }
}
