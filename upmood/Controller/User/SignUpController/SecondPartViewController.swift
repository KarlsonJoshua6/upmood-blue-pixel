//
//  SecondPartViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 08/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import SkyFloatingLabelTextField
import UIKit

class SecondPartViewController: UIViewController {

    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lblSignIn: UILabel!
    
    lazy var subViewControllers: [UIViewController] = {
        return [
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "FirstPartViewController") as! FirstPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "SecondPartViewController") as! SecondPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "ThirdPartViewController") as! ThirdPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "ValidatingEmailViewController") as! ValidatingEmailViewController,
            ]
    }()
    
    var email: String {
        return emailTextField.text ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let email = UserDefaults.standard.string(forKey: "email") ?? ""
        emailTextField.text = email
        emailTextField.delegate = self
        hideTextInput()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpTextField()
    }
    
    private func setUpTextField() {
        lblSignIn.text = TranslationConstants.SIGNIN.LOCALIZED
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .light {
                emailTextField.lineColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                emailTextField.placeholderColor = .darkGray
            } else {
                emailTextField.lineColor = .white
                emailTextField.placeholderColor = .white
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setUpTextField()
    }
    
    private func hideTextInput() {
          let did = UserDefaults.standard.integer(forKey: "didAgreeTermsAndCondi")
          if did == 0 {
              emailTextField.text = ""
          }
      }
    
    private func checkEmail(email: String) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: "#0099A3"))
        Alamofire.request(APIClient.checkEmail(email: email)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: Constants.MessageDialog.EMAIL_ALREADY_TAKEN)
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        UserDefaults.standard.set(email, forKey: "email")
                        UserDefaults.standard.set(email, forKey: "credentialsEmail")
                        let parentVC = self.parent as! PageViewController
                        parentVC.setViewControllers([self.subViewControllers[2]], direction: .forward, animated: true, completion: nil)
                    })
                } else if statusCode == 419 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Invalid email address.")
                    })
                } else if statusCode == 426 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: Constants.MessageDialog.EMAIL_ALREADY_TAKEN)
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    @IBAction func next(_ sender: UIButton) {
        if email.isEmpty {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString(Constants.MessageDialog.INCOMPLETE_FORM, comment: ""))
        } else if !email.isValidEmail() {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: Constants.MessageDialog.INVALID_EMAIL_FORMAT)
        } else {
            checkEmail(email: email)
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        let parentVC = self.parent as! PageViewController
        parentVC.setViewControllers([subViewControllers[0]], direction: .reverse, animated: true, completion: nil)
    }
}

extension SecondPartViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == UIReturnKeyType.done {
            view.endEditing(true)
        }
        
        return true
    }
}

extension SecondPartViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = subViewControllers.index(of: viewController) ?? 0
        
        if currentIndex <= 0 {
            return nil
        }
        
        return subViewControllers[currentIndex-1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = subViewControllers.index(of: viewController) ?? 0
        if currentIndex >= subViewControllers.count-1 {
            return nil
        }
        
        return subViewControllers[currentIndex+1]
    }
}

extension SecondPartViewController {
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
