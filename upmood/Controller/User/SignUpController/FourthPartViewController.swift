//
//  FourthPartViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 08/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import UIKit

class FourthPartViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var selectedIndex = 0
    private var titles = ["Regular", "Gummy Bear", "Cloud"]
    
    lazy var subViewControllers: [UIViewController] = {
        return [
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "FirstPartViewController") as! FirstPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "SecondPartViewController") as! SecondPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "ThirdPartViewController") as! ThirdPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "FourthPartViewController") as! FourthPartViewController,
            ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.setTheme(sticker: self.titles[selectedIndex])
    }
    
    @IBAction func done(_ sender: UIButton) {
        
        print("ito ang selected nyo: \(self.titles[selectedIndex])")
        
       self.setTheme(sticker: self.titles[selectedIndex])
    }
    
    private func setUpLanguage() {
        let storyboard = UIStoryboard.init(name: "DashboardScreen", bundle: nil)
        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
    }
}

extension FourthPartViewController:  UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedIndexPath, animated: true, scrollPosition: [])
        if let cell = collectionView.cellForItem(at: selectedIndexPath) as? FourthPartCollectionViewCell {
            cell.defaultIconsNameLabel.textColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FourthPartCollectionViewCell", for: indexPath) as? FourthPartCollectionViewCell {
            let images = ["Emoji/regular/calm", "Emoji/gummy bear/calm", "Emoji/cloud/Calm"]
            cell.defaultIconsImageView.image = UIImage(named: images[indexPath.row])
            cell.defaultIconsNameLabel.text = titles[indexPath.row]
            return cell
        }
        
        return UICollectionViewCell()
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! FourthPartCollectionViewCell
//        cell?.layer.cornerRadius = 10
//        cell?.layer.masksToBounds = true
        self.selectedIndex = indexPath.row
        cell.defaultIconsNameLabel.textColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
//        cell?.contentView.backgroundColor = hexStringToUIColor(hex: "#CCCCCC")
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! FourthPartCollectionViewCell
        if #available(iOS 13.0, *) {
            cell.defaultIconsNameLabel.textColor = .darkGray
        } else {
            cell.defaultIconsNameLabel.textColor = .darkGray
        }
    }
}

extension FourthPartViewController : UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        return CGSize(width: collectionViewWidth/3, height: collectionViewWidth/3)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

extension FourthPartViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = subViewControllers.index(of: viewController) ?? 0

        if currentIndex <= 0 {
            return nil
        }

        return subViewControllers[currentIndex-1]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = subViewControllers.index(of: viewController) ?? 0

        if currentIndex >= subViewControllers.count-1 {
            return nil
        }

        return subViewControllers[currentIndex+1]
    }
}

extension FourthPartViewController {
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension FourthPartViewController {
    private func setTheme(sticker: String) {
        Alamofire.request(APIClient.setTheme(sticker: sticker)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let _ = JSON["data"] as? [String: Any] else { return }
                
                print("TEST MO PO AKO: \(JSON)")
                UserDefaults.standard.set(sticker.lowercased(), forKey: "currentTheme")
                
                if statusCode == 200 {
                    this?.update(userStatus: 3)
                } else if statusCode == 204 {
                    //this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                } else {
                    //this?.presentDismissableAlertController(title: "Warning...", message: "Can't Set The Selected Emoji")
                }
                
                this?.setUpLanguage()
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    private func update(userStatus: Int) {
        Alamofire.request(APIClient.updateUserProfile(userStatus: userStatus)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    // do nothing
                } else if statusCode == 204 {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                } else {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
}
