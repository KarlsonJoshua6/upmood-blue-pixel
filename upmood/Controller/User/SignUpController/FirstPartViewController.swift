//
//  FirstPartViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 08/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class FirstPartViewController: UIViewController {

    @IBOutlet weak var firstNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lblSignIn: UILabel!
    
    lazy var subViewControllers: [UIViewController] = {
        return [
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "FirstPartViewController") as! FirstPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "SecondPartViewController") as! SecondPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "ThirdPartViewController") as! ThirdPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "ValidatingEmailViewController") as! ValidatingEmailViewController,
            ]
    }()
    
    var firstName: String {
        return firstNameTextField.text ?? ""
    }
    
    var lastName: String {
        return lastNameTextField.text ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let firstName = UserDefaults.standard.string(forKey: "firstName") ?? ""
        let lastName = UserDefaults.standard.string(forKey: "lastName") ?? ""
        firstNameTextField.text = firstName
        lastNameTextField.text = lastName
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        firstNameTextField.becomeFirstResponder()
        hideTextInput()
        setUpTextField()
        UserDefaults.standard.set(true, forKey: "isFirstVC")
    }
    
    private func setUpTextField() {
        lblSignIn.text = TranslationConstants.SIGNIN.LOCALIZED
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .light {
                firstNameTextField.lineColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                lastNameTextField.lineColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                firstNameTextField.placeholderColor = .darkGray
                lastNameTextField.placeholderColor = .darkGray
            } else {
                firstNameTextField.lineColor = .white
                lastNameTextField.lineColor = .white
                firstNameTextField.placeholderColor = .white
                lastNameTextField.placeholderColor = .white
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setUpTextField()
    }

    @IBAction func next(_ sender: UIButton) {
        if firstName.isEmpty || lastName.isEmpty {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString(Constants.MessageDialog.INCOMPLETE_FORM, comment: ""))
        } else {
            UserDefaults.standard.set(firstName, forKey: "firstName")
            UserDefaults.standard.set(lastName, forKey: "lastName")
            let parentVC = self.parent as! PageViewController
            parentVC.setViewControllers([subViewControllers[1]], direction: .forward, animated: true, completion: nil)
        }
    }
    
    @IBAction func close(_ sender: UIButton) {
        UserDefaults.standard.removeObject(forKey: "firstName")
        UserDefaults.standard.removeObject(forKey: "lastName")
        UserDefaults.standard.removeObject(forKey: "email")
        performSegue(withIdentifier: "reloadSplashScreen", sender: self)
    }
    
    private func hideTextInput() {
        let did = UserDefaults.standard.integer(forKey: "didAgreeTermsAndCondi")
        if did == 0 {
            firstNameTextField.text = ""
            lastNameTextField.text = ""
        }
    }
}

extension FirstPartViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstNameTextField {
            self.lastNameTextField.becomeFirstResponder()
        }
        
        if textField.returnKeyType == UIReturnKeyType.done {
            view.endEditing(true)
        }
        
        return true
    }
}

extension FirstPartViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = subViewControllers.index(of: viewController) ?? 0
        
        if currentIndex <= 0 {
            return nil
        }
        
        return subViewControllers[currentIndex-1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = subViewControllers.index(of: viewController) ?? 0
        
        if currentIndex >= subViewControllers.count-1 {
            return nil
        }
        
        return subViewControllers[currentIndex+1]
    }
}
