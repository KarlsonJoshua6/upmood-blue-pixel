//
//  PageViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 08/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
   
    lazy var subViewControllers: [UIViewController] = {
        return [
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "FirstPartViewController") as! FirstPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "SecondPartViewController") as! SecondPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "ThirdPartViewController") as! ThirdPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "ValidatingEmailViewController") as! ValidatingEmailViewController,
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        self.removeSwipeGesture()
        setViewControllers([subViewControllers[0]], direction: .forward, animated: true, completion: nil)
    }
    
    func removeSwipeGesture(){
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = subViewControllers.index(of: viewController) ?? 0
        if (currentIndex <= 0) {
            return nil
        }
        
        return subViewControllers[currentIndex-1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = subViewControllers.index(of: viewController) ?? 0
        if (currentIndex >= subViewControllers.count-1) {
            return nil
        }
        
        return subViewControllers[currentIndex+1]
    }
}

