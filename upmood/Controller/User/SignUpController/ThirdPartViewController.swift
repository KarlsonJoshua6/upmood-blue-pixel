//
//  ThirdPartViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 08/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SVProgressHUD
import SkyFloatingLabelTextField
import UIKit

class ThirdPartViewController: UIViewController {

    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lblSignIn: UILabel!
    
    lazy var subViewControllers: [UIViewController] = {
        return [
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "FirstPartViewController") as! FirstPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "SecondPartViewController") as! SecondPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "ThirdPartViewController") as! ThirdPartViewController,
            UIStoryboard(name: "CreateAccountScreen", bundle: nil).instantiateViewController(withIdentifier: "ValidatingEmailViewController") as! ValidatingEmailViewController,
            ]
    }()
    
    var didClickShowPassword = true
    var didClickShowConfirmPassword = true
    
    var password: String {
        return passwordTextField.text ?? ""
    }
    
    var confirmPassword: String {
        return confirmPasswordTextField.text ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpTextField()
    }
    
    private func setUpTextField() {
        lblSignIn.text = TranslationConstants.SIGNIN.LOCALIZED
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .light {
                passwordTextField.lineColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                confirmPasswordTextField.lineColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                passwordTextField.placeholderColor = .darkGray
                confirmPasswordTextField.placeholderColor = .darkGray
            } else {
                passwordTextField.lineColor = .white
                confirmPasswordTextField.lineColor = .white
                passwordTextField.placeholderColor = .white
                confirmPasswordTextField.placeholderColor = .white
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setUpTextField()
    }
    
    private func sendValidation(email: String, id: Int, token: String) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: "#0099A3"))
        Alamofire.request(APIClient.sendValidationEmail(email: email, id: id, token: token)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let statusCode = jsonData["status"] as? Int ?? 0
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        print("success: \(jsonData)")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }

    @IBAction func showPassword(_ sender: UIButton) {
        if didClickShowPassword == true {
            passwordTextField.isSecureTextEntry = false
            //sender.setTitle("HIDE", for: .normal)
            sender.setTitle(NSLocalizedString("HIDE", comment: ""), for: .normal)
            
            
            if passwordTextField.isFirstResponder {
                passwordTextField.becomeFirstResponder()
                passwordTextField.layoutIfNeeded()
            }
            
        } else {
            passwordTextField.isSecureTextEntry = true
           // sender.setTitle("SHOW", for: .normal)
            sender.setTitle(NSLocalizedString("SHOW", comment: ""), for: .normal)
        }
        
        didClickShowPassword = !didClickShowPassword
    }
    
    @IBAction func showConfirmPassword(_ sender: UIButton) {
        if didClickShowConfirmPassword == true {
            confirmPasswordTextField.isSecureTextEntry = false
            //sender.setTitle("HIDE", for: .normal)
            sender.setTitle(NSLocalizedString("HIDE", comment: ""), for: .normal)
            
            if confirmPasswordTextField.isFirstResponder {
                confirmPasswordTextField.becomeFirstResponder()
                confirmPasswordTextField.layoutIfNeeded()
            }
            
        } else {
            confirmPasswordTextField.isSecureTextEntry = true
           // sender.setTitle("SHOW", for: .normal)
            sender.setTitle(NSLocalizedString("SHOW", comment: ""), for: .normal)
        }
        
        didClickShowConfirmPassword = !didClickShowConfirmPassword
    }
    
    @IBAction func back(_ sender: UIButton) {
        let parentVC = self.parent as! PageViewController
        parentVC.setViewControllers([subViewControllers[1]], direction: .reverse, animated: true, completion: nil)
    }
    
    @IBAction func next(_ sender: UIButton) {
        view.isUserInteractionEnabled = false
        if password.isEmpty || confirmPassword.isEmpty {
            view.isUserInteractionEnabled = true
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString(Constants.MessageDialog.INCOMPLETE_FORM, comment: ""))
        } else if password != confirmPassword {
            view.isUserInteractionEnabled = true
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Password mismatch")
        } else if !password.isValidPassword() {
            view.isUserInteractionEnabled = true
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Password format incorrect")
        } else {
            signUp()
        }
    }
    
    private func setUpDate(birthday: String) {
        let df = DateFormatter()
        df.dateFormat = "yyyy/MM/dd"
        let oldDate = df.date(from: birthday)
        
        let df2 = DateFormatter()
        df2.dateFormat = "MM/dd/yyyy"
        
        let newDate = df2.string(from: oldDate ?? Date())
        print("new Date po ito: \(newDate)")
        UserDefaults.standard.set(newDate, forKey: "fetchUserBirthday")
    }
    
    private func instantiateViewController(withStoryboardName: String, viewControllerId: String) {
        let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId)
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: false, completion: nil)
    }
    
     private func signUp() {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: "#0099A3"))
        let email = UserDefaults.standard.string(forKey: "email") ?? ""
        let firstName = UserDefaults.standard.string(forKey: "firstName") ?? ""
        let lastName = UserDefaults.standard.string(forKey: "lastName") ?? ""
        let name = "\(firstName) \(lastName)"
        
        Alamofire.request(APIClient.signUp(email: email, name: name, password: password, confirmPassword: confirmPassword)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonData = json as? [String: Any] else { return }
                let errorMessage = jsonData["message"] as? String ?? ""
                let statusCode = jsonData["status"] as? Int ?? 0

                print("test signUP --- \(jsonData)")
                
                if statusCode == 200 {
                    UserDefaults.standard.removeObject(forKey: "inputPassword")
                    UserDefaults.standard.removeObject(forKey: "inputConfirmPassword")

                    Constants.firstTimeUser = true
                    
                    guard let userData = jsonData["data"] as? [String: Any] else { return }
                    let isVerified = userData["account_native_verify"] as? Int ?? 0
                    let email = userData["email"] as? String ?? ""
                    let userId = userData["id"] as? Int ?? 0
                    let token = userData["api_token"] as? String ?? ""
                    let name = userData["name"] as? String ?? ""
                    let profilePic = userData["image"] as? String ?? ""
                    let gender = userData["gender"] as? String ?? ""
                    let birthday = userData["birthday"] as? String ?? ""
                    let country = userData["country"] as? String ?? ""
                    let username =  userData["username"] as? String ?? ""
                    let facebookId = userData["facebook_id"] as? String ?? ""
                    let profilePost = userData["profile_post"] as? String ?? ""
                    let basic_emoji_set = userData["basic_emoji_set"] as? String ?? ""
                    let status = userData["status"] as? Int ?? 0
                    let paid_emoji_set = userData["paid_emoji_set"] as? String ?? ""
                    let has_trial = userData["has_trial"] as? Int ?? 0
                    let subscription_status = userData["subscription_status"] as? Int ?? 0
                    
                    if let settings = userData["settings"] as? [String: Any] {
                        let privacy = settings["privacy"] as? Int ?? 0
                        let emotion = settings["emotion"] as? Int ?? 0
                        let heartbeat = settings["heartbeat"] as? Int ?? 0
                        let stress_level = settings["stress_level"] as? Int ?? 0
                        let notification_type = settings["notification_type"] as? String ?? ""
                        let auto_reconnect = settings["auto_reconnect"] as? Int ?? 0
                        UserDefaults.standard.set(privacy, forKey: "privacyManagePrivacy")
                        UserDefaults.standard.set(emotion, forKey: "emotionManagePrivacy")
                        UserDefaults.standard.set(heartbeat, forKey: "heartbeatManagePrivacy")
                        UserDefaults.standard.set(stress_level, forKey: "stressLevelManagerPrivacy")
                        
                        UserDefaults.standard.set(notification_type, forKey: "fetchUserNotiificationType")
                        UserDefaults.standard.set(auto_reconnect, forKey: "fetchAutoReconnect")
                        UserDefaults.standard.set(stress_level, forKey: "fetchStressLevel")
                    }
                    
                    self.view.isUserInteractionEnabled = true
                    print("THIRDPARTGENDER", gender)
                    SVProgressHUD.dismiss(completion: {
                        let keychain = KeychainSwift()
                        keychain.set(token, forKey: "apiToken")
                        UserDefaults.standard.set(profilePic, forKey: "fetchProfilePic")
                        UserDefaults.standard.set(name, forKey: "fetchName")
                        UserDefaults.standard.set(email, forKey: "fetchEmail")
                        UserDefaults.standard.set(userId, forKey: "fetchUserId")
                        UserDefaults.standard.set(token, forKey: "fetchUserToken")
                        UserDefaults.standard.set(self.genderCase(gender: gender), forKey: "fetchUserGender")
                        UserDefaults.standard.set(country, forKey: "fetchUserCountry")
                        UserDefaults.standard.set(username, forKey: "fetchUsername")
                        UserDefaults.standard.set(facebookId, forKey: "fetchFacebookId")
                        UserDefaults.standard.set(profilePost, forKey: "profile_post")
                        UserDefaults.standard.set(status, forKey: "status")
                        UserDefaults.standard.set(basic_emoji_set.lowercased(), forKey: "currentTheme")
                        UserDefaults.standard.set(paid_emoji_set, forKey: "paid_emoji_set")
                        UserDefaults.standard.set(0, forKey: "isGuest")
                        
                        print("ito po ang selected theme login: \(basic_emoji_set)")
                        UserDefaults.standard.removeObject(forKey: "email")
                        UserDefaults.standard.removeObject(forKey: "firstName")
                        UserDefaults.standard.removeObject(forKey: "lastName")
                        UserDefaults.standard.set(has_trial, forKey: "has_trial")
                        UserDefaults.standard.set(subscription_status, forKey: "subscription_status")
                        
                        Constants.nativeUser = true
                        
                        if !birthday.isEmpty {
                            this?.setUpDate(birthday: birthday)
                        }
                    })
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "CreateAccountScreen", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ValidatingEmailViewController") as! ValidatingEmailViewController
                    self.present(nextViewController, animated:false, completion:nil)
                    
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        self.view.isUserInteractionEnabled = true
                        this?.presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: errorMessage)
                    })
                } else if statusCode == 422 {
                    SVProgressHUD.dismiss(completion: {
                        self.view.isUserInteractionEnabled = true
                        this?.presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: errorMessage)
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        print("json error to: \(response.result.value)")
                        self.view.isUserInteractionEnabled = true
                        this?.presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: errorMessage)
                    })
                }
                
                break
            case.failure(let error):
                SVProgressHUD.dismiss(completion: {
                    self.view.isUserInteractionEnabled = true
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    func genderCase(gender: String) -> String{
        var genderStr = ""
        switch gender {
        case "male":
            genderStr = "Male"
        case "female":
            genderStr = "Female"
        default:
            break
        }
        return genderStr
    }
}

extension ThirdPartViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField {
            self.confirmPasswordTextField.becomeFirstResponder()
        }
        
        if textField.returnKeyType == UIReturnKeyType.done {
            view.endEditing(true)
        }
        
        return true
    }
}

extension ThirdPartViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = subViewControllers.index(of: viewController) ?? 0
        
        if currentIndex <= 0 {
            return nil
        }
        
        return subViewControllers[currentIndex-1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = subViewControllers.index(of: viewController) ?? 0
        
        if currentIndex >= subViewControllers.count-1 {
            return nil
        }
        
        return subViewControllers[currentIndex+1]
    }
}
