//
//  FriendsTabViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 14/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SDWebImage
import SVProgressHUD
import UIKit

class FriendsTabViewController: UIViewController {

    @IBOutlet weak var emptyState: UIView!
    @IBOutlet weak var emptyStateImageView: UIImageView!
    @IBOutlet weak var emptyStateLabelOne: UILabel!
    @IBOutlet weak var emptyStateLabelTwo: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private var is_online = [Int]()
    private var friendId = [Int]()
    private var friendUserName = [String]()
    private var profPost = [String]()
    private var friendProfilePhoto = [String]()
    private var emotionFilepath = [String]()
    private var stressLevel = [String]()
    private var emotionValue = [String]()
    private var bpm = [Int]()
    private var privacyFlag = [Int]()
    private var privacy_heart_beat = [Int]()
    private var privacy_emotion = [Int]()
    private var privacy_stress_level = [Int]()
    private var dateCreated = [String]()
    
    private var postId = [Int]()
    private var recordId = [Int]()
    private var privacy_settings_emotion = [Int]()
    private var privacy_settings_heartbeat = [Int]()
    private var privacy_settings_stress_level = [Int]()
    private var privacy_settings_stress_privacy = [Int]()
    
    
    // helper
    private var selectedIndex = 0
    private var seeMoreIndex = 0
    private var didSelectSeeMore = false
    private var selectedSendReactionIndex = 0
    private var didHaveNextPage = true
    private var currentPage = 0
    
//    var bpm: String!
//    var bpmIndicatorImageStr: String!
//    var emotionValue: String!
//    var emotionValueImageStr: String!
//    var profilePhoto: String!
//    var username: String!
    var recordsArr = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        emptyState.isHidden = true
        reload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        didSelectSeeMore = false
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.setUpLanguage()
//        self.reload()
    }
    
    @objc private func createGroup(_ sender: UIButton) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? FriendDashboardViewController {
            if didSelectSeeMore == true {
                itemDetails.friendId = friendId[seeMoreIndex]
                itemDetails.postId = postId[seeMoreIndex]
                itemDetails.recordId = recordId[seeMoreIndex]
                
                // privacy settings
                itemDetails.privacy_settings_emotion = privacy_settings_emotion[seeMoreIndex]
                itemDetails.privacy_settings_heartbeat = privacy_settings_heartbeat[seeMoreIndex]
                itemDetails.privacy_settings_stress_level = privacy_settings_stress_level[seeMoreIndex]
            } else {
                itemDetails.friendId = friendId[selectedIndex]
                itemDetails.postId = postId[selectedIndex]
                itemDetails.recordId = recordId[selectedIndex]
                
                // privacy settings
                itemDetails.privacy_settings_emotion = privacy_settings_emotion[selectedIndex]
                itemDetails.privacy_settings_heartbeat = privacy_settings_heartbeat[selectedIndex]
                itemDetails.privacy_settings_stress_level = privacy_settings_stress_level[selectedIndex]
            }
           
        } else if let itemDetails2 = segue.destination as? SendReactionViewController {
            if didSelectSeeMore == true {
                itemDetails2.bpm = "\(bpm[seeMoreIndex])"
                itemDetails2.bpmIndicatorImageStr = stressLevel[seeMoreIndex]
                itemDetails2.emotionValue = emotionValue[seeMoreIndex]
                itemDetails2.emotionValueImageStr = "\(Constants.Routes.BASE_URL_RESOURCE)\(self.emotionFilepath[seeMoreIndex])"
                itemDetails2.profilePhoto = friendProfilePhoto[seeMoreIndex].replacingOccurrences(of: " ", with: "%20")
                itemDetails2.username = friendUserName[seeMoreIndex]
                itemDetails2.profPost = profPost[seeMoreIndex]
               // itemDetails2.dateCreated = dateCreated[seeMoreIndex]
                itemDetails2.postId = postId[seeMoreIndex]
                itemDetails2.recordId = recordId[seeMoreIndex]
                itemDetails2.friendId = friendId[seeMoreIndex]
            } else {
                itemDetails2.bpm = "\(bpm[selectedSendReactionIndex])"
                itemDetails2.bpmIndicatorImageStr = stressLevel[selectedSendReactionIndex]
                itemDetails2.emotionValue = emotionValue[selectedSendReactionIndex]
                itemDetails2.emotionValueImageStr = "\(Constants.Routes.BASE_URL_RESOURCE)\(self.emotionFilepath[selectedSendReactionIndex])"
                itemDetails2.profilePhoto = friendProfilePhoto[selectedSendReactionIndex].replacingOccurrences(of: " ", with: "%20")
                itemDetails2.username = friendUserName[selectedSendReactionIndex]
                itemDetails2.profPost = profPost[selectedSendReactionIndex]
                //itemDetails2.dateCreated = dateCreated[selectedSendReactionIndex]
                itemDetails2.postId = postId[selectedSendReactionIndex]
                itemDetails2.recordId = recordId[selectedSendReactionIndex]
                itemDetails2.friendId = friendId[selectedSendReactionIndex]
            }
        } else if let itemDetails3 = segue.destination as? FriendAccountPrivateViewController {
            if didSelectSeeMore == true {
                itemDetails3.friendUsername = friendUserName[seeMoreIndex]
                itemDetails3.friendProfilePicStr = friendProfilePhoto[seeMoreIndex]
            } else {
                itemDetails3.friendUsername = friendUserName[selectedIndex]
                itemDetails3.friendProfilePicStr = friendProfilePhoto[selectedIndex]
            }
        } else if let itemDetails4 = segue.destination as? FriendDashboardPrivateEmotionViewController {
            itemDetails4.friendId = friendId[selectedIndex]
            itemDetails4.privacy_settings_heartbeat = privacy_settings_heartbeat[selectedIndex]
        }
    }
    
    private func removeFriend(withId: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.removeFriend(withId: withId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        DispatchQueue.main.async {
                            this?.reload()
                        }
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No Record Found!")
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func reload() {
        friendId.removeAll()
        friendUserName.removeAll()
        friendProfilePhoto.removeAll()
        emotionFilepath.removeAll()
        stressLevel.removeAll()
        emotionValue.removeAll()
        bpm.removeAll()
        postId.removeAll()
        recordId.removeAll()
        profPost.removeAll()
        

        // privacy_settings
        privacy_settings_emotion.removeAll()
        privacy_settings_heartbeat.removeAll()
        privacy_settings_stress_level.removeAll()
        privacy_settings_stress_privacy.removeAll()
    
        
        // helper
        selectedIndex = 0
        seeMoreIndex = 0
        selectedSendReactionIndex = 0
        tableView.reloadData()
        recordsArr.removeAll()
        loadFriendList()
    }
    
    //pagination
    private func loadFriendWithPagination(currentPage: Int) {
        
        let token = KeychainSwift().get("apiToken") ?? ""
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/connection?page=\(currentPage)") else { return }
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
 
                guard let jsonDic = userObject as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                guard let data = jsonDic["data"] as? [String: Any] else { return }
                guard let jsonData = data["data"] as? [[String: Any]] else { return }
                
//                print("jsonData", jsonData)
                
                if statusCode == 200 {
                    
                    let currentpage = data["current_page"] as? Int ?? 0
                    self.currentPage = currentpage
                    let nextPage = data["next_page_url"] as? String ?? ""
                    if nextPage.isEmpty {
                        self.didHaveNextPage = false
                    } else {
                        self.didHaveNextPage = true
                    }
                    
                    SVProgressHUD.dismiss(completion: {
                        for data in jsonData {
                            
                            let friendId = data["id"] as? Int ?? 0
                            let name = data["name"] as? String ?? ""
                            let profilePhoto = data["image"] as? String ?? ""
                            let is_online = data["is_online"] as? Int ?? 0
                            let profile_post = data["profile_post"] as? String ?? ""
                            
                            self.is_online.append(is_online)
                            self.friendUserName.append(name)
                            self.friendProfilePhoto.append(profilePhoto)
                            self.profPost.append(profile_post)
                            
                            print("count of profPost", self.profPost.count )
                            print("taena", self.profPost )
                            print("tanginame", self.friendUserName )
                            
                            // check if the friend is in private mode
                            
                            // privacy_setting arr
                            //                            if let privacy_settings = member["privacy_settings"] as? [String: Any] {
                            //                                this?.privacy_emotion.append(privacy_emotion)
                            //                                this?.privacy_heartbeat.append(privacy_heartbeat)
                            //                                this?.privacy_stress_level.append(privacy_stress_level)
                            //                            }
                            
//                            if let privacy_setting = data["privacy_settings"] as? [String: Any] {
//                                let privacy_emotion = privacy_setting["emotion"] as? Int ?? 0
//                                let privacy_heartbeat = privacy_setting["heartbeat"] as? Int ?? 0
//                                let privacy_stress_level = privacy_setting["stress_level"] as? Int ?? 0
//                                let privacy = privacy_setting["privacy"] as? Int ?? 0
//                                self.privacyFlag.append(privacy)
//                                self.privacy_emotion.append(privacy_emotion)
//                                self.privacy_heart_beat.append(privacy_heartbeat)
//                                self.privacy_stress_level.append(privacy_stress_level)
//                            }
                            
                            
                            guard let privacy_settings = data["privacy_settings"] as? [String: Any] else { return }
                            let privacy_settings_emotion = privacy_settings["emotion"] as? Int ?? 0
                            let privacy_settings_heartbeat = privacy_settings["heartbeat"] as? Int ?? 0
                            let privacy_settings_stress_level = privacy_settings["stress_level"] as? Int ?? 0
                            let privacy_settings_stress_privacy = privacy_settings["privacy"] as? Int ?? 0
                            self.privacy_settings_emotion.append(privacy_settings_emotion)
                            self.privacy_settings_heartbeat.append(privacy_settings_heartbeat)
                            self.privacy_settings_stress_level.append(privacy_settings_stress_level)
                            self.privacy_settings_stress_privacy.append(privacy_settings_stress_privacy)
                            
                            
                            // record
                            guard let record = data["record"] as? [String: Any] else { return }
                            self.recordsArr = record
                            let emotionpath = record["filepath"] as? String ?? ""
                            let bpm = record["heartbeat_count"] as? Int ?? 0
                            let emotionValue = record["emotion_value"] as? String ?? ""
                            let stressLevel = record["stress_level"] as? String ?? ""
                            let post_id = record["post_id"] as? Int ?? 0
                            let record_id = record["id"] as? Int ?? 0
                            let created_at = record["created_at"] as? String ?? ""
                            
                            self.postId.append(post_id)
                            self.recordId.append(record_id)
                            self.stressLevel.append(stressLevel)
                            self.emotionFilepath.append(emotionpath)
                            self.bpm.append(bpm)
                            self.emotionValue.append(emotionValue)
                            self.friendId.append(friendId)
                         //   self.dateCreated.append(created_at)
                           
                            
                        } // end of fucking loop
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss(completion: {
                                self.tableView.reloadData()
                            })
                        }
                    })
                    
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        self.tableView.reloadData()
                    })
                    
                } else {
                    SVProgressHUD.dismiss(completion: {
                        self.tableView.reloadData()
                        self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
        }}.resume()
    }
    
    private func setUpLanguage() {
        
        let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
        
        if selectedLanguage == "en" {
            Bundle.set(language: "en")
            print("friends en")
        } else if selectedLanguage == "ja" {
            Bundle.set(language: "ja")
            print("friends jap")
        } else {
            //
        }
    }
    

    private func loadFriendList() {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.friendList(keyword: "")).responseJSON { [weak this = self] response in
            switch response.result {
           
            case .success(let json):
                
                guard let jsonDic = json as? [String: Any] else { return }
                
                let statusCode = jsonDic["status"] as? Int ?? 0
                guard let data = jsonDic["data"] as? [String: Any] else { return }
                guard let jsonData = data["data"] as? [[String: Any]] else { return }
                print("jsonData", jsonData)
                if statusCode == 200 {
                    
                    let currentpage = data["current_page"] as? Int ?? 0
                    self.currentPage = currentpage
                    let nextPage = data["next_page_url"] as? String ?? ""
                    if nextPage.isEmpty {
                        self.didHaveNextPage = false
                    } else {
                        self.didHaveNextPage = true
                    }
                    
                    SVProgressHUD.dismiss(completion: {
                        for data in jsonData {
                            let friendId = data["id"] as? Int ?? 0
                            let name = data["name"] as? String ?? ""
                            let profilePhoto = data["image"] as? String ?? ""
                            let is_online = data["is_online"] as? Int ?? 0
                            let profile_post = data["profile_post"] as? String ?? ""
                            
                            this?.is_online.append(is_online)
                            this?.friendUserName.append(name)
                            this?.friendProfilePhoto.append(profilePhoto)
                            this?.profPost.append(profile_post)
                            
                            // check if the friend is in private mode
                            
                            // privacy_setting arr
//                            if let privacy_settings = member["privacy_settings"] as? [String: Any] {
//                                this?.privacy_emotion.append(privacy_emotion)
//                                this?.privacy_heartbeat.append(privacy_heartbeat)
//                                this?.privacy_stress_level.append(privacy_stress_level)
//                            }
                            
//                            if let privacy_setting = data["privacy_settings"] as? [String: Any] {
//                                let privacy_emotion = privacy_setting["emotion"] as? Int ?? 0
//                                let privacy_heartbeat = privacy_setting["heartbeat"] as? Int ?? 0
//                                let privacy_stress_level = privacy_setting["stress_level"] as? Int ?? 0
//                                let privacy = privacy_setting["privacy"] as? Int ?? 0
//                                this?.privacyFlag.append(privacy)
//                                this?.privacy_emotion.append(privacy_emotion)
//                                this?.privacy_heart_beat.append(privacy_heartbeat)
//                                this?.privacy_stress_level.append(privacy_stress_level)
//                            }
                            
                            // privacy_settings
                            guard let privacy_settings = data["privacy_settings"] as? [String: Any] else { return }
                            let privacy_settings_emotion = privacy_settings["emotion"] as? Int ?? 0
                            let privacy_settings_heartbeat = privacy_settings["heartbeat"] as? Int ?? 0
                            let privacy_settings_stress_level = privacy_settings["stress_level"] as? Int ?? 0
                            let privacy_settings_stress_privacy = privacy_settings["privacy"] as? Int ?? 0
                            self.privacy_settings_emotion.append(privacy_settings_emotion)
                            self.privacy_settings_heartbeat.append(privacy_settings_heartbeat)
                            self.privacy_settings_stress_level.append(privacy_settings_stress_level)
                            self.privacy_settings_stress_privacy.append(privacy_settings_stress_privacy)
                            
                            // record
                            guard let record = data["record"] as? [String: Any] else { return }
                            self.recordsArr = record
                            let emotionpath = record["filepath"] as? String ?? ""
                            let bpm = record["heartbeat_count"] as? Int ?? 0
                            let emotionValue = record["emotion_value"] as? String ?? ""
                            let stressLevel = record["stress_level"] as? String ?? ""
                            let post_id = record["post_id"] as? Int ?? 0
                            let record_id = record["id"] as? Int ?? 0
                            this?.postId.append(post_id)
                            this?.recordId.append(record_id)
                            this?.stressLevel.append(stressLevel)
                            this?.emotionFilepath.append(emotionpath)
                            this?.bpm.append(bpm)
                            this?.emotionValue.append(emotionValue)
                            this?.friendId.append(friendId)
                        }
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss(completion: {
                                this?.tableView.reloadData()
                            })
                        }
                    })
                } else {
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss(completion: {
                            this?.emptyState.isHidden = false
                        })
                    }
                }
                
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss(completion: {
                        this?.emptyStateImageView.image = UIImage(named: "No Internet")
                        this?.emptyStateLabelOne.text = "You're Offline"
                        this?.emptyStateLabelTwo.text = "Please connect to the internet and try again."
                    })
                }
                break
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
    }
}

extension FriendsTabViewController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsTabTableViewCell", for: indexPath) as? FriendsTabTableViewCell {
            setUpCell(cell: cell, indexPath: indexPath)
           
            return cell
        }
        
        return UITableViewCell()
    }
    
  
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendUserName.count
    //    return GlobalVariable.friendListArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        if privacy_settings_stress_privacy[indexPath.row] == 0 {
            self.performSegue(withIdentifier: "showFriendProfilePrivate", sender: self)
        } else if privacy_settings_emotion[indexPath.row] == 0 {
            self.performSegue(withIdentifier: "showFriendDashboardPrivateEmotion", sender: self)
        } else {
            self.performSegue(withIdentifier: "friendDashboard", sender: self)
        }
    }

    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
      //  getDarkmodeInCell(cell: cell as! FriendsTabTableViewCell)
        
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if self.didHaveNextPage == true {
            if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                currentPage+=1
                self.loadFriendWithPagination(currentPage: currentPage)
                
            }
            
        } else {
            self.tableView.tableFooterView?.isHidden = true
        }
    }
}

extension FriendsTabViewController {
    @objc private func seeMore(_ sender: UIButton) {
       
        let alert = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Visit Profile", comment: ""), style: .default , handler:{ [weak this = self] alert in

            if let indexpath = this?.tableView.indexPathForView(sender) {
                this?.seeMoreIndex = indexpath.row
                this?.didSelectSeeMore = true
            }
            
            this?.performSegue(withIdentifier: "friendDashboard", sender: self)
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Remove Friend", comment: ""), style: .default, handler:{ [weak this = self] alert in
            if let index = self.tableView.indexPathForView(sender) {
                this?.removeFriend(withId: self.friendId[index.row])
            }
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler:{ alert in
            
        }))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func sendReaction(_ sender: UIButton) {
        if let indexpath = self.tableView.indexPathForView(sender) {
            self.selectedSendReactionIndex = indexpath.row
            performSegue(withIdentifier: "showSendReaction", sender: self)
        }
    }
    
    private func setUpStressLevelHeart(stressLevel: String, cell: FriendsTabTableViewCell) {
        switch stressLevel {
        case "Low":
            cell.heartUIimageView.image = UIImage(named: "ic_heart_blue")
            break
        case "Mild":
            cell.heartUIimageView.image = UIImage(named: "ic_heart_green")
            break
        case "Moderate":
            cell.heartUIimageView.image = UIImage(named: "ic_heart_yellow")
            break
        case "Severe":
            cell.heartUIimageView.image = UIImage(named: "ic_heart_red")
            break
        default:
            cell.heartUIimageView.image = UIImage(named: "heart")
            break
        }
    }
    private func getDarkmodeInCell(cell: FriendsTabTableViewCell){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                cell.bgView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_FRIENDS)
                break
            case .dark:
                // dark mode detected
                cell.innerView.backgroundColor  = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)
                cell.usernameValue.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_FRIENDS)
                cell.bpmValue.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_FRIENDS)
                cell.emotionValue.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_FRIENDS)
                cell.seeMoreButton.tintColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_DEVICENAME)
                break
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    private func helper(cell: FriendsTabTableViewCell, indexPath: IndexPath, isOnline: Bool) {
        
        // temp lang to
        
//        cell.sendReactionButton.isEnabled = true
//        cell.sendReactionButton.addTarget(self, action: #selector(self.sendReaction(_:)), for: .touchUpInside)
        
        if isOnline == true {
            cell.isOnlineIndicator.image = UIImage(named: "ic_is_online_dot")
            cell.sendReactionButton.setTitleColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR), for: .normal)
            cell.sendReactionButton.isEnabled = true
            cell.sendReactionButton.addTarget(self, action: #selector(self.sendReaction(_:)), for: .touchUpInside)
        } else {
            cell.isOnlineIndicator.image = UIImage(named: "ic_is_offline_indicator")
            cell.imgSendReaction.tintColor = UIColor.gray
            cell.sendReactionButton.isEnabled = false
            darkmodeSendReaction(cell: cell)
        }
        
    }
    
    private func darkmodeSendReaction(cell: FriendsTabTableViewCell){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                cell.sendReactionButton.setTitleColor(UIColor.gray, for: .normal)
                break
            case .dark:
                // dark mode detected
                cell.sendReactionButton.setTitleColor(hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_BUTTON_FRIENDS), for: .normal)
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    private func setUpCurrentMood(cell: FriendsTabTableViewCell, emotion: String) {
        switch emotion {
        case "Excitement":
            cell.emotionValue.text = "\(NSLocalizedString("Excitement", comment: ""))"
        case "Happy":
            cell.emotionValue.text = "\(NSLocalizedString("Happy", comment: ""))"
        case "Zen":
            cell.emotionValue.text = "\(NSLocalizedString("Zen", comment: ""))"
        case "Pleasant":
            cell.emotionValue.text = "\(NSLocalizedString("Pleasant", comment: ""))"
        case "Calm":
            cell.emotionValue.text = "\(NSLocalizedString("Calm", comment: ""))"
        case "Unpleasant":
            cell.emotionValue.text = "\(NSLocalizedString("Unpleasant", comment: ""))"
        case "Confused":
            cell.emotionValue.text = "\(NSLocalizedString("Confused", comment: ""))"
        case "Challenged":
            cell.emotionValue.text = "\(NSLocalizedString("Challenged", comment: ""))"
        case "Tense":
            cell.emotionValue.text = "\(NSLocalizedString("Tense", comment: ""))"
        case "Sad":
            cell.emotionValue.text = "\(NSLocalizedString("Sad", comment: ""))"
        case "Anxious":
            cell.emotionValue.text = "\(NSLocalizedString("Anxious", comment: ""))"
        case "Loading":
            cell.emotionValue.text = "\(NSLocalizedString("Loading", comment: ""))"
        default:
            break
        }
    }
    
    private func setUpCell(cell: FriendsTabTableViewCell, indexPath: IndexPath) {
        
        //* checking privacy_settings */
        
        // privacy_emotion
        if privacy_settings_emotion[indexPath.row] == 0 {
            
            if is_online[indexPath.row] == 0 {
                cell.isOnlineIndicator.image = UIImage(named: "ic_is_offline_indicator")
                cell.sendReactionButton.setTitleColor(UIColor.gray, for: .normal)
                cell.sendReactionButton.isEnabled = false
            } else {
                cell.isOnlineIndicator.image = UIImage(named: "ic_is_online_dot")
                cell.sendReactionButton.setTitleColor(UIColor.gray, for: .normal)
                cell.sendReactionButton.isEnabled = false
            }
            
            
            cell.emotionValue.text = "\(NSLocalizedString("Private", comment: ""))"
            cell.emotionImageView.image = UIImage(named: "protection")
            
        } else {
            
            if is_online[indexPath.row] == 0 {
                self.helper(cell: cell, indexPath: indexPath, isOnline: false)
                cell.emotionValue.text = "\(NSLocalizedString("Inactive", comment: ""))"
            } else {
                self.helper(cell: cell, indexPath: indexPath, isOnline: true)
                self.setUpCurrentMood(cell: cell, emotion: emotionValue[indexPath.row].capitalized)
                cell.sendReactionButton.setTitleColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR), for: .normal)
                cell.imgSendReaction.setImageColor(color: hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
            }
            
            // setUP Images
            if let url = URL(string: "\(Constants.Routes.BASE_URL_RESOURCE)\(self.emotionFilepath[indexPath.row].replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: ".svg", with: ".png"))") {
                cell.emotionImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_no_emotion_value"), options: [], completed: nil)
            }
        }
        
        // privacy_heartBeat
        if self.privacy_settings_stress_level[indexPath.row] == 0 {
            cell.heartUIimageView.image = UIImage(named: "private-friends-heart")
            cell.bpmValue.text = NSLocalizedString("Private", comment: "")
            
//            if is_online[indexPath.row] == 0 {
//                self.helper(cell: cell, indexPath: indexPath, isOnline: false)
//            } else {
//                self.helper(cell: cell, indexPath: indexPath, isOnline: true)
//            }
            
        } else {
            
//            if is_online[indexPath.row] == 0 {
//                self.helper(cell: cell, indexPath: indexPath, isOnline: false)
//            } else {
//                self.helper(cell: cell, indexPath: indexPath, isOnline: true)
//            }
            
            if stressLevel[indexPath.row] == "" {
                cell.bpmValue.text = "\(NSLocalizedString("Stress Level", comment: "")) -"
            } else {
                //cell.bpmValue.text = "\(self.bpm[indexPath.row]) \(TranslationStr.bpm)"
                setUpStressLevel(cell: cell, indicator: self.stressLevel[indexPath.row])
            }
            
            if emotionValue[indexPath.row].isEmpty {
                cell.emotionValue.text = "No Mood Yet"
            }
            
            self.setUpStressLevelHeart(stressLevel: self.stressLevel[indexPath.row].capitalized, cell: cell)
        }
        
        // default and complete data
        cell.usernameValue.text = friendUserName[indexPath.row]
        
        if self.friendProfilePhoto[indexPath.row].isEmpty {
            cell.profilePhotoImageView.image = UIImage(named: "ic_empty_state_profile")
        }
        
        if let url2 = URL(string: self.friendProfilePhoto[indexPath.row].replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: ".svg", with: ".png")) {
            cell.profilePhotoImageView.sd_setImage(with: url2, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: .continueInBackground, completed: nil)
        }
        
        // setUP See More Option Button
        cell.seeMoreButton.addTarget(self, action: #selector(self.seeMore(_:)), for: .touchUpInside)
    }
    
    private func setUpStressLevel(cell: FriendsTabTableViewCell,indicator: String) {
        
        if indicator == "low" {
            cell.bpmValue.text = "\(NSLocalizedString("Stress Level", comment: "")): \(NSLocalizedString("Low", comment: ""))"
        } else if indicator == "Mild" {
            cell.bpmValue.text = "\(NSLocalizedString("Stress Level", comment: "")): \(NSLocalizedString("Mild", comment: ""))"
        } else if indicator == "Moderate" {
            cell.bpmValue.text = "\(NSLocalizedString("Stress Level", comment: "")): \(NSLocalizedString("Normal", comment: ""))"
        } else if indicator == "Severe" {
            cell.bpmValue.text = "\(NSLocalizedString("Stress Level", comment: "")): \(NSLocalizedString("High", comment: ""))"
        } else {
        
        }
    }
}

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
