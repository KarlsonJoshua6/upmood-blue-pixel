//
//  FriendsViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 14/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SDWebImage
import SVProgressHUD
import UIKit

extension FriendsViewController {
    @objc func keyboardWillAppear() {
        reload()
    }
    
    @objc func keyboardWillDisappear() {
        self.reload()
        self.tableVIew.isHidden = true
        self.searchBar.text = ""
    }
    
//    private func notificationListener() {
//
//        let type_id_notif = UserDefaults.standard.string(forKey: "type_id_notif")
//
//        if type_id_notif == "1" {
//            setUpSegmentControl(indexOf: 2)
//            segmentControlTab.selectedSegmentIndex = 2
//            UserDefaults.standard.set("", forKey: "type_id_notif")
//        } else if type_id_notif == "3" {
//            setUpSegmentControl(indexOf: 1)
//            segmentControlTab.selectedSegmentIndex = 1
//            UserDefaults.standard.set("", forKey: "type_id_notif")
//        }
//    }
}

class FriendsViewController: UIViewController {
    
    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var tableVIew: UITableView!
    @IBOutlet weak var segmentControlTab: UISegmentedControl!
    
    // MARK: Stored
    lazy var searchBar = UISearchBar(frame: .zero)
    let viewControllerIdentifiers = ["FriendsTab", "GroupTab", "PendingTab"]
    
    @IBOutlet weak var blankview: UIVisualEffectView!
    
    var searchFriends: String {
        return searchBar.text ?? ""
    }
    
    private var id = [Int]()
    private var name = [String]()
    private var image = [String]()
    private var email = [String]()
    private var connectionStatus = [String]()
    private var didHaveNextPage = true
    private var currentPage = 0
    private var didEndEditing = false
    private var selectedIndex = 0
    private var currentSearchKey: String!
    private var privacy_settings_emotion = [Int]()
    private var privacy_settings_heartbeat = [Int]()
    private var privacy_settings_stress_privacy = [Int]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSearchBar()
        setUpSegmentControl(indexOf: 0)
        tableVIew.delegate = self
        tableVIew.dataSource = self
        tableVIew.isHidden = true
        searchBar.returnKeyType = .done
        emptyStateView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // guest
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
            DispatchQueue.main.async {
                self.blankview.isHidden = false
                self.blankview.backgroundColor = UIColor.black.withAlphaComponent(0.75)
                self.blankview.isUserInteractionEnabled = false
                self.navigationController?.setNavigationBarHidden(true, animated: false)
            }
        } else {
            DispatchQueue.main.async {
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                self.blankview.isHidden = true
            }
        }
        
        self.searchBar.setShowsCancelButton(false, animated: true)
        self.emptyStateView.isHidden = true
        self.tableVIew.isHidden = true
        self.tableVIew.reloadData()
        self.reload()
        self.searchBar.endEditing(true)
        self.searchBar.text = ""
        navigationController?.navigationBar.isHidden = false
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FriendsViewController.timeChangedNotification), name: NSNotification.Name.NSSystemTimeZoneDidChange, object: nil)
//        notificationListener()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NSSystemTimeZoneDidChange, object: nil)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? FriendDashboardViewController {
            itemDetails.didClickSearchResult = true
            itemDetails.friendId = id[selectedIndex]
            itemDetails.connectionStatus = connectionStatus[selectedIndex]
            itemDetails.privacy_settings_heartbeat = privacy_settings_heartbeat[selectedIndex]
            itemDetails.privacy_settings_emotion = privacy_settings_emotion[selectedIndex]
//            itemDetails.postId = postId[selectedIndex]
//            itemDetails.recordId = recordId[selectedIndex]
//            itemDetails.privacy_emotion = privacy_emotion[selectedIndex]
//            itemDetails.is_online = is_online[selectedIndex]
        } else if let itemDetails2 = segue.destination as? FriendAccountPrivateViewController {
            itemDetails2.friendUsername = name[selectedIndex]
            itemDetails2.friendProfilePicStr = image[selectedIndex]
        } else if let itemDetails3 = segue.destination as? FriendDashboardPrivateEmotionViewController {
            itemDetails3.friendId = id[selectedIndex]
            itemDetails3.privacy_settings_heartbeat = privacy_settings_heartbeat[selectedIndex]
        }
    }
    
    @objc func timeChangedNotification(notification: NSNotification) {
     self.navigationController?.popToRootViewController(animated: false)
     let delegate = UIApplication.shared.delegate as? AppDelegate
     let storyboardName = "DashboardScreen"
     let storybaord = UIStoryboard(name: storyboardName, bundle: nil)
     delegate?.window?.rootViewController = storybaord.instantiateInitialViewController()
    }
    
    private func warningDialogForGuest() {
        let alert = UIAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString("You're on Guest mode. Please sign up to Upmood", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (alert) in
            self.tabBarController?.selectedIndex = 0
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func setUpSearchBar() {
        searchBar.delegate = self
        searchBar.placeholder = NSLocalizedString("Search", comment: "")
        searchBar.searchBarStyle = .prominent
        searchBar.tintColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        navigationItem.titleView = searchBar
    }
    
    @objc private func addFriend(_ sender: UIButton) {
        if let indexpath = self.tableVIew.indexPathForView(sender) {
            self.addFriend(withId: id[indexpath.row])
        }
    }
    
    @objc private func cancelFriendRequest(_ sender: UIButton) {
        if let indexpath = self.tableVIew.indexPathForView(sender) {
            self.cancelFriendRequest(withFriendId: id[indexpath.row])
        }
    }

    private func setUpSegmentControl(indexOf: Int) {
        let newController = storyboard!.instantiateViewController(withIdentifier: viewControllerIdentifiers[indexOf])
        let oldController = childViewControllers.last!
        
        oldController.willMove(toParentViewController: nil)
        addChildViewController(newController)
        newController.view.frame = oldController.view.frame
        
        transition(from: oldController, to: newController, duration: 0.25, options: .transitionCrossDissolve, animations: {
            // nothing needed here
        }, completion: { _ -> Void in
            oldController.removeFromParentViewController()
            newController.didMove(toParentViewController: self)
        })
    }
    
    @IBAction func selectOnSegmentControl(_ sender: UISegmentedControl) {
        let newController = storyboard!.instantiateViewController(withIdentifier: viewControllerIdentifiers[sender.selectedSegmentIndex])
        let oldController = childViewControllers.last!
        
        oldController.willMove(toParentViewController: nil)
        addChildViewController(newController)
        newController.view.frame = oldController.view.frame
        
        transition(from: oldController, to: newController, duration: 0.25, options: .transitionCrossDissolve, animations: {
        }, completion: { _ -> Void in
            oldController.removeFromParentViewController()
            newController.didMove(toParentViewController: self)
        })
    }
}

// MARK: - UISearchBarDelegate

extension FriendsViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
       
        if searchBar == self.searchBar {
            if searchBar.returnKeyType == .done {
//                self.didEndEditing = true
//                self.tableVIew.reloadData()
//                self.reload()
//                searchBar.setShowsCancelButton(false, animated: true)
//                searchBar.resignFirstResponder()
            }
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.tableVIew.reloadData()
        self.reload()
        
        // hide clear button
        guard let firstSubview = searchBar.subviews.first else { return }
        firstSubview.subviews.forEach { ($0 as? UITextField)?.clearButtonMode = .never }
        searchBar.setShowsCancelButton(true, animated: true)
        tableVIew.isHidden = true
        emptyStateView.isHidden = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.searchBar.endEditing(true)
        self.tableVIew.reloadData()
        self.reload()
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        self.tableVIew.isHidden = false
        self.tableVIew.reloadData()
        self.reload()
        
        let typeCasteToStringFirst = searchBar.text as NSString?
        let newString = typeCasteToStringFirst?.replacingCharacters(in: range, with: text)
        let finalSearchString = newString ?? ""
        self.loadSearchFriendList(withKeyword: finalSearchString.lowercased())
        self.currentSearchKey = finalSearchString.lowercased()
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.emptyStateView.isHidden = true
        self.tableVIew.isHidden = true
        self.tableVIew.reloadData()
        self.reload()
        self.searchBar.endEditing(true)
        self.searchBar.text = ""
    }
}

extension FriendsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SearchFriendTableViewCell", for: indexPath) as? SearchFriendTableViewCell {
            setUp(cell: cell, indexpath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return name.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        let currentOffset = scrollView.contentOffset.y
//        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
//
//        // Change 10.0 to adjust the distance from bottom
//        if maximumOffset - currentOffset <= 10.0 {
//            if self.loadingData {
//                let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//                spinner.startAnimating()
//                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableVIew.bounds.width, height: CGFloat(44))
//
//                self.tableVIew.tableFooterView = spinner
//                self.tableVIew.tableFooterView?.isHidden = false
//                self.tableVIew.reloadData()
//
//                currentPage+=1
//                self.loadFriendWithPagination(currentPage: self.currentPage)
//            } else {
//                self.tableVIew.tableFooterView?.isHidden = true
//            }
//        }
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if self.didHaveNextPage {
            if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                currentPage+=1
                self.loadFriendWithPagination(currentPage: self.currentPage)
            }
            
        } else {
            self.tableVIew.tableFooterView?.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        if connectionStatus[indexPath.row] == "Connect" || connectionStatus[indexPath.row] == "For Confirmation" || connectionStatus[indexPath.row] == "Pending" {
            performSegue(withIdentifier: "privateProfile", sender: self)
        } else {
            if privacy_settings_stress_privacy[indexPath.row] == 0 {
                performSegue(withIdentifier: "privateProfile", sender: self)
            } else {
                
                if privacy_settings_emotion[indexPath.row] == 0 {
                    performSegue(withIdentifier: "privateEmotionProfile", sender: self)
                } else {
                    performSegue(withIdentifier: "showFriendDashboard", sender: self)
                }
            }
        }
    }
}

// MARK: - Network request

extension FriendsViewController {
    
    private func loadFriendWithPagination(currentPage: Int) {
        
        let token = KeychainSwift().get("apiToken") ?? ""
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/connection?page=\(currentPage)") else { return }
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                guard let JSON = userObject as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let dataArray = JSON["data"] as? [String: Any] else { return }
                guard let JSONDataArray = dataArray["data"] as? [[String: Any]] else { return }
                
                if statusCode == 200 {
                    
                    // LOGIC HERE
                    
                        let nextPage = dataArray["next_page_url"] as? String ?? ""
                        if nextPage.isEmpty {
                            self.didHaveNextPage = false
                        }
                    
                        SVProgressHUD.dismiss(completion: {
                            for jsondata in JSONDataArray {
                                let id = jsondata["id"] as? Int ?? 0
                                let username = jsondata["name"] as? String ?? ""
                                let imageString = jsondata["image"] as? String ?? ""
                                let email = jsondata["email"] as? String ?? ""
                                let connection_status = jsondata["connection_status"] as? String ?? ""
                                self.id.append(id)
                                self.name.append(username)
                                self.image.append(imageString)
                                self.email.append(email)
                                self.connectionStatus.append(connection_status)
                            }// end of fucking loop
                        })
                        
                        // operations
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss(completion: {
                                self.emptyStateView.isHidden = true
                                self.tableVIew.reloadData()
                            })
                        }
                    
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
//                        self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
        }}.resume()
    }
    
    
    private func cancelFriendRequest(withFriendId: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.cancelFriendRequest(withFriendId: withFriendId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        print("cancelFriendRequest : \(json)")
                        this?.reload()
//                        this?.searchBar.endEditing(true)
                        this?.loadSearchFriendList(withKeyword: self.currentSearchKey)
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.reload()
//                        this?.searchBar.endEditing(true)
                        this?.loadSearchFriendList(withKeyword: self.currentSearchKey)
//                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No User Found.")
                    })
                } else {
                    print("error response: \(JSON)")
//                    SVProgressHUD.dismiss(completion: {
//                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
//                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func addFriend(withId: Int) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.addFriend(withId: withId)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        print("addFriend: \(json)")
                        this?.reload()
//                        this?.searchBar.endEditing(true)
                        this?.loadSearchFriendList(withKeyword: self.currentSearchKey)
                    })
                    
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.reload()
                        this?.loadSearchFriendList(withKeyword: self.currentSearchKey)
//                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No User Found.")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func loadSearchFriendList(withKeyword: String) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.searchOnFriendList(withKeyword: withKeyword)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let dataArray = JSON["data"] as? [String: Any] else { return }
                guard let JSONDataArray = dataArray["data"] as? [[String: Any]] else { return }
                
                if statusCode == 200 {
                    SVProgressHUD.dismiss(completion: {
                        this?.reload()
                        for jsondata in JSONDataArray {
                            let id = jsondata["id"] as? Int ?? 0
                            let username = jsondata["name"] as? String ?? ""
                            let imageString = jsondata["image"] as? String ?? ""
                            let email = jsondata["email"] as? String ?? ""
                            let connection_status = jsondata["connection_status"] as? String ?? ""
                            this?.id.append(id)
                            this?.name.append(username)
                            this?.image.append(imageString)
                            this?.email.append(email)
                            this?.connectionStatus.append(connection_status)
                            
                            if let privacy_settings = jsondata["privacy_settings"] as? [String: Any]  {
                                let privacy_settings_privacy = privacy_settings["privacy"] as? Int ?? 0
                                let privacy_settings_emotion = privacy_settings["emotion"] as? Int ?? 0
                                let privacy_settings_heartbeat = privacy_settings["heartbeat"] as? Int ?? 0
                                this?.privacy_settings_stress_privacy.append(privacy_settings_privacy)
                                this?.privacy_settings_emotion.append(privacy_settings_emotion)
                                this?.privacy_settings_heartbeat.append(privacy_settings_heartbeat)
                            }
                            
                        }// end of fucking loop
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss(completion: {
                                this?.emptyStateView.isHidden = true
                                this?.tableVIew.reloadData()
                            })
                        }
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        if !self.didEndEditing {
                            this?.reload()
                            this?.emptyStateView.isHidden = false
                            this?.tableVIew.reloadData()
                        } else {
                            this?.emptyStateView.isHidden = true
                            this?.didEndEditing = false
                        }
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
//                        if !self.didEndEditing {
//                            this?.reload()
//                            this?.emptyStateView.isHidden = false
//                            this?.tableVIew.reloadData()
//                        } else {
//                            this?.emptyStateView.isHidden = true
//                            this?.didEndEditing = false
//                        }
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func setUp(cell: SearchFriendTableViewCell, indexpath: IndexPath) {

        // set up connection status
        cell.addFriendBtutton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        switch connectionStatus[indexpath.row] {
        case "Connect":
            cell.addFriendBtutton.isEnabled = true
            cell.addFriendBtutton.setTitle(NSLocalizedString("Add Friend", comment: ""), for: .normal)
            cell.addFriendBtutton.addTarget(self, action: #selector(self.addFriend(_:)), for: .touchUpInside)
            break
        case "Friend":
            cell.addFriendBtutton.isEnabled = false
            cell.addFriendBtutton.setTitle("Connected", for: .normal)
            break
        case "For Confirmation":
            cell.addFriendBtutton.isEnabled = false
            cell.addFriendBtutton.setTitle(NSLocalizedString("For Confirmation", comment: ""), for: .normal)
            break
        case "Pending":
            cell.addFriendBtutton.isEnabled = true
            cell.addFriendBtutton.addTarget(self, action: #selector(self.cancelFriendRequest(_:)), for: .touchUpInside)
            cell.addFriendBtutton.setTitle(NSLocalizedString("Cancel Request", comment: ""), for: .normal)
        default:
            break
        }
        
        // set up image
        
        if self.image[indexpath.row].isEmpty {
            cell.friendProfilePhotoImageView.image = UIImage(named: "ic_empty_state_profile")
        }
        
        if let url = URL(string: "\(self.image[indexpath.row])") {
            cell.friendProfilePhotoImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], completed: nil)
        }
        
        // set up texts
        
        cell.friendUsernameLabel.text = name[indexpath.row]
        //cell.email.text = email[indexpath.row]
    }
    
    private func reload() {
        self.id.removeAll()
        self.name.removeAll()
        self.image.removeAll()
        self.email.removeAll()
        self.connectionStatus.removeAll()
        self.privacy_settings_emotion.removeAll()
        self.privacy_settings_heartbeat.removeAll()
        self.privacy_settings_stress_privacy.removeAll()
        self.tableVIew.reloadData()
        self.emptyStateView.isHidden = true
    }
}
