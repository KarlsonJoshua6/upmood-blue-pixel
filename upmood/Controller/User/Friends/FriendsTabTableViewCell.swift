//
//  FriendsTabTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 14/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//
	
import UIKit

class FriendsTabTableViewCell: UITableViewCell {

    @IBOutlet weak var isOnlineIndicator: UIImageView!
    @IBOutlet weak var sendReactionButton: UIButton!
    @IBOutlet weak var seeMoreButton: UIButton!
    @IBOutlet weak var heartUIimageView: UIImageView!
    @IBOutlet weak var profilePhotoImageView: UIImageView!
    @IBOutlet weak var emotionImageView: UIImageView!
    @IBOutlet weak var emotionValue: UILabel!
    @IBOutlet weak var bpmValue: UILabel!
    @IBOutlet weak var usernameValue: UILabel!
    @IBOutlet weak var imgSendReaction: UIImageView!
    
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        getDarkMode()
       
        // Initialization code
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?)
    {
        super.traitCollectionDidChange(previousTraitCollection)
        getDarkMode()
    }
    func getDarkMode(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                bgView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_FRIENDS)
                
                innerView.backgroundColor  = UIColor.white
                usernameValue.textColor = UIColor.black
                bpmValue.textColor = UIColor.black
                emotionValue.textColor = UIColor.darkGray
                seeMoreButton.tintColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_DEVICENAME)
                break
            case .dark:
                // dark mode detected
                innerView.backgroundColor  = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)
                usernameValue.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_FRIENDS)
                bpmValue.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_FRIENDS)
                emotionValue.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_FRIENDS)
                seeMoreButton.tintColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_DEVICENAME)
               bgView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_COLOR_DARKMODE)
                
                break
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   

}
