//
//  SelectLanguageInSplashViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 02/06/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class SelectLanguageInSplashViewController: UIViewController {
    
    @IBOutlet weak var imgEnglish: UIImageView!
    @IBOutlet weak var imgJapanese: UIImageView!
    @IBOutlet weak var imgChinise: UIImageView!
    var senderTagButton = 1;
    @IBOutlet weak var viewBackground: UIView!
    
    @IBOutlet weak var pg1Background: UIProgressView!
    @IBOutlet weak var pg2Background: UIProgressView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getLanguange()
        getDarkMode()
        
        
    }
    
    private func getDarkMode(){
        if #available(iOS 12.0, *) {
            switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                // light mode detected
                print("light mode detected")
                getBorderView(modeiOS: "light")
            case .dark:
                // dark mode detected
                print("dark mode detected")
                
                navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: Constants.Color.GRAY_COLOR)
                viewBackground.backgroundColor = hexStringToUIColor(hex: Constants.Color.GRAY_COLOR)
                getBorderView(modeiOS: "dark")
            }
        } else {
            // Fallback on earlier versions
        }
    }
    private func getBorderView(modeiOS: String){
        viewBackground.layer.borderWidth = 2
        switch modeiOS{
        case "light":
            viewBackground.layer.borderColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW).cgColor
            
            break
        case "dark":
            viewBackground.layer.borderColor = hexStringToUIColor(hex: Constants.Color.GRAY_COLOR).cgColor
            pg1Background.tintColor = UIColor.white
            pg2Background.tintColor = UIColor.white
            break
        default: break
        }
           }
    private func getLanguange(){
         let lang = UserDefaults.standard.string(forKey: "selectedLanguage")
        
        if lang == "en" {
            imgEnglish.image =  UIImage(named: "radio")
            imgEnglish.setImageColor(color: hexStringToUIColor(hex: Constants.Color.NEW_COLOR))
            
            imgJapanese.image =  UIImage(named: "circle-icon")
            imgChinise.image =  UIImage(named: "circle-icon")
            
            senderTagButton = 1
        }else if lang == "ja"{
            imgJapanese.image =  UIImage(named: "radio")
            imgJapanese.setImageColor(color: hexStringToUIColor(hex: Constants.Color.NEW_COLOR))
            
            imgEnglish.image =  UIImage(named: "circle-icon")
            imgChinise.image =  UIImage(named: "circle-icon")
            
            senderTagButton = 2
        } else {
            
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func btnSaveLang(_ sender: UIBarButtonItem) {
        switch senderTagButton {
        case 1:
            Bundle.set(language: "en")
            UserDefaults.standard.set("en", forKey: "selectedLanguage")
            _ = navigationController?.popToRootViewController(animated: true)
            let storyboard = UIStoryboard.init(name: "SpashScreen", bundle: nil)
            UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
            break
        case 2:
            Bundle.set(language: "ja")
            UserDefaults.standard.set("ja", forKey: "selectedLanguage")
            _ = navigationController?.popToRootViewController(animated: true)
            let storyboard = UIStoryboard.init(name: "SpashScreen", bundle: nil)
             UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
            break
        case 3:
           presentDismissableAlertController(title: "Coming soon...", message: nil)
            break
        default:
            break
        }
    }
    
    @IBAction func chooseLanguage(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            
            imgEnglish.image =  UIImage(named: "radio")
            imgEnglish.setImageColor(color: hexStringToUIColor(hex: Constants.Color.NEW_COLOR))
            
            imgJapanese.image =  UIImage(named: "circle-icon")
            imgChinise.image =  UIImage(named: "circle-icon")
            
            senderTagButton = 1
            

            break
        case 2:
            
            imgJapanese.image =  UIImage(named: "radio")
            imgJapanese.setImageColor(color: hexStringToUIColor(hex: Constants.Color.NEW_COLOR))
            
            imgEnglish.image =  UIImage(named: "circle-icon")
            imgChinise.image =  UIImage(named: "circle-icon")
            
            senderTagButton = 2
            
            break

        case 3:
            
            imgChinise.image =  UIImage(named: "radio")
            imgChinise.setImageColor(color: hexStringToUIColor(hex: Constants.Color.NEW_COLOR))
            
            imgEnglish.image =  UIImage(named: "circle-icon")
            imgJapanese.image =  UIImage(named: "circle-icon")
            
            senderTagButton = 3
            
            break
        default: break
        }
    }
}

