//
//  SplashScreenViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 09/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIPageViewController {
    
    var interval = 3
    var currentIndex = 0
    lazy var subViewControllers: [UIViewController] = {
        return [
            UIStoryboard(name: "SpashScreen", bundle: nil).instantiateViewController(withIdentifier: "SplashScreenOneViewController"),
            UIStoryboard(name: "SpashScreen", bundle: nil).instantiateViewController(withIdentifier: "SplashScreenTwoViewController"),
            UIStoryboard(name: "SpashScreen", bundle: nil).instantiateViewController(withIdentifier: "SplashScreenThirdViewController"),
            UIStoryboard(name: "SpashScreen", bundle: nil).instantiateViewController(withIdentifier: "SplashScreenFourthViewController")
            ]
    }()
    var timer = Timer()
    var seconds = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        setViewControllers([subViewControllers[0]], direction: .forward, animated: true, completion: nil)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.runTimer), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    @objc private func runTimer() {
        self.seconds += 1
        switch seconds {
        case 3:
            setViewControllers([subViewControllers[1]], direction: .forward, animated: true, completion: nil)
        case 6:
            setViewControllers([subViewControllers[2]], direction: .forward, animated: true, completion: nil)
        case 9:
            setViewControllers([subViewControllers[3]], direction: .forward, animated: true, completion: nil)
        case 12:
            setViewControllers([subViewControllers[0]], direction: .forward, animated: true, completion: nil)
        case 15:
            seconds = 0
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.runTimer), userInfo: nil, repeats: true)
        default:
            break
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
}

extension SplashScreenViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = subViewControllers.index(of: viewController) ?? 0
        self.currentIndex = currentIndex
        if (currentIndex <= 0) {
            return nil
        }
        
        seconds = 0
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.runTimer), userInfo: nil, repeats: true)
        
        return subViewControllers[currentIndex-1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = subViewControllers.index(of: viewController) ?? 0
        self.currentIndex = currentIndex
        if (currentIndex >= subViewControllers.count-1) {
            return nil
        }
        
        seconds = 0
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.runTimer), userInfo: nil, repeats: true)
        
        return subViewControllers[currentIndex+1]
    }
}


