//
//  PersonRecord.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 15/11/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import SwiftyJSON

struct PersonRecord {
    let user_id: Int
    let heartbeat_count: String
    let emotion_set: String
    let emotion_value: String
    let stress_level: String
    let type: String
    let longitude: String
    let latitude: String
    let ppi: String
    let total_ppi: String
    let created_at: String
    let timezone: String
    let post: String
    let stress_level_value: String
}

extension PersonRecord {
    init?(json: [String: Any]) {
        guard let user_id = json["user_id"] as? Int,
            let heartbeat_count = json["heartbeat_count"] as? String,
            let emotion_set = json["emotion_set"] as? String,
            let emotion_value = json["emotion_value"] as? String,
            let stress_level = json["stress_level"] as? String,
            let type = json["type"] as? String,
            let longitude = json["longitude"] as? String,
            let latitude = json["latitude"] as? String,
            let ppi = json["ppi"] as? String,
            let total_ppi = json["total_ppi"] as? String,
            let created_at = json["created_at"] as? String,
            let timezone = json["timezone"] as? String,
            let post = json["post"] as? String,
            let stress_level_value = json["stress_level_value"] as? String
            else {
                return nil
        }
        
        self.user_id = user_id
        self.heartbeat_count = heartbeat_count
        self.emotion_set = emotion_set
        self.emotion_value = emotion_value
        self.stress_level = stress_level
        self.type = type
        self.longitude = longitude
        self.latitude = latitude
        self.ppi = ppi
        self.total_ppi = total_ppi
        self.created_at = created_at
        self.timezone = timezone
        self.post = post
        self.stress_level_value = stress_level_value
    }
}
