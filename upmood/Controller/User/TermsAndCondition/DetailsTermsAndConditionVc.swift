//
//  DetailsTermsAndConditionVc.swift
//  upmood
//
//  Created by Arvin Lemuel M Cabunoc on 17/03/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit
import WebKit

class DetailsTermsAndConditionVc: UIViewController {


    @IBOutlet weak var webView: WKWebView!
    
    @IBOutlet weak var btnClose: UIButton!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setButtonBackColor()
        loadWebview()
        setUserInterfaceStyleLight(self: self)
    }
    private func loadWebview(){
        let fontName =  "System"
        let fontSize = 50
        let fontSetting = "<span style=\"font-family: \(fontName);font-size: \(fontSize)\"</span>"
        
        webView.loadHTMLString(fontSetting + Constants.ContentOfTerms, baseURL: nil)
    }
    
    private func setButtonBackColor(){
        
        btnClose.setImage(UIImage(named: "ic_back"), for: .normal) // You can set image direct from Storyboard
        btnClose.imageView?.tintColor = hexStringToUIColor(hex: Constants.Color.NEW_COLOR)
        
    }
    @IBAction func btnClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnTerms(_ sender: Any) {
        openTerms()
    }
    private func openTerms(){
        
        Constants.ContentOfTerms.isEmpty
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "TermsAndConditionScreen", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TermsAndCondition") as! TermsAndConditionVc
        self.present(nextViewController, animated:false, completion:nil)
    }

}
