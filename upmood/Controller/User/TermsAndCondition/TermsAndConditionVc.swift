//
//  TermsAndConditionVc.swift
//  upmood
//
//  Created by Arvin Lemuel M Cabunoc on 17/03/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class TermsAndConditionVc: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getArrayOfTermsAndCondi()
        tableView.delegate   = self
        tableView.dataSource = self
        setUserInterfaceStyleLight(self: self)
    }
    private func getArrayOfTermsAndCondi(){
       
    }
    @IBAction func btnDisagree(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "SpashScreen", bundle: nil)
        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
        
        NotificationCenter.default.post(name: Notification.Name("toast1"), object: nil)
        UserDefaults.standard.set(0, forKey: "didAgreeTermsAndCondi")
    }
    @IBAction func btnAgree(_ sender: Any) {
        instantiateViewController(withStoryboardName: "PrivacyAndPolicy", viewControllerId: "privacyAndPolicyVc")
        UserDefaults.standard.set(1, forKey: "didAgreeTermsAndCondi")
    }
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 200, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    private func instantiateViewController(withStoryboardName: String, viewControllerId: String) {
        
        if viewControllerId == "privacyAndPolicyVc"{
            let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! PrivacyAndPolicyVc
                      
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: false, completion: nil)
        }else{
            let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! DetailsTermsAndConditionVc
                       
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: false, completion: nil)
        }
            
        
    }
}
// MARK: - CellIdentifier

let tableCell = "termsAndConditionCell"

 // MARK: - TermsAndConditionCell

class TermsAndConditionCell: UITableViewCell{
    
    @IBOutlet weak var lblTitleOfTerms: UILabel!
    
}
// MARK: - UITableViewDelegate, UITableViewDataSource

extension TermsAndConditionVc: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ParagraphConstants.termsAndConditionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: tableCell, for: indexPath) as! TermsAndConditionCell
        
        cell.lblTitleOfTerms.text = ParagraphConstants.termsAndConditionArray[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        Constants.ContentOfTerms = ParagraphConstants.contentArray[indexPath.row]
        print("tryIndex", Constants.ContentOfTerms)
            
        instantiateViewController(withStoryboardName: "TermsAndConditionScreen", viewControllerId: "DetailsTerm")
   }    
}
