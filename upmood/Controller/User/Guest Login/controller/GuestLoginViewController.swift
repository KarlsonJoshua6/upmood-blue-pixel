//
//  GuestLoginViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 05/11/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SkyFloatingLabelTextField
import UIKit

class GuestLoginViewController: UIViewController {

    @IBOutlet var viewModel: GuestLoginViewModel!
    @IBOutlet var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet var nameTextField: SkyFloatingLabelTextField!
    
    var email: String {
        return emailTextField.text ?? ""
    }
    
    var name: String {
        return nameTextField.text ?? "" 
    }
    
    var allow = true
    var isEmailAlreadySignedUp = false
    var isGuest = 0
    var isLimit = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.delegate = self
        emailTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        nameTextField.becomeFirstResponder()
        setUpTextField()
    }
    
    private func setUpTextField() {
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .light {
                emailTextField.lineColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                nameTextField.lineColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                emailTextField.placeholderColor = .darkGray
                nameTextField.placeholderColor = .darkGray
            } else {
                emailTextField.lineColor = .white
                nameTextField.lineColor = .white
                emailTextField.placeholderColor = .white
                nameTextField.placeholderColor = .white
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setUpTextField()
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func next(_ sender: UIButton) {
        if email.isEmpty {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString(Constants.MessageDialog.INCOMPLETE_FORM, comment: ""))
        } else if !email.isValidEmail() {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: Constants.MessageDialog.INVALID_EMAIL_FORMAT)
        } else if name.isEmpty {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString(Constants.MessageDialog.INCOMPLETE_FORM, comment: ""))
        } else {
            checkEmail(email: email)
        }
    }
}

extension GuestLoginViewController {
    
    private func guestLogin() {
       
        // login
        viewModel.loginAsGuest(withEmail: email, name: name) {
            let keychain = KeychainSwift()
            keychain.set(self.viewModel.getApiToken(), forKey: "apiToken")
            UserDefaults.standard.set(self.viewModel.getUserName(), forKey: "getGuestUserName")
            UserDefaults.standard.set(self.viewModel.getApiToken(), forKey: "fetchUserToken")
            UserDefaults.standard.set(self.viewModel.getBasicEmoji(), forKey: "currentTheme")
            UserDefaults.standard.set(self.viewModel.getUserId(), forKey: "fetchUserId")
            UserDefaults.standard.set(1, forKey: "fetchAutoReconnect")
            UserDefaults.standard.set(1, forKey: "isGuest")
            Constants.didUserLogin = true
            
            DispatchQueue.main.async {
        
                let storyBoard: UIStoryboard = UIStoryboard(name: "TermsAndConditionScreen", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "TermsAndCondition") as! TermsAndConditionVc
                self.present(newViewController, animated: true, completion: nil)
                
                Constants.condiPriStr = "guest"
            }
        }
    }
    
    private func checkEmail(email: String) {
        Alamofire.request(APIClient.checkEmail(email: email)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let data = json as? [String: Any] else { return }
                let statusCode = data["status"] as? Int ?? 0
                
                if statusCode == 200 {
                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Email already in use.")
                } else if statusCode == 204 {
                    self.guestLogin()
                } else if statusCode == 419 {
                    self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Invalid email address.")
                } else {
                    debugPrint("error checkEmail: \(data)")
                }
                
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
}

extension GuestLoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.nameTextField {
            self.emailTextField.becomeFirstResponder()
        }
        
        if textField.returnKeyType == UIReturnKeyType.done {
            self.emailTextField.resignFirstResponder()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.count == 0 {
            return true
        }
        
        let newLength = nameTextField.text?.count ?? 0 + string.count - range.length
        
        if newLength > 100 {
            isLimit = false
        } else {
            isLimit = true
        }

        return isLimit
    }
    
    private func limitCharacterDialog() {
        let messageTxt = "Name is too long."
        let alert = UIAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: messageTxt, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (alert) in
            self.isLimit = true
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
