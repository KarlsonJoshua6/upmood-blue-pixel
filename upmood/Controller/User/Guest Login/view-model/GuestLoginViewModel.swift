//
//  GuestLoginViewModel.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 05/11/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class GuestLoginViewModel: NSObject {
    
    @IBOutlet weak var service: GuestLoginService!
    
    var guestData: NSDictionary?
    
    func loginAsGuest(withEmail: String, name: String, completion: @escaping () -> ()) {
        service.loginAsGuest(email: withEmail, name: name) { [weak self] dict in
            self?.guestData = dict
            completion()
        }
    }
    
    func getUserId() -> Int {
        return guestData?.value(forKeyPath: "id") as? Int ?? 0
    }
    
    func getUserEmail() -> String {
        return guestData?.value(forKeyPath: "email") as? String ?? ""
    }
    
    func getUserName() -> String {
        return guestData?.value(forKeyPath: "name") as? String ?? ""
    }
    
    func getIsGuest() -> Int {
        return guestData?.value(forKeyPath: "is_guest") as? Int ?? 0
    }
    
    func getApiToken() -> String {
        return guestData?.value(forKeyPath: "api_token") as? String ?? ""
    }
    
    func getBasicEmoji() -> String {
        return guestData?.value(forKeyPath: "basic_emoji_set") as? String ?? ""
    }
}
