//
//  GuestLoginService.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 05/11/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import UIKit

class GuestLoginService: NSObject {
    
    func loginAsGuest(email: String, name: String, completion: @escaping (_ session: NSDictionary?) -> ()) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        Alamofire.request(Service.loginAsGuest(withEmail: email, name: name)).responseJSON { response  in
            switch response.result {
            case .success(let json):
                
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0

                switch statusCode {
                case 200:
                    if let jsonDict = JSON["data"] as? NSDictionary {
                        completion(jsonDict)
                        return
                    }
                    break
                case 204:
                    
                    var errorData = [String: Any]()
                    errorData.updateValue(204, forKey: "status")
                    
                    if let emptyDict = errorData as? NSDictionary {
                        completion(emptyDict)
                        return
                    }
                    
                    break
                case 422:
                    if let errorDict = JSON["error"] as? NSDictionary {
                        completion(errorDict)
                        return
                    }
                    
                    break
                default:break
                }
                
                break
            case .failure(let error):
                var errorData = [String: Any]()
                errorData.updateValue(error.localizedDescription, forKey: "status_failure")
                
//                if let emptyDict = errorData as? NSDictionary {
//                    completion([emptyDict])
//                    return
//                }
                
                break
            }
        }
    }
}
