//
//  PrivacyAndPolicyVc.swift
//  upmood
//
//  Created by jomarie madijanon on 18/03/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SVProgressHUD
import UIKit

class PrivacyAndPolicyVc: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let storyboardId = ""
    let dialogVc = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate   = self
        tableView.dataSource = self
        setUserInterfaceStyleLight(self: self)
    }
    
    @IBAction func btnDisagree(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "SpashScreen", bundle: nil)
        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
        
         NotificationCenter.default.post(name: Notification.Name("toast2"), object: nil)
         UserDefaults.standard.set(0, forKey: "didAgreeTermsAndCondi")
                
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "firstName")
        UserDefaults.standard.removeObject(forKey: "lastName")
    }
    
    @IBAction func btnAgree(_ sender: Any) {
        UserDefaults.standard.set(1, forKey: "didAgreeTermsAndCondi")
        if  Constants.condiPriStr == "guest" {
            let delegate = UIApplication.shared.delegate as? AppDelegate
            let storyboardName = "DashboardScreen"
            let storybaord = UIStoryboard(name: storyboardName, bundle: nil)
            delegate?.window?.rootViewController = storybaord.instantiateInitialViewController()
        } else if  Constants.condiPriStr == "signUp" {
            instantiateViewController(withStoryboardName: "CreateAccountScreen", viewControllerId: "pageViewController")
        } else {
            let delegate = UIApplication.shared.delegate as? AppDelegate
            let storyboardName = "DashboardScreen"
            let storybaord = UIStoryboard(name: storyboardName, bundle: nil)
            delegate?.window?.rootViewController = storybaord.instantiateInitialViewController()
        }
    }
    
    private func instantiateViewController(withStoryboardName: String, viewControllerId: String) {
        
        if viewControllerId == "DetailsPolicy"{
            let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! DetailsPrivacyAndPolicyVc
                                  
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: false, completion: nil)
        }
       else{
            let storyboard = UIStoryboard(name: withStoryboardName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId) as! PageViewController
                                             
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: false, completion: nil)
        }
           
  
            
        
    }
}
// MARK: - Cell Indentifier

let privacyCell = "privacyAndPolicyCell"

// MARK: - Privacy Cell
class PrivacyAndPolicyCell: UITableViewCell{
    @IBOutlet weak var lblTitleOfPrivacy: UILabel!
}

extension PrivacyAndPolicyVc: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ParagraphConstants.privacyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: privacyCell, for: indexPath) as! PrivacyAndPolicyCell
        cell.lblTitleOfPrivacy.text = ParagraphConstants.privacyArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Constants.contentOfPrivacy = ParagraphConstants.contentPrivacyArray[indexPath.row]
            
         instantiateViewController(withStoryboardName: "PrivacyAndPolicy", viewControllerId: "DetailsPolicy")
    }
    
    
}
