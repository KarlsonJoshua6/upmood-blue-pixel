//
//  DetailsPrivacyAndPolicyVc.swift
//  upmood
//
//  Created by Arvin Lemuel M Cabunoc on 18/03/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit
import WebKit

class DetailsPrivacyAndPolicyVc: BaseViewController {

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        webViewLoad()
        setButtonBackColor()
        setUserInterfaceStyleLight(self: self)
    }
    private func webViewLoad(){
        let fontName =  "System"
        let fontSize = 50
        let fontSetting = "<span style=\"font-family: \(fontName);font-size: \(fontSize)\"</span>"
        
        webView.loadHTMLString(fontSetting + Constants.contentOfPrivacy, baseURL: nil)
    }
    private func setButtonBackColor(){
        btnClose.setImage(UIImage(named: "ic_back"), for: .normal) // You can set image direct from Storyboard
        btnClose.imageView?.tintColor = hexStringToUIColor(hex: Constants.Color.NEW_COLOR)
    }

    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
