//
//  CreateStatusViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 31/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import UIKit

class CreateStatusViewController: UIViewController {

    @IBOutlet weak var statusTextView: UITextView!
    @IBOutlet weak var textCount: UILabel!
    @IBOutlet weak var suggestionCollectionView: UICollectionView!
    @IBOutlet var accesoryView: UIView!
    @IBOutlet weak var postButton: UIBarButtonItem!
    
    var allow = true
    
    var status: String {
        return statusTextView.text ?? ""
    }
    
    var suggestions = ["Commuting", "Working", "Sleeping", "Exercising"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        suggestionCollectionView.delegate = self
        suggestionCollectionView.dataSource = self
        
        statusTextView.inputAccessoryView = accesoryView
        displayUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(textViewTextDidChange(_:)), name: NSNotification.Name.UITextViewTextDidChange, object: statusTextView)
    }
    
    private func displayUI() {
        let status = UserDefaults.standard.string(forKey: "profile_post") ?? ""
//        self.statusTextView.delegate = self
        self.statusTextView.text = status
        
        if self.statusTextView.text == "" {
            DispatchQueue.main.async {
                self.postButton.isEnabled = false
                self.postButton.tintColor = .gray
            }
        } else {
            DispatchQueue.main.async {
                self.postButton.isEnabled = true
                self.postButton.tintColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            }
        }
    }
    
    @IBAction func postStatus(_ sender: UIBarButtonItem) {
        view.isUserInteractionEnabled = false
        if !status.isEmpty {
            SVProgressHUD.show()
            SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 3
            manager.request(APIClient.add(status: status)).responseJSON { [weak this = self] response in
                switch response.result {
                case .success(let json):
                    guard let jsonData = json as? [String: Any] else { return }
                    let statusCode = jsonData["status"] as? Int ?? 0
                    SVProgressHUD.dismiss(completion: {
                        if statusCode == 200 {
                            this?.view.isUserInteractionEnabled = true
                            guard let JSON = jsonData["data"] as? [String: Any] else { return }
                            let status = JSON["post"] as? String ?? ""
                            UserDefaults.standard.set(status, forKey: "profile_post")
                            
//                            this?.performSegue(withIdentifier: "reload", sender: self)
                        } else {
                            this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                            this?.view.isUserInteractionEnabled = true
                        }})
                    
                    break
                case .failure(let error):
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                        this?.view.isUserInteractionEnabled = false
                    })
                    break
            }}
        } else {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString(Constants.MessageDialog.INCOMPLETE_FORM, comment: ""))
        }
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func textViewTextDidChange(_ notification: Notification) {
        
        guard let length = statusTextView.text?.lengthOfBytes(using: String.Encoding.utf8) else { return }
        
        //        textCount.text = "\(length)/50"
        print("text count ====== \(length)")
        
        if length == 0 {
            DispatchQueue.main.async {
                self.postButton.tintColor = .gray
                self.postButton.isEnabled = false
            }
        } else {
            DispatchQueue.main.async {
                self.postButton.tintColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                self.postButton.isEnabled = true
            }
        }
    }
}

//extension CreateStatusViewController: UITextViewDelegate {
//////    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
////////        if text == "\n" {
////////            textView.resignFirstResponder()
////////            return false
////////        }
////////
////////        if range.length + range.location > statusTextView.text.count {
////////            return false
////////        }
////////
////////        let newLength = statusTextView.text.count + text.count - range.length
////////
////////        self.textCount.text = "\(newLength)"
////////
////////        return newLength <= 50
////////
//////        return allow
//////    }
//////
//
//////
//////    private func customAlert() {
//////        let alert = UIAlertController(title: "Alert", message: "Message", preferredStyle: UIAlertControllerStyle.alert)
//////
//////        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (alert) in
//////            self.allow = true
//////        }))
//////
//////        self.present(alert, animated: true, completion: nil)
//////    }
//}

extension CreateStatusViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SuggestionCollectionViewCell", for: indexPath) as? SuggestionCollectionViewCell {
            cell.suggestionLabel.text = suggestions[indexPath.row]
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return suggestions.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            self.postButton.tintColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            self.postButton.isEnabled = true
        }
        
        statusTextView.text = suggestions[indexPath.row]
    }
}
