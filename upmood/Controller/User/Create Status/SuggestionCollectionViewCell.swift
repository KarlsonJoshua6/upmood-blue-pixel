//
//  SuggestionCollectionViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 04/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class SuggestionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var suggestionLabel: UILabel!
}
