//
//  NewDeviceViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 25/11/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import CoreBluetooth
import Pulsator
import RealmSwift
import UserNotifications
import UIKit
import Lottie
import KeychainSwift

var savedWriteChannel: CBCharacteristic!

class NewDeviceViewController: UIViewController {
    
    @IBOutlet weak var sensorView: RoundUIView!
    @IBOutlet weak var sleepView: RoundUIView!
    @IBOutlet weak var disconnectView: RoundUIView!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var wave: UIView!
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var service: NewDeviceService!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var registeredDeviceLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var estTimeLabel: UILabel!
    @IBOutlet weak var bandNoLabel: UILabel!
    @IBOutlet weak var autoReconnectSwitch: UISwitch!
    @IBOutlet weak var pulseViewHolder: UIView!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var batterySavingLabel: UILabel!
    @IBOutlet weak var batterySavingImageView: UIImageView!
    
    @IBOutlet weak var sleepModeImageView: UIView!
    @IBOutlet weak var disconnectUIView: UIView!
    @IBOutlet weak var batteryUIView: UIView!
    
    // stored value
    private var registeredDevices = [String]()
    private var savedDevicesArr = [String]()
    private let pulsator = Pulsator()
    private var selectedBandName = ""
    private var percent_batt = 0
    var isConnected = false
    var backgroundUUID = [UUID]()
    var backgroundUUIDStr = [String]()
    let charRead = "0000FFF1-1000-1000-8000-00805F9B34FB"
    let charNotif = "0000FFF3-1000-1000-8000-00805F9B34FB"
    let charWrite = "0000FFF2-1000-1000-8000-00805F9B34FB"
    
    let charReadWatch = "00002a01-0000-1000-8000-00805f9b34fb"
    let charNotifWatch = "FFF7"
    let charWriteWatch = "0000FFF6-0000-1000-8000-00805F9B34FB"
    
    //let dataProcessor = HRMDataProcessor()
    let watchProcessor = WatchDataProcessor()
    var centralManager: CBCentralManager!
    var heartRatePeripheral: CBPeripheral!
    var watches : Array<String> = Array<String>()
    var watchesPeriperhalCode : [String : CBPeripheral] = [:]
    var writeChannel: CBCharacteristic!
    var writeChannels: Array<CBCharacteristic> = Array<CBCharacteristic>()
    
    var initWave = SPWaterProgressIndicatorView.init(frame: .zero)
    var didRegisteredDevicesShow = false
    
    var currentBLEName = ""

    var realm: Realm!
    var objectArray: Results<Device> {
        get {
            return realm.objects(Device.self)
        }
    }
    
    // MARK: - new bluetooth
       
    let bluetoothManager = BluetoothManager.getInstance()
    fileprivate var nearbyPeripheralInfos: [PeripheralInfos] = []
    var preferences: Preferences? {
        return PreferencesStore.shared.preferences
    }
    
    fileprivate var services : [CBService]?
    var previousDeviceConnected: CBPeripheral?
    var peripheralName = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if !bluetoothManager.connected {
            realm = try! Realm()
            initObserver()
            setUpHideUI()
            bluetoothManager.delegate = self
            tableView.delegate   = self
            tableView.dataSource = self
            circleView.isHidden  = true
            UICircleView()
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "recievedSMSLovers"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.fcmNotif), name: NSNotification.Name(rawValue: "recievedSMSLovers"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "getCalStepsCommand"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getCalSteps), name: NSNotification.Name(rawValue: "getCalStepsCommand"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "watchSettingCommand"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.watchSettings), name: NSNotification.Name(rawValue: "watchSettingCommand"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "clearDataFitness"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.clearDataFitness), name: NSNotification.Name(rawValue: "clearDataFitness"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "disconnectBLE"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.disconnectBLE), name: NSNotification.Name(rawValue: "disconnectBLE"), object: nil)
    }

    private func UICircleView(){
        batteryUIView.makeCircular()
        sleepModeImageView.makeCircular()
        disconnectUIView.makeCircular()
    }
    
    private func getDarkmode() {
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .light {
                sensorView.backgroundColor = UIColor.white
                sleepView.backgroundColor = UIColor.white
                disconnectView.backgroundColor = UIColor.white
            } else {
                sensorView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)
                sleepView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)
                disconnectView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)

            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?){
        super.traitCollectionDidChange(previousTraitCollection)
        getDarkmode()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: hexStringToUIColor(hex: Constants.Color.CUSTOM_COLOR)]
        self.tableView.isUserInteractionEnabled = true
        
        
        if !bluetoothManager.connected {
            startScan()
            setupPulsator()
            setUpUIConstraints()
            self.scanButton.setTitle(NSLocalizedString("SCAN", comment: ""), for: .normal)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                self.stopScan()
                self.scanButton.setTitle(NSLocalizedString("SCAN", comment: ""), for: .normal)
            }
        } else {
            willAppearContentView()
            checkAutoReconnectSwitch()
        }
        
        getDarkmode()
    }
    
    private func willAppearContentView() {
        
        self.tableView.isHidden = true
        self.scanButton.isHidden = true
        
        self.contentView.isHidden = false
        self.containerView.isHidden = false
        self.percentLabel.isHidden = false
    
        self.isConnected = true
        self.resultLabel.isHidden = true
        self.circleView.isHidden = false
        self.circleView.asCircle()
        
        let bandName = UserDefaults.standard.string(forKey: "bandName") ?? "No Name"
        let bat_percent = UserDefaults.standard.integer(forKey: "batteryPercent")
        
        self.bandNoLabel.text = bandName
        self.deInitWave()
        
        if self.isConnected == true {
            self.isConnected = false
            self.updateWave(withPercent: bat_percent)
            
            if self.selectedBandName.contains("UP") || self.selectedBandName.contains("UpMood-") {
                self.estTimeLabel.text = timeRemain(withPercent: bat_percent)
            } else {
                self.estTimeLabel.text = timeRemainWatch(withPercent: bat_percent)
            }
        }
        
        self.initBatLabel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //stopScan()
        //nearbyPeripheralInfos.removeAll()
    }
    
    private func setUpUIConstraints() {
        let savedDevices = UserDefaults.standard.string(forKey: "registeredDevices") ?? ""
        self.registeredDevices = savedDevices.components(separatedBy: ",")
        self.scanButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        
        DispatchQueue.main.async {
            self.scanButton.setTitle(NSLocalizedString("SCAN", comment: ""), for: .normal)
            self.resultLabel.text = NSLocalizedString("Scan to find Devices", comment: "")
            
            if Constants.firstTimeUser == true {
                self.didRegisteredDevicesShow = false
                self.registeredDeviceLabelConstraint.constant = 0.0
            } else {
                // temp lang = true dapt to, constants = 32.0
                self.didRegisteredDevicesShow = true
                self.registeredDeviceLabelConstraint.constant = 32.0
            }
            
            self.scanButton.isUserInteractionEnabled = true
            self.tableView.reloadData()
        }
    }
    
    private func setUpHideUI() {
        // hides UI
        contentView.isHidden = true
        containerView.isHidden = true
        percentLabel.isHidden = true
    }
    
    private func setupPulsator() {
        pulsator.radius = 100.0
        pulsator.numPulse = 5
        pulsator.animationDuration = 8
        pulsator.backgroundColor = UIColor.white.cgColor
        pulsator.position = CGPoint(x: pulseViewHolder.fs_height/2, y: pulseViewHolder.fs_width/2)
        pulseViewHolder.layer.addSublayer(pulsator)
    }
    
    private func sort() {
        nearbyPeripheralInfos = nearbyPeripheralInfos.filter { (peripheralInfo) -> Bool in
            guard peripheralInfo.RSSI != 127 else {
                return false
            }
            
            guard let preferences = self.preferences, preferences.needFilter, peripheralInfo.RSSI >= preferences.filter  else {
                return true
            }
            
            return false
        }.sorted {
            $0.RSSI > $1.RSSI
        }
        
        tableView.reloadData()
    }
    
    @IBAction func scan(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            self.registeredDeviceLabelConstraint.constant = 0.0
            self.didRegisteredDevicesShow = false
            self.tableView.isUserInteractionEnabled = true
            self.tableView.reloadData()
        }
        
        if let buttonTitle = sender.title(for: .normal) {
            if buttonTitle == "STOP" {
                self.stopScan()
            } else {
                self.startScan()
                self.tableView.reloadData()
            }
        }
    }
    
    private func startScan() {
//        self.tableView.isUserInteractionEnabled = false
//        self.scanButton.isUserInteractionEnabled = false
        scanButton.setTitle(NSLocalizedString("STOP", comment: ""), for: .normal)
        tableView.isHidden = false
        pulsator.isHidden = false
        pulsator.start()
        nearbyPeripheralInfos.removeAll()
        peripheralName.removeAll()
        bluetoothManager.startScanPeripheral()
    }
    
    private func stopScan() {
        scanButton.setTitle(NSLocalizedString("SCAN", comment: ""), for: .normal)
        resultLabel.text = NSLocalizedString("Scan to find Devices", comment: "")
        bluetoothManager.stopScanPeripheral()
        pulsator.isHidden = true
        pulsator.stop()
        tableView.isHidden = false
    }
    
    @IBAction func removeRegisteredDevices(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Remove", comment: ""), style: .destructive , handler:{ _ in
            let savedDevices = self.objectArray[sender.tag]
              try! self.realm!.write {
                  self.realm!.delete(savedDevices)
                  self.tableView.reloadData()
              }
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func disconnect(_ sender: UIButton) {
        if let fetchAutoReconnect = UserDefaults.standard.object(forKey: "fetchAutoReconnect") {
            let autoReconnect = fetchAutoReconnect as? Int ?? 0
            if autoReconnect == 1 {
                presentDismissableAlertController(title: NSLocalizedString(NSLocalizedString("Auto reconnect is on", comment: ""), comment: ""), message: NSLocalizedString("Please turn off the auto reconnect before disconnecting.", comment: ""))
            } else {
                Constants.didUserDisconnectedToBand = true
                bluetoothManager.disconnectPeripheral()
                disconnectedAction()
            }
        }
    }
    
    private func disconnectedAction() {
    
        resultLabel.text = NSLocalizedString("Scan to find Devices", comment: "")
        
        self.contentView.isHidden = true
        self.containerView.isHidden = true
        self.percentLabel.isHidden = true
        self.scanButton.isHidden = false
        self.scanButton.setTitle(NSLocalizedString("SCAN", comment: ""), for: .normal)
        self.pulsator.stop()
        self.deInitWave()
        self.resultLabel.isHidden = false
        self.circleView.isHidden = true
        self.tableView.isHidden = false
        
        UserDefaults.standard.set("", forKey: "bandName")
        UserDefaults.standard.set(0, forKey: "batteryPercent")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewDidLoad()
            self.registeredDeviceLabelConstraint.constant = 0.0
            self.didRegisteredDevicesShow = false
            self.tableView.isUserInteractionEnabled = true
            self.tableView.reloadData()
        }
    }
    
    @IBAction func autoReconnect(_ sender: UISwitch) {
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
            if sender.isOn {
                UserDefaults.standard.set(1, forKey: "fetchAutoReconnect")
            } else {
                UserDefaults.standard.set(0, forKey: "fetchAutoReconnect")
            }
        } else {
            let privacy = UserDefaults.standard.integer(forKey: "privacyManagePrivacy")
            let emotion = UserDefaults.standard.integer(forKey: "emotionManagePrivacy")
            let heartBeat = UserDefaults.standard.integer(forKey: "heartbeatManagePrivacy")
            let stressLevel = UserDefaults.standard.integer(forKey: "stressLevelManagerPrivacy")
            let fetchUserNotiificationType = UserDefaults.standard.string(forKey: "fetchUserNotiificationType") ?? ""
            
            if sender.isOn {
                self.updateManageProfile(privacy: privacy, emotion: emotion, heartbeat: heartBeat, stress_level: stressLevel, notification_type: fetchUserNotiificationType, auto_reconnect: 1)
            } else {
                self.updateManageProfile(privacy: privacy, emotion: emotion, heartbeat: heartBeat, stress_level: stressLevel, notification_type: fetchUserNotiificationType, auto_reconnect: 0)
            }
        }
    }
    
    @IBAction func batterySaving(_ sender: UIButton) {
        
    
        if batterySavingLabel.text == NSLocalizedString("Enable Battery Saving", comment: "") {
            self.batterySavingLabel.text = NSLocalizedString("Disable Battery Saving", comment: "")
            self.batterySavingImageView.image = UIImage(named: "turn_off_sensor")
        } else if batterySavingLabel.text == "Sensor: ON" {
            self.batterySavingLabel.text = NSLocalizedString("Sensor: OFF", comment: "")
            self.writeCharacteristic(commandType: 1)
            self.batterySavingImageView.image = UIImage(named: "turn_off_sensor")
        } else if batterySavingLabel.text == NSLocalizedString("Disable Battery Saving", comment: "") {
            self.batterySavingLabel.text = NSLocalizedString("Enable Battery Saving", comment: "")
            self.batterySavingImageView.image = UIImage(named: "turn_on_sensor")
        } else {
            self.batterySavingLabel.text = NSLocalizedString("Sensor: ON", comment: "")
            self.writeCharacteristic(commandType: 0)
            self.batterySavingImageView.image = UIImage(named: "turn_on_sensor")
        }
    }
    
    private func initBatLabel() {
        
        let bandName = UserDefaults.standard.string(forKey: "bandName") ?? "No Name"
        
        if bandName.contains("UpMood-") || self.selectedBandName.contains("UpMood-") {
            self.batterySavingLabel.text = NSLocalizedString("Enable Battery Saving", comment: "")
        } else {
            self.batterySavingLabel.text = "Sensor: ON"
        }
        
        self.batterySavingImageView.image = UIImage(named: "turn_on_sensor")
    }
    
    @IBAction func sleepMode(_ sender: UIButton) {
        performSegue(withIdentifier: "showSleepMode", sender: self)
    }
}
// MARK: - CellIdentifier

let tableViewCellIdentifier = "NewDeviceTableViewCell"

// MARK: - NewDeviceTableViewCell

class NewDeviceTableViewCell: UITableViewCell {
    @IBOutlet weak var bandNameLabel: UILabel!
    @IBOutlet weak var connectToBandButton: UIButton!
    @IBOutlet weak var removeBandButton: UIButton!
    @IBOutlet weak var removeButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var animationViewLottie: AnimationView!
    @IBOutlet weak var RoundView: RoundUIView!
    @IBOutlet weak var pairingToBand: UIButton!
    
    override func awakeFromNib() {
         super.awakeFromNib()
         // Initialization code
         getDarkmode()
     }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?){
           super.traitCollectionDidChange(previousTraitCollection)
           getDarkmode()
    }

    private func getDarkmode(){
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .light {
                bandNameLabel.textColor = UIColor.black
                RoundView.backgroundColor = UIColor.white
                connectToBandButton.setTitleColor(UIColor.black, for: .normal)
            } else {
                
                bandNameLabel.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_DEVICENAME)
                RoundView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)
                connectToBandButton.setTitleColor(hexStringToUIColor(hex: Constants.Color.CUSTOM_COLOR), for: .normal)
              //  cell.connectTo
            }
        } else {
            // Fallback on earlier versions
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension NewDeviceViewController: UITableViewDelegate, UITableViewDataSource {

    func startLottie(cell: NewDeviceTableViewCell) {
        cell.RoundView.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        cell.bandNameLabel.textColor = UIColor.white
        cell.animationViewLottie.animation = Animation.named("pairing_dot_animation")
        cell.animationViewLottie.loopMode = .loop
        cell.animationViewLottie.play()
        cell.animationViewLottie.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        cell.connectToBandButton.isHidden = true
        cell.pairingToBand.isHidden = false
        cell.animationViewLottie.isHidden = false
    }
    
    private func initLottie(cell: NewDeviceTableViewCell) {
        cell.animationViewLottie.stop()
        cell.animationViewLottie.isHidden = true
    }
    
    private func initCell(cell: NewDeviceTableViewCell) {
        cell.pairingToBand.isHidden = true
        cell.connectToBandButton.isHidden = false

        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .light {
                cell.bandNameLabel.textColor = UIColor.black
                cell.RoundView.backgroundColor = UIColor.white
                cell.connectToBandButton.setTitleColor(UIColor.black, for: .normal)
            } else {
                
                cell.bandNameLabel.textColor = hexStringToUIColor(hex: Constants.Color.TEXT_COLOR_DEVICENAME)
                cell.RoundView.backgroundColor = hexStringToUIColor(hex: Constants.Color.BACKGROUND_VIEW_INNER)
                cell.connectToBandButton.setTitleColor(hexStringToUIColor(hex: Constants.Color.CUSTOM_COLOR), for: .normal)
              //  cell.connectTo
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    private func setUpCell(cell: NewDeviceTableViewCell, indexPath: IndexPath) {
        if didRegisteredDevicesShow == true {
            cell.removeButtonWidth.constant = 29.0
            cell.removeBandButton.addTarget(self, action: #selector(self.removeRegisteredDevices(_:)), for: .touchUpInside)
            cell.removeBandButton.tag = indexPath.row
            cell.bandNameLabel.text = objectArray[indexPath.row].deviceName
            self.resultLabel.text = "Scan to find Devices"
        } else {
            cell.removeButtonWidth.constant = 0.0
            let peripheralInfo = nearbyPeripheralInfos[indexPath.row]
            let peripheral = peripheralInfo.peripheral
            
            var peripheralName = ""
            let periname = peripheral.name ?? ""
            
            if periname.contains("UP") || periname.contains("UpMood-") {
                peripheralName = peripheral.name ?? "".replacingOccurrences(of: "UpMood-", with: "Upmood-")
            } else {
                peripheralName = ""
            }
            
            cell.bandNameLabel.text = peripheralName
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdentifier, for: indexPath) as! NewDeviceTableViewCell
        initLottie(cell: cell)
        initCell(cell: cell)
        setUpCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if didRegisteredDevicesShow == true {
            return objectArray.count
          //  return testArray.count
        } else {
            return nearbyPeripheralInfos.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 86.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.isUserInteractionEnabled = false
        let cell = tableView.cellForRow(at: indexPath) as! NewDeviceTableViewCell
        self.startLottie(cell: cell)
        self.scanButton.isUserInteractionEnabled = false
        self.scanButton.backgroundColor = .gray
        
        if self.didRegisteredDevicesShow == true {
            if self.objectArray[indexPath.row].deviceName != "" {
                
                if peripheralName.contains(self.objectArray[indexPath.row].deviceName) {
                    if let uuid = UUID(uuidString: self.objectArray[indexPath.row].deviceUiid) {
                        if let peripheral = self.bluetoothManager._manager?.retrievePeripherals(withIdentifiers: [uuid]).first {
                            self.bluetoothManager.connectPeripheral(peripheral)
                        }
                    }
                } else {
                    self.tableView.isUserInteractionEnabled = true
                    self.initCell(cell: cell)
                    self.initLottie(cell: cell)
                    self.scanButton.isUserInteractionEnabled = true
                    self.scanButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                }
            }
        } else {
            
//            self.oldSelectedBLEName = self.nearbyPeripheralInfos[indexPath.row].peripheral.name ?? ""
//            self.stopScan()
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                self.startScan()
//            }
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
//                self.stopScan()
//                self.tableView.reloadData()
//
//                if self.peripheralName.contains(self.oldSelectedBLEName) {
//                    let peripheral = self.nearbyPeripheralInfos[indexPath.row].peripheral
//                    self.bluetoothManager.connectPeripheral(peripheral)
//                } else {
//                    print("wala na ahahaha")
//                    self.tableView.isUserInteractionEnabled = true
//                    self.scanButton.isUserInteractionEnabled = true
//                    self.scanButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
//                }
//            }
            
            
            // old
            let peripheral = self.nearbyPeripheralInfos[indexPath.row].peripheral
            let cell = tableView.cellForRow(at: indexPath) as! NewDeviceTableViewCell
            
            if cell.bandNameLabel.text == "" {
                self.tableView.isUserInteractionEnabled = true
                self.initCell(cell: cell)
                self.initLottie(cell: cell)
                self.scanButton.isUserInteractionEnabled = true
                self.scanButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            } else {
                self.bluetoothManager.connectPeripheral(peripheral)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! NewDeviceTableViewCell
        cell.bandNameLabel.textColor = UIColor.black
        cell.pairingToBand.isHidden = true
        cell.connectToBandButton.isHidden = false
        cell.animationViewLottie.stop()
        cell.animationViewLottie.isHidden = true
        cell.RoundView.backgroundColor = UIColor.white
    }
}

// MARK: - BluetoothDelegate

extension NewDeviceViewController: BluetoothDelegate {
    
    func autoReconnect(peripheral: CBPeripheral) {
        if bluetoothManager.connectedPeripheral != nil {
            print("Previous Peripheral Discovered, Trying to Reconnect...")
            let fetchAutoReconnect = UserDefaults.standard.integer(forKey:  "fetchAutoReconnect")
            
            switch fetchAutoReconnect {
            case 0:
                self.startScan()
                print("Auto Reconnect Disabled")
            case 1:
                print("Auto Reconnect Enabled")
                if let peripheral = self.previousDeviceConnected {
                    self.bluetoothManager.connectPeripheral(peripheral)
                }
            default:
                break
            }
         }
     }
    
    func writeCharacteristic(commandType: Int) {
        if bluetoothManager.connectedPeripheral != nil {

            var command:[UInt8]
            var year: UInt8
            var month: UInt8
            var day: UInt8

            var hour: UInt8
            var minute: UInt8
            var second: UInt8

            if commandType == 0 {
                
               command = [0xA3]
               command = WatchCommands().appControlHRMonitoringFunction(hrPPG: 0x02, module: 0x01)
                         
            } else if commandType == 1 {
                
               command = [0xA2]
               command = WatchCommands().appControlHRMonitoringFunction(hrPPG: 0x02, module: 0x02)

            } else {
           
                let strDate = Date().today(format: "yy.MM.dd")
                let date = strDate.components(separatedBy: ".")
                
                let dateSaved = UserDefaults.standard.string(forKey: "prevDateWatch") ?? ""
                
                if dateSaved.isEmpty {
                    print("add new date")
                    UserDefaults.standard.setValue(strDate, forKey: "prevDateWatch")
                } else if dateSaved == strDate {
                    print("Equal date")
                } else {
                    UserDefaults.standard.setValue(strDate, forKey: "prevDateWatch")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "clearDataFitness"), object: nil)
                    print("not equal date")
                }
                    
                let strTime = Date().today(format: "HH:mm:ss")
                let time = strTime.components(separatedBy: ":")
              
                year = WatchCommands().date(date: date[0])
                month = WatchCommands().date(date: date[1])
                day = WatchCommands().date(date: date[2])

                hour = WatchCommands().date(date: time[0])
                minute = WatchCommands().date(date: time[1])
                second = WatchCommands().date(date: time[2])

                command = WatchCommands().setTime(year: year, month: month, day: day, hour: hour, minute: minute , second: second)

            }
            
            if writeChannel != nil {
                bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: writeChannel!, type: .withResponse)
            } else {
                if savedWriteChannel != nil {
                    bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: savedWriteChannel!, type: .withResponse)
                } else {
                    print("No channel found talaga.")
                }
            }

        } else {
            print("Not connected to any device.")
        }
    }
    
    func didDiscoverPeripheral(_ peripheral: CBPeripheral, advertisementData: [String : Any], RSSI: NSNumber) {
        let peripheralInfo = PeripheralInfos(peripheral)
        
        if let peripheralName = peripheral.name {
//            if peripheralName.contains("UpMood-") || peripheralName.contains("UP") {
               if !nearbyPeripheralInfos.contains(peripheralInfo) {
                   if let preference = preferences, preference.needFilter {
                       if RSSI.intValue != 127, RSSI.intValue > preference.filter {
                           peripheralInfo.RSSI = RSSI.intValue
                           peripheralInfo.advertisementData = advertisementData
                           nearbyPeripheralInfos.append(peripheralInfo)
                           self.peripheralName.append(peripheralName)
                       }
                   } else {
                       peripheralInfo.RSSI = RSSI.intValue
                       peripheralInfo.advertisementData = advertisementData
                       nearbyPeripheralInfos.append(peripheralInfo)
                       self.peripheralName.append(peripheralName)
                       tableView.reloadData()
                   }
               }
                
                self.resultLabel.text = "\(self.nearbyPeripheralInfos.count) \(NSLocalizedString("result", comment: ""))"
            }
//        }
    }
        
    func didConnectedPeripheral(_ connectedPeripheral: CBPeripheral) {
        
        writeChannels.removeAll()
        bandConnected(peripheral: connectedPeripheral)
        
        // save connected devices
        registeredConnectedDevices(peripheral: connectedPeripheral)
        watchesPeriperhalCode = [connectedPeripheral.name! : connectedPeripheral]
        
        Constants.firstTimeUser = false
        
        self.previousDeviceConnected = connectedPeripheral
        self.selectedBandName = connectedPeripheral.name ?? "No-Name"
        self.tableView.isUserInteractionEnabled = true
        self.initBatLabel()
        self.navigationController?.popViewController(animated: false)
    }
    
    func didDisconnectPeripheral(_ peripheral: CBPeripheral) {
        // to be able to auto reconnect when app is in the background, and in the foreground
        
        bandDisconnected(peripheral: peripheral)
        scanButton.isUserInteractionEnabled = true
        scanButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        scheduleNotification()
        
        DispatchQueue.main.async {
           self.registeredDeviceLabelConstraint.constant = 0.0
           self.didRegisteredDevicesShow = false
           self.tableView.isUserInteractionEnabled = true
           self.tableView.reloadData()
        }
        
        //NotificationCenter.default.post(name: .didTappedLogOut, object: nil, userInfo: ["data":"yes"])
        NotificationCenter.default.post(name: .didDisconnectBand, object: nil, userInfo: nil)
        
        autoReconnect(peripheral: peripheral)
        UserDefaults.standard.set("", forKey: "bandName")
        UserDefaults.standard.set(0, forKey: "batteryPercent")
        UserDefaults.standard.removeObject(forKey: "stringFV")
        savedWriteChannel = nil
    }

    func didDiscoverServices(_ peripheral: CBPeripheral) {
        tableView.isHidden = true
        guard let services = peripheral.services else { return }
        
        for service in services {
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func didDiscoverCharacteritics(_ peripheral: CBPeripheral, service: CBService) {
        guard let characteristics = service.characteristics else { return }
        for characteristic in characteristics {
          if characteristic.properties.contains(.read) {
              peripheral.readValue(for: characteristic)
          }

          if characteristic.properties.contains(.notify) {
              peripheral.setNotifyValue(true, for: characteristic)
          }

          if characteristic.properties.contains(.write) {
              writeChannel = characteristic
              savedWriteChannel = writeChannel
              writeChannels.append(characteristic)
          }
       }
        self.writeCharacteristic(commandType: 2)
    }
    
    func onPPIReceived(_ ppi: [UInt8]) {
        //dataProcessor.convertBytesToHex(byteArray: ppi, watch: false, writeChannel: writeChannel, bluetoothManager: bluetoothManager)
    }
     
     func getPPI(from characteristic: CBCharacteristic) -> [UInt8] {
         guard let characteristicData = characteristic.value else { return [] }
         return [UInt8](characteristicData)
     }
    
    func didReadValueForCharacteristic(_ characteristic: CBCharacteristic) {
        
        switch characteristic.uuid.uuidString {
        case charNotif:
            let ppi = getPPI(from: characteristic)
            onPPIReceived(ppi)
        case charNotifWatch:
            let ppi = getPPI(from: characteristic)
            print("wanda", ppi)
            // old
            //watchProcessor.bytesToString(byteArray: ppi, writeChannel: writeChannel, bluetoothManager: bluetoothManager)
            
            // new
            if writeChannel != nil {
                watchProcessor.bytesToString(byteArray: ppi, writeChannel: writeChannel, bluetoothManager: bluetoothManager)
            } else {
                if savedWriteChannel != nil {
                    watchProcessor.bytesToString(byteArray: ppi, writeChannel: savedWriteChannel, bluetoothManager: bluetoothManager)
                } else {
                    print("No channel found talaga.")
                }
            }
            
        default:
            ()
        }
    }
}

// MARK: - Updating UI, Battery Usage

extension NewDeviceViewController {
    
    /*** Battery Usage ***/
    
    func timeRemainWatch(withPercent: Int) -> String {
        if withPercent >= 5 && withPercent <= 24 {
            return "36 Minutes"
        } else if withPercent >= 25 && withPercent <= 49 {
            return "3 \(NSLocalizedString("Hours", comment: ""))"
        } else if withPercent >= 50 && withPercent <= 74 {
            return "6 \(NSLocalizedString("Hours", comment: ""))"
        } else if withPercent >= 75 && withPercent <= 89 {
            return "9 \(NSLocalizedString("Hours", comment: ""))"
        } else if withPercent >= 90 && withPercent <= 100 {
            return "12 \(NSLocalizedString("Hours", comment: ""))"
        } else {
            return "UnKnown"
        }
    }
    
     func timeRemain(withPercent: Int) -> String {
        switch withPercent {
        case 100:
            return "12 \(NSLocalizedString("Hours", comment: ""))"
        case 75:
            return "9 \(NSLocalizedString("Hours", comment: ""))"
        case 50:
            return "6 \(NSLocalizedString("Hours", comment: ""))"
        case 25:
            return "3 \(NSLocalizedString("Hours", comment: ""))"
        case 5:
            return "36 Minutes"
        default:
            return "UnKnown"
        }
    }
    
    @objc func showBatteryPercent() {
        DispatchQueue.main.async {
            UserDefaults.standard.set(self.selectedBandName, forKey: "bandName")
            UserDefaults.standard.set(self.percent_batt, forKey: "batteryPercent")
            
            self.updateWave(withPercent: self.percent_batt)
            
            if self.selectedBandName.contains("UP") || self.selectedBandName.contains("UpMood-") {
                self.updateTimeRemaining(withPercent: self.percent_batt)
            } else {
                self.timeRemainWatch(withPercent: self.percent_batt)
            }
        }
    }
    
    func updateTimeRemaining(withPercent: Int) {
        self.estTimeLabel.text = timeRemain(withPercent: withPercent)
        self.bandNoLabel.text = self.selectedBandName
    }
    
    func updateWave(withPercent: Int) {
        self.containerView.isHidden = false
        self.percentLabel.isHidden = false
        self.initWave = SPWaterProgressIndicatorView(frame: self.containerView.bounds)
        if withPercent >= 100 || withPercent == 0 {
            self.percentLabel.text = "100%"
        } else {
            self.percentLabel.text = "\(withPercent)%"
        }
        
        self.initWave.completionInPercent = withPercent
        self.initWave.waveColor = hexStringToUIColor(hex: "#3DBFCD")
        self.containerView.addSubview(self.initWave)
    }
    
    func deInitWave() {
        self.containerView.isHidden = true
        self.percentLabel.isHidden = true
        self.initWave.removeFromSuperview()
    }
}

// MARK: - Band is Connected/Disconnected

extension NewDeviceViewController {
    
    private func saveConnectedDevice(withName: String, peripheral: CBPeripheral) {
        if (realm.object(ofType: Device.self, forPrimaryKey: withName) != nil) {
            // do nothing
        } else {
            let item = Device()
            item.deviceName = withName
            item.deviceUiid = peripheral.identifier.uuidString
            RealmHelper.saveObject(object: item)
        }
    }
    
    private func registeredConnectedDevices(peripheral: CBPeripheral) {
        if let deviceName = peripheral.name {
            self.saveConnectedDevice(withName: deviceName, peripheral: peripheral)
        }
    }
    
    private func bandDisconnected(peripheral: CBPeripheral) {
        self.nearbyPeripheralInfos.removeAll()
        self.peripheralName.removeAll()
        resultLabel.text = NSLocalizedString("Scan to find Devices", comment: "")
        self.tableView.isHidden = false
        self.contentView.isHidden = true
        self.containerView.isHidden = true
        self.percentLabel.isHidden = true
        self.scanButton.isHidden = false
        self.scanButton.setTitle(NSLocalizedString("SCAN", comment: ""), for: .normal)
        self.pulsator.stop()
        self.deInitWave()
        self.resultLabel.isHidden = false
        self.circleView.isHidden = true
    }
    
    private func bandConnected(peripheral: CBPeripheral) {
        self.deInitWave()
        self.tabBarController?.selectedIndex = 0
        NotificationCenter.default.post(name: .didConnectToUpMoodBand, object: nil, userInfo: ["data":"yes"])
        bluetoothManager.connectedPeripheral?.discoverServices(nil)
        self.tableView.isHidden = true
        self.scanButton.isHidden = true
        self.nearbyPeripheralInfos.removeAll()
        self.peripheralName.removeAll()
        self.tableView.reloadData()
        self.contentView.isHidden = false
        self.containerView.isHidden = false
        self.percentLabel.isHidden = false
        self.backgroundUUID.append(peripheral.identifier)
        self.isConnected = true
        self.resultLabel.isHidden = true
        self.circleView.isHidden = false
        self.circleView.asCircle()
    }
    
    private func checkAutoReconnectSwitch() {
        // set up auto reconnect switch
        let fetchAutoReconnect = UserDefaults.standard.integer(forKey: "fetchAutoReconnect")
        if fetchAutoReconnect == 1 {
            self.autoReconnectSwitch.isOn = true
        } else {
            self.autoReconnectSwitch.isOn = false
        }
    }
}

// MARK: - NotificationCenter Observer

extension NewDeviceViewController {
    func initObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(displayBatteryPercent(_:)), name: .batteryPercent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.didTappedLogOut), name: .didTappedLogOut, object: nil)
    }
    
    @objc func didTappedLogOut(_ notification: Notification) {
        if heartRatePeripheral == nil {
            
        } else {
            centralManager.cancelPeripheralConnection(heartRatePeripheral)
        }
    }
    
    @objc func displayBatteryPercent(_ notification: Notification) {
        if let dict = notification.userInfo as Dictionary? as! [String:Any]? {
            let batteryLevel = dict["percent"] as? UInt8 ?? 0
            let batteryInt = Int(batteryLevel)
            self.percent_batt = batteryInt
            
            if self.isConnected == true {
                self.isConnected = false
                showBatteryPercent()
            }
        }
    }
    
    @objc func fcmNotif(_ notification: NSNotification) {
        if writeChannel != nil {
            recieveSMSLover(writeChannel: writeChannel!, bluetoothManager: bluetoothManager)
        }
    }
    
    @objc func getCalSteps(_ notification: NSNotification) {
        if writeChannel != nil {
            getCalStepsCommand(writeChannel: writeChannel!, bluetoothManager: bluetoothManager)
        }
    }
    
    @objc func watchSettings(_ notification: NSNotification) {
        if writeChannel != nil {
            getWatchSettings(writeChannel: writeChannel!, bluetoothManager: bluetoothManager)
        }
        print("TEST!!",  Constants.timeDisplayStr)
        print("TEST22",  Constants.distanceStr)
        print("TEST33",  Constants.wristSenseStr)
        print("TEST44",  Constants.langStr)
    }
    
    @objc func clearDataFitness(_ notification: NSNotification) {
        if writeChannel != nil {
            getClearDataFitness(writeChannel: writeChannel!, bluetoothManager: bluetoothManager)
        }
    }
    
    @objc func disconnectBLE(_ notification: NSNotification) {
        disconnectedAction()
    }
    

    
    func recieveSMSLover(writeChannel: CBCharacteristic!, bluetoothManager: BluetoothManager){
        var command:[UInt8]
        command = WatchCommands().receiveSMSLoversMode()
        Constants.receivedCommand = true
        if writeChannel != nil {
            bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: writeChannel!, type: .withResponse)
        } else {
            if savedWriteChannel != nil {
                bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: savedWriteChannel!, type: .withResponse)
            } else {
                print("No channel found talaga.")
            }
        }
        
    }
    
    func getCalStepsCommand(writeChannel: CBCharacteristic!, bluetoothManager: BluetoothManager){
        var command:[UInt8]
        command = WatchCommands().getTotalStepsData(readOrDelete: 0x00)
        if writeChannel != nil {
            bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: writeChannel!, type: .withResponse)
        } else {
            if savedWriteChannel != nil {
                bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: savedWriteChannel!, type: .withResponse)
            } else {
                print("No channel found talaga.")
            }
        }
       
    }
    
    func getWatchSettings(writeChannel: CBCharacteristic!, bluetoothManager: BluetoothManager){
        var command:[UInt8]
        command = WatchCommands().setDeviceBasicParameters(distanceUnit: convertIntToBytes(index: Constants.distanceStr), hourFormat: convertIntToBytes(index: Constants.timeDisplayStr), wristSense: convertIntToBytes(index: Constants.wristSenseStr), language: convertIntToBytesLang(index: Constants.langStr ))
        
        if writeChannel != nil {
            bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: writeChannel!, type: .withResponse)
        } else {
            if savedWriteChannel != nil {
                bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: savedWriteChannel!, type: .withResponse)
            } else {
                print("No channel found talaga.")
            }
        }
       
    }
    
    
    func getClearDataFitness(writeChannel: CBCharacteristic!, bluetoothManager: BluetoothManager){
        var command:[UInt8]
        command = WatchCommands().getTotalStepsData(readOrDelete: 0x99)
        if writeChannel != nil {
            bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: writeChannel!, type: .withResponse)
        } else {
            if savedWriteChannel != nil {
                bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: savedWriteChannel!, type: .withResponse)
            } else {
                print("No channel found talaga.")
            }
        }
       
    }
    
    func convertIntToBytes(index: Int) -> UInt8{
        var hex: UInt8?
        switch index {
        case 0:
           hex = 0x81
        case 1:
           hex = 0x80
        case 2:
            hex = 0x00
        default:
            break
        }
        return hex ?? 0x00
    }
    
    func convertIntToBytesLang(index: Int) -> UInt8{
        var hex: UInt8?
        switch index {
        case 0:
           hex = 0x80
        case 1:
           hex = 0x81
        case 2:
            hex = 0x82
        case 3:
            hex = 0x83
        case 4:
            hex = 0x84
        case 5:
            hex = 0x85
        case 6:
            hex = 0x86
        case 7:
            hex = 0x87
        case 8:
            hex = 0x88
        case 9:
            hex = 0x00
        default:
            break
        }
        return hex ?? 0x00
    }
}

// MARK: - UserNotifications

extension NewDeviceViewController: UNUserNotificationCenterDelegate {
    func scheduleNotification() {
        let center = UNUserNotificationCenter.current()
        
        let content = UNMutableNotificationContent()
        content.title = "Upmood"
        content.body = NSLocalizedString("Your band has been disconnected", comment: "")
        content.categoryIdentifier = "alarm"
        content.userInfo = ["customData": "fizzbuzz"]
        content.sound = UNNotificationSound.default()
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.add(request)
    }
    
    func registerCategories() {
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        
        let show = UNNotificationAction(identifier: "show", title: "Tell me more…", options: .foreground)
        let category = UNNotificationCategory(identifier: "alarm", actions: [show], intentIdentifiers: [])
        
        center.setNotificationCategories([category])
    }
    
    // delegate
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // pull out the buried userInfo dictionary
        let userInfo = response.notification.request.content.userInfo
        
        if let customData = userInfo["customData"] as? String {
            print("Custom data received: \(customData)")
            
            switch response.actionIdentifier {
            case UNNotificationDefaultActionIdentifier:
                print("Default identifier")
                
            case "show":
                print("Show more information…")
                break
                
            default:
                break
            }
        }
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}

// MARK: - Temp only to be transfer in service model

extension NewDeviceViewController {
    private func updateManageProfile(privacy: Int, emotion: Int, heartbeat: Int, stress_level: Int, notification_type: String, auto_reconnect: Int) {
        Alamofire.request(APIClient.updateManagePrivacy(privacy: privacy, emotion: emotion, heartbeat: heartbeat, stress_level: stress_level, notification_type: notification_type, auto_reconnect: auto_reconnect, auto_renew: 0)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                let message = jsonDic["message"] as? String ?? ""
                
                switch statusCode {
                case 200:
                    UserDefaults.standard.set(auto_reconnect, forKey: "fetchAutoReconnect")
                    print("success updated auto_reconnect \n\n\n \(json) \n\n\n")
                    break
                case 204:
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: message)
                    break
                default:
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: message)
                    break
                }
                
                break
            case .failure(let error):
                UserDefaults.standard.set(1, forKey: "fetchAutoReconnect")
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
}

fileprivate class PeripheralInfos: Equatable, Hashable {
    
      let peripheral: CBPeripheral
      var RSSI: Int = 0
      var advertisementData: [String: Any] = [:]
      var lastUpdatedTimeInterval: TimeInterval
          
      init(_ peripheral: CBPeripheral) {
          self.peripheral = peripheral
          self.lastUpdatedTimeInterval = Date().timeIntervalSince1970
      }
      
      static func == (lhs: PeripheralInfos, rhs: PeripheralInfos) -> Bool {
          return lhs.peripheral.isEqual(rhs.peripheral)
      }
          
      var hashValue: Int {
          return peripheral.hash
      }
}

extension UIView {
    func makeCircular() {
        self.layer.cornerRadius = min(self.frame.size.height, self.frame.size.width) / 2.0
        self.clipsToBounds = true
    }

}


extension Date {

   func today(format : String) -> String{
      let date = Date()
      let formatter = DateFormatter()
      formatter.dateFormat = format
      return formatter.string(from: date)
   }
}



