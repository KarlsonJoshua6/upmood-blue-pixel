//
//  Constant.swift
//  bluetooth2
//
//  Created by Joseph Mikko Mañoza on 05/03/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import UIKit

class Constant {
    static var bandDisconnected = false
    
    // post status select and deselect
    public static var btnWorkBool = true
    public static var bntDesignBool = true
    public static var btnMeetBool = true
    public static var btnAnalyzeBool = true
    public static var btnMonitorBool = true
    public static var btnTrackBool = true
    public static var btnAuditBool = true
    public static var btnOrganizeBool = true
    public static var btnEvaluteBool = true
    public static var btnTestBool = true
    public static var btnThinkBool = true
    public static var btnProgramBool = true
    public static var btnInterpretBool = true
    public static var btnIndentifyBool = true
    
    public static var btnPlayBool = true
    public static var btnExerciseBool = true
    public static var btnReadBool = true
    public static var btnWatchBool = true
    public static var btnShopBool = true
    public static var btnDanceBool = true
    public static var btnSingBool = true
    public static var btnPartyBool = true
    public static var btnGameBool = true
    public static var btnListenBool = true
    public static var btnCelebrateBool = true
    public static var btnSleepBool = true
    public static var btnRestBool = true
    public static var btnRelaxBool = true
    public static var btnMeditateBool = true
    
    public static var btnBakeBool = true
    public static var btnCookBool = true
    public static var btnEatBool = true
    public static var btnCleanBool = true
    
    public static var btnTravelBool = true
    public static var btnDriveBool = true
    public static var btnJogBool = true
    public static var btnCommuteBool = true
    public static var btnVisitBool = true
    public static var btnRidingBool = true
    
    public static var btnStudyBool = true
    public static var btnDiscoverBool = true
    public static var btnResolveBool = true
    public static var btnBuildBool = true
    public static var btnCreateBool = true
    public static var btnAttendBool = true
    public static var btnDecorateBool = true
    public static var btnWriteBool = true
    public static var btnReportBool = true
}

extension UIApplication {
    
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        
        return viewController
    }
}


