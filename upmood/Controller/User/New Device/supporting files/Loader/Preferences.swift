//
//  Pref.swift
//  bluetooth2
//
//  Created by Joseph Mikko Mañoza on 05/03/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

struct Preferences: Codable {
    var needFilter: Bool
    var filter: Int
    
    init() {
        self.needFilter = false
        filter = -90
    }
}
