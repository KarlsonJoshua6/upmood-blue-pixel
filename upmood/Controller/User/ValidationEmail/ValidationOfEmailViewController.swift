//
//  ValidationOfEmailViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 16/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import UIKit

class ValidationOfEmailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func resend() {
        let userEmail = UserDefaults.standard.string(forKey: "fetchEmail") ?? ""
        let userId = UserDefaults.standard.integer(forKey: "fetchUserId")
        let userToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: "#0099A3"))
        Alamofire.request(APIClient.sendValidationEmail(email: userEmail, id: userId, token: userToken)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                SVProgressHUD.dismiss(completion: {
                    print("response: \(json)")
                    this?.checkUserStatus()
                })
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
    
    @IBAction func skip(_ sender: UIButton) {
        checkUserStatus()
    }
    
    @IBAction func resend(_ sender: RoundButton) {
       resend()
    }
    
    private func checkUserStatus() {
        let status = UserDefaults.standard.integer(forKey: "status")
        switch status {
        case 3:
            self.setUpLanguage()
        default:
            performSegue(withIdentifier: "showEmoji", sender: self)
        }
    }
    
    private func setUpLanguage() {
        
        let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage")
        
        if selectedLanguage == "en" {
            Bundle.set(language: "en")
        } else if selectedLanguage == "ja" {
            Bundle.set(language: "ja")
        } else {
            //
        }
        
        let storyboard = UIStoryboard.init(name: "DashboardScreen", bundle: nil)
        UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
        UserDefaults.standard.set(selectedLanguage, forKey: "selectedLanguage")
    }
}


