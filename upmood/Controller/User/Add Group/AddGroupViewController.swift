//
//  AddGroupViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 02/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SDWebImage
import SVProgressHUD
import SkyFloatingLabelTextField
import UIKit

extension AddGroupViewController: ChooseMemberViewControllerDelegate {
    
    func didSelectMember(withMemberId: [Int], memberProfilePic: [String], memberName: [String]) {
        self.selectedFriendUsername = memberName
        self.selectedFriendProfilePic = memberProfilePic
        self.selectedFriendId = withMemberId
        self.tableView.reloadData()
    }
    
    private func addGroup(withGroupName: String) {
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let uniqueId = self.selectedFriendId.map { String($0) }
        Alamofire.request(APIClient.addNewGroup(withGroupName: withGroupName, my_mood: 1, emotion: 1, heartbeat: 1, emotions: "anxious,calm,challenged,confused,excitement,happy,tense,pleasant,sad,unpleasant,zen", members: uniqueId.joined(separator: ","), notificationType: "live")).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                SVProgressHUD.dismiss(completion: {
                    print("success added group: \(json)")
                    UserDefaults.standard.set(true, forKey: "didReloadAddGroupViewController")
                    this?.performSegue(withIdentifier: "reloadGroup", sender: self)
                })
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
}

class AddGroupViewController: UIViewController {

    @IBOutlet weak var groupName: SkyFloatingLabelTextField!
    @IBOutlet weak var tableView: UITableView!
    
    private var selectedFriendUsername = [String]()
    private var selectedFriendProfilePic = [String]()
    private var selectedFriendId = [Int]()
    
    var friendName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        if UserDefaults.standard.string(forKey: "selectedLanguage") == "en" {
            groupName.title = "Group Name"
        } else {
            groupName.title = "グループ名"
        }
        
        setUserInterfaceStyleLight(self: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        if groupName.text!.isEmpty {
            presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString(Constants.MessageDialog.INCOMPLETE_FORM, comment: ""))
        } else {
            addGroup(withGroupName: groupName.text ?? "")
        }
    }
    
    @IBAction func addNewMember(_ sender: UIButton) {
        let nextVC = storyboard!.instantiateViewController(withIdentifier: "ChooseMemberViewController") as! ChooseMemberViewController
        nextVC.delegate = self
        present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func reloadAddGroupVC(_ sender: UIStoryboardSegue) {
        print("ito ang friendname: \(self.friendName)")
    }
    
    private func setUpCell(cell: ChooseAndLoadFriendTableViewCell, indexPath: IndexPath) {
        cell.usernameLabel.text = selectedFriendUsername[indexPath.row]
        
        if self.selectedFriendProfilePic[indexPath.row].isEmpty {
            cell.profilePicImageView.image = UIImage(named: "ic_empty_state_profile")
        }
        
        if let url = URL(string: self.selectedFriendProfilePic[indexPath.row]) {
            cell.profilePicImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: .continueInBackground, completed: nil)
        }
    }
}

extension AddGroupViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseAndLoadFriendTableViewCell", for: indexPath) as? ChooseAndLoadFriendTableViewCell {
            self.setUpCell(cell: cell, indexPath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedFriendUsername.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
}
