//
//  ChooseMemberTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 02/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class ChooseMemberTableViewCell: UITableViewCell {

    @IBOutlet weak var profilePicImageVIew: ClipToBoundsImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var checkMark: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.checkMark.setImage(UIImage(named: ""), for: .normal)
    }
}
