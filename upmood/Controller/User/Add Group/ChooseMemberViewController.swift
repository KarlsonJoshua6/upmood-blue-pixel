//
//  ChooseMemberViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 02/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SDWebImage
import SkyFloatingLabelTextField
import SVProgressHUD
import UIKit

protocol ChooseMemberViewControllerDelegate {
    func didSelectMember(withMemberId: [Int], memberProfilePic: [String], memberName: [String])
}

class ChooseMemberViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var friendId = [Int]()
    private var friendUsername = [String]()
    private var friendProfilePicture = [String]()
    private var selectedIndex = 0
    private var selectedId = [Int]()
    private var didHaveNextPage = true
    private var currentPage = 0
    
    private var selectedNameArr = [String]()
    private var selectedIdArr = [Int]()
    private var selectedProfilePicArr = [String]()
    
    var groupIdSelected: Int!
    var delegate: ChooseMemberViewControllerDelegate?
    var selectedPaths=Set<IndexPath>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        loadFriendList()
        setUserInterfaceStyleLight(self: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Choose Member"
        navigationController?.navigationBar.isHidden = false
        
        if let index = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: index, animated: true)
        }
        
        self.searchBar.setShowsCancelButton(false, animated: true)
        //self.tableView.reloadData()
        self.reload()
        self.searchBar.endEditing(true)
        self.searchBar.text = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func done(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            if self.selectedIdArr.isEmpty || self.selectedProfilePicArr.isEmpty || self.selectedNameArr.isEmpty {
                self.presentDismissableAlertController(title: Constants.MessageDialog.WARINING, message: "One of your selected friend is dummy")
            } else {
                self.delegate?.didSelectMember(withMemberId: self.selectedIdArr, memberProfilePic: self.selectedProfilePicArr, memberName: self.selectedNameArr)
            }
        })
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.reload()
        self.dismiss(animated: true, completion: nil)
    }
    
    private func initializeCheckMark(cell: ChooseMemberTableViewCell, indexPath: IndexPath) {
        if self.selectedPaths.contains(indexPath) {
            cell.checkMark.setBackgroundImage(UIImage(named: "ic_check_mark"), for: .normal)
        } else {
            cell.checkMark.setBackgroundImage(UIImage(named: ""), for: .normal)
        }
    }
    
    private func setUpCell(cell: ChooseMemberTableViewCell, indexPath: IndexPath) {
        cell.usernameLabel.text = friendUsername[indexPath.row]
        
        if self.friendProfilePicture[indexPath.row].isEmpty {
            cell.profilePicImageVIew.image = UIImage(named: "ic_empty_state_profile")
        } else {
            if let url = URL(string: self.friendProfilePicture[indexPath.row]) {
                cell.profilePicImageVIew.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: .continueInBackground, completed: nil)
            }
        }
    }
}

extension ChooseMemberViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseMemberTableViewCell", for: indexPath) as? ChooseMemberTableViewCell {
            self.setUpCell(cell: cell, indexPath: indexPath)
            self.initializeCheckMark(cell: cell, indexPath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendUsername.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if self.didHaveNextPage == true {
            if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
                currentPage+=1
                self.loadFriendListWithPagination(currentPage: self.currentPage)
            }

        } else {
            self.tableView.tableFooterView?.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ChooseMemberTableViewCell
        self.buttonTappedOnCell(cell: cell)
        //self.tableView.reloadData()
    }
}

// MARK: - UISearchBarDelegate

extension ChooseMemberViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        self.tableView.reloadData()
        self.reload()
        
        // hide clear button
        guard let firstSubview = searchBar.subviews.first else { return }
        firstSubview.subviews.forEach { ($0 as? UITextField)?.clearButtonMode = .never }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.searchBar.endEditing(true)
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.tableView.reloadData()
        self.loadFriendList()
        self.searchBar.endEditing(true)
        self.searchBar.text = ""
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == self.searchBar {
            if searchBar.returnKeyType == .search {
                self.tableView.reloadData()
                self.reload()
                searchBar.setShowsCancelButton(false, animated: true)
                searchBar.resignFirstResponder()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        self.tableView.reloadData()
        self.reload()
        
        let typeCasteToStringFirst = searchBar.text as NSString?
        let newString = typeCasteToStringFirst?.replacingCharacters(in: range, with: text)
        let finalSearchString = newString ?? ""
        self.searchFriendAvailable(withKeyword: finalSearchString.lowercased())
        return true
    }
    
    private func reload() {
        friendId.removeAll()
        friendUsername.removeAll()
        friendProfilePicture.removeAll()
        selectedId.removeAll()
        selectedNameArr.removeAll()
        selectedIdArr.removeAll()
        selectedProfilePicArr.removeAll()
        selectedPaths.removeAll()
        didHaveNextPage = true
        selectedIndex = 0
        currentPage = 0
        tableView.reloadData()
    }
}

// MARK - REST

extension ChooseMemberViewController {
    
    private func searchFriendAvailable(withKeyword: String) {
        self.reload()
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.loadFriendAvailableOnGroup(withKeyword: withKeyword, groupId: 0)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                guard let data = jsonDic["data"] as? [String: Any] else { return }
                guard let jsonData = data["data"] as? [[String: Any]] else { return }
                
                if statusCode == 200 {
                    
                    let currentpage = data["current_page"] as? Int ?? 0
                    self.currentPage = currentpage
                    let nextPage = data["next_page_url"] as? String ?? ""
                    if nextPage.isEmpty {
                        self.didHaveNextPage = false
                    } else {
                        self.didHaveNextPage = true
                    }
                    
                    SVProgressHUD.dismiss(completion: {
                        
                        this?.friendId.removeAll()
                        this?.friendUsername.removeAll()
                        this?.friendProfilePicture.removeAll()

                        for data in jsonData {
                            let friendId = data["id"] as? Int ?? 0
                            let name = data["name"] as? String ?? ""
                            let profilePhoto = data["image"] as? String ?? ""
                            this?.friendId.append(friendId)
                            this?.friendUsername.append(name)
                            this?.friendProfilePicture.append(profilePhoto.replacingOccurrences(of: " ", with: "%20"))
                        } // end of fucking loop
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss(completion: {
                                this?.tableView.reloadData()
                            })
                        }
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No available friend.")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func addFriendToGroup(withGroupId: Int, members: String) {
        Alamofire.request(APIClient.addFriendToGroup(members: members, group_id: withGroupId)).responseJSON { [weak this = self] response in
            
            print("response to 3 - \(response.result.value)")
            
            switch response.result {
            case .success(let json):
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                if statusCode == 200 {
                    print("success edit: \(jsonDic)")
                    this?.dismiss(animated: true, completion: {
                        //                        this?.performSegue(withIdentifier: "reloadGroup", sender: self)
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No record found!")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func loadFriendList() {
        self.reload()
        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        Alamofire.request(APIClient.loadFriendAvailableOnGroup(withKeyword: "", groupId: 0)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                guard let data = jsonDic["data"] as? [String: Any] else { return }
                guard let jsonData = data["data"] as? [[String: Any]] else { return }
                
                if statusCode == 200 {
                    
                    let currentpage = data["current_page"] as? Int ?? 0
                    self.currentPage = currentpage
                    let nextPage = data["next_page_url"] as? String ?? ""
                    if nextPage.isEmpty {
                        self.didHaveNextPage = false
                    } else {
                        self.didHaveNextPage = true
                    }
                    
                    print("ito ang nextPage: \(nextPage), \(self.didHaveNextPage)")
                    
                    SVProgressHUD.dismiss(completion: {
                        for data in jsonData {
                            let friendId = data["id"] as? Int ?? 0
                            let name = data["name"] as? String ?? ""
                            let profilePhoto = data["image"] as? String ?? ""
                            this?.friendId.append(friendId)
                            this?.friendUsername.append(name)
                            this?.friendProfilePicture.append(profilePhoto.replacingOccurrences(of: " ", with: "%20"))
                        } // end of fucking loop
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss(completion: {
                                this?.tableView.reloadData()
                            })
                        }
                    })
                } else if statusCode == 204 {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No available friend.")
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
                break
            case .failure(let error):
                SVProgressHUD.dismiss(completion: {
                    this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                })
                break
            }
        }
    }
    
    private func loadFriendListWithPagination(currentPage: Int) {
        let token = KeychainSwift().get("apiToken") ?? ""
        guard let url = URL(string: "\(Constants.Routes.BASE)/api/v4/ios/user/group/search?page=\(currentPage)") else { return }
        
        let parameters = "keyword=&group_id="
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        request.httpBody = parameters.data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let userObject = (try? JSONSerialization.jsonObject(with: data!, options: [])) {
                
                guard let jsonDic = userObject as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                guard let data = jsonDic["data"] as? [String: Any] else { return }
                guard let jsonData = data["data"] as? [[String: Any]] else { return }
                
                if statusCode == 200 {
                    
                    let currentpage = data["current_page"] as? Int ?? 0
                    self.currentPage = currentpage
                    let nextPage = data["next_page_url"] as? String ?? ""
                    if nextPage.isEmpty {
                        self.didHaveNextPage = false
                    } else {
                        self.didHaveNextPage = true
                    }
                    
                    SVProgressHUD.dismiss(completion: {
                        for data in jsonData {
                            let friendId = data["id"] as? Int ?? 0
                            let name = data["name"] as? String ?? ""
                            let profilePhoto = data["image"] as? String ?? ""
                            self.friendId.append(friendId)
                            self.friendUsername.append(name)
                            self.friendProfilePicture.append(profilePhoto.replacingOccurrences(of: " ", with: "%20"))
                        } // end of fucking loop
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss(completion: {
                                self.tableView.reloadData()
                            })
                        }
                    })
                } else {
                    SVProgressHUD.dismiss(completion: {
                        self.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                }
                
        }}.resume()
    }
}

extension ChooseMemberViewController {
    
    private func manageSelect(indexPath: IndexPath) {
        let names = self.friendUsername[indexPath.row]
        let images = self.friendProfilePicture[indexPath.row]
        let ids = self.friendId[indexPath.row]
        
        print("selected items: \(names), \(images), \(ids)")
        
        if names.isEmpty || images.isEmpty {
            
        } else {
            if !self.selectedNameArr.contains(names) {
                self.selectedNameArr.append(self.friendUsername[indexPath.row])
            }
            
            if !self.selectedProfilePicArr.contains(images) {
                self.selectedProfilePicArr.append(self.friendProfilePicture[indexPath.row])
            }

            if !self.selectedIdArr.contains(ids) {
                self.selectedIdArr.append(self.friendId[indexPath.row])
            }
        }
    }
    
    private func manageDeSelect(indexPath: IndexPath) {
        if let index = selectedNameArr.index(of: friendUsername[indexPath.row]) {
            selectedNameArr.remove(at: index)
        }
        
        if let index = selectedProfilePicArr.index(of: friendProfilePicture[indexPath.row]) {
            selectedProfilePicArr.remove(at: index)
        }
        
        if let index = selectedIdArr.index(of: friendId[indexPath.row]) {
            selectedIdArr.remove(at: index)
        }
    }
    
    private func buttonTappedOnCell(cell: ChooseMemberTableViewCell) {
        if let indexPath = self.tableView.indexPath(for: cell) {
            var image = UIImage(named: "ic_check_mark")
            if self.selectedPaths.contains(indexPath) {
                image = UIImage(named: "")
                self.selectedPaths.remove(indexPath)
                self.manageDeSelect(indexPath: indexPath)
            } else {
                self.selectedPaths.insert(indexPath)
                self.manageSelect(indexPath: indexPath)
            }
            
            cell.checkMark.setBackgroundImage(image, for: .normal)
        }
    }
}
