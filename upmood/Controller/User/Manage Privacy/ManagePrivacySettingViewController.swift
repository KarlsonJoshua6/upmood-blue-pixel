//
//  ManagePrivacySettingViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 05/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class ManagePrivacySettingViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private var menu = [NSLocalizedString("Profile Details", comment: ""), NSLocalizedString("Current Emotion", comment: ""), NSLocalizedString("Heart rate", comment: ""), NSLocalizedString("Show all", comment: ""), NSLocalizedString("Hide all", comment: "")]
    private var didSelectAll = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func done(_ sender: UIButton) {
        
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ManagePrivacySettingViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountSettingTableViewCell", for: indexPath) as? AccountSettingTableViewCell {
            cell.managePrivacyLabel.text = menu[indexPath.row]
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
            
            if indexPath.row == 3 {
                self.didSelectAll = true
            } else if indexPath.row == 4 {
                if let cell = tableView.cellForRow(at: indexPath) {
                    cell.accessoryType = .none
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
    }
}
