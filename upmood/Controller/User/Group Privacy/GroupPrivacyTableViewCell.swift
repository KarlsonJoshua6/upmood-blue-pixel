//
//  GroupPrivacyTableViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 30/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class GroupPrivacyTableViewCell: UITableViewCell {

    @IBOutlet weak var checkmark: UIButton!
    @IBOutlet weak var checkMark: UIImageView!
    @IBOutlet weak var managePrivacyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
