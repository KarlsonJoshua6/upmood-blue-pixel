//
//  GroupPrivacyViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 30/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import SVProgressHUD
import UIKit

class GroupPrivacyViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var menu:[[String:String]] = []
    private var selectedRows:[IndexPath] = []
    private var didUserSelectOnGroupPrivacy = false
    
    // helper
    var privacyProfileDetails: Int!
    var privacyEmotion: Int!
    var privacyHeartBeat: Int!
    var groupId: Int!
    
    // store value
    private var privacyProfileDetailsSelected = 0
    private var privacyEmotionSelected = 0
    private var privacyHeartBeatSelected = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        privacyProfileDetails = UserDefaults.standard.integer(forKey: "fetchPrivacyProfileDetails")
        privacyEmotion = UserDefaults.standard.integer(forKey: "fetchPrivacyEmotion")
        privacyHeartBeat = UserDefaults.standard.integer(forKey: "fetchPrivacyHeartBeat")
        
        menu = [["menu": NSLocalizedString("Profile Details", comment: "")],["menu": NSLocalizedString("Current Emotion", comment: "")],["menu":NSLocalizedString("Heart rate", comment: "")],["menu": NSLocalizedString("Show all", comment: "")],["menu":NSLocalizedString("Hide all", comment: "")]]
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelection = true
        setUserInterfaceStyleLight(self: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func done(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func removeAllIndexPath() -> [IndexPath] {
        var indexPaths: [IndexPath] = []
        for j in 0..<tableView.numberOfRows(inSection: 0) - 5 {
            indexPaths.append(IndexPath(row: j, section: 0))
        }
        
        return indexPaths
    }
    
    func getAllIndexPaths() -> [IndexPath] {
        var indexPaths: [IndexPath] = []
        for j in 0..<tableView.numberOfRows(inSection: 0) - 2 {
            indexPaths.append(IndexPath(row: j, section: 0))
        }
        
        return indexPaths
    }
    
    private func loadCell(cell: GroupPrivacyTableViewCell, indexpath: IndexPath) {
        
        if self.privacyProfileDetails ?? 0 == 1 {
            if indexpath.row == 0 {
                cell.checkMark.isHidden = false
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if self.privacyEmotion ?? 0 == 1 {
            if indexpath.row == 1 {
                cell.checkMark.isHidden = false
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
        
        if self.privacyHeartBeat ?? 0 == 1 {
            if indexpath.row == 2 {
                cell.checkMark.isHidden = false
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
        }
    }
    
    private func setUpcell(cell: GroupPrivacyTableViewCell, indexPath: IndexPath) {
        cell.checkMark.isHidden = true
        loadCell(cell: cell, indexpath: indexPath)
        
        if indexPath.row == 3 && indexPath.row == 4 {
            cell.checkMark.isHidden = true
        }
    }
    
    private func manageDidDeSelect(cell: GroupPrivacyTableViewCell, index: IndexPath) {
        switch index.row {
        case 0:
            cell.checkMark.isHidden = true
            cell.checkMark.image = UIImage(named: "")
            self.privacyProfileDetails = 0
            self.updateGroupPrivacySettings(withGroupId: self.groupId ?? 0, type: "notification_type", heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 1:
            cell.checkMark.isHidden = true
            cell.checkMark.image = UIImage(named: "")
            self.privacyEmotion = 0
                 self.updateGroupPrivacySettings(withGroupId: self.groupId ?? 0, type: "notification_type", heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 2:
            cell.checkMark.isHidden = true
            cell.checkMark.image = UIImage(named: "")
            self.privacyHeartBeat = 0
            self.updateGroupPrivacySettings(withGroupId: self.groupId ?? 0, type: "notification_type", heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        default:
            break
        }
    }
    
    private func manageSelection(cell: GroupPrivacyTableViewCell, index: IndexPath) {
        switch index.row {
        case 0:
            cell.checkMark.isHidden = false
            cell.checkMark.image = UIImage(named: "ic_check_mark")
            self.privacyProfileDetails = 1
            self.updateGroupPrivacySettings(withGroupId: self.groupId ?? 0, type: "notification_type", heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 1:
            cell.checkMark.isHidden = false
            cell.checkMark.image = UIImage(named: "ic_check_mark")
            self.privacyEmotion = 1
            self.updateGroupPrivacySettings(withGroupId: self.groupId ?? 0, type: "notification_type", heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 2:
            cell.checkMark.isHidden = false
            cell.checkMark.image = UIImage(named: "ic_check_mark")
            self.privacyHeartBeat = 1
            self.updateGroupPrivacySettings(withGroupId: self.groupId ?? 0, type: "notification_type", heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            break
        case 3:
            self.selectedRows = getAllIndexPaths()
            
            if index.row == 0 && index.row == 1 && index.row == 2 {
                cell.checkMark.isHidden = false
                cell.checkMark.image = UIImage(named: "ic_check_mark")
            }
            
            self.privacyProfileDetails = 1
            self.privacyEmotion = 1
            self.privacyHeartBeat = 1
            self.updateGroupPrivacySettings(withGroupId: self.groupId ?? 0, type: "notification_type", heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            self.tableView.reloadData()
            break
        case 4:
            self.selectedRows = removeAllIndexPath()
            
            if index.row == 0 && index.row == 1 && index.row == 2 {
                cell.checkMark.isHidden = true
                cell.checkMark.image = UIImage(named: "")
            }
            
            self.privacyProfileDetails = 0
            self.privacyEmotion = 0
            self.privacyHeartBeat = 0
            self.updateGroupPrivacySettings(withGroupId: self.groupId ?? 0, type: "notification_type", heartbeat: privacyHeartBeat, stress_level: 0, emotion: privacyEmotion, my_mood: privacyProfileDetails)
            self.tableView.reloadData()
            break
        default:
            break
        }
    }
}

extension GroupPrivacyViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "GroupPrivacyTableViewCell", for: indexPath) as? GroupPrivacyTableViewCell {
            cell.managePrivacyLabel.text = menu[indexPath.row]["menu"]
            setUpcell(cell: cell, indexPath: indexPath)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? GroupPrivacyTableViewCell {
            if cell.checkMark.image == UIImage(named: "") {
                self.manageSelection(cell: cell, index: indexPath)
            } else {
                self.manageDidDeSelect(cell: cell, index: indexPath)
            }
        }
    }
}

extension GroupPrivacyViewController {
    private func updateGroupPrivacySettings(withGroupId: Int, type: String, heartbeat: Int, stress_level: Int, emotion: Int, my_mood: Int) {

        SVProgressHUD.show()
        SVProgressHUD.setForegroundColor(hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR))
        let selectedEmotions = UserDefaults.standard.string(forKey: "fetchEmotions") ?? ""
        let notificationValue = UserDefaults.standard.string(forKey: "fetchNotificationType") ?? ""
        
        Alamofire.request(APIClient.editGroup(withGroupId: withGroupId, type: type, value: notificationValue, type_data: "", emotions: selectedEmotions, heartbeat: heartbeat, stress_level: stress_level, emotion: emotion, my_mood: my_mood)).responseJSON { [weak this = self] response in
            switch response.result {
            case .success(let json):
                guard let jsonDic = json as? [String: Any] else { return }
                let statusCode = jsonDic["status"] as? Int ?? 0
                
                switch statusCode {
                case 200:
                    
                    SVProgressHUD.dismiss(completion: {
                        UserDefaults.standard.set(my_mood, forKey: "fetchPrivacyProfileDetails")
                        UserDefaults.standard.set(emotion, forKey: "fetchPrivacyEmotion")
                        UserDefaults.standard.set(heartbeat, forKey: "fetchPrivacyHeartBeat")
                        
                        self.privacyProfileDetails = my_mood
                        self.privacyEmotion = emotion
                        self.privacyHeartBeat = heartbeat
                        print("success: \(my_mood), \(emotion), \(heartbeat)")
                        self.tableView.reloadData()
                    })

                    break
                case 204:
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Group Privacy did not successfully update, Please try again.")
                    })
                    break
                default:
                    SVProgressHUD.dismiss(completion: {
                        this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: nil)
                    })
                    break
                }
                break
            case .failure(let error):
                this?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: error.localizedDescription)
                break
            }
        }
    }
}
