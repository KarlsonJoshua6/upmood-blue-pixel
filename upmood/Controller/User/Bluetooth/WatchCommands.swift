//
//  WatchCommands.swift
//  upmood
//
//  Created by Jason Juliane on 9/17/20.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

class WatchCommands {
    
//    COMMAND RESULTS GOES TO didReadValueForCharacteristic
//    SUCESS RETURN = DATA0  || Ex. setTime data0 = 0x01, success = 0x01
    
//    ALL GET() Formats = same format as set
    
//    AA = year, BB = month, CC = day, DD = hour, EE = minute, FF = second
//    Ea. 12 year, AA = 0x12
    
    func setTime(year: UInt8, month: UInt8, day: UInt8, hour: UInt8, minute: UInt8, second: UInt8) -> [UInt8]{
        return setCharacteristicData(data0: 0x01, data1: year, data2: month, data3: day, data4: hour, data5: minute, data6: second)
    }
    
    func getTime() -> [UInt8]{
        return setCharacteristicData(data0: 0x41)
    }
    
    func setUserPersonalInformation(gender: UInt8, age: UInt8, height: UInt8, weight: UInt8, strideLength: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x02, data1: gender, data2: age, data3: height, data4: weight, data5: strideLength)
    }
    
    func getUserPersonalInformation() -> [UInt8] {
        return setCharacteristicData(data0: 0x42)
    }
    
//    AA = distance unit, 0x81 MILE, 0x80 KM
//    BB = hour format, 0x81 12hour, 0x80 24hour
//    CC = wrist sense, 0x81 enable, 0x80, disable
//    DD, EE = reserved field
//    FF = ancs, 0x81 enable, 0x80 disable
    func setDeviceBasicParameters(distanceUnit: UInt8, hourFormat: UInt8, wristSense: UInt8, language: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x03, data1: distanceUnit, data2: hourFormat, data3: wristSense, data4: 0x00, data5: 0x00, data6: 0x00, data7: 0x00, data8: 0x00, data9: 0x00, data10: 0x00, data11: 0x00, data12: language)
    }
    
    func getDeviceParameters() -> [UInt8] {
        return setCharacteristicData(data0: 0x04)
    }
    
//    6-byte ID code arrangement is high byte first
    func setDeviceIDCode(id5: UInt8, id4: UInt8, id3: UInt8, id2: UInt8, id1: UInt8, id0: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x05, data1: id5, data2: id4, data3: id3, data4: id2, data5: id1, data6: id0)
    }
    
//    0x01 = start real step mode, 0x00 = close real step mode
    func realtimeStepMode(mode: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x09, data1: mode)
    }
    
//    AA BB CC DD is the target steps value, 4 bytes, low byte in front
    func setTargetSteps(step1: UInt8, step2: UInt8, step3: UInt8, step4: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x0B, data1: step1, data2: step2, data3: step3, data4: step4)
    }
    
    func getTargetSteps() -> [UInt8] {
         return setCharacteristicData(data0: 0x4B)
    }
    
//    command AA: 0X99, now perform a power check
//    return AA means battery level, range is from 0 to 100, stands for the battery %
    func readDeviceBattery(powerCheck: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x13, data1: powerCheck)
    }
    
//    returns 0x22 MAC0 MAC1 MAC2 MAC3 MAC4 MAC5 00 00 00 00 00
    func readMacAddress() -> [UInt8] {
        return setCharacteristicData(data0: 0x22)
    }
    
//    AA BB CC DD are software version number (BCD code)
//    EE FF GG are time
    func readSoftwareVersionNumber() -> [UInt8] {
          return setCharacteristicData(data0: 0x27)
    }
    
    func factoryReset() -> [UInt8] {
        return setCharacteristicData(data0: 0x12)
    }

    func MCUResetCommand() -> [UInt8] {
        return setCharacteristicData(data0: 0x2E)
    }
    
    func sendMotorVibrationSignal() -> [UInt8] {
        return setCharacteristicData(data0: 0x36)
    }
    
//    AA -> NN is the device name (it must be ASCII byte code, 32 to 127, if send other data will be regarded as Blank)
    func setBluetoothName(name0: UInt8, name1: UInt8, name2: UInt8, name3: UInt8, name4: UInt8, name5: UInt8, name6: UInt8, name7: UInt8, name8: UInt8, name9: UInt8, name10: UInt8, name11: UInt8, name12: UInt8, name13: UInt8) -> [UInt8]{
       
        return setCharacteristicData(data0: 0x3D, data1: name0, data2: name1, data3: name2, data4: name3, data5: name4, data6: name5, data7: name6, data8: name7, data9: name8, data10: name9, data11: name10, data12: name11, data13: name12, data14: name13)
    }
    
//    AA -> NN is the device name (it must be ASCII byte code, 32 to 127, if send other data will be regarded as Blank)
    func readBluetoothNameOfDevice() -> [UInt8] {
        return setCharacteristicData(data0: 0x3E)
    }
    
//    Refer to docu for this one, too long
    func setAutoHeartRateMonitoringPeriod(workMode: UInt8, hourStart: UInt8, minuteStart: UInt8, hourEnd: UInt8, minuteEnd: UInt8, weekBit: UInt8,
                                          testTime0: UInt8, testTime1: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x2A, data1: workMode, data2: hourStart, data3: minuteStart, data4: hourEnd, data5: minuteEnd, data6: weekBit,
                                     data7: testTime0, data8: testTime1)
    }
    
    func readAutoHeartRateMonitoringPeriod() -> [UInt8] {
        return setCharacteristicData(data0: 0x2B)
    }
    
//    TODO NEED HELP
//    func setAlarm() -> [UInt8] {
//
//    }
    
    func notification() -> [UInt8] {
        return setCharacteristicData(data0: 0x4D)
    }
    
//    Refer to docu for this one, too long
    func setActivityPeriod(hourActivityStart: UInt8, minuteActivityStart: UInt8, hourActivityEnd: UInt8, minuteActivityEnd: UInt8, weekBit: UInt8,
                           exerciseReminder: UInt8, minSteps: UInt8, reminderSwitchSetting: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x25)
    }
    
    func readActivityPeriod() -> [UInt8] {
        return setCharacteristicData(data0: 0x26)
    }

//    AA: 99: delete all history data, 0:read all data。
    func getTotalStepsData(readOrDelete: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x51, data1: readOrDelete)
    }
    
//    Refer to docu as well, important details
//    AA =
//    99: delete all history data
//    0: read the latest steps detailed data
//    1: read specified steps detailed data (BB CC means specified, low byte in front)
//    2: continue the next steps data of the last read position
    func getDetailedStepsData(readOrDelete: UInt8, lowByte: UInt8, highByte: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x52, data1: readOrDelete, data2: lowByte, data3: highByte)
    }
    
//    Refer to docu as well, important details
//    AA =
//    99: delete the detailed sleep data
//    0: read the latest detailed sleep data
//    1: read sleep details at a specified location (BB CC means specified, low byte in front)
//    2: continue the next steps data of the last read position
    func getDetailedStepsDataV2(readOrDelete: UInt8, lowByte: UInt8, highByte: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x53, data1: readOrDelete, data2: lowByte, data3: highByte)
    }
    
//    Refer to docu as well, important details
//    AA =
//    99: delete all HR data
//    0: read the latest heart rate data
//    1: read specified heart rate data (BB CC means specified, low byte in front)
//    2: continue the next steps data of the last read position
    func getHeartRateData(readOrDelete: UInt8, lowByte: UInt8, highByte: UInt8) -> [UInt8] {
          return setCharacteristicData(data0: 0x54, data1: readOrDelete, data2: lowByte, data3: highByte)
    }
    
//    Refer to docu as well, important details
//    AA =
//    99: delete all single HR data
//    0: read the latest single heart rate data
//    1: read specified single heart rate data (BB CC means specified, low byte in front)
//    2: continue the next steps data of the last read position
    func getSingleHRData(readOrDelete: UInt8, lowByte: UInt8, highByte: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x55, data1: readOrDelete, data2: lowByte, data3: highByte)
    }
    
//    AA: 99: delete all alarm data 00: read all alarm data
    func getAlarmData(readOrDelete: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x57, data1: readOrDelete)
    }
    
//    Refer to docu as well, important details
//    AA = 99: sleep debugging data
//    = 1: read Specified sleep debugging data
//    = 2: Continue the next paragraph data of the last read position
    func getSleepDebuggingData(sleepData: UInt8, AA: UInt8, BB: UInt8, CC: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x58, data1: sleepData, data2: AA, data3: BB, data4: CC)
    }
    

//    Refer to docu as well, important details
//    AA = 99: system debugging data
//    = 1: read Specified system debugging data
//    = 2: Continue the next paragraph data of the last read position
    func getSystemDebuggingData(systemData: UInt8, AA: UInt8, BB: UInt8, CC: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x59, data1: systemData, data2: AA, data3: BB, data4: CC)
    }
    

//    TODO NEED HELP
//    func combinedPacketReceiveAndDispatch() -> [UInt8] {
//
//    }
    
//    AA = 1: turn on HR monitoring,
//    BB = 1: turn on HR module; BB = 2: turn off HR module
    func appControlHRMonitoringFunction(hrPPG: UInt8, module: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x19, data1: hrPPG, data2: module)
    }
    
//    Refer to docu as well
//    AA: Number of PPI value generated in 5 seconds.
    func ppiData(ppiValuePer5Seconds: UInt8) -> [UInt8] {
       return setCharacteristicData(data0: 0x16, data1: ppiValuePer5Seconds)
    }
    
//    When AA = 0x01, BB value is stress value;
//    When AA = 0x02, BB value is mood value.
    func stressLevelAndMoodValue(mode: UInt8, value: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x17, data1: mode, data2: value)
    }
    
//    AA=0x01 auto sleep monitor
//    AA=0x00 turn of auto sleep monitor
    func sleepModeSwitch(mode: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x23, data1: mode)
    }
    
    func toCameraMode()-> [UInt8] {
        return setCharacteristicData(data0: 0x20)
    }
    
    func turnOffCamera()-> [UInt8] {
        return setCharacteristicData(data0: 0x10)
    }

//    AA=value, 1: Anxious, 2: Calm, 3: Challenged, 4: Confused, 5: Excited, 6: Happy, 7: Tense, 8: Loading, 9: Pleasant, 10: sad, 11: Unpleasant, 12: Zen
//    AA=99: check current settings
    func setCurrentMood(mood: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x06, data1: mood, data2: 0x01)
    }

//    AA=value, 1: low, 2: mild, 3: normal, 4: high
//    AA=99: set current settings
    func setStressLevel(stressLevel: UInt8) -> [UInt8] {
        return setCharacteristicData(data0: 0x07, data1: stressLevel)
    }

//    Function: Receive SMS of lover’s mode. After receiving this command, the device will enter the mode. AA: value, 0x01
    func receiveSMSLoversMode() -> [UInt8] {
        return setCharacteristicData(data0: 0x0C, data1: 0x01)
    }
    
//    Function: Send out SMS of lover’s mode. The device will send this command when it’s under this mode.
    func sendSMSLoversMode() -> [UInt8] {
        return setCharacteristicData(data0: 0x0C, data1: 0x05, data2: 0x02)
    }
    
//    Refer to docu as well, important details
    func getBPM(bpm: UInt8) -> [UInt8] {
       // return setCharacteristicData(data0: 0x0D, data1: 0x01, data2: 0x3C)
        return setCharacteristicData(data0: 0x0D, data1: 0x01, data2: bpm)
    }
      
    func setCharacteristicData(data0: UInt8 = 0x00, data1: UInt8 = 0x00, data2: UInt8 = 0x00, data3: UInt8 = 0x00, data4: UInt8 = 0x00, data5: UInt8 = 0x00, data6: UInt8 = 0x00, data7: UInt8 = 0x00, data8: UInt8 = 0x00, data9: UInt8 = 0x00, data10: UInt8 = 0x00, data11: UInt8 = 0x00, data12: UInt8 = 0x00, data13: UInt8 = 0x00, data14: UInt8 = 0x00) -> [UInt8] {
        
        return [data0, data1, data2, data3, data4, data5, data6, data7, data8, data9, data10, data11, data12, data13, data14, (data0 + data1 + data2 + data3 + data4 + data5 + data6 + data7 + data8 + data9 + data10 + data11 + data12 + data13 + data14 ) & 0xff]
    }
    
    func date(date: String) ->UInt8{
        var hex: UInt8?
        
        switch date {
        case "00":
            hex = 0x00
        case "01":
            hex = 0x01
        case "02":
            hex = 0x02
        case "03":
            hex = 0x03
        case "04":
            hex = 0x04
        case "05":
            hex = 0x05
        case "06":
            hex = 0x06
        case "07":
            hex = 0x07
        case "08":
            hex = 0x08
        case "09":
            hex = 0x09
        case "10":
            hex = 0x10
        case "11":
            hex = 0x11
        case "12":
            hex = 0x12
        case "13":
            hex = 0x13
        case "14":
            hex = 0x14
        case "15":
            hex = 0x15
        case "16":
            hex = 0x16
        case "17":
            hex = 0x17
        case "18":
            hex = 0x18
        case "19":
            hex = 0x19
        case "20":
            hex = 0x20
        case "21":
            hex = 0x21
        case "22":
            hex = 0x22
        case "23":
            hex = 0x23
        case "24":
            hex = 0x24
        case "25":
            hex = 0x25
        case "26":
            hex = 0x26
        case "27":
            hex = 0x27
        case "28":
            hex = 0x28
        case "29":
            hex = 0x29
        case "30":
            hex = 0x30
        case "31":
            hex = 0x31
        case "32":
            hex = 0x32
        case "33":
            hex = 0x33
        case "34":
            hex = 0x34
        case "35":
            hex = 0x35
        case "36":
            hex = 0x36
        case "37":
            hex = 0x37
        case "38":
            hex = 0x38
        case "39":
            hex = 0x39
        case "40":
            hex = 0x40
        case "41":
            hex = 0x41
        case "42":
            hex = 0x42
        case "43":
            hex = 0x43
        case "44":
            hex = 0x44
        case "45":
            hex = 0x45
        case "46":
            hex = 0x46
        case "47":
            hex = 0x47
        case "48":
            hex = 0x48
        case "49":
            hex = 0x49
        case "50":
            hex = 0x50
        case "51":
            hex = 0x51
        case "52":
            hex = 0x52
        case "53":
            hex = 0x53
        case "54":
            hex = 0x54
        case "55":
            hex = 0x55
        case "56":
            hex = 0x56
        case "57":
            hex = 0x57
        case "58":
            hex = 0x58
        case "59":
            hex = 0x59
        case "60":
            hex = 0x60
        default:
            break
        }
        return hex!
    }
    
}

