//
//  WatchDataProcessor.swift
//  upmood
//
//  Created by Taison Digital on 01/10/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import CoreBluetooth
import SwiftyJSON
import Alamofire

class WatchDataProcessor {
    
    var byteArray : [UInt8] = []
    var byteArrayData : [UInt8] = []
    var byteOfCalAndStep1 : [UInt8] = []
    var byteOfCalAndStep2 : [UInt8] = []
    var byteOfCalAndStep3 : [UInt8] = []
    var byteOfCalAndStep4 : [UInt8] = []
    var byteOfCalAndStep5 : [UInt8] = []
    var byteOfCalAndStep6 : [UInt8] = []
    
    var setOfArray1: [String: Any] = [:]
    var setOfArray2: [String: Any] = [:]
    var setOfArray3: [String: Any] = [:]
    var setOfArray4: [String: Any] = [:]
    var setOfArray5: [String: Any] = [:]
    var setOfArray6: [String: Any] = [:]
    
    var arraySave = [[String: Any]]()

    
    //let dataProcessor = HRMDataProcessor()
    var calStepInt = 0
    
    var savedJSONData = [JSON]()
    var jsonStringArr = [String]()
    var didCallAPI = false
    
    var data = 0 , x4data = 0
    
    func bytesToString(byteArray: [UInt8], writeChannel: CBCharacteristic!, bluetoothManager: BluetoothManager) {
        
        let bytes = String(byteArray[0], radix: 16)
        print("received bytes response: \(bytes)")
        
        switch bytes {
        case "13":
           
            self.byteArray = byteArray
            NotificationCenter.default.post(name: .batteryPercent, object: nil, userInfo: ["percent": self.byteArray[1]])
            
            turnOnSensor(writeChannel: writeChannel, bluetoothManager: bluetoothManager)
            
        case "1":
            var command:[UInt8]
            
            command = WatchCommands().readDeviceBattery(powerCheck: 0x99)
           // setCurrentMood(mood: 0x03, writeChannel: writeChannel, bluetoothManager: bluetoothManager)
           // setStressLevel(stressLevel: 0x02, writeChannel: writeChannel, bluetoothManager: bluetoothManager)
            getPPIData(ppiValuePer5Seconds: 0x01, writeChannel: writeChannel , bluetoothManager: bluetoothManager)
          
            
            if writeChannel != nil {
                print("Command test.")
                bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: writeChannel!, type: .withResponse)
            } else {
                print("No channel found.")
            }
        case "16":
            //dataProcessor.convertBytesToHex(byteArray: byteArray, watch: true, writeChannel: writeChannel, bluetoothManager: bluetoothManager)
            break
        case "c":
            
            if Constants.receivedCommand == true {
                debugPrint("LOVERS MODE RECEIVER")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    Constants.receivedCommand = false
                }
            } else {
                debugPrint("LOVERS MODE SENDER")
                requestFCMWatch()
            }
        case "51":

            print("byteArray.count", byteArray.count)
            removeArrayByte()
            calStepInt += 1
            
            
            print("calStepInt", calStepInt)
            

            
            if calStepInt == 1 {
                
                if byteArray.count <= 26 {
                    print("LESS VALUE")
                }
                
                else if byteArray.count <= 29 {
                    print("FIRST APPEND")
                    firstAppend(byteArray: byteArray)
                    getArrayOfBytes(index: 1)
                }
                
                else if byteArray.count <= 56 {
                    print("Second APPEND")
                    firstAppend(byteArray: byteArray)
                    secondAppend(byteArray: byteArray)
                    getArrayOfBytes(index: 2)
                }
                
                else if byteArray.count <= 83 {
                    print("third APPEND")
                    firstAppend(byteArray: byteArray)
                    secondAppend(byteArray: byteArray)
                    thirdAppend(byteArray: byteArray)
                    getArrayOfBytes(index: 3)
                }
                
                else if byteArray.count <= 110 {
                    print("fourth APPEND")
                    firstAppend(byteArray: byteArray)
                    secondAppend(byteArray: byteArray)
                    thirdAppend(byteArray: byteArray)
                    fourthAppend(byteArray: byteArray)
                    getArrayOfBytes(index: 4)
                }
                
                else if byteArray.count <= 137 {
                    print("fifth APPEND")
                    firstAppend(byteArray: byteArray)
                    secondAppend(byteArray: byteArray)
                    thirdAppend(byteArray: byteArray)
                    fourthAppend(byteArray: byteArray)
                    fifthAppend(byteArray: byteArray)
                    getArrayOfBytes(index: 5)
                }
                
                else  if byteArray.count <= 162 {
                    print("sixth APPEND")
                    firstAppend(byteArray: byteArray)
                    secondAppend(byteArray: byteArray)
                    thirdAppend(byteArray: byteArray)
                    fourthAppend(byteArray: byteArray)
                    fifthAppend(byteArray: byteArray)
                    sixthAppend(byteArray: byteArray)
                    getArrayOfBytes(index: 6)
                }
                
                calStepInt = 0
            }
            
        default:
            break
        }
    }
    
    func sendSMSLover(writeChannel: CBCharacteristic!, bluetoothManager: BluetoothManager){
        var command:[UInt8]
        
        command = WatchCommands().sendSMSLoversMode()
        
        if writeChannel != nil {
            bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: writeChannel!, type: .withResponse)
        } else {
            if savedWriteChannel != nil {
                bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: savedWriteChannel!, type: .withResponse)
            } else {
                print("No channel found talaga.")
            }
        }
    }
    
    func turnOnSensor(writeChannel: CBCharacteristic!, bluetoothManager: BluetoothManager) {
        var command:[UInt8]
        
        command = WatchCommands().appControlHRMonitoringFunction(hrPPG: 0x02, module: 0x01)
        
        if writeChannel != nil {
            bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: writeChannel!, type: .withResponse)
        } else {
            if savedWriteChannel != nil {
                bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: savedWriteChannel!, type: .withResponse)
            } else {
                print("No channel found talaga.")
            }
        }
    }
    
    func getPPIData(ppiValuePer5Seconds: UInt8, writeChannel: CBCharacteristic!, bluetoothManager: BluetoothManager) {
        
        var command:[UInt8]
               
        command = WatchCommands().ppiData(ppiValuePer5Seconds: ppiValuePer5Seconds)
               
        if writeChannel != nil {
            bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: writeChannel!, type: .withResponse)
        } else {
            if savedWriteChannel != nil {
                bluetoothManager.connectedPeripheral!.writeValue(Data(command), for: savedWriteChannel!, type: .withResponse)
            } else {
                print("No channel found talaga.")
            }
        }
    }
        
}

extension WatchDataProcessor {
    
    private func removeArrayByte(){
        self.byteOfCalAndStep1.removeAll()
        self.byteOfCalAndStep2.removeAll()
        self.byteOfCalAndStep3.removeAll()
        self.byteOfCalAndStep4.removeAll()
        self.byteOfCalAndStep5.removeAll()
        self.byteOfCalAndStep6.removeAll()
        self.setOfArray1.removeAll()
        self.setOfArray2.removeAll()
        self.setOfArray3.removeAll()
        self.setOfArray4.removeAll()
        self.setOfArray5.removeAll()
        self.setOfArray6.removeAll()
        self.arraySave.removeAll()
        self.savedJSONData.removeAll()
        self.jsonStringArr.removeAll()
    }
    
    private func getArrayOfBytes(index: Int){
        
        switch index {
        case 1:
            arraySave1()
            addSteps(arraySave: arraySave)
        case 2:
            arraySave1()
            arraySave2()
            addSteps(arraySave: arraySave)
        case 3:
            arraySave1()
            arraySave2()
            arraySave3()
            addSteps(arraySave: arraySave)
        case 4:
            arraySave1()
            arraySave2()
            arraySave3()
            arraySave4()
            addSteps(arraySave: arraySave)
        case 5:
            arraySave1()
            arraySave2()
            arraySave3()
            arraySave4()
            arraySave5()
            addSteps(arraySave: arraySave)
        case 6:
            arraySave1()
            arraySave2()
            arraySave3()
            arraySave4()
            arraySave5()
            arraySave6()
            addSteps(arraySave: arraySave)
        default:
            break
        }
      
        print("TEST1", arraySave)
        
        //addSteps()
        
    }
    
    func arraySave1() {
        let year1 = String(self.byteOfCalAndStep1[2], radix: 16)
        let month1 = String(self.byteOfCalAndStep1[3], radix: 16)
        let day1 = String(self.byteOfCalAndStep1[4], radix: 16)
        let steps1 = self.byteOfCalAndStep1[5]
       // let calories1 = String(format: "%.2f", self.byteOfCalAndStep1[17])
        let calories1 = Double(self.byteOfCalAndStep1[17])
        
        let date1 = "20\(year1)-\(dateConvert(day: month1))-\(dateConvert(day: day1))"
        
        setOfArray1 = ["calories": String(format:"%.2f", calories1), "date": date1, "steps": steps1]
        arraySave.append(setOfArray1)
    }
    
    func arraySave2() {
        let year2 = String(self.byteOfCalAndStep2[2], radix: 16)
        let month2 = String(self.byteOfCalAndStep2[3], radix: 16)
        let day2 = String(self.byteOfCalAndStep2[4], radix: 16)
        let steps2 = self.byteOfCalAndStep2[5]
       // let calories2 = String(format: "%.2 `f",self.byteOfCalAndStep2[17])
        let calories2 = Double(self.byteOfCalAndStep2[17])
        
        let date2 = "20\(year2)-\(dateConvert(day: month2))-\(dateConvert(day: day2))"
        
        setOfArray2 = ["calories": String(format:"%.2f", calories2) , "date": date2, "steps": steps2]
        arraySave.append(setOfArray2)
    }
    
    func arraySave3(){
        let year3 = String(self.byteOfCalAndStep3[2], radix: 16)
        let month3 = String(self.byteOfCalAndStep3[3], radix: 16)
        let day3 = String(self.byteOfCalAndStep3[4], radix: 16)
        let steps3 = self.byteOfCalAndStep3[5]
    //    let calories3 = String(format: "%.2f",self.byteOfCalAndStep3[17])
        let calories3 = Double(self.byteOfCalAndStep3[17])
        
        let date3 = "20\(year3)-\(dateConvert(day: month3))-\(dateConvert(day: day3))"
        
        setOfArray3 = ["calories": String(format:"%.2f", calories3) , "date": date3, "steps": steps3]
        arraySave.append(setOfArray3)
    }
    
    func arraySave4(){
        let year4 = String(self.byteOfCalAndStep4[2], radix: 16)
        let month4 = String(self.byteOfCalAndStep4[3], radix: 16)
        let day4 = String(self.byteOfCalAndStep4[4], radix: 16)
        let steps4 = self.byteOfCalAndStep4[5]
//        let calories4 = String(format: "%.2f",self.byteOfCalAndStep4[17])
        let calories4 = Double(self.byteOfCalAndStep4[17])
        
        let date4 = "20\(year4)-\(dateConvert(day: month4))-\(dateConvert(day: day4))"
        
        setOfArray4 = ["calories": String(format:"%.2f", calories4), "date": date4, "steps": steps4]
        arraySave.append(setOfArray4)
    }
    
    func arraySave5() {
        let year5 = String(self.byteOfCalAndStep5[2], radix: 16)
        let month5 = String(self.byteOfCalAndStep5[3], radix: 16)
        let day5 = String(self.byteOfCalAndStep5[4], radix: 16)
        let steps5 = self.byteOfCalAndStep5[5]
   //     let calories5 = String(format: "%.2f",self.byteOfCalAndStep5[17])
        let calories5 = Double(self.byteOfCalAndStep5[17])
        
        let date5 = "20\(year5)-\(dateConvert(day: month5))-\(dateConvert(day: day5))"
        
        setOfArray5 = ["calories": String(format:"%.2f", calories5) , "date": date5, "steps": steps5]
        arraySave.append(setOfArray5)
    }
    
    func arraySave6() {
        let year6 = String(self.byteOfCalAndStep6[2], radix: 16)
        let month6 = String(self.byteOfCalAndStep6[3], radix: 16)
        let day6 = String(self.byteOfCalAndStep6[4], radix: 16)
        let steps6 = self.byteOfCalAndStep6[5]
        //let calories6 = String(format: "%.2f",self.byteOfCalAndStep6[17])
        let calories6 = Double(self.byteOfCalAndStep6[17])
        
        let date6 = "20\(year6)-\(dateConvert(day: month6))-\(dateConvert(day: day6))"
        
        setOfArray6 = ["calories": String(format: "%.2f", calories6), "date": date6, "steps": steps6]
        arraySave.append(setOfArray6)
    }
    
    func dateConvert(day: String) -> String {
        var date = ""
        if Int(day) ?? 0 <= 9 {
            date = "0\(day)"
        }else{
            date = "\(day)"
        }
        return date
    }
    
    private func requestFCMWatch() {
        let loversUserToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        let header = ["Accept": "application/json", "Authorization": "Bearer \(loversUserToken)"]
        
        Alamofire.request(URL(string: "\(Constants.Routes.BASE)/api/v4/ios/lovers/watch-fcm")!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { response in
          switch response.result {
            case .success:
                print("requestFCMWatch - requestFCMWatch", response)
                break
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func addSteps(arraySave: [[String: Any]]) {
        UserDefaults.standard.removeObject(forKey: "stringFV")
        
        print("arraySave", arraySave)
        
        let jsonData = try! JSONSerialization.data(withJSONObject: arraySave)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) ?? ""
        let final = JSON.init(parseJSON: String(jsonString))

        self.savedJSONData.append(final)
         
         var stringF = ""
         var stringS = ""
         
         for json in self.savedJSONData {
             stringS = json.rawString([:]) ?? ""
         }
         
         self.jsonStringArr.append(stringS)
         stringF = self.jsonStringArr.joined(separator: ",\n")
       
        print("test isSleepMode = true \(stringF)")
        UserDefaults.standard.set(stringF, forKey: "stringFV")
    }
    
    func firstAppend(byteArray: [UInt8]){
        for i in 0..<26 {
            self.byteOfCalAndStep1.append(byteArray[i])
        }
        
        print("gettting", self.byteOfCalAndStep1)
    }
    
    func secondAppend(byteArray: [UInt8]){
        for i in 27..<53 {
            self.byteOfCalAndStep2.append(byteArray[i])
        }

        print("getttin2", self.byteOfCalAndStep2)
    }
    
    func thirdAppend(byteArray: [UInt8]){

        for i in 54..<80 {
            self.byteOfCalAndStep3.append(byteArray[i])
        }
        
         print("getttin3", self.byteOfCalAndStep3)
    }
    
    func fourthAppend(byteArray: [UInt8]){
        
        for i in 81..<108 {
            self.byteOfCalAndStep4.append(byteArray[i])
        }
        
        print("getttin4", self.byteOfCalAndStep4)
    }
    
    func fifthAppend(byteArray: [UInt8]){
        
        for i in 108..<135 {
            self.byteOfCalAndStep5.append(byteArray[i])
        }
        
        print("getttin5", self.byteOfCalAndStep5)
    }
    
    func sixthAppend(byteArray: [UInt8]){
        
        for i in 135..<162 {
            self.byteOfCalAndStep6.append(byteArray[i])
        }
        
        print("getttin6", self.byteOfCalAndStep6)
        
    }

}
