//
//  BluetoothProtocolModel.swift
//  upmood
//
//  Created by Jason Juliane on 10/11/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

class BluetoothProtocolModel{
    weak var delegate: BluetoothProtocol?
    
    func getData(bpm: Double) {
        delegate?.didReceiveData(bpm)
    }
    
}
