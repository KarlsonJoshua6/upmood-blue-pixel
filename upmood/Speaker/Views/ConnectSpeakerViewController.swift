//
//  ConnectSpeakerViewController.swift
//  upmood
//
//  Created by Taison Digital on 27/08/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit
import CoreBluetooth

class ConnectSpeakerViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoSpeaker: UILabel!
    
    let charRead  = ""
    let charNotif = ""
    let charWrite = ""
    
    var writeChannel: CBCharacteristic!
    //private var selectedSpeakerName = ""
    
    // MARK: - BLE Stored Values
    let manager = BluetoothManagerSpeaker.getInstance()
    private var nearyBySpeaker: [PeripheralInfosSpeaker] = []
    var preferences: Preferences? {
        return PreferencesStore.shared.preferences
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        manager.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let colorWheelController = segue.destination as? ColorWheelViewController  {
            colorWheelController.writeChannel = writeChannel
            //colorWheelController.selectedSpeakerName = selectedSpeakerName
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnScan(_ sender: UIButton) {
        nearyBySpeaker.removeAll()
        manager.startScanPeripheral()
        tableView.reloadData()
        lblNoSpeaker.isHidden = true
    }
}

extension ConnectSpeakerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "speakerCell", for: indexPath) as! speakserCell
        cell.lblSpeakerName.text = nearyBySpeaker[indexPath.row].peripheral.name ?? "Speaker"
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nearyBySpeaker.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        manager.stopScanPeripheral()
        
        let peripheral = self.nearyBySpeaker[indexPath.row].peripheral
        self.manager.connectPeripheral(peripheral)
    
        
        let cell = tableView.cellForRow(at: indexPath) as! speakserCell
        initCell(cell: cell)
    }
    
    private func initCell(cell: speakserCell) {
        cell.bgView.backgroundColor = hexStringToUIColor(hex: "#3DBFCD")
        cell.lblSpeakerName.textColor = UIColor.white
        cell.bgViewContent.backgroundColor = hexStringToUIColor(hex: "#222443")
       
    }
}

extension ConnectSpeakerViewController: BluetoothDelegate {
    
    func didDiscoverPeripheral(_ peripheral: CBPeripheral, advertisementData: [String : Any], RSSI: NSNumber) {
        let peripheralInfo = PeripheralInfosSpeaker(peripheral)
        
        if let peripheralName = peripheral.name {
            if peripheralName.contains("BLE") {
               if !nearyBySpeaker.contains(peripheralInfo) {
                   if let preference = preferences, preference.needFilter {
                       if RSSI.intValue != 127, RSSI.intValue > preference.filter {
                           peripheralInfo.RSSI = RSSI.intValue
                           peripheralInfo.advertisementData = advertisementData
                           self.nearyBySpeaker.append(peripheralInfo)
                       }
                   } else {
                       peripheralInfo.RSSI = RSSI.intValue
                       peripheralInfo.advertisementData = advertisementData
                       nearyBySpeaker.append(peripheralInfo)
                       self.tableView.reloadData()
                   }
               }
            }
        }
    }
        
    func didConnectedPeripheral(_ connectedPeripheral: CBPeripheral) {
        print("successfully connected")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.performSegue(withIdentifier: "colorWheel", sender: nil)
        }
        selectedSpeakerName = connectedPeripheral.name ?? "No-Name"
    }
    
    func didDisconnectPeripheral(_ peripheral: CBPeripheral) {
        // hide UI
        selectedSpeakerName = ""
        NotificationCenter.default.post(name: .didDisconnectSpeaker, object: nil, userInfo: nil)
    }

    func didDiscoverServices(_ peripheral: CBPeripheral) {
        tableView.isHidden = true
        guard let services = peripheral.services else { return }

        for service in services {
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func didDiscoverCharacteritics(_ peripheral: CBPeripheral, service: CBService) {
        guard let characteristics = service.characteristics else { return }
        for characteristic in characteristics {

          if characteristic.properties.contains(.read) {
              peripheral.readValue(for: characteristic)
          }

          if characteristic.properties.contains(.notify) {
              peripheral.setNotifyValue(true, for: characteristic)
          }

          if characteristic.properties.contains(.write) {
              print("Write Channel Discovered")
              writeChannel = characteristic
          }
       }
    }
    
    func didReadValueForCharacteristic(_ characteristic: CBCharacteristic) {
        switch characteristic.uuid.uuidString {
        case charNotif:
            // Write Data If Needed
            break
        default: break
        }
    }
}

class speakserCell: UITableViewCell {
    @IBOutlet weak var lblSpeakerName: UILabel!
    @IBOutlet weak var lblConnected: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bgViewContent: UIView!
}

class PeripheralInfosSpeaker: Equatable, Hashable {
    
      let peripheral: CBPeripheral
      var RSSI: Int = 0
      var advertisementData: [String: Any] = [:]
      var lastUpdatedTimeInterval: TimeInterval
          
      init(_ peripheral: CBPeripheral) {
          self.peripheral = peripheral
          self.lastUpdatedTimeInterval = Date().timeIntervalSince1970
      }
      
      static func == (lhs: PeripheralInfosSpeaker, rhs: PeripheralInfosSpeaker) -> Bool {
          return lhs.peripheral.isEqual(rhs.peripheral)
      }
          
      var hashValue: Int {
          return peripheral.hash
      }
}
