//
//  ColorWheelViewController.swift
//  upmood
//
//  Created by Taison Digital on 27/08/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import CoreBluetooth
import UIKit

var selectedSpeakerName = ""

class ColorWheelViewController: UIViewController {
    
    let speakerManager = BluetoothManagerSpeaker.getInstance()
    var bluetooManager = BluetoothManager.getInstance()
    var writeChannel: CBCharacteristic!
    
    @IBOutlet weak var sliderBrightness: UISlider!
    @IBOutlet weak var imgColorWheel: UIImageView!
    
    @IBOutlet weak var segmentMode: UISegmentedControl!
    
    @IBOutlet weak var lblSpeakerName: UILabel!

        
    @IBOutlet weak var btnWhite: UIButton!
    @IBOutlet weak var btnBlue: UIButton!
    
    @IBOutlet weak var btnCyan: UIButton!
    @IBOutlet weak var btnGreen: UIButton!
    @IBOutlet weak var btnYellow: UIButton!
    @IBOutlet weak var btnOrange: UIButton!
    @IBOutlet weak var btnRedOrange: UIButton!
    @IBOutlet weak var btnPink: UIButton!
    @IBOutlet weak var btnViolet: UIButton!
    @IBOutlet weak var btnRed: UIButton!
    @IBOutlet weak var btnBrown: UIButton!
    @IBOutlet weak var btnSync: UIButton!
    

    @IBOutlet weak var brightnessView: UIStackView!
    
    var syncBool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initColor()
        sliderBrightness.isContinuous = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .didDisconnectSpeaker, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didDisconnectConnectedSpeaker(_:)), name: .didDisconnectSpeaker, object: nil)
    }
    
    func initColor(){
        defaultBrightness()
        initSegment()
        // getSaveSync()
    }
    
    func initSegment() {
        let modeMusicSegment = UserDefaults.standard.integer(forKey: "modeMusic")
        switch modeMusicSegment {
        case 0:
            segmentMode.selectedSegmentIndex = 0
            segmentMode.sendActions(for: UIControl.Event.valueChanged)
        case 1:
            segmentMode.selectedSegmentIndex = 1
            segmentMode.sendActions(for: UIControl.Event.valueChanged)
        case 2:
            segmentMode.selectedSegmentIndex = 2
            segmentMode.sendActions(for: UIControl.Event.valueChanged)
        default:break
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
       dismiss(animated: false, completion: nil)
       self.performSegue(withIdentifier: "moodMixDashboard", sender: nil)
    }
    
    func defaultBrightness() {
        writeCommand(withHexUInt: [0x04, 0x01, 0x02], manager: speakerManager)
       
        lblSpeakerName.text = selectedSpeakerName
    }
    
    @IBAction func segment(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            writeCommand(withHexUInt: [0x03, 0x01, 0x01], manager: speakerManager)
            UserDefaults.standard.set(0, forKey: "modeMusic")
            showBrightness()
        case 1:
            writeCommand(withHexUInt: [0x03, 0x01, 0x02], manager: speakerManager)
            UserDefaults.standard.set(1, forKey: "modeMusic")
            hideBrightness()
        case 2:
            writeCommand(withHexUInt: [0x03, 0x01, 0x03], manager: speakerManager)
            UserDefaults.standard.set(2, forKey: "modeMusic")
            hideBrightness()
        default:
            break;
        }
    }
    
    @IBAction func btnBottom(_ sender: UIButton) {
        buttonBottom(withIndex: sender.tag)
    }
    @IBAction func sliderBrightness(_ sender: Any) {
        print("SLIDER VAL", String(sliderBrightness.value))
        if sliderBrightness.value <= 0.33{
            changeSlider(withIndex: 0)
        }else if sliderBrightness.value <= 0.66 {
            changeSlider(withIndex: 1)
        }else{
            changeSlider(withIndex: 2)
        }
    }
    
    @IBAction func changeColor(_ sender: UIButton) {
        changeColor(withIndex: sender.tag)
    }
           
    @IBAction func btnChangeSlider(_ sender: UIButton) {
        changeSlider(withIndex: sender.tag)
    }
    
    @IBAction func disconnect(_ sender: UIButton) {
        speakerManager.disconnectPeripheral()
        speakerManager.stopScanPeripheral()
        self.performSegue(withIdentifier: "moodMixDashboard", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let moodMix = segue.destination as? MoodMixDashboardViewController  {
               moodMix.writeChannel = writeChannel
        }
    }
    
    private func buttonBottom(withIndex: Int) {
        switch withIndex {
        case 1:
            if !bluetooManager.connected {
                showWarningDialog()
            }else {
                if syncBool == true {
                    getSync(title: "Sync Emotion: OFF", syncBool: true, switchMode: 1)
                } else {
                    getSync(title: "Sync Emotion: ON", syncBool: false, switchMode: 0)
                }
            }
        case 2:
            print("Disconnect")
        default: break
        }
    }
    
    private func getSync(title: String, syncBool: Bool, switchMode: Int) {
        btnSync.setTitle(title, for: UIControlState.normal)
       // UserDefaults.standard.set(switchMode, forKey: "syncStatus")
        
        Constants.syncBool = syncBool
        
        switch switchMode{
        case 0:
            enableAllColor()
            self.syncBool = true
        case 1:
            self.syncBool = false
            disableAllColor()
        default: break
        }
    }
    
    private func getSaveSync() {
        if Constants.syncBool == true {
           getSync(title: "Sync Emotion: OFF", syncBool: true, switchMode: 1)
        } else {
            getSync(title: "Sync Emotion: ON", syncBool: false, switchMode: 0)
        }
    }
    
    private func changeSlider(withIndex: Int){
        switch withIndex{
            case 0:
            writeCommand(withHexUInt: [0x04, 0x01, 0x01], manager: speakerManager)
           
            case 1:
            writeCommand(withHexUInt: [0x04, 0x01, 0x02], manager: speakerManager)
            
            case 2:
            writeCommand(withHexUInt: [0x04, 0x01, 0x03], manager: speakerManager)
           
            default: break
        }
    }
    
   private func changeColor(withIndex: Int) {
       switch withIndex {
       case 1:  writeCommand(withHexUInt: [0x05, 0x03, 0xFF, 0xFF, 0xFF], manager: speakerManager) // pure white
        imgColorWheel.image = UIImage(named: "color_wheel_white-2.png")
       case 2:  writeCommand(withHexUInt: [0x05, 0x03, 0x33, 0x99, 0xFF], manager: speakerManager) // blue
        imgColorWheel.image = UIImage(named: "color_wheel_blue.png")
       case 3:  writeCommand(withHexUInt: [0x05, 0x03, 0x66, 0xFF, 0xB2], manager: speakerManager) // sky blue
        imgColorWheel.image = UIImage(named: "color_wheel_light_green.png")
       case 4:  writeCommand(withHexUInt: [0x05, 0x03, 0x80, 0xFF, 0x00], manager: speakerManager) // green
        imgColorWheel.image = UIImage(named: "color_wheel_green.png")
       case 5:  writeCommand(withHexUInt: [0x05, 0x03, 0xFF, 0x80, 0x00], manager: speakerManager) // yellow
        imgColorWheel.image = UIImage(named: "color_wheel_light_orange.png")
       case 6:  writeCommand(withHexUInt: [0x05, 0x03, 0xFF, 0x45, 0x00], manager: speakerManager) // light blue
        imgColorWheel.image = UIImage(named: "color_wheel_orange.png")
       case 7:  writeCommand(withHexUInt: [0x05, 0x03, 0xFF, 0x11, 0x11], manager: speakerManager) // white
        imgColorWheel.image = UIImage(named: "color_wheel_red_orange.png")
       case 8:  writeCommand(withHexUInt: [0x05, 0x03, 0xFF, 0x00, 0x7F], manager: speakerManager) // light blue
        imgColorWheel.image = UIImage(named: "color_wheel_pink.png")
       case 9:  writeCommand(withHexUInt: [0x05, 0x03, 0x7F, 0x00, 0xFF], manager: speakerManager) // violet
        imgColorWheel.image = UIImage(named: "color_wheel_violet.png")
       case 10: writeCommand(withHexUInt: [0x05, 0x03, 0xCC, 0x00, 0x00], manager: speakerManager) // red
        imgColorWheel.image = UIImage(named: "color_wheel_red.png")
       case 11: writeCommand(withHexUInt: [0x05, 0x03, 0x66, 0x00, 0x00], manager: speakerManager) // brown
        imgColorWheel.image = UIImage(named: "color_wheel_brown.png")
       default: break
       }
    }
    
    func writeCommand(withHexUInt: [UInt8], manager: BluetoothManagerSpeaker) {
        if writeChannel != nil {
            print("write command here")
            manager.writeValue(withCommand: withHexUInt, forCharacteristic: writeChannel, type: .withResponse)
        } else {
            print("No channel found.")
        }
    }
    
    private func showWarningDialog() {
        let alertController = UIAlertController(title: Constants.MessageDialog.WARINING, message: "Please connect your band/watch first.", preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: "Close", style: .cancel) { action -> Void in
            self.getSync(title: "Sync Emotion: ON", syncBool: false, switchMode: 0)
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    func disableAllColor(){
        imgColorWheel.image = UIImage(named: "color wheel-1")
        btnWhite.isEnabled = false
        btnBlue.isEnabled = false
        btnCyan.isEnabled = false
        btnGreen.isEnabled = false
        btnYellow.isEnabled = false
        btnOrange.isEnabled = false
        btnRedOrange.isEnabled = false
        btnPink.isEnabled = false
        btnViolet.isEnabled = false
        btnRed.isEnabled = false
        btnBrown.isEnabled = false
    }
    
    func enableAllColor(){
        btnWhite.isEnabled = true
        btnBlue.isEnabled = true
        btnCyan.isEnabled = true
        btnGreen.isEnabled = true
        btnYellow.isEnabled = true
        btnOrange.isEnabled = true
        btnRedOrange.isEnabled = true
        btnPink.isEnabled = true
        btnViolet.isEnabled = true
        btnRed.isEnabled = true
        btnBrown.isEnabled = true
    }
    
    func hideBrightness(){
        brightnessView.isHidden = true
    }
    
    func showBrightness(){
        brightnessView.isHidden = false
    }
    
    @objc func didDisconnectConnectedSpeaker(_ notification: Notification) {
        self.performSegue(withIdentifier: "moodMixDashboard", sender: nil)
    }
}

extension Notification.Name {
    static let didDisconnectSpeaker = Notification.Name("didDisconnectSpeaker")
}

