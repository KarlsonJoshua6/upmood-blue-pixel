//
//  CustomUIImageView.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 29/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class CustomUIImageView: UIImageView {

    override func awakeFromNib() {
        self.clipsToBounds = true
    }
}
