//
//  ClipToBoundsImageView.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 24/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class ClipToBoundsImageView: UIImageView {
    
    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
}
