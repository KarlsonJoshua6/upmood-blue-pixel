//
//  BaseViewController.swift
//  SRF
//
//  Created by NDPH on 09/08/2019.
//  Copyright © 2019 Srf konsulterna AB. All rights reserved.
//

import UIKit

protocol IBaseView {
    
    func onBackPressed()
    
}

class BaseViewController: UIViewController, IBaseView {

    var mView: IBaseView? = nil

    var mWindow : UIWindow?

    override func viewDidLoad() {
        
       
        
        onLoadOnce()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!
        
        mWindow = appDelegate.window

        onLoad()
    }
    
    
    // abstract function
    func onLoad() { }
    
    func onLoadOnce() { }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    func getBaseView() -> IBaseView? {
        return mView
    }
    
    func launchStoryboard(storyboard: String) -> UIViewController {
        
        let vc = UIStoryboard(name: storyboard, bundle: nil).instantiateInitialViewController() as! UIViewController
        
        self.present(vc, animated:false, completion:nil)

        return vc
    }
    
    
    func launchSubScreen(storyboard: String) -> UIViewController {

        let vc = UIStoryboard(name: storyboard, bundle: nil).instantiateInitialViewController() as! UIViewController
        
        self.navigationController?.pushViewController(vc, animated: true)

        return vc
    }

    func onBackPressed() {
        self.dismiss(animated: true, completion: nil)
    }
}
