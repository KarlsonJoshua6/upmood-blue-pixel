//
//  GenericErrorCatch.swift
//  upmood
//
//  Created by Dre on 08/02/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

enum GenericErrorCatch: Swift.Error{
    case OutOfRange
}
