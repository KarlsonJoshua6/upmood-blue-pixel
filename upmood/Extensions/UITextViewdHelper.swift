
//
//  UITextFieldHelper.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 11/09/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func localized(_ lang: String) -> String {
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

private var maxLengths = [UITextView: Int]()

extension UITextView : UITextViewDelegate {
    
    @IBInspectable var maxLength: Int {
        
        get {
            
            guard let length = maxLengths[self]
                else {
                    return Int.max
            }
            return length
        }
        set {
            maxLengths[self] = newValue
            self.delegate = self
        }
    }
    
    @objc func limitLength(textView: UITextView) {
        guard let prospectiveText = textView.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = String(prospectiveText[..<maxCharIndex])
        selectedTextRange = selection
        
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        limitLength(textView: textView)
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        scrollToBottom()
        return true
    }
    
    public func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        scrollToBottom()
        return true
    }
    
    func scrollToBottom() {
        let location = text.count - 1
        let bottom = NSMakeRange(location, 1)
        self.scrollRangeToVisible(bottom)
    }
}
