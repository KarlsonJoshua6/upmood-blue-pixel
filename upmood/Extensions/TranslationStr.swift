//
//  TranslationStr.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 23/01/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

struct TranslationStr {
    
    // MARK: - Dialog
    static let errorTitle = NSLocalizedString("Error, something is wrong!", comment: "")
    static let cancel = NSLocalizedString("Cancel", comment: "")
    
    static let bpm = NSLocalizedString("BPM", comment: "")
    
    // MARK: - Emotions Str
    static let excitement = NSLocalizedString("Excitement", comment: "")
    static let happy = NSLocalizedString("Happy", comment: "")
    static let zen = NSLocalizedString("Zen", comment: "")
    static let pleasant = NSLocalizedString("Pleasant", comment: "")
    static let calm = NSLocalizedString("Calm", comment: "")
    static let unplesant = NSLocalizedString("Unpleasant", comment: "")
    static let confused = NSLocalizedString("Confused", comment: "")
    static let challenged = NSLocalizedString("Challenged", comment: "")
    static let tense = NSLocalizedString("Tense", comment: "")
    static let sad = NSLocalizedString("Sad", comment: "")
    static let anxious = NSLocalizedString("Anxious", comment: "")
    static let loading = NSLocalizedString("Loading", comment: "")
}
