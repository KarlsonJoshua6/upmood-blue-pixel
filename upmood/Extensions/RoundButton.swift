//
//  RoundButton.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 07/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

@IBDesignable class RoundUIView: UIView {

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

@IBDesignable class RoundButton: UIButton {
    
//    // added
//    @IBInspectable var borderWidth: CGFloat {
//        set {
//            layer.borderWidth = newValue
//        }
//        get {
//            return layer.borderWidth
//        }
//    }
//
//    @IBInspectable var borderColor: UIColor? {
//        set {
//            guard let uiColor = newValue else { return }
//            layer.borderColor = uiColor.cgColor
//        }
//        get {
//            guard let color = layer.borderColor else { return nil }
//            return UIColor(cgColor: color)
//        }
//    }
//
//    // original
//    @IBInspectable var cornerRadius: CGFloat = 15 {
//        didSet {
//            refreshCR(_value: cornerRadius)
//        }
//    }
//
//    func refreshCR(_value: CGFloat) {
//        layer.cornerRadius = _value
//    }
//
//    @IBInspectable var customBGColor: UIColor = UIColor.init(red: 0, green: 122/255, blue: 255/255, alpha: 1) {
//        didSet {
//            refreshColor(_color: customBGColor)
//        }
//    }
//
//    func refreshColor(_color: UIColor) {
//        print("refreshColor(): \(_color)")
//        let size: CGSize = CGSize(width: 1, height: 1)
//        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
//        _color.setFill()
//        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
//        let bgImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() as UIImage!
//        UIGraphicsEndImageContext()
//        setBackgroundImage(bgImage, for: UIControlState.normal)
//        clipsToBounds = true
//    }
//
//    override init(frame: CGRect) {
//        print("init(frame:)")
//        super.init(frame: frame)
//
//        sharedInit()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        print("init?(coder:)")
//        super.init(coder: aDecoder)
//
//        sharedInit()
//    }
//
//    override func prepareForInterfaceBuilder() {
//        print("prepareForInterfaceBuilder()")
//        sharedInit()
//    }
//
//    func sharedInit() {
//        refreshCR(_value: cornerRadius)
//        refreshColor(_color: customBGColor)
//        self.tintColor = UIColor.white
//    }
}
