//
//  RoundImageView.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 15/04/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class RoundImageView: UIImageView {

    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}
