//
//  Constants.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 08/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

enum PeripheralNotificationKeys : String { // The notification name of peripheral
    case DisconnectNotif = "disconnectNotif" // Disconnect notification name
    case CharacteristicNotif = "characteristicNotif" // Characteristic discover notification name
}

struct Constants {
    
    static var type_id = 0
    static var receivedCommand = false
    
    struct User {
        static var isPremiumUser = false
        static var isRepeatSong = false
    }
    
    // moodmix & speaker cons
    static var syncBool = false
    static var currentSongIsPlayingOnPlayerView = false
    static var playerSetTime = ""
    static var didUserSetTimer = false
    static var setTimerSecond = 0
    static var didStartSleepMood = false
    static var timeDisplayStr = 0
    static var wristSenseStr = 0
    static var langStr = 0
    static var distanceStr = 0
    static var getLangSelected = 0
    static var getTimeDate = 2
    static var getWristsense = 0
    
    static var didUserChangeLanguage = false
    static var firstTimeUser = false
    static var didUserStopSleepMode = false
    static var didEndCalculation = false
    static var didUserSubscribeToFcm = false
    static var didUserDisconnectedToBand = false
    static var nativeUser = false
    static var notifBool = false
    static var fcmBool = false
    
    static var ContentOfTerms = ""
    static var contentOfPrivacy = ""
    static var condiPriStr = ""
    static var emptyCurrentSession = false
    
    static var didHaveNextPageInPublicSession = true
    static var currentPageInPublicSession = 0
    
    static let emotions = ["anxious", "calm", "challenged", "confused", "excitement", "happy", "tense", "pleasant", "sad", "unpleasant", "zen"]
    
    struct Color {
        static let MOOD_MIX_PRIMARY_COLOR = "#222443"
        static let PRIMARY_COLOR = "#3DBFCD"
        static let CALM = "EDA05A"
        static let HAPPY = "#3DBFCD"
        static let SAD = "#E53535"
        static let MID_UNPLESANT = "#D96637"
        static let MID_PLEASANT = "#EBBB6F"
        static let NEW_COLOR = "#35B7C6"
        static let BACKGROUND_VIEW = "#EFEFF4"
        static let GRAY_COLOR = "#636366"
        static let BACKGROUND_SESSION_DASHBOARD = "#363638"
        static let TEXT_COLOR_SESSION_DASHBOARD = "#EBEBF0"
        static let BACKGROUND_BUTTON_POSTSTATUS = "#444446"
        static let TEXT_COLOR_BUTTON_POSTSTATUS = "#91AAAA"
        static let TEXT_COLOR_TFSTATUS = "#DFE2E8"
        static let BACKGROUND_VIEW_FRIENDS = "#F2F2F7"
        static let BACKGROUND_INNER_VIEW_FRIENDS = "#15724270"
        static let BACKGROUND_VIEW_INNER = "#444446"
        static let TEXT_COLOR_FRIENDS = "#D8D8DC"
        static let TEXT_COLOR_BUTTON_FRIENDS = "#EBEBF0"
        static let TEXT_COLOR_DATE_MOODSTREAM = "#91AAAA"
        static let TEXT_COLOR_MOODSTREAM = "#8395A7"
        static let TEXT_COLOR_DEVICENAME = "#EBEBF0"
        static let CUSTOM_COLOR = "#3ABBCE"
        static let BACKGROUND_VIEW_SENDREACTION = "#F4F4F6"
        static let BACKGROUND_VIEW_EMOJI_SENDREACTION = "#545456"
        static let TEXT_COLOR_SENDREACTION = "#94ACAC"
        static let TEXT_COLOR_EXCITEMENT = "#84888B"
        static let TEXT_COLOR_NOTFRIENDSYET = "#707070"
        static let BACKGROUND_COLOR_SEPERATOR = "#DFE2E8"
        static let BACKGROUND_COLOR_UI = "#3025964"
        static let BACKGROUND_COLOR_DARKMODE = "#2C2C2E"
        static let TEXT_COLOR_WEEKDAYS_CALENDAR = "#00A3AD"
        static let TEXT_COLOR_PRIMARY_COLOR = "#0099A7"
    }
    
    struct Routes {
        static let BASE = "http://18.139.163.192:8080"
        static let VERSION = "v3"
        static let BASE_URL_RESOURCE = "http://18.139.163.192:8080/img/resources/"
        static let ADMIN_TOKEN = "UpmoodAPI-a0XbCZeTxi1zW9sU5Y2GoQf1M0G55m3JNPrHNH96JSJNpj2SOwaMUggW5V9U"
    }
    
    struct MessageDialog {
        static let WARINING = "Warning"
        static let EMAIL_ALREADY_TAKEN = "The email has already been taken."
        static let SIGNINGIN = "Signing in..."
        static let SIGNINGUP = "Signing up..."
        static let ERROR_CREATING_ACCOUNT = "Error Creating Account"
        static let INCOMPLETE_FORM = "The form is incomplete."
        static let errorTitle = "Error, something is wrong!"
        static let errorMessage = "Minimum of 1 and Maximum of 4 Image!"
        static let errorValidatingYoutubeLink = "Youtube Link is not working!"
        static let errorExceedEnteredNumber = "The profile phonenumber must be between 4 and 15 digits."
        static let PASSWORD_MISMATCH = "The password confirmation does not match."
        static let CHECKBOX_ERROR = "You must agree to our Terms of Services, including Cookie Use."
        static let EMAIL_DOES_NOT_EXIST = "These credentials do not match our records."
        static let INVALID_EMAIL_FORMAT = "Your entered email address doesn't look right"
    }
    
    struct Misc {
        static let SPACE = " "
        static let PERCENT20 = "%20"
        static let PNG_EXTENSION = ".png"
        static let SVG_EXTENSION = ".svg"
    }
    
    struct SessionType {
        static let FITNESS_TWO = "Fitness2"
        static let FITNESS = "Fitness"
        static let MOVIE = "Movie"
        static let CORPORATE = "Corporate"
        static let SOLUTION = "Solution"
        static let RESEARCH = "Research"
    }
    
    struct GatheringType {
        static let INTERVAL = "Interval"
        static let CONTINUOUS = "Continuous"
    }
    
    struct SessionTypeIcon {
        static let FITNESS = "ic_session_fitness"
        static let MOVIE = "ic_session_movie"
        static let CORPORATE = "ic_session_corporate"
        static let SOLUTION = "ic_session_music"
        static let RESEARCH = "ic_session_research"
    }
    
    struct ViewControllerId {
        static let PUBLIC_SESSION = "public_session"
        static let PRIVATE_SESSION = "private_session"
        static let HISTORY_SESSION = "history_session"
        static let CURRENT_SESSION = "current_session"
        static let MAIN_CURRENT_SESSION = "main_current_session"
    }
    
    struct Language {
        static let ENGLISH = "English"
        static let JAPANESE = "Japanese"
        static let ENG = "en"
        static let JAP = "ja"
    }
    
    static var didUserLogin = false
    static let EMPTY_STRING = ""
}

