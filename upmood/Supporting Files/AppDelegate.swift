//
//  AppDelegate.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 01/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import AVFoundation
import AWSS3
import Braintree
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import IQKeyboardManagerSwift
import KeychainSwift
import FBSDKCoreKit
import FBSDKCoreKit
import FBSDKLoginKit
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {
    
    var type_id = ""
    let accessKey = "AKIA6ATKMKH2GBRMJTD4"
    let secretKey = "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"
    var window: UIWindow?
    let keychain = KeychainSwift()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Custom Keyboard Helper
        IQKeyboardManager.shared.enable = true
        
        // Custom Function (checker for User Logged In, checker of language)
        preload()
        
        // FCM Notif Helper
        let center = UNUserNotificationCenter.current()
        // Request permission to display alerts and play sounds.
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        
        // Firebase Helper
        Messaging.messaging().delegate = self
        Messaging.messaging().shouldEstablishDirectChannel = true
        FirebaseApp.configure()
        
        // Local Notif Helper
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(tokenRefreshNotification),name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        application.registerForRemoteNotifications()
        
        // Alarm Notif Helper
        if let options = launchOptions {
            if let notification = options[UIApplicationLaunchOptionsKey.localNotification] as? UILocalNotification {
                print(notification.userInfo ?? "No info attached.")
            }
        }
        
        // AVPlayer
        let session = AVAudioSession.sharedInstance()
        
        do {
            try session.setActive(true)
            // swift 4 syntx, swift 4.2 AVAudioSession.Category.playback
            try session.setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            print(error.localizedDescription)
        }
        
        // AWSS3
        setUpAWS()
        
        //Braintree
        BTAppSwitch.setReturnURLScheme("com.taison.digital.upmood.payments")
        
        return true
    }
    
    func setUpAWS() {
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration.init(region: AWSRegionType.APSoutheast1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
   
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        return AppEvents.activateApp()
    }
    
    // MARK: - FACEBOOK LOGIN
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        // Add FBSDK fix
        return ApplicationDelegate.shared.application(app, open: url, options: options)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        UserDefaults.standard.removeObject(forKey: "integerBPM")
        UserDefaults.standard.removeObject(forKey: "emotionValueString")
        UserDefaults.standard.removeObject(forKey: "stressLevelString")
    }
    
    private func preload() {
        if UIApplication.isFirstLaunch() {
            
            //UserDefaults.standard.set("en", forKey: "selectedLanguage")
            
            let token = keychain.get("apiToken") ?? ""
            if !token.isEmpty {
                print("user token is not empty")
                publicLogOut()
                removeAllSharedPref()
            } else {
                print("user token is empty")
            }
            
        //    self.setUpLanguage()
            setUpFirstLang()
            
        } else {
           // app is installed already
           // Detect if user is already login
           
            let token = keychain.get("apiToken") ?? ""
            self.setUpLanguage()
            
            if !token.isEmpty {
               let storyboard = UIStoryboard(name: "DashboardScreen", bundle: nil)
               let initialViewController = storyboard.instantiateViewController(withIdentifier: "MainTabBar")
               UIApplication.shared.registerForRemoteNotifications()
               self.window!.rootViewController = initialViewController
               self.window!.rootViewController!.viewDidLoad()
            }
        }
    }
    private func setUpFirstLang(){
        let langStr = Locale.current.languageCode
        
        if langStr == "en" {
            UserDefaults.standard.set("en", forKey: "selectedLanguage")
            Bundle.set(language: "en")
        } else if langStr == "ja"{
            UserDefaults.standard.set("ja", forKey: "selectedLanguage")
            Bundle.set(language: "ja")
        }else {
            
        }
    }
    private func setUpLanguage() {
        
        let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
        
        if selectedLanguage == "en" {
            Bundle.set(language: "en")
        } else {
            Bundle.set(language: "ja")
        }
        
        UserDefaults.standard.set(false, forKey: "didUserChangeLanguage")
    }
}

extension AppDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
        print("Firebase Token :  \(fcmToken)")
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Refreshed Token: \(fcmToken)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        var readableToken: String = ""
        for i in 0..<deviceToken.count {
            readableToken += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        
        print("Received an APNs device token: \(readableToken)")
        
        Messaging.messaging().apnsToken = deviceToken
    }
        
     func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        print("Handle push from foreground")
        // custom code to handle push while app is in the foreground
        print("\(notification.request.content.userInfo)")
        
        if let notif_data = notification.request.content.userInfo as? [String: Any] {
            let notif_type_id = notif_data["type_id"] as? String ?? ""
            let type_id = Int(notif_type_id) ?? 0
            Constants.type_id = type_id
            
            print("hello type id: \(type_id)")
            
            if type_id == 15 {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "recievedSMSLovers"), object: nil)
            }
            if type_id == 0 {
                NotificationCenter.default.post(name:
                    NSNotification.Name(rawValue: "disconnectBLE"), object: nil)
            }
        }
        
        completionHandler([.alert, .badge, .sound])
     }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Handle Push on background When User Tapped the banner")
        print("\(response.notification.request.content.userInfo)")
        
        if let notif_data = response.notification.request.content.userInfo as? [String: Any] {
            let notif_type_id = notif_data["type_id"] as? String ?? ""
            let type_id = Int(notif_type_id) ?? 0
            
            if type_id == 15 {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "recievedSMSLovers"), object: nil)
            }
        }
    }
    
    // MARK: - Firebase
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("notification receive: \(remoteMessage.appData)")
        //NotificationCenter.default.post(name: .didUserReceiveNotif, object: nil, userInfo: remoteMessage.appData)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("notification receive: \(userInfo)")
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Oh no! Failed to register for remote notifications with error \(error)")
    }
    
    func printFCMToken() {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Your FCM token is \(result.token)")
                UserDefaults.standard.set(result.token, forKey: "fcmToken")
                Constants.didUserSubscribeToFcm = true
                print("Remote instance ID token: \(result.token)")
            }
        }
    }
    
    @objc func tokenRefreshNotification(_ notification: NSNotification?) {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Your FCM token is \(result.token)")
                UserDefaults.standard.set(result.token, forKey: "fcmToken")
                Constants.didUserSubscribeToFcm = true
                self.printFCMToken()
            }
        }
    }
}

extension UIApplication {
    class func isFirstLaunch() -> Bool {
        if !UserDefaults.standard.bool(forKey: "HasAtLeastLaunchedOnce") {
            UserDefaults.standard.set(true, forKey: "HasAtLeastLaunchedOnce")
            UserDefaults.standard.synchronize()
            return true
        }
        return false
    }
}

