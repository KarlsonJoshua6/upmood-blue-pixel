//
//  LoginPresenter.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 17/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

// MARK: - Setup View Data

struct LoverViewData {
    let id: Int
    let name: String
    let image: String
}

struct RequestLoverViewData {
    let id: Int
    let lover_id: Int
    let name: String
    let image: String
}

struct SearchLoverViewData {
    let id: Int
    let name: String
    let image: String
}

// MARK: - Setup View with Object Protocol

protocol LoverView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func getLover(data: LoverViewData)
    func getRequestLover(data: [RequestLoverViewData])
    func searchLover(data: [SearchLoverViewData])
    func requestALover(withLoverId: Int)
    func acceptLover(withLoverId: Int)
    func rejectLover(withLoverId: Int)
    func removeLover(withId: Int)
    func setEmpty()
    func getError(withMessage: String)
    func requestFCM(id: Int)
}

// MARK: - Setup Presenter

class LoverPresenter {
    
    private let loverService: LoverService
    weak private var loverView : LoverView?
    
    init(loverService: LoverService) {
        self.loverService = loverService
    }
    
    func attachView(view: LoverView) {
        loverView = view
    }
    
    func detachView() {
        loverView = nil
    }
    
    func getLover() {
        self.loverView?.startLoading()
        loverService.callAPILover(onSuccess: { (data) in
            self.loverView?.finishLoading()
            
            if (data.id == 0) {
                self.loverView?.setEmpty()
            } else {
                let mappedData = LoverViewData(id: data.id ?? 0, name: data.name ?? "", image: data.image ?? "")
                self.loverView?.getLover(data: mappedData)
            }
            
        }) { (errorMessage) in
            self.loverView?.setEmpty()
            debugPrint("getLover() error: \(errorMessage)")
        }
    }
    
    func getRequestLover() {
        self.loverView?.startLoading()
        
        loverService.callAPIRequestLover(onSuccess: { (data) in
        
            if (data.count == 0) {
                self.loverView?.setEmpty()
            } else {
                let mappedData = data.map {
                    return RequestLoverViewData(id: $0.id ?? 0, lover_id: $0.lover_id ?? 0, name: "\($0.name ?? "")", image: "\($0.image ?? "")")
                }
                
                self.loverView?.getRequestLover(data: mappedData)
            }
            
            self.loverView?.finishLoading()
            
        }) { (errorMessage) in
            self.loverView?.setEmpty()
            debugPrint("getRequestLover() error: \(errorMessage)")
        }
    }
    
    func searchLover(withKeyword: String) {
        self.loverView?.startLoading()
        
        loverService.callAPISearchLover(withKeyword: withKeyword, onSuccess: { (data) in
        
            if (data.count == 0) {
                self.loverView?.setEmpty()
            } else {
                let mappedData = data.map {
                    return SearchLoverViewData(id: $0.id ?? 0, name: "\($0.name ?? "")", image: "\($0.image ?? "")")
                }
                
                self.loverView?.searchLover(data: mappedData)
            }
            
            self.loverView?.finishLoading()
            
        }) { (errorMessage) in
            self.loverView?.setEmpty()
            debugPrint("getRequestLover() error: \(errorMessage)")
        }

    }
    
    func requestALover(withLoverId: Int) {
        self.loverView?.startLoading()
        
        loverService.callAPIRequestALover(lover_id: withLoverId, onSuccess: { (data) in
            // success
        }) { (errorMessage) in
            self.loverView?.setEmpty()
            self.loverView?.getError(withMessage: errorMessage)
            debugPrint("requestALover() error: \(errorMessage)")
        }
    }
    
    func acceptLover(withLoverId: Int) {
        self.loverView?.startLoading()
        
        loverService.callAPIAcceptLover(lover_id: withLoverId, onSuccess: { (data) in
            // success
        }) { (errorMessage) in
            self.loverView?.setEmpty()
            self.loverView?.getError(withMessage: errorMessage)
            debugPrint("getRequestLover() error: \(errorMessage)")
        }
    }
    
    func rejectLover(withLoverId: Int) {
        self.loverView?.startLoading()
     
        loverService.callAPIRejectLover(lover_id: withLoverId, onSuccess: { (data) in
            // success
        }) { (errorMessage) in
            self.loverView?.setEmpty()
            debugPrint("getRequestLover() error: \(errorMessage)")
        }
    }
    
    func removeLover(withId: Int) {
        self.loverView?.startLoading()
        
        loverService.callAPIRemoveLover(id: withId, onSuccess: { (data) in
            // success
        }) { (errorMessage) in
            self.loverView?.setEmpty()
            debugPrint("getRequestLover() error: \(errorMessage)")
        }
    }
    
    func requestFCM(id: Int) {
        self.loverView?.startLoading()
        
        loverService.callAPIRequestFCMWatch(id: id, onSuccess: { (data) in
            // success
        }) { (errorMessage) in
            self.loverView?.setEmpty()
            self.loverView?.getError(withMessage: errorMessage)
            debugPrint("requestFCM() error: \(errorMessage)")
        }
    }
}
