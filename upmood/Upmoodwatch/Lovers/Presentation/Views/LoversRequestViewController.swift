//
//  LoverModeRequestViewController.swift
//  upmood
//
//  Created by Taison Digital on 17/09/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import SDWebImage
import UIKit

class LoversRequestViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let presenter = LoverPresenter(loverService: LoverService())
    var requestedLoversToDisplay = [RequestLoverViewData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        setUserInterfaceStyleLight(self: self)
        presenter.attachView(view: self)
        presenter.getRequestLover()
    }
    
    @objc private func acceptLoverAction(_ sender: UIButton) {
        if let indexpath = self.tableView.indexPathForView(sender) {
            presenter.acceptLover(withLoverId: requestedLoversToDisplay[indexpath.row].lover_id)
            presenter.attachView(view: self)
            presenter.getRequestLover()
            tableView.reloadData()
        }
    }
    
    @objc private func rejectLoverAction(_ sender: UIButton) {
        if let indexpath = self.tableView.indexPathForView(sender) {
            presenter.rejectLover(withLoverId: requestedLoversToDisplay[indexpath.row].lover_id)
            presenter.attachView(view: self)
            presenter.getRequestLover()
            tableView.reloadData()
        }
    }
}

extension LoversRequestViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requestedLoversToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestLoverTableViewCell", for: indexPath) as! RequestLoverTableViewCell
        setUpCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func setUpCell(cell: RequestLoverTableViewCell, indexPath: IndexPath){
        cell.loverNameLabel.text = requestedLoversToDisplay[indexPath.row].name
        if let imageURL = URL(string: "\(Constants.Routes.BASE)/\(requestedLoversToDisplay[indexPath.row].image)") {
            cell.loverProfileImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], progress: nil, completed: nil)
        }
        
        // setUP Button
        cell.buttonAdd.addTarget(self, action: #selector(self.acceptLoverAction), for: .touchUpInside)
        cell.buttonReject.addTarget(self, action: #selector(self.rejectLoverAction), for: .touchUpInside)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71.0
    }
}

extension LoversRequestViewController: LoverView {
    func startLoading() {
        //
    }
    
    func finishLoading() {
        //
    }
    
    func getLover(data: LoverViewData) {
        //
    }
    
    func getRequestLover(data: [RequestLoverViewData]) {
        requestedLoversToDisplay = data
        tableView.reloadData()
    }
    
    func searchLover(data: [SearchLoverViewData]) {
        
    }
    
    func requestALover(withLoverId: Int) {
        //
    }
    
    func acceptLover(withLoverId: Int) {
        viewDidLoad()
        tableView.reloadData()
    }
    
    func rejectLover(withLoverId: Int) {
        viewDidLoad()
        tableView.reloadData()
    }
    
    func removeLover(withId: Int) {
        //
    }
    
    func setEmpty() {
        //
    }
    
    func getError(withMessage: String) {
        presentDismissableAlertController(title: "One lover only", message: withMessage)
    }
    
    func requestFCM(id: Int) {
        
    }
}

class RequestLoverTableViewCell: UITableViewCell {
    @IBOutlet weak var viewLayer: UIView!
    @IBOutlet weak var loverProfileImageView: UIImageView!
    @IBOutlet weak var loverNameLabel: UILabel!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var buttonReject: UIButton!
}
