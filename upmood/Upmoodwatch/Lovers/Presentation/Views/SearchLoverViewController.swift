//
//  SearchLoverViewController.swift
//  
//
//  Created by John Paul Manoza on 22/10/2020.
//

import UIKit

class SearchLoverViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    let presenter = LoverPresenter(loverService: LoverService())
    var searchResultLoversToDisplay = [SearchLoverViewData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        setUserInterfaceStyleLight(self: self)
        presenter.attachView(view: self)
        presenter.searchLover(withKeyword: "")
        
        searchBar.delegate = self
    }
    
    @objc private func acceptLoverAction(_ sender: UIButton) {
        if let indexpath = self.tableView.indexPathForView(sender) {
            presenter.requestALover(withLoverId: searchResultLoversToDisplay[indexpath.row].id)
            _ = navigationController?.popToRootViewController(animated: true)
        }
    }
}

extension SearchLoverViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchLoverTableViewCell", for: indexPath) as! SearchLoverTableViewCell
        setUpCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func setUpCell(cell: SearchLoverTableViewCell, indexPath: IndexPath) {
        cell.loverNameLabel.text = searchResultLoversToDisplay[indexPath.row].name
        if let imageURL = URL(string: searchResultLoversToDisplay[indexPath.row].image) {
            cell.loverProfileImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], progress: nil, completed: nil)
        }
        
        // setUP Button
        cell.buttonAdd.addTarget(self, action: #selector(self.acceptLoverAction), for: .touchUpInside)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResultLoversToDisplay.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56.0
    }
}

extension SearchLoverViewController: LoverView {
    
    func startLoading() {
        //
    }
    
    func finishLoading() {
        //
    }
    
    func getLover(data: LoverViewData) {
        //
    }
    
    func getRequestLover(data: [RequestLoverViewData]) {
        //
    }
    
    func searchLover(data: [SearchLoverViewData]) {
        searchResultLoversToDisplay = data
        tableView.reloadData()
    }
    
    func requestALover(withLoverId: Int) {
        //
    }
    
    func acceptLover(withLoverId: Int) {
        //
    }
    
    func rejectLover(withLoverId: Int) {
        //
    }
    
    func removeLover(withId: Int) {
        //
    }
    
    func setEmpty() {
        //
    }
    
    func getError(withMessage: String) {
        //
    }
    
    func requestFCM(id: Int) {
        
    }
}

class SearchLoverTableViewCell: UITableViewCell {
    @IBOutlet weak var viewLayer: UIView!
    @IBOutlet weak var loverProfileImageView: UIImageView!
    @IBOutlet weak var loverNameLabel: UILabel!
    @IBOutlet weak var buttonAdd: UIButton!
}

// MARK: - UISearchBarDelegate

extension SearchLoverViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        guard let firstSubview = searchBar.subviews.first else { return }
        firstSubview.subviews.forEach { ($0 as? UITextField)?.clearButtonMode = .never }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.searchBar.endEditing(true)
        self.tableView.isHidden = false
        self.searchBar.text = ""
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == self.searchBar {
            if searchBar.returnKeyType == .search {
                
                presenter.searchLover(withKeyword: searchBar.text ?? "".lowercased())
                tableView.reloadData()
                
                searchBar.setShowsCancelButton(false, animated: true)
                searchBar.resignFirstResponder()
            }
        }
    }
}
