//
//  LoversModeViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 22/10/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import SDWebImage
import UIKit

class LoversModeViewController: UIViewController {
    
    @IBOutlet weak var loverImageView: UIImageView!
    @IBOutlet weak var loverLabel: UILabel!
    @IBOutlet weak var loverView: UIView!
    @IBOutlet weak var emptyStateLoverUIView: UIView!
    
    let presenter = LoverPresenter(loverService: LoverService())
    var loversDataToDisplay: LoverViewData?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        presenter.getLover()
    }
    
    private func setUpUI(loverData: LoverViewData) {
        emptyStateLoverUIView.isHidden = true
        loverView.isHidden = false
        loverLabel.text = "❤️ \(loverData.name)"
        if let imageURL = URL(string: loverData.image) {
            loverImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "ic_empty_state_profile"), options: [], progress: nil, completed: nil)
        }
        
        UserDefaults.standard.set(loverData.name, forKey: "lover_name")
    }
    
    @IBAction func seeMoreOption(_ sender: UIButton) {
        let alert = UIAlertController(title: "Remove lover", message: "Feel what you need to feel. And then let it go.", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Okay", style: .destructive , handler:{ [weak this = self] _ in
            this?.presenter.removeLover(withId: this?.loversDataToDisplay?.id ?? 0)
            this?.loverView.isHidden = true
            this?.emptyStateLoverUIView.isHidden = false
            UserDefaults.standard.removeObject(forKey: "lover_name")
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension LoversModeViewController: LoverView {

    func startLoading() {
        //
    }
    
    func finishLoading() {
        //
    }
    
    func getLover(data: LoverViewData) {
        loversDataToDisplay = data
        setUpUI(loverData: data)
    }
    
    func getRequestLover(data: [RequestLoverViewData]) {
        
    }

    func searchLover(data: [SearchLoverViewData]) {
        
    }
    
    func requestALover(withLoverId: Int) {
        //
    }
    
    func acceptLover(withLoverId: Int) {
        
    }
    
    func rejectLover(withLoverId: Int) {
        
    }
    
    func removeLover(withId: Int) {
        
    }
    
    func setEmpty() {
        loverView.isHidden = true
        emptyStateLoverUIView.isHidden = false
    }
    
    func getError(withMessage: String) {

    }
    
    func requestFCM(id: Int) {
        
    }
}
