//
//  LoverModeViewController.swift
//  upmood
//
//  Created by Taison Digital on 17/09/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class LoversModeController: UIViewController {

    @IBOutlet weak var segmentControl: UISegmentedControl!
    let viewControllerIdentifiers = ["loverVC", "requestVC"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSegmentControl(indexOf: 0)
    }
    
    @IBAction func close(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    private func setUpSegmentControl(indexOf: Int) {
        
        if #available(iOS 13.0, *) {
            segmentControl.selectedSegmentTintColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        } else {
            // Fallback on earlier versions
        }
        
        segmentControl.backgroundColor = .clear
        segmentControl.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: UIControlState.selected)
        segmentControl.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.black], for: UIControlState.normal)
        
        let newController = storyboard!.instantiateViewController(withIdentifier: viewControllerIdentifiers[indexOf])
        let oldController = childViewControllers.last!
        
        oldController.willMove(toParentViewController: nil)
        addChildViewController(newController)
        newController.view.frame = oldController.view.frame
        
        transition(from: oldController, to: newController, duration: 0.25, options: .transitionCrossDissolve, animations: {
        }, completion: { _ -> Void in
            oldController.removeFromParentViewController()
            newController.didMove(toParentViewController: self)
        })
    }
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        setUpSegmentControl(indexOf: sender.selectedSegmentIndex)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
}
