//
//  APIService.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 20/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

class LoverService {
    public func callAPILover(onSuccess successCallback: ((_ data: Lover) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        LoverAPICallManager.instance.callAPILover(onSuccess: { (data) in
            successCallback?(data)
        }) { (errorMessage) in
            failureCallback?(errorMessage)
        }
    }
    
    public func callAPIRequestLover(onSuccess successCallback: ((_ data: [RequestLover]) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        LoverAPICallManager.instance.callAPIRequestLover(onSuccess: { (data) in
            successCallback?(data)
        }) { (errorMessage) in
            failureCallback?(errorMessage)
        }
    }
    
    public func callAPISearchLover(withKeyword: String, onSuccess successCallback: ((_ data: [SearchLover]) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        LoverAPICallManager.instance.callAPISearchLover(withKeyword: withKeyword, onSuccess: { (data) in
            successCallback?(data)
        }) { (errorMessage) in
            failureCallback?(errorMessage)
        }
    }
    
    public func callAPIRequestALover(lover_id: Int, onSuccess successCallback: ((_ data: String) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        LoverAPICallManager.instance.callAPIRequestALover(lover_id: lover_id, onSuccess: { (data) in
            successCallback?(data)
        }) { (errorMessage) in
            failureCallback?(errorMessage)
        }
    }
    
    public func callAPIAcceptLover(lover_id: Int, onSuccess successCallback: ((_ data: String) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        LoverAPICallManager.instance.callAPIAcceptLover(lover_id: lover_id, onSuccess: { (data) in
            successCallback?(data)
        }) { (errorMessage) in
            failureCallback?(errorMessage)
        }
    }
    
    public func callAPIRejectLover(lover_id: Int, onSuccess successCallback: ((_ data: String) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?)  {
        LoverAPICallManager.instance.callAPIRejectLover(lover_id: lover_id, onSuccess: { (data) in
            successCallback?(data)
        }) { (errorMessage) in
            failureCallback?(errorMessage)
        }
    }
    
    public func callAPIRemoveLover(id: Int, onSuccess successCallback: ((_ data: String) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?)  {
        LoverAPICallManager.instance.callAPIRemoveLover(id: id, onSuccess: { (data) in
            successCallback?(data)
        }) { (errorMessage) in
            failureCallback?(errorMessage)
        }
    }
    
    public func callAPIRequestFCMWatch(id: Int, onSuccess successCallback: ((_ data: String) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        LoverAPICallManager.instance.callAPIRequestFCMWatch(id: id, onSuccess: { (data) in
            successCallback?(data)
        }) { (errorMessage) in
            failureCallback?(errorMessage)
        }
    }
}
