//
//  LoginAPICallManager.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 17/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SwiftyJSON
import Foundation

class LoverAPICallManager {
    
    var header: HTTPHeaders = [:]
    var params: [String: Any] = [:]
    
    static let instance = LoverAPICallManager()
    
    enum RequestMethod {
        case get
    }
    
    enum Endpoint: String {
        case lover = "/api/v4/ios/lovers/lover"
        case requestLover = "/api/v4/ios/lovers/request-list"
        case acceptLover = "/api/v4/ios/lovers/accept"
        case rejectLover = "/api/v4/ios/lovers/reject"
        case removeLover = "/api/v4/ios/lovers/remove"
        case requestALover = "/api/v4/ios/lovers/request"
        case requestFCMWatch = "/api/v4/ios/lovers/watch-fcm"
    }
    
    func callAPILover(onSuccess successCallback: ((_ data: Lover) -> Void)?,
                      onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.lover.rawValue
        let loversUserToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        header = ["Accept": "application/json", "Authorization": "Bearer \(loversUserToken)"]
        
        // call API
        self.createRequest(
            url, method: .get, headers: header, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let dataDictionary = responseObject["data"].dictionary {
                       let loverData = Lover.build(dataDictionary)
                       successCallback?(loverData)
                    }
                    
                    break
                case 204:
                    failureCallback?("An error has occured. statusCode == 204")
                    break
                case 400...500:
                    failureCallback?("An error has occured. statusCode == 400...500")
                    break
                default:
                    failureCallback?("An error has occured.")
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    func callAPIRequestLover(onSuccess successCallback: ((_ data: [RequestLover]) -> Void)?,
                      onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        // Build URL
        let url = API_BASE_URL + Endpoint.requestLover.rawValue
        let loversUserToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        header = ["Accept": "application/json", "Authorization": "Bearer \(loversUserToken)"]
        
        // call API
        self.createRequest(
            url, method: .get, headers: header, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let responseDict = responseObject["data"]["data"].arrayObject {
                       let requestLoverDict = responseDict as! [[String:AnyObject]]

                        // Create object
                        var data = [RequestLover]()
                        for item in requestLoverDict {
                            let single = RequestLover.build(item)
                            data.append(single)
                        }

                        // Fire callback
                        successCallback?(data)
                    } else {
                        failureCallback?("An error has occured, when parsing json array")
                    }
                    
                    break
                case 204:
                    failureCallback?("An error has occured. statusCode == 204")
                    break
                case 400...500:
                    failureCallback?("An error has occured. statusCode == 400...500")
                    break
                default:
                    failureCallback?("An error has occured.")
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    func callAPISearchLover(withKeyword: String, onSuccess successCallback: ((_ data: [SearchLover]) -> Void)?,
                      onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        // Build URL
        let url = "\(API_BASE_URL)/api/v4/ios/lovers/search-lover?\(withKeyword)"
        let loversUserToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        header = ["Accept": "application/json", "Authorization": "Bearer \(loversUserToken)"]
        
        // call API
        self.createRequest(
            url, method: .get, headers: header, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let responseDict = responseObject["data"]["data"].arrayObject {
                       let requestLoverDict = responseDict as! [[String:AnyObject]]

                        // Create object
                        var data = [SearchLover]()
                        for item in requestLoverDict {
                            let single = SearchLover.build(item)
                            data.append(single)
                        }

                        // Fire callback
                        successCallback?(data)
                    } else {
                        failureCallback?("An error has occured, when parsing json array")
                    }
                    
                    break
                case 204:
                    failureCallback?("An error has occured. statusCode == 204")
                    break
                case 400...500:
                    failureCallback?("An error has occured. statusCode == 400...500")
                    break
                default:
                    failureCallback?("An error has occured.")
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    func callAPIRequestALover(lover_id: Int, onSuccess successCallback: ((_ successMessage: String) -> Void)?,
                      onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = API_BASE_URL + Endpoint.requestALover.rawValue
        let loversUserToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        params = ["lover_id": lover_id]
        header = ["Accept": "application/json", "Authorization": "Bearer \(loversUserToken)"]
    
        self.createRequestWithParams(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                switch statusCode {
                case 200:
                    successCallback?("SUCCESS")
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    func callAPIAcceptLover(lover_id: Int, onSuccess successCallback: ((_ successMessage: String) -> Void)?,
                      onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = API_BASE_URL + Endpoint.acceptLover.rawValue
        let loversUserToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        params = ["lover_id": lover_id]
        header = ["Accept": "application/json", "Authorization": "Bearer \(loversUserToken)"]
    
        self.createRequestWithParams(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                switch statusCode {
                case 200:
                    successCallback?("SUCCESS")
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    func callAPIRejectLover(lover_id: Int, onSuccess successCallback: ((_ successMessage: String) -> Void)?,
                      onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        // Build URL
        params = ["lover_id": lover_id]
        let url = API_BASE_URL + Endpoint.rejectLover.rawValue
        let loversUserToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        header = ["Accept": "application/json", "Authorization": "Bearer \(loversUserToken)"]
        
        // call API
        self.createRequestWithParams(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                
                switch statusCode {
                case 200:
                    successCallback?("SUCCESS")
                    break
                case 204:
                    failureCallback?("An error has occured. statusCode == 204")
                    break
                case 400...500:
                    failureCallback?("An error has occured. statusCode == 400...500")
                    break
                default:
                    failureCallback?("An error has occured.")
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    func callAPIRemoveLover(id: Int, onSuccess successCallback: ((_ successMessage: String) -> Void)?,
                      onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        // Build URL
        params = ["id": id]
        let url = API_BASE_URL + Endpoint.removeLover.rawValue
        let loversUserToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        header = ["Accept": "application/json", "Authorization": "Bearer \(loversUserToken)"]
        
        // call API
        self.createRequestWithParams(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                
                switch statusCode {
                case 200:
                    successCallback?("SUCCESS")
                    break
                case 204:
                    failureCallback?("An error has occured. statusCode == 204")
                    break
                case 400...500:
                    failureCallback?("An error has occured. statusCode == 400...500")
                    break
                default:
                    failureCallback?("An error has occured.")
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    func callAPIRequestFCMWatch(id: Int, onSuccess successCallback: ((_ successMessage: String) -> Void)?,
                      onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = API_BASE_URL + Endpoint.requestFCMWatch.rawValue
        let loversUserToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        params = ["id": id]
        header = ["Accept": "application/json", "Authorization": "Bearer \(loversUserToken)"]
    
        self.createRequestWithParams(
            url, method: .post, headers: header, parameters: params, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                let message = responseObject["message"].stringValue
                
                switch statusCode {
                case 200:
                    successCallback?("SUCCESS")
                    break
                case 204:
                    failureCallback?(message)
                    break
                case 400...500:
                    failureCallback?(message)
                    break
                default:
                    failureCallback?(message)
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    // MARK: Request Handler
    // Create request
    func createRequest(
        _ url: String,
        method: HTTPMethod,
        headers: [String: String],
        encoding: JSONEncoding,
        onSuccess successCallback: ((JSON) -> Void)?,
        onFailure failureCallback: ((String) -> Void)?
        ) {
        
        Alamofire.request(url, method: method, encoding: encoding, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                successCallback?(json)
            case .failure(let error):
                if let callback = failureCallback {
                    // Return
                    callback(error.localizedDescription)
                }
            }
        }
    }
    
    func createRequestWithParams(
        _ url: String,
        method: HTTPMethod,
        headers: [String: String],
        parameters: [String: Any],
        encoding: JSONEncoding,
        onSuccess successCallback: ((JSON) -> Void)?,
        onFailure failureCallback: ((String) -> Void)?
        ) {
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                successCallback?(json)
            case .failure(let error):
                if let callback = failureCallback {
                    // Return
                    callback(error.localizedDescription)
                }
            }
        }
    }
}
