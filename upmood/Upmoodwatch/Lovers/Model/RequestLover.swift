//
//  PublicSession.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 14/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

class RequestLover {
    var id: Int?
    var lover_id: Int?
    var name: String?
    var image: String?
    
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: AnyObject]) {
        if let data = dict["id"] as? Int {
            self.id = data
        }
        if let data = dict["lover_id"] as? Int {
            self.lover_id = data
        }
        if let data = dict["name"] as? String {
            self.name = data
        }
        if let data = dict["image"] as? String {
            self.image = data
        }
    }
    
    // MARK: Class Method
    class func build(_ dict: [String: AnyObject]) -> RequestLover {
        let data = RequestLover()
        data.loadFromDictionary(dict)
        return data
    }
}

