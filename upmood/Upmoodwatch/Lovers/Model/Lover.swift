//
//  User.swift
//  ExerciseProject
//
//  Created by Joseph Mikko Mañoza on 17/02/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import SwiftyJSON

class Lover {
    var id: Int?
    var name: String?
    var image: String?
    
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: JSON]) {
        if let data = dict["id"]?.intValue {
            self.id = data
        }
        if let data = dict["name"]?.stringValue {
            self.name = data
        }
        if let data = dict["image"]?.stringValue {
            self.image = data
        }
    }
    
    // MARK: Class Method
    class func build(_ dict: [String: JSON]) -> Lover {
        let data = Lover()
        data.loadFromDictionary(dict)
        return data
    }
}
