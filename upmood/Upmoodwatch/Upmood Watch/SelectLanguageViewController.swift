//
//  SelectLanguageViewController.swift
//  upmood
//
//  Created by Taison Digital on 12/3/20.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class SelectLanguageViewController: UIViewController {

    @IBOutlet weak var img0: UIImageView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var img6: UIImageView!
    @IBOutlet weak var img7: UIImageView!
    @IBOutlet weak var img8: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getLanguageSelected(withIndex: Constants.getLangSelected)
    }
    
    @IBAction func btnLanguage(_ sender: UIButton) {
        buttonFunction(withIndex: sender.tag)
    }
    
    func buttonFunction(withIndex: Int){
       switch withIndex {
        case 0:
            imageCheck(img0c: false, img1c: true, img2c: true, img3c: true, img4c: true, img5c: true, img6c: true, img7c: true, img8c: true, index: 0)
            constantsValue(time: 2, distance: 2, wrist: 2, lang: 0)
        case 1:
            imageCheck(img0c: true, img1c: false, img2c: true, img3c: true, img4c: true, img5c: true, img6c: true, img7c: true, img8c: true, index: 1)
            constantsValue(time: 2, distance: 2, wrist: 2, lang: 1)
        case 2:
            imageCheck(img0c: true, img1c: true, img2c: false, img3c: true, img4c: true, img5c: true, img6c: true, img7c: true, img8c: true, index: 2)
            constantsValue(time: 2, distance: 2, wrist: 2, lang: 2)
        case 3:
            imageCheck(img0c: true, img1c: true, img2c: true, img3c: false, img4c: true, img5c: true, img6c: true, img7c: true, img8c: true, index: 3)
            constantsValue(time: 2, distance: 2, wrist: 2, lang: 3)
        case 4:
            imageCheck(img0c: true, img1c: true, img2c: true, img3c: true, img4c: false, img5c: true, img6c: true, img7c: true, img8c: true, index: 4)
            constantsValue(time: 2, distance: 2, wrist: 2, lang: 4)
        case 5:
            imageCheck(img0c: true, img1c: true, img2c: true, img3c: true, img4c: true, img5c: false, img6c: true, img7c: true, img8c: true, index: 5)
            constantsValue(time: 2, distance: 2, wrist: 2, lang: 5)
        case 6:
            imageCheck(img0c: true, img1c: true, img2c: true, img3c: true, img4c: true, img5c: true, img6c: false, img7c: true, img8c: true, index: 6)
            constantsValue(time: 2, distance: 2, wrist: 2, lang: 6)
        case 7:
            imageCheck(img0c: true, img1c: true, img2c: true, img3c: true, img4c: true, img5c: true, img6c: true, img7c: false, img8c: true, index: 7)
            constantsValue(time: 2, distance: 2, wrist: 2, lang: 7)
        case 8:
            imageCheck(img0c: true, img1c: true, img2c: true, img3c: true, img4c: true, img5c: true, img6c: true, img7c: true, img8c: false, index: 8)
            constantsValue(time: 2, distance: 2, wrist: 2, lang: 8)

        default: break
        }
    }
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func imageCheck(img0c: Bool,
                    img1c: Bool,
                    img2c: Bool,
                    img3c: Bool,
                    img4c: Bool,
                    img5c: Bool,
                    img6c: Bool,
                    img7c: Bool,
                    img8c: Bool,
                    index: Int){
        
        img0.isHidden = img0c
        img1.isHidden = img1c
        img2.isHidden = img2c
        img3.isHidden = img3c
        img4.isHidden = img4c
        img5.isHidden = img5c
        img6.isHidden = img6c
        img7.isHidden = img7c
        img8.isHidden = img8c
        Constants.getLangSelected = index
        
    }
    
    func getLanguageSelected(withIndex: Int){
        switch withIndex {
        case 0:
            imageCheck(img0c: false, img1c: true, img2c: true, img3c: true, img4c: true, img5c: true, img6c: true, img7c: true, img8c: true, index: 0)
        case 1:
            imageCheck(img0c: true, img1c: false, img2c: true, img3c: true, img4c: true, img5c: true, img6c: true, img7c: true, img8c: true, index: 1)
        case 2:
            imageCheck(img0c: true, img1c: true, img2c: false, img3c: true, img4c: true, img5c: true, img6c: true, img7c: true, img8c: true, index: 2)
        case 3:
            imageCheck(img0c: true, img1c: true, img2c: true, img3c: false, img4c: true, img5c: true, img6c: true, img7c: true, img8c: true, index: 3)
        case 4:
            imageCheck(img0c: true, img1c: true, img2c: true, img3c: true, img4c: false, img5c: true, img6c: true, img7c: true, img8c: true, index: 4)
        case 5:
            imageCheck(img0c: true, img1c: true, img2c: true, img3c: true, img4c: true, img5c: false, img6c: true, img7c: true, img8c: true, index: 5)
        case 6:
            imageCheck(img0c: true, img1c: true, img2c: true, img3c: true, img4c: true, img5c: true, img6c: false, img7c: true, img8c: true, index: 6)
        case 7:
            imageCheck(img0c: true, img1c: true, img2c: true, img3c: true, img4c: true, img5c: true, img6c: true, img7c: false, img8c: true, index: 7)
        case 8:
            imageCheck(img0c: true, img1c: true, img2c: true, img3c: true, img4c: true, img5c: true, img6c: true, img7c: true, img8c: false, index: 8)
        default: break
        }
    }
    
    func constantsValue(time: Int, distance: Int, wrist: Int, lang: Int ){
        Constants.timeDisplayStr = time
        Constants.distanceStr = distance
        Constants.wristSenseStr = wrist
        Constants.langStr = lang
        watchSettingObserver()
    }
    
    func watchSettingObserver(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "watchSettingCommand"), object: nil)
    }

}
