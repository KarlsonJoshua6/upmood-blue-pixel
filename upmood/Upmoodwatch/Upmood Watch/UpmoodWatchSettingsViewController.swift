//
//  UpmoodWatchSettingsViewController.swift
//  upmood
//
//  Created by Taison Digital on 16/09/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class UpmoodWatchSettingsViewController: UIViewController {

    @IBOutlet weak var outlineView: UIView!
    
    @IBOutlet weak var img12hrs: UIImageView!
    @IBOutlet weak var img24hrs: UIImageView!
    
    @IBOutlet weak var lbl12hrs: UILabel!
    @IBOutlet weak var lbl24hrs: UILabel!
    @IBOutlet weak var loverNameLabel: UILabel!
    
    @IBOutlet weak var categorySwitch: UISwitch!
    
    @IBOutlet weak var lblLang: UILabel!
    var categorySwitchIsOn:Bool =  false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
        categorySwitch.addTarget(self, action:#selector(categorySwitchValueChanged(_:)), for: .valueChanged)

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        firstload()
        lblLang.text = getStringLang(withIndex: Constants.getLangSelected)
    }
    
    //UIImage(named: "radio")
    
    func initView(){
        getDate()
        getWirstSense()
        getRadiuesView()
        
       
    }
    
    @objc func categorySwitchValueChanged(_ sender : UISwitch!){

          if sender.isOn {
                categorySwitchIsOn =  true
                Constants.wristSenseStr = 0
                Constants.distanceStr = 2
                Constants.timeDisplayStr = 2
                Constants.langStr = 9
                Constants.getWristsense = 0
          } else {

              categorySwitchIsOn =  false
              Constants.wristSenseStr = 1
              Constants.distanceStr = 2
              Constants.timeDisplayStr = 2
              Constants.langStr = 9
              Constants.getWristsense = 1
          }
        
        watchSettingObserver()

      }
    
    func firstload(){
        let lover_name = UserDefaults.standard.string(forKey: "lover_name") ?? "No Lover's Yet"
        loverNameLabel.text = lover_name
    }
    
    func getStringLang(withIndex: Int)->String{
        var language = ""
        switch withIndex {
        case 0:
            language = "English"
        case 1:
            language = "日本"
        case 2:
            language = "简体中文"
        case 3:
            language = "繁體中文"
        case 4:
            language = "한국어"
        case 5:
            language = "Español"
        case 6:
            language = "Français"
        case 7:
            language = "Italiano"
        case 8:
            language = "Deutsch"
        default: break
        }
        return language
    }
    
    func getDate(){
        switch Constants.getTimeDate{
        case 0:
            getTimeRadio(name1: "radio", name: "ucCircle", color1: "#63C2D1" ,color2: "#8C9CAD", time: 0)
                      img12hrs.setImageColor(color: hexStringToUIColor(hex: Constants.Color.NEW_COLOR))
        case 1:
            getTimeRadio(name1: "ucCircle", name: "radio", color1: "#8C9CAD" ,color2: "#63C2D1", time: 1)
            img24hrs.setImageColor(color: hexStringToUIColor(hex: Constants.Color.NEW_COLOR))
        case 2:
            getTimeRadio(name1: "radio", name: "radio", color1: "#8C9CAD" ,color2: "#8C9CAD", time: 2)
            img24hrs.isHidden = true
            img12hrs.isHidden = true
        default: break
        }
    }
    
    func getWirstSense(){
        switch Constants.getWristsense {
        case 0:
            categorySwitch.isOn = true
        case 1:
            categorySwitch.isOn = false
        default: break
        }
    }
    
    func getRadiuesView(){
        outlineView.layer.cornerRadius = 10
        outlineView.clipsToBounds = true
        outlineView.layer.borderColor = hexStringToUIColor(hex: "#EBEBEB").cgColor
        outlineView.layer.borderWidth = 1
    }

    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func selectedTime(_ sender: UIButton) {
        switch sender.tag{
        case 0:
            getTimeRadio(name1: "radio", name: "ucCircle", color1: "#63C2D1" ,color2: "#8C9CAD", time: 0)
            img12hrs.setImageColor(color: hexStringToUIColor(hex: Constants.Color.NEW_COLOR))
            Constants.timeDisplayStr = 0
            Constants.distanceStr = 2
            Constants.wristSenseStr = 2
            Constants.langStr = 9
          
            img12hrs.isHidden = false
    
          
        case 1:
            getTimeRadio(name1: "ucCircle", name: "radio", color1: "#8C9CAD" ,color2: "#63C2D1", time: 1)
            img24hrs.setImageColor(color: hexStringToUIColor(hex: Constants.Color.NEW_COLOR))
            Constants.timeDisplayStr = 1
            Constants.distanceStr = 2
            Constants.wristSenseStr = 2
            Constants.langStr = 9
            
            img24hrs.isHidden = false
        default:
            break
        }
        watchSettingObserver()
       
    }
    
    func watchSettingObserver(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "watchSettingCommand"), object: nil)
    }
    
    func getTimeRadio(name1: String, name: String, color1: String, color2: String, time: Int){
        
        img12hrs.image = UIImage(named: name1)
        img24hrs.image = UIImage(named: name)
               
        lbl12hrs.textColor = hexStringToUIColor(hex: color1)
        lbl24hrs.textColor = hexStringToUIColor(hex: color2)
        Constants.getTimeDate = time
    }
    
}
