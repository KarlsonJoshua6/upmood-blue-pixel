//
//  SleepModeViewController.swift
//  upmood
//
//  Created by Taison Digital on 16/09/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class SleepModeViewController: UIViewController {
    
    @IBOutlet weak var outlineView: UIView!
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var syncView: UIView!
    
    let viewControllerIdentifiers1 = ["dailyVC", "weeklyVC"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()

    }
    
    func initView(){
        getRadiuesView()
        
        setUpSegmentControl(indexOf: 0)
           
    }
    
    private func setUpSegmentControl(indexOf: Int) {
        let newController = storyboard!.instantiateViewController(withIdentifier: viewControllerIdentifiers1[indexOf])
        let oldController = childViewControllers.last!
        
        oldController.willMove(toParentViewController: nil)
        addChildViewController(newController)
        newController.view.frame = oldController.view.frame
        
        transition(from: oldController, to: newController, duration: 0.25, options: .transitionCrossDissolve, animations: {
            // nothing needed here
        }, completion: { _ -> Void in
            oldController.removeFromParentViewController()
            newController.didMove(toParentViewController: self)
        })
    }
    
    func getRadiuesView(){
        outlineView.layer.cornerRadius = 10
        outlineView.clipsToBounds = true
        
        syncView.layer.cornerRadius = 5
        syncView.clipsToBounds = true
        
     
    }
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func segmentIndex(_ sender: UISegmentedControl) {
        setUpSegmentControl(indexOf: sender.selectedSegmentIndex)
    }
    
    

}
