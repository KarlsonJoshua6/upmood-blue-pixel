//
//  FitnessService.swift
//  upmood
//
//  Created by Taison Digital on 10/29/20.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

class FitnessService {
    
    public func callAPIWeeklyStats(date: String, onSuccess successCallback: ((_ data: WeeklyStatisticModel) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        FitnessAPICallManager.instance.callAPIWeeklyStats(date: date, onSuccess: { (data) in
            successCallback?(data)
        }) { (errorMessage) in
            failureCallback?(errorMessage)
        }
    }
    
    public func callAPIMonthlyStats(date: String, onSuccess successCallback: ((_ data: MonthlyStatisticModel) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        FitnessAPICallManager.instance.callAPIMonthlyStats(date: date, onSuccess: { (data) in
            successCallback?(data)
        }) { (errorMessage) in
            failureCallback?(errorMessage)
        }
    }
    
    public func callAPIYearlyStats(date: String, onSuccess successCallback: ((_ data: YearlyStatisticModel) -> Void)? , onSuccess successCallback1: ((_ data: [YearlyChartModel]) -> Void)?, onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        FitnessAPICallManager.instance.callAPIYearlyStats(date: date, onSuccess: { (data) in
            successCallback?(data)
        }, onSuccess: { (data) in
            successCallback1?(data)
        }) { (errorMessage) in
            failureCallback?(errorMessage)
        }
    }
}
