//
//  FitnessAPICallManager.swift
//  upmood
//
//  Created by Taison Digital on 10/29/20.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import SwiftyJSON
import Foundation

class FitnessAPICallManager {
    
    var header: HTTPHeaders = [:]
    var params: [String: Any] = [:]
    
    static let instance = FitnessAPICallManager()
    
    enum RequestMethod {
        case get
    }
    
    enum Endpoint: String {
        case weeklyStats = "/api/v4/ios/statistics/weekly"
        case monthlyStats = "/api/v4/ios/statistics/monthly"
        case yearlyStats = "/api/v4/ios/statistics/yearly"
    }
    
    func callAPIWeeklyStats(date: String, onSuccess successCallback: ((_ data: WeeklyStatisticModel) -> Void)?,
                      onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = API_BASE_URL + Endpoint.weeklyStats.rawValue + "?date=\(date)"
        let userToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        header = ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        
        print("test url: \(url)")
    
        // call API
        self.createRequest(
            url, method: .get, headers: header, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                
                //print("test responseObject: \(responseObject)")
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let mainDataDict = responseObject["data"].dictionary {
                       let mainData = WeeklyStatisticModel.build(mainDataDict)
                       successCallback?(mainData)
                    }
                    break
                case 204:
                    failureCallback?("An error has occured. statusCode == 204")
                    break
                case 400...500:
                    failureCallback?("An error has occured. statusCode == 400...500")
                    break
                default:
                    failureCallback?("An error has occured.")
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    func callAPIMonthlyStats(date: String, onSuccess successCallback: ((_ data: MonthlyStatisticModel) -> Void)?,
                      onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = API_BASE_URL + Endpoint.monthlyStats.rawValue + "?date=\(date)"
        let userToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""

        header = ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        print("test url: \(url)")
    
        // call API
        self.createRequest(
            url, method: .get, headers: header, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                
                switch statusCode {
                case 200:
                    // Create dictionary
                    if let mainDataDict = responseObject["data"].dictionary {
                       let mainData = MonthlyStatisticModel.build(mainDataDict)
                       successCallback?(mainData)
                    }
                    
                    break
                case 204:
                    failureCallback?("An error has occured. statusCode == 204")
                    break
                case 400...500:
                    failureCallback?("An error has occured. statusCode == 400...500")
                    break
                default:
                    failureCallback?("An error has occured.")
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    func callAPIYearlyStats(date: String, onSuccess successCallback: ((_ data: YearlyStatisticModel) -> Void)?, onSuccess successCallback1: ((_ data: [YearlyChartModel]) -> Void)?,
                      onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        let url = API_BASE_URL + Endpoint.yearlyStats.rawValue + "?date=\(date)"
        let userToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        print("userToken", userToken)
        header = ["Accept": "application/json", "Authorization": "Bearer \(userToken)"]
        print("test url: \(url)")
    
        // call API
        self.createRequest(
            url, method: .get, headers: header, encoding: JSONEncoding.default,
            onSuccess: {(responseObject: JSON) -> Void in
                
                let statusCode = responseObject["status"].intValue
                
                
                switch statusCode {
                case 200:
                    // Create dictionary
                     if let mainDataDict = responseObject["data"].dictionary {
                        
                        if let responseDict = mainDataDict["chart"]?.arrayObject {
                            let yearlyStatsDict = responseDict as! [[String:AnyObject]]
                            
                            // Create object
                            var data = [YearlyChartModel]()
                            for item in yearlyStatsDict {
                                let single = YearlyChartModel.build(item)
                                data.append(single)
                            }
                            successCallback1?(data)
                        }
                        
                        let mainData = YearlyStatisticModel.build(mainDataDict)
                        successCallback?(mainData)
                                       
                    } else {
                        failureCallback?("An error has occured, when parsing json array")
                    }
                    
                    break
                case 204:
                    failureCallback?("An error has occured. statusCode == 204")
                    break
                case 400...500:
                    failureCallback?("An error has occured. statusCode == 400...500")
                    break
                default:
                    failureCallback?("An error has occured.")
                }
            },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
            }
        )
    }
    
    // MARK: Request Handler
    // Create request

    func createRequest(
        _ url: String,
        method: HTTPMethod,
        headers: [String: String],
        encoding: JSONEncoding,
        onSuccess successCallback: ((JSON) -> Void)?,
        onFailure failureCallback: ((String) -> Void)?
        ) {
        
        Alamofire.request(url, method: method, encoding: encoding, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                successCallback?(json)
            case .failure(let error):
                if let callback = failureCallback {
                    // Return
                    callback(error.localizedDescription)
                }
            }
        }
    }
}
