//
//  FitnessPresenter.swift
//  upmood
//
//  Created by Taison Digital on 10/29/20.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

// MARK: - Weekly

struct WeeklyStatsViewData {
    let daily_calories: Float
    let daily_steps: Float
    let total_calories: Float
    let total_steps: Int
    let chartData: [WeeklyChartViewData]
}

struct WeeklyChartViewData {
    var id: Int
    var user_id: Int
    var date: String
    var steps: String
    var calories: String
}


// MARK: - Monthly

struct MonthlyStatsViewData {
    let daily_calories: Float
    let daily_steps: Float
    let weekly_calories: Float
    let weekly_steps: Float
    let total_calories: Float
    let total_steps: Int
    let chartData: [MonthlyChartViewData]
}

struct MonthlyChartViewData {
    var id: Int
    var user_id: Int
    var date: String
    var steps: String
    var calories: String
}

// MARK: - Yealy

struct YearlyStatsViewData {
       let daily_calories: Float
       let daily_steps: Float
       let monthly_calories: Float
       let monthly_steps: Float
       let total_calories: Int
       let total_steps: Int
}

struct YearlyChartViewData {
    let id: Int
    let user_id: Int
    let date: String
    let steps: Int
    let calories: Float
}

// MARK: - Setup View with Object Protocol

protocol FitnessView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func getMonthlyStats(data: MonthlyStatsViewData)
    func getWeekly(data: WeeklyStatsViewData)
    func getYearly(data: [YearlyChartViewData])
    func getYearlyChartStats(data: YearlyStatsViewData )
    func setEmpty()
    func getError(withMessage: String)
}

// MARK: - Setup Presenter

class FitnessPresenter {
    
    private let fitnessService: FitnessService
    weak private var fitnessView : FitnessView?
    
    init(fitnessService: FitnessService) {
        self.fitnessService = fitnessService
    }
    
    func attachView(view: FitnessView) {
        fitnessView = view
    }
    
    func detachView() {
        fitnessView = nil
    }
    
    func getWeeklyStats(date: String) {
        self.fitnessView?.startLoading()
            
        fitnessService.callAPIWeeklyStats(date: date, onSuccess: { (data) in
            
            if data.daily_calories ?? 0.0 != 0.0 {
                let mappedChardData = data.chartData!.map {
                    return WeeklyChartViewData(id: $0.id ?? 0, user_id: $0.user_id ?? 0, date: "\($0.date ?? "")", steps: "\($0.steps ?? "")", calories: "\($0.calories ?? "")")
                }

                let mappedData = WeeklyStatsViewData(daily_calories: data.daily_calories ?? 0.0, daily_steps: data.daily_steps ?? 0.0, total_calories: data.total_calories ?? 0.0, total_steps: data.total_steps ?? 0, chartData: mappedChardData)
                self.fitnessView?.getWeekly(data: mappedData)
            } else {
                self.fitnessView?.setEmpty()
            }
            
            self.fitnessView?.finishLoading()

        }) { (errorMessage) in
            print(errorMessage)
        }
    }

    func getMonthlyStats(date: String) {
        self.fitnessView?.startLoading()
        
        fitnessService.callAPIMonthlyStats(date: date, onSuccess: { (data) in
            
            if data.daily_calories ?? 0.0 != 0.0 {
                let mappedChardData = data.chartData!.map {
                    return MonthlyChartViewData(id: $0.id ?? 0, user_id: $0.user_id ?? 0, date: "\($0.date ?? "")", steps: "\($0.steps ?? "")", calories: "\($0.calories ?? "")")
                }

                let monthlyMappedData = MonthlyStatsViewData(daily_calories: data.daily_calories ?? 0.0, daily_steps: data.daily_steps ?? 0.0, weekly_calories: data.weekly_calories ?? 0.0, weekly_steps: data.weekly_steps ?? 0.0, total_calories: data.total_calories ?? 0.0, total_steps: data.total_steps ?? 0, chartData: mappedChardData)
                
                self.fitnessView?.getMonthlyStats(data: monthlyMappedData)
            } else {
                self.fitnessView?.setEmpty()
            }

            self.fitnessView?.finishLoading()
            
        }) { (errorMessage) in
            self.fitnessView?.setEmpty()
            self.fitnessView?.getError(withMessage: errorMessage)
            debugPrint("weeklyStats() error: \(errorMessage)")
        }
    }
    
     func getYearlyStats(date: String) {
        self.fitnessView?.startLoading()
        
        fitnessService.callAPIYearlyStats(date: date, onSuccess: { (data) in
            self.fitnessView?.finishLoading()
           
            if data.daily_calories ?? 0.0 != 0.0 {
                let mappedSession = YearlyStatsViewData(daily_calories: data.daily_calories ?? 0.0, daily_steps: data.daily_steps ?? 0.0, monthly_calories: data.monthly_calories ?? 0.0, monthly_steps: data.monthly_steps ?? 0.0, total_calories: Int(data.total_calories ?? 0), total_steps: data.total_steps ?? 0)
                    
                self.fitnessView?.getYearlyChartStats(data: mappedSession)
            }else{
                self.fitnessView?.setEmpty()
            }
               
            
        }, onSuccess: { (data) in
            self.fitnessView?.finishLoading()
            
            let mappedChardData = data.map {
                return YearlyChartViewData(id: $0.id ?? 0, user_id: $0.user_id ?? 0, date: "\($0.date ?? "")", steps: $0.steps ?? 0, calories: $0.calories ?? 0.0)
        }
            
                                
        self.fitnessView?.getYearly(data: mappedChardData)
        }) { (errorMessage) in
            self.fitnessView?.setEmpty()
            self.fitnessView?.getError(withMessage: errorMessage)
            debugPrint("weeklyStats() error: \(errorMessage)")
        }
    }
    

}
