//
//  CalYearlyViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 04/11/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Charts
import UIKit

class CalYearlyViewController: UIViewController {

    @IBOutlet weak var barChart: BarChartView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var totalStepsLabel: UILabel!
    @IBOutlet weak var avgWeeklyLabel: UILabel!
    @IBOutlet weak var avgDailyLabel: UILabel!
    @IBOutlet weak var emptyStateView: UIView!
    
    var maxNumberOfMonth = 0
    var integerArray = [Int]()
    let presenter = FitnessPresenter(fitnessService: FitnessService())
    var yearlyDataToDisplay = [YearlyStatsViewData]()
    var yearlyInt = 0
    var maxYear = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.attachView(view: self)
        getYearly()
    }
    
    @IBAction func btnPrevious(_ sender: Any) {
        yearlyInt -= 1
        yearLabel.text = "\(yearlyInt)"
        presenter.getYearlyStats(date: "\(yearlyInt)")
    }
    
    @IBAction func btnNext(_ sender: Any) {
        if yearlyInt < maxYear {
            yearlyInt += 1
            yearLabel.text = "\(yearlyInt)"
            presenter.getYearlyStats(date: "\(yearlyInt)")
        }else {
            
        }
    }
    
    private func setUpChart(yValue : [Float], firstMonth: Int, lastMonth: Int) {
        
        self.integerArray.removeAll()
        for i in firstMonth...lastMonth {
            self.integerArray.append(i)
        }
        barChart.isUserInteractionEnabled = false
        barChart.leftAxis.enabled = true
        barChart.rightAxis.enabled = false
        barChart.xAxis.enabled = true
        barChart.legend.enabled = false
        barChart.xAxis.labelPosition = XAxis.LabelPosition.bottom
        barChart.xAxis.granularityEnabled = true
                 
        self.barChart.xAxis.valueFormatter = DefaultAxisValueFormatter(block: {(index, _) in
            let xStrings = self.integerArray.map { String($0) }
            return xStrings[Int(index)]
        })
                 
        var dataEntries: [BarChartDataEntry] = []
        for i in 0..<yValue.count {
           let dataEntry = BarChartDataEntry(x: Double(i), y: Double(yValue[i]))
           dataEntries.append(dataEntry)
        }
                 
        let chartDataSet = BarChartDataSet(entries: dataEntries)
        chartDataSet.drawValuesEnabled = false
        chartDataSet.colors = [hexStringToUIColor(hex: "#3BB5EA")]
                 
        let chartData = BarChartData(dataSet: chartDataSet)
        chartData.barWidth = Double(0.50)

        barChart.data = chartData
    }

}

extension CalYearlyViewController: FitnessView {
    func getYearly(data: [YearlyChartViewData]) {
        let values = data.map { $0.calories }
        let dates = data.map { Int($0.date.dropFirst(5)) ?? 0 }
        
        self.emptyStateView.isHidden = true
        
        setUpChart(yValue: values, firstMonth: dates.first ?? 0, lastMonth: dates.last ?? 0)
    }
    
    func getYearlyChartStats(data: YearlyStatsViewData) {
        self.emptyStateView.isHidden = true
        
        if data != nil {
            totalStepsLabel.text = "\(Int(data.total_calories))"
            avgWeeklyLabel.text = "\(Int(data.monthly_calories))"
            avgDailyLabel.text = "\(Int(data.daily_calories))"
        }
    }
    
    func getWeekly(data: WeeklyStatsViewData) {
        //
    }
    
    func getMonthlyStats(data: MonthlyStatsViewData) {
        //
    }
    
    func startLoading() {
        //
    }
    
    func finishLoading() {
        //
    }
    
    func setEmpty() {
        emptyStateView.isHidden = false
    }
    
    func getError(withMessage: String) {
        //
    }
    
}

extension CalYearlyViewController {
    
    func getYearly(){
      let strDate = Date().today(format: "yyyy.MM.dd")
        let date = strDate.components(separatedBy: ".")
        let year = date[0]
        yearlyInt = Int(year) ?? 0
        maxYear = Int(year) ?? 0
        yearLabel.text = "\(yearlyInt)"
        presenter.getYearlyStats(date: "\(yearlyInt)")
        totalStepsLabel.text = "0"
        avgWeeklyLabel.text = "0"
        avgDailyLabel.text = "0"
    }
}
