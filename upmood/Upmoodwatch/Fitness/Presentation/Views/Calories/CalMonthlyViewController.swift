//
//  CalMonthlyViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 04/11/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Charts
import UIKit

class CalMonthlyViewController: UIViewController {
    
    @IBOutlet weak var barChart: BarChartView!
    @IBOutlet weak var avgDailyLabel: UILabel!
    @IBOutlet weak var avgWeeklyLabel: UILabel!
    @IBOutlet weak var avgTotalLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var emptyStateView: UIView!
    
    var integerArray = [Int]()
    let presenter = FitnessPresenter(fitnessService: FitnessService())
    var monthlyDataToDisplay: MonthlyStatsViewData?
    var monthlyInt = 0
    var yearlyInt = 0
    var dateArr = [Date]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        //registerObs()
        getMonth()
    }
    
    @IBAction func btnPrevious(_ sender: Any) {
        dateArr.removeAll()
        monthlyInt = monthlyInt - 1
        
        if monthlyInt == 0 {
            monthlyInt = 1
        }
        
        if monthlyInt < 10 {
            let monthInt = String(format: "%02d", monthlyInt)
            presenter.getMonthlyStats(date: "\(yearlyInt)-\(monthInt)")
        } else {
            presenter.getMonthlyStats(date: "\(yearlyInt)-\(monthlyInt)")
        }
        
        monthLabel.text = "\(convert(monthNumber: monthlyInt)) \(yearlyInt)"
        
        //NotificationCenter.default.post(name: .didPreviousMonthlySteps, object: nil)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        dateArr.removeAll()
        monthlyInt = monthlyInt + 1
        
        if monthlyInt > 12 {
            monthlyInt = 1
        }
        
        if monthlyInt < 10 {
            let monthInt = String(format: "%02d", monthlyInt)
            presenter.getMonthlyStats(date: "\(yearlyInt)-\(monthInt)")
        } else {
            presenter.getMonthlyStats(date: "\(yearlyInt)-\(monthlyInt)")
        }
        
        monthLabel.text = "\(convert(monthNumber: monthlyInt)) \(yearlyInt)"
        
        //NotificationCenter.default.post(name: .didNextMonthlySteps, object: nil)
    }
    
    private func setUpChart(yValue : [Float], date: [Date]) {
        
        barChart.isUserInteractionEnabled = false
        barChart.leftAxis.enabled = true
        barChart.rightAxis.enabled = false
        barChart.xAxis.enabled = true
        barChart.legend.enabled = false
        barChart.xAxis.labelPosition = XAxis.LabelPosition.bottom
        barChart.xAxis.granularityEnabled = true
        
        self.barChart.xAxis.valueFormatter = DefaultAxisValueFormatter(block: {(index, _) in
            let df = DateFormatter()
            df.dateFormat = "dd"
            let strings = date.map{df.string(from: $0)}
            
            return strings[Int(index)]
        })
        
        var dataEntries: [BarChartDataEntry] = []
        for i in 0..<yValue.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: Double(yValue[i]))
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(entries: dataEntries)
        chartDataSet.drawValuesEnabled = false
        chartDataSet.colors = [hexStringToUIColor(hex: "#3BB5EA")]
        
        let chartData = BarChartData(dataSet: chartDataSet)
        chartData.barWidth = Double(0.50)
        
        barChart.data = chartData
    }
    
    private func setUpLabels() {
        if monthlyDataToDisplay != nil {
            avgDailyLabel.text  = "\(Int(monthlyDataToDisplay!.daily_calories))"
            avgWeeklyLabel.text = "\(Int(monthlyDataToDisplay!.weekly_calories))"
            avgTotalLabel.text  = "\(Int(monthlyDataToDisplay!.total_calories))"
        }
    }
}

extension CalMonthlyViewController: FitnessView {
    func getYearly(data: [YearlyChartViewData]) {
        //
    }
    
    func getYearlyChartStats(data: YearlyStatsViewData) {
        //
    }
    
    
    func getWeekly(data: WeeklyStatsViewData) {
        //
    }
    
    private func getDate(data: MonthlyStatsViewData) {
        let dates = data.chartData.map { String($0.date) }
        
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        
        for date in dates {
            let dateG = df.date(from: date) ?? Date()
            self.dateArr.append(dateG)
        }
    }
    
    func getMonthlyStats(data: MonthlyStatsViewData) {
        self.emptyStateView.isHidden = true
        self.monthlyDataToDisplay = data
        self.setUpLabels()
        self.getDate(data: data)
        
        let values = data.chartData.map { Float($0.calories) ?? 0.0 }
        let dates = data.chartData.map { Int($0.date.dropFirst(8)) ?? 0 }
        
        setUpChart(yValue: values, date: self.dateArr)
    }
    
    func startLoading() {
        //
    }
    
    func finishLoading() {
        //
    }
    
    func setEmpty() {
        emptyStateView.isHidden = false
    }
    
    func getError(withMessage: String) {
        //
    }
}

extension CalMonthlyViewController {
    private func convert(monthNumber: Int) -> String {
        var monthStr = ""
        switch monthNumber {
        case 1:
            monthStr = "January"
        case 2:
            monthStr = "Febuary"
        case 3:
            monthStr = "March"
        case 4:
            monthStr = "April"
        case 5:
            monthStr = "May"
        case 6:
            monthStr = "June"
        case 7:
            monthStr = "July"
        case 8:
            monthStr = "August"
        case 9:
            monthStr = "September"
        case 10:
            monthStr = "October"
        case 11:
            monthStr = "November"
        case 12:
            monthStr = "December"
        default:
            break;
        }
        
        return NSLocalizedString(monthStr, comment: "")
    }
    
    func getMonth() {
        let strDate = Date().today(format: "yyyy.MM.dd")
        let date = strDate.components(separatedBy: ".")
        let month = date[1]
        let year = date[0]
        
        monthlyInt = Int(month) ?? 0
        yearlyInt = Int(year) ?? 0
        
        monthLabel.text = "\(convert(monthNumber: Int(month) ?? 0)) \(yearlyInt)"
        presenter.getMonthlyStats(date: "\(yearlyInt)-\(month)")
    }
}

//// MARK: - Observer Actions
//
//extension CalMonthlyViewController {
//
//    private func registerObs() {
//        NotificationCenter.default.removeObserver(self, name: .didNextMonthlyCalories, object: nil)
//        NotificationCenter.default.removeObserver(self, name: .didPreviousMonthlyCalories, object: nil)
//
//        NotificationCenter.default.addObserver(self, selector: #selector(didNextMonthlyCalories(_:)), name: .didNextMonthlyCalories, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(didPreviousMonthlyCalories(_:)), name: .didPreviousMonthlyCalories, object: nil)
//    }
//
//    @objc func didPreviousMonthlyCalories(_ notification: Notification) {
//        btnPrevious(self)
//    }
//
//    @objc func didNextMonthlyCalories(_ notification: Notification) {
//        btnNext(self)
//    }
//}
