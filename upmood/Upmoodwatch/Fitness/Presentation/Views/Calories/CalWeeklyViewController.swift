//
//  CalWeeklyViewController.swift
//  upmood
//
//  Created by John Paul Manoza on 04/11/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Charts
import UIKit

class CalWeeklyViewController: UIViewController {

    @IBOutlet weak var barChart: BarChartView!
    @IBOutlet weak var weeklyLabel: UILabel!
    @IBOutlet weak var weeklyDailyCaloriesLabel: UILabel!
    @IBOutlet weak var weeklyTotalCaloriesLabel: UILabel!
    @IBOutlet weak var emptyStateView: UIView!
    
    let presenter = FitnessPresenter(fitnessService: FitnessService())
    var weeklyToDisplay: WeeklyStatsViewData?
    let arrWeekDates = Date().getWeekDates()
    var previousMonday: Date!
    let dateFormat = "YYYY-MM-dd"
    var dateArr = [Date]()
    
    var boolOfDate = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        
        let thisMon = arrWeekDates.thisWeek.first!.toDate(format: dateFormat)
        let thisSun = arrWeekDates.thisWeek[arrWeekDates.thisWeek.count - 1].toDate(format: dateFormat)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.presenter.getWeeklyStats(date: "\(thisMon),\(thisSun)")
        }
        
        let newDateFormat = "MMM dd YYYY"
        weeklyLabel.text = "\(arrWeekDates.thisWeek.first!.toDate(format: newDateFormat)) - \(arrWeekDates.thisWeek[arrWeekDates.thisWeek.count - 1].toDate(format: newDateFormat))"
    }

    @IBAction func nextWeek(_ sender: UIButton) {
        dateArr.removeAll()
        viewDidLoad()
        self.previousMonday = Date().previous(.monday)
        boolOfDate = false
    }
    
    @IBAction func previousWeek(_ sender: UIButton) {
        dateArr.removeAll()
        if previousMonday != nil {
            print("hindi empty prev monday")
            
            let currentMonday = self.previousMonday!
            let previousMonday = currentMonday.getPreviousWeekStartDay()!.toDate(format: "YYYY-MM-dd")
            let previousSunday = currentMonday.getPreviousWeekEndDay()!.toDate(format: "YYYY-MM-dd")
            presenter.getWeeklyStats(date: "\(previousMonday),\(previousSunday)")
            
            let newDateFormat = "MMM dd YYYY"
            weeklyLabel.text = "\(currentMonday.getPreviousWeekStartDay()!.toDate(format: newDateFormat)) - \(currentMonday.getPreviousWeekEndDay()!.toDate(format: newDateFormat))"
            self.previousMonday = currentMonday.getPreviousWeekStartDay()!
            
        } else {
            print("empty prev monday")
            let currentMonday = Date().previous(.monday)
            let previousMonday = currentMonday.getPreviousWeekStartDay()!.toDate(format: "YYYY-MM-dd")
            let previousSunday = currentMonday.getPreviousWeekEndDay()!.toDate(format: "YYYY-MM-dd")
            
            presenter.getWeeklyStats(date: "\(previousMonday),\(previousSunday)")
            
            let newDateFormat = "MMM dd YYYY"
            weeklyLabel.text = "\(currentMonday.getPreviousWeekStartDay()!.toDate(format: newDateFormat)) - \(currentMonday.getPreviousWeekEndDay()!.toDate(format: newDateFormat))"
            self.previousMonday = currentMonday.getPreviousWeekStartDay()!
        }
         boolOfDate = false
    }
    
    private func setUpChart(yValue : [Float], date: [Date]) {
       
       barChart.isUserInteractionEnabled = false
       barChart.leftAxis.enabled = true
       barChart.rightAxis.enabled = false
       barChart.xAxis.enabled = true
       barChart.legend.enabled = false
       barChart.xAxis.labelPosition = XAxis.LabelPosition.bottom
       barChart.xAxis.granularityEnabled = true
       
       self.barChart.xAxis.valueFormatter = DefaultAxisValueFormatter(block: {(index, _) in
           let df = DateFormatter()
           df.dateFormat = "EE"
           let strings = date.map{df.string(from: $0)}
           return strings[Int(index)]
       })
       
       var dataEntries: [BarChartDataEntry] = []
       for i in 0..<yValue.count {
         let dataEntry = BarChartDataEntry(x: Double(i), y: Double(yValue[i]))
         dataEntries.append(dataEntry)
       }
       
       let chartDataSet = BarChartDataSet(entries: dataEntries)
       chartDataSet.drawValuesEnabled = false
       chartDataSet.colors = [hexStringToUIColor(hex: "#3BB5EA")]
       
       let chartData = BarChartData(dataSet: chartDataSet)
       chartData.barWidth = Double(0.50)

       barChart.data = chartData
   }
    
    private func setUpLabels() {
       
        if weeklyToDisplay != nil {
            weeklyDailyCaloriesLabel.text = "\(Int(weeklyToDisplay!.daily_calories))"
            
        }
    }
}

extension CalWeeklyViewController: FitnessView {
    func getYearly(data: [YearlyChartViewData]) {
        //
    }
    
    func getYearlyChartStats(data: YearlyStatsViewData) {
        //
    }
    
    func startLoading() {
        //
    }
    
    func finishLoading() {
        //
    }
    
    func getMonthlyStats(data: MonthlyStatsViewData) {
        //
    }
    
    private func getDate(data: WeeklyStatsViewData) {
        let dates = data.chartData.map { String($0.date) }
        
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        
        for date in dates {
            let dateG = df.date(from: date) ?? Date()
            self.dateArr.append(dateG)
        }
    }
    
    func getWeekly(data: WeeklyStatsViewData) {
        self.emptyStateView.isHidden = true
        
        self.weeklyToDisplay = data
        self.setUpLabels()
        self.getDate(data: data)
        
        let values = data.chartData.map { Float($0.calories) ?? 0.0 }
        self.setUpChart(yValue: values, date: self.dateArr)
        
        weeklyTotalCaloriesLabel.text = "\(values.last ?? 0.0)"
        
        if boolOfDate == true{
            UserDefaults.standard.set(values.last, forKey: "profileCalories")
   
        }
        
    }
        
    func setEmpty() {
        emptyStateView.isHidden = false
        weeklyDailyCaloriesLabel.text = "0"
        weeklyTotalCaloriesLabel.text = "0.0"
        UserDefaults.standard.set("0.0", forKey: "profileCalories")
 
    }
    
    func getError(withMessage: String) {
        //
    }
}

extension Date {

    func getWeekDates() -> (thisWeek:[Date],nextWeek:[Date]) {
        var tuple: (thisWeek:[Date],nextWeek:[Date])
        var arrThisWeek: [Date] = []
        for i in 0..<7 {
            arrThisWeek.append(Calendar.current.date(byAdding: .day, value: i, to: startOfWeek)!)
        }
        var arrNextWeek: [Date] = []
        for i in 1...7 {
            arrNextWeek.append(Calendar.current.date(byAdding: .day, value: i, to: arrThisWeek.last!)!)
        }
        tuple = (thisWeek: arrThisWeek,nextWeek: arrNextWeek)
        return tuple
    }

    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }

    var startOfWeek: Date {
        let gregorian = Calendar(identifier: .gregorian)
        let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
        return gregorian.date(byAdding: .day, value: 1, to: sunday!)!
    }

    func toDate(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}


extension Date {
    
    func getPreviousWeekStartDay() -> Date? {
        let gregorian = Calendar(identifier: .gregorian)
        let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
        return gregorian.date(byAdding: .day, value: -6, to: sunday!)!
    }

    // Getting last day of previous week
    func getPreviousWeekEndDay() -> Date? {
        let gregorian = Calendar(identifier: .gregorian)
        let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
        return gregorian.date(byAdding: .day, value: 0, to: sunday!)!
    }
    
    static func today() -> Date {
        return Date()
    }
    
    func next(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.next,
                   weekday,
                   considerToday: considerToday)
    }
    
    func previous(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.previous,
                   weekday,
                   considerToday: considerToday)
    }
    
    func get(_ direction: SearchDirection,
             _ weekDay: Weekday,
             considerToday consider: Bool = false) -> Date {
        
        let dayName = weekDay.rawValue
        
        let weekdaysName = getWeekDaysInEnglish().map { $0.lowercased() }
        
        assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")
        
        let searchWeekdayIndex = weekdaysName.firstIndex(of: dayName)! + 1
        
        let calendar = Calendar(identifier: .gregorian)
        
        if consider && calendar.component(.weekday, from: self) == searchWeekdayIndex {
            return self
        }
        
        var nextDateComponent = calendar.dateComponents([.hour, .minute, .second], from: self)
        nextDateComponent.weekday = searchWeekdayIndex
        
        let date = calendar.nextDate(after: self,
                                     matching: nextDateComponent,
                                     matchingPolicy: .nextTime,
                                     direction: direction.calendarSearchDirection)
        
        return date!
    }
}

// MARK: Helper methods
extension Date {
    func getWeekDaysInEnglish() -> [String] {
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale(identifier: "en_US_POSIX")
        return calendar.weekdaySymbols
    }
    
    enum Weekday: String {
        case monday, tuesday, wednesday, thursday, friday, saturday, sunday
    }
    
    enum SearchDirection {
        case next
        case previous
        
        var calendarSearchDirection: Calendar.SearchDirection {
            switch self {
            case .next:
                return .forward
            case .previous:
                return .backward
            }
        }
    }
}
