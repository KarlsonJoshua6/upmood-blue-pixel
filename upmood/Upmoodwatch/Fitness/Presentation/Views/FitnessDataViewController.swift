//
//  FitnessDataViewController.swift
//  upmood
//
//  Created by Taison Digital on 15/09/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import UIKit

class FitnessDataViewController: UIViewController {
        // view
    @IBOutlet weak var sleepStatsView: UIView!
    @IBOutlet weak var caloriesStatsView: UIView!
   
    @IBOutlet weak var caloriesStatsContainer: UIView!
    
    @IBOutlet weak var stepStatsView: UIView!
    
    // end of container
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var segmentControl1: UISegmentedControl!
    
    @IBOutlet weak var syncView: UIView!
   
    let viewControllerIdentifiers1 = ["caloriesWeekly", "caloriesMonthly", "caloriesYear"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getInit()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let stringF = UserDefaults.standard.string(forKey: "stringFV") ?? ""
        
        print("SYNC DATA: --------------------------------------------------------------------------- \(stringF)")
        
        if !stringF.isEmpty {
            addSteps()
        }
    }
    
    func getInit(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getCalStepsCommand"), object: nil)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func segmentChange(_ sender: UISegmentedControl) {
       selectedSegmentIndexTo(index: sender.selectedSegmentIndex)
       switch sender.selectedSegmentIndex {
            case 0:
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification0"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification00"), object: nil)
            case 1:
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification1"), object: nil)
                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification11"), object: nil)
            case 2:
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification2"), object: nil)
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification22"), object: nil)
            default:
                break
            }
    }

    @IBAction func segmentChangeStep(_ sender: UISegmentedControl) {
        selectedSegmentIndexTo(index: sender.selectedSegmentIndex)
        switch sender.selectedSegmentIndex {
        case 0:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification0"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification00"), object: nil)
        case 1:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification1"), object: nil)
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification11"), object: nil)
        case 2:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification2"), object: nil)
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notification22"), object: nil)
        default:
            break
        }
      
    }
    
    func selectedSegmentIndexTo(index: Int){
        switch index {
        case 0:
            segIndex(seg: index)
        case 1:
            segIndex(seg: index)
        case 2:
            segIndex(seg: index)
        default:
            break
        }
    }
    
    func segIndex(seg: Int){
        segmentControl.selectedSegmentIndex = seg
        segmentControl1.selectedSegmentIndex = seg
    }
    
 
    private func addSteps(){
        let stringF = UserDefaults.standard.string(forKey: "stringFV") ?? ""
        let loversUserToken = UserDefaults.standard.string(forKey: "fetchUserToken") ?? ""
        let header = ["Accept": "application/json", "Authorization": "Bearer \(loversUserToken)"]
        let params = ["data": stringF]

        Alamofire.request(URL(string: "\(Constants.Routes.BASE)/api/v4/ios/statistics/add-statistics")!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { response in
          switch response.result {
            case .success:
                print("check - params", params)
                UserDefaults.standard.removeObject(forKey: "stringFV")
                break
            case .failure(let error):
                print(error)
            }
        }
    }
}
