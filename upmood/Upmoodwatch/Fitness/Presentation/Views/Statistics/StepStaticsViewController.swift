//
//  StepStaticsViewController.swift
//  upmood
//
//  Created by Taison Digital on 16/09/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class StepStaticsViewController: UIViewController {
    
    let viewControllerIdentifiers = ["stepWeekly", "stepMonthly", "stepYear"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpSegmentControl(indexOf: 0)
        
       NotificationCenter.default.addObserver(self, selector: #selector(self.segment1), name: NSNotification.Name(rawValue: "notification0"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.segment2), name: NSNotification.Name(rawValue: "notification1"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.segment3), name: NSNotification.Name(rawValue: "notification2"), object: nil)
    }
    // handle notification
    @objc func segment1(_ notification: NSNotification) {
        setUpSegmentControl(indexOf: 0)
    }
    // handle notification
    @objc func segment2(_ notification: NSNotification) {
        setUpSegmentControl(indexOf: 1)
    }
    // handle notification
    @objc func segment3(_ notification: NSNotification) {
        setUpSegmentControl(indexOf: 2)
    }
    
    private func setUpSegmentControl(indexOf: Int) {
        let newController = storyboard!.instantiateViewController(withIdentifier: viewControllerIdentifiers[indexOf])
        let oldController = childViewControllers.last!
        
        oldController.willMove(toParentViewController: nil)
        addChildViewController(newController)
        newController.view.frame = oldController.view.frame
        
        transition(from: oldController, to: newController, duration: 0.25, options: .transitionCrossDissolve, animations: {
            // nothing needed here
        }, completion: { _ -> Void in
            oldController.removeFromParentViewController()
            newController.didMove(toParentViewController: self)
        })
    }

   

}
