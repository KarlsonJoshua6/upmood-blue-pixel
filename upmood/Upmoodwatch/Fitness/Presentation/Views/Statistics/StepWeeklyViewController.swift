//
//  StepWeeklyViewController.swift
//  upmood
//
//  Created by Taison Digital on 15/09/2020.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Charts
import UIKit

class StepWeeklyViewController: UIViewController {
    
    @IBOutlet weak var barChart: BarChartView!
    @IBOutlet weak var weekLabel: UILabel!
    @IBOutlet weak var avgDailyStepLabel: UILabel!
    @IBOutlet weak var totalStepLabel: UILabel!
    @IBOutlet weak var emptyStateView: UIView!
    
    let presenter = FitnessPresenter(fitnessService: FitnessService())
    var weeklyToDisplay: WeeklyStatsViewData?
    let arrWeekDates = Date().getWeekDates()
    var previousMonday: Date!
    let dateFormat = "YYYY-MM-dd"
    var dateArr = [Date]()
    
    var boolOfDate = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        let thisMon = arrWeekDates.thisWeek.first!.toDate(format: dateFormat)
        let thisSun = arrWeekDates.thisWeek[arrWeekDates.thisWeek.count - 1].toDate(format: dateFormat)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.presenter.getWeeklyStats(date: "\(thisMon),\(thisSun)")
        }
        
        let newDateFormat = "MMM dd YYYY"
        weekLabel.text = "\(arrWeekDates.thisWeek.first!.toDate(format: newDateFormat)) - \(arrWeekDates.thisWeek[arrWeekDates.thisWeek.count - 1].toDate(format: newDateFormat))"
    }
    
    @IBAction func nextWeek(_ sender: UIButton) {
        dateArr.removeAll()
        viewDidLoad()
        self.previousMonday = Date().previous(.monday)
        boolOfDate = false
    }
    
    @IBAction func previousWeek(_ sender: UIButton) {
        dateArr.removeAll()
        if previousMonday != nil {
            print("hindi empty prev monday")
            
            let currentMonday = self.previousMonday!
            let previousMonday = currentMonday.getPreviousWeekStartDay()!.toDate(format: "YYYY-MM-dd")
            let previousSunday = currentMonday.getPreviousWeekEndDay()!.toDate(format: "YYYY-MM-dd")
            presenter.getWeeklyStats(date: "\(previousMonday),\(previousSunday)")
            
            let newDateFormat = "MMM dd YYYY"
            weekLabel.text = "\(currentMonday.getPreviousWeekStartDay()!.toDate(format: newDateFormat)) - \(currentMonday.getPreviousWeekEndDay()!.toDate(format: newDateFormat))"
            self.previousMonday = currentMonday.getPreviousWeekStartDay()!
            
        } else {
            print("empty prev monday")
            let currentMonday = Date().previous(.monday)
            let previousMonday = currentMonday.getPreviousWeekStartDay()!.toDate(format: "YYYY-MM-dd")
            let previousSunday = currentMonday.getPreviousWeekEndDay()!.toDate(format: "YYYY-MM-dd")
            
            presenter.getWeeklyStats(date: "\(previousMonday),\(previousSunday)")
            
            let newDateFormat = "MMM dd YYYY"
            weekLabel.text = "\(currentMonday.getPreviousWeekStartDay()!.toDate(format: newDateFormat)) - \(currentMonday.getPreviousWeekEndDay()!.toDate(format: newDateFormat))"
            self.previousMonday = currentMonday.getPreviousWeekStartDay()!
        }
        boolOfDate = false
    }
    
    private func setUpChart(yValue : [Int], date: [Date]) {
        
        barChart.isUserInteractionEnabled = false
        barChart.leftAxis.enabled = true
        barChart.rightAxis.enabled = false
        barChart.xAxis.enabled = true
        barChart.legend.enabled = false
        barChart.xAxis.labelPosition = XAxis.LabelPosition.bottom
        barChart.xAxis.granularityEnabled = true
        
        let formatter: CustomIntFormatter = CustomIntFormatter()
        barChart.data?.setValueFormatter(formatter)
        
        self.barChart.xAxis.valueFormatter = DefaultAxisValueFormatter(block: {(index, _) in
            
            let df = DateFormatter()
            df.dateFormat = "EE"
            let strings = date.map{df.string(from: $0)}
            
            return strings[Int(index)]
        })
        
        var dataEntries: [BarChartDataEntry] = []
        for i in 0..<yValue.count {
          let dataEntry = BarChartDataEntry(x: Double(i), y: Double(yValue[i]))
          dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(entries: dataEntries)
        chartDataSet.drawValuesEnabled = false
        chartDataSet.colors = [hexStringToUIColor(hex: "#3BB5EA")]
        
        let chartData = BarChartData(dataSet: chartDataSet)
        chartData.barWidth = Double(0.50)
 
        barChart.data = chartData
    }
    
    private func setUpLabels() {
        
        if weeklyToDisplay != nil {
            avgDailyStepLabel.text = "\(Int(weeklyToDisplay!.daily_steps))"
            
        }
    }
}

class CustomIntFormatter: NSObject, IValueFormatter{
    public func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        let correctValue = Int(value)
        print("correctValue: \(correctValue)")
        return String(correctValue)
    }
}

extension StepWeeklyViewController: FitnessView {
    func getYearly(data: [YearlyChartViewData]) {
        //
    }
    
    func getYearlyChartStats(data: YearlyStatsViewData) {
        //
    }
    
    func getMonthlyStats(data: MonthlyStatsViewData) {
           //
    }
    
    func startLoading() {
        //
    }
    
    func finishLoading() {
        //
    }
    
    private func getDate(data: WeeklyStatsViewData) {
        let dates = data.chartData.map { String($0.date) }
        
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        
        for date in dates {
            let dateG = df.date(from: date) ?? Date()
            self.dateArr.append(dateG)
        }
    }
  
    func getWeekly(data: WeeklyStatsViewData) {
        self.emptyStateView.isHidden = true
        self.weeklyToDisplay = data
        self.setUpLabels()
        self.getDate(data: data)
        
        let values = data.chartData.map { Int($0.steps) ?? 0 }
      
        totalStepLabel.text = "\(values.last ?? 0)"
        
        self.setUpChart(yValue: values, date: self.dateArr)
        if boolOfDate == true{
            UserDefaults.standard.set(values.last, forKey: "profileSteps")
        }
    }
    
    func setEmpty() {
        emptyStateView.isHidden = false
        avgDailyStepLabel.text = "0"
        totalStepLabel.text = "0"
        UserDefaults.standard.set("0", forKey: "profileSteps")
    }
    
    func getError(withMessage: String) {
        //
    }
}


extension Calendar {
    static let iso8601 = Calendar(identifier: .iso8601)
}
extension Date {
    var currentWeekMonday: Date {
        return Calendar.iso8601.date(from: Calendar.iso8601.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
    }
    var currentWeekdays: [Date] {
        return (0...6).compactMap{ Calendar.iso8601.date(byAdding: DateComponents(day: $0), to: currentWeekMonday) }
    }
}

