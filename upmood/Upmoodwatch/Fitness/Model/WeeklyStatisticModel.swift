//
//  WeeklyStatisticModel.swift
//  upmood
//
//  Created by Taison Digital on 10/29/20.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import SwiftyJSON

class WeeklyStatisticModel {
    
    var daily_calories: Float?
    var daily_steps: Float?
    var total_calories: Float?
    var total_steps: Int?
    var chartData: [ChartData]?
  
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: JSON]) {
        if let data = dict["daily_calories"]?.floatValue {
            self.daily_calories = data
        }
        if let data = dict["daily_steps"]?.floatValue {
            self.daily_steps = data
        }
        if let data = dict["total_calories"]?.floatValue {
            self.total_calories = data
        }
        if let data = dict["total_steps"]?.intValue {
            self.total_steps = data
        }
        if let data = dict["chart"]?.array {
            self.chartData = data.map { ChartData.build($0.dictionary!) }
        }
    }
    
    // MARK: Class Method
    class func build(_ dict: [String: JSON]) -> WeeklyStatisticModel {
        let data = WeeklyStatisticModel()
        data.loadFromDictionary(dict)
        return data
    }
}

class ChartData {
    
    var id: Int?
    var user_id: Int?
    var date: String?
    var steps: String?
    var calories: String?
    
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: JSON]) {
        if let data = dict["id"]?.intValue {
            self.id = data
        }
        
        if let data = dict["user_id"]?.intValue {
            self.user_id = data
        }
        
        if let data = dict["date"]?.stringValue {
            self.date = data
        }
        
        if let data = dict["steps"]?.stringValue {
            self.steps = data
        }
        
        if let data = dict["calories"]?.stringValue {
            self.calories = data
        }
    }
    
    // MARK: Class Method
    class func build(_ dict: [String: JSON]) -> ChartData {
        let data = ChartData()
        data.loadFromDictionary(dict)
        return data
    }
}
