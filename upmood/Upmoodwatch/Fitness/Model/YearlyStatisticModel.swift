//
//  YearlyStatisticModel.swift
//  upmood
//
//  Created by Taison Digital on 10/29/20.
//  Copyright © 2020 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import SwiftyJSON

class YearlyStatisticModel {
    var daily_calories: Float?
    var daily_steps: Float?
    var monthly_calories: Float?
    var monthly_steps: Float?
    var total_calories: Int?
    var total_steps: Int?
    
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: JSON]) {
        if let data = dict["daily_calories"]?.floatValue {
            self.daily_calories = data
        }
        if let data = dict["daily_steps"]?.floatValue {
            self.daily_steps = data
        }
        if let data = dict["monthly_calories"]?.floatValue {
            self.monthly_calories = data
        }
        if let data = dict["monthly_steps"]?.floatValue {
            self.monthly_steps = data
        }
        if let data = dict["total_calories"]?.intValue {
            self.total_calories = data
        }
        if let data = dict["total_steps"]?.intValue {
            self.total_steps = data
        }
    }
      
    // MARK: Class Method
    class func build(_ dict: [String: JSON]) -> YearlyStatisticModel {
        let data = YearlyStatisticModel()
        data.loadFromDictionary(dict)
        return data
    }
    
}

class YearlyChartModel {
    
    var id: Int?
    var user_id: Int?
    var date: String?
    var steps: Int?
    var calories: Float?
    
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: AnyObject]) {
        if let data = dict["id"] as? Int {
            self.id = data
        }
            
        if let data = dict["user_id"] as? Int {
            self.user_id = data
        }
            
        if let data = dict["date"] as? String {
            self.date = data
        }
            
        if let data = dict["steps"] as? Int {
            self.steps = data
        }
            
        if let data = dict["calories"] as? Float  {
            self.calories = data
        }
    }
        
    // MARK: Class Method
    class func build(_ dict: [String: AnyObject]) -> YearlyChartModel {
        let data = YearlyChartModel()
        data.loadFromDictionary(dict)
        return data
    }
}
