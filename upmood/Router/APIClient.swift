//
//  RouterAPI.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 07/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import Foundation

enum APIClient: URLRequestConvertible {
    
    case signUp(email: String, name: String, password: String, confirmPassword: String)
    case login(email: String, password: String)
    case loginWithFacebook(facebook_id: Int, email: String, name: String, image: String)
    case forgotPassword(email: String)
    case checkEmail(email: String)
    case sendValidationEmail(email: String, id: Int, token: String)
    case fetchMonthlyMood(year: Int, month: String)
    case fetchReactionHistory(id: Int, userId: Int)
    case updateProfile(name: String)
    case updateProfileUser(name: String)
    case udpateProfileUser(birthday: String)
    case updateProfileWeight(weight: String)
    case updateProfileCountry(place: String)
    case updateProfileGender(gender: String)
    case updateProfileEmail(email: String)
    case updateUserPassword(oldPassword: String, newPassword: String, confirmPassword: String)
    case linkAccountToFacebook(facebookId: Int, email: String)
    case forceLinkAccountToFacebook(facebookId: Int, email: String)
    case unlinkAccountToFacebook()
    case deactivateAccount()
    case add(status: String)
    case checkUser(name: String)
    case friendList(keyword: String)
    case pendingList()
    case approvePendingRequestWith(friendId: Int)
    case denyPendingrequestWith(FriendId: Int)
    case viewFriendProfile(friendId: Int)
    case viewFriendReactionHistory(id: Int)
    case searchOnFriendList(withKeyword: String)
    case addFriend(withId: Int)
    case cancelFriendRequest(withFriendId: Int)
    case loadStickerMenu()
    case sendReaction(id: Int, post_id: Int, reaction_resource_id: Int, record_id: Int)
    case loadAllStickersAndEmojis()
    case download(sticker: String)
    case setTheme(sticker: String)
    case deleteSticker(withSet: String)
    case loadGroupList()
    case loadSpecificGroupList(withId: Int)
    case deleteGroup(withId: Int)
    case addNewGroup(withGroupName: String, my_mood: Int, emotion: Int, heartbeat: Int, emotions: String, members: String, notificationType: String)
    case removeFriend(withId: Int)
    case editGroup(withGroupId: Int, type: String, value: String, type_data: String, emotions: String, heartbeat: Int, stress_level: Int, emotion: Int, my_mood: Int)
    case addFriendToGroup(members: String, group_id: Int)
    case notificationList()
    case sendFcmToken(withDeviceId: String, fcmToken: String, device_type: String)
    case loadFriendAvailableOnGroup(withKeyword: String, groupId: Int)
    case removeFriendToGroup(withId: Int)
    case updateGroupPrivacy(withGroupId: Int, type: String, heartbeat: Int, stress_level: Int, emotion: Int, my_mood: Int)
    case updateManagePrivacy(privacy: Int, emotion: Int, heartbeat: Int, stress_level: Int, notification_type: String, auto_reconnect: Int, auto_renew: Int)
    case storedRecord(heartbeat_count: Int, stress_level: String, type: String, emotion_set: String, emotion_value: String, emotion_level: Double, longitude: Int, latitude: Int, ppi: String, total_ppi: Int, created_at: String, timezone: String, old_emotion: String, stress_level_value: Double)
    case loadDownloadedTheme()
    case searchSticker(keyword: String)
    case sendDataOffline(data: String)
    case sendFeedback(email: String, type: String, content: String)
    case updateUserProfile(userStatus: Int)
    case stickerList()
    case storeProgressValue(userId: Int, progress_value: Int, bpm: Int)
    case logOut()
    
    // MARK: Stored
    
    static let baseURLString = "http://18.139.163.192:8080/api/v4/ios"
    
    // MARK: Computed
    
    private var path: String {
        switch self {
        case .signUp:
            return "/authenticate/signup"
        case .login:
            return "/authenticate/local"
        case .loginWithFacebook:
            return "/authenticate/facebook"
        case .forgotPassword:
            return "/authenticate/forgotPassword"
        case .checkEmail:
            return "/authenticate/checkEmail"
        case .sendValidationEmail:
            return "/authenticate/sendValidationEmail"
        case .fetchMonthlyMood(let year, let month):
            return "/user/record?sort=monthly&date=\(year)-\(month)"
        case .fetchReactionHistory:
            return "/user/reaction/history"
        case .updateProfile:
            return "/user/profile/updateProfile"
        case .updateProfileWeight:
            return "/user/profile/updateProfile"
        case .updateProfileUser:
            return "/user/profile/updateProfile"
        case .udpateProfileUser:
            return "/user/profile/updateProfile"
        case .updateProfileCountry:
            return "/user/profile/updateProfile"
        case .updateProfileGender:
            return "/user/profile/updateProfile"
        case .updateProfileEmail:
            return "/user/profile/updateProfile"
        case .updateUserPassword:
            return "/user/profile/changePassword"
        case .linkAccountToFacebook:
            return "/user/linkAccount/facebook"
        case .forceLinkAccountToFacebook:
            return "/user/linkAccount/fbForceLink"
        case .unlinkAccountToFacebook:
            return "/user/linkAccount/unlinkFb"
        case .deactivateAccount:
            return "/user/profile/deactivateAccount"
        case .add:
            return "/user/post"
        case .checkUser:
            return "/user/profile/usernameCheck"
        case .friendList:
            return "/user/connection"
        case .pendingList:
            return "/user/notification/connect"
        case .approvePendingRequestWith:
            return "/user/connection/accept"
        case .denyPendingrequestWith:
            return "/user/connection/disconnect"
        case .viewFriendProfile(let friendId):
            return "/user/profile/\(friendId)"
        case .viewFriendReactionHistory(let id):
            return "/user/reaction/\(id)"
        case .searchOnFriendList:
            return "/user/search"
        case .addFriend:
            return "/user/connection/connect"
        case .cancelFriendRequest:
            return "/user/connection/disconnect"
        case .loadStickerMenu:
            return "/user/resources/owned"
        case .sendReaction:
            return "/user/reaction"
        case .loadAllStickersAndEmojis:
            return "/user/resources/all"
        case .download:
            return "/user/resources/add-to-set"
        case .setTheme(_):
            return "/user/profile/basicEmoji"
        case .deleteSticker:
            return "/user/resources/remove-to-set"
        case .loadGroupList:
            return "/user/group"
        case .loadSpecificGroupList(let withId):
            return "/user/group/\(withId)"
        case .deleteGroup:
            return "/user/group/remove"
        case .addNewGroup:
            return "/user/group"
        case .removeFriend(_):
            return "/user/connection/disconnect"
        case .editGroup:
            return "/user/group/updateGroup"
        case .addFriendToGroup:
            return "/user/group/addToGroup"
        case .notificationList:
            return "/user/notification"
        case .sendFcmToken:
            return "/user/devices"
        case .loadFriendAvailableOnGroup:
            return "/user/group/search"
        case .removeFriendToGroup:
            return "/user/group/removeToGroup"
        case .updateGroupPrivacy:
            return "/user/group/updateGroup"
        case .updateManagePrivacy:
            return "/user/profile/privacy"
        case .storedRecord:
            return "/user/record"
        case .loadDownloadedTheme:
            return "/user/resources/owned"
        case .searchSticker(let keyword):
            return "/user/resources/all?set=\(keyword)"
        case .sendDataOffline:
            return "/user/record/batchStore"
        case .sendFeedback:
            return "/user/messages"
        case .updateUserProfile:
            return "/user/profile/status"
        case .stickerList:
            return "/user/resources/setname-list"
        case .storeProgressValue(let userId, let progress_value, let bpm):
            return "/insight/user/session/progress-value?session_user_id=\(userId)&progress_value=\(progress_value)&bpm=\(bpm)"
        case .logOut:
            return "/user/profile/logout"
        }
    }
    
    private var method: HTTPMethod {
        switch self {
        case .signUp: return .post
        case .login: return .post
        case .loginWithFacebook: return .post
        case .forgotPassword: return .post
        case .checkEmail: return .post
        case .sendValidationEmail: return .post
        case .fetchMonthlyMood: return .get
        case .fetchReactionHistory: return .post
        case .updateProfile: return .post
        case .updateProfileUser: return .post
        case .updateProfileWeight: return .post
        case .udpateProfileUser: return .post
        case .updateProfileCountry: return .post
        case .updateProfileGender: return .post
        case .updateUserPassword: return .post
        case .updateProfileEmail: return .post
        case .linkAccountToFacebook: return .post
        case .forceLinkAccountToFacebook: return .post
        case .unlinkAccountToFacebook: return .post
        case .deactivateAccount: return .post
        case .add: return .post
        case .checkUser: return .post
        case .friendList: return .post
        case .pendingList: return .get
        case .approvePendingRequestWith: return .post
        case .denyPendingrequestWith: return .post
        case .viewFriendProfile: return .get
        case .viewFriendReactionHistory: return .get
        case .searchOnFriendList: return .post
        case .addFriend: return .post
        case .cancelFriendRequest: return .post
        case .loadStickerMenu: return .get
        case .sendReaction: return .post
        case .loadAllStickersAndEmojis: return .get
        case .download: return .post
        case .setTheme: return .post
        case .deleteSticker: return .post
        case .loadGroupList: return .get
        case .loadSpecificGroupList: return .get
        case .deleteGroup: return .post
        case .addNewGroup: return .post
        case .removeFriend: return .post
        case .editGroup: return .post
        case .addFriendToGroup: return .post
        case .notificationList: return .get
        case .sendFcmToken: return .post
        case .loadFriendAvailableOnGroup: return .post
        case .removeFriendToGroup: return .post
        case .updateGroupPrivacy: return .post
        case .updateManagePrivacy: return .post
        case .storedRecord: return .post
        case .loadDownloadedTheme: return .get
        case .searchSticker: return .get
        case .sendDataOffline: return .post
        case .sendFeedback: return .post
        case .updateUserProfile: return .post
        case .stickerList: return .get
        case .storeProgressValue: return .get
        case .logOut: return .post
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try APIClient.baseURLString.asURL()
        let adminToken = "UpmoodAPI-a0XbCZeTxi1zW9sU5Y2GoQf1M0G55m3JNPrHNH96JSJNpj2SOwaMUggW5V9U"
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        let token = KeychainSwift().get("apiToken") ?? ""
        
        switch self {
        case .signUp(let email, let name, let password, let confirmPassword):
            let parameters: [String: Any] = ["email": email, "name": name, "password": password, "password_confirmation": confirmPassword]
            urlRequest.setValue("Bearer \(adminToken)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .login(let email, let password):
            let parameters: [String: Any] = ["username": email, "password": password]
            urlRequest.setValue("Bearer \(adminToken)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .loginWithFacebook(let facebook_id, let email, let name, let image):
            let parameters: [String: Any] = ["facebook_id": facebook_id, "email": email, "name": name, image: "image"]
            urlRequest.setValue("Bearer \(adminToken)", forHTTPHeaderField: "Authorization")
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
            break
        case .forgotPassword(let email):
            let parameters: [String: Any] = ["email": email]
            urlRequest.setValue("Bearer \(adminToken)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .checkEmail(let email):
            let parameters: [String: Any] = ["email": email]
            urlRequest.setValue("Bearer \(adminToken)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .sendValidationEmail(let email, let id, let token):
            let parameters: [String: Any] = ["email": email, "id": id, "api_token": token, "facebook_id": 0]
            urlRequest.setValue("Bearer \(adminToken)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .fetchMonthlyMood(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
            break
        case .fetchReactionHistory(let id, let userId):
            let parameters: [String: Any] = ["id": id, "user_id": userId]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .updateProfile(let name):
            let parameters: [String: Any] = ["type": "name", "value": name]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .updateProfileUser(let name):
            let parameters: [String: Any] = ["type": "username", "value": name]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .updateProfileWeight(let weight):
            let parameters: [String: Any] = ["type": "weight", "value": weight]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .udpateProfileUser(let birthday):
            let parameters: [String: Any] = ["type": "birthday", "value": birthday]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .updateProfileCountry(let place):
            let parameters: [String: Any] = ["type": "country", "value": place]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .updateProfileGender(let gender):
            let parameters: [String: Any] = ["type": "gender", "value": gender]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .updateUserPassword(let oldPassword, let newPassword, let confirmPassword):
            let parameters: [String: Any] = ["oldpassword": oldPassword, "password": newPassword, "password_confirmation": confirmPassword]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .updateProfileEmail(let email):
            let parameters: [String: Any] = ["type": "email", "value": email]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .linkAccountToFacebook(let facebookId, let email):
            let parameters: [String: Any] = ["facebook_id": facebookId, "email": email]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .forceLinkAccountToFacebook(let facebookId, let email):
            let parameters: [String: Any] = ["facebook_id": facebookId, "email": email]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .unlinkAccountToFacebook(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
            break
        case .deactivateAccount(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
            break
        case .add(let status):
            let parameters: [String: Any] = ["content": status]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .checkUser(let name):
            let parameters: [String: Any] = ["keyword": name]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .friendList(let keyword):
            let parameters: [String: Any] = ["keyword": keyword]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .pendingList(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
            break
        case .approvePendingRequestWith(let friendId):
            let parameters: [String: Any] = ["id": friendId]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .denyPendingrequestWith(let FriendId):
            let parameters: [String: Any] = ["id": FriendId]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .viewFriendProfile(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
            break
        case .viewFriendReactionHistory(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
            break
        case .searchOnFriendList(let withKeyword):
            let parameters: [String: Any] = ["keyword": withKeyword]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .addFriend(let withId):
            let parameters: [String: Any] = ["id": withId]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .cancelFriendRequest(let withFriendId):
            let parameters: [String: Any] = ["id": withFriendId]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .loadStickerMenu(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
            break
        case .sendReaction(let id, let post_id, let reaction_resource_id, let record_id):
            let parameters: [String: Any] = ["id": id, "post_id": post_id, "reaction_resource_id": reaction_resource_id, "record_id": record_id]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .loadAllStickersAndEmojis(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
            break
        case .download(let sticker):
            let parameters: [String: Any] = ["set_name": sticker]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .setTheme(let sticker):
            let parameters: [String: Any] = ["set_name": sticker]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .deleteSticker(let withSet):
            let parameters: [String: Any] = ["set_name": withSet]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .loadGroupList(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
            break
        case .loadSpecificGroupList(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
            break
        case .deleteGroup(let withId):
            let parameters: [String: Any] = ["id": withId]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .addNewGroup(let withGroupName, let my_mood, let emotion, let heartbeat, let emotions, let members, let notificationType):
            let parameters: [String: Any] = ["name":withGroupName, "my_mood": my_mood, "emotion": emotion, "heartbeat": heartbeat, "emotions": emotions, "members": members, "notification_type": notificationType]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .removeFriend(let withId):
            let parameters: [String: Any] = ["id":withId]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .editGroup(let withGroupId, let type, let value, let type_data, let emotions, let heartbeat, let stress_level, let emotion, let my_mood):
            let parameters: [String: Any] = ["id": withGroupId, "type": type, "value": value, "type_data": type_data, "emotions": emotions, "heartbeat": heartbeat, "stress_level": stress_level, "emotion": emotion, "my_mood": my_mood]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .addFriendToGroup(let members, let group_id):
            let parameters: [String: Any] = ["members": members, "group_id": group_id]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .notificationList(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
            break
        case .sendFcmToken(let withDeviceId, let fcmToken, let device_type):
            let parameters: [String: Any] = ["device_id": withDeviceId, "token": fcmToken, "device_type": device_type]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .loadFriendAvailableOnGroup(let withKeyword, let groupId):
            let parameters: [String: Any] = ["keyword": withKeyword, "group_id": groupId]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .removeFriendToGroup(let withId):
            let parameters: [String: Any] = ["friend_id": withId]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .updateGroupPrivacy(let withGroupId, let type, let heartbeat, let stress_level, let emotion, let my_mood):
            let parameters: [String: Any] = ["id": withGroupId, "type": type, "value": "", "type_data": "", "emotions": "", "heartbeat": heartbeat, "stress_level": stress_level, "emotion": emotion, "my_mood": my_mood]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .updateManagePrivacy(let privacy, let emotion, let heartbeat, let stress_level, let notification_type, let auto_reconnect, let auto_renew):
            let parameters: [String: Any] = ["privacy": privacy, "emotion": emotion, "heartbeat": heartbeat, "stress_level": stress_level, "notification_type": notification_type, "auto_reconnect": auto_reconnect, "auto_renew": auto_renew]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .storedRecord(let heartbeat_count, let stress_level, let type, let emotion_set, let emotion_value, let emotion_level, let longitude, let latitude, let ppi, let total_ppi, let created_at, let timezone, let old_emotion, let stress_level_value):
            let parameters: [String: Any] = ["heartbeat_count": heartbeat_count, "stress_level": stress_level, "type": type, "emotion_set": emotion_set, "emotion_value": emotion_value, "emotion_level": emotion_level, "longitude": longitude, "latitude": latitude, "ppi": ppi, "total_ppi": total_ppi, "created_at": created_at, "timezone": timezone, "old_emotion": old_emotion, "stress_level_value": stress_level_value]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .loadDownloadedTheme(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .searchSticker(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .sendDataOffline(let data):
            let parameters: [String: Any] = ["data": data]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .sendFeedback(let email, let type, let content):
            let parameters: [String: Any] = ["email": email, "type": type, "content": content]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .updateUserProfile(let userStatus):
            let parameters: [String: Any] = ["status": userStatus]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .stickerList(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .storeProgressValue(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
        case .logOut(_):
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: nil)
            break
        }
        
        return urlRequest
    }
}
