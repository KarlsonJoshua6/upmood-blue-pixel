//
//  AlarmAddEditViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 05/04/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer

class AlarmAddEditViewController: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    
    var alarmScheduler: AlarmSchedulerDelegate = Scheduler()
    var alarmModel: Alarms = Alarms()
    var segueInfo: SegueInfo!
    var snoozeEnabled: Bool = false
    var enabled: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        alarmModel=Alarms()
        snoozeEnabled = segueInfo.snoozeEnabled
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveEditAlarm(_ sender: AnyObject) {
        let date = Scheduler.correctSecondComponent(date: datePicker.date)
        var tempAlarm = Alarm()
        tempAlarm.date = date
        tempAlarm.label = segueInfo.label
        tempAlarm.enabled = true
        tempAlarm.mediaLabel = segueInfo.mediaLabel
        tempAlarm.mediaID = segueInfo.mediaID
        tempAlarm.snoozeEnabled = snoozeEnabled
        tempAlarm.repeatWeekdays = segueInfo.repeatWeekdays
        tempAlarm.uuid = UUID().uuidString
        tempAlarm.onSnooze = false
        
        alarmModel.alarms.removeAll()
        snoozeEnabled = false
        
        if segueInfo.isEditMode {
            alarmModel.alarms[0] = tempAlarm
        } else {
            alarmModel.alarms.append(tempAlarm)
        }
        
        // ser alarm
        alarmScheduler.setNotificationWithDate(alarmModel.alarms[0].date, onWeekdaysForNotify: alarmModel.alarms[0].repeatWeekdays, snoozeEnabled: alarmModel.alarms[0].snoozeEnabled, onSnooze: false, soundName: alarmModel.alarms[0].mediaLabel, index: 0)
        
        UserDefaults.standard.set(alarmModel.alarms[0].formattedTime, forKey: "alarmTime")
        
        self.performSegue(withIdentifier: "testUnwind", sender: self)
    }
}
