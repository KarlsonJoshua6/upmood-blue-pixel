//
//  Persistable.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 03/05/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

protocol Persistable {
    var ud: UserDefaults {get}
    var persistKey : String {get}
    func persist()
    func unpersist()
}
