//
//  testViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 14/10/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import CLWaterWaveView
import SVProgressHUD
import Reachability
import UserNotifications
import UIKit

protocol UserSleepModeDelegate {
    func didUserStopSleepMode(bool: Bool)
}

class testViewController: UIViewController {
    
    // MARK: - For Setting Alarm
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    var alarmScheduler: AlarmSchedulerDelegate = Scheduler()
    var alarmModel: Alarms = Alarms()
    var segueInfo: SegueInfo!
    var snoozeEnabled: Bool = false
    var enabled: Bool!
    var reachability = Reachability()
    var sleepModeDelegate: UserSleepModeDelegate?
    
    @IBOutlet weak var blankView: UIVisualEffectView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var alarmTimeLabel: UILabel!
    @IBOutlet weak var waterView: CLWaterWaveView!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var alarmView: UIView!
    @IBOutlet weak var connectionBannerView: UIView!
    
//    var alarmScheduler: AlarmSchedulerDelegate = Scheduler()
//    var alarmModel: Alarms = Alarms()
    
    override func viewDidLoad() {
        
        // guest
        let isGuest = UserDefaults.standard.integer(forKey: "isGuest")
        if isGuest == 1 {
            DispatchQueue.main.async {
                self.dismissButton.isHidden = false
                self.blankView.isHidden = false
                self.blankView.backgroundColor = UIColor.black.withAlphaComponent(0.75)
            }
        } else {
            DispatchQueue.main.async {
                self.dismissButton.isHidden = true
                self.blankView.isHidden = true
            }
        }
        
        super.viewDidLoad()
        alarmScheduler.checkNotification()
        setUpWaveView()
        displayAlarmTime()
        blurView.isHidden = true
    
        if UserDefaults.standard.bool(forKey: "didTappedDontShowAgain") == false {
            self.warningDialog()
        } else {
            // true == dont show dialog again
        }
        
        // notification action
        UNUserNotificationCenter.current().delegate = self
        
        // check internet
        checkInternet()
    }
    
    // MARK: - For Setting Alarm
    override func viewWillAppear(_ animated: Bool) {
        alarmModel=Alarms()
        snoozeEnabled = true
        super.viewWillAppear(animated)
//        monitorNetworkConnection()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.sleepModeDelegate?.didUserStopSleepMode(bool: true)
    }
    
    // MARK: - For Setting Alarm
    
    @IBAction func setAlarm(_ sender: UIButton) {
        blurView.fadeIn()
        dialogView.isHidden = true
        alarmView.isHidden = false
    }
    
    @IBAction func noAlarm(_ sender: UIButton) {
        let noAlartText = NSLocalizedString("No Alarm", comment: "")
        UserDefaults.standard.set(noAlartText, forKey: "alarmTime")
        self.dialogView.isHidden = true
        self.alarmView.isHidden = true
        self.blurView.fadeOut()
        self.displayAlarmTime()
    }
    
    @IBAction func saveEditAlarm(_ sender: AnyObject) {
        let date = Scheduler.correctSecondComponent(date: datePicker.date)
        var tempAlarm = Alarm()
        tempAlarm.date = date
        tempAlarm.label = "Alarm"
        tempAlarm.enabled = true
        tempAlarm.mediaLabel = "bell"
        tempAlarm.mediaID = ""
        tempAlarm.snoozeEnabled = true
        tempAlarm.repeatWeekdays = []
        tempAlarm.uuid = UUID().uuidString
        tempAlarm.onSnooze = false
        
        alarmModel.alarms.removeAll()
        snoozeEnabled = false
        alarmModel.alarms.append(tempAlarm)
        
        // ser alarm
        alarmScheduler.setNotificationWithDate(alarmModel.alarms[0].date, onWeekdaysForNotify: alarmModel.alarms[0].repeatWeekdays, snoozeEnabled: alarmModel.alarms[0].snoozeEnabled, onSnooze: false, soundName: alarmModel.alarms[0].mediaLabel, index: 0)
        UserDefaults.standard.set(alarmModel.alarms[0].formattedTime, forKey: "alarmTime")
        
        self.dialogView.isHidden = true
        self.alarmView.isHidden = true
        self.blurView.fadeOut()
        self.displayAlarmTime()
    }
    
    @IBAction func close(_ sender: UIButton) {
        blurView.fadeIn()
        dialogView.isHidden = false
        alarmView.isHidden = true
    }
    
    @IBAction func ok(_ sender: UIButton) {
        UserDefaults.standard.set("", forKey: "sleep_code")
        UserDefaults.standard.set("No Alarm", forKey: "alarmTime")
        UserDefaults.standard.set(false, forKey: "didUserIsInSleepMode")
        Constants.didUserStopSleepMode = true
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        self.dialogView.isHidden = true
        self.alarmView.isHidden = true
        self.blurView.fadeOut()
    }
    
    private func checkInternet() {
        if SystemConfig.isConnectedToNetwork() {
            connectionBannerView.isHidden = true
        }else{
            connectionBannerView.isHidden = false
        }
    }
    
    private func displayAlarmTime() {
        UserDefaults.standard.set(true, forKey: "didUserIsInSleepMode")
        UserDefaults.standard.set(1, forKey: "fetchAutoReconnect")
        alarmTimeLabel.text = UserDefaults.standard.string(forKey: "alarmTime") ?? NSLocalizedString("Set Alarm", comment: "")
    }
    
    private func setUpWaveView() {
        waterView.amplitude = 50.0
        waterView.speed = 0.035
        waterView.angularVelocity = 0.50
        waterView.depth = 0.50
        waterView.startAnimation()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dist = segue.destination as! UINavigationController
        let addEditController = dist.topViewController as! AlarmAddEditViewController
        if segue.identifier == Id.addSegueIdentifier {
            addEditController.navigationItem.title = "Add Alarm"
            addEditController.segueInfo = SegueInfo(curCellIndex: alarmModel.count, isEditMode: false, label: "Alarm", mediaLabel: "bell", mediaID: "", repeatWeekdays: [], enabled: false, snoozeEnabled: false)
        } else if segue.identifier == Id.editSegueIdentifier {
            addEditController.navigationItem.title = "Edit Alarm"
            addEditController.segueInfo = sender as! SegueInfo
        }
    }
    
    @IBAction func testUnwind(_ segue: UIStoryboardSegue) {
        displayAlarmTime()
    }
    
    @IBAction func dismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func warningDialog() {
        DispatchQueue.main.async {
            let messageTxt = NSLocalizedString("You will not be able to gather emotion data during sleep mode.", comment: "")
            
            let alert = UIAlertController(title: NSLocalizedString("Sleep Mode", comment: ""), message: "\(messageTxt) \(NSLocalizedString("By going into sleep mode. you automatically turn on Auto-reconnect.", comment: ""))", preferredStyle: UIAlertControllerStyle.alert)

            let gotItTxt = NSLocalizedString("Got it", comment: "")
            alert.addAction(UIAlertAction(title: gotItTxt, style: .default, handler: { (alert) in
                UserDefaults.standard.set(false, forKey: "didTappedDontShowAgain")
            }))

            let dontShowTxt = NSLocalizedString("Don't show this again.", comment: "")
            alert.addAction(UIAlertAction(title: dontShowTxt, style: .cancel, handler: { (alert) in
                UserDefaults.standard.set(true, forKey: "didTappedDontShowAgain")
            }))

            self.present(alert, animated: true, completion: nil)
        }
    }
}

// MARK: - UILocalNotification

extension testViewController: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let _ = response.notification.request.content.userInfo
        self.dismiss(animated: true, completion: nil)

        // you must call the completion handler when you're done
        completionHandler()
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}
