//
//  SleepModeDashboardViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 16/09/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import AVFoundation
import CLWaterWaveView
import SVProgressHUD
import UIKit

class SleepModeDashboardViewController: UIViewController {
    
    @IBOutlet weak var alarmTimeLabel: UILabel!
    @IBOutlet weak var waterView: CLWaterWaveView!

    var alarmScheduler: AlarmSchedulerDelegate = Scheduler()
    var alarmModel: Alarms = Alarms()
    
    var didUserTappedDontShow = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alarmScheduler.checkNotification()
        setUpWaveView()
        displayAlarmTime()
    }
    
    private func displayAlarmTime() {
        UserDefaults.standard.set(true, forKey: "didUserIsInSleepMode")
         UserDefaults.standard.set(1, forKey: "fetchAutoReconnect")
        alarmTimeLabel.text = UserDefaults.standard.string(forKey: "alarmTime") ?? "Set Alarm"
        
        if UserDefaults.standard.bool(forKey: "didTappedDontShowAgain") == false {
            self.warningDialog()
        } else {
            // true == dont show dialog again
        }
    }
    
    private func setUpWaveView() {
        waterView.amplitude = 50.0
        waterView.speed = 0.035
        waterView.angularVelocity = 0.50
        waterView.depth = 0.50
        waterView.startAnimation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dist = segue.destination as! UINavigationController
        let addEditController = dist.topViewController as! AlarmAddEditViewController
        if segue.identifier == Id.addSegueIdentifier {
            addEditController.navigationItem.title = "Add Alarm"
            addEditController.segueInfo = SegueInfo(curCellIndex: alarmModel.count, isEditMode: false, label: "Alarm", mediaLabel: "bell", mediaID: "", repeatWeekdays: [], enabled: false, snoozeEnabled: false)
        } else if segue.identifier == Id.editSegueIdentifier {
            addEditController.navigationItem.title = "Edit Alarm"
            addEditController.segueInfo = sender as! SegueInfo
        }
    }
    
    @IBAction func testUnwind(_ segue: UIStoryboardSegue) {
        displayAlarmTime()
    }
    
    @IBAction func stopSleeepMode(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Sleep Mode Activated", message: "You should be asleep right now.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "I'm Awake", style: .default, handler: { [weak self] (alert) in
            UserDefaults.standard.set("Set Alarm", forKey: "alarmTime")
            UserDefaults.standard.removeObject(forKey: "sleep_code")
            UserDefaults.standard.set(false, forKey: "didUserIsInSleepMode")
//            let retriveJSONString = UserDefaults.standard.string(forKey: "offlineCalculation") ?? ""
//            self?.sendOfflineData(data: retriveJSONString)
            self?.didUserTappedDontShow = false
//            self?.alarmModel.alarms.removeAll()
            self?.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { (alert) in
            
        }))

        self.present(alert, animated: true, completion: nil)
    }
    
    private func warningDialog() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Sleep Mode", message: "You will not be able to gather emotion data during sleep mode. By going into sleep mode. you automatically turn on Auto-reconnect.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Got it", style: .default, handler: { (alert) in
                UserDefaults.standard.set(false, forKey: "didTappedDontShowAgain")
                UserDefaults.standard.synchronize()
            }))
            
            alert.addAction(UIAlertAction(title: "Don't show this again.", style: .cancel, handler: { (alert) in
                UserDefaults.standard.set(true, forKey: "didTappedDontShowAgain")
                UserDefaults.standard.synchronize()
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}
