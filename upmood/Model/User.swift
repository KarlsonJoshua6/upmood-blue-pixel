//
//  User.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 07/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import AuthenticationServices

struct User {
    var userName: String
    var UserId: String
    var email: String
    var image: String
}

struct UserCredential {
    var email: String
    var userId: String
    var token: String
}

struct AppleUser {
    let id: String
    let firstName: String
    let lastName: String
    let email: String
    
    @available(iOS 13.0, *)
    init(credentials: ASAuthorizationAppleIDCredential) {
        self.id = credentials.user
        self.firstName = credentials.fullName?.givenName ?? ""
        self.lastName = credentials.fullName?.familyName ?? ""
        self.email = credentials.email ?? ""
    }
}
