//
//  group.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 29/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

struct Group {
    
    let name: String
    let image: String

    init(name: String, image: String) {
        self.name = name
        self.image = image
    }
}
