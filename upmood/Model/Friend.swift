//
//  Friend.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 04/10/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

struct Friend {
    var friendId: String
    var userName: String
    var image: String
}
