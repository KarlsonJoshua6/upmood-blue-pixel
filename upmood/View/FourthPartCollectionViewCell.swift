//
//  FourthPartCollectionViewCell.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 08/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class FourthPartCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var defaultIconsImageView: UIImageView!
    @IBOutlet weak var defaultIconsNameLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                self.isOpaque = true
                self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            } else {
                self.isOpaque = false
                self.transform = CGAffineTransform.identity
            }
        }
    }
}
