//
//  WorkspaceDashboardViewController.swift
//  workspacec
//
//  Created by John Paul Manoza on 27/04/2019.
//  Copyright © 2019 Joseph Mikko Manoza. All rights reserved.
//

import TTSegmentedControl
import QRCodeReader
import UIKit

let viewControllerIdentifiers = ["MY Sessions", "Session"]

class WorkspaceDashboardViewController: UIViewController {
    
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            
            // Configure the view controller (optional)
            $0.showTorchButton        = false
            $0.showSwitchCameraButton = false
            $0.showCancelButton       = true
            $0.showOverlayView        = true
            $0.rectOfInterest         = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    @IBOutlet weak var tab: TTSegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        initTab()
        initContainerView()
    }

    @IBAction func scan(_ sender: UIBarButtonItem) {
        readerVC.delegate = self
        readerVC.modalPresentationStyle = .formSheet
        present(readerVC, animated: true, completion: nil)
    }
    
    private func changeTab(withIndex: Int) {
        let newController = storyboard!.instantiateViewController(withIdentifier: viewControllerIdentifiers[withIndex])
        let oldController = childViewControllers.last!
        
        oldController.willMove(toParentViewController: nil)
        addChildViewController(newController)
        newController.view.frame = oldController.view.frame
        
        transition(from: oldController, to: newController, duration: 0.25, options: .transitionCrossDissolve, animations: {
            // nothing needed here
        }, completion: { _ -> Void in
            oldController.removeFromParentViewController()
            newController.didMove(toParentViewController: self)
        })
    }
    
    private func initTab() {
        tab.allowChangeThumbWidth = false
        tab.itemTitles = [NSLocalizedString("My Sessions", comment: ""), NSLocalizedString("Sessions", comment: "")]
        tab.cornerRadius = 5.0
        tab.didSelectItemWith = { [weak self] (index, title) -> () in
            self?.changeTab(withIndex: index)
        }
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func unwindFromSession(_ sender: UIStoryboardSegue) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension WorkspaceDashboardViewController: QRCodeReaderViewControllerDelegate {
    
    // MARK: - QRCodeReaderViewController Delegate Methods
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        
        print("qr id: - \(result.value)")
        
        UserDefaults.standard.set(result.value, forKey: "idStr")
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "showViewSession", sender: self)
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
    }
}

extension WorkspaceDashboardViewController {
    private func initContainerView() {
        let newController = storyboard!.instantiateViewController(withIdentifier: viewControllerIdentifiers[0])
        let oldController = childViewControllers.last!
        
        oldController.willMove(toParentViewController: nil)
        addChildViewController(newController)
        newController.view.frame = oldController.view.frame
        
        transition(from: oldController, to: newController, duration: 0.25, options: .transitionCrossDissolve, animations: {
            // nothing needed here
        }, completion: { _ -> Void in
            oldController.removeFromParentViewController()
            newController.didMove(toParentViewController: self)
        })
    }
}
