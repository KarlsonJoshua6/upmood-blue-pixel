
//
//  DisplayCurrentSessionViewModel.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 07/05/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import DateToolsSwift
import UIKit

class DisplayCurrentSessionViewModel: NSObject {
    
    @IBOutlet var currentSessionService: DisplayCurrentSessionService!
    var session: NSDictionary?

    var startTimeArr: [String] = []
    var endTimeArr: [String] = []
    var day: [String] = []
    
    func fetchCurrentSessionList(completion: @escaping () -> ()) {
        currentSessionService.fetchCurrentSession { dict in
            self.session = dict
            completion()
        }
    }
    
    func getCurrentUserId() -> Int {
         return session?.value(forKeyPath: "user_id") as? Int ?? 0
    }
    
    func getSessionId() -> Int {
        return session?.value(forKeyPath: "id") as? Int ?? 0
    }
    
    func getSessionName() -> String {
        return session?.value(forKeyPath: "session_name") as? String ?? ""
    }
    
    func getSessionStatus() -> Int {
        return session?.value(forKeyPath: "status") as? Int ?? 0
    }
    
    func getCompanyName() -> String {
        return session?.value(forKeyPath: "company_name") as? String ?? ""
    }
    
    func getSessionStart() -> String {
        return session?.value(forKeyPath: "session_start") as? String ?? ""
    }
    
    func getSessionEnd() -> String {
        return session?.value(forKeyPath: "session_end") as? String ?? ""
    }
    
    func getElapsedTime() -> String {
        return elapsedTimeWithGatheringType()
    }
    
    func getGatheringType() -> String {
        return session?.value(forKeyPath: "gathering_type") as? String ?? ""
    }
    
    func getUserStatus() -> Int {
        return session?.value(forKeyPath: "user_status") as? Int ?? 0
    }
    
    private func elapsedTimeWithGatheringType() -> String {
        
        if let daytime_interval = session?.value(forKeyPath: "distinct_days") as? [[String: Any]] {
            for daytime in daytime_interval {
                let day = daytime["day"] as? String ?? ""
                let startTime = daytime["start_time"] as? String ?? ""
                let endTime = daytime["end_time"] as? String ?? ""
                self.day.append(day)
                self.endTimeArr.append(endTime)
                self.startTimeArr.append(startTime)
            }
        }
        
        if getGatheringType() == "Interval" {
            
            let start_interval = getSessionStart() + " " + self.startTimeArr[0]
            let end_interval = getSessionEnd() + " " + self.endTimeArr.last!
            
//            let start_interval = getSessionStart()
//            let end_interval = getSessionEnd()
        
            if checkIfStarted(startTime: start_interval) {
                return elapsedTimeContinuos(endTime: end_interval)
            } else {
                return "Not yet started"
            }

        } else {
            
            if checkIfStarted(startTime: getSessionStart()) {
                return elapsedTimeContinuos(endTime: getSessionEnd())
            } else {
                return "Not yet started"
            }
        }
    }
    
    private func elapsedTimeInterval() -> String {
        return "None"
    }
    
    private func elapsedTimeContinuos(endTime: String) -> String {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = TimeZone.current
        df.locale = Locale.current
        
        let date2 = df.date(from: endTime)
        
        if date2 == nil {
             return "None"
        } else {
            
            let userCalendar = Calendar.current
            let date = Date()
            let components = userCalendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: date)
            let components2 = userCalendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: date2!)
            var localTimeZoneName: String { return TimeZone.current.identifier }
            let currentDate = userCalendar.date(from: components)!
        
            var eventDateComponents = DateComponents()
            eventDateComponents.year = components2.year!
            eventDateComponents.month = components2.month!
            eventDateComponents.day = components2.day!
            eventDateComponents.hour = components2.hour
            eventDateComponents.minute = components2.minute!
            eventDateComponents.second = components2.second!
            eventDateComponents.timeZone = TimeZone(abbreviation: localTimeZoneName)
             
            let eventDate = userCalendar.date(from: eventDateComponents)!
            let timeLeft = userCalendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: eventDate)
            
            if timeLeft.day! > 1 {
                if timeLeft.hour! > 1 {
                    return "\(timeLeft.day!) Days, \(timeLeft.hour!) Hours"
                } else {
                    return "\(timeLeft.day!) Days, \(timeLeft.hour!) Hour"
                }
            } else {
                if timeLeft.hour! == 0 {
                    if timeLeft.minute! > 1 {
                        return "\(timeLeft.minute!) Minutes"
                    } else if timeLeft.minute! == 0 {
                       return "End"
                    } else {
                        return "\(timeLeft.minute!) Minute"
                    }
                    
                } else {
                     if timeLeft.minute! > 1 {
                         return "\(timeLeft.hour!) Hours, \(timeLeft.minute!) Minutes"
                     } else {
                         return "\(timeLeft.hour!) Hours, \(timeLeft.minute!) Minute"
                     }
                }
            }
        }
    }
    
    private func checkIfStarted(startTime: String) -> Bool {
        
        let Dateformatter = DateFormatter()
        Dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let start = Dateformatter.date(from: startTime) ?? Date()
        let current = Date()
        
        print("TEST IF STARTED - \(start), \(current)")
        
        if current > start {
            print("Date1 is after Date2")
            return true
        } else if current == start {
            print("Date1 is equal to Date2")
            return true
        } else if current < start {
            print("Date1 is before Date2")
            return false
        }
        
        return false
    }
}
