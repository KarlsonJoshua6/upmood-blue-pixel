//
//  DisplayCurrentSessionService.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 10/05/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import UIKit

class DisplayCurrentSessionService: NSObject {

    func fetchCurrentSession(completion: @escaping (_ session: NSDictionary?) -> ()) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        Alamofire.request(Service.fetchMySessionList(time_zone: localTimeZoneName)).responseJSON { response  in
            switch response.result {
            case .success(let json):
                
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                guard let jsonData = JSON["data"] as? [String: Any] else { return }
                
                switch statusCode {
                case 200:
                    if let jsonDict = jsonData["current_session"] as? NSDictionary {
                        print("fetchCurrentSession: \(jsonDict)")
                        completion(jsonDict)
                        return
                    }
                    break
                case 204:
                    //
                    break
                case 422:
                    if let errorDict = JSON["error"] as? NSDictionary {
                        completion(errorDict)
                        return
                    }
                    break
                default:break
                }
                
                break
            case .failure(_):
                break
            }
        }
    }
}
