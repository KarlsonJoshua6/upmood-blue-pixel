//
//  ViewSessionService.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 10/05/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import UIKit

class ViewSessionService: NSObject {
    
    @IBOutlet var viewModel: ViewSessionViewModel!
    
    func fetchViewSession(completion: @escaping (_ session: NSDictionary?) -> ()) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateFormatter.string(from: Date())
        var localTimeZoneName: String { return TimeZone.current.identifier }
        
        let fetchUserQrCodeId = UserDefaults.standard.string(forKey: "idStr") ?? ""
        
        Alamofire.request(Service.viewSession(withId: Int(fetchUserQrCodeId) ?? 0, time_zone: localTimeZoneName)).responseJSON { response  in
            switch response.result {
            case .success(let json):
                
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                
                switch statusCode {
                case 200:
                    if let jsonDict = JSON["data"] as? NSDictionary {
                        print("fetchViewSession: \(jsonDict)")
                        completion(jsonDict)
                        return
                    }
                    break
                case 204:
                    if let emptyDict = JSON["status"] as? NSDictionary {
                        print("emptyDict: \(emptyDict)")
                        completion(emptyDict)
                        return
                    }
                case 422:
                    if let errorDict = JSON["error"] as? NSDictionary {
                        completion(errorDict)
                        return
                    }
                    break
                default:break
                }
                
                break
            case .failure(let error):
                print("error fetchviewsession - \(error.localizedDescription)")
                
                
                if let emptyDict = error as? NSDictionary {
                    print("emptyDict: \(emptyDict)")
                    completion(emptyDict)
                    return
                }
                return
            }
        }
    }
    
    func joinFitnessSession(completion: @escaping (_ session: NSDictionary?) -> ()) {
        
        var passcode = ""
        let age = UserDefaults.standard.string(forKey: "ageString") ?? ""
        let weight = UserDefaults.standard.string(forKey: "weightString") ?? ""
        var sex = UserDefaults.standard.string(forKey: "sexString") ?? ""
        
        if viewModel.getSessionPrivacy() == 1 {
            passcode = UserDefaults.standard.string(forKey: "userEnteredPasscode") ?? ""
        }
        
        let fetchUserQrCodeId = UserDefaults.standard.string(forKey: "idStr") ?? ""
        
        if sex == "男" {
            sex = "Male"
        } else if sex == "女" {
            sex = "Female"
        }
        
        print("credentials: = \(fetchUserQrCodeId), \(passcode), \(age), \(weight), \(sex.lowercased())")
        
        Alamofire.request(Service.joinSessionTypeFitness(withId: Int(fetchUserQrCodeId) ?? 0, passcode: passcode, age: Int(age) ?? 0, weight: Float(weight) ?? 0.0, sex: sex.lowercased())).responseJSON { response  in
            switch response.result {
            case .success(let json):
                
                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                let responseMessage = JSON["message"] as? String ?? ""
                
                print("join fitness response: - \(JSON)")
                
                switch statusCode {
                case 200:
                    if let jsonDict = JSON["data"] as? NSDictionary {
                        print("join fitness session: \(jsonDict)")
                        completion(jsonDict)
                        return
                    }
                    
                default:
                    
                    var errorData = [String: Any]()
                      errorData.updateValue(statusCode, forKey: "statusCode")
                      errorData.updateValue(responseMessage, forKey: "message")
                      
                      if let emptyDict = errorData as? NSDictionary {
                          completion(emptyDict)
                          return
                      }
                }
                
                print("json: \(JSON)")
                
                break
            case .failure(_):
                return
            }
        }
    }
    
    func joinViewSession(completion: @escaping (_ session: NSDictionary?) -> ()) {

        var passcode = ""
        if viewModel.getSessionPrivacy() == 1 {
            passcode = UserDefaults.standard.string(forKey: "userEnteredPasscode") ?? ""
        }

        let fetchUserQrCodeId = UserDefaults.standard.string(forKey: "idStr") ?? ""

        Alamofire.request(Service.joinSesion(withId: Int(fetchUserQrCodeId) ?? 0, passcode: passcode)).responseJSON { response  in
            switch response.result {
            case .success(let json):

                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                let responseMessage = JSON["message"] as? String ?? ""

                switch statusCode {
                case 200:
                    if let jsonDict = JSON["data"] as? NSDictionary {
                        print("join session: \(jsonDict)")
                        completion(jsonDict)
                        return
                    }
                    break
                default:
                    var errorData = [String: Any]()
                    errorData.updateValue(statusCode, forKey: "statusCode")
                    errorData.updateValue(responseMessage, forKey: "message")
                    
                    if let emptyDict = errorData as? NSDictionary {
                        completion(emptyDict)
                        return
                    }
                }

                print("json: \(JSON)")

                break
            case .failure(_):
                return
            }
        }
    }

    func leaveViewSession(completion: @escaping (_ session: NSDictionary?) -> ()) {

        var passcode = ""

        if viewModel.getSessionPrivacy() == 1 {
            passcode = UserDefaults.standard.string(forKey: "userEnteredPasscode") ?? ""
        }

        let fetchUserQrCodeId = UserDefaults.standard.string(forKey: "idStr") ?? ""

        Alamofire.request(Service.leaveSession(withId: Int(fetchUserQrCodeId) ?? 0, passcode: passcode)).responseJSON { response  in
            switch response.result {
            case .success(let json):

                guard let JSON = json as? [String: Any] else { return }
                let statusCode = JSON["status"] as? Int ?? 0
                let responseMessage = JSON["message"] as? String ?? ""

                switch statusCode {
                case 200:
                    if let jsonDict = JSON["data"] as? NSDictionary {
                        print("leave session: \(jsonDict)")
                        completion(jsonDict)
                        return
                    }
                    
                default:
                    var errorData = [String: Any]()
                    errorData.updateValue(statusCode, forKey: "statusCode")
                    errorData.updateValue(responseMessage, forKey: "message")
                    
                    if let emptyDict = errorData as? NSDictionary {
                        completion(emptyDict)
                        return
                    }
                }

                print("json: \(JSON)")

                break
            case .failure(_):
                return
            }
        }
    }
}
