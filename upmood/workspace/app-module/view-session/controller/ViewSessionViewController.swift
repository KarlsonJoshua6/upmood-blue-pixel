//
//  ViewSessionViewController.swift
//  workspacec
//
//  Created by John Paul Manoza on 27/04/2019.
//  Copyright © 2019 Joseph Mikko Manoza. All rights reserved.
//

import UIKit

class ViewSessionViewController: UIViewController {
    
    @IBOutlet weak var session_name_label: UILabel!
    @IBOutlet weak var session_type_label: UILabel!
    @IBOutlet weak var session_description_label: UILabel!
    @IBOutlet weak var session_status: UILabel!
    @IBOutlet weak var session_date_time: UILabel!
    @IBOutlet weak var session_active_user_count_label: UILabel!
    @IBOutlet weak var session_company_label: UILabel!
    @IBOutlet weak var session_time_start_end: UITextView!
    @IBOutlet weak var session_time_start_end_icon: UIImageView!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var gathering_since_label: UILabel!
    @IBOutlet var viewModel: ViewSessionViewModel!
    @IBOutlet weak var sessionViewCorporate: UIView!
    @IBOutlet weak var sessionViewFitness: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
  
    //labels corporate view
    @IBOutlet weak var session_name_corporate_label: UILabel!
    @IBOutlet weak var session_dot_corporate_imageView: UIImageView!
    @IBOutlet weak var session_company_name_label: UILabel!
    @IBOutlet weak var session_status_corporate: UILabel!
    @IBOutlet weak var passCodeCorporateLabel: UITextField!
    
    // labels fitness
    @IBOutlet weak var session_name_fitness_label: UILabel!
    @IBOutlet weak var session_dot_fitness_imageView: UIImageView!
    @IBOutlet weak var session_company_name_fitness_label: UILabel!
    @IBOutlet weak var session_status_fitness: UILabel!
    @IBOutlet weak var ageFitnessLabel: UITextField!
    @IBOutlet weak var weightFitnessLabel: UITextField!
    @IBOutlet weak var sexFitnessLabel: UITextField!
    @IBOutlet weak var passCodeFitnessText: UILabel!
    @IBOutlet weak var passCodeFitnessLabel: UITextField!
    @IBOutlet weak var sessionViewFitnessConstraints: NSLayoutConstraint!
    @IBOutlet weak var session_time_start_label_height: NSLayoutConstraint!
    @IBOutlet weak var timeIconHeight: NSLayoutConstraint!
    @IBOutlet weak var view_header_height: NSLayoutConstraint!
    @IBOutlet weak var intervalButton: UIButton!
    
    let genderPickerData = ["Please Select Gender", NSLocalizedString("Male", comment: ""), NSLocalizedString("Female", comment: "")]
    var selectedGender = ""
    
    var txt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.blurView.isHidden = true
        
        viewModel.fetchViewSession {
            DispatchQueue.main.async { [weak self] in
                self?.updateLayout()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let itemDetails = segue.destination as? IntervalDateAndTimeLIstViewController  {
            itemDetails.monday = viewModel.getMondayIntervalSched()
            itemDetails.tuesday = viewModel.getTuesdayIntervalSched()
            itemDetails.wednesday = viewModel.getWednesdayIntervalSched()
            itemDetails.thursday = viewModel.getThursdayIntervalSched()
            itemDetails.friday = viewModel.getFridayIntervalSched()
            itemDetails.saturday = viewModel.getSaturdayIntervalSched()
            itemDetails.sunday = viewModel.getSundayIntervalSched()
        }
    }
    
    private func updateLayout() {
        
        self.sessionViewCorporate.dropShadow()
        self.sessionViewFitness.dropShadow()
        
        self.session_name_label.text = self.viewModel.getSessionName()
        self.session_description_label.text = self.viewModel.getSessionDescription()
        self.session_active_user_count_label.text = self.viewModel.getActiveUser()
        self.session_company_label.text = self.viewModel.getCompanyName()
        self.session_date_time.text = self.viewModel.getDate()
        
        session_time_start_end.textContainer.heightTracksTextView = true
        session_time_start_end.isScrollEnabled = false
        self.session_time_start_end.text = self.viewModel.getTime()
        
        // session_type
        
        if self.viewModel.getSessionPrivacy() == 0 {
            self.session_type_label.text = NSLocalizedString("Public Session", comment: "")
        } else {
            self.session_type_label.text = NSLocalizedString("Private Session", comment: "")
        }
        
        // session_status
        
        if self.viewModel.getSessionStatus() == 0 {
            self.session_status.text = "End Session"
            self.session_status.textColor = UIColor.red
            self.joinButton.isHidden = true
            self.gathering_since_label.isHidden = true
        } else if self.viewModel.getSessionStatus() == 1 {
            self.session_status.text = NSLocalizedString("On Going", comment: "")
            self.session_status.textColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            self.joinButton.isHidden = false
            self.gathering_since_label.isHidden = true
            self.gathering_since_label.isHidden = false
        } else if self.viewModel.getSessionStatus() == 2 {
            self.session_status.text = NSLocalizedString("Paused", comment: "")
            self.session_status.textColor = UIColor.orange
            self.joinButton.isHidden = false
            self.gathering_since_label.isHidden = false
        }
        
        // button
        if self.viewModel.getUserStatus() == 0 {
            self.joinButton.setTitle(NSLocalizedString("Join", comment: ""), for: .normal)
            self.gathering_since_label.text = NSLocalizedString("by joining you agree to share your emotion", comment: "")
        } else if self.viewModel.getUserStatus() == 1 || self.viewModel.getUserStatus() == 2 {
            self.gathering_since_label.text = "\(NSLocalizedString("Gathering since", comment: "")) \(viewModel.getJoinedDate())"
            self.joinButton.setTitle(NSLocalizedString("Leave", comment: ""), for: .normal)
        } else if self.viewModel.getUserStatus() == 3 {
            self.gathering_since_label.text = NSLocalizedString("by joining you agree to share your emotion", comment: "")
            self.joinButton.setTitle(NSLocalizedString("Return", comment: ""), for: .normal)
        }
        
        // gathering_type
        if self.viewModel.getGatheringType() == "Continuous" {
            self.session_time_start_end.isHidden = true
            self.session_time_start_end_icon.isHidden = true
            self.session_time_start_end_icon.image = UIImage(named: "")
            self.intervalButton.isHidden = true
            self.session_time_start_label_height.constant = 0.0
            self.timeIconHeight.constant = 0.0
        } else {
            self.session_time_start_end.isHidden = false
            self.session_time_start_end_icon.isHidden = false
            self.intervalButton.isHidden = false
            self.session_time_start_label_height.constant = 52.0
            self.timeIconHeight.constant = 20.0
        }
    }
    
    @IBAction func join(_ sender: UIButton) {
        joinButton.backgroundColor = UIColor.gray
        view.isUserInteractionEnabled = false
        joinAction()
    }
    
    @IBAction func closeModal(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Stored (These 3 Buttons are exist only when the session type is Public Fitness Session And Private Any Session)
    
    @IBAction func pickGender(_ sender: UIButton) {
        pickGenderAction()
    }
    
    @IBAction func okayAction(_ sender: UIButton) {
        
        self.view.isUserInteractionEnabled = false
        
        if viewModel.getSessionType() == "Fitness" || viewModel.getSessionType() == "Fitness2"  {
            if self.ageFitnessLabel.text!.isEmpty || self.weightFitnessLabel.text!.isEmpty || self.sexFitnessLabel.text!.isEmpty {
                presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString(Constants.MessageDialog.INCOMPLETE_FORM, comment: ""))
                self.view.isUserInteractionEnabled = true
            } else if !isUserIsInValidEnteredAge() {
                presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Please Enter valid age. Max: 120")
                self.view.isUserInteractionEnabled = true
            } else if !isUserIsInvalidEnteredWeight() {
                presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "Please Enter valid weight in lbs. Max: 1,500 lbs")
                self.view.isUserInteractionEnabled = true
            } else {
                self.okayAction()
            }
            
        } else {
            if self.passCodeCorporateLabel.text!.isEmpty {
                presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: NSLocalizedString(Constants.MessageDialog.INCOMPLETE_FORM, comment: ""))
                self.view.isUserInteractionEnabled = true
            } else {
                self.okayAction()
            }
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.blurView.fadeOut()
    }
    
    @IBAction func showIntervalLIst(_ sender: UIButton) {
        performSegue(withIdentifier: "showIntervalLIst", sender: self)
    }
}
