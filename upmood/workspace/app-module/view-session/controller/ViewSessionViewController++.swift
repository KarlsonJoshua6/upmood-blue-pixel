//
//  ViewSessionViewController++.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 21/05/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import SCLAlertView
import UIKit

// MARK: - Stored Helper For Session Type = Fitness, Corporate, etc

extension ViewSessionViewController {
    
    private func showUIForOtherSessionDialog(withUserStatus: String) {
        
        DispatchQueue.main.async {
            self.blurView.isHidden = false
            self.blurView.fadeIn()
            self.sessionViewFitness.isHidden = true
            self.sessionViewCorporate.isHidden = false
            self.view.isUserInteractionEnabled = true
            self.joinButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            self.passCodeCorporateLabel.becomeFirstResponder()
            
            if self.viewModel.getSessionStatus() == 0 {
                self.session_status.textColor = UIColor.red
            } else if self.viewModel.getSessionStatus() == 1 {
                self.session_status.textColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            } else if self.viewModel.getSessionStatus() == 2 {
                self.session_status_corporate.textColor = UIColor.orange
            }
            
            self.session_name_corporate_label.text = self.viewModel.getSessionName()
            self.session_company_name_label.text = self.viewModel.getCompanyName()
            self.session_status_corporate.text = "\(withUserStatus)"
        }
    }
    
    private func showUIForFitnessSessionDialog(withUserStatus: String) {
        
        DispatchQueue.main.async {
            self.blurView.isHidden = false
            self.blurView.fadeIn()
            self.sessionViewFitness.isHidden = false
            self.sessionViewCorporate.isHidden = true
            self.view.isUserInteractionEnabled = true
            self.joinButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            self.ageFitnessLabel.becomeFirstResponder()
       
            if self.viewModel.getSessionStatus() == 0 {
                self.session_status_fitness.textColor = UIColor.red
            } else if self.viewModel.getSessionStatus() == 1 {
                self.session_status_fitness.textColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            } else if self.viewModel.getSessionStatus() == 2 {
                self.session_status_fitness.textColor = UIColor.orange
            }
            
            self.session_name_fitness_label.text = self.viewModel.getSessionName()
            self.session_company_name_fitness_label.text = self.viewModel.getCompanyName()
            self.session_status_fitness.text = "\(withUserStatus)"
        }
    }
    
    private func showUIForPublicFitnessSession(withUserStatus: String) {
        self.blurView.isHidden = false
        self.sessionViewFitness.isHidden = false
        self.sessionViewCorporate.isHidden = true
        self.session_name_fitness_label.text = viewModel.getSessionName()
        self.session_company_name_fitness_label.text = "by \(viewModel.getCompanyName())"
        self.session_status_fitness.text = withUserStatus
        self.sessionViewFitnessConstraints.constant = 400.0
        self.passCodeFitnessLabel.isHidden = true
        self.passCodeFitnessText.isHidden = true
        self.blurView.fadeIn()
        self.joinButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        self.view.isUserInteractionEnabled = true
    }
    
    func leaveToSession() {
        
        self.blurView.isHidden = true
        self.sessionViewFitness.isHidden = true
        self.sessionViewCorporate.isHidden = true
        self.view.isUserInteractionEnabled = true
        self.joinButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
        
        if sessionIsPrivate() {
            // private
            let alertController = UIAlertController(title: NSLocalizedString("Passcode is required.", comment: ""), message: NSLocalizedString("Enter passcode to leave session.", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = NSLocalizedString("Type passcode", comment: "")
            }
            
            let saveAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: { alert -> Void in
                let firstTextField = alertController.textFields![0] as UITextField
                UserDefaults.standard.set(firstTextField.text!, forKey: "userEnteredPasscode")
                if self.viewModel.getSessionType() == "Fitness" || self.viewModel.getSessionType() == "Fitness2"  {
                    self.join_session_fitness()
                } else {
                    self.join_session()
                }
            })
            
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .destructive, handler: nil)
            
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            // public
            if self.viewModel.getSessionType() == "Fitness" || self.viewModel.getSessionType() == "Fitness2" {
                self.join_session_fitness()
            } else {
                self.join_session()
            }
        }
    }
    
    func returnToSession() {
        if self.viewModel.getSessionType() == "Fitness" || self.viewModel.getSessionType() == "Fitness2" {
            self.join_session_fitness()
        } else {
            self.join_session()
        }
    }
    
    func joinAction() {
        
        var message = ""
        if viewModel.getSessionStatus() == 0 {
            message += "End Session"
        } else if viewModel.getSessionStatus() == 1 {
            message += NSLocalizedString("On Going", comment: "")
        } else if viewModel.getSessionStatus() == 2 {
            message += NSLocalizedString("Paused", comment: "")
        }
        
        if sessionIsPrivate() {
            
            // private session
            
            if viewModel.getSessionType() == "Fitness" || self.viewModel.getSessionType() == "Fitness2" {
                
                switch self.viewModel.getUserStatus() {
                case 0:
                    // joining
                    self.showUIForFitnessSessionDialog(withUserStatus: message)
                    break
                case 1:
                    // leaving but already join to a session
                    self.leaveToSession()
                case 2:
                    // leaving
                    self.leaveToSession()
                case 3:
                    // returning
                    self.showUIForFitnessSessionDialog(withUserStatus: message)
                default:
                    break
                }
        
            } else {
                // corporate, research, and other sessions
                switch self.viewModel.getUserStatus() {
                case 0:
                     self.showUIForOtherSessionDialog(withUserStatus: message)
                    break
                case 1:
                    // leaving but already join to a session
                    self.leaveToSession()
                case 2:
                    // leaving
                    self.leaveToSession()
                case 3:
                    // returning
                    self.returnToSession()
                default:
                    break
                }
            }
            
        } else {
            
            // public session
            
            if viewModel.getSessionType() == "Fitness" || viewModel.getSessionType() == "Fitness2" {
                
                switch self.viewModel.getUserStatus() {
                case 0:
                    // joining
                    self.showUIForPublicFitnessSession(withUserStatus: message)
                case 1:
                    // leaving but already join to a session
                    self.leaveToSession()
                case 2:
                    // leaving
                    self.leaveToSession()
                case 3:
                    // returning
                    self.returnToSession()
                default:
                    break
                }
                
            } else {
                // corporate, research, and other sessions
                self.join_session()
            }
        }
    }
}

// MARK: - Stored Helper For Joining Session Type == Corporate Session, Research Session, Etc, except. Fitness Session

extension ViewSessionViewController {
    func join_session() {
        switch self.viewModel.getUserStatus() {
        case 1:
            viewModel.leaveJoinedSession {
                
                self.joinButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                self.view.isUserInteractionEnabled = true
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                
                if self.viewModel.getResponseStatusCode() == 0 {
                    SCLAlertView().showSuccess(NSLocalizedString("Successfully Left Session", comment: ""), subTitle: "")
                    self.joinButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                    self.view.isUserInteractionEnabled = true
                    
                    if UserDefaults.standard.bool(forKey: "didUserClickOnPublicSessionList") == true {
                        self.dismiss(animated: true, completion: nil)
                        self.performSegue(withIdentifier: "unwindFromSession", sender: self)
                    } else {
                        self.performSegue(withIdentifier: "unwindToMySession", sender: self)
                    }
                } else {
                    SCLAlertView().showError(NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), subTitle: self.viewModel.getResponseMessage())
                }
                
                self.viewDidLoad()
            }
            
            break
        case 2:
            viewModel.leaveJoinedSession {
                
                self.joinButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                self.view.isUserInteractionEnabled = true
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                
                if self.viewModel.getResponseStatusCode() == 0 {
                     SCLAlertView().showSuccess(NSLocalizedString("Successfully Left Session", comment: ""), subTitle: "")
                     self.performSegue(withIdentifier: "unwindToMySession", sender: self)
                     self.joinButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                     self.view.isUserInteractionEnabled = true
                    
                     if UserDefaults.standard.bool(forKey: "didUserClickOnPublicSessionList") == true {
                         self.dismiss(animated: true, completion: nil)
                         self.performSegue(withIdentifier: "unwindFromSession", sender: self)
                     } else {
                         self.performSegue(withIdentifier: "unwindToMySession", sender: self)
                     }
                } else {
                    SCLAlertView().showError(NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), subTitle: self.viewModel.getResponseMessage())
                }
                
                self.viewDidLoad()
            }
            
            break
        default:
            viewModel.joinViewSession {
                
                self.joinButton.backgroundColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
                self.view.isUserInteractionEnabled = true
                self.navigationController?.setNavigationBarHidden(false, animated: false)

                if self.viewModel.getResponseStatusCode() == 0 {
                      self.performSegue(withIdentifier: "unwindToMySession", sender: self)
                      SCLAlertView().showSuccess(NSLocalizedString("Successfully Joined Session", comment: ""), subTitle: "\(NSLocalizedString("You are now sharing your emotion data.", comment: "")) \(NSLocalizedString("Session ends at", comment: "")) \(self.viewModel.getMessageDataOnSuccessJoined())")
                      
                      if UserDefaults.standard.bool(forKey: "didUserClickOnPublicSessionList") == true {
                          self.dismiss(animated: true, completion: nil)
                          self.performSegue(withIdentifier: "unwindFromSession", sender: self)
                      } else {
                          self.performSegue(withIdentifier: "unwindToMySession", sender: self)
                      }
                    
                } else {
                    SCLAlertView().showError(NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), subTitle: self.viewModel.getResponseMessage())
                }
                
                self.viewDidLoad()
            }
            
            break
        }
    }
}

// MARK: - Stored Helper For Joining Session Type == Fitness Session Only

extension ViewSessionViewController {
    func join_session_fitness() {
        
        switch self.viewModel.getUserStatus() {
        case 1:
            viewModel.leaveJoinedSession {
                
                self.view.isUserInteractionEnabled = true
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                
                if self.viewModel.getResponseStatusCode() == 0 {
                   self.performSegue(withIdentifier: "unwindToMySession", sender: self)
                   SCLAlertView().showSuccess(NSLocalizedString("Successfully Left Session", comment: ""), subTitle: "")
                   
                   if UserDefaults.standard.bool(forKey: "didUserClickOnPublicSessionList") == true {
                       self.dismiss(animated: true, completion: nil)
                       self.performSegue(withIdentifier: "unwindFromSession", sender: self)
                   } else {
                       self.performSegue(withIdentifier: "unwindToMySession", sender: self)
                   }
                } else {
                    SCLAlertView().showError(NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), subTitle: self.viewModel.getResponseMessage())
                }
                
                self.viewDidLoad()
            }
            
            break
        case 2:
            viewModel.leaveJoinedSession {
                
                self.view.isUserInteractionEnabled = true
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                
                if self.viewModel.getResponseStatusCode() == 0 {
                    self.performSegue(withIdentifier: "unwindToMySession", sender: self)
                    SCLAlertView().showSuccess(NSLocalizedString("Successfully Left Session", comment: ""), subTitle: "")
                    
                    if UserDefaults.standard.bool(forKey: "didUserClickOnPublicSessionList") == true {
                        self.dismiss(animated: true, completion: nil)
                        self.performSegue(withIdentifier: "unwindFromSession", sender: self)
                    } else {
                        self.performSegue(withIdentifier: "unwindToMySession", sender: self)
                    }
                } else {
                    SCLAlertView().showError(NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), subTitle: self.viewModel.getResponseMessage())
                }
                
                self.viewDidLoad()
            }
        default:
            viewModel.joinFitnessSession {
                
                self.view.isUserInteractionEnabled = true
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                
                if self.viewModel.getResponseStatusCode() == 0 {
                    self.performSegue(withIdentifier: "unwindToMySession", sender: self)
                    SCLAlertView().showSuccess(NSLocalizedString("Successfully Joined Session", comment: ""), subTitle: "\(NSLocalizedString("You are now sharing your emotion data.", comment: "")) \(NSLocalizedString("Session ends at", comment: "")) \(self.viewModel.getMessageDataOnSuccessJoined())")
                    
                    if UserDefaults.standard.bool(forKey: "didUserClickOnPublicSessionList") == true {
                        self.dismiss(animated: true, completion: nil)
                        self.performSegue(withIdentifier: "unwindFromSession", sender: self)
                    } else {
                        self.performSegue(withIdentifier: "unwindToMySession", sender: self)
                    }
                } else {
                    SCLAlertView().showError(NSLocalizedString(NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), comment: ""), subTitle: self.viewModel.getResponseMessage())
                }
                
                self.viewDidLoad()
            }
            
            break
        }
    }
}
