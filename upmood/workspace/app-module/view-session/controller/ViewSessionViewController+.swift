//
//  ViewSessionViewControllerPlus.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 21/05/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

// MARK: - Stored Helper

extension ViewSessionViewController {
    func sessionIsPrivate() -> Bool {
        if viewModel.getSessionPrivacy() == 1 {
            return true
        }
        
        return false
    }
}

// MARK: - Stored UIPickerViewDelegate, UIPickerViewDataSource For Gender Picker

extension ViewSessionViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedGender = genderPickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderPickerData[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderPickerData.count
    }
}

// MARK: - Stored Helper For Session Type = Fitness, Corporate, etc

extension ViewSessionViewController {
    func isUserIsInValidEnteredAge() -> Bool {
        
        let textAge = self.ageFitnessLabel.text!
        let intAge = Int(textAge) ?? 0
        
        if intAge > 120 {
            return false
        }
        
        return true
    }
    
    func isUserIsInvalidEnteredWeight() -> Bool {
        let textWeight = self.weightFitnessLabel.text!
        let intWeight = Int(textWeight) ?? 0
        
        if intWeight > 1500 {
            return false
        }
        
        return true
    }
    
    func pickGenderAction() {
        let alertView = UIAlertController(title: NSLocalizedString("Select Gender", comment: ""), message: "\n\n\n\n\n\n", preferredStyle: .alert)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 50, width: 260, height: 80))
        pickerView.dataSource = self
        pickerView.delegate = self
        
        alertView.view.addSubview(pickerView)
        let action = UIAlertAction(title: NSLocalizedString("Change", comment: ""), style: UIAlertActionStyle.default, handler: { alertView in
            
            DispatchQueue.main.async { [weak self] in
                
                if self?.selectedGender == "Please Select Gender" {
                    self?.presentDismissableAlertController(title: "Warning", message: "Please Select Gender")
                }
    
                let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
                if selectedLanguage == "ja" {
                   if self?.selectedGender == "Male" {
                        self?.sexFitnessLabel.text = "男"
                   } else {
                       self?.sexFitnessLabel.text = "女"
                   }
                } else {
                    self?.sexFitnessLabel.text = self?.selectedGender
                }
            }
            
            pickerView.reloadAllComponents()
        })
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        
        alertView.addAction(action)
        alertView.addAction(cancel)
        
        present(alertView, animated: true, completion: {
            pickerView.frame.size.width = alertView.view.frame.size.width
        })
    }
    
    func okayAction() {
        // button
        if self.viewModel.getUserStatus() == 0 || self.viewModel.getUserStatus() == 3  {
            if viewModel.getSessionType() == "Fitness" || viewModel.getSessionType() == "Fitness2" {
                UserDefaults.standard.set(self.ageFitnessLabel.text!, forKey: "ageString")
                UserDefaults.standard.set(self.weightFitnessLabel.text!, forKey: "weightString")
//                UserDefaults.standard.set(self.sexFitnessLabel.text!, forKey: "sexString")
                let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
                if selectedLanguage == "ja" {
                   if self.sexFitnessLabel.text! == "男" {
                        UserDefaults.standard.set("Male", forKey: "sexString")
                   } else {
                        UserDefaults.standard.set("Female", forKey: "sexString")
                   }
                } else {
                    UserDefaults.standard.set(self.sexFitnessLabel.text!, forKey: "sexString")
                }
                
                
                UserDefaults.standard.set(self.passCodeFitnessLabel.text!, forKey: "userEnteredPasscode")
                
                join_session_fitness()
                
            } else {
                if self.sessionIsPrivate() {
                    UserDefaults.standard.set(self.passCodeCorporateLabel.text!, forKey: "userEnteredPasscode")
                }
                
                self.join_session()
            }
            
        } else if self.viewModel.getUserStatus() == 1 || self.viewModel.getUserStatus() == 2 {
            // leave
            if viewModel.getSessionType() == "Fitness" || viewModel.getSessionType() == "Fitness2"  {
                UserDefaults.standard.set(self.ageFitnessLabel.text!, forKey: "ageString")
                UserDefaults.standard.set(self.weightFitnessLabel.text!, forKey: "weightString")
//                UserDefaults.standard.set(self.sexFitnessLabel.text!, forKey: "sexString")
                UserDefaults.standard.set(self.passCodeFitnessLabel.text!, forKey: "userEnteredPasscode")
                
                let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
                   if selectedLanguage == "ja" {
                      if self.sexFitnessLabel.text! == "男" {
                           UserDefaults.standard.set("Male", forKey: "sexString")
                      } else {
                           UserDefaults.standard.set("Female", forKey: "sexString")
                      }
                 } else {
                    UserDefaults.standard.set(self.sexFitnessLabel.text!, forKey: "sexString")
                 }
                
                
                join_session_fitness()
                
            } else {
                if self.sessionIsPrivate() {
                    UserDefaults.standard.set(self.passCodeCorporateLabel.text!, forKey: "userEnteredPasscode")
                }
                
                self.join_session()
            }
        }
    }
}

extension ViewSessionViewController {
    func setUpTimeStartTextView() {
        switch viewModel.numberOfTime() {
        case 0:
            self.view_header_height.constant = 150.0
            self.session_time_start_label_height.constant = 0.0
        case 1:
            self.view_header_height.constant = 150.0
            self.session_time_start_label_height.constant = 26.0
        case 2:
            self.view_header_height.constant = 150.0
            self.session_time_start_label_height.constant = 52.0
        case 3:
            self.view_header_height.constant = 130.0
            self.session_time_start_label_height.constant = 70.0
        case 4:
            self.view_header_height.constant = 120.0
            self.session_time_start_label_height.constant = 70.0
        case 5:
            self.view_header_height.constant = 110.0
            self.session_time_start_label_height.constant = 100.0
        case 6:
            self.view_header_height.constant = 100.0
            self.session_time_start_label_height.constant = 100.0
        case 7:
            self.view_header_height.constant = 80.0
            self.session_time_start_label_height.constant = 140.0
        case 8:
            self.view_header_height.constant = 65.0
            self.session_time_start_label_height.constant = 140.0
        case 9:
            self.view_header_height.constant = 45.0
            self.session_time_start_label_height.constant = 140.0
        case 10:
            self.view_header_height.constant = 30.0
            self.session_time_start_label_height.constant = 140.0
        default:
            break
        }
    }
}
