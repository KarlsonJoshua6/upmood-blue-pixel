//
//  IntervalDateAndTimeLIstViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 23/07/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

struct cellData {
    var opened = Bool()
    var title = String()
    var sectionData = [String]()
}

class IntervalDateAndTimeListViewControllerCell: UITableViewCell {
    @IBOutlet weak var timeAndDateLabel: UILabel!
}

class IntervalDateAndTimeLIstViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var tableViewData = [cellData]()
    
    var monday = [String]()
    var tuesday = [String]()
    var wednesday = [String]()
    var thursday = [String]()
    var friday = [String]()
    var saturday = [String]()
    var sunday = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableViewData = [cellData(opened: false, title: NSLocalizedString("Monday", comment: ""), sectionData: monday),
                         cellData(opened: false, title: NSLocalizedString("Tuesday", comment: ""), sectionData: tuesday),
                         cellData(opened: false, title: NSLocalizedString("Wednesday", comment: ""), sectionData: wednesday),
                         cellData(opened: false, title: NSLocalizedString("Thursday", comment: ""), sectionData: thursday),
                         cellData(opened: false, title: NSLocalizedString("Friday", comment: ""), sectionData: friday),
                         cellData(opened: false, title: NSLocalizedString("Saturday", comment: ""), sectionData: saturday),
                         cellData(opened: false, title: NSLocalizedString("Sunday", comment: ""), sectionData: sunday)]
    }

    @IBAction func done(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension IntervalDateAndTimeLIstViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "IntervalDateAndTimeListViewControllerCell", for: indexPath) as? IntervalDateAndTimeListViewControllerCell else { return UITableViewCell() }
            cell.timeAndDateLabel.textColor = hexStringToUIColor(hex: Constants.Color.PRIMARY_COLOR)
            cell.timeAndDateLabel.text = tableViewData[indexPath.section].title
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "IntervalDateAndTimeListViewControllerCell", for: indexPath) as? IntervalDateAndTimeListViewControllerCell else { return UITableViewCell() }
            cell.timeAndDateLabel.textColor = UIColor.gray
            
            cell.timeAndDateLabel.text = tableViewData[indexPath.section].sectionData[indexPath.row - 1]
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewData[section].opened == true {
            return tableViewData[section].sectionData.count + 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableViewData[indexPath.section].opened == true {
            tableViewData[indexPath.section].opened = false
            let section = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(section, with: .none)
        } else {
            tableViewData[indexPath.section].opened = true
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
}
