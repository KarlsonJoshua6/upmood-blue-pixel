//
//  ViewSessionViewModel+.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 22/05/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation

extension ViewSessionViewModel {
    func getDayInterval() -> String {
        switch self.start.count {
        case 0:
            return ""
        case 1:
            return "\(start[0]) to \(end[0])"
        case 2:
            return "\(start[0]) to \(end[0]), \(start[1]) to \(end[1])"
        case 3:
            return "\(start[0]) to \(end[0]), \(start[1]) to \(end[1]), \(start[2]) to \(end[2])"
        case 4:
            return "\(start[0]) to \(end[0]), \(start[1]) to \(end[1]), \(start[2]) to \(end[2]), \(start[3]) to \(end[3])"
        case 5:
            return "\(start[0]) to \(end[0]), \(start[1]) to \(end[1]), \(start[2]) to \(end[2]), \(start[3]) to \(end[3]), \(start[4]) to \(end[4])"
        case 6:
            return "\(start[0]) to \(end[0]), \(start[1]) to \(end[1]), \(start[2]) to \(end[2]), \(start[3]) to \(end[3]), \(start[4]) to \(end[4]), \(start[5]) to \(end[5])"
        default:
            return "\(start[0]) to \(end[0]), \(start[1]) to \(end[1]), \(start[2]) to \(end[2]), \(start[3]) to \(end[3]), \(start[4]) to \(end[4]), \(start[5]) to \(end[5])..."
        }
    }
}
