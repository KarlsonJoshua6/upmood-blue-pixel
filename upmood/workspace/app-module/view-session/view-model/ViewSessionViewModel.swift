//
//  ViewSessionViewModel.swift
//  workspacec
//
//  Created by John Paul Manoza on 27/04/2019.
//  Copyright © 2019 Joseph Mikko Manoza. All rights reserved.
//

import UIKit

class ViewSessionViewModel: NSObject {
    
    var session: NSDictionary?
    var day = [String]()
    var start = [String]()
    var end = [String]()
    var endDateStr = ""
    var newDay = [String]()
    
    // days
    var mondayArr = [String]()
    var tuesdayArr = [String]()
    var wednesdayArr = [String]()
    var thursdayArr = [String]()
    var fridayArr = [String]()
    var saturdayArr = [String]()
    var sundayArr = [String]()
    
    var startTimeArr = [String]()
    var endTimeArr = [String]()
    
    @IBOutlet var viewSessionService: ViewSessionService!
    
    func fetchViewSession(completion: @escaping () -> ()) {
        viewSessionService.fetchViewSession { [weak self] result in
            self?.session = result
            completion()
        }
    }
    
    func joinViewSession(completion: @escaping () -> ()) {
        viewSessionService.joinViewSession { [weak self] result in
            self?.session = result
            completion()
        }
    }
    
    func joinFitnessSession(completion: @escaping () -> ()) {
        viewSessionService.joinFitnessSession { [weak self] result in
            self?.session = result
            completion()
        }
    }
    
    func leaveJoinedSession(completion: @escaping () -> ()) {
        viewSessionService.leaveViewSession { [weak self] result in
            self?.session = result
            completion()
        }
    }
    
    func getResponseMessage() -> String {
        return session?.value(forKeyPath: "message") as? String ?? ""
    }
    
    func getResponseStatusCode() -> Int {
        return session?.value(forKeyPath: "statusCode") as? Int ?? 0
    }
    
    func getSessionId() -> Int {
        return session?.value(forKeyPath: "id") as? Int ?? 0
    }
    
    func getUserId() -> Int {
        return session?.value(forKeyPath: "user_id") as? Int ?? 0
    }
    
    func getSessionType() -> String {
        return session?.value(forKeyPath: "session_type") as? String ?? ""
    }
    
    func getSessionName() -> String {
       return session?.value(forKeyPath: "session_name") as? String ?? ""
    }
    
    func getSessionStatus() -> Int {
        return session?.value(forKeyPath: "status") as? Int ?? 0
    }
    
    func getSessionDescription() -> String {
        return session?.value(forKeyPath: "description") as? String ?? ""
    }
    
    func getSessionPrivacy() -> Int {
        return session?.value(forKeyPath: "session_privacy") as? Int ?? 0
    }
    
    func getGatheringType() -> String {
        return session?.value(forKeyPath: "gathering_type") as? String ?? ""
    }
    
    func getActiveUser() -> String {
        if session?.value(forKeyPath: "active_users") as? Int ?? 0 == 0 {
            return "None"
        }
        
        return "\(session?.value(forKeyPath: "active_users") as? Int ?? 0)"
    }
    
    func getCompanyName() -> String {
        return session?.value(forKeyPath: "company_name") as? String ?? ""
    }
    
    func getErrorMessage() -> String {
        return session?.value(forKeyPath: "message") as? String ?? ""
    }
    
    func getUserStatus() -> Int {
        return session?.value(forKeyPath: "user_status") as? Int ?? 0
    }
    
    func getSessionStart() -> String {
        return session?.value(forKeyPath: "session_start") as? String ?? ""
    }
    
    func getSessionEnd() -> String {
        return session?.value(forKeyPath: "session_end") as? String ?? ""
    }
    
    func numberOfItemAtIndexPath(section: Int) -> Int {
        getTimeIntervalArr()
        return self.start.count
    }
    
    func getJoinedDate() -> String {
        let joinedDateStr = session?.value(forKeyPath: "joined_date") as? String ?? ""
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let joined = df.date(from: joinedDateStr)
        
        df.dateFormat = "MMM dd, yyyy hh:mm a"
        let joined_date_string = df.string(from: joined!)
        
        return joined_date_string
    }
    
    func getDate() -> String {
        
        if let daytime_interval = session?.value(forKeyPath: "distinct_days") as? [[String: Any]] {
            for daytime in daytime_interval {
                let startTime = daytime["start_time"] as? String ?? ""
                let endTime = daytime["end_time"] as? String ?? ""
                self.endTimeArr.append(endTime)
                self.startTimeArr.append(startTime)
            }
        }

        if getGatheringType() == "Interval" {
            
            // parse start and end date
            let startDateStrFromDic = getSessionStart() + " " + self.startTimeArr[0]
            let endDateStrFromDic = getSessionEnd() + " " + self.endTimeArr.last!
            
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let startDateStr = df.date(from: startDateStrFromDic)
            let endDateStr = df.date(from: endDateStrFromDic)
            
            df.dateFormat = "MMM dd, yyyy"
            let start = df.string(from: startDateStr!)
            let end = df.string(from: endDateStr!)
            
            // parse interval_days
            day.removeAll()
            if let interval_days = session?.value(forKeyPath: "interval_days") as? [String] {
                for days in interval_days {
                    day.append(days)
                }
            }
           
            self.endDateStr = end
            return "\(start) - \(end) - \(day.joined(separator: ", "))"
            
        } else {
            
            let startDateStrFromDic = getSessionStart()
            let endDateStrFromDic = getSessionEnd()
            
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let startDateStr = df.date(from: startDateStrFromDic)
            let endDateStr = df.date(from: endDateStrFromDic)
            
            df.dateFormat = "MMM dd, yyyy hh:mm a"
            let start = df.string(from: startDateStr!)
            let end = df.string(from: endDateStr!)
            
            self.endDateStr = end
            return "\(start) - \(end)"
        }
    }
    
    func numberOfTime() -> Int {
        getTimeIntervalArr()
        return start.count
    }
    
    func getTodayString() -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        
        let currentDateStr = formatter.string(from: Date())
        print(currentDateStr)
        return currentDateStr
    }
    
    func getTime() -> String {
        getTimeIntervalArr()
        return "\(getDayInterval())"
    }
    
    func getMessageDataOnSuccessJoined() -> String {
        return "\(self.endDateStr) \(self.end.last ?? "")"
    }
    
    func timeItemAtIndexPath(indexPath: IndexPath) -> String {
        getTimeIntervalArr()
        return "\(self.start[indexPath.row]) to \(self.end[indexPath.row])"
    }
    
    func getMondayIntervalSched() -> [String] {
        getTimeIntervalArr()
        return mondayArr
    }
    
    func getTuesdayIntervalSched() -> [String] {
        getTimeIntervalArr()
        return tuesdayArr
    }
    
    func getWednesdayIntervalSched() -> [String] {
        getTimeIntervalArr()
        return wednesdayArr
    }
    
    func getThursdayIntervalSched() -> [String] {
        getTimeIntervalArr()
        return thursdayArr
    }
    
    func getFridayIntervalSched() -> [String] {
        getTimeIntervalArr()
        return fridayArr
    }
    
    func getSaturdayIntervalSched() -> [String] {
        getTimeIntervalArr()
        return saturdayArr
    }
    
    func getSundayIntervalSched() -> [String] {
        getTimeIntervalArr()
        return sundayArr
    }
    
    func getTimeIntervalArr() {
        start.removeAll()
        end.removeAll()
        
        mondayArr.removeAll()
        tuesdayArr.removeAll()
        wednesdayArr.removeAll()
        thursdayArr.removeAll()
        fridayArr.removeAll()
        saturdayArr.removeAll()
        sundayArr.removeAll()
        
        if let daytime_interval = session?.value(forKeyPath: "distinct_days") as? [[String: Any]] {
            for daytime in daytime_interval {
                let day = daytime["day"] as? String ?? ""
                
                switch day {
                case "Monday":
                    let startTime = daytime["start_time"] as? String ?? ""
                    let endTime = daytime["end_time"] as? String ?? ""
                    
                    let inFormatter = DateFormatter()
                    inFormatter.locale = Locale.current
                    inFormatter.dateFormat = "HH:mm:ss"
                    
                    let outFormatter = DateFormatter()
                    outFormatter.locale = Locale.current
                    outFormatter.dateFormat = "hh:mm a"
                    
                    let inStr1 = startTime
                    let inStr2 = endTime
                    let date1 = inFormatter.date(from: inStr1)
                    let date2 = inFormatter.date(from: inStr2)
                    let startTimeOutPut = outFormatter.string(from: date1!)
                    let endTimeOutPut = outFormatter.string(from: date2!)
                    
                    let mondaySched = "\(startTimeOutPut) - \(endTimeOutPut)"
                    self.mondayArr.append(mondaySched)
                    
                case "Tuesday":
                    let startTime = daytime["start_time"] as? String ?? ""
                    let endTime = daytime["end_time"] as? String ?? ""
                    
                    let inFormatter = DateFormatter()
                    inFormatter.locale = Locale.current
                    inFormatter.dateFormat = "HH:mm:ss"
                    
                    let outFormatter = DateFormatter()
                    outFormatter.locale = Locale.current
                    outFormatter.dateFormat = "hh:mm a"
                    
                    let inStr1 = startTime
                    let inStr2 = endTime
                    let date1 = inFormatter.date(from: inStr1)
                    let date2 = inFormatter.date(from: inStr2)
                    let startTimeOutPut = outFormatter.string(from: date1!)
                    let endTimeOutPut = outFormatter.string(from: date2!)
                    
                    let tuesdaySched = "\(startTimeOutPut) - \(endTimeOutPut)"
                    self.tuesdayArr.append(tuesdaySched)
                case "Wednesday":
                    let startTime = daytime["start_time"] as? String ?? ""
                    let endTime = daytime["end_time"] as? String ?? ""
                    
                    let inFormatter = DateFormatter()
                    inFormatter.locale = Locale.current
                    inFormatter.dateFormat = "HH:mm:ss"
                    
                    let outFormatter = DateFormatter()
                    outFormatter.locale = Locale.current
                    outFormatter.dateFormat = "hh:mm a"
                    
                    let inStr1 = startTime
                    let inStr2 = endTime
                    let date1 = inFormatter.date(from: inStr1)
                    let date2 = inFormatter.date(from: inStr2)
                    let startTimeOutPut = outFormatter.string(from: date1!)
                    let endTimeOutPut = outFormatter.string(from: date2!)
                    
                    let wednesdaySched = "\(startTimeOutPut) - \(endTimeOutPut)"
                    self.wednesdayArr.append(wednesdaySched)
                    
                case "Thursday":
                    let startTime = daytime["start_time"] as? String ?? ""
                    let endTime = daytime["end_time"] as? String ?? ""
                    
                    let inFormatter = DateFormatter()
                    inFormatter.locale = Locale.current
                    inFormatter.dateFormat = "HH:mm:ss"
                    
                    let outFormatter = DateFormatter()
                    outFormatter.locale = Locale.current
                    outFormatter.dateFormat = "hh:mm a"
                    
                    let inStr1 = startTime
                    let inStr2 = endTime
                    let date1 = inFormatter.date(from: inStr1)
                    let date2 = inFormatter.date(from: inStr2)
                    let startTimeOutPut = outFormatter.string(from: date1!)
                    let endTimeOutPut = outFormatter.string(from: date2!)
                    
                    let thursdaySched = "\(startTimeOutPut) - \(endTimeOutPut)"
                    self.thursdayArr.append(thursdaySched)
                case "Friday":
                    let startTime = daytime["start_time"] as? String ?? ""
                    let endTime = daytime["end_time"] as? String ?? ""
                    
                    let inFormatter = DateFormatter()
                    inFormatter.locale = Locale.current
                    inFormatter.dateFormat = "HH:mm:ss"
                    
                    let outFormatter = DateFormatter()
                    outFormatter.locale = Locale.current
                    outFormatter.dateFormat = "hh:mm a"
                    
                    let inStr1 = startTime
                    let inStr2 = endTime
                    let date1 = inFormatter.date(from: inStr1)
                    let date2 = inFormatter.date(from: inStr2)
                    let startTimeOutPut = outFormatter.string(from: date1!)
                    let endTimeOutPut = outFormatter.string(from: date2!)
                    
                    let fridaySched = "\(startTimeOutPut) - \(endTimeOutPut)"
                    self.fridayArr.append(fridaySched)
                case "Saturday":
                    let startTime = daytime["start_time"] as? String ?? ""
                    let endTime = daytime["end_time"] as? String ?? ""
                    
                    let inFormatter = DateFormatter()
                    inFormatter.locale = Locale.current
                    inFormatter.dateFormat = "HH:mm:ss"
                    
                    let outFormatter = DateFormatter()
                    outFormatter.locale = Locale.current
                    outFormatter.dateFormat = "hh:mm a"
                    
                    let inStr1 = startTime
                    let inStr2 = endTime
                    let date1 = inFormatter.date(from: inStr1)
                    let date2 = inFormatter.date(from: inStr2)
                    let startTimeOutPut = outFormatter.string(from: date1!)
                    let endTimeOutPut = outFormatter.string(from: date2!)
                    
                    let saturdaySched = "\(startTimeOutPut) - \(endTimeOutPut)"
                    self.saturdayArr.append(saturdaySched)
                case "Sunday":
                    let startTime = daytime["start_time"] as? String ?? ""
                    let endTime = daytime["end_time"] as? String ?? ""
                    
                    let inFormatter = DateFormatter()
                    inFormatter.locale = Locale.current
                    inFormatter.dateFormat = "HH:mm:ss"
                    
                    let outFormatter = DateFormatter()
                    outFormatter.locale = Locale.current
                    outFormatter.dateFormat = "hh:mm a"
                    
                    let inStr1 = startTime
                    let inStr2 = endTime
                    let date1 = inFormatter.date(from: inStr1)
                    let date2 = inFormatter.date(from: inStr2)
                    let startTimeOutPut = outFormatter.string(from: date1!)
                    let endTimeOutPut = outFormatter.string(from: date2!)
                    
                    let sundaySched = "\(startTimeOutPut) - \(endTimeOutPut)"
                    self.sundayArr.append(sundaySched)
                default:
                    break
                }
            }
        }
    }
}
