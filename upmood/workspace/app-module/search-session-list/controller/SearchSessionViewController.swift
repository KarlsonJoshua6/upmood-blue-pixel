//
//  SearchSessionViewController.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 30/04/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//
import RealmSwift
import UIKit

let tableViewId = "SearchTableViewCell"
let searchSegueId = "showViewSessionFromSearch"

class SearchRecentTableViewCell: UITableViewCell {
    @IBOutlet weak var searchText: UILabel!
}

class SearchTableViewCell: UITableViewCell {
    @IBOutlet weak var sesion_name_label: UILabel!
    @IBOutlet weak var session_company_name_label: UILabel!
}

class SearchSessionViewController: UIViewController {
    
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var recentSearchTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet var viewModelSearchResult: SearchSessionViewModel!
    @IBOutlet var viewModelRecentSearch: RecentSearchViewModel!
    
    var realm: Realm!
    var objectArray: Results<Item> {
        get {
            return realm.objects(Item.self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        realm = try! Realm()
        initUI()
    }
    
    private func initUI() {
        searchTableView.delegate = self
        searchTableView.dataSource = self
        
        recentSearchTableView.delegate = self
        recentSearchTableView.dataSource = self
        
        viewModelRecentSearch.fetchRecentSearch {
            DispatchQueue.main.async { [weak self] in
                self?.searchTableView.isHidden = true
                self?.recentSearchTableView.isHidden = false
                self?.recentSearchTableView.reloadData()
            }
        }
        
        searchBar.delegate = self
    }
    
    private func reloadData() {
        viewModelSearchResult.reloadData()
    }
    
    private func saveRecentSearchKeyWord(searchKeyword: String) {

        if (realm.object(ofType: Item.self, forPrimaryKey: searchKeyword) != nil) {
            // do nothing
        } else {
            let item = Item()
            item.searchText = searchKeyword
            RealmHelper.saveObject(object: item)
        }
    }
    
    @IBAction func deleteSaveRecentSearchKeyword(_ sender: UIButton) {
        try? realm.write { [weak self] in
            self?.realm.deleteAll()
            self?.recentSearchTableView.reloadData()
        }
    }
}

// MARK: - UISearchBarDelegate

extension SearchSessionViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        guard let firstSubview = searchBar.subviews.first else { return }
        firstSubview.subviews.forEach { ($0 as? UITextField)?.clearButtonMode = .never }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.searchBar.endEditing(true)
        self.recentSearchTableView.isHidden = false
        self.searchTableView.isHidden = true
        self.searchBar.text = ""
        self.recentSearchTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == self.searchBar {
            if searchBar.returnKeyType == .search {
                
                viewModelSearchResult.fetchSearchSessionList(searchKeyword: searchBar.text ?? "".lowercased()) {
                    DispatchQueue.main.async { [weak self] in
                        
                        if self?.viewModelSearchResult.getErrorCode() == 204 {
                            self?.presentDismissableAlertController(title: NSLocalizedString(Constants.MessageDialog.errorTitle, comment: ""), message: "No session found.")
                            self?.searchTableView.isHidden = true
                            self?.recentSearchTableView.isHidden = false
                            self?.recentSearchTableView.reloadData()
                        } else {
                            self?.searchTableView.isHidden = false
                            self?.recentSearchTableView.isHidden = true
                            self?.searchTableView.reloadData()
                        }
                    }
                }
                
                self.saveRecentSearchKeyWord(searchKeyword: searchBar.text ?? "".lowercased())
                
                searchBar.setShowsCancelButton(false, animated: true)
                searchBar.resignFirstResponder()
            }
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension SearchSessionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.recentSearchTableView {
            return objectArray.count
        }
        
        return viewModelSearchResult.numberOfItemAtIndexPath(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.recentSearchTableView {
            let recentSearchResultCell = tableView.dequeueReusableCell(withIdentifier: "SearchRecentTableViewCell", for: indexPath) as! SearchRecentTableViewCell
            recentSearchResultCell.searchText.text = objectArray[indexPath.row].searchText
            return recentSearchResultCell
        }
        
        let searchResultCell = tableView.dequeueReusableCell(withIdentifier: tableViewId, for: indexPath) as! SearchTableViewCell
        searchResultCell.sesion_name_label.text = viewModelSearchResult.sessionNameAtIndexPath(indexPath: indexPath)
        searchResultCell.session_company_name_label.text = viewModelSearchResult.sessionCompanyNameAtIndexPath(indexPath: indexPath)
        return searchResultCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.recentSearchTableView {
            return 44.0
        }
        
        return 77.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.recentSearchTableView {
            self.searchBar.text = objectArray[indexPath.row].searchText
            self.searchBar.becomeFirstResponder()
        } else {
            UserDefaults.standard.set(viewModelSearchResult.sessionIdAtIndexPath(indexPath: indexPath), forKey: "idStr")
            performSegue(withIdentifier: searchSegueId, sender: self)
        }
    }
}
