//
//  SearchSessionViewModel.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 30/04/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class SearchSessionViewModel: NSObject {
    
    @IBOutlet var searchSessionService: SearchSessionService!
    var session: [NSDictionary]?
    
    func fetchSearchSessionList(searchKeyword: String, completion: @escaping () -> ()) {
        searchSessionService.fetchSearchSessionList(searchKeyword: searchKeyword) { [weak self] dict in
            self?.session = dict
            completion()
        }
    }
    
    func reloadData() {
        session?.removeAll()
    }
    
    func numberOfItemAtIndexPath(section: Int) -> Int {
        return session?.count ?? 0
    }
    
    func sessionIdAtIndexPath(indexPath: IndexPath) -> Int {
        return session?[indexPath.row].value(forKeyPath: "id") as? Int ?? 0
    }
    
    func sessionCompanyNameAtIndexPath(indexPath: IndexPath) -> String {
        return session?[indexPath.row].value(forKeyPath: "company_name") as? String ?? ""
    }
    
    func sessionNameAtIndexPath(indexPath: IndexPath) -> String {
        return session?[indexPath.row].value(forKeyPath: "session_name") as? String ?? ""
    }
    
    func getErrorCode() -> Int {
        return session?[0].value(forKeyPath: "status") as? Int ?? 0
    }
    
    func getLocalizeErrorString() -> String {
        return session?[0].value(forKeyPath: "status_failure") as? String ?? ""
    }
}
