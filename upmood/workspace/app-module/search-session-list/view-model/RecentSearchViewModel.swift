//
//  RecentSearchViewModel.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 03/05/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import UIKit

class RecentSearchViewModel: NSObject {
    
    func fetchRecentSearch(completion: @escaping () -> ()) {
        completion()
    }
    
    func numberOfItemInSearchKeywordAtIndexPath(section: Int) -> Int {
        return 0
    }
    
    func searchKeywordAtIndexPath(indexPath: IndexPath) -> String {
        return ""
    }
}
