
//
//  MySessionLIstViewController.swift
//  workspacec
//
//  Created by John Paul Manoza on 28/04/2019.
//  Copyright © 2019 Joseph Mikko Manoza. All rights reserved.
//

import UIKit

let cellId = "MySessionListCell"

class MySessionListCell: UITableViewCell {
    @IBOutlet weak var past_session_name_label: UILabel!
    @IBOutlet weak var past_session_created_date_label: UILabel!
    @IBOutlet weak var past_session_number_of_active_user_label: UILabel!
    @IBOutlet weak var past_session_online_indicator_imageVIew: UIImageView!
    @IBOutlet weak var time_remaining_label: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var no_session_found_label: UILabel!
    
    override func awakeFromNib() {
        self.no_session_found_label.text = ""
        self.cardView.isHidden = false
    }
}

class MySessionLIstViewController: UIViewController {

    @IBOutlet weak var upConstraint: NSLayoutConstraint!
    @IBOutlet var viewModel: MySessionLIstViewModel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentSessionView: UIView!
    
    // current session outlets
    @IBOutlet weak var current_session_name_label: UILabel!
    @IBOutlet weak var current_session_created_date_label: UILabel!
    @IBOutlet weak var current_session_number_of_active_user_label: UILabel!
    @IBOutlet weak var currentSessionButton: UIButton!
    @IBOutlet weak var current_time_remaining_label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        
        hideCurrentSession()
        
        // current_session
        viewModel.fetchCurrentSession {
            DispatchQueue.main.async { [weak self] in
                self?.currentSession()
            }
        }
        
        // past_session
        viewModel.fetchPastSession {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
        viewDidLoad()
    }
    
    private func initUI() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func currentSession() {
        if viewModel.currentSessionName().isEmpty {
            hideCurrentSession()
        } else {
            unHideCurrentSession()
            self.current_session_name_label.text = viewModel.currentSessionName()
            self.current_session_created_date_label.text = viewModel.currentSessionCreatedDate()
            self.current_session_number_of_active_user_label.text = viewModel.currentActiveUser()
            self.current_time_remaining_label.text = viewModel.currentSessionRemainingTime()
        }
    }
    
    private func hideCurrentSession() {
        upConstraint.constant = 0
        currentSessionButton.isHidden = true
        currentSessionView.isHidden = true
    }
    
    private func unHideCurrentSession() {
        upConstraint.constant = 128
        currentSessionButton.isHidden = false
        currentSessionView.isHidden = false
    }
    
    @IBAction func didSelectCurrentSession(_ sender: UIButton) {
        UserDefaults.standard.set("\(viewModel.currentSessionId())", forKey: "idStr")
        performSegue(withIdentifier: segueId, sender: self)
    }
    
    @IBAction func unwindToMySession(_ sender: UIStoryboardSegue) {
        self.dismiss(animated: true, completion: nil)
        DispatchQueue.main.async { [weak self] in
            self?.viewDidLoad()
        }
    }
}

extension MySessionLIstViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if viewModel.numberOfItemInPastSessionListAtIndexPath(section: section) == 0 {
            return 1
        }
        
        return viewModel.numberOfItemInPastSessionListAtIndexPath(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MySessionListCell
        
        if viewModel.numberOfItemInPastSessionListAtIndexPath(section: 1) == 0 {
//            cell.no_session_found_label.text = "No past session found."
            cell.cardView.isHidden = true
        } else {
            cell.no_session_found_label.text = ""
            cell.cardView.isHidden = false
            
            // past session
            
            cell.past_session_name_label.text = viewModel.pastSessionNameItemAtIndexPath(indexPath: indexPath)
            cell.past_session_created_date_label.text = viewModel.pastSessionCreatedDateAtIndexPath(indexPath: indexPath)
            cell.past_session_number_of_active_user_label.text = viewModel.pastActiveUserItemAtIndexPath(indexPath: indexPath)
            cell.time_remaining_label.text = viewModel.pastSessionRemainingTime(indexPath: indexPath)
            
            if viewModel.pastSessionStatus(indexPath: indexPath) == 0 {
                cell.past_session_online_indicator_imageVIew.image = UIImage(named: "ic_is_offline_indicator")
                cell.time_remaining_label.text = NSLocalizedString("End", comment: "")
            } else if viewModel.pastSessionStatus(indexPath: indexPath) == 1 {
                cell.past_session_online_indicator_imageVIew.image = UIImage(named: "ic_is_online_dot")
            } else if viewModel.pastSessionStatus(indexPath: indexPath) == 2 {
                cell.past_session_online_indicator_imageVIew.image = UIImage(named: "ic_is_paused_dot")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if viewModel.numberOfItemInPastSessionListAtIndexPath(section: 1) == 0 {
            return 60.0
        }
        
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.set("\(viewModel.pastSessionIdItemAtIndexPath(indexPath: indexPath))", forKey: "idStr")
        UserDefaults.standard.set(false, forKey: "didUserClickOnPublicSessionList")
        performSegue(withIdentifier: segueId, sender: self)
    }
}
