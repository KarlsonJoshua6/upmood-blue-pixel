//
//  PublicSessionListViewModel.swift
//  workspacec
//
//  Created by John Paul Manoza on 27/04/2019.
//  Copyright © 2019 Joseph Mikko Manoza. All rights reserved.
//

import UIKit

class PublicSessionListViewModel: NSObject {

    @IBOutlet var publicSessionListService: PublicSessionListService!
    var session: [NSDictionary]?
    var startTimeArr = [String]()
    var endTimeArr = [String]()
    
    func fetchPublicSessionList(completion: @escaping () -> ()) {
        publicSessionListService.fetchPublicSessionList(completion: { dictionary in
            self.session = dictionary
            print("test: \(String(describing: self.session))")
            completion()
        })
    }
    
    func numberOfItemAtIndexPath(section: Int) -> Int {
        return session?.count ?? 0
    }
    
    func sessionIdItemAtIndexPath(indexPath: IndexPath) -> Int {
        return session?[indexPath.row].value(forKeyPath: "id") as? Int ?? 0
    }
    
    func sessionNameItemAtIndexPath(indexPath: IndexPath) -> String {
        return session?[indexPath.row].value(forKeyPath: "session_name") as? String ?? ""
    }
    
    func companyNameItemAtIndexPath(indexPath: IndexPath) -> String {
        return "by \(session?[indexPath.row].value(forKeyPath: "company_name") as? String ?? "")"
    }
    
    func activeUserItemAtIndexPath(indexPath: IndexPath) -> String {
        if session?[indexPath.row].value(forKeyPath: "active_users") as? Int ?? 0 == 0 {
            return "None"
        }
        
        return "\(session?[indexPath.row].value(forKeyPath: "active_users") as? Int ?? 0)"
    }
    
    func sessionCreatedItemAtIndexPath(indexPath: IndexPath) -> String {
        return "\(session?[indexPath.row].value(forKeyPath: "created_at") as? String ?? "")"
    }
    
    func createdDateAtIndexPath(indexPath: IndexPath) -> String {
        let dateString = session?[indexPath.row].value(forKeyPath: "created_at") as? String ?? ""
        let df = DateFormatter()
        
        if UserDefaults.standard.string(forKey: "selectedLanguage") == "en" {
            df.locale = Locale(identifier: "ja_JP")
        } else {
            df.locale = Locale(identifier: "en")
        }
        
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let yourDate = df.date(from: dateString)
        df.dateFormat = "MMM dd, yyyy"
        let dateStr = df.string(from: yourDate!)
        return "\(TranslationConstants.SESSION_CREATED.LOCALIZED): \(convertDateStr(dateStr: dateStr))"
    }
    
    func sessionStatus(indexPath: IndexPath) -> Int {
        return session?[indexPath.row].value(forKeyPath: "status") as? Int ?? 0
    }
    
    func setRemainingDay(indexPath: IndexPath) -> String  {
        let dateStr = session?[indexPath.row].value(forKeyPath: "session_end") as? String ?? ""
        let dateStr2 = session?[indexPath.row].value(forKeyPath: "session_start") as? String ?? ""
        let df = DateFormatter()
        
        if UserDefaults.standard.string(forKey: "selectedLanguage") == "en" {
            df.locale = Locale(identifier: "ja_JP")
        } else {
            df.locale = Locale(identifier: "en")
        }
        
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let startDate = df.date(from: dateStr2) ?? Date()
        let endDate = df.date(from: dateStr) ?? Date()
        
        let form = DateComponentsFormatter()
        form.maximumUnitCount = 2
        form.unitsStyle = .full
        form.allowedUnits = [.year, .month, .day]
        
        let stringRemaining = form.string(from: startDate, to: endDate)
        
        print("Date today: \(startDate), endDate: \(endDate)")
        
        return "\(stringRemaining ?? "") Remaining"
    }
    
    func sessionStartAtIndexPath(indexPath: IndexPath) -> String {
        return session?[indexPath.row].value(forKeyPath: "session_start") as? String ?? ""
    }
    
    func sessionEndAtIndexPath(indexPath: IndexPath) -> String {
        return session?[indexPath.row].value(forKeyPath: "session_end") as? String ?? ""
    }
    
    func sessionGatheringType(indexPath: IndexPath) -> String {
        return session?[indexPath.row].value(forKeyPath: "gathering_type") as? String ?? ""
    }
    
    func sessionRemainingTime(indexPath: IndexPath) -> String {
        self.endTimeArr.removeAll()
        self.startTimeArr.removeAll()
        if let daytime_interval = session?[indexPath.row].value(forKeyPath: "distinct_days") as? [[String: Any]] {
            for daytime in daytime_interval {
                let startTime = daytime["start_time"] as? String ?? ""
                let endTime = daytime["end_time"] as? String ?? ""
                self.endTimeArr.append(endTime)
                self.startTimeArr.append(startTime)
            }
        }
        
        if sessionGatheringType(indexPath: indexPath) == "Interval" {
            return sessionElapsedTimeInterval(indexPath: indexPath)
        } else {
            
            if checkIfStarted(startTime: sessionStartAtIndexPath(indexPath: indexPath)) {
                return sessionElapsedTimeContinuos(endTime: sessionEndAtIndexPath(indexPath: indexPath))
            } else {
                return "Not yet started"
            }
        }
    }
    
    private func sessionElapsedTimeInterval(indexPath: IndexPath) -> String {
        
        let start_interval = sessionStartAtIndexPath(indexPath: indexPath) + " " + self.startTimeArr[0]
        let end_interval = sessionEndAtIndexPath(indexPath: indexPath) + " " + self.endTimeArr.last!
    
        if checkIfStarted(startTime: start_interval) {
            return sessionElapsedTimeContinuos(endTime: end_interval)
        } else {
            return "Not yet started"
        }
    }
    
    private func sessionElapsedTimeContinuos(endTime: String) -> String {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = TimeZone.current
        df.locale = Locale.current
        
        let date2 = df.date(from: endTime)
        
        if date2 == nil {
             return "None"
        } else {
            
            let userCalendar = Calendar.current
            let date = Date()
            let components = userCalendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: date)
            let components2 = userCalendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: date2!)
            var localTimeZoneName: String { return TimeZone.current.identifier }
            let currentDate = userCalendar.date(from: components)!
        
            var eventDateComponents = DateComponents()
            eventDateComponents.year = components2.year!
            eventDateComponents.month = components2.month!
            eventDateComponents.day = components2.day!
            eventDateComponents.hour = components2.hour
            eventDateComponents.minute = components2.minute!
            eventDateComponents.second = components2.second!
            eventDateComponents.timeZone = TimeZone(abbreviation: localTimeZoneName)
             
            let eventDate = userCalendar.date(from: eventDateComponents)!
            let timeLeft = userCalendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: eventDate)
            
            if timeLeft.day! > 1 {
                if timeLeft.hour! > 1 {
                    return "\(timeLeft.day!) Days, \(timeLeft.hour!) Hours"
                } else {
                    return "\(timeLeft.day!) Days, \(timeLeft.hour!) Hour"
                }
               
            } else {
                if timeLeft.hour! == 0 {
                    if timeLeft.minute! > 1 {
                        return "\(timeLeft.minute!) Minutes"
                    } else if timeLeft.minute! == 0 {
                       return "End"
                    } else {
                        return "\(timeLeft.minute!) Minute"
                    }
                    
                } else {
                     if timeLeft.minute! > 1 {
                         return "\(timeLeft.hour!) Hours, \(timeLeft.minute!) Minutes"
                     } else {
                         return "\(timeLeft.hour!) Hours, \(timeLeft.minute!) Minute"
                     }
                }
            }
        }
    }
    
    private func checkIfStarted(startTime: String) -> Bool {
        
        let Dateformatter = DateFormatter()
        Dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let start = Dateformatter.date(from: startTime) ?? Date()
        let current = Date()
        
        print("TEST IF STARTED - \(start), \(current)")
        
        if current > start {
            print("Date1 is after Date2")
            return true
        } else if current == start {
            print("Date1 is equal to Date2")
            return true
        } else if current < start {
            print("Date1 is before Date2")
            return false
        }
        
        return false
    }
    
    func convertDateStr(dateStr: String) -> String {
        let selectedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage") ?? ""
        if selectedLanguage == "en" {
            return "\(backToOriginalStr(dateStr: dateStr))"
        } else {
            return "\(replaceStr(dateStr: dateStr))"
        }
    }
    
    func replaceStr(dateStr: String) -> String {
        if dateStr.contains("Jan") {
            return dateStr.replacingOccurrences(of: "Jan", with: "1月")
        } else if dateStr.contains("Feb") {
            return dateStr.replacingOccurrences(of: "Feb", with: "2月")
        } else if dateStr.contains("Mar") {
            return dateStr.replacingOccurrences(of: "Mar", with: "3月")
        } else if dateStr.contains("Apr") {
            return dateStr.replacingOccurrences(of: "Apr", with: "4月")
        } else if dateStr.contains("May") {
            return dateStr.replacingOccurrences(of: "May", with: "5月")
        } else if dateStr.contains("Jun") {
            return dateStr.replacingOccurrences(of: "Jun", with: "6月")
        } else if dateStr.contains("Jul") {
            return dateStr.replacingOccurrences(of: "Jul", with: "7月")
        } else if dateStr.contains("Aug") {
            return dateStr.replacingOccurrences(of: "Aug", with: "8月")
        } else if dateStr.contains("Sep") {
            return dateStr.replacingOccurrences(of: "Sep", with: "9月")
        } else if dateStr.contains("Oct") {
            return dateStr.replacingOccurrences(of: "Oct", with: "10月")
        } else if dateStr.contains("Nov") {
            return dateStr.replacingOccurrences(of: "Nov", with: "11月")
        } else if dateStr.contains("Dec") {
            return dateStr.replacingOccurrences(of: "Dec", with: "12月")
        }
        
        return "\(dateStr)"
    }
    
    func backToOriginalStr(dateStr: String) -> String {
        if dateStr.contains("1月") {
            return dateStr.replacingOccurrences(of: "1月", with: "Jan")
        } else if dateStr.contains("2月") {
            return dateStr.replacingOccurrences(of: "2月", with: "Feb")
        } else if dateStr.contains("3月") {
            return dateStr.replacingOccurrences(of: "3月", with: "Mar")
        } else if dateStr.contains("4月") {
            return dateStr.replacingOccurrences(of: "4月", with: "Apr")
        } else if dateStr.contains("5月") {
            return dateStr.replacingOccurrences(of: "5月", with: "May")
        } else if dateStr.contains("6月") {
            return dateStr.replacingOccurrences(of: "6月", with: "Jun")
        } else if dateStr.contains("7月") {
            return dateStr.replacingOccurrences(of: "7月", with: "Jul")
        } else if dateStr.contains("8月") {
            return dateStr.replacingOccurrences(of: "8月", with: "Aug")
        } else if dateStr.contains("9月") {
            return dateStr.replacingOccurrences(of: "9月", with: "Sep")
        } else if dateStr.contains("10月") {
            return dateStr.replacingOccurrences(of: "10月", with: "Oct")
        } else if dateStr.contains("11月") {
            return dateStr.replacingOccurrences(of: "11月", with: "Nov")
        } else if dateStr.contains("12月") {
            return dateStr.replacingOccurrences(of: "12月", with: "Dec")
        }
        
        return "\(dateStr)"
    }

}
