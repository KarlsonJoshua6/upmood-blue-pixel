//
//  PublicSessionListViewController.swift
//  workspacec
//
//  Created by John Paul Manoza on 27/04/2019.
//  Copyright © 2019 Joseph Mikko Manoza. All rights reserved.
//

import UIKit

let tableViewIdentifier = "PublicSessionListViewControllerCell"
let segueId = "showViewSession"

class PublicSessionListViewControllerCell: UITableViewCell {
    @IBOutlet weak var session_status_activity_indicator_imageview: UIImageView!
    @IBOutlet weak var session_name_label: UILabel!
    @IBOutlet weak var company_name_label: UILabel!
    @IBOutlet weak var session_created_label: UILabel!
    @IBOutlet weak var time_remaining_label: UILabel!
    @IBOutlet weak var session_active_user_count_label: UILabel!
}

class PublicSessionListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var viewModel: PublicSessionListViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        viewModel.fetchPublicSessionList {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func unwindToMySessionFromPublicSession(_ sender: UIStoryboardSegue) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension PublicSessionListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewIdentifier, for: indexPath) as! PublicSessionListViewControllerCell
        configCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemAtIndexPath(section: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 174.0
    }
    
    private func configCell(cell: PublicSessionListViewControllerCell, indexPath: IndexPath) {
        cell.session_name_label.text = viewModel.sessionNameItemAtIndexPath(indexPath: indexPath)
        cell.company_name_label.text = viewModel.companyNameItemAtIndexPath(indexPath: indexPath)
        cell.session_active_user_count_label.text = viewModel.activeUserItemAtIndexPath(indexPath: indexPath)
        cell.session_created_label.text = viewModel.createdDateAtIndexPath(indexPath: indexPath)
        cell.time_remaining_label.text = viewModel.sessionRemainingTime(indexPath: indexPath)
        
        if viewModel.sessionStatus(indexPath: indexPath) == 0 {
            cell.session_status_activity_indicator_imageview.image = UIImage(named: "ic_is_offline_indicator")
            cell.time_remaining_label.text = NSLocalizedString("End", comment: "")
        } else if viewModel.sessionStatus(indexPath: indexPath) == 1 {
            cell.session_status_activity_indicator_imageview.image = UIImage(named: "ic_is_online_dot")
        } else if viewModel.sessionStatus(indexPath: indexPath) == 2 {
            cell.session_status_activity_indicator_imageview.image = UIImage(named: "ic_is_paused_dot")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.set("\(viewModel.sessionIdItemAtIndexPath(indexPath: indexPath))", forKey: "idStr")
        UserDefaults.standard.set(true, forKey: "didUserClickOnPublicSessionList")
        performSegue(withIdentifier: segueId, sender: self)
    }
}
