//
//  RouterAPI.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 07/08/2018.
//  Copyright © 2018 Joseph Mikko Mañoza. All rights reserved.
//

import Alamofire
import KeychainSwift
import Foundation

enum Service: URLRequestConvertible {
    
    case fetchPublicSessionList(keyword: String, time_zone: String)
    case viewSession(withId: Int, time_zone: String)
    case joinSesion(withId: Int, passcode: String)
    case joinSessionTypeFitness(withId: Int, passcode: String, age: Int, weight: Float, sex: String)
    case leaveSession(withId: Int, passcode: String)
    case fetchMySessionList(time_zone: String)
    case searchSessionList(withKeyword: String, time_zone: String)
    
    //guestLogin
    case loginAsGuest(withEmail: String, name: String)
    
    // MARK: Stored
    
    static let baseURLString = "http://18.139.163.192:8080/api/v4/ios"
    
    // MARK: Computed
    
    private var path: String {
        switch self {
        case .fetchPublicSessionList:
            return "/insight/user/session/session-list"
        case .viewSession:
            return "/insight/user/session/view"
        case .joinSesion:
            return "/ios/insight/user/session/join-session"
        case .joinSessionTypeFitness:
            return "/insight/user/session/join-session"
        case .leaveSession:
            return "/insight/user/session/leave-session"
        case .fetchMySessionList:
            return "/insight/user/session/user-session"
        case .searchSessionList:
            return "/insight/user/session/session-list"
        case .loginAsGuest:
            return "/authenticate/guest"
        }
    }
    
    private var method: HTTPMethod {
        switch self {
        case .fetchPublicSessionList: return .post
        case .viewSession: return .post
        case .joinSesion: return .post
        case .joinSessionTypeFitness: return .post
        case .leaveSession: return .post
        case .fetchMySessionList: return .post
        case .searchSessionList: return .post
        case .loginAsGuest: return .post
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try Service.baseURLString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        let token = KeychainSwift().get("apiToken") ?? ""
        
        switch self {
        case .fetchPublicSessionList(let keyword, let time_zone):
            let parameters: [String: Any] = ["keyword": keyword, "timezone": time_zone]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .viewSession(let withId, let time_zone):
            let parameters: [String: Any] = ["id": withId, "timezone": time_zone]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .joinSesion(let withId, let passcode):
            let parameters: [String: Any] = ["id": withId, "passcode": passcode]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .joinSessionTypeFitness(let withId, let passcode, let age, let weight, let sex):
            let parameters: [String: Any] = ["id": withId, "passcode": passcode, "age": age, "weight": weight, "sex": sex]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .leaveSession(let withId, let passcode):
            let parameters: [String: Any] = ["id": withId, "passcode": passcode]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .fetchMySessionList(let time_zone):
            let parameters: [String: Any] = ["timezone": time_zone]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .searchSessionList(let withKeyword, let time_zone):
            let parameters: [String: Any] = ["keyword": withKeyword, "timezone": time_zone]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        case .loginAsGuest(let withEmail, let name):
            let parameters: [String: Any] = ["email": withEmail, "name": name]
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            break
        }
        
        return urlRequest
    }
}
