//
//  DbHelper.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 06/05/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import RealmSwift

class RealmHelper {
    
    static func saveObject<T:Object>(object: T) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(object)
        }
    }
    
    static func deleteAllObjects() {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
}
