//
//  UserIdItem.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 06/06/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import RealmSwift

class UserIdItem: Object {
    
    @objc dynamic var userId = ""
    
    override static func primaryKey() -> String? {
        return "userId"
    }
}
