//
//  Device.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 17/12/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import RealmSwift

class Device: Object {
    
    @objc dynamic var deviceName = ""
    @objc dynamic var deviceUiid = ""
    
    override static func primaryKey() -> String? {
        return "deviceName"
    }
}

