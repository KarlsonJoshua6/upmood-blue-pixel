//
//  Item.swift
//  upmood
//
//  Created by Joseph Mikko Mañoza on 03/05/2019.
//  Copyright © 2019 Joseph Mikko Mañoza. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object {
    
    @objc dynamic var searchText = ""

    override static func primaryKey() -> String? {
        return "searchText"
    }
}
